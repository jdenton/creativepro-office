<? 
$loginForm = 1;
$ads       = 1;
$sajax     = 1;
$loginForm = 1;
$ads       = 1;
$sajax     = 1;
$loginForm = 1;
$ads       = 1;
$sajax     = 1;
$loginForm = 1;
$ads       = 1;
$sajax     = 0;

include("includes/functions.php");

/*
 * Create random key and push to database
 */
$randomKey = random_string(15);

$sql = "INSERT into cpo_sysBuyKey (
		SessionID,
		RandomKey,
		KeyDate
		) values (
		'".session_id()."',
		'$randomKey',
		'".date('Y-m-d')."'
		)";
$result = mysql_db_query($dbname,$sql,$db);

include("includes/header.php"); 
?>
<script type="text/javascript">
function checkEULA() {
	if (document.getElementById('readEULA').checked == false) {
		alert('Please make sure that you have agreed to the terms of the License Agreement.');
		return false;		
	} else {
		return true;
	}
}
</script>	
<div id="left" style="width: 660px; float: left;">	
	<div style="margin-left: 25px;">		
		<h2>Buy CreativePro Office Source Code</h2>
		
		<p>The CreativePro Office version 2 self-hosted version will be available for purchase in November 2010. Please visit <a href="http://www.mycpohq.com">www.MyCPOHq.com</a> for more information.</p>
		
		<p>When you buy the CreativePro Office source code, you get:
		<ul>
			<li>The complete source code to install on your own server and modify as you please,</li>
			<li>A perpetual license - buy once and use forever,
			<li>1 hour of installation support,
			<li>Free upgrades and patches for 1 year from date of purchase.
		</ul>
		
		<p><b>System Requirements</b></p>
		<ul>	
			<li>Web server with PHP 5.2 support. We recommend Apache with PHP 5.2 or greater.</li>
			<li>One MySQL database.</li>
			<li>PHP GD extensions (these should be available by default in most PHP installations)</li>
		</ul>
		
		</p>		
	</div>	
</div>		
<div id="right" style="width: 220px; float: left;">		
	<? include("includes/rightCol.php"); ?>
</div>
<div style="clear: both;"></div>
<? include("includes/footer.php"); ?>	
