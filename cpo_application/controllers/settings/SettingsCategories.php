<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class SettingsCategories extends Controller {
	function __construct()
	{
		parent::Controller();
        checkAuthentication($_POST,$_SERVER);
		loadLanguageFiles();
        
		$this->load->model('common/app_data','AppData',TRUE);
		$this->load->helper('security');
	}

	function newCategory($itemValue,$itemType,$siteArea=null) {
        if ($itemValue == 0) {
            $itemValue = fieldToDB($this->input->post('itemValue', TRUE));
        } else {
            $itemValue = $this->input->xss_clean($itemValue);
        }

		if ($itemType == 'category') {
			$itemID = $this->AppData->saveCategory($this->session->userdata('accountUserid'),null,$itemValue,'add',$siteArea);
			echo makeCategoryJSON(SettingsCategories::getCategories($itemType,$siteArea));
		} elseif ($itemType == 'tag') {
			$itemID = $this->AppData->saveTag($this->session->userdata('accountUserid'),null,$itemValue,'add',$siteArea);
			echo makeTagJSON(SettingsCategories::getCategories($itemType,$siteArea));
		}
	}

	function updateCategory($itemID,$itemValue,$itemType,$siteArea=null) {
        if ($itemValue == 0) {
            $itemValue = fieldToDB($this->input->post('itemValue', TRUE));
        } else {
            $itemValue = $this->input->xss_clean($itemValue);
        }

		if ($itemType == 'category') {
			$this->AppData->saveCategory($this->session->userdata('accountUserid'),$itemID,$itemValue,'edit',$siteArea);
			echo makeCategoryJSON(SettingsCategories::getCategories($itemType,$siteArea));
		} elseif ($itemType == 'tag') {
			$this->AppData->saveTag($this->session->userdata('accountUserid'),$itemID,$itemValue,'edit',$siteArea);
			echo makeTagJSON(SettingsCategories::getCategories($itemType,$siteArea));
		}
	}

	function deleteCategory($itemID,$itemType,$siteArea=null) {
		if ($itemType == 'category') {
			$this->AppData->deleteCategory($itemID);
			echo makeCategoryJSON(SettingsCategories::getCategories($itemType,$siteArea));
		} elseif ($itemType == 'tag') {
			$this->AppData->deleteTag($itemID);
			echo makeTagJSON(SettingsCategories::getCategories($itemType,$siteArea));
		}
	}

	function getCategories($itemType=null,$siteArea=null) {
		if ($itemType == 'category') {
			return $this->AppData->getCategories($this->session->userdata('accountUserid'),$siteArea);
		} elseif ($itemType == 'tag') {
			return $this->AppData->getTags($this->session->userdata('accountUserid'),$siteArea);
		} else {
			return false;
		}
	}
}
?>
