<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//include(APPPATH.'libraries/excel/excel_xml.php');
//RTL Code Start
include_once('/var/www/dev.mycpohq.com/web/cpo_application/controllers/export/tbs_class_php5.php'); 
include_once('/var/www/dev.mycpohq.com/web/cpo_application/controllers/export/tbs_plugin_opentbs.php');
//RTL Code End


class ExportWord extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('common/App_activity','',true);
		$this->load->model('finances/Finance_view','',true);
        $this->load->model('finances/Finance_reports','',true);
		$this->load->model('users/User_information','',TRUE);
		$this->load->model('projects/Project_view','',TRUE);
		$this->load->model('tasks/Task_view','',TRUE);
		$this->load->model('contacts/Contacts','',TRUE);

		$this->load->library('application/CommonFinance');
		$this->load->library('application/CommonTimesheet');
		$this->load->library('application/CommonTask');
	}

	function _init() {

	}

	

    function exportTasks($varString) {
	

	/*
	 * Parse $varString and get our timesheet report array
	 */
	$varArray = explode('---',$varString);
	$projectID   = $varArray[0];
	$milestoneID = $varArray[1];
	$dateStart   = $varArray[2];
	$dateEnd     = $varArray[3];
	$userID      = $varArray[4];
	$seeOnlyMyTasks         = $varArray[5];
	$seeOnlyCompleteTasks   = $varArray[6];
	$seeOnlyIncompleteTasks = $varArray[7];


	$milestoneArray = $this->Task_view->getMilestonesForProject($projectID,1);
	$projectInfoArray = $this->Project_view->getProjectInformation($projectID);
	$userNameFilename = '';
	if ($seeOnlyMyTasks == 1) {
		$userInformationArray  = $this->Contacts->getContactPerson(NULL,$this->session->userdata('userid'));
		$userName = $userInformationArray['NameFirst'].' '.$userInformationArray['NameLast'];
		$userNameFilename = '_'.$userInformationArray['NameFirst'].'_'.$userInformationArray['NameLast'];
	}

	$exportFileName = lang('task_tasks').'_'.$projectInfoArray['Title'].$userNameFilename.'.doc';

	/*
         * Let's create a new array filtered by our variables
         */
	$a=0;
	$b=0;
	$c=0;
	$z=0;
	$total = 0;
	$seeOnlyMyTasks = 1;
	$newMilestoneArray = array();
	$newTaskArray = array();
	foreach($milestoneArray as $milestone) {
		$milestone = $this->commontask->renderMilestone($milestone,'array');
		$milestone['Tasks'] = $this->commontask->renderTasks($milestone['Tasks'],'array');
		$taskArray = $milestone['Tasks'];
        foreach($taskArray as $task) {
			$keep = 0;
            $teamMemberArray = $task['TeamMembers'];
            foreach($teamMemberArray as $teamMember) {
				if ($seeOnlyMyTasks == 1 && $teamMember['UID'] == $this->session->userdata('userid')) {
					$keep = 1;
				}
			}
			if ($seeOnlyCompleteTasks == 1) {
				if ($task['Status'] == 2) {
					$keep = 1;
				}
			}
			if($keep==1){
				$newMilestoneArray[$a]['Tasks'][$b] = $task;
				$newTaskArray[$z] =  $task;
				$total = $total + $task['Cost'];
				$b++;
				$z++;
			}
		}
		

	}//end milestones
//print_r($newTaskArray);
	//RTL Code Start
	$TBS = new clsTinyButStrong(); // new instance of TBS
	$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load OpenTBS plugin

	global $yourname, $testTotal, $TotalWithFees;
	$yourname = $this->session->userdata('fullName');
	$filename = "CPO_Tasks.docx";
	//$TBS->PlugIn(OPENTBS_FORCE_DOCTYPE,'docs');
	$testTotal = $total; //number_format($total,2);
	$TotalWithFees = $total; //number_format($total,2);
	$TBS->LoadTemplate('/var/www/dev.mycpohq.com/web/cpo_application/controllers/export/tmpl.docx');
	$TBS->MergeBlock('a',$newTaskArray);
	$TBS->Show(OPENTBS_DOWNLOAD+TBS_EXIT, $filename);
	/**/
	//RTL Code End
	}
}
	
