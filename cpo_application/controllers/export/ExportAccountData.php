<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set("memory_limit","512M");
include(APPPATH.'libraries/excel/excel_xml.php');

class ExportAccountData extends Controller {
    var $header_style = array(
            'bold'       => 1,
            'size'       => '12',
            'color'      => '#FFFFFF',
            'bgcolor'    => '#4F81BD'
        );

	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('clients/Client_view','',true);
        $this->load->model('projects/Project_view','',true);
		$this->load->model('finances/Finance_view','',true);
        $this->load->model('finances/Finance_view_expenses','',true);
        $this->load->model('timesheets/Timesheet_view','',true);
        $this->load->model('contacts/Contacts','',true);
        $this->load->model('calendar/Calendar_view','',true);
        $this->load->model('messaging/Site_messages','',true);
		$this->load->model('users/User_information','',true);		
		$this->load->model('tasks/Task_view','',true);

		$this->load->library('application/CommonFinance');
		$this->load->library('application/CommonTimesheet');
		$this->load->library('application/CommonTask');
        $this->load->library('application/CommonEmail');
        $this->load->library('zip');
	}

    function _init() {

    }

	function index() {
        $clientFile    = ExportAccountData::exportClients();
        $projectFile   = ExportAccountData::exportProjects();
        $taskFile      = ExportAccountData::exportTasks();
        $invoiceFile   = ExportAccountData::exportInvoices();
        $expenseFile   = ExportAccountData::exportExpenses();
        $timesheetFile = ExportAccountData::exportTimesheets();
        $contactsFile  = ExportAccountData::exportContacts();
        $messagesFile  = ExportAccountData::exportMessages();
        $eventsFile    = ExportAccountData::exportEvents();

        $zipFilename = 'CreativeProOfficeBackup_'.$this->session->userdata('accountUserid').'_'.date('Y-m-d').'.zip';
        $zipFilepath = USER_DIR.$zipFilename;
        $zipFilepathRel = USER_DIR_REL.$zipFilename;

        $this->zip->read_file($clientFile);
        $this->zip->read_file($projectFile);
        $this->zip->read_file($taskFile);
        $this->zip->read_file($invoiceFile);
        $this->zip->read_file($expenseFile);
        $this->zip->read_file($timesheetFile);
        $this->zip->read_file($contactsFile);
        $this->zip->read_file($messagesFile);
        $this->zip->read_file($eventsFile);
        
        $subject = 'Your CreativePro Offie export is ready.';
        $message  = '<p>Hello '.$this->session->userdata('firstName').'</p>';
        $message .= '<p>Your CreativePro Office account export is ready to be downloaded.  Please click the link below to download your export file.</p>';
        $message .= $zipFilepathRel;
        $this->commonemail->sendExportEmail($subject,$message,$this->session->userdata('userEmail'));
        //$this->zip->download($zipFilename);
        $this->zip->archive($zipFilepath);
        
        $data = array(
          'AccountUserid' => $this->session->userdata('accountUserid'),
          'UserPID' => $this->session->userdata('pid'),
          'ProjectID' => 0, 
          'UserType' => $this->session->userdata('userType'),
          'Activity' => 'export',
          'ItemID' => $this->session->userdata('accountUserid'),
          'ItemType' => 'account',
          'ItemTitle' => 'Account data'
        );
        $this->App_activity->logAppActivity($data);
	}

    function exportClients() {
        $excel = new excel_xml();
        $exportFileName = USER_DIR.'Clients_Account Export '.$this->session->userdata('accountUserid').' '.date('Y-m-d').'.xls';
        $clientArray = $this->Client_view->getClientsForUser($this->session->userdata('accountUserid'));
        $excel->add_row(array(
			lang('client_form_company'),
            lang('client_form_address'),
            lang('client_form_city'),
            lang('client_form_state'),
            lang('client_form_zip'),
            lang('client_form_country'),
			lang('client_form_phone1'),
			lang('client_form_phone2'),
			lang('client_form_phone3'),
			lang('client_form_email'),
			lang('client_form_url'),
            lang('client_form_notes'),
            lang('client_form_industry'),
            lang('client_form_client_since'),
			lang('client_form_tags'),
            'Archive'
        ), 'header');

        $excel->add_style('header', $this->header_style);
        foreach($clientArray as $client) {
            if ($client['Archive'] == 1) {
                $archive = 'Archived';
            } else {
                $archive = '';
            }
            $excel->add_row(array(
                DBToField($client['Company'],TRUE),
                DBToField($client['Address'],TRUE),
                $client['City'],
                $client['State'],
                $client['Zip'],
                $client['Country'],
                $client['Phone1'],
                $client['Phone2'],
                $client['Phone3'],
                $client['Email'],
                $client['URL'],
                DBToField($client['Comments'],TRUE),
                $client['Industry'],
                $client['Client_Since'],
                $client['Tags'],
                $archive
            ));
        }

        $excel->create_worksheet('Clients');
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->save($exportFileName);
        return $exportFileName;
    }

    function exportProjects() {
        $excel = new excel_xml();
        $exportFileName = USER_DIR.'Projects_Account Export '.$this->session->userdata('accountUserid').' '.date('Y-m-d').'.xls';
        $projectArray = $this->Project_view->getProjectList($this->session->userdata('accountUserid'),'user','all',NULL,NULL,'all',TRUE);

        $excel->add_row(array(
			lang('client_form_company'),
            lang('project_form_title'),
            lang('project_form_category'),
            lang('project_form_url'),
            lang('project_form_notes'),
            lang('common_date_start'),
            lang('common_date_end'),
			lang('project_complete'),
            lang('project_form_status'),
			lang('client_form_tags'),
            'Archive'
        ), 'header');

        $excel->add_style('header', $this->header_style);
        foreach($projectArray as $project) {
            if ($project['Archive'] == 1) {
                $archive = 'Archived';
            } else {
                $archive = '';
            }
            $statusArray = getProjectStatus($project['Status'],$project['Date_Start'],$project['Date_End']);
            $excel->add_row(array(
                DBToField($project['Company'],TRUE),
                DBToField($project['Title'],TRUE),
                $project['MainCat'],
                $project['URL'],
                DBToField($project['Description'],TRUE),
                $project['Date_Start'],
                $project['Date_End'],
                $project['Date_Complete'],
                $statusArray[0].' '.$statusArray[2],
                $project['Tags'],
                $archive
            ));
        }
        $excel->create_worksheet('Projects');
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->save($exportFileName);
        return $exportFileName;
    }

    function exportTasks() {
        $excel = new excel_xml();
        $exportFileName = USER_DIR.'Tasks_Account Export '.$this->session->userdata('accountUserid').' '.date('Y-m-d').'.xls';
        $taskArray = $this->Task_view->getTasksForExport($this->session->userdata('accountUserid'));

        $excel->add_row(array(
            lang('project_form_title'),
            lang('task_number'),
            lang('task_task'),
            lang('finance_invoice_item_description'),
            lang('common_date_start'),
            lang('common_date_end'),
            lang('common_created_by'),
            lang('common_assigned_to'),
            lang('common_priority'),
            lang('status_status'),
            lang('task_estimated_time'),
            lang('task_actual_hours'),
            lang('task_milestone'),
            lang('client_form_tags')
        ), 'header');

        $excel->add_style('header', $this->header_style);
        foreach($taskArray as $task) {
            $milestone = '';
            if ($task['Milestone'] == '1') {
                $milestone = 'yes';
            }
            $teamMemberString = '';
            foreach($task['TeamMembers'] as $teamMember) {
                $teamMemberString .= $teamMember['NameFirst'].' '.$teamMember['NameLast'].', ';
            }
            $teamMemberString = substr($teamMemberString,0,-2);

            $statusArray = getProjectStatus($task['Status'],$task['Date_Start'],$task['Date_End'],TRUE);
            $statusHuman = $statusArray[0];
            $statusText  = $statusArray[2];

            if (!empty($statusText)) {
                $fullStatusText = $statusHuman.': '.$statusText;
            } else {
                $fullStatusText = $statusHuman;
            }

            /*
             * Task priority
             */
            $priorityArray = getTaskPriorityIcon($task['Priority']);
            $priorityText = $priorityArray[0];

            $excel->add_row(array(
                DBToField($task['ProjectTitle'],TRUE),
                $task['TaskNo'],
                DBToField($task['Title'],TRUE),
                DBToField($task['Description'],TRUE),
                $task['Date_Start'],
                $task['Date_End'],
                $task['CreatorName'],
                $teamMemberString,
                $priorityText,
                $fullStatusText,
                $task['HoursEstimated'],
                $task['HoursActual'],
                $task['Milestone'],
                $task['Tags']
            ));
        }

        $excel->create_worksheet('Tasks');
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->save($exportFileName);
        return $exportFileName;
    }

    function exportInvoices() {
        $excel = new excel_xml();
        $exportFileName = USER_DIR.'Invoices_Account Export '.$this->session->userdata('accountUserid').' '.date('Y-m-d').'.xls';
        $invoiceArray = $this->Finance_view->getInvoicesForExport($this->session->userdata('accountUserid'));

        $excel->add_row(array(
			lang('client_form_company'),
            lang('project_form_title'),
            lang('common_category'),
            lang('finance_invoice_number'),
            lang('finance_invoice_title'),
            lang('finance_total_invoiced'),
            lang('finance_date_created'),
            lang('finance_date_sent'),
            lang('finance_date_paid'),
            lang('common_date_due'),
            lang('common_status')
        ), 'header');

        $excel->add_style('header', $this->header_style);
        foreach($invoiceArray as $invoice) {
            $statusArray = getInvoiceStatus($invoice['Status'],$invoice['Paid'],$invoice['Sent'],$invoice['DateInvoice'],$invoice['DatePaid'],$invoice['DateSent'],$invoice['PaymentDueDate']);

            $excel->add_row(array(
                DBToField($invoice['Company'],TRUE),
                DBToField($invoice['ProjectTitle'],TRUE),
                $invoice['MainCat'],
                $invoice['InvNo'],
                DBToField($invoice['Title'],TRUE),
                $invoice['InvTotal'],
                $invoice['DateInvoice'],
                $invoice['DateSent'],
                $invoice['DatePaid'],
                $invoice['PaymentDueDate'],
                $statusArray[0]
            ));
        }

        $excel->create_worksheet('Invoices');
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->save($exportFileName);
        return $exportFileName;
    }

    function exportExpenses() {
        $excel = new excel_xml();
        $exportFileName = USER_DIR.'Expenses_Account Export '.$this->session->userdata('accountUserid').' '.date('Y-m-d').'.xls';
        $expenseArray = $this->Finance_view_expenses->getExpensesForExport($this->session->userdata('accountUserid'));

        $excel->add_row(array(
			lang('client_form_company'),
            lang('project_form_title'),
            lang('common_category'),
            lang('finance_vendor_name'),
            lang('finance_vendor_company'),
            lang('finance_expense_name'),
            lang('finance_expense_amount'),
            lang('finance_expense_payment_method'),
            lang('finance_expense_pay_method_acct_no'),
            lang('finance_expense_reimbursement'),
            lang('finance_expense_markup'),
            lang('finance_expense_notes'),
            lang('finance_expense_purchase_date'),
            lang('client_form_tags')
        ), 'header');

        $excel->add_style('header', $this->header_style);
        foreach($expenseArray as $expense) {
            switch($expense['PaymentMethod']) {
                case 'cc_company':
                    $expense['PaymentMethod'] = 'company credit card';
                    break;
                case 'cc_personal':
                    $expense['PaymentMethod'] = 'personal credit card';
                    break;
            }

            $markupSuffix = '';
            if ($expense['MarkupType'] == 'percent') {
                $markupSuffix = '%';
            }

            $excel->add_row(array(
                DBToField($expense['Company'],TRUE),
                DBToField($expense['ProjectTitle'],TRUE),
                $expense['MainCat'],
                $expense['NameFirst'].' '.$expense['NameLast'],
                DBToField($expense['VendorCompany'],TRUE),
                DBToField($expense['Title'],TRUE),
                $expense['Amount'],
                $expense['PaymentMethod'],
                $expense['AccountNumber'],
                $expense['Reimbursement'],
                $expense['Markup'].$markupSuffix,
                DBToField($expense['Comments'],TRUE),
                $expense['DateExpense'],
                $expense['Tags']
            ));
        }

        $excel->create_worksheet('Expenses');
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->save($exportFileName);
        return $exportFileName;
    }

    function exportTimesheets() {
        $excel = new excel_xml();
        $exportFileName = USER_DIR.'TimesheetRecords_Account Export '.$this->session->userdata('accountUserid').' '.date('Y-m-d').'.xls';
        $timesheetArray = $this->Timesheet_view->getTimesheetRecordsForExport($this->session->userdata('accountUserid'));

        $excel->add_row(array(
			'Client',
			'Project',
			'Task',
			'Comments',
			'Team member',
			'Billable',
			'Date',
			'Hours'
			), 'header');

        $excel->add_style('header', $this->header_style);
        foreach($timesheetArray as $entry) {
            if ($entry['Billable'] == 1) {
                $billableString = lang('common_yes');
            } else {
                $billableString = lang('common_no');
            }

            $excel->add_row(array(
                DBToField($entry['Company'],TRUE),
                DBToField($entry['ProjectTitle'],TRUE),
                DBToField($entry['TaskTitle'],TRUE),
                DBToField($entry['Comments'],TRUE),
                $entry['NameFirst'].' '.$entry['NameLast'],
                $billableString,
                $entry['DateClockEnd'],
                $entry['ElapsedTime']
            ));
        }

        $excel->create_worksheet('Timesheet Records');
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->save($exportFileName);
        return $exportFileName;
    }

    function exportContacts() {
        $excel = new excel_xml();
        $exportFileName = USER_DIR.'Contacts_Account Export '.$this->session->userdata('accountUserid').' '.date('Y-m-d').'.xls';
        $contactArray = $this->Contacts->getContacts($this->session->userdata('accountUserid'));

        $headerArray = array(
            lang('common_first_name'),
            lang('common_last_name'),
            lang('client_form_company'),
            lang('common_title'),
            lang('client_form_address'),
            lang('client_form_city'),
            lang('client_form_state'),
            lang('client_form_zip'),
            lang('client_form_country'),
            lang('client_form_timezone'),
            lang('common_language'),
            lang('common_information'),
            lang('common_avatar'),
            lang('client_form_tags'),
            lang('finance_invoice_item_type')
        );
        for($a=1;$a<=5;$a++) {
            array_push($headerArray,'Contact '.$a,'Contact Type '.$a,'Contact Location '.$a);
        }
        $excel->add_row($headerArray, 'header');

        $excel->add_style('header', $this->header_style);
        foreach($contactArray as $contact) {
            $contactType = '';
            if ($contact['TeamMember'] == 1) {
                $contactType .= 'Team member';
            } elseif ($contact['Vendor'] == 1) {
                $contactType .= ' Vendor';
            }

            $exportArray = array(
                $contact['NameFirst'],
                $contact['NameLast'],
                DBToField($contact['Company'],TRUE),
                $contact['Title'],
                DBToField($contact['Address'],TRUE),
                $contact['City'],
                $contact['State'],
                $contact['Zip'],
                $contact['Country'],
                $contact['Timezone'],
                $contact['Language'],
                DBToField($contact['Info'],TRUE),
                $contact['AvatarURL'],
                $contact['Tags'],
                $contactType
            );

            $contactItemsArray = $contact['ContactItems'];
            for($a=0;$a<=4;$a++) {
                if (!empty($contactItemsArray[$a]['ContactData']) && trim($contactItemsArray[$a]['ContactData']) != '|') {
                    array_push($exportArray,$contactItemsArray[$a]['ContactData'],$contactItemsArray[$a]['ContactType'],$contactItemsArray[$a]['ContactLocation']);
                } else {
                    array_push($exportArray,'','','');
                }
            }
            $excel->add_row($exportArray);
        }

        $excel->create_worksheet('Contacts');
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->save($exportFileName);
        return $exportFileName;
    }

    function exportMessages() {
        $excel = new excel_xml();
        $exportFileName = USER_DIR.'Messages_Account Export '.$this->session->userdata('accountUserid').' '.date('Y-m-d').'.xls';
        $messageArrayProjects = $this->Site_messages->getMessagesForExport($this->session->userdata('accountUserid'),'project');
        $messageArrayClients = $this->Site_messages->getMessagesForExport($this->session->userdata('accountUserid'),'client');
        $messageArrayInvoices = $this->Site_messages->getMessagesForExport($this->session->userdata('accountUserid'),'invoice');
        $messageArrayTasks = $this->Site_messages->getMessagesForExport($this->session->userdata('accountUserid'),'task');
        $messageArrayExpenses = $this->Site_messages->getMessagesForExport($this->session->userdata('accountUserid'),'expense');

        $excel->add_row(array(
            'Project',
            'Client Company',
            'Invoice',
            'Expense',
            'Task',
            'Sender',
            'Message',
            'Message Date',
            'Date Read'
        ), 'header');

        $excel->add_style('header', $this->header_style);

        foreach($messageArrayProjects as $message) {
            $excel->add_row(array(
                DBToField($message['Title'],TRUE),
                '',
                '',
                '',
                '',
                $message['NameFirst'].' '.$message['NameLast'],
                DBToField($message['Message'],TRUE),
                $message['DateMessage'],
                $message['DateRead']
            ));
        }

        foreach($messageArrayClients as $message) {
            $excel->add_row(array(
                '',
                DBToField($message['Title'],TRUE),
                '',
                '',
                '',
                $message['NameFirst'].' '.$message['NameLast'],
                DBToField($message['Message'],TRUE),
                $message['DateMessage'],
                $message['DateRead']
            ));
        }

        foreach($messageArrayInvoices as $message) {
            $excel->add_row(array(
                '',
                '',
                $message['InvNo'].' '.$message['Title'],
                '',
                '',
                $message['NameFirst'].' '.$message['NameLast'],
                DBToField($message['Message'],TRUE),
                $message['DateMessage'],
                $message['DateRead']
            ));
        }

        foreach($messageArrayExpenses as $message) {
            $excel->add_row(array(
                '',
                '',
                '',
                $message['Title'],
                '',
                $message['NameFirst'].' '.$message['NameLast'],
                DBToField($message['Message'],TRUE),
                $message['DateMessage'],
                $message['DateRead']
            ));
        }

        foreach($messageArrayTasks as $message) {
            $excel->add_row(array(
                '',
                '',
                '',
                '',
                $message['Title'],
                $message['NameFirst'].' '.$message['NameLast'],
                DBToField($message['Message'],TRUE),
                $message['DateMessage'],
                $message['DateRead']
            ));
        }

        $excel->create_worksheet('Messages');
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->save($exportFileName);
        return $exportFileName;
    }

    function exportEvents() {
        $excel = new excel_xml();
        $exportFileName = USER_DIR.'Events_Account Export '.$this->session->userdata('accountUserid').' '.date('Y-m-d').'.xls';
        $eventArray = $this->Calendar_view->getEventsForExport($this->session->userdata('accountUserid'));

        $excel->add_row(array(
            lang('common_title'),
            lang('common_date_start'),
            lang('common_date_end'),
            lang('calendar'),
            lang('finance_invoice_item_description'),
            lang('common_location'),
            lang('client_form_tags')
        ), 'header');

        $excel->add_style('header', $this->header_style);

        foreach($eventArray as $event) {
            $excel->add_row(array(
                DBToField($event['Title'],TRUE),
                $event['DateStart'],
                $event['DateEnd'],
                $event['Calendar'],
                DBToField($event['Description'],TRUE),
                DBToField($event['Location'],TRUE),
                $event['Tags']
            ));
        }
        
        $excel->create_worksheet('Events');
		$xml = $excel->generate($this->session->userdata('fullName'),$this->session->userdata('userCompany'));
		$excel->save($exportFileName);
        return $exportFileName;
    }
}
?>