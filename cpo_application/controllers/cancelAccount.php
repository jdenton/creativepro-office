<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CancelAccount extends Controller {
	function __construct()
	{
		parent::Controller();
		$this->load->model('common/public_site','',true);
	}

    function _init() {
		$data['child']           = TRUE;
        $data['headerTextImage'] = 'txtCancelHeader_public.png';
        $data['rightColumnComponents'] = array('loginBox','whosUsingCPO');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'Cancel my account';

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        return $data;
    }

    function index() {
		$data = CancelAccount::_init();
        $this->load->view('public_site/CancelAccount',$data);
    }
}
?>
