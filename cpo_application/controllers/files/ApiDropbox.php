<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ApiDropbox extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

		$this->load->model('messaging/Site_messages','SiteMessages',true);
        $this->load->model('tasks/Task_view','',true);
        $this->load->library('application/CommonMessages');
		$this->load->library('application/CommonEmail');

		$this->load->helper('zend');

        Zend_Loader::loadClass('Zend_Gdata_AuthSub');
        Zend_Loader::loadClass('Zend_Gdata_Docs');
	}

	function _init() {

    }
}
?>