<?
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProjectUpdate extends Controller {
	
	function __construct()
	{
		parent::Controller();
        checkPermissionsForController($this->router->fetch_class());
        checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

        $this->load->model('common/app_data','AppData',TRUE);
        $this->load->model('projects/project_update','ProjectUpdate',TRUE);
        $this->load->model('projects/project_view','ProjectView',TRUE);
        $this->load->model('projects/project_template','ProjectTemplate',TRUE);
        $this->load->model('clients/client_view','Clients',TRUE);
        $this->load->model('clients/client_update','ClientUpdate',TRUE);

        $this->load->library('application/CommonAppData');
	}
	
	function _init($projectID=NULL,$clientID=NULL) {
        global $subMenuArray;
        $data['page']            = 'projects';
        $data['pageTitle']       = lang('project_page_title');
        $data['pageTitleLink']   = site_url('projects/ProjectView');
        $data['wysiwyg']         = 0;
        $data['map']             = 0;
        $data['validate']        = 1;
        $data['jsFileArray']     = array('jsProjects.js');
        $data['subMenuPage']     = $subMenuArray['projects'];
        $data['pageIconClass']   = 'iconPageProjects';
        $data['pageLayoutClass'] = 'withRightColumn';
		
        $messageArray = array(
            'clientID'     => lang('form_required_client'),
            'projectTitle' => lang('form_required_project_title')
            );
        /*
         * Create tag string for projects
         */				
        $tagArray = $this->AppData->getTags($this->session->userdata('accountUserid'),'project');
        $data['tags'] = makeTagJSON($tagArray);

        $data['validationJSON'] = json_encode($messageArray);				

        /*
         * Get client list for select box
         */
        $data['clients'] = $this->Clients->getClientsForSelect();

        /*
         * Get category list
         */
        $data['categories'] = $this->AppData->getCategories($this->session->userdata('accountUserid'),'project');
        $data['catJSON']    = makeCategoryJSON($data['categories']);

        /*
         * Templates
         */
        $data['templates']  = $this->ProjectTemplate->getProjectTemplates($this->session->userdata('accountUserid'));
		        
        if ($projectID>0) {
            /*
             * We're editing, retrieve and format project data
             */
            if (!$this->ProjectView->checkForValidProject($projectID,$this->session->userdata('accountUserid'))) {
                header('Location: '.site_url('projects/ProjectView'));
            } else {
                $data['action'] = 'edit';
                $data['projectInformation'] = ProjectUpdate::getProjectDetails($projectID);
                
                if ($data['projectInformation']['Tags'] == '0') {
                    $data['projectInformation']['Tags'] = '';
                }
            }
            $data['pageSubTitle'] = lang('project_edit_page_title');

        } else {
            $data['projectInformation']['ProjectDateStart'] = MySQLDateToJS(date('Y-m-d'));
            $data['projectInformation']['ProjectDateEnd']   = MySQLDateToJS(new_date_from_days(date('Y-m-d'),30));
            $data['projectInformation']['ProjectID']        = null;
            $data['projectInformation']['TemplateID']       = null;
            $data['projectInformation']['CatID']            = null;
            $data['projectInformation']['Category']         = null;
            $data['projectInformation']['ClientID']         = null;
            $data['projectInformation']['ClientCompany']    = null;
            $data['projectInformation']['Title']            = null;
            $data['projectInformation']['Description']      = null;
            $data['projectInformation']['Budget']           = null;
            $data['projectInformation']['URL']              = null;
            $data['projectInformation']['RateAmount']       = null;
            $data['projectInformation']['RatePer']          = null;
            $data['projectInformation']['RateDescription']  = null;
            $data['projectInformation']['Tags']             = null;
            $data['projectInformation']['TagString']        = null;
            $data['projectInformation']['Status']           = null;

            $data['action'] = 'add';
            $data['pageSubTitle'] = lang('project_add_page_title');

            /*
             * Can we even add a new project?
             */
            $projectCountArray = $this->ProjectView->getNumberOfProjects($this->session->userdata('accountUserid'),null,0);
            $projectCount = $projectCountArray['Projects'];
            if ($projectCount >= getAccountLevelSetting('projects')) {
                header('Location: '.BASE_URL.'projects/ProjectView/maxProjects');
            }
        }

        if ($clientID>0) {
            $clientArray = $this->Clients->getClientInformation($clientID);
            $data['projectInformation']['ClientID'] = $clientArray['ClientID'];
            $data['projectInformation']['ClientCompany'] = $clientArray['Company'];
        }

        noCache();
		return $data;
	}
	
	function index($projectID=NULL,$clientID=NULL) {
        /*
         * Are we supposed to be editing
         */
		$data = ProjectUpdate::_init($projectID,$clientID);
		$this->load->view('projects/ProjectUpdate',$data);
	}
	
	function getProjectDetails($projectID) {
            $projectArray = $this->ProjectView->getProjectInformation($projectID);

            /*
             * Format dates
             */
            $projectArray['ProjectDateStart'] = MySQLDateToJS($projectArray['Date_Start']);
            $projectArray['ProjectDateEnd']   = MySQLDateToJS($projectArray['Date_End']);

            if ($projectArray['URL'] == '0') {
                $projectArray['URL'] = '';
            }

            $projectArray['Description'] = DBToField($projectArray['Description'],TRUE);
            $clientID = $projectArray['ClientID'];
            $clientArray = $this->Clients->getClientInformation($clientID);

            if (isset($clientArray['Company'])) {
                $projectArray['ClientCompany'] = $clientArray['Company'];
            } else {
                $projectArray['ClientCompany'] = NULL;
            }

            /*
             * TODO: Placeholder until we get the rate thing sorted out
             */
            $projectArray['RateAmount']      = '';
            $projectArray['RatePer']         = 'hour';
            $projectArray['RateDescription'] = '';
        
            return $projectArray;
	}
	
	function saveProject() {
            if (isset($_POST['save']) && $_POST['save'] == 1) {
                if ($this->form_validation->run() == FALSE)	{
                    $data = ProjectUpdate::_init();
                    $this->load->view('projects/ProjectUpdate',$data);
                } else {
		/*
                 * Are we saving a new client?
                 */
                if (empty($_POST['clientIDHolder'])) {
                    /*
                     * Then save the new client name
                     */
                    $clientCompany = fieldToDB($this->input->post('clientID', TRUE));
                    $clientArray = array(
                        'userid'        => $this->session->userdata('accountUserid'),
                        'clientCompany' => $clientCompany,
                        'action'        => 'add',
                        'clientCategory'=> '',
                        'clientAddress' => '',
                        'clientCity'    => '',
                        'clientState'   => '',
                        'clientZip'     => '',
                        'clientCountry' => '',
                        'clientPhone1'  => '',
                        'clientPhone2'  => '',
                        'clientPhone3'  => '',
                        'clientEmail'   => '',
                        'clientURL'     => '',
                        'clientNotes'   => '',
                        'clientArchive' => '',
                        'clientStatus'  => '',
                        'clientIndustry'=> '',
                        'clientSince'   => '',
                        'clientTimezone'=> '',
                        'clientLanguage'=> '',
                        'clientTags'    => ''
                    );
                    $projectArray['clientID'] = $this->ClientUpdate->saveClient($clientArray);
                    $_POST['projectID'] = NULL;
                } else {
                    $projectArray['clientID'] = $_POST['clientIDHolder'];
                }

                $projectArray['projectID']      = $_POST['projectID'];
                $projectArray['action']         = $_POST['action'];
                $projectArray['projectTitle']   = fieldToDB($this->input->post('projectTitle', TRUE));
                $projectArray['projectNotes']   = fieldToDB(strip_tags($this->input->post('projectNotes', TRUE)), TRUE,TRUE,FALSE,TRUE);
                $projectArray['projectBudget']  = str_to_float($this->input->post('projectBudget', TRUE));
                $projectArray['projectURL']     = fieldToDB($this->input->post('projectURL', TRUE), TRUE,FALSE,TRUE);
                 
                /*
                 * Prep dates
                 */
                $projectArray['projectDateStart'] = null;
                $projectArray['projectDateEnd']   = null;
                if (!empty($_POST['projectDateStart'])) {
                    $projectArray['projectDateStart'] = jsDateToMySQL($this->input->post('projectDateStart', TRUE));
                }
                if (!empty($_POST['projectDateEnd'])) {
                    $projectArray['projectDateEnd'] = jsDateToMySQL($this->input->post('projectDateEnd', TRUE));
                }
                
                /*
                 * Project category
                 */
                $projectArray['projectCategory'] = $this->commonappdata->saveCategory(fieldToDB($this->input->post('projectCategoryCombo', TRUE)),'project');
                
                $projectArray['projectStatus']    = $_POST['projectStatus'];
                
                $projectArray['projectTags'] = fieldToDB($this->input->post('projectTagsHolder', TRUE));
                $projectArray['projectTags'] = str_replace(',',' ',$projectArray['projectTags']);                

                $projectArray['projectTemplateID']= $_POST['projectTemplate'];
                $projectArray['userid']           = $this->session->userdata('userid');
                $projectArray['accountUserid']    = $this->session->userdata('accountUserid');

				
                $newProjectID = $this->ProjectUpdate->saveProject($projectArray);

                /*
                 * Let's take care of some template stuff
                 */
                $projectArray['TemplateID'] = $_POST['projectTemplate'];
                if (
                    $projectArray['action'] == 'edit' &&
                    $projectArray['TemplateID'] != $_POST['origTemplateID'] &&
                    !empty($_POST['origTemplateID']))
                {
                    /*
                     * Then we are updating templates for an existing project.
                     * We need to delete the template entries for this project.
                     */
                     $this->ProjectTemplate->removeTemplateItemsForProject($projectArray['projectID']);
                }

                if ( ($projectArray['action'] == 'add' && $projectArray['TemplateID'] > 0) || ($projectArray['action'] == 'edit' && $projectArray['TemplateID'] != $_POST['origTemplateID']) ) {
                    $this->load->model('files/file_maintenance','',TRUE);
                    $this->load->model('tasks/task_update','',TRUE);
                    $this->load->model('notes/site_notes','',TRUE);
                    $this->load->model('finances/expense_update','',TRUE);
                    $this->load->model('team/team','',TRUE);
                    $this->load->model('contacts/contacts','',TRUE);

                    /*
                     * Get template definition.
                     */
                    $templateArray = $this->ProjectTemplate->getProjectTemplate($projectArray['TemplateID']);
                    $templateProjectID = $templateArray['ProjectID'];

                    /*
                     * Create template items.
                     */
                    if ($templateArray['IncludeMilestones'] == 1 && $templateArray['IncludeTasks'] == 1) {
                        $this->task_update->makeTemplateRecords($templateProjectID,$newProjectID,$this->session->userdata('userid'),true,true);
                    } elseif ($templateArray['IncludeMilestones'] == 1) {
                        $this->task_update->makeTemplateRecords($templateProjectID,$newProjectID,$this->session->userdata('userid'),true,false);
                    } elseif ($templateArray['IncludeTasks'] == 1) {
                        $this->task_update->makeTemplateRecords($templateProjectID,$newProjectID,$this->session->userdata('userid'),false,true);
                    }
                    if ($templateArray['IncludeContacts'] == 1) {
                        $this->contacts->makeTemplateRecords($templateProjectID,$newProjectID,$this->session->userdata('userid'),false);
                    }
                    if ($templateArray['IncludeTeam'] == 1) {
                        $this->contacts->makeTemplateRecords($templateProjectID,$newProjectID,$this->session->userdata('userid'),true);
                    }
                    if ($templateArray['IncludeExpenses'] == 1) {
                        $this->expense_update->makeTemplateRecords($templateProjectID,$newProjectID,$this->session->userdata('userid'));
                    }
                    if ($templateArray['IncludeFiles'] == 1) {
                        $this->file_maintenance->makeTemplateRecords($templateProjectID,$newProjectID,$this->session->userdata('userid'));
                    }
                    if ($templateArray['IncludeNotes'] == 1) {
                        $this->site_notes->makeTemplateRecords($templateProjectID,$newProjectID,$this->session->userdata('userid'));
                    }

                }

                /*
                 * Save tags
                 */
                if (!empty($projectArray['projectTags'])) {
                    $this->commonappdata->saveTags($projectArray['projectTags'],'project');
                }
                
                /*
                 * Log activity
                 */
                $activity = 'create';
                if ($projectArray['action'] != 'add') {
                    $activity = 'update';
                }
                $data = array(
                    'AccountUserid' => $this->session->userdata('accountUserid'),
                    'UserPID' => $this->session->userdata('pid'),
                    'ProjectID' => $newProjectID,
                    'UserType' => $this->session->userdata('userType'),
                    'Activity' => $activity,
                    'ItemID' => $newProjectID,
                    'ItemType' => 'project',
                    'ItemTitle' => $projectArray['projectTitle'],
                    'ReportToUser' => 1
                );
                $this->App_activity->logAppActivity($data);
				header('Location: '.BASE_URL.'projects/ProjectDetail/index/'.$newProjectID);
			}
		} else {	
			header('Location: '.BASE_URL.'projects/ProjectView');
		}	
	}

	function updateProjectStatus($currentStatus,$projectID) {
		/*
		 * Determine new status
		 */
		if ($currentStatus == 4) {
			$newStatus = 0;
		} else {
			$newStatus = $currentStatus+1;
		}

		if ($newStatus == 2) {
			$date = date('Y-m-d');
		} else {
			$date = '0000-00-00';
		}

		$statusArray = getProjectStatus($newStatus);
		$statusJSON = '{"StatusText":"'.$statusArray[0].'","StatusCSSClass":"'.$statusArray[1].'","StatusDayText":"'.$statusArray[2].'","Status":"'.$newStatus.'"}';

		/*
		 * Update completed date
		 */
		$this->ProjectUpdate->updateProjectStatus($projectID,$newStatus,$date);
		echo $statusJSON;
	}

    function updateArchive($projectID) {
        $error = FALSE;
        $archiveArray = $this->ProjectView->getArchiveStatus($projectID);
        $currentArchive = $archiveArray[0]['Archive'];
        $action = 'archive';
        if ($currentArchive == 0) {
            $newArchive = 1;
        } else {
            /*
             * Reopen this project - but let's see if we aren't maxed
             * out on open projects yet.
             */
            $projectCountArray = $this->ProjectView->getNumberOfProjects($this->session->userdata('accountUserid'),null,0);
            $projectCount = $projectCountArray['Projects'];
            if ($projectCount >= getAccountLevelSetting('projects')) {
                $error = TRUE;
            }
            $newArchive = 0;
            $action = 'reopen';
        }
        if ($error != TRUE) {
            $projectArray = ProjectUpdate::getProjectDetails($projectID);
            $title = $projectArray['Title'];
            $data = array(
                'AccountUserid' => $this->session->userdata('accountUserid'),
                'UserPID' => $this->session->userdata('pid'),
                'ProjectID' => $projectID,
                'UserType' => $this->session->userdata('userType'),
                'Activity' => 'delete',
                'ItemID' => $projectID,
                'ItemType' => 'project',
                'ItemTitle' => $title,
                'ReportToUser' => 1
            );
            $this->App_activity->logAppActivity($data);
            
            $this->ProjectUpdate->updateArchive($projectID,$newArchive,$this->session->userdata('accountUserid'),$this->session->userdata('userid'));
        } else {
            echo 'maxProjects';
        }
        //return $newArchive;
    }

    function deleteProject($projectID,$tagForDelete=TRUE) {
        if ($tagForDelete == true) {
            $projectArray = ProjectUpdate::getProjectDetails($projectID);
            $title = $projectArray['Title'];
            $data = array(
                'AccountUserid' => $this->session->userdata('accountUserid'),
                'UserPID' => $this->session->userdata('pid'),
                'ProjectID' => $projectID,
                'UserType' => $this->session->userdata('userType'),
                'Activity' => 'delete',
                'ItemID' => $projectID,
                'ItemType' => 'project',
                'ItemTitle' => $title,
                'ReportToUser' => 1
            );
            $this->App_activity->logAppActivity($data);
        }    
        $this->ProjectUpdate->deleteProject($projectID,$tagForDelete,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }

    function restoreProject($projectID) {
        $this->ProjectUpdate->restoreProject($projectID,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }
}	
?>