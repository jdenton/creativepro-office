<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProjectDetail extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

        $this->load->model('projects/Project_view','',true);
		$this->load->model('finances/Finance_view','',true);
		$this->load->model('contacts/Contacts','',true);
        $this->load->model('common/App_data','AppData',true);
        
		$this->load->library('application/CommonProject');
        $this->load->library('application/CommonAppData');
        $this->load->helper('zend');
	}

	function _init($projectID=NULL) {
		$data['page']                  = 'projects';
		$data['pageTitle']             = lang('project_page_title');
		$data['pageTitleLink']         = site_url('projects/ProjectView');
        $data['wysiwyg']               = 1;
        $data['charts']                = 1;
		$data['map']                   = 0;
		$data['projectID']             = $projectID;
		$data['jsFileArray']           = array('jsClients.js','jsProjects.js','jsFinances.js','jsContacts.js','jsTasks.js','jsFileManager.js','jsSettings.js');
		$data['pageIconClass']         = 'iconPageProjects';
		$data['pageLayoutClass']       = 'withRightColumn';
        $data['tagRenderArray']        = $this->commonappdata->renderTags('project','array');
        $data['taskTagRenderArray']    = $this->commonappdata->renderTags('task','array');
        $data['rightColumnComponents'] = array('taskSidebar','tagRender','peopleSidebar','feedSidebar');
        $data['pageButtons'] = array();
        array_push($data['pageButtons'], '<button class="buttonExpand buttonEditMain action hide" id="buttonedit" itemid="'.$projectID.'" title="'.lang('button_edit').'"><span class="edit">'.lang('button_edit').'</span></button>');
        array_push($data['pageButtons'], '<button class="buttonExpand buttonDeleteMain action hide" id="buttondelete" itemid="'.$projectID.'" title="'.lang('button_delete').'"><span class="delete">'.lang('button_delete').'</span></button>');
        array_push($data['pageButtons'], '<button class="buttonExpand buttonPrint green hide" id="buttonprint" itemid="'.$projectID.'" title="'.lang('button_print').'"><span class="print">'.lang('button_print').'</span></button>');
        array_push($data['pageButtons'], '<button class="buttonExpand buttonExportExcel green hide" id="buttonexportExcel" itemid="'.$projectID.'" title="'.lang('button_export_excel').'"><span class="print">'.lang('button_export_excel').'</span></button>');
        
		$data['projectInformation'] = ProjectDetail::getProjectDetail($projectID,'json',1);
		$jsonArray = json_decode($data['projectInformation'], true);
		$data['pageSubTitle']      = $jsonArray['Title'];

        $tagArray = $this->AppData->getTags($this->session->userdata('accountUserid'),'task');
		$data['tags'] = makeTagJSON($tagArray);

		/*
		 * Create RSS Feed Links
		 */
		$taskFeedLink  = site_url('rss/Rss').'/createRSSFeed/'.$this->session->userdata('persAuthString').'/task/'.$projectID;
		$taskFeedTitle = lang('widget_tasks').' : '.$jsonArray['Title'];

		$calendarFeedLink  = site_url('rss/Rss').'/createRSSFeed/'.$this->session->userdata('accountAuthString').'/calendar/'.$projectID.'/project';
		$calendarFeedTitle = lang('widget_calendar').' : '.$jsonArray['Title'];

		$messageFeedLink  = site_url('rss/Rss').'/createRSSFeed/'.$this->session->userdata('accountAuthString').'/message/'.$projectID.'/project';
		$messageFeedTitle = lang('widget_messages').' : '.$jsonArray['Title'];

		$fileFeedLink  = site_url('rss/Rss').'/createRSSFeed/'.$this->session->userdata('accountAuthString').'/files/'.$projectID.'/project';
		$fileFeedTitle = lang('files_files').' : '.$jsonArray['Title'];

		$invoiceFeedLink  = site_url('rss/Rss').'/createRSSFeed/'.$this->session->userdata('accountAuthString').'/invoice/'.$projectID.'/project';
		$invoiceFeedTitle = lang('widget_invoices').' : '.$jsonArray['Title'];

		$data['rssFeeds'] = array(
			array('Link' => $taskFeedLink, 'Title' => $taskFeedTitle, 'Type' => 'task')
			//array('Link' => $calendarFeedLink, 'Title' => $calendarFeedTitle, 'Type' => 'calendar'),
			//array('Link' => $messageFeedLink, 'Title' => $messageFeedTitle, 'Type' => 'message'),
			//array('Link' => $fileFeedLink, 'Title' => $fileFeedTitle, 'Type' => 'file'),
			//array('Link' => $invoiceFeedLink, 'Title' => $invoiceFeedTitle, 'Type' => 'invoice')
		);

        $data['permissions'] = $this->session->userdata('permissions');

        noCache();
		return $data;
	}

	function index($projectID=NULL) {
		if (!is_numeric($projectID)) {
			header('Location: '.BASE_URL.'projects/ProjectView');
		}
		$data = ProjectDetail::_init($projectID);
		$this->load->view('projects/ProjectDetail',$data);
	}

    function getProjectDetail($projectID,$renderType=NULL,$return=1) {
        /*
		 * Check and make sure we can view this project
		 */
        $viewAll = TRUE;
        $permArray = $this->session->userdata('permissions');
        if ($permArray['projectViewOwn'] == 1) {
            $viewAll = FALSE;
        }
            
		if (!$this->Project_view->checkForValidProject($projectID,$this->session->userdata('accountUserid'),null,$viewAll) || empty($projectID)) {
			/*
			 * Send them back to this users' project view page.
			 */
			header('Location: '.site_url('projects/ProjectView'));
		} else {
			$projectArray = $this->Project_view->getProjectInformation($projectID);
			if ($renderType == 'array') {
				return $this->commonproject->renderProject($projectArray,'array');
			} elseif ($renderType == 'json') {
				if ($return == 1) {
					return $this->commonproject->renderProject($projectArray,'json');
				} else {
					echo $this->commonproject->renderProject($projectArray,'json');
				}
			}
		}
     }
}
?>
