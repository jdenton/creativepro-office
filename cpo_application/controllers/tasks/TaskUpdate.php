<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TaskUpdate extends Controller {
	function TaskUpdate()
	{
		parent::Controller();
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('tasks/task_update','TaskUpdate',TRUE);
        $this->load->model('tasks/task_view','TaskView',TRUE);
        $this->load->model('files/file_maintenance','FileMaintenance',TRUE);
        $this->load->model('calendar/calendar_view','CalendarView',TRUE);
        $this->load->model('timesheets/timesheet_update','TimesheetUpdate',TRUE);
        $this->load->model('projects/project_view','ProjectView',TRUE);
        $this->load->model('contacts/contacts','',TRUE);

        $this->load->library('application/CommonTask');
        $this->load->library('application/CommonAppData');
        $this->load->library('application/CommonEmail');
	}

    function saveTask() {
        if (isset($_POST['save']) && $_POST['save'] == 1) {			
            /*
            * Save data
            */
            $taskArray['taskID']        = $_POST['taskID'];
            $taskArray['milestoneID']   = $_POST['milestoneID'];
            $taskArray['projectID']     = $_POST['projectID'];
            $taskArray['action']        = $_POST['action'];
            $taskArray['taskTitle']     = fieldToDB($this->input->post('taskTitle', TRUE));
            $taskArray['estimatedTime'] = fieldToDB($this->input->post('taskEstimatedTime', TRUE));
            $taskArray['actualTime'] = 0;

            if (!is_numeric($taskArray['estimatedTime'])) {
                $taskArray['estimatedTime'] = '';
            }
            if (!is_numeric($taskArray['actualTime'])) {
                $taskArray['actualTime'] = '';
            }           

            $taskDateStart = jsDateToMySQL(trim($_POST['taskDateStart']));
            $taskDateEnd   = jsDateToMySQL(trim($_POST['taskDateEnd']));

            if ($taskDateStart == '0000-00-00' || $taskDateStart == '0000-00-00 00:00:00') {
                $taskDateStart = '';
                $taskArray['taskDateStart'] = '0000-00-00 00:00:00';
            } else {
                $taskArray['taskDateStart'] = convertMySQLToGMT($taskDateStart,'push','Y-m-d H:i:s');
            }
            if ($taskDateEnd == '0000-00-00' || $taskDateEnd == '0000-00-00 00:00:00') {
                $taskDateEnd = '';
                $taskArray['taskDateEnd'] = '0000-00-00 00:00:00';
            } else {
                $taskArray['taskDateEnd']   = convertMySQLToGMT($taskDateEnd,'push','Y-m-d H:i:s');
            }
            
            $taskArray['priority']      = $_POST['priority'];
            $taskArray['taskTags']      = fieldToDB($this->input->post('taskTags', TRUE));
            $taskArray['description']   = fieldToDB($this->input->post('description', TRUE),TRUE,FALSE,FALSE,FALSE,TRUE,FALSE);
            $calendarArray = $this->CalendarView->getCalendarDetailsFromType('task',$this->session->userdata('accountUserid'));
            $taskArray['calendarID']    = $calendarArray['CalendarID'];
            $teamMembers                = $_POST['teamMembers'];

           /*
            * Determine status
            */
            $taskArray['status'] = 1;
            if ( !empty($taskDateEnd) && strtotime($taskDateEnd.' 23:59:59') < time()) {
                /*
                    * Task is overdue
                    */
                $taskArray['status'] = 4;
            }
            
            $priorityArray = getTaskPriorityIcon($taskArray['priority']);

            if ($taskArray['action'] == 'add') {
                /*
                    * Get new task number
                    */
                $taskNumber = $this->TaskUpdate->getNewTaskNumber($taskArray['projectID']);
                $taskArray['taskNumber'] = $taskNumber;
            } else {
                $taskArray['taskNumber'] = $_POST['taskNo'];
            }

            $taskArray['accountUserid'] = $this->session->userdata('accountUserid');
            $taskArray['userid']        = $this->session->userdata('userid');

            /*
             * Save the task
             */
            $idArray = $this->TaskUpdate->saveTask($taskArray);
            $taskID = $idArray[0];

            /*
            * See what's happening with out actual time. If
            * actualTime > 0 then we need to log it to the
            * timesheet on the day it was entered.
            */
            if ($taskArray['actualTime']>0) {
                $data['timeSheetID'] = 0;
                $data['projectID']   = $taskArray['projectID'];
                $data['taskID']      = $taskID;
                $data['elapsedTime'] = $taskArray['actualTime'];
                $data['date']        = date('Y-m-d H:i:s');
                $this->TimesheetUpdate->saveTimesheetRecordFromTimesheet($data);
            }

            /*
            * Get some project information
            */
            $projectArray = $this->ProjectView->getProjectInformation($taskArray['projectID']);
            $projectTitle = $projectArray['Title'];

            /*
             * Save team member assignments
             */
            $teamMembersArray = makeUserArrayFromString($teamMembers);
            $this->TaskUpdate->clearUsersFromTask($taskID);
            if (sizeof($teamMembersArray)>0 && $teamMembersArray != FALSE) {                
                foreach($teamMembersArray as $teamMember) {
                    $teamMemberPID      = $teamMember['PID'];
                    $teamMemberLanguage = $teamMember['Language'];
                    
					if ($teamMemberPID>0) {
						$this->TaskUpdate->assignUserToTask($teamMemberPID,$taskID);
                        $userArray = $this->Contacts->getContactPerson($teamMemberPID);
                        $teamMemberUID = $userArray['UID'];

                        $messageType = 'taskNew';
                        $messageTypeEmail = 'emailReceiveTask';
                        if ($taskArray['action'] == 'edit') {
                            $messageType = 'taskUpdate';
                            $messageTypeEmail = 'emailReceiveTaskUpdate';
                        }
                        $taskNumberTitle = $projectTitle.': #'.$taskArray['taskNumber'].' '.$taskArray['taskTitle'];
                        $messageArray = $this->commonemail->createSystemMessage($messageType,$teamMemberLanguage);
                        $subject = DBToField(sprintf($messageArray['subject'],$taskNumberTitle));

                        $messageIntro = sprintf($messageArray['message'],$this->session->userdata('fullName'),$taskNumberTitle);
                        $footer = $this->commonemail->createSystemMessage('messageFooter',$teamMemberLanguage);

                        $message = strtoupper($taskNumberTitle).Chr(10);
                        if (!empty($taskArray['description'])) {
                            $message .= Chr(10).$taskArray['description'].Chr(10);
                        }

                        $message .= Chr(10).lang('project_project').': '.$projectTitle;
                        $message .= Chr(10).lang('common_date_due').': '.$_POST['taskDateEnd'];
                        $estimatedHours = '';
                        if ($taskArray['estimatedTime']>0) {
                            $estimatedHours = $taskArray['estimatedTime'].' '.lang('common_hours');
                        }

                        $message .= Chr(10).lang('task_estimated_time').': '.$estimatedHours;
                        $message .= Chr(10).lang('common_priority').': '.$priorityArray[0];
                        $message .= Chr(10).Chr(10).lang('email_task_link').Chr(10);
                        $message .= BASE_URL.'tasks/TaskViewExternal/viewTask/'.$this->session->userdata('persAuthString').'/'.$taskID;

                        $finalMessage = DBToField($messageIntro.Chr(10).Chr(10).$message.Chr(10).Chr(10).$footer['message'],TRUE);
                        //$this->commonemail->sendEmail($finalMessage,$teamMemberUID,'team',$subject,$messageTypeEmail);
                        $this->commonemail->sendEmail($finalMessage,$teamMemberPID,'team',$subject,$messageTypeEmail);
					}                    
                }
            } else {
                /*
                 * Enter creator as person task is assigned to
                 */
                $this->TaskUpdate->assignUserToTask($this->session->userdata('pid'),$taskID);
            }

            /*
             * Save taskID's to file attachments if we're adding
             */
            if ($_POST['action'] == 'add') {
                $fileLinkIDArray = explode('|',$_POST['fileLinkIDString']);
                foreach($fileLinkIDArray as $fileLinkID) {
                    if ($fileLinkID>0) {
                        $this->FileMaintenance->updateFileLinkWithItemID($fileLinkID,$taskID);
                    }
                }
            }

            $taskJSON = $this->commontask->getTask($taskID,'json',1);
            $taskJSON = substr($taskJSON,1);
            $taskJSON = substr($taskJSON,0,-1);

            /*
             * Log activity
             */
            $activity = 'create';
            if ($taskArray['action'] != 'add') {
                $activity = 'update';
            }
            $data = array(
                'AccountUserid' => $this->session->userdata('accountUserid'),
                'UserPID' => $this->session->userdata('pid'),
                'ProjectID' => $taskArray['projectID'],
                'UserType' => $this->session->userdata('userType'),
                'Activity' => $activity,
                'ItemID' => $taskID,
                'ItemType' => 'task',
                'ItemTitle' => $taskArray['taskTitle'],
                'ReportToUser' => 1
            );
            $this->App_activity->logAppActivity($data);
            
            /*
             * Save new tags
             */
            $this->commonappdata->saveTags($taskArray['taskTags'],'task');

            echo $taskJSON;
        }
    }

    function saveTaskFromWidget() {
            $taskArray['action']      = $_POST['action'];
            $taskArray['taskDateEnd'] = jsDateToMySQL(trim($_POST['taskDateDue']));
            $taskDateEnd = $taskArray['taskDateEnd'];

			if (!empty($_POST['taskTimeDue'])) {
				$timeDue = convertTimeToMySQL($_POST['taskTimeDue']);
				$taskArray['taskDateEnd'] .= ' '.$timeDue;
				$taskDateTimeEnd = $taskArray['taskDateEnd'];
			} else {
				$taskDateTimeEnd = $taskArray['taskDateEnd'].' 23:59:59';
			}

			$taskArray['status'] = 1;
            if (!empty($taskArray['taskDateEnd'])) {
                $taskArray['taskDateEnd'] = convertMySQLToGMT($taskArray['taskDateEnd'],'push','Y-m-d H:i:s');
				if (strtotime($taskArray['taskDateEnd']) < time()) {
					$taskArray['status'] = 4;
				}

                if ($taskArray['taskDateEnd'] != '0000-00-00' && $taskArray['taskDateEnd'] != '0000-00-00 00:00:00') {
                    $taskDateArray = explode(':',$taskArray['taskDateEnd']);
                    if (sizeof($taskDateArray) == 3 && $taskDateArray[2] == '59') {
                        $dateTemplate = 'M j';
                    } else {
                        $dateTemplate = 'M j, g:i a';
                    }
                    $taskDateRender = convertMySQLToGMT($taskArray['taskDateEnd'],'pull',$dateTemplate);
                 } else {
                    $taskDateRender = '';
                 }

            } else {
                $taskArray['taskDateEnd'] = '';
            }
            
            $taskArray['taskID']        = $_POST['taskID'];
            $taskArray['projectID']     = $_POST['taskProjectID'];
            $taskArray['milestoneID']   = $_POST['taskMilestoneID'];
            $taskArray['projectTitle']  = $_POST['taskProject'];
			$taskArray['taskTitle']     = fieldToDB($this->input->post('taskTitle', TRUE));

            $taskArray['accountUserid'] = $this->session->userdata('accountUserid');
            $taskArray['userid']        = $this->session->userdata('userid');

            $calendarArray = $this->CalendarView->getCalendarDetailsFromType('task',$this->session->userdata('accountUserid'));
            $taskArray['calendarID']    = $calendarArray['CalendarID'];

            if ($taskArray['action'] == 'add') {
                /*
                 * Get new task number
                 */
                $taskNumber = $this->TaskUpdate->getNewTaskNumber($taskArray['projectID']);
                $taskArray['taskNumber'] = $taskNumber;
            } else {
                $taskArray['taskNumber'] = '';
            }

            $taskArray['taskDateStart'] = '';
            $taskArray['description']   = '';
            $taskArray['estimatedTime'] = '';
            $taskArray['priority']      = '';
            $taskArray['taskTags']      = '';

            if (!empty($taskArray['taskTitle'])) {
                $idArray = $this->TaskUpdate->saveTask($taskArray);
                /*
                 * Find out if date is today, tomorrow, this week or next week
                 */
                $statusArray = getProjectStatus($taskArray['status'],NULL,$taskArray['taskDateEnd']);
                $returnArray = array(
                    'TaskID'        => $idArray[0],
                    'Title'         => $taskArray['taskTitle'],
                    'Description'   => '',
                    'Date_Start'    => $taskArray['taskDateStart'],
                    'Date_End'      => $taskArray['taskDateEnd'],
                    'TaskNo'        => $taskArray['taskNumber'],
                    'Status'        => $taskArray['status'],
                    'ProjectID'     => $taskArray['projectID'],
                    'ProjectTitle'  => $taskArray['projectTitle'],
                    'StatusHuman'   => $statusArray[0],
                    'StatusMachine' => $statusArray[1],
                    'StatusText'    => $statusArray[2],
                    'TaskLink'      => site_url('tasks/TaskViewExternal/viewTask/'.$this->session->userdata('persAuthString').'/'.$idArray[0]),
                    'TaskDateText'  => dateTextFromDate($taskArray['taskDateEnd'])
                );

                $returnArray['ParentTaskID'] = $taskArray['milestoneID'];
                $returnArray['DateEndRender'] = $taskDateRender;

                echo json_encode($returnArray);
            } else {
                echo '0';
            }
    }    

	function saveMilestone() {
		if (isset($_POST['save']) && $_POST['save'] == 1) {
            /*
             * Save data
             */
            $msArray['milestoneID']        = $_POST['milestoneID'];
            $msArray['projectID']          = $_POST['projectID'];
            $msArray['action']             = $_POST['action'];
            $msArray['milestoneTitle']     = fieldToDB($this->input->post('milestoneTitle', TRUE));
            $milestoneDateStart            = jsDateToMySQL(trim($_POST['milestoneDateStart']));
            $milestoneDateEnd              = jsDateToMySQL(trim($_POST['milestoneDateEnd']));
            $msArray['milestoneDateStart'] = convertMySQLToGMT($milestoneDateStart,'push','Y-m-d H:i:s');
            $msArray['milestoneDateEnd']   = convertMySQLToGMT($milestoneDateEnd,'push','Y-m-d H:i:s');
            if (!empty($msArray['milestoneTitle'])) {
				$msID = $this->TaskUpdate->saveMilestone($msArray);
				$msJSON = $this->commontask->getMilestone($msID,'json',1);
				$msJSON = substr($msJSON,1);
				$msJSON = substr($msJSON,0,-1);
			} else {
				$msJSON = '0';
			}

            echo '{'.$msJSON.'}';
        }
	}

    function updateTaskStatus($taskID,$newStatus=NULL) {
        /*
         * Handles the click event on a status icon
         */
        $currentStatus = $this->TaskView->getTaskStatus($taskID);
        if (empty($newStatus)) {
            if ($currentStatus == 4) {
                $newStatus = 0;
            } else {
                $newStatus = $currentStatus+1;
            }
        }
		
        $this->TaskUpdate->updateTaskStatus($taskID,$newStatus);
        $statusArray = getProjectStatus($newStatus);
        
        $taskArray = $this->TaskView->getTask($taskID);
        $data = array(
            'AccountUserid' => $this->session->userdata('accountUserid'),
            'UserPID' => $this->session->userdata('pid'),
            'ProjectID' => $taskArray['ProjectID'],
            'UserType' => $this->session->userdata('userType'),
            'Activity' => 'update status',
            'ItemID' => $taskID,
            'ItemType' => 'task',
            'ItemTitle' => $taskArray[0]['Title'].' ('.$statusArray[0].')',
            'ReportToUser' => 1
        );
        $this->App_activity->logAppActivity($data);
        
        
        // Human, Machine, Text
        echo $statusArray[0].'|'.$statusArray[1].'|'.$statusArray[2];
    }	

    function updateTaskDatesAfterMove($taskID,$newDateStart,$newDateEnd) {
        if (empty($taskID)) {
            if (isset($_POST['eventID'])) {
                $taskID = $this->input->post('eventID', TRUE);
            } else {
                $taskID = $this->input->post('taskID', TRUE);
            }
            $newDateStart = $this->input->post('newStartDate', TRUE);
            $newDateEnd   = $this->input->post('newEndDate', TRUE);
        }
        $this->TaskUpdate->updateTaskDatesAfterMove($taskID,$newDateStart,$newDateEnd);
    }

    function moveTasksToMilestone($milestoneFrom,$milestoneTo) {
        if ($milestoneFrom > 0) {
            $this->TaskUpdate->moveTasksToMilestone($milestoneFrom,$milestoneTo);
        }
    }

    function reorderTasks() {
        $orderJSON = $_POST['orderJSON'];
        $orderJSONArray = json_decode($orderJSON);
        
        $currentMilestoneID = '';
        $milestoneOrder = 1;
        foreach($orderJSONArray as $item) {
            /*
             * Are we a milestone or a task?
             */
            $itemArray = explode('_',$item);
            $itemType = $itemArray[0];
            $itemID   = $itemArray[1];
            if ($itemType == 'milestone' && $itemID != $currentMilestoneID) {
                $currentMilestoneID = $itemID;
                $taskOrder = 1;
                /*
                 * Save milestone order.
                 */
                $this->TaskUpdate->updateTaskOrder($currentMilestoneID,$milestoneOrder);
                //echo 'Saving milestone '.$currentMilestoneID.' with order of '.$milestoneOrder.Chr(10);
                $milestoneOrder++;
            } else {
                /*
                 * Now save task order.
                 */
                //echo '-- Saving task '.$itemID.' with order of '.$taskOrder.Chr(10);
                $this->TaskUpdate->updateTaskOrder($itemID,$taskOrder,$currentMilestoneID);
                $taskOrder++;
            }
        }
    }
    
    function deleteMilestone($milestoneID,$withTasks=0,$tagForDelete=TRUE) {
        if ($tagForDelete == true) {
            $msArray = $this->TaskView->getTask($milestoneID);
            $data = array(
                'AccountUserid' => $this->session->userdata('accountUserid'),
                'UserPID' => $this->session->userdata('pid'),
                'ProjectID' => $msArray['ProjectID'],
                'UserType' => $this->session->userdata('userType'),
                'Activity' => 'delete',
                'ItemID' => $milestoneID,
                'ItemType' => 'task',
                'ItemTitle' => $msArray[0]['Title'],
                'ReportToUser' => 1
            );
            $this->App_activity->logAppActivity($data);
        }
        /*
         * If withTasks = 1 then we delete all tasks under this
         * milestone as well.
         */
        if ($milestoneID > 0) {
            $this->TaskUpdate->deleteMilestone($milestoneID,$withTasks,$tagForDelete,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
            $this->commontrash->countItemsInTrash();
        }
	}

    function deleteTask($taskID,$tagForDelete=TRUE) {
        if ($tagForDelete == true) {
            $taskArray = $this->TaskView->getTask($taskID);
            $data = array(
                'AccountUserid' => $this->session->userdata('accountUserid'),
                'UserPID' => $this->session->userdata('pid'),
                'ProjectID' => $taskArray[0]['ProjectID'],
                'UserType' => $this->session->userdata('userType'),
                'Activity' => 'delete',
                'ItemID' => $taskID,
                'ItemType' => 'task',
                'ItemTitle' => $taskArray[0]['Title'],
                'ReportToUser' => 1
            );
            $this->App_activity->logAppActivity($data);
        }
        $this->TaskUpdate->deleteTask($taskID,'task',$tagForDelete,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
        $this->FileMaintenance->removeAttachedFileFromItem(NULL,NULL,$taskID,'task');
        $this->commontrash->countItemsInTrash();
    }

    function restoreMilestone($milestoneID) {
        $this->TaskUpdate->restoreMilestone($milestoneID,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
        $this->commontrash->countItemsInTrash();
    }

    function restoreTask($taskID) {
        $this->TaskUpdate->restoreTask($taskID,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));
        $this->commontrash->countItemsInTrash();
    }
}
?>
