<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TaskViewExternal extends Controller {
	function __construct()
	{
		parent::Controller();

		$this->load->model('common/app_data','AppData',TRUE);
		$this->load->model('accounts/Accounts','',TRUE);
		$this->load->model('projects/Project_view','',TRUE);
        $this->load->model('contacts/Contacts','',TRUE);
		$this->load->model('tasks/Task_view','',TRUE);
        $this->load->model('tasks/Task_update','',TRUE);

        $this->load->library('application/CommonTask');
        $this->load->library('application/CommonMessages');
        $this->load->library('application/CommonEmail');
	}

    function viewTask($authKey,$taskID) {
		$loginArray = checkAuthKeyFromGetRequest($_SERVER['PHP_SELF'],'item');
		$userid        = $loginArray['UID'];
        $pid           = $this->Contacts->getPIDFromLoginUserid($userid);
        $accountUserid = $loginArray['UseridAccount'];
        $permissions   = $loginArray['RolePermissions'];
		$langCode      = $loginArray['Language'];
		loadLanguageFiles($langCode);

		/*
		 * Do we have rights to view this task?
		 */
		$permArray = createPermissionsArray($permissions);
		$viewAll = TRUE;
		if ($permArray['taskViewOwn'] == 1) {
			$viewAll = FALSE;
		}
		$taskRights = $this->Task_view->checkForValidTask($taskID,$pid,$accountUserid,$viewAll);

		$data['errorMessage']    = '';
		$data['page']            = 'external';
		$data['pageTitle']       = lang('widget_tasks');
		$data['pageLayoutClass'] = 'withRightColumn';
		$data['pageIconClass']   = 'iconPageTasks';
		$data['jsFileArray']     = array('jsTaskViewExternal.js');
        $data['child']           = TRUE;

        $data['pageButtons']           = array(
                                      /*
                                       * Button text, button title, visual style, extra className, itemID, buttonID, initialState
                                       */
									   'print'       => array(lang('button_print'),lang('button_print'),'green','buttonPrint','','print','')
									);
        if ($this->session->userdata('logged') != TRUE) {
            $loginButton = array(lang('login_button'),lang('login_button'),'yellow','buttonLogin','','login','');
            $data['pageButtons']['login'] = $loginButton;
        }
		
		if ($taskRights == TRUE) {
			/*
			 * Get task information
			 */
			$taskArray   = $this->Task_view->getTask($taskID);
			$taskArray   = $this->commontask->renderTasks($taskArray,'array');

			if (isset($taskArray[0]) && is_array($taskArray[0])) {
				$projectID   = $taskArray[0]['ProjectID'];
				$milestoneID = $taskArray[0]['MilestoneID'];

				/*
				 * Get some project info
				 */
				$projectArray = $this->Project_view->getProjectInformation($projectID);
				/*
				 * Get some milestone info
				 */
				$milestoneArray = $this->Task_view->getMilestone($milestoneID);
				/*
				 * Get any messages for this task
				 */
				$messageArray = $this->commonmessages->getMessages($taskID,'task',NULL,NULL,'array',1,$userid);

				$data['taskInfo'] = $taskArray[0];
				$data['taskInfo']['projectInfo']   = $projectArray;
				$data['taskInfo']['milestoneInfo'] = $milestoneArray;
				$data['taskInfo']['messages']      = $messageArray;
			} else {
				$data['errorMessage'] = lang('error_task_deleted');
			}
		} else {
			$data['errorMessage'] = lang('error_cant_view_this');
		}
		$this->load->view('tasks/TaskViewExternal',$data);
	}

    function sendTaskReminderEmails() {
        // Get overdue tasks
        /*
         * Date of 1 week ago and 1 week from now
         */
        $dateOneWeekAgo = new_date_from_days(date('Y-m-d'),-7);
        $dateOneWeekFromNow = new_date_from_days(date('Y-m-d'),7);

        $taskOverdueUseridArray = $this->Task_view->getOverdueTaskUserids($dateOneWeekAgo);
        $taskID = 0;
        $memberID = 0;
        foreach ($taskOverdueUseridArray as $overdueUserid) {
            $overdueTaskArray = $this->Task_view->getOverdueTasksByMemberID($overdueUserid['MemberID']);
            $daysFromSignup = days_between_dates(strtotime($overdueUserid['DateSignup']),time());

            /*
             * Only send overdue task emails if we have a user who's account
             * has not expired.
             */
            if ( ($daysFromSignup <= 31 || $overdueUserid['IsPaid'] == 1) && $overdueUserid['IsActive'] == 1) {
                $taskString = '';
                foreach($overdueTaskArray as $task) {
                    $overdueTaskID = $task['TaskID'];
                    if ($overdueTaskID != $taskID) {
                        $taskID = $overdueTaskID;
                        $taskString .= '[OVERDUE] #'.$task['TaskNo'].' '.$task['Title'].' due on '.convertMySQLToGMT($task['Date_End'],'pull','M j').' ('.$task['Project'].')'.Chr(10);
                        $taskString .= BASE_URL.'tasks/TaskViewExternal/viewTask/'.$overdueUserid['AuthString'].'/'.$overdueTaskID.Chr(10).Chr(10);

                        /*
                         * Update DateOverdueNotify
                         */
                        $this->Task_update->setDateOverdueNotify($overdueTaskID,date('Y-m-d'));
                    }
                }

                if ($memberID != $overdueUserid['MemberID'] && !empty($taskString)) {
                    $memberID = $overdueUserid['MemberID'];
                    /*
                     * Send email to this user.
                     */
                    $teamMemberUID = $overdueUserid['UID'];
                    $language = $overdueUserid['Language'];
                    if (empty($language)) {
                        $language = 'en';
                    }

                    $messageArray = $this->commonemail->createSystemMessage('taskOverdue',$language);
                    $footer       = $this->commonemail->createSystemMessage('messageFooter',$language);

                    $subject      = $messageArray['subject'];
                    $messageIntro = $messageArray['message'];
                    $finalMessage = DBToField($messageIntro.Chr(10).Chr(10).$taskString.Chr(10).Chr(10).$footer['message'],TRUE);

                    $this->commonemail->sendEmail($finalMessage,$memberID,'team',$subject,NULL,TRUE);
                }
            }
        }

        /*
         * Now get upcoming tasks.
         */
        $taskUpcomingUseridArray = $this->Task_view->getUpcomingTaskUserids($dateOneWeekFromNow);
        $taskID = 0;
        $userid = 0;
        foreach ($taskUpcomingUseridArray as $upcomingUserid) {
            $upcomingTaskArray = $this->Task_view->getUpcomingTasksByUserid($upcomingUserid['MemberID'],$dateOneWeekFromNow);
            $daysFromSignup = days_between_dates(strtotime($upcomingUserid['DateSignup']),time());

            /*
             * Only send overdue task emails if we have a user who's account
             * has not expired.
             */
            if ( ($daysFromSignup <= 31 || $upcomingUserid['IsPaid'] == 1) && $upcomingUserid['IsActive'] == 1) {
                $taskString = '';
                foreach($upcomingTaskArray as $task) {
                    $upcomingTaskID = $task['TaskID'];
                    if ($upcomingTaskID != $taskID) {
                        $taskID = $upcomingTaskID;
                        $taskString .= '[UPCOMING] #'.$task['TaskNo'].' '.$task['Title'].' due on '.convertMySQLToGMT($task['Date_End'],'pull','M j').' ('.$task['Project'].')'.Chr(10);
                        $taskString .= BASE_URL.'tasks/TaskViewExternal/viewTask/'.$upcomingUserid['AuthString'].'/'.$upcomingTaskID.Chr(10).Chr(10);

                        /*
                         * Update DateOverdueNotify
                         */
                        //$this->Task_update->setDateOverdueNotify($overdueTaskID,date('Y-m-d'));
                    }
                }

                if ($userid != $upcomingUserid['MemberID'] && !empty($taskString)) {
                    $userid = $upcomingUserid['MemberID'];
                    /*
                     * Send email to this user.
                     */
                    $teamMemberUID = $userid;
                    $language = $upcomingUserid['Language'];
                    if (empty($language)) {
                        $language = 'en';
                    }

                    $messageArray = $this->commonemail->createSystemMessage('taskUpcoming',$language);
                    $footer       = $this->commonemail->createSystemMessage('messageFooter',$language);

                    $subject      = $messageArray['subject'];
                    $messageIntro = $messageArray['message'];
                    $finalMessage = DBToField($messageIntro.Chr(10).Chr(10).$taskString.Chr(10).Chr(10).$footer['message'],TRUE);

                    $this->commonemail->sendEmail($finalMessage,$userid,'team',$subject,NULL,TRUE);
                }
            }
        }
    }
}
?>