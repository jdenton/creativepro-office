<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ClientUpdate extends Controller {
	
    function __construct() {
        parent::Controller();
        checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        
        $this->load->model('common/app_data','AppData',true);
        $this->load->model('clients/client_update','ClientUpdate',true);
        $this->load->model('clients/client_view','ClientView',true);
        $this->load->model('settings/settings_update','SettingsUpdate',TRUE);

        $this->load->library('application/CommonAppData');
        $this->load->library('application/CommonEmail');
    }
	
	function _init($clientID=NULL) {
		global $subMenuArray;
		$data['page']            = 'clients';
		$data['pageTitle']       = lang('client_page_title');
		$data['pageSubTitle']    = lang('client_add_page_title');
        $data['pageTitleLink']   = site_url('clients/ClientView');
		$data['wysiwyg']         = 0;
		$data['map']             = 0;
		$data['validate']        = 1;
		$data['jsFileArray']     = array('jsClients.js');
		$data['subMenuPage']     = $subMenuArray['clients'];
		$data['pageIconClass']   = 'iconPageClients';
		$data['pageLayoutClass'] = 'withRightColumn';
		
		$messageArray = array(
                    'clientUserid'  => lang('form_required_clientUserid'),
                    'clientCompany' => lang('form_required_clientCompany'),
                    'clientEmail'   => array(
                            'required' => lang('form_required_email'),
                            'minlength'=> lang('form_minlength_clientEmail')
                    )
		);
	
		/*
         * Get category list
         */
        $data['categories'] = $this->AppData->getCategories($this->session->userdata('accountUserid'),'client');
		$data['catJSON']    = makeCategoryJSON($data['categories']);

        $settingsEmailClientNew = $this->SettingsUpdate->getSettings($this->session->userdata('accountUserid'),'emailSendClientNew');
        $data['settingEmailClientNew'] = $settingsEmailClientNew['SettingValue'];
							
        if ($clientID>0) {
            /*
             * We're editing, retrieve and format client data
             */
            $validClient = $this->ClientView->checkForValidClient($clientID,$this->session->userdata('accountUserid'));
            if ($validClient == false) {
                header('Location: '.site_url('clients/ClientView'));
            } else {
                $data['action'] = 'edit';
                $data['clientInformation'] = ClientUpdate::getClientDetails($clientID);
            }
            $data['pageSubTitle'] = lang('client_edit_client');

        } else {
            $data['clientInformation'] = null;
            $data['action'] = 'add';
            $data['pageSubTitle'] = lang('client_add_page_title');
        }

		/*
		 * Create tag string for clients
		 */				
		$tagArray = $this->AppData->getTags($this->session->userdata('accountUserid'),'client');
		$data['tags']           = makeTagJSON($tagArray);		
		$data['validationJSON'] = json_encode($messageArray);				

		noCache();
		return $data;
	}
	
	function index($clientID=null) {
		$data = ClientUpdate::_init($clientID);
		$this->load->view('clients/ClientUpdate',$data);
	}
	
	function getClientDetails($clientID) {
		$clientArray = $this->ClientView->getClientInformation($clientID);

        /*
         * TODO: Placeholder until we get the rate thing sorted out
         */
        $clientArray['RateAmount']      = '';
        $clientArray['RatePer']         = 'hour';
        $clientArray['RateDescription'] = '';

        /*
         * Do some data formatting
         */
        if ($clientArray['URL'] == '0') {
            $clientArray['URL'] = '';
        }

        $clientArray['Address']      = clearLinebreaks($clientArray['Address']);
        $clientArray['Comments']     = clearLinebreaks($clientArray['Comments']);
        $clientArray['Client_Since'] = MySQLDateToJS($clientArray['Client_Since']);

		return $clientArray;			
	}
	
	function saveClient() {
		if (isset($_POST['save']) && $_POST['save'] == 1) {
			if ($this->form_validation->run() == FALSE)	{
				$data = ClientUpdate::_init();
				$this->load->view('clients/ClientUpdate',$data);
			} else {
				/*
				 * Save data
				 * fieldToDB($string,$escape=TRUE,$linebreak=FALSE,$prepURL=FALSE,$autoLink=FALSE,$autoLinkNewWindow=TRUE)
				 */
                $clientArray['clientID']      = $_POST['clientID'];
                $clientArray['action']        = $_POST['action'];
                $clientArray['userid']        = $this->session->userdata('accountUserid');
				$clientArray['clientCompany'] = fieldToDB($this->input->post('clientCompany', TRUE));
				$clientArray['clientPhone1']  = fieldToDB($this->input->post('clientPhone1', TRUE));
				$clientArray['clientPhone2']  = fieldToDB($this->input->post('clientPhone2', TRUE));
				$clientArray['clientPhone3']  = fieldToDB($this->input->post('clientPhone3', TRUE));
				$clientArray['clientEmail']   = fieldToDB($this->input->post('clientEmail', TRUE));
				$clientArray['clientURL']     = fieldToDB($this->input->post('clientURL', TRUE), TRUE,FALSE,TRUE);
				
				$clientArray['clientAddress'] = fieldToDB($this->input->post('clientAddress', TRUE), TRUE,TRUE);                
                $clientArray['clientCity']    = fieldToDB($this->input->post('clientCity', TRUE));
                $clientArray['clientState']   = fieldToDB($this->input->post('clientState'));
                $clientArray['clientZip']     = fieldToDB($this->input->post('clientZip', TRUE));
                $clientArray['clientCountry'] = $_POST['clientCountry'];
                $clientArray['clientTimezone']= $_POST['clientTimezone'];
				$clientArray['clientLanguage']= $_POST['clientLanguage'];
                $clientArray['clientIndustry']= $_POST['clientIndustry'];
                
                /*
                 * Project category
                 */
                $clientArray['clientCategory'] = $this->commonappdata->saveCategory(fieldToDB($this->input->post('clientCategoryCombo', TRUE)),'client');
                
                $clientArray['clientArchive'] = 0;
                $clientArray['clientStatus']  = 0;

                $clientArray['clientNotes']   = fieldToDB($this->input->post('clientNotes', TRUE), TRUE,TRUE,FALSE,TRUE);                
                $clientArray['clientSince']   = fieldToDB(jsDateToMySQL($_POST['clientSince']));
                
                $clientArray['clientTags'] = fieldToDB($this->input->post('clientTagsHolder', TRUE));
                $clientArray['clientTags'] = str_replace(',',' ',$clientArray['clientTags']);                
                
                $clientArray['clientSendEmail'] = $_POST['clientSendEmail'];


				$newClientID = $this->ClientUpdate->saveClient($clientArray);
                $updateClientUserid = false;
                if ($clientArray['action'] == 'add') {
                    /*
                     * Save client login information
                     */
                    include(SITE_CODE_PATH);
                    $loginArray['action']        = 'add';
                    $loginArray['Userid']        = $clientArray['clientEmail'];
                    $password = random_string();
                    $loginArray['Password']      = sha1($password.$siteCode);
                    $loginArray['RoleID']        = '';
                    $loginArray['UserType']      = USER_TYPE_CLIENT;
                    $loginArray['UseridAccount'] = $this->session->userdata('accountUserid');

                    $loginUID = $this->User_login->saveLoginInformation($loginArray);
                    
                    /*
                     * Create authString and update
                     */
                    $authString              = sha1($loginUID.$siteCode);
                    $authArray['UID']        = $loginUID;
                    $authArray['action']     = 'updateAuthString';
                    $authArray['authString'] = $authString;
                    $this->User_login->saveLoginInformation($authArray);

                    /*
                     * Update client table with ClientUserid
                     */
                    $clientUseridArray['action']       = 'update';
                    $clientUseridArray['clientUserid'] = $loginUID;
                    $clientUseridArray['clientID']     = $newClientID;
                    $newClientID = $this->ClientUpdate->saveClient($clientUseridArray);
                } else {
                    if ($_POST['origClientEmail'] != $clientArray['clientEmail']) {
                        /*
                         * Then we've changed the email address. Change the login email address too.
                         */
                        $this->ClientUpdate->updateClientLoginEmail($clientArray['clientEmail'],$_POST['clientUserid']);
                        $updateClientUserid = true;
                        $loginUID = $_POST['clientUserid'];
                    }
                }

                /*
                 * Get some email settings
                 */
                if ($clientArray['clientSendEmail'] == '1') {
                    if ($updateClientUserid == true) {
                        /*
                         * Send userid update email to client
                         */
                        $messageArray = $this->commonemail->createSystemMessage('clientUpdateUserid',$clientArray['clientLanguage']);
                        $message = $messageArray['message'];
                        $subject = $messageArray['subject'];
                        /*
                         * Company, Company, User Name, Password
                         */
                        $messageIntro = sprintf($message,$clientArray['clientEmail']);
                        $footer = $this->commonemail->createSystemMessage('messageFooter',$clientArray['clientLanguage']);
                    } else {
                        /*
                         * Send welcome email to client
                         */
                        $messageArray = $this->commonemail->createSystemMessage('clientNew',$clientArray['clientLanguage']);
                        $message = $messageArray['message'];
                        $subject = $messageArray['subject'];
                        /*
                         * Company, Company, User Name, Password
                         */
                        $messageIntro = sprintf($message,$this->session->userdata('userCompany'),$clientArray['clientCompany'],$this->session->userdata('userCompany'),$clientArray['clientEmail'],$password);
                        $footer = $this->commonemail->createSystemMessage('messageFooter',$clientArray['clientLanguage']);
                    }

                    $finalMessage = DBToField($messageIntro.Chr(10).Chr(10).$footer['message'],TRUE);
                    $this->commonemail->sendEmail($finalMessage,$loginUID,'client',$subject,'');
                }
                /*
                 * Save tags
                 */
                if (!empty($clientArray['clientTags'])) {
                    $this->commonappdata->saveTags($clientArray['clientTags'],'client');
                }
                
                /*
                 * Log activity
                 */
                $activity = 'create';
                if ($projectArray['action'] != 'add') {
                    $activity = 'update';
                }
                $data = array(
                    'AccountUserid' => $this->session->userdata('accountUserid'),
                    'UserPID' => $this->session->userdata('pid'),
                    'ProjectID' => 0,
                    'UserType' => $this->session->userdata('userType'),
                    'Activity' => $activity,
                    'ItemID' => $newClientID,
                    'ItemType' => 'client',
                    'ItemTitle' => $clientArray['clientCompany'],
                    'ReportToUser' => 1
                );
                $this->App_activity->logAppActivity($data);
				header('Location: '.BASE_URL.'clients/ClientDetail/index/'.$newClientID);
			}
		} else {	
			header('Location: '.BASE_URL.'clients/ClientView');
		}	
	}

    function deleteClient($clientID,$tagForDelete=TRUE) {
        $clientArray = ClientUpdate::getClientDetails($clientID);
        $title = $clientArray['Company'];
        $data = array(
            'AccountUserid' => $this->session->userdata('accountUserid'),
            'UserPID' => $this->session->userdata('pid'),
            'ProjectID' => 0,
            'UserType' => $this->session->userdata('userType'),
            'Activity' => 'delete',
            'ItemID' => $clientID,
            'ItemType' => 'client',
            'ItemTitle' => $title,
            'ReportToUser' => 1
        );
        $this->App_activity->logAppActivity($data);
        
        $this->ClientUpdate->deleteClient($clientID,$tagForDelete,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }

    function restoreClient($clientID) {
        $this->ClientUpdate->restoreExpense($clientID,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }
}	
?>