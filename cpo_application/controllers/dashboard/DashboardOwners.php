<?
class DashboardOwners extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
		$this->load->library('application/CommonWidgets');

        $this->load->model('projects/Project_update','',true);
        $this->load->model('tasks/Task_view','',true);
        $this->load->model('finances/Finance_view','',true);
        $this->load->model('timesheet/Timesheet_view','',true);
	}
	
	function _init() {
		$data['page']            = 'dashboard';
		$data['siteName']        = 'CreativePro Office';
		$data['pageTitle']       = lang('dashboard_page_title');
		$data['wysiwyg']         = 0;
		$data['map']             = 0;
		$data['jsFileArray']     = array('jsDashboardWidgets.js');
        
		$data['pageIconClass']   = 'iconDashboard';
		$data['pageLayoutClass'] = 'noRightColumn';
		
		$data['activeWidgets']   = DashboardOwners::getActiveWidgets('array');
        $data['pageButtons'] = array();
        array_push($data['pageButtons'], '<button class="buttonExpand popUpMenuClick action" id="buttonaddDashboardWidgets" title="'.lang('menu_add_dashboard_widgets').'" content="widgetSelector"><span class="dashboard">'.lang('menu_add_dashboard_widgets').'</span></button>');
        
        /*
         * Enforce project limits here because this is a nice convenient place.
         */
        if ($this->session->userdata('userType') == '100') {
            $this->Project_update->enforceProjectLimit(getAccountLevelSetting('projects'),$this->session->userdata('accountUserid'));
        }
        noCache();
		return $data;
	}
	
	function index() {			
		$data = DashboardOwners::_init();
		$this->load->view('dashboard/DashboardOwners',$data);
	}
	
	function getActiveWidgets($renderType=null) {
		/*
		 * Get widgets that user already has on their dashboard
		 */
		$widgetActiveArray = $this->Dashboard_owners->getActiveWidgets('dashboard');
		if ($renderType == 'array') {
			return $widgetActiveArray;
		}		
	}
	
	function saveDashboardOrder() {
        $orderArray = json_decode($_POST['jsonString'],true);
		for($a=1;$a<=3;$a++) {
			$widgetArray = $orderArray['Column'.$a];
            if (count($widgetArray)>0) {
                foreach($widgetArray as $widget) {
                    $widgetIDArray = explode('_',$widget['WidgetID']);
                    $wID    = $widgetIDArray[1];
                    $order  = $widget['Order'];
                    $column = $a-1;
                    $this->Dashboard_owners->saveDashboardOrder($wID,$column,$order);
                }
            }
		}
	}
	
	function removeWidget($wID,$type) {
		$this->Dashboard_owners->removeWidget($wID,$type);
	}
	
	function addWidget($wID,$widgetName,$type=NULL,$return=0) {
		$this->Dashboard_owners->addWidget($wID,$this->session->userdata('userid'),$type);
		
		/*
		 * Create widget HTML
		 */
		if ($type == 'footer') {
			$widgetString = DashboardOwners::rebuildFooterWidgetJSON();
		} else {
			$widgetString = createWidgetFrame($widgetName,$wID);
		}
		
		if ($return == 0) {
			echo $widgetString;
		}	
	}

	function rebuildFooterWidgetJSON() {
		$widgetArray = $this->commonwidgets->getActiveFooterWidgets($this->session->userdata('userid'));
		$sessionArray['footerWidgetArray'] = $widgetArray;
		$this->session->set_userdata($sessionArray);
		return json_encode($widgetArray);
	}
    
    function getOfficeStats($dateStart=null,$dateEnd=null,$renderType='json',$return=0) {
        /*
         * This method gets data for the stats window of the iPad app
         * (and maybe the web app eventually). Here's what we're getting...
         * 1. Incomplete tasks: open tasks / total tasks for date range.
         * 2. Open invoices: open invoices / total invoices for date range.
         * 3. Billable hours: billable hours / total hours for date range.
         */
        $permissionArray = $this->session->userdata('permissions');
        $viewAllTasks = TRUE;
        $viewAllInvoices = TRUE;
        $viewAllTimesheets = TRUE;
		 
        if ($permissionArray['taskViewOwn'] == 1) {
                $viewAllTasks = FALSE;
        }
        if ($permissionArray['invoiceViewOwn'] == 1) {
                $viewAllInvoices = FALSE;
        }
        if ($permissionArray['timesheetViewOwn'] == 1) {
                $viewAllTimesheets = FALSE;
        }
        $taskStatsArray = $this->Task_view->getTaskCountForDateRange($dateStart,$dateEnd,$this->session->userdata('pid'),$viewAllTasks,$this->session->userdata('accountUserid'));
        $invoiceStatsArray = $this->Finance_view->getInvoiceCountForDateRange($dateStart,$dateEnd,$this->session->userdata('userid'),$viewAllInvoices,$this->session->userdata('accountUserid'));
        $hoursStatsArray = $this->Timesheet_view->getHoursCountForDateRange($dateStart,$dateEnd,$this->session->userdata('userid'),$viewAllTimesheets,$this->session->userdata('accountUserid'));
        
        $outArray = array(
            'Tasks' => $taskStatsArray,
            'Invoices' => $invoiceStatsArray,
            'Hours' => $hoursStatsArray
        );
        
        $outArray['Tasks']['TotalTasks'] = $taskStatsArray['Complete'] + $taskStatsArray['Incomplete'];
        $outArray['Tasks']['PercentComplete'] = number_format(($taskStatsArray['Complete']/$outArray['Tasks']['TotalTasks'])*100,0);
        $outArray['Invoices']['TotalInvoices'] = $invoiceStatsArray['Paid'] + $invoiceStatsArray['Unpaid'];
        $outArray['Invoices']['PercentPaid'] = number_format(($invoiceStatsArray['Paid']/$outArray['Invoices']['TotalInvoices'])*100,0);
        $outArray['Hours']['TotalHours'] = number_format($hoursStatsArray['Billable'] + $hoursStatsArray['NotBillable'],1);
        $outArray['Hours']['PercentBillable'] = number_format(($hoursStatsArray['Billable']/$outArray['Hours']['TotalHours'])*100,0);
        
        if ($renderType == 'json') {
            if ($return == 0) {
                echo json_encode($outArray);
            } else {
                return json_encode($outArray);
            }
        } else {
            return $outArray;
        }
    }

    function gmailGadget() {
        $this->load->view('dashboard/DashboardGmail',$data);
    }

    function igoogleGadget() {
        $this->load->view('dashboard/DashboardIGoogle',$data);
    }
}
?>