<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Messages extends Controller {
	function __construct()
	{
		parent::Controller();	
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
		
		$this->load->model('messaging/Site_messages','',true);
        $this->load->model('tasks/Task_view','',true);
        $this->load->model('projects/Project_view','',true);
        $this->load->model('clients/Client_view','',true);
        $this->load->model('finances/Finance_view','',true);
        $this->load->model('finances/Finance_view_expenses','',true);
        $this->load->model('files/file_maintenance','FileMaintenance',TRUE);
        
        $this->load->library('application/CommonMessages');
		$this->load->library('application/CommonEmail');
        $this->load->library('application/CommonFileManager');
	}
	
	function _init() {
		$data['page']            = 'clients';
		$data['pageTitle']       = lang('client_page_title');
		$data['pageSubTitle']    = lang('client_view_page_title');
		$data['wysiwyg']         = 0;
		$data['jsFileArray']     = array('jsMessages.js');
		$data['pageIconClass']   = 'iconPageClients';
		$data['pageLayoutClass'] = 'withRightColumn';
				
		$this->load->view('includes/header',$data);
  		$this->load->view('messaging/MessageView');
  		$this->load->view('includes/footer',$data);
	}
	
	function saveMessage() {
		$toIDs      = $_POST['toIDs'];
        $toType     = $_POST['toType'];
        $toLanguage = $_POST['toLanguage'];

		$fromType = $_POST['fromType'];
        $message  = fieldToDB($this->input->post('message', TRUE), TRUE,TRUE,FALSE,TRUE);
		$itemType = $_POST['itemType'];
		$itemID   = $_POST['itemID'];
		if (isset($_POST['replyTo'])) {
			$replyTo = $_POST['replyTo'];
		} else {
			$replyTo = '';
		}	
        $messageID = $this->Site_messages->saveMessage($toIDs,$toType,$fromType,$message,$itemType,$itemID,$replyTo,$this->session->userdata('userid'),$this->session->userdata('accountUserid'));

        /*
         * Save messageID to file attachments
         */
        $fileLinkIDArray = explode('|',$_POST['fileLinkIDString']);
        foreach($fileLinkIDArray as $fileLinkID) {
            if ($fileLinkID>0) {
                $this->FileMaintenance->updateFileLinkWithItemID($fileLinkID,$messageID);
            }
        }

        /*
         * Get some information about itemType
         */
        $projectID = 0;
        if ($itemType == 'task') {
            $taskInfoArray = $this->Task_view->getTask($itemID);
            $subject2 = '#'.$taskInfoArray[0]['TaskNo'].' '.$taskInfoArray[0]['Title'];
        } elseif ($itemType == 'project') {
            $projectInfoArray = $this->Project_view->getProjectInformation($itemID);
            $projectTitle = $projectInfoArray['Title'];
            $projectID = $projectInfoArray['ProjectID'];
        } elseif ($itemType == 'client') {
            $clientInfoArray = $this->Client_view->getClientInformation($itemID);
            $clientCompany = $clientInfoArray['Company'];
        } elseif ($itemType == 'invoice') {
            $invoiceInfoArray = $this->Finance_view->getInvoiceDetails($itemID);
            $invoiceInfo = '#'.$invoiceInfoArray[0]['InvNo'].' '.$invoiceInfoArray[0]['Title'];
        } elseif ($itemType == 'expense') {
            $expenseInfoArray = $this->Finance_view_expenses->getExpenseDetails($itemID);
            $expenseInfo = $expenseInfoArray[0]['Title'];
        } elseif ($itemType == 'file') {
            $fileInfoArray = $this->FileMaintenance->getFileItemInformation($itemID);
            $fileName = $fileInfoArray['FileNameActual'];
        } elseif ($itemType == 'attachedFile') {
            $fileInfoArray = $this->FileMaintenance->getFileItemInformationFromLinkID($itemID);
            $fileName = $fileInfoArray['FileNameActual'];
        }

		$toArray         = explode('|',$toIDs);
        $toTypeArray     = explode('|',$toType);
        $toLanguageArray = explode('|',$toLanguage);
        $a=0;
		foreach($toArray as $recipID) {
            if (!empty($recipID)) {
                $recipLanguage = $toLanguageArray[$a];
                $recipType = $toTypeArray[$a];

                if ($recipID != $this->session->userdata('pid')) {
                    /*
                     * Because we don't need to send a message to ourselves
                     */
                    switch ($itemType) {
                        case 'task':
                            $messageArray = $this->commonemail->createSystemMessage('taskMessage',$recipLanguage);
                            $subject = sprintf($messageArray['subject'],$subject2);
                            $messageIntro = sprintf($messageArray['message'],$this->session->userdata('fullName'),$subject2);
                            break;
                        case 'project':
                            $messageArray = $this->commonemail->createSystemMessage('projectMessage',$recipLanguage);
                            $subject = sprintf($messageArray['subject'],$projectTitle);
                            $messageIntro = sprintf($messageArray['message'],$this->session->userdata('fullName'),$projectTitle);
                            break;
                        case 'file':
                            $messageArray = $this->commonemail->createSystemMessage('fileMessage',$recipLanguage);
                            $subject = sprintf($messageArray['subject'],$fileName);
                            $messageIntro = sprintf($messageArray['message'],$this->session->userdata('fullName'),$fileName);
                            break;
                        case 'attachedFile':
                            $messageArray = $this->commonemail->createSystemMessage('fileMessage',$recipLanguage);
                            $subject = sprintf($messageArray['subject'],$fileName);
                            $messageIntro = sprintf($messageArray['message'],$this->session->userdata('fullName'),$fileName);
                            break;
                        case 'client':
                            $messageArray = $this->commonemail->createSystemMessage('clientMessage',$recipLanguage);
                            $subject = sprintf($messageArray['subject'],$clientCompany);
                            $messageIntro = sprintf($messageArray['message'],$this->session->userdata('fullName'),$clientCompany);
                            break;
                        case 'invoice':
                            $messageArray = $this->commonemail->createSystemMessage('invoiceMessage',$recipLanguage);
                            $subject = sprintf($messageArray['subject'],$invoiceInfo);
                            $messageIntro = sprintf($messageArray['message'],$this->session->userdata('fullName'),$invoiceInfo);
                            break;
                        case 'expense':
                            $messageArray = $this->commonemail->createSystemMessage('expenseMessage',$recipLanguage);
                            $subject = sprintf($messageArray['subject'],$expenseInfo);
                            $messageIntro = sprintf($messageArray['message'],$this->session->userdata('fullName'),$expenseInfo);
                            break;
                        default:
                            $messageArray = '';
                            $subject = '';
                            $messageIntro = '';
                    }

                    /*
                     * Piece the entire message together
                     */
                    $footer = $this->commonemail->createSystemMessage('messageFooter',$recipLanguage);
                    $finalMessage = DBToField($messageIntro.Chr(10).Chr(10).$message.Chr(10).Chr(10).$footer['message'],TRUE);
                    $this->commonemail->sendEmail($finalMessage,$recipID,$recipType,$subject,'message');
                }
            }
            $a++;
		}

		$messageDateSent = convertMySQLToGMT(date('Y-m-d G:i:s'),'pull','M j, g:i a');

        $itemIcon = addslashes($this->commonmessages->getMessageIcon($itemType,$itemID));
        $files = json_encode($this->commonfilemanager->getFilesForSomething($messageID,'message','array',1));

		$jsonString =  '{"Messages":[{"MID":"'.$messageID.'","ItemType":"'.$itemType.'","ItemID":"'.$itemID.'","UIDFrom":"'.$this->session->userdata('userid').'",';
		$jsonString .= '"Message":"'.$message.'","DateSent":"'.$messageDateSent.'","FromName":"'.$this->session->userdata('fullName').'","Avatar":"'.$this->session->userdata('avatarMember').'","Editable":"1","ItemIcon":"'.$itemIcon.'","Files":'.$files.'}]}';
		
        $activity = 'create';
        $data = array(
            'AccountUserid' => $this->session->userdata('accountUserid'),
            'UserPID' => $this->session->userdata('pid'),
            'ProjectID' => $projectID,
            'UserType' => $this->session->userdata('userType'),
            'Activity' => $activity,
            'ItemID' => $messageID,
            'ItemType' => 'comment',
            'ItemTitle' => $message,
            'ReportToUser' => 1
        );
        $this->App_activity->logAppActivity($data);
        
		echo $jsonString;
	}
	
	function getMessagesForUpdateWindow() {
		$messageArray = $this->commonmessages->getMessagesForUpdateWindow('json',1);
        echo $messageArray;
	}
	
	function getMessages($itemID=NULL,$itemType=NULL,$unread=NULL,$viewAll=NULL,$toUserOnly=NULL,$renderType=NULL,$return=0,$start=NULL,$limit=NULL,$notNotified=NULL) {
		$permArray = $this->session->userdata('permissions');
        if ($viewAll == 1 && $permArray['messageViewAll'] == 1) {
            $viewAll = TRUE;
        } else {
            $viewAll = FALSE;
        }

        if ($toUserOnly == '1') {
            $toUserOnly = TRUE;
        } else {
            $toUserOnly = FALSE;
        }
        
        $messageArray = $this->commonmessages->getMessages($itemID,$itemType,$unread,$viewAll,$toUserOnly,$renderType,$return,$this->session->userdata('userid'),$this->session->userdata('accountUserid'),$this->session->userdata('pid'),$start,$limit,$notNotified);
        if ($return == 1) {
            return $messageArray;
        }
	}
    
    function getDiscussions($itemID=NULL,$itemType=NULL,$toUserOnly=NULL,$renderType='json',$return=0) {
        
    }
    
    function getMessageThreadFromMessageID($messageID,$renderType='json',$return=0) {
        $messageArray = $this->Site_messages->getMessage($messageID);
        print_r($messageArray);
        $itemID = $messageArray['ItemID'];
        $itemType = $messageArray['ItemType'];
        $messagesArray = $this->getMessages($itemID,$itemType,FALSE,null,null,$renderType,1);
        if ($renderType == 'json') {
            if ($return == 0) {
                echo json_encode($messagesArray);
            } else {
                return json_encode($messagesArray);
            }
        } else {
            return $messagesArray;
        }
    }

    function markMessageRead($messageID) {
        $this->Site_messages->markMessageRead($messageID,$this->session->userdata('userid'),$this->session->userdata('pid'));
    }
}	
?>