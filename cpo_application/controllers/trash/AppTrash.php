<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AppTrash extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('trash/Trash','',true);
	}

    function getItemsInTrash($itemType=NULL,$renderType='json',$return=0) {
        $permissions = $this->session->userdata('permissions');
        $userid = $this->session->userdata('userid');
        $accountUserid = 0;
        if ($permissions['trashViewAll'] == 1) {
            $accountUserid = $this->session->userdata('accountUserid');
        }
        $trashItemArray = $this->Trash->getItemsInTrashByType($itemType,$userid,$accountUserid);

        noCache();
        if ($renderType == 'json') {

            $jsonString = json_encode($trashItemArray);
            if ($return == 1) {
                return $jsonString;
            } else {
                echo decode_utf8($jsonString);
            }
        }
    }

    function getAllItemsInTrash($countOnly=0,$renderType='json',$return=0) {
        $trashClientsArray  = AppTrash::getItemsInTrash('client');
        $trashProjectsArray = AppTrash::getItemsInTrash('project');
        $trashInvoicesArray = AppTrash::getItemsInTrash('invoice');
        $trashExpensesArray = AppTrash::getItemsInTrash('expense');
        $trashContactsArray = AppTrash::getItemsInTrash('contact');
        $trashContactsArray = AppTrash::getItemsInTrash('calendar');
        $trashContactsArray = AppTrash::getItemsInTrash('event');
        $trashContactsArray = AppTrash::getItemsInTrash('note');
        $trashMessageArray  = AppTrash::getItemsInTrash('message');
        $trashCalendarArray = AppTrash::getItemsInTrash('task');
    }

    function getItemCountInTrash($renderType='json',$return=0) {
        $permissions = $this->session->userdata('permissions');
        $userid = $this->session->userdata('userid');
        $accountUserid = 0;
        if ($permissions['trashViewAll'] == 1) {
            $accountUserid = $this->session->userdata('accountUserid');
        }
        $trashCountArray['client']['label']   = lang('menu_clients');
        $trashCountArray['client']['iconCSS'] = 'icon_client_small';
        $trashCountArray['client']['count']   = $this->Trash->getNumberOfItemsInTrashByType('client',$userid,$accountUserid);

        $trashCountArray['project']['label']   = lang('menu_projects');
        $trashCountArray['project']['iconCSS'] = 'icon_project_small';
        $trashCountArray['project']['count']   = $this->Trash->getNumberOfItemsInTrashByType('project',$userid,$accountUserid);

        $trashCountArray['invoice']['label']   = lang('finance_invoices');
        $trashCountArray['invoice']['iconCSS'] = 'icon_invoice_small';
        $trashCountArray['invoice']['count']   = $this->Trash->getNumberOfItemsInTrashByType('invoice',$userid,$accountUserid);

        $trashCountArray['message']['label']   = lang('widget_messages');
        $trashCountArray['message']['iconCSS'] = 'icon_comment_small';
        $trashCountArray['message']['count']   = $this->Trash->getNumberOfItemsInTrashByType('message',$userid,$accountUserid);

        $trashCountArray['calendar']['label']   = lang('calendars');
        $trashCountArray['calendar']['iconCSS'] = 'icon_calendar_small';
        $trashCountArray['calendar']['count']   = $this->Trash->getNumberOfItemsInTrashByType('calendar',$userid,$accountUserid);

        $trashCountArray['event']['label']   = lang('calendar_events');
        $trashCountArray['event']['iconCSS'] = 'icon_event_small';
        $trashCountArray['event']['count']   = $this->Trash->getNumberOfItemsInTrashByType('event',$userid,$accountUserid);

        $trashCountArray['expense']['label']   = lang('finance_expenses');
        $trashCountArray['expense']['iconCSS'] = 'icon_expense_small';
        $trashCountArray['expense']['count']   = $this->Trash->getNumberOfItemsInTrashByType('expense',$userid,$accountUserid);

        $trashCountArray['note']['label']   = lang('widget_notes');
        $trashCountArray['note']['iconCSS'] = 'icon_notes_small';
        $trashCountArray['note']['count']   = $this->Trash->getNumberOfItemsInTrashByType('note',$userid,$accountUserid);

        $trashCountArray['contact']['label']   = lang('common_contacts');
        $trashCountArray['contact']['iconCSS'] = 'icon_vcard';
        $trashCountArray['contact']['count']   = $this->Trash->getNumberOfItemsInTrashByType('contact',$userid,$accountUserid);

        $trashCountArray['task']['label']   = lang('widget_tasks');
        $trashCountArray['task']['iconCSS'] = 'icon_tasks_small';
        $trashCountArray['task']['count']   = $this->Trash->getNumberOfItemsInTrashByType('task',$userid,$accountUserid);

        noCache();
        if ($renderType == 'json') {
            $jsonString = json_encode($trashCountArray);
            if ($return == 1) {
                return $jsonString;
            } else {
                echo $jsonString;
            }
        }
    }
}
?>