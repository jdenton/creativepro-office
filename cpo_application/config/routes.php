<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['scaffolding_trigger'] = 'scaffolding';
|
| This route lets you set a "secret" word that will trigger the
| scaffolding feature for added security. Note: Scaffolding must be
| enabled in the controller in which you intend to use it.   The reserved 
| routes must come before any wildcard or regular expression routes.
|
*/

if(preg_match("/^([a-zA-Z0-9\-_]+)\.([a-zA-Z0-9\-_]+)\.([a-zA-Z]{2,5})$/",$_SERVER["SERVER_NAME"],$matches)) {
	list($domain,$subdomain,$tld) = $matches;
}

if (empty($subdomain)) {
    header('Location: '.INSECURE_URL);
}

/*
if ($subdomain == 'www') {
	$route['default_controller'] = "appinit";
} elseif ($subdomain == '') {
    header('Location: '.BASE_URL);
} else {
    $route['default_controller'] = "login";
}
*/

if ($subdomain == 'm') {
    $route['default_controller'] = "mobile/mobile";
} elseif (!empty($subdomain) && $subdomain != 'www') {
    $route['default_controller'] = "login/index/0/".urlencode($subdomain);
} else {
    $route['default_controller'] = "appinit";
}
$route['blog'] = "blog/Blog";
$route['blog/entry'] = "blog/Blog/entry";
$route['blog/entry/(:any)'] = "blog/Blog/entry/$1";
$route['blog/archive'] = "blog/Blog/archive";
$route['blog/archive/(:any)/(:any)'] = "blog/Blog/archive/$1/$2";
$route['blog/tag'] = "blog/Blog/tag";
$route['blog/tag/(:any)'] = "blog/Blog/tag/$1";
$route['blog/search'] = "blog/Blog/search";

$route['helpdesk'] = "helpdesk/Helpdesk";
$route['helpdesk/entry'] = "helpdesk/Helpdesk/entry";
$route['helpdesk/entry/(:any)'] = "helpdesk/Helpdesk/entry/$1";
$route['helpdesk/archive'] = "helpdesk/Helpdesk/archive";
$route['helpdesk/archive/(:any)/(:any)'] = "helpdesk/Helpdesk/archive/$1/$2";
$route['helpdesk/tag'] = "helpdesk/Helpdesk/tag";
$route['helpdesk/tag/(:any)'] = "helpdesk/Helpdesk/tag/$1";
$route['helpdesk/search'] = "helpdesk/Helpdesk/search";

$route['forum'] = "forum/forum";
$route['mobile'] = "mobile/mobile";

$route['finances/EstimateUpdate'] = 'finances/InvoiceUpdate/estimate';

$route['scaffolding_trigger'] = "";
/* End of file routes.php */
/* Location: ./system/application/config/routes.php */