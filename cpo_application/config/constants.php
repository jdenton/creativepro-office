<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('VER_NUM',   '2000');
define('VER_HUMAN', '2.0.0.0');
define('IS_OFFLINE', FALSE);

define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 					'ab');
define('FOPEN_READ_WRITE_CREATE', 				'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 			'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
|---------------------------------------------------------------
| CUSTOM APPLICATION CONSTANTS (CPO)
|---------------------------------------------------------------
|
*/

/*
 * Determing http/https
 */
$http = 'http://';
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
    $http = 'https://';
}

define('SITE_URL',          $http.$_SERVER['HTTP_HOST']);
define('SITE_DOMAIN',       'mycpohq.com');
define('BASE_URL',          $http.$_SERVER['HTTP_HOST'].'/');

define('INSECURE_URL',      'http://www.mycpohq.com/');
define('SECURE_URL',        'https://secure.mycpohq.com/');

//define('INSECURE_URL',      'http://dev.mycpohq.com/');
//define('SECURE_URL',        'http://dev.mycpohq.com/');

define('BASE_JS_URL',       BASE_URL);
define('MAIL_SUPPORT',      'info@creativeprooffice.com');
define('MAIL_ADMIN',        'chiefupstart@gmail.com');
define('MAIL_NOBODY',       'mailsender@mycpohq.com');
define('ROOT_DIR',          $_SERVER['DOCUMENT_ROOT']);
define('FONT_DIR',          APPPATH.'libraries/fonts/');
define('ADMIN_CHART_DIR',   ROOT_DIR.'/images/adminCharts/');
define('ADMIN_CHART_PATH',  BASE_URL.'images/adminCharts/');
define('IMAGES_PATH',       BASE_URL.'images/');
define('IMAGES_PATH_EMAIL', INSECURE_URL.'images/emailImages/');
define('CHART_BG_COLOR',    '#FBFDED');
define('CHART_TMP_DIR',     APPPATH.'libraries/pchart/temp/');
define('USER_DIR',          ROOT_DIR.'/user_files/');
define('USER_DIR_REL',      BASE_URL.'user_files/');
define('SITE_TITLE',        'CreativePro Office');
define('EMAIL_SETTINGS_LINK',SITE_URL.'/settings/Settings');
define('SITE_CODE_PATH',    '/var/www/mycpohq.com/sitecode/CPOsiteCode.php');
define('LANGUAGE_FILE_PATH',ROOT_DIR.'/cpo_system/language/');
define('SWF_PATH',          BASE_URL.'flash/');
define('EMAIL_SUPPORT_FROM', 'CreativePro Office');
define('PUBLIC_META_DESCRIPTION', 'CreativePro Office is a complete set of online project management tools for creative professionals. Manage your team, clients, projects, invoices, time tracking, and project calendar from one web-based app.');

define('ASSET_URL',         'http://cpoassets.upstart-productions.com/');
define('CPOV1_URL',         'http://www.creativeprooffice.com');
define('PAY_PAL_URL',       'https://www.paypal.com/cgi-bin/webscr');

define('CPO_BLOG_URL',      'http://blog.mycpohq.com');
define('CPO_BLOG_TITLE',    'CreativePro Office Blog');

define('REMEMBER_ME_EXPIRE',2592000); // Remember me exipiration (30 days = 2592000)

/*
 * These are the PAYPAL sandbox settings.
 */
define('PAYPAL_ENDPOINT_LIVE',    'https://api-3t.paypal.com/nvp');
define('PAYPAL_ENDPOINT_SANDBOX', 'https://api-3t.sandbox.paypal.com/nvp');
define('PAYPAL_ENVIRONMENT',      'live'); // Or 'sandbox'
define('PAYPAL_API_VERSION',      '51.0');

/*
 ***** PERMISSISONS AND ROLES *****
 */
define("USER_TYPE_OWNER",  1);
define("USER_TYPE_TEAM",   2);
define("USER_TYPE_CLIENT", 3);
define("USER_TYPE_GOD",    100);

/*
 * Client-Project-Task-Invoices-Expenses-Timesheets-Files-Team-Messages-Calendar-Contacts
 */
define("PERM_ADMIN",      "1111-10111-10111-101011-10111-10111-1111-1111-10111-10111-1111-10-10");
define("PERM_USER",       "1111-10111-10111-101011-10111-10100-1111-0000-01111-10111-1111-01-01");
define("PERM_CONTRACTOR", "0000-01000-01110-010111-01111-01111-1111-0000-01111-01111-0000-01-01");
define("PERM_CLIENT",     "0000-01000-01000-010100-01000-01000-1000-0000-01111-01111-1111-10-10");

define("MAX_LOGO_UPLOAD_SIZE","2048000"); // 2MB in bytes
define("MAX_FILE_UPLOAD_SIZE","512000000"); // 500MB in bytes
define("ACCOUNT_FILE_FOLDER","accountFiles");

/*
 ***** FIREPHP *****
 */
define("FIRE_PHP_ENABLED", TRUE);

/*
 ***** API KEYS AND STUFF ***** 
 */
//define('GOOGLE_MAP_KEY', 'ABQIAAAA68ZZBxgEtZQuRj49NjRVKhS3ubTaOxk_cDuONCqi8oPeHjbXcxSxs7OK9WceYB2n2LVDhfQvJJFc7Q'); // CreativeProOffice.com
//define('GOOGLE_MAP_KEY', 'ABQIAAAA-4dpn0xgLICHe7OEQYMdqBSxsXYKiGMI_l9zmqmjhwvNzPhwDBR3UcZ1o5RsYIEGbU9eZgMxTx0vUA'); // CPOApp.com
define('GOOGLE_MAP_KEY', 'ABQIAAAA-4dpn0xgLICHe7OEQYMdqBRqOut4_L3jP_o6EHZ3RwM72EJ0qRSOqaSOl6q6d4yBZmKAOPCBbU1hhA'); // MyCPOHQ.com

define("GOOGLE_OAUTH_CONSUMER_KEY", "www.mycpohq.com");
define("GOOGLE_OAUTH_CONSUMER_SECRET", "TNMzRTjEaSM3tweHgZibiGYa");

define("AWS_ACCESS_KEY", "0TRVYJ15ER5NMW4K1XG2");
define("AWS_SECRET_KEY", "AE7yEtjTvat7r97I7+5i7EHCdjgqNr2/t/2ZGDbW");
define("AWS_BUCKET",     "creativeprooffice");
define("AWS_URL_ROOT",   "http://creativeprooffice.s3.amazonaws.com");
define("AWS_BUCKET_NAME","creativeprooffice");
define("AWS_ACCOUNT_LIMIT_PAID","3000000000");  // <--  3 GB
define('AWS_LINK_EXPIRE','28800');               // 8 hours: Time after which AWS links to files will expire unless user logs in again.

define("BOX_NET_KEY",    "11h169d4jvsqhsi7armevoiprp2sp08m");

define('TWITTER_CONSUMER_KEY',    'DciJb70VrFz1uQs5eb1mg');
define('TWITTER_CONSUMER_SECRET', 'Jz9CRBctvttrFpyisKS0uvgGvh3xzEp63EG4TQ7AEo');
define('TWITTER_OAUTH_TOKEN',     '19058844-Qn8xXuAvr8BBA4ePLRNht01608YZ1jxZZy9nz5dA7');
define('TWITTER_OAUTH_SECRET',    'DSAHg88wUeyAFASd2uURjZlWwNUVe4nZczyCjDYeTM');

define('FACEBOOK_APP_ID',     '288572127872894');
define('FACEBOOK_APP_SECRET', '348b6943eb816f518678117c208ba7c2');
define('FACEBOOK_PAGE_ID',    '114793700316');
define('FACEBOOK_BASE_URL',   'http://dev.mycpohq.com/');

define('DROPBOX_CONSUMER_KEY', 'bcoep12c9m5j9v1');
define('DROPBOX_CONSUMER_SECRET', 'swyml9bctq59q03');

/*
 * PAYPAL: These are the sandbox credentials.
 */
/*
define('PAYPAL_USER_NAME',        'chiefupstart+paypal_api1.gmail.com');
define('PAYPAL_PASSWORD',         'B2533C3EN6XW3HT8');
define('PAYPAL_SIGNATURE',        'AOlX34MekN0cvudVEEMixvczf0cHAZmy9YEOn1qAP7PBUBnIam3aUvNa');
*/
/*
 * PAYPAL: These are the live credentials.
 */
define('PAYPAL_USER_NAME',        'info_api1.upstart-productions.com');
define('PAYPAL_PASSWORD',         'Q3SSBWPZ56HSKES6');
define('PAYPAL_SIGNATURE',        'AFNQ2LsmzfhzbX9ayaILlRTwBBY7A96wAqqPv.4U5.Hgj1qrMTMyjZiO');
/*
 ***** SOME GENERAL ARRAYS *****
 */
$monthNameArray = array('January','February','March','April','May','June','July','August','September','October','November','December');
$alphaArray = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
$colorCSSArray = array(
						'light-blue'   => '#AFD8F8',
						'med-blue'     => '#0099FF',
						'dark-blue'    => '#1941A5',
						'light-green'  => '#B2DD32',
						'med-green'    => '#669966',
						'green-swatch' => '#8BBA00',
						'dark-green'   => '#006F00',
						'light-purple' => '#CCCCFF',
						'med-purple'   => '#7C7CB4',
						'purple'       => '#A66EDD',
						'dark-purple'  => '#9900FF',
						'light-red'    => '#F984A1',
						'pink'         => '#FF66CC',
						'red-swatch'   => '#FF0000',
						'orange'       => '#F6BD0F',
						'dark-orange'  => '#FF9933',
						'light-brown'  => '#e0ad4d',
						'brown'        => '#BF9A55',
						'dark-brown'   => '#ad7f2a',
						'gold'         => '#CCCC00',
						'light-gray'   => '#DEDDDD',
						'dark-gray'    => '#999999',
						'teal'         => '#0099CC',
						'light-teal'   => '#99FFCC'
					);

$colorChartArray = array(
                    '#0662B0',
					'#E48701',
					'#A5bC4E',
					'#1B95D9',
					'#CACA9E',
					'#6693B0',
					'#F05E27',
					'#86D1E4',
					'#E4F9A0',
					'#FFD512',
					'#75B000',
					'#F984A1',
					'#FF66CC',
					'#FF0000',
					'#F6BD0F',
					'#FF9933',
					'#e0ad4d',
					'#BF9A55',
					'#ad7f2a',
					'#CCCC00',
					'#DEDDDD',
					'#999999',
					'#0099CC',
					'#99FFCC'
					);

/*
 ***** HTML EMAIL STYLE SETTINGS *****
 */
$emailStyleArray = array(
    'H1' => 'style="font-size: 24px; font-family: arial; font-weight: bold;"',
    'H2' => 'style="font-size: 18px; font-family: arial; font-weight: bold;"',
    'H3' => 'style="font-size: 15px; font-family: arial; font-weight: bold;"',
    'P'  => 'style="font-size: 12px; font-family: arial; margin: 0px; padding: 0px;"',
    'SMALL_GRAY' => 'style="font-size: 11px; font-family: arial; color: #CCCCCC;"',
    'BLUE' => 'style="font-size: 12px; font-family: arial; color: #003399;"',
    'BOX_YELLOW' => 'style="border: 3px solid #fcfc80; background: #FFFFD2; padding: 6px;"',
    'BOX_BLUE'   => 'style="border: 3px solid #bddbf9; background: #ebf4fd; padding: 6px;"'
);

/*
 ***** DISALLOWED SUBDOMAINS *****
 */
$disallowedSubdomains = array(
  'blog',
  'forum',
  'dev',
  'code',
  'www',
  'secure',
  'm',
  'mobile',
  'help'
);

/*
 ***** PRODUCT PRICING
 */
$productArray = array(
    'selfhosted' => array(
        'name'        => 'Self-hosted source code',
        'price'       => '599.00',
        'description' => 'CreativePro Office self-hosted source code with 1 year of free updates and email support.'
    ),
    'updates'    => array(
        'name'        => '1 year self-hosted update and email support subscription',
        'price'       => '199.00',
        'description' => '1 year subscription: free updates and email suppport for CreativePro Office self-hosted source code.'
    )
);

/*
 ***** ACCOUNT LEVEL INFORMATION    
 */
$accountPricePrev = array('0','5','15','30');
$accountPriceNext = array('0','9','29','49');
$datePriceChange  = '2012-09-30';

$accountLevelArray = array(
    '0' => array(
        'name'        => 'free',
        'teamMembers' => '1',
        'projects'    => '3',
        'invoices'    => '100000',
        'fileSpace'   => '0',
        'cost'        => '0',
        'support'     => 'Forum only'
    ),
    '1' => array(
        'name'        => 'solo',
        'teamMembers' => '3',
        'projects'    => '10',
        'invoices'    => '100000',
        'cost'        => '5',
        'fileSpace'   => '10737418240', // 10GB
        'support'     => 'Forum only'
    ),
    '2' => array(
        'name'        => 'shop',
        'teamMembers' => '10',
        'projects'    => '50',
        'invoices'    => '100000',
        'cost'        => '15',
        'fileSpace'   => '53687091200', // 50GB
        'support'     => 'Priority email'
    ),
    '3' => array(
        'name'        => 'team',
        'teamMembers' => '100000',
        'projects'    => '100000',
        'invoices'    => '100000',
        'cost'        => '30',
        'fileSpace'   => '107374182400', // 100GB
        'support'     => 'Priority email'
    )
);
/* End of file constants.php */
/* Location: ./system/application/config/constants.php */