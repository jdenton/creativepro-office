<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		Rick Ellis
 * @copyright	Copyright (c) 2006, EllisLab, Inc.
 * @license		http://www.codeignitor.com/user_guide/license.html
 * @link		http://www.codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * FTP Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Rick Ellis ( Modified by Philip Sturgeon )
 * @link		http://www.codeigniter.com/user_guide/libraries/ftp.html
 */
class MY_FTP extends CI_FTP {

	var $ssl_mode	= FALSE;

	/**
	 * Constructor - Sets Preferences
	 *
	 * The constructor can be passed an array of config values
	 */
	function MY_FTP($config = array())
	{
		if (count($config) > 0)
		{
			$this->initialize($config);
		}

		log_message('debug', "FTP Class Initialized");
	}

	// --------------------------------------------------------------------

	/**
	 * Initialize preferences
	 *
	 * @access	public
	 * @param	array
	 * @return	void
	 */
	function initialize($config = array())
	{
		foreach ($config as $key => $val)
		{
			if (isset($this->$key))
			{
				$this->$key = $val;
			}
		}

		// Prep the hostname
		$this->hostname = preg_replace('|.+?://|', '', $this->hostname);
	}

	// --------------------------------------------------------------------

	/**
	 * FTP Connect
	 *
	 * @access	public
	 * @param	array	 the connection values
	 * @return	bool
	 */
	function connect($config = '')
	{
		// Load the DB config file if a DSN string wasn't passed
		if (is_string($config))
		{
			include(APPPATH.'config/Ftp'.EXT);

			$group = ($config == '') ? $active_group : $config;

			if ( ! isset($ftp[$group]) || !is_array($ftp[$group]))
			{
				show_error('You have specified an invalid ftp connection group: '.$group);
			}
			else
			{
				$this->initialize($ftp[$group]);
			}
		}

		elseif(is_array($config))
		{
			if (count($config) > 0)
			{
				$this->initialize($config);
			}
		}

		if($this->ssl_mode == TRUE)
		{
			if(function_exists('ftp_ssl_connect'))
			{
				$this->conn_id = @ftp_ssl_connect($this->hostname, $this->port);
			}
			else
			{
				$this->_error('ftp_ssl_not_supported');
			}
		}
		else
		{

			$this->conn_id = @ftp_connect($this->hostname, $this->port);
		}


		if ($this->conn_id === FALSE)
		{
			if ($this->debug == TRUE)
			{
				$this->_error('ftp_unable_to_connect');
			}
			return FALSE;
		}

		if ( ! $this->_login())
		{
			if ($this->debug == TRUE)
			{
				$this->_error('ftp_unable_to_login');
			}
			return FALSE;
		}

		// Set passive mode if needed
		if ($this->passive == TRUE)
		{
			ftp_pasv($this->conn_id, TRUE);
		}

		return TRUE;
	}


	function download($rempath, $locpath, $mode = 'auto')
	{
		if ( ! $this->_is_conn())
		{
			return FALSE;
		}

		// Set the mode if not specified
		if ($mode == 'auto')
		{
			// Get the file extension so we can set the upload type
			$ext = $this->_getext($rempath);
			$mode = $this->_settype($ext);
		}

		$mode = ($mode == 'ascii') ? FTP_ASCII : FTP_BINARY;

        if (ftp_size($this->conn_id,$rempath) != -1) {
            $result = ftp_get($this->conn_id, $locpath, $rempath, $mode);
        } else {
            $result = FALSE;
        }
		if ($result === FALSE)
		{
			if ($this->debug == TRUE)
			{
				$this->_error('ftp_unable_to_download');
			}
			return FALSE;
		}

		return TRUE;
	}

}
// END MY_FTP Class
?>