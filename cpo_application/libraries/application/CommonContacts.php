<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonContacts {
	function saveTeamMemberLoginInformation($data) {
		$CI =& get_instance();
		$CI->load->model('users/User_information','',TRUE);

		$CI->lang->load('app',$CI->session->userdata('language'));


	}
	
    function getContacts($accountUserid,$itemType=NULL,$itemID=NULL,$teamMember=FALSE,$vendor=FALSE,$renderType='array',$return=0) {
        $CI =& get_instance();
        $CI->load->model('contacts/Contacts','',TRUE);

        $max = NULL;
        if ($teamMember == TRUE) {
            /*
             * Make sure we only catch our limit. Add 1 to account for the account owner.
             */
            $max = getAccountLevelSetting('teamMembers');
        }

        $contactArray = $CI->Contacts->getContacts($accountUserid,$itemType,$itemID,$teamMember,$vendor,$max);
        
        /*
         * Sort array by NameLast
         */
        function cmp($a, $b) {
            return strcmp($a['NameLast'], $b['NameLast']);
        }
        usort($contactArray, "cmp");
		if ($renderType == 'array') {
			return $contactArray;
		} elseif ($renderType == 'json') {
            if ($return == 1) {
                return DBToField(json_encode($contactArray));
            } else {
                echo DBToField(json_encode($contactArray));
            }
		}
	}

    function buildTeamMemberArray($accountUserid) {
        $CI =& get_instance();
        $CI->load->model('contacts/Contacts','',TRUE);

        $teamArray = $CI->Contacts->getContacts($accountUserid,NULL,NULL,0,TRUE);
        $newTeamArray = array();
        $a=0;
        foreach($teamArray as $team) {
            $newTeamArray[$a]['First_Name'] = $team['NameFirst'];
            $newTeamArray[$a]['Last_Name']  = $team['NameLast'];
            $newTeamArray[$a]['UID']        = $team['LoginUserid'];
            $newTeamArray[$a]['Language']   = $team['Language'];
            $a++;
        }

        return $newTeamArray;
    }
}
?>
