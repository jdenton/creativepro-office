<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonProject {
     /**
	 * getTask : retrieve task information for view or edit
	 *
	 * @access	public
	 * @param	int  $taskID ID of the task for which we want data
	 * @return	mixed  PHP array of task data or JSON sting
	 */
     function renderProject($projectArray,$renderType=NULL,$short=NULL,$group=NULL) {
		$CI =& get_instance();
		$CI->load->model('projects/Project_view','',true);
        $CI->load->model('finances/Finance_view_expenses','',true);
		$CI->load->model('finances/Finance_view','',true);
		$CI->load->model('tasks/Task_view','',true);
		$CI->load->model('messaging/Site_messages','',true);
		$CI->load->model('files/File_maintenance','',true);
		$CI->load->model('contacts/Contacts','',true);
        $CI->load->model('timesheets/Timesheet_view','',true);
		$CI->lang->load('app',$CI->session->userdata('language'));
		$CI->lang->load('date',$CI->session->userdata('language'));

        $permissionArray = $CI->session->userdata('permissions');
        $viewAll = TRUE;

        $projectArray['ProjectTime'] = number_format($projectArray['ProjectTime'],2);
        $projectArray['Title'] = stripslashes($projectArray['Title']);

		/*
		 * Project status
		 */
		$statusArray = getProjectStatus($projectArray['Status'],$projectArray['Date_Start'],$projectArray['Date_End']);
		$projectArray['StatusHuman']   = $statusArray[0];
		$projectArray['StatusMachine'] = $statusArray[1];
		$projectArray['StatusText']    = $statusArray[2];

		/*
		 * Project date range
		 */
		if ($projectArray['Date_Start'] != '0000-00-00') {
			$projectArray['Date_Start'] = date('M j, Y', strtotime($projectArray['Date_Start']));
			$projectArray['Date_Start_Sort'] = date('m/d/Y', strtotime($projectArray['Date_Start']));
		} else {
			$projectArray['Date_Start'] = '';
			$projectArray['Date_Start_Sort'] = '';
		}

		if ($projectArray['Date_End'] != '0000-00-00') {
			$projectArray['Date_End'] = date('M j, Y', strtotime($projectArray['Date_End']));
			$projectArray['Date_End_Sort'] = date('m/d/Y', strtotime($projectArray['Date_End']));
		} else {
			$projectArray['Date_End'] = '';
			$projectArray['Date_End_Sort'] = '';
		}
		if (!empty($projectArray['Date_Start']) && !empty($projectArray['Date_End']) ) {
			$projectArray['Date_Range'] = $projectArray['Date_Start'].' to '.$projectArray['Date_End'];
		} elseif (!empty($projectArray['Date_Start'])) {
			$projectArray['Date_Range'] = $projectArray['Date_Start'];
		} elseif (!empty($projectArray['Date_End'])) {
			$projectArray['Date_Range'] = $projectArray['Date_End'];
		} else {
			$projectArray['Date_Range'] = lang('common_not_specified');
		}

		$projectArray['Description'] = clearNewlineReturn($projectArray['Description']);

		/*
		 * Project hours
		 */
		if (empty($projectArray['ProjectTime'])) {
			$projectArray['ProjectTime'] = 0;
		}

		/*
		 * Number of invoices, messages, files for this project
		 */
		if (($invoiceTotalArray = $CI->Finance_view->getInvoiceTotals($projectArray['ProjectID'],'project'))>0) {
			$invoiceCountArray  = $CI->Finance_view->getInvoiceCount($projectArray['ProjectID'],'project',$CI->session->userdata('accountUserid'));

			$projectArray['InvoiceTotal']      = $invoiceTotalArray['Total'];
			$projectArray['InvoiceTotalHuman'] = getCurrencyMark($CI->session->userdata('currency')).number_format($invoiceTotalArray['Total'],2);
			$projectArray['InvoiceCount']      = $invoiceCountArray['InvoiceCount'];
		} else {
			$projectArray['InvoiceTotal']      = 0;
			$projectArray['InvoiceTotalHuman'] = 0;
			$projectArray['InvoiceCount']      = 0;
		}

		/*
		 * Number of tasks for this project
		 */
        if ($permissionArray['taskViewOwn'] == 1) {
            $viewAll = FALSE;
		}
		$taskTotalArray    = $CI->Task_view->getNumberOfTasks($projectArray['ProjectID'],'project',NULL,$viewAll);
        $taskCompleteArray = $CI->Task_view->getNumberOfTasks($projectArray['ProjectID'],'project','complete',$viewAll);
        $taskOverdueArray  = $CI->Task_view->getNumberOfTasks($projectArray['ProjectID'],'project','overdue',$viewAll);
		if ($taskTotalArray['Tasks']>0) {
			$projectArray['TaskTotal']          = $taskTotalArray['Tasks'];
            $projectArray['TasksCompleted']     = $taskCompleteArray['Tasks'];
            $projectArray['TasksOverdue']       = $taskOverdueArray['Tasks'];
			$projectArray['TaskEstimatedHours'] = $taskTotalArray['HoursEstimated'];
		} else {
			$projectArray['TaskTotal']          = '0';
            $projectArray['TasksCompleted']     = '0';
            $projectArray['TasksOverdue']       = '0';
			$projectArray['TaskEstimatedHours'] = '0';
		}

		/*
		 * Number of messages for this project (read and unread)
		 */         
		 $viewAll = TRUE;

		 if ($permissionArray['messageViewOwn'] == 1) {
			 $viewAll = FALSE;
		 }
		 $noOfMessagesArray = $CI->Site_messages->getNumberOfMessagesForSomething($projectArray['ProjectID'],'project',$CI->session->userdata('userid'),$CI->session->userdata('accountUserid'),$CI->session->userdata('pid'),$viewAll);
		 $projectArray['MessageTotal'] = $noOfMessagesArray['NoOfMessagesTotal'];
		 $projectArray['MessageUnread'] = $noOfMessagesArray['NoOfMessagesUnread'];

		/*
		 * Number of files for this project
		 */
         $shared = FALSE;
         if ($group == 'client') {
             $shared = TRUE;
         }
		 $projectArray['FileTotal'] = $CI->File_maintenance->getNumberOfFilesForSomething($projectArray['ProjectID'],'project',$shared);

		/*
		 * Project Contacts
		 */
         if ($short != TRUE) {
            $contactArray = $CI->Contacts->getContacts($CI->session->userdata('accountUserid'),'P',$projectArray['ProjectID'],0);
            $projectArray['Contacts'] = $contactArray;
         } else {
            $projectArray['Contacts'] = array();
         }
        /*
         * Make message recipient JSON
         */
        $projectArray['MessageRecipients'] = array();
        $a=0;
        if (sizeof($projectArray['Contacts'])>0) {
            
            foreach($projectArray['Contacts'] as $contact) {
                if ($contact['TeamMember'] == 1) {
                    $type = 'team';
                } elseif ($contact['Vendor'] == 1) {
                    $type = 'vendor';
                } elseif ($contact['AccountCreator'] == 1) {
                    $type = 'owner';
                } else {
                    $type = 'contact';
                }
                $projectArray['MessageRecipients'][$a]['NameFull'] = $contact['NameFirst'].' '.$contact['NameLast'];
                $projectArray['MessageRecipients'][$a]['PID']      = $contact['PID'];
                $projectArray['MessageRecipients'][$a]['UID']      = $contact['LoginUserid'];
                $projectArray['MessageRecipients'][$a]['Type']     = $type;
                $projectArray['MessageRecipients'][$a]['Language'] = $contact['Language'];
                $a++;
            }
        }
        /*
         * Now add the client
         */
        $projectArray['Company'] = stripslashes($projectArray['Company']);
        $projectArray['MessageRecipients'][$a]['NameFull'] = stripslashes($projectArray['Company']);
        $projectArray['MessageRecipients'][$a]['PID']      = $projectArray['ClientID'];
        $projectArray['MessageRecipients'][$a]['UID']      = $projectArray['ClientUserid'];
        $projectArray['MessageRecipients'][$a]['Type']     = 'client';
        $projectArray['MessageRecipients'][$a]['Language'] = $projectArray['Language'];
		
		$jsonString = json_encode($projectArray).',';
		$totalHours = $projectArray['ProjectTime'];
        
        /*
         * Tally up the budget, if there's a project budget.
         */
        $projectArray['BudgetSpent'] = 0;
        $projectArray['BudgetPercent'] = 0;
        if ($projectArray['Budget'] > 0) {
            $expenseTotalArray = $CI->Finance_view_expenses->getTotalExpensesForProject($projectArray['ProjectID']);
            $hourlyTotal = $CI->Timesheet_view->getRateTotalForHours($projectArray['ProjectID'], 'project', $CI->session->userdata('accountUserid'));
            $grandTotal = $expenseTotalArray['Total'] + $hourlyTotal;
            
            $projectArray['BudgetSpent'] = number_format( ($grandTotal),2 );
            $projectArray['BudgetPercent'] = number_format( ( ( $grandTotal / $projectArray['Budget'] ) * 100 ), 0 );
        }
        
		if ($renderType == 'json') {
			$return = json_encode($projectArray);
        } else if ($renderType == 'array') {
			$return = $projectArray;
        }
        return $return;
     }
}
?>