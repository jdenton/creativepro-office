<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonFileManager {

    function uploadLocalFile($filePath,$fileData,$maxFileSize=NULL,$fileTypes=NULL) {
        $CI =& get_instance();
        $recipLanguage = $CI->session->userdata('language');
        $CI->lang->load('app',$recipLanguage);
        $fileTemp = $fileData['tmp_name'];
        $fileName = $fileData['name'];
        $fileType = $fileData['type'];
        $fileSize = $fileData['size'];

        /*
         * Check file types
         */
        if (!empty($fileTypes)) {
            $error = TRUE;
            $extension = substr(strrchr($fileName, '.'), 1);
            $fileTypeArray = explode('|',$fileTypes);
            $fileTypeString = '';
            foreach($fileTypeArray as $type) {
                if ($type == $extension) {
                    $error = FALSE;
                }
                $fileTypeString .= $type.' ';
            }
            if ($error == TRUE) {
                return 'error|'.sprintf(lang('error_incorrect_file_type'),$fileTypeString);
            }
        }
        
        if ($fileSize>$maxFileSize) {
            return 'error|'.lang('error_file_too_large');
        }
        $extension = substr(strrchr($fileName, '.'), 1);
        $newFileName = random_string('alnum',15).'.'.$extension;
        
        $filePath = $filePath.$newFileName;
        if (move_uploaded_file($fileTemp,$filePath)) {
            return $newFileName;
        } else {
            return 'error|'.lang('error_unknown_error');
        }
    }

	function deleteLocalFile($filePath,$fileName) {
		@unlink($filePath.$fileName);
	}

    function getFilesForSomething($itemID,$itemType,$renderType='json',$return=0) {
        $CI =& get_instance();
        $CI->load->library('application/FileManagerS3');
        $CI->load->model('files/File_maintenance','',true);
        $CI->load->helper('file');
        
        $guid = null;
        if (!is_numeric($itemID) && $itemID != 0) {
            $guid = $itemID;
            $itemID = 0;
        }
        
        $shared = FALSE;
        if ($CI->session->userdata('userType') == USER_TYPE_CLIENT) {
            $shared = TRUE;
        }
        $fileArray = $CI->File_maintenance->getFilesForSomething($itemID,$itemType,$shared,$guid);

        $j=0;
        foreach($fileArray as $file) {
            $fileArray[$j]['FolderID'] = '';
            if ($file['FileService'] == 1) {
                $fileIconArray = get_mime_icon_by_fileservice_path($file['FileServicePath']);
                $fileArray[$j]['fileLink'] = $file['FileServicePath'];
                $fileArray[$j]['FolderID'] = $file['LinkID'];
            } else {
                $fileIconArray = get_mime_icon_by_extension($file['FileNameActual']);
                $fileArray[$j]['fileLink']     = $CI->filemanagers3->createFileLink($file['FileNameActual']);
            }
            $fileArray[$j]['FileClassName'] = 'icon_file_'.$fileIconArray[2];
            $fileArray[$j]['iconSmall']     = $fileIconArray[0];
            $fileArray[$j]['iconBig']       = $fileIconArray[1];
            $fileArray[$j]['fileExt']       = $fileIconArray[2];
            $fileArray[$j]['UploaderName']  = $file['NameFirst'].' '.$file['NameLast'];
            $j++;
        }
        
        if ($renderType == 'json') {
                $return = '{"Files:"'.json_encode($fileArray).'}';
        } elseif ($renderType == 'array') {
                $return = $fileArray;
        }

        if ($return == 0) {
            echo $return;
        } else {
            return $return;
        }
    }
}
?>