<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FileManagerS3 extends Controller {
	/**
      * uploadFileHandler : upload files to Amazon S3 service
	  *
      * @access	public
	  * @param  array  $fileArray Array of files to be deleted
      * @return void
      */
	function uploadFileHandler() {
		$CI =& get_instance();
        $CI->load->model('files/File_maintenance');
        include(APPPATH.'libraries/s3/S3.php');
		$CI->load->helper('file');
		$CI->load->helper('download');

          // Check the upload
          $fileNameTmp  = $_FILES['Filedata']['tmp_name'];
          $fileName     = $_FILES['Filedata']['name'];
          $someFileType = $_FILES['Filedata']['type'];
          $someFileSize = $_FILES['Filedata']['size'];
          $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);

		  /*
		   * Create hash filename for S3
		   */
		   $extension = substr(strrchr($fileName, '.'), 1);
		   $hashFileName = random_string('alnum',30).'_'.time().'.'.$extension;

		  /*
		   * Try creating a 'folder' and then putting the file into the folder
		   */
		  $folderName = $CI->session->userdata('userDir');
		  $s3->putObject($folderName.'_$folder$', AWS_BUCKET_NAME, $folderName.'_$folder$', S3::ACL_PUBLIC_READ);

		  //move the file
		  if ($s3->putObjectFile($fileNameTmp, AWS_BUCKET_NAME, $folderName.'/'.$hashFileName, S3::ACL_PUBLIC_READ)) {
			$status = 'Success';
			/*
			 * Enter file information into database
			 */
			$fileArray['FileNameActual'] = $fileName;
			$fileArray['FileNameHash']   = $hashFileName;
			$fileArray['FileSize']       = $someFileSize;
			$fileArray['FileType']       = $someFileType;
			$fileArray['action']         = 'add';
			$fileArray['ParentID']       = $_POST['selectedFolderID'];

			$CI->File_maintenance->insertFileData($fileArray);
		  } else {
			$status = 'Error';
		  }
          return $status;
		  echo $status;
     }

	 /**
      * deleteItemFromS3 : delete files from S3 bucket
	  *
      * @access	public
	  * @param  array  $fileArray Array of files to be deleted
      * @return void
      */
	 function deleteItemFromS3($fileArray) {
		$CI =& get_instance();
        $CI->load->model('files/File_maintenance');
        include(APPPATH.'libraries/s3/S3.php');
		$CI->load->helper('file');
		$CI->load->helper('download');

		if (is_array($fileArray)) {
			 $folderName = $CI->session->userdata('userDir');
			 $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
			 $a=0;
			 foreach($fileArray as $file) {
				 $hashFileName = $file;
				 echo $hashFileName;
				 $s3->deleteObject(AWS_BUCKET_NAME, $folderName.'/'.$hashFileName);
				 $a++;
			 }
		 }
	 }
}

?>
