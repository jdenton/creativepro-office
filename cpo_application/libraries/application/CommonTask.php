<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonTask {
     /**
	 * getTask : retrieve task information for view or edit
	 *
	 * @access	public
	 * @param	int  $taskID ID of the task for which we want data
	 * @return	mixed  PHP array of task data or JSON sting
	 */
     function getTask($taskID,$renderType=NULL,$return=0) {
          $CI =& get_instance();
          $CI->load->model('tasks/task_view','TaskView',TRUE);
          $taskArray = $CI->TaskView->getTask($taskID);
          $taskJSON  = CommonTask::renderTasks($taskArray,'json');
          if ($return == 1) {
               return $taskJSON;
          } else {
               echo $taskJSON;
          }
     }

	 /**
	 * getMilestone : retrieve milestone information for view or edit
	 *
	 * @access	public
	 * @param	int    $milestone ID of the milestone for which we want data
	 * @return	mixed  PHP array of milestone data or JSON sting
	 */
     function getMilestone($milestoneID,$renderType=NULL,$return=0) {
          $CI =& get_instance();
          $CI->load->model('tasks/task_view','TaskView',TRUE);

          $milestoneArray = $CI->TaskView->getMilestone($milestoneID);
		  $noOfTasksArray = $CI->TaskView->getNumberOfTasks($milestoneID,'milestone');
		  $milestoneArray['TaskCount'] = $noOfTasksArray['Tasks'];
          $milestoneArray['Tasks']     = array();
          $milestoneJSON  = CommonTask::renderMilestone($milestoneArray,'json');
          if ($return == 1) {
               return $milestoneJSON;
          } else {
               echo $milestoneJSON;
          }
     }

	 function renderMilestone($milestoneArray,$renderType=NULL) {
        if ($milestoneArray['Date_Start'] != '0000-00-00' && !empty($milestoneArray['Date_Start']) && $milestoneArray['Date_Start'] != '0000-00-00 00:00:00') {
            $milestoneArray['DateStartField']  = MySQLDateToJS(convertMySQLToGMT($milestoneArray['Date_Start'],'pull','Y-m-d'));
            $milestoneArray['Date_Start_Sort'] = convertMySQLToGMT($milestoneArray['Date_Start'],'pull','m/d/Y');
			$milestoneArray['Date_Start']      = convertMySQLToGMT($milestoneArray['Date_Start'],'pull','M j, Y');
        } else {
            $milestoneArray['Date_Start']      = '';
            $milestoneArray['DateStartField']  = '';
            $milestoneArray['Date_Start_Sort'] = '';
        }
        if ($milestoneArray['Date_End'] != '0000-00-00' && !empty($milestoneArray['Date_End']) && $milestoneArray['Date_End'] != '0000-00-00 00:00:00') {
            $milestoneArray['DateEndField']  = MySQLDateToJS(convertMySQLToGMT($milestoneArray['Date_End'],'pull','Y-m-d'));
            $milestoneArray['Date_End_Sort'] = convertMySQLToGMT($milestoneArray['Date_End'],'pull','m/d/Y');
			$milestoneArray['Date_End']      = convertMySQLToGMT($milestoneArray['Date_End'],'pull','M j, Y');
        } else {
            $milestoneArray['Date_End']      = '';
            $milestoneArray['DateEndField']  = '';
            $milestoneArray['Date_End_Sort'] = '';
        }
        $milestoneArray['DateString'] = getDateString($milestoneArray['Date_Start'],$milestoneArray['Date_End'],'milestone');
        if ($renderType == 'json') {
            $return = json_encode($milestoneArray);
        } elseif ($renderType == 'array') {
            $return = $milestoneArray;
        }
        return $return;
     }

     function renderTasks($taskArray,$renderType=NULL) {
          $CI =& get_instance();
          $CI->load->library('application/CommonFileManager');
          $j=0;

          foreach($taskArray as $task) {
			   if ($task['Date_Start'] != '0000-00-00' && $task['Date_Start'] != '0000-00-00 00:00:00') {
                    $taskArray[$j]['Date_Start_Sort'] = convertMySQLToGMT($task['Date_Start'],'pull','m/d/Y');
					$taskArray[$j]['DateStartField']  = MySQLDateToJS(convertMySQLToGMT($task['Date_Start'],'pull','Y-m-d'));
					$taskArray[$j]['Date_Start']      = convertMySQLToGMT($task['Date_Start'],'pull','M j');
               } else {
                    $taskArray[$j]['Date_Start'] = '';
                    $taskArray[$j]['Date_Start_Sort'] = '';
					$taskArray[$j]['DateStartField']  = '';
               }
               if ($task['Date_End'] != '0000-00-00' && $task['Date_End'] != '0000-00-00 00:00:00') {
                    $taskArray[$j]['Date_End_Sort'] = convertMySQLToGMT($task['Date_End'],'pull','m/d/Y');
					$taskArray[$j]['DateEndField']  = MySQLDateToJS(convertMySQLToGMT($task['Date_End'],'pull','Y-m-d'));
					$taskArray[$j]['Date_End']      = convertMySQLToGMT($task['Date_End'],'pull','M j');
               } else {
                    $taskArray[$j]['Date_End'] = '';
                    $taskArray[$j]['Date_End_Sort'] = '';
					$taskArray[$j]['DateEndField']  = '';
               }
               if ($task['Date_Completed'] != '0000-00-00' && !empty($task['Date_Completed'])) {
                    $taskArray[$j]['Date_Completed'] = convertMySQLToGMT($task['Date_Completed'],'pull','M j');
               } else {
                    $taskArray[$j]['Date_Completed'] = '';
               }
			   $taskArray[$j]['DateString'] = getDateString($taskArray[$j]['Date_Start'],$taskArray[$j]['Date_End'],'task');

               /*
                * Task status
                */
               $statusArray = getProjectStatus($task['Status'],$task['Date_Start'],$task['Date_End'],TRUE);
               $taskArray[$j]['StatusHuman']   = $statusArray[0];
               $taskArray[$j]['StatusMachine'] = $statusArray[1];
               $taskArray[$j]['StatusText']    = $statusArray[2];

               /*
                * Task priority
                */
               $priorityArray = getTaskPriorityIcon($task['Priority']);
               $taskArray[$j]['PriorityText']  = $priorityArray[0];
               $taskArray[$j]['PriorityClass'] = $priorityArray[1];
               
               /*
                * Get attached files
                */
               $taskArray[$j]['Files'] = $CI->commonfilemanager->getFilesForSomething($task['TaskID'],'task','array',1);

               $j++;
          }
          if ($renderType == 'json') {
               $return = json_encode($taskArray);
          } else if ($renderType == 'array') {
               $return = $taskArray;
          }

          return $return;
     }

    function getTasksForSelect($projectID,$renderType='json',$return=1,$flattenMilestones=FALSE) {
        $CI =& get_instance();
        $CI->load->model('tasks/task_view','TaskView',TRUE);
        $permArray = $CI->session->userdata('permissions');
        $viewAll = TRUE;
        if ($permArray['taskViewOwn'] == 1) {
            $viewAll = FALSE;
        }

	$taskArray = $CI->TaskView->getTasksForSelectFromProject($projectID,$viewAll,$CI->session->userdata('pid'));

        /*
         * Need some logic in here to get tasks under the correct milestones.
         */
        $newTaskArray = array();
        
        //$flattenMilestones = TRUE;
        $a=0;
        if (sizeof($taskArray)>0) {
            if ($viewAll == TRUE) {
                foreach($taskArray as $task) {
                    if ($task['Milestone'] == 1) {
                        $parentTaskID = $task['TaskID'];
                        $newTaskArray[$a]['TaskID']    = $parentTaskID;
                        $newTaskArray[$a]['Title']     = $task['Title'];
                        $newTaskArray[$a]['Milestone'] = 1;

                        if ($flattenMilestones == TRUE) {
                            $a++;
                            foreach($taskArray as $task2) {
                                if ($task2['ParentTaskID'] == $parentTaskID) {
                                    $newTaskArray[$a]['TaskID']   = $task2['TaskID'];
                                    $newTaskArray[$a]['Title']    = $task2['Title'];
                                    $newTaskArray[$a]['Milestone'] = 0;
                                    $a++;
                                }
                            }
                        } else {
                            $newTaskArray[$a]['Tasks']     = array();

                            $b=0;
                            foreach($taskArray as $task2) {
                                if ($task2['ParentTaskID'] == $parentTaskID) {
                                    $newTaskArray[$a]['Tasks'][$b]['TaskID']   = $task2['TaskID'];
                                    $newTaskArray[$a]['Tasks'][$b]['Title']    = $task2['Title'];
                                    $newTaskArray[$a]['Tasks'][$b]['Milestone'] = 0;
                                    $b++;
                                }
                            }
                            $a++;
                        }     
                    }
                    /*
                     * Pick up miscelaneous tasks here
                     */
                    $newTaskArray[$a]['TaskID']    = '00';
                    $newTaskArray[$a]['Title']     = lang('task_miscellaneous_tasks');
                    $newTaskArray[$a]['Milestone'] = 1;

                    if ($flattenMilestones == TRUE) {
                        $a++;
                        foreach($taskArray as $task) {
                            if ($task['Milestone'] == 0 && $task['ParentTaskID'] == 0) {
                                $newTaskArray[$a]['TaskID']   = $task['TaskID'];
                                $newTaskArray[$a]['Title']    = $task['Title'];
                                $newTaskArray[$a]['Milestone'] = 0;
                                $a++;
                            }
                        }
                    } else {
                        $newTaskArray[$a]['Tasks']     = array();
                        $b=0;
                        foreach($taskArray as $task) {
                            if ($task['Milestone'] == 0 && $task['ParentTaskID'] == 0) {
                                $newTaskArray[$a]['Tasks'][$b]['TaskID']   = $task['TaskID'];
                                $newTaskArray[$a]['Tasks'][$b]['Title']    = $task['Title'];
                                $newTaskArray[$a]['Tasks'][$b]['Milestone'] = 0;
                                $b++;
                            }
                        }
                    }
                }
            } else {
                $b=0;
                $newTaskArray[$a]['TaskID']    = '00';
                $newTaskArray[$a]['Title']     = lang('task_miscellaneous_tasks');
                $newTaskArray[$a]['Milestone'] = 1;

                if ($flattenMilestones == TRUE) {
                    $a++;
                    foreach($taskArray as $task) {
                        $newTaskArray[$a]['TaskID']   = $task['TaskID'];
                        $newTaskArray[$a]['Title']    = $task['Title'];
                        $newTaskArray[$a]['Milestone'] = 0;
                        $a++;
                    }
                } else {
                    $newTaskArray[$a]['Tasks']     = array();
                    foreach($taskArray as $task) {
                        $newTaskArray[$a]['Tasks'][$b]['TaskID']   = $task['TaskID'];
                        $newTaskArray[$a]['Tasks'][$b]['Title']    = $task['Title'];
                        $newTaskArray[$a]['Tasks'][$b]['Milestone'] = 0;
                        $b++;
                    }
                }
            }
        }
        //print_r($newTaskArray);

        if ($renderType == 'json') {
                $output = json_encode($newTaskArray);
        } else {
                $output = $newTaskArray;
        }

        if ($return == 1) {
                return $output;
        } else {
                echo decode_utf8($output);
        }
    }

    function getMilestonesForSelect($projectID,$renderType='json',$return=1) {
		$CI =& get_instance();
        $CI->load->model('tasks/task_view','TaskView',TRUE);
		$milestoneArray = $CI->TaskView->getMilestonesForProject($projectID,0);
		if ($renderType == 'json') {
			$output = json_encode($milestoneArray);
		} else {
			$output = $milestoneArray;
		}

		if ($return == 1) {
			return $output;
		} else {
			echo decode_utf8($output);
		}
	}

	function getTaskStats($projectID,$renderType=NULL,$return=0) {
          $CI =& get_instance();
          $CI->load->model('tasks/task_view','TaskView',TRUE);
		  $statsArray = array();
          $statsArray['HoursCompleted'] = $CI->TaskView->getCompletedHoursForProject($projectID);
		  $statsArray['HoursEstimated'] = $CI->TaskView->getEstimatedTimeForProjectTasks($projectID);

		  $taskArray = $CI->TaskView->getNumberOfTasks($projectID,'project');
		  $statsArray['TotalTasks'] = $taskArray['Tasks'];

		  $taskArray = $CI->TaskView->getNumberOfTasks($projectID,'project','complete');
		  $statsArray['TasksCompleted'] = $taskArray['Tasks'];

		  $taskArray = $CI->TaskView->getNumberOfTasks($projectID,'project','incomplete');
		  $statsArray['TasksIncomplete'] = $taskArray['Tasks'];

		  $taskArray = $CI->TaskView->getNumberOfTasks($projectID,'project','overdue');
		  $statsArray['TasksOverdue'] = $taskArray['Tasks'];
          $statsArray['Labels']       = array(
                                        'TasksComplete'   => lang('task_complete'),
                                        'TasksIncomplete' => lang('task_incomplete'),
                                        'HoursCompleted'  => lang('task_hours_complete'),
                                        'HoursEstimated'  => lang('task_hours_estmated'),
                                        'TasksOverdue'    => lang('task_overdue'),
                                        'TotalTasks'      => lang('task_total')
                                         );
          $statsJSON  = json_encode($statsArray);
          if ($return == 1) {
               return $statsJSON;
          } else {
               echo $statsJSON;
          }
     }

    function getAllUserTasks($renderType='json',$return=0) {
        $CI =& get_instance();
        $CI->load->model('tasks/task_view','TaskView',TRUE);
        $taskArray = $CI->TaskView->getAllUserTasks($CI->session->userdata('userid'));
        $taskArray['ServerTime'] = time();

        if ($return == 0) {
            echo json_encode($taskArray);
        } else {
            return $taskArray;
        }
    }
}
?>
