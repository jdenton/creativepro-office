<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonCalendar {
     /**
	 * getEvents : retrieve events for calendars
	 *
	 * @access	public
	 * @param   string  $dateStart   Start date of date range for which to find events.
     *                               Will also serve as the single date for finding events for 1 day.
     * @param   string  $dateEnd     End date of date range for which to find events
     * @param   string  $range       For which format are we finding events (month,week,day)
     * @param   string  $renderType  Return data as PHP array or JSON
     * @param   bool    $return      1 = return results, 0 = echo results
	 * @return	mixed  PHP array of task data or JSON sting
	 */
     function getEvents($dateStart=NULL,$dateEnd=NULL,$range='month',$accountUserid,$renderType='json',$return=0) {
          $CI =& get_instance();
          $CI->load->model('calendar/Calendar_view','',TRUE);
          $CI->load->model('projects/Project_view','',TRUE);
          $CI->load->model('tasks/Task_view','',TRUE);
          $CI->load->model('finances/Finance_view','',TRUE);

          /*
           * Who are we logged in as?
           */
         $accountUserid = $CI->session->userdata('accountUserid');
         $userid        = $CI->session->userdata('userid');
         $pid           = $CI->session->userdata('pid');

         $clientID = NULL;
         $viewAllEvents   = FALSE;
         $viewOwnEvents   = FALSE;
         $viewAllProjects = TRUE;
         $viewAllTasks    = TRUE;

         $permArray = $CI->session->userdata('permissions');

          if ($CI->session->userdata('userType') == USER_TYPE_CLIENT) {
              $clientID = $userid;
          } elseif ($permArray['calendarViewAll'] == 1) {
              $viewAllEvents = TRUE;
          }
          if ($permArray['calendarViewOwn'] == 1) {
              $viewOwnEvents = TRUE;
          }

          $eventsArray   = $CI->Calendar_view->getEventsForCalendar($dateStart,$dateEnd,$accountUserid,$userid,$pid,$clientID,$viewAllEvents,$viewOwnEvents);
          
          if ($permArray['projectViewOwn'] == 1) {
              $viewAllProjects = FALSE;
          }
          $projectsArray = $CI->Project_view->getProjectsForCalendar($dateStart,$dateEnd,$accountUserid,$viewAllProjects,$pid,$clientID);

          if ($permArray['taskViewOwn'] == 1) {
			 $viewAllTasks = FALSE;
		  }
          $tasksArray    = $CI->Task_view->getTasksForCalendar($dateStart,$dateEnd,$accountUserid,$viewAllTasks,$pid);

          if ($permArray['invoiceViewAll'] == 1) {
            $invoicesArray = $CI->Finance_view->getInvoicesForCalendar($dateStart,$dateEnd,$accountUserid);
          }
          
		  foreach($eventsArray as &$event) {
              $event['Deletable'] = '1';
              $event['Editable']  = '1';
			  $event['IconClass'] = 'icon_calendar_small';

              if ($permArray['calendarViewOwn'] && $event['UseridEntry'] == $CI->session->userdata('userid')) {
                  $event['Deletable'] = '1';
                  $event['Editable']  = '1';
              } else {
                  if ($permArray['calendarDelete'] != 1) {
                      $event['Deletable'] = '0';
                  }

                  if ($permArray['calendarUpdate'] != 1) {
                      $event['Editable'] = '0';
                  }
              }

              $event['Type']      = 'Event';
              $event['Dragable']  = '1';
              $event['StartDateTime']   = convertMySQLToGMT($event['StartDateTime'],'pull','Y-m-d H:i:s');
              $event['EndDateTime']     = convertMySQLToGMT($event['EndDateTime'],'pull','Y-m-d H:i:s');

              /*
               * Let's check our allDay status.
               */
              if (determineAllDay($event['StartDateTime'], $event['EndDateTime']) == true) {
                  $event['AllDay'] = '1';
              } else {
                  $event['AllDay'] = '0';
              }

              $event['StartDateTimeTS'] = standard_date('DATE_ISO8601FC',strtotime($event['StartDateTime']));

              if (!empty($event['EndDateTime']) && $event['EndDateTime'] != '0000-00-00 00:00:00') {
                  $event['EndDateTimeTS']   = standard_date('DATE_ISO8601FC',strtotime($event['EndDateTime']));
              } else {
                  $event['EndDateTimeTS'] = '0000-00-00 00:00:00';
              }
		  }

          foreach($projectsArray as &$project) {
			  $project['IconClass'] = 'icon_project_small';
              $project['CssClass']  = 'clear';
              $project['Deletable'] = '0';
              $project['Editable']  = '0';
              $project['Type']      = 'Project';
              $project['EventID']   = $project['ProjectID'];
              $project['Dragable']  = '0';
              $project['StartDateTime'] = $project['StartDateTime'].' 11:59:00';
              //$project['StartDateTimeTS'] = strtotime($project['StartDateTime']);
              $project['StartDateTimeTS'] = standard_date('DATE_ISO8601FC',strtotime($project['StartDateTime']));
              $project['EndDateTimeTS']   = '';
              $project['AllDay']          = '1';
		  }

          foreach($tasksArray as &$task) {
              $task['Title']      = $task['Title'].', '.$task['ProjectTitle'];
			  $task['IconClass']  = 'icon_tasks_small';
              $task['Deletable']  = '0';
              $task['Editable']   = '0';
              $task['Type']       = 'Task';
              $task['EventID']    = $task['TaskID'];
              $task['Dragable']   = '1';
              $task['StartDateTime'] = convertMySQLToGMT($task['StartDateTime'],'pull','Y-m-d H:i:s');
              $task['EndDateTime']   = convertMySQLToGMT($task['EndDateTime'],'pull','Y-m-d H:i:s');

              $task['Description'] .= '<p class="icon_team_small" style="margin-top: 6px;"><strong>'.lang('common_assigned_to').':</strong> ';
              $assignedString = '';
              foreach($task['TeamMembers'] as $teamMember) {
                  $assignedString .= $teamMember['FullName'].', ';
              }
              $assignedString =  substr($assignedString,0,-2);
              $task['Description'] .= $assignedString.'</p>';

              if (determineAllDay($task['StartDateTime'], $task['EndDateTime']) == true) {
                  $task['AllDay'] = '1';
              } else {
                  $task['AllDay'] = '0';
              }

              if ($task['EndDateTime'] == '0000-00-00') {
                  $task['EndDateTime'] = '0000-00-00 00:00:00';
              }
              //$task['StartDateTimeTS'] = strtotime($task['StartDateTime']);
              //$task['EndDateTimeTS']   = strtotime($task['EndDateTime']);
              $task['StartDateTimeTS'] = standard_date('DATE_ISO8601FC',strtotime($task['StartDateTime']));
              $task['EndDateTimeTS']   = standard_date('DATE_ISO8601FC',strtotime($task['EndDateTime']));
		  }

          $calendarEventsArray = array_merge($eventsArray,$projectsArray,$tasksArray);
          $eventJSON = json_encode($calendarEventsArray);

          //$taskJSON  = CommonTask::renderTasks($taskArray,'json');
          if ($renderType == 'json') {
              if ($return == 1) {
                   return $eventJSON;
              } else {
                   echo $eventJSON;
              }
          } else {
              return $eventsArray;
          }
     }
}
?>