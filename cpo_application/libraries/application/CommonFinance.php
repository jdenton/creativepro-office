<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonFinance {
	/**
	 * getInvoiceDetails : retrieve invoice information for view or edit
	 *
	 * @access	public
	 * @param	int    $invoiceID ID of the invoice for which we want data
	 * @return	mixed  PHP array of invoice data or JSON sting
	 */
	function getInvoiceDetails($invoiceID,$renderType=null,$return=1,$language=NULL,$payPalLink=NULL,$accountUserid=NULL) {
		$CI =& get_instance();
		$CI->load->model('finances/Finance_view','FinanceView',true);
		$CI->load->model('contacts/Contacts','',TRUE);
		$CI->load->model('settings/Settings_update','',TRUE);
		$CI->load->model('accounts/Accounts','',TRUE);

        if (empty($language)) {
            $language = $CI->session->userdata('language');
        }
        if (empty($accountUserid)) {
            $accountUserid = $CI->session->userdata('accountUserid');
        }
        $CI->lang->load('invoice_'.$language,$language);

        /*
        if (!$CI->FinanceView->checkForValidInvoice($invoiceID) || empty($invoiceID)) {
			header('Location: '.site_url('finances/FinanceView'));
		}
        */

		$invoiceArray                = $CI->FinanceView->getInvoiceDetails($invoiceID);
        $invoiceArray['accountInfo'] = $CI->Accounts->getAccountInformation($accountUserid);
        $settingsArray = $CI->Settings_update->getSettings($accountUserid,'','account');

        $currency = $invoiceArray['accountInfo']['Currency'];

        /*
         * Invoice status
         */
        $statusArray = getInvoiceStatus($invoiceArray[0]['Status'],$invoiceArray[0]['Paid'],$invoiceArray[0]['Sent'],$invoiceArray[0]['DateInvoice'],$invoiceArray[0]['DatePaid'],$invoiceArray[0]['DateSent'],$invoiceArray[0]['PaymentDueDate'],$invoiceArray[0]['PaymentDueDays']);
        $invoiceArray[0]['StatusHuman']   = $statusArray[0];
        $invoiceArray[0]['StatusMachine'] = $statusArray[1];

		/*
		 * Format some dates
		 */
		if (!empty($invoiceArray[0]['PaymentDueDate']) && $invoiceArray[0]['PaymentDueDate'] != '0000-00-00') {
			$invoiceArray[0]['DueDate'] = date('M j, Y', strtotime($invoiceArray[0]['PaymentDueDate']));
		} elseif ($invoiceArray[0]['PaymentDueDays']>0) {
			$invoiceArray[0]['DueDate'] = date('M j, Y', strtotime(new_date_from_days($invoiceArray[0]['DateInvoice'],$invoiceArray[0]['PaymentDueDays'])));
		} else {
			$invoiceArray[0]['DueDate'] = NULL;
		}

        $invoiceArray[0]['DateInvoiceOriginal'] = $invoiceArray[0]['DateInvoice'];
		$invoiceDate = date('M j, Y', strtotime($invoiceArray[0]['DateInvoice']));
		$invoiceArray[0]['DateInvoice'] = $invoiceDate;

        $invoiceArray['settings'] = array();
        if (is_array($settingsArray)) {
            foreach($settingsArray as $setting) {
                $invoiceArray['settings'][$setting['Setting']] = $setting['SettingValue'];
            }
        }

        $invoiceArray['payments'] = $this->getInvoicePayments($invoiceID,'array');

		if ($renderType == 'json') {
			if ($return == 1) {
				return json_encode($invoiceArray);
			} else {
				echo json_encode($invoiceArray);
			}
		} elseif ($renderType == 'html') {
			$invoiceHTML = $this->renderInvoiceHTML($invoiceArray,$language,$currency,$payPalLink);
            
			if ($return == 1) {
				return $invoiceHTML;
			} else {
				echo $invoiceHTML;
			}
		} elseif ($renderType == 'email') {
			$invoiceHTML = $this->renderInvoiceEmailHTML($invoiceArray,$language,$currency,$payPalLink);

			if ($return == 1) {
				return $invoiceHTML;
			} else {
				echo $invoiceHTML;
			}
		} else {
			return $invoiceArray;
		}
	}

	/**
	 * getInvoiceDetails : retrieve invoice information for view or edit
	 *
	 * @access	public
	 * @param	int    $invoiceID ID of the invoice for which we want data
	 * @return	mixed  PHP array of invoice data or JSON sting
	 */
	function renderInvoiceHTML($invoiceArray,$language=NULL,$currency=NULL,$payPalLink) {
        $CI =& get_instance();
        
        if (empty($payPalLink)) {
            $payPalLink = 1;
        }
        
        if (empty($language)) {
            $language = $CI->session->userdata('language');
        }
        $CI->lang->load('invoice_'.$language,$language);

        if (empty($currency)) {
            $currencyMark = getCurrencyMark($CI->session->userdata('currency'));
        } else {
            $currencyMark = getCurrencyMark($currency);
        }

        /*
         * Determine due date
         */
        $dueDate = '';

        $dueDate = $invoiceArray[0]['DueDate'];

        /*
         * Determine any messages to be printed on the invoice
         */
        $invMessage = '';
        if (!empty($invoiceArray[0]['InvComments']) && $invoiceArray[0]['IncComments'] == 1) {
            $invMessage .= '<p>'.$invoiceArray[0]['InvComments'].'</p>';
        }

        $CI->load->model('clients/Client_view','',TRUE);
        $clientAuthString = $CI->Client_view->getClientAuthKey($invoiceArray[0]['ClientID']);
        $clientViewURL = BASE_URL.'finances/InvoiceViewExternal/viewInvoiceExternal/'.$clientAuthString.'/'.$invoiceArray[0]['InvoiceID'];

        $invMessage .= '<p>';        
        /*
         * Jan 16, 2013 - removed the stock message about logging into your client area.
         * It was causing more confusion that is was worth. We should inform clients of their
         * ability to login and print invoices from their welcome email.
         */
        if ($invoiceArray[0]['Estimate'] == 1) {
            //$invMessage .= sprintf(lang('finance_estimate_message'),$clientViewURL).' ';
        } else {
            /*
            * Stock message.
            */
            //$invMessage .= sprintf(lang('finance_invoice_message'),$clientViewURL).' ';

            /*
            * Include PayPal message?
            */
            if ($payPalLink == 1) {
                //$invMessage .= sprintf(lang('finance_invoice_message_paypal'),$clientViewURL);
            }
        }    
        $invMessage .= '</p>';

		/*
		 * Vendor information
		 */
		$outString = '';

        $outString .= '<input type="hidden" id="invoiceNumber" value="'.$invoiceArray[0]['InvNo'].'" />';
        $outString .= '<input type="hidden" id="clientCompany" value="'.$invoiceArray[0]['ClientCompany'].'" />';
        $outString .= '<input type="hidden" id="clientEmail" value="'.$invoiceArray[0]['ClientEmail'].'" />';
        $outString .= '<input type="hidden" id="clientID" value="'.$invoiceArray[0]['ClientID'].'" />';
        $outString .= '<input type="hidden" id="clientLanguage" value="'.$invoiceArray[0]['ClientLanguage'].'" />';
        $outString .= '<input type="hidden" id="clientUID" value="'.$invoiceArray[0]['ClientUserid'].'" />';
        $outString .= '<input type="hidden" id="invoiceTotal" value="'.$invoiceArray[0]['InvTotal'].'" />';
        $outString .= '<input type="hidden" id="invoiceVendor" value="'.$invoiceArray['accountInfo']['Company'].'" />';
        $outString .= '<input type="hidden" id="statusHuman" value="'.$invoiceArray[0]['StatusHuman'].'" />';
        $outString .= '<input type="hidden" id="statusMachine" value="'.$invoiceArray[0]['StatusMachine'].'" />';
        $outString .= '<input type="hidden" id="estimate" value="'.$invoiceArray[0]['Estimate'].'" />';

        $outString .= '<div style="width: 495px; float: left;">';
        if (!empty($invoiceArray['settings']['logoLowRes'])) {
            $outString .= '<img src="'.$invoiceArray['settings']['logoLowRes'].'" alt="'.$invoiceArray['accountInfo']['Company'].'" style="margin-bottom: 6px;" />';
        } else {
            $outString .= $invoiceArray['accountInfo']['Company'];
        }
        $outString .= '</div>';
        $outString .= '<div style="width: 190px; float: left;">';
        
		
        if (!empty($invoiceArray['accountInfo']['URL'])) {
            $outString .= '<h3 class="mediumBlue"><a href="'.$invoiceArray['accountInfo']['URL'].'" title="'.$invoiceArray['accountInfo']['Company'].'" target="_blank">'.$invoiceArray['accountInfo']['Company'].'</a></h3>';
        } else {
            $outString .= '<h3 class="mediumBlue">'.$invoiceArray['accountInfo']['Company'].'</h3>';
        }
        $outString .= '<p>';
		if (!empty($invoiceArray['accountInfo']['Address1'])) {
				$outString .= $invoiceArray['accountInfo']['Address1'].'<br />';
		}
		if (!empty($invoiceArray['accountInfo']['Address2'])) {
				$outString .= $invoiceArray['accountInfo']['Address2'].'<br />';
		}
		if (!empty($invoiceArray['accountInfo']['City'])) {
				$outString .= $invoiceArray['accountInfo']['City'].' '.$invoiceArray['accountInfo']['State'].' '.$invoiceArray['accountInfo']['Zip'].' '.$invoiceArray['accountInfo']['Country'].'<br />';
		}
        if (!empty($invoiceArray[0]['TaxID'])) {
				$outString .= lang('finance_tax_id').': '.$invoiceArray[0]['TaxID'].'<br />';
		}
		if (!empty($invoiceArray['accountInfo']['PhoneOffice'])) {
				$outString .= '<strong>'.lang('common_phone').':</strong> '.$invoiceArray['accountInfo']['PhoneOffice'].'<br />';
		}
		if (!empty($invoiceArray['accountInfo']['Email'])) {
				$outString .= '<a href="mailto:'.$invoiceArray['accountInfo']['Email'].'">'.$invoiceArray['accountInfo']['Email'].'</a>';
		}
        $outString .= '</p>';
        $outString .= '</div>';
        $outString .= '<div style="clear: left; height: 24px;"></div>';
        $outString .= '<div style="width: 300px; float: left;">';

		/*
		 * Invoice details (meta information)
         */
		if ($invoiceArray[0]['Estimate'] == 1) {
            $outString .= '<h2>'.lang('finance_estimate_number_abbr').' '.$invoiceArray[0]['InvNo'];
        } else {
            $outString .= '<h2>'.lang('finance_invoice_number_abbr').' '.$invoiceArray[0]['InvNo'];
        }
        
        if (!empty($invoiceArray[0]['Title'])) {
            $outString .= ': '.$invoiceArray[0]['Title'];
        }
        $outString .= '</h2>';
        
        if (!empty($invoiceArray[0]['ClientURL'])) {
            $outString .= '<h3 class="mediumBlue"><a href="'.$invoiceArray[0]['ClientURL'].'">'.$invoiceArray[0]['ClientCompany'].'</a></h3>';
        } else {
            $outString .= '<h3 class="mediumBlue">'.$invoiceArray[0]['ClientCompany'].'</h3>';
        }
        $outString .= '<p>';
		if (!empty($invoiceArray[0]['ClientAddress'])) {
			$outString .= $invoiceArray[0]['ClientAddress'].'<br />';
		}
		if (!empty($invoiceArray[0]['ClientCity'])) {
			$outString .= $invoiceArray[0]['ClientCity'].' '.$invoiceArray[0]['ClientState'].' '.$invoiceArray[0]['ClientZip'].'<br />';
		}
		if (!empty($invoiceArray[0]['ClientPhone'])) {
			$outString .= '<strong>'.lang('common_phone').':</strong> '.$invoiceArray[0]['ClientPhone'];
		}

        $outString .= '</p>';
        $outString .= '</div><div style="float: right; width: 300px;" class="boxBlue radius">';

        $outString .= '<table class="infoDisplay">';
        $outString .= '<tr><td class="leftCol">'.lang('common_date').':</td><td class="rightCol">'.$invoiceArray[0]['DateInvoice'].'</td></tr>';
		$outString .= '<tr><td class="leftCol">'.lang('common_date_due').':</td><td class="rightCol">'.$dueDate.'</td></tr>';
        if (!empty($invoiceArray[0]['PONumber'])) {
            $outString .= '<tr><td class="leftCol">'.lang('finance_invoice_ponumber').':</td><td class="rightCol">'.$invoiceArray[0]['PONumber'].'</td></tr>';
        }
        $outString .= '</table>';
        $outString .= '</div><div style="clear: both;"></div>';

		/*
		 * Invoice items
		 */
		$outString .= '<table class="dataTable" style="margin-top: 6px;">';
		$outString .= '<thead><tr>';
		$outString .= '<th style="width: 50%;">'.lang('finance_invoice_item_description').'</th>';
        $outString .= '<th style="width: 12%;">'.lang('finance_invoice_item_type').'</th>';
        $outString .= '<th style="width: 7%; text-align: right;">'.lang('finance_invoice_item_qty').'</th>';
        $outString .= '<th style="width: 12%; text-align: right;">'.lang('finance_invoice_item_cost').'</th>';
        $outString .= '<th style="width: 7%; text-align: center;">'.lang('finance_invoice_item_tax').'</th>';
        $outString .= '<th style="width: 12%; text-align: right;">'.lang('common_total').'</th>';
		$outString .= '</tr></thead>';
        $outString .= '<tbody>';
        $invoiceItems = $invoiceArray[0]['InvoiceItems'];
        $a=0;
        foreach($invoiceItems as $invoiceItem) {
            $rowClass = 'evenRow';
            if ($a % 2) {
                $rowClass = 'oddRow';
            }

            if ($invoiceItem['Taxable'] == 1) {
                $taxableIcon = '<span class="status_completed" style="margin-left: 20px;"></span>';
            } else {
                $taxableIcon = '';
            }

            $outString .= '<tr class="'.$rowClass.'">';
                $outString .= '<td>'.$invoiceItem['ItemDesc'].'</td>';
                $outString .= '<td>'.lang('finance_invoice_type_'.$invoiceItem['Kind']).'</td>';
                $outString .= '<td style="text-align: right;">'.$invoiceItem['Qty'].'</td>';
                $outString .= '<td style="text-align: right;">'.$currencyMark.$invoiceItem['CostPer'].'</td>';
                $outString .= '<td style="text-align: center;">'.$taxableIcon.'</td>';
                $outString .= '<td style="text-align: right;">'.$currencyMark.$invoiceItem['ItemTotal'].'</td>';
            $outString .= '</tr>';
            $a++;
        }
		
		$outString .= '</tbody>';
        $outString .= '<tfoot style="border-top: 1px solid #ccc;">';
        
        /*
         * Subtotal
         */
        $outString .= '<tr id="rowSubTotal" class="plain">';
        $outString .= '<td colspan="3" style="text-align: right;"><strong>'.lang('common_subtotal').':</strong></td>';
        $outString .= '<td colspan="2"></td>';
        $outString .= '<td style="text-align: right;"><span id="amountSubTotal">'.$currencyMark.$invoiceArray[0]['InvSubTotal'].'</span></td>';
        $outString .= '</tr>';        
        /*
         * Discount
         */
        if ($invoiceArray[0]['DiscountAmount']>0) {
            $outString .= '<tr id="rowDiscount" class="plain">';
            $outString .= '<td colspan="3" style="text-align: right;"><strong>'.lang('finance_invoice_discount').':</strong></td>';
            $outString .= '<td colspan="2"></td>';
            $outString .= '<td class="red" style="text-align: right;"><span id="amountDiscount" class="redText">-'.$currencyMark.$invoiceArray[0]['DiscountAmount'].'</span></td>';
            $outString .= '</tr>';
        }
        /*
		 * Shipping
		 */
        if ($invoiceArray[0]['ShippingAmount']>0) {
            $outString .= '<tr id="rowShipping" class="plain">';
            $outString .= '<td colspan="3" style="text-align: right;"><strong>'.lang('finance_invoice_freight').':</strong></td>';
            $outString .= '<td colspan="2"></td>';
            $outString .= '<td style="text-align: right;"><span id="amountShipping">'.$currencyMark.$invoiceArray[0]['ShippingAmount'].'</span></td>';
            $outString .= '</tr>';
        }
        /*
		 * Tax
		 */
        if ($invoiceArray[0]['TaxAmount']>0) {
            $outString .= '<tr id="rowTax" class="plain">';
            $outString .= '<td colspan="3" style="text-align: right;"><strong>'.lang('finance_invoice_item_tax').':</strong></td>';
            $outString .= '<td colspan="2" style="text-align: right;">'.$invoiceArray[0]['TaxRate'].'%</td>';
            $outString .= '<td style="text-align: right;"><span id="amountTax">'.$currencyMark.$invoiceArray[0]['TaxAmount'].'</span></td>';
            $outString .= '</tr>';
        }

        /*
         * PAYMENTS
         */
        if (count($invoiceArray['payments'])>0) {
            foreach($invoiceArray['payments']['Payments'] as $payment) {
                $outString .= '<tr class="rowPayment plain">';
                $outString .= '<td colspan="3" style="background: #ebf4fd; text-align: right;"><strong>'.lang('finance_payment').':</strong></td>';
                $outString .= '<td colspan="2" style="background: #ebf4fd; text-align: right;">'.$payment['DatePayment'].'</td>';
                $outString .= '<td style="background: #ebf4fd; text-align: right;"><span id="amountTax">'.$currencyMark.$payment['PaymentAmt'].'</span></td>';
                $outString .= '</tr>';
            }
            $grandTotal = $invoiceArray['payments']['TotalDue'];
        } else {
            $grandTotal = $invoiceArray[0]['InvTotal'];
        }

        /*
		 * GRAND TOTAL
		 */
        $totalString = lang('common_invoice_total');
        if ($invoiceArray[0]['Estimate'] == 1) {
            $totalString = lang('common_estimate_total');
        }
        
        $outString .= '<tr>';
        $outString .= '<td class="bigText" colspan="3" style="text-align: right;">'.$totalString.':</td>';
        $outString .= '<td colspan="2"></td>';
        $outString .= '<td id="totalCell" class="bigText" style="text-align: right;">'.$currencyMark.$grandTotal.'</td>';
        $outString .= '</tr>';
        $outString .= '</tfoot>';
        $outString .= '</table>';

        /*
         * Invoice message (I don't think we need this here - only on email invoice below)
         */
        if (!empty($invMessage)) {
            $outString .= '<div style="margin: 12px;"><strong>'.lang('finance_invoice_message_form').'</strong><br />'.$invMessage.'</div>';
        }
		return $outString;
	}

    function renderInvoiceEmailHTML($invoiceArray,$language=NULL,$currency=NULL,$payPalLink) {
        global $emailStyleArray;

		$CI =& get_instance();
        $CI->load->model('clients/Client_view','',TRUE);
        $clientAuthString = $CI->Client_view->getClientAuthKey($invoiceArray[0]['ClientID']);
        $clientViewURL = BASE_URL.'finances/InvoiceViewExternal/viewInvoiceExternal/'.$clientAuthString.'/'.$invoiceArray[0]['InvoiceID'];
        
        if (empty($language)) {
            $language = $CI->session->userdata('language');
        }
        $CI->lang->load('invoice_'.$language,$language);

        if (empty($currency)) {
            $currencyMark = getCurrencyMark($CI->session->userdata('currency'));
        } else {
            $currencyMark = getCurrencyMark($currency);
        }

        /*
         * Set some styles for the HTML email
         */
        $h1 = $emailStyleArray['H1'];
        $h2 = $emailStyleArray['H2'];
        $h3 = $emailStyleArray['H3'];
        $p  = $emailStyleArray['P'];

        /*
         * Determine due date
         */
        $dueDate = '';
        $dueDate = $invoiceArray[0]['DueDate'];

        /*
         * Determine any messages to be printed on the invoice
         */
        $invMessage = '';
        if (!empty($invoiceArray[0]['InvComments']) && $invoiceArray[0]['IncComments'] == 1) {
            $invMessage .= '<p '.$p.'>'.$invoiceArray[0]['InvComments'].'</p>';
        }
        
        $invMessage .= '<p '.$p.'>';        
        if ($invoiceArray[0]['Estimate'] == 1) {
            $itemType = lang('finance_estimate_number_abbr');
            //$invMessage .= sprintf(lang('finance_estimate_message'),$clientViewURL).' ';
        } else {
            $itemType = lang('finance_invoice_number_abbr');
           /*
            * Stock message.
            */
            $invMessage .= sprintf(lang('finance_invoice_message'),$clientViewURL).' ';

           /*
            * Include PayPal message?
            */
            if ($payPalLink == 1) {
                $invMessage .= '<div style="float: left; margin: 30px 12px 0 0;"><a href="'.$clientViewURL.'"><img src="'.INSECURE_URL.'images/payPalInvoiceGraphic.png" alt="Pay this invoice using PayPal." /></a></div>';
                $invMessage .= '<div style="float: left; margin-top: 40px;"><a href="'.$clientViewURL.'">'.sprintf(lang('finance_invoice_message_paypal'),$clientViewURL).'</a></div>';
                $invMessage .= '<div style="clear: both;"></div>';
            }
        }    
        $invMessage .= '</p>';

		/*
		 * Vendor information
		 */
		$outString = '';        
        $outString .= '<table><tr><td style="width: 75%;">';
        if (!empty($invoiceArray['settings']['logoLowRes'])) {
            $outString .= '<img src="'.$invoiceArray['settings']['logoLowRes'].'" alt="'.$CI->session->userdata('userCompany').'" style="margin-bottom: 6px;" />';
        } else {
            $outString .= '<h1 '.$h1.'>'.$CI->session->userdata('userCompany').'</h1>';
        }
        $outString .= '</td><td style="width: 30%;">';
        

        if (!empty($invoiceArray['accountInfo']['URL'])) {
            $outString .= '<h3 '.$h3.'><a href="'.$invoiceArray['accountInfo']['URL'].'" title="'.$invoiceArray['accountInfo']['Company'].'" target="_blank">'.$invoiceArray['accountInfo']['Company'].'</a></h3>';
        } else {
            $outString .= '<h3 '.$h3.'>'.$invoiceArray['accountInfo']['Company'].'</h3>';
        }
        $outString .= '<p '.$p.'>';
		if (!empty($invoiceArray['accountInfo']['Address1'])) {
				$outString .= $invoiceArray['accountInfo']['Address1'].'<br />';
		}
		if (!empty($invoiceArray['accountInfo']['Address2'])) {
				$outString .= $invoiceArray['accountInfo']['Address2'].'<br />';
		}
		if (!empty($invoiceArray['accountInfo']['City'])) {
				$outString .= $invoiceArray['accountInfo']['City'].' '.$invoiceArray['accountInfo']['State'].' '.$invoiceArray['accountInfo']['Zip'].' '.$invoiceArray['accountInfo']['Country'].'<br />';
		}
        if (!empty($invoiceArray[0]['TaxID'])) {
				$outString .= lang('finance_tax_id').': '.$invoiceArray[0]['TaxID'].'<br />';
		}
		if (!empty($invoiceArray['accountInfo']['PhoneOffice'])) {
				$outString .= '<strong>'.lang('common_phone').':</strong> '.$invoiceArray['accountInfo']['PhoneOffice'].'<br />';
		}
		if (!empty($invoiceArray['accountInfo']['Email'])) {
				$outString .= '<a href="mailto:'.$invoiceArray['accountInfo']['Email'].'">'.$invoiceArray['accountInfo']['Email'].'</a>';
		}
        $outString .= '</p>';
        $outString .= '</td></tr></table>';
        $outString .= '<table><tr><td style=width: 40%;">';        

		/*
		 * Invoice details (meta information)
         */

        $outString .= '<h2 '.$h2.'>'.$itemType.' '.$invoiceArray[0]['InvNo'];
        if (!empty($invoiceArray[0]['Title'])) {
            $outString .= ': '.$invoiceArray[0]['Title'];
        }
        $outString .= '</h2>';

        if (!empty($invoiceArray[0]['ClientURL'])) {
            $outString .= '<h3 '.$h3.'><a href="'.$invoiceArray[0]['ClientURL'].'">'.$invoiceArray[0]['ClientCompany'].'</a></h3>';
        } else {
            $outString .= '<h3 '.$h3.'>'.$invoiceArray[0]['ClientCompany'].'</h3>';
        }
        $outString .= '<p '.$p.'>';
		if (!empty($invoiceArray[0]['ClientAddress'])) {
			$outString .= $invoiceArray[0]['ClientAddress'].'<br />';
		}
		if (!empty($invoiceArray[0]['ClientCity'])) {
			$outString .= $invoiceArray[0]['ClientCity'].' '.$invoiceArray[0]['ClientState'].' '.$invoiceArray[0]['ClientZip'].'<br />';
		}
		if (!empty($invoiceArray[0]['ClientPhone'])) {
			$outString .= '<strong>'.lang('common_phone').':</strong> '.$invoiceArray[0]['ClientPhone'];
		}

        $outString .= '</p>';

        $outString .= '<table class="infoDisplay">';
        $outString .= '<tr><td class="left" '.$p.'>'.lang('common_date').':</td><td class="right" '.$p.'>'.$invoiceArray[0]['DateInvoice'].'</td></tr>';
		$outString .= '<tr><td class="left" '.$p.'>'.lang('common_date_due').':</td><td class="right" '.$p.'>'.$dueDate.'</td></tr>';
        if (!empty($invoiceArray[0]['PONumber'])) {
            $outString .= '<tr><td class="left" '.$p.'>'.lang('finance_invoice_ponumber').':</td><td class="right">'.$invoiceArray[0]['PONumber'].'</td></tr>';
        }
        $outString .= '</table>';

        $outString .= '</td></tr></table>';

		/*
		 * Invoice items
		 */
		$outString .= '<table style="margin-top: 6px; border-collapse: collapse; width: 600px;">';
		$outString .= '<thead style="border: 1px solid rgb(196,196,199); background: rgb(231,231,232); height: 25px;"><tr '.$p.'>';
		$outString .= '<th style="width: 50%; text-align: left;">'.lang('finance_invoice_item_description').'</th>';
        $outString .= '<th style="width: 12%;">'.lang('finance_invoice_item_type').'</th>';
        $outString .= '<th style="width: 7%; text-align: right;">'.lang('finance_invoice_item_qty').'</th>';
        $outString .= '<th style="width: 12%; text-align: right;">'.lang('finance_invoice_item_cost').'</th>';
        $outString .= '<th style="width: 7%; text-align: center;">'.lang('finance_invoice_item_tax').'</th>';
        $outString .= '<th style="width: 12%; text-align: right;">'.lang('common_total').'</th>';
		$outString .= '</tr></thead>';

        $invoiceItems = $invoiceArray[0]['InvoiceItems'];
        $a=0;
        foreach($invoiceItems as $invoiceItem) {
            $rowStyle = '';
            if ($a % 2) {
                $rowStyle = 'background: rgb(231,231,232);';
            }

            if ($invoiceItem['Taxable'] == 1) {
                $taxableIcon = '<span class="status_completed" style="margin-left: 20px;"></span>';
            } else {
                $taxableIcon = '';
            }
            $outString .= '<tr style="font-size: 12px; font-family: arial; '.$rowStyle.'">';
                $outString .= '<td style="padding: 3px 0 3px 0;">'.$invoiceItem['ItemDesc'].'</td>';
                $outString .= '<td style="padding: 3px 0 3px 0;">'.lang('finance_invoice_type_'.$invoiceItem['Kind']).'</td>';
                $outString .= '<td style="padding: 3px 0 3px 0; text-align: right;">'.$invoiceItem['Qty'].'</td>';
                $outString .= '<td style="padding: 3px 0 3px 0; text-align: right;">'.$currencyMark.$invoiceItem['CostPer'].'</td>';
                $outString .= '<td style="padding: 3px 0 3px 0; text-align: center;">'.$taxableIcon.'</td>';
                $outString .= '<td style="padding: 3px 0 3px 0; text-align: right;">'.$currencyMark.$invoiceItem['ItemTotal'].'</td>';
            $outString .= '</tr>';
            $a++;
        }
		$outString .= '<tbody>';
		$outString .= '</tbody>';
        $outString .= '<tfoot style="border-top: 2px solid rgb(196,196,199);">';

        /*
         * Subtotal
         */
        $outString .= '<tr style="font-size: 12px; font-family: arial;">';
        $outString .= '<td colspan="3" style="padding: 3px 0 3px 0; text-align: right;"><strong>'.lang('common_subtotal').':</strong></td>';
        $outString .= '<td colspan="2"></td>';
        $outString .= '<td style="padding: 3px 0 3px 0; text-align: right;"><strong>'.$currencyMark.$invoiceArray[0]['InvSubTotal'].'</strong></td>';
        $outString .= '</tr>';
        /*
         * Discount
         */
        if ($invoiceArray[0]['DiscountAmount']>0) {
            $outString .= '<tr '.$p.'>';
            $outString .= '<td colspan="3" style="text-align: right;"><strong>'.lang('finance_invoice_discount').':</strong></td>';
            $outString .= '<td colspan="2"></td>';
            $outString .= '<td class="red" style="text-align: right;"><span id="amountDiscount" style="color: red; font-weight: bold;">-'.$currencyMark.$invoiceArray[0]['DiscountAmount'].'</span></td>';
            $outString .= '</tr>';
        }
        /*
		 * Shipping
		 */
        if ($invoiceArray[0]['ShippingAmount']>0) {
            $outString .= '<tr '.$p.'>';
            $outString .= '<td colspan="3" style="text-align: right;"><strong>'.lang('finance_invoice_freight').':</strong></td>';
            $outString .= '<td colspan="2"></td>';
            $outString .= '<td style="text-align: right;"><strong>'.$currencyMark.$invoiceArray[0]['ShippingAmount'].'</strong></td>';
            $outString .= '</tr>';
        }
        /*
		 * Tax
		 */
        if ($invoiceArray[0]['TaxAmount']>0) {
            $outString .= '<tr '.$p.'>';
            $outString .= '<td colspan="3" style="text-align: right;"><strong>'.lang('finance_invoice_item_tax').':</strong></td>';
            $outString .= '<td colspan="2" style="text-align: right;">'.$invoiceArray[0]['TaxRate'].'%</td>';
            $outString .= '<td style="text-align: right;"><strong>'.$currencyMark.$invoiceArray[0]['TaxAmount'].'</strong></td>';
            $outString .= '</tr>';
        }

        /*
         * PAYMENTS
         */
        if (count($invoiceArray['payments'])>0) {
            foreach($invoiceArray['payments']['Payments'] as $payment) {
                $outString .= '<tr '.$p.'>';
                $outString .= '<td colspan="3" style="background: #ebf4fd; text-align: right;"><strong>'.lang('finance_payment').':</strong></td>';
                $outString .= '<td colspan="2" style="background: #ebf4fd; text-align: right;">'.$payment['DatePayment'].'</td>';
                $outString .= '<td style="background: #ebf4fd; text-align: right;"><span id="amountTax">'.$currencyMark.$payment['PaymentAmt'].'</span></td>';
                $outString .= '</tr>';
            }
            $grandTotal = $invoiceArray['payments']['TotalDue'];
        } else {
            $grandTotal = $invoiceArray[0]['InvTotal'];
        }

        /*
		 * GRAND TOTAL
		 */
        $totalString = lang('common_invoice_total');
        if ($invoiceArray[0]['Estimate'] == 1) {
            $totalString = lang('common_estimate_total');
        }
        
        $outString .= '<tr>';
        $outString .= '<td colspan="5" style="padding: 6px 0 6px 0; font-size: 24px; font-weight: bold; font-family: arial; text-align: right; margin-right: 12px;">'.$totalString.':&nbsp;</td>';
        $outString .= '<td style="padding: 6px 0 6px 0; font-size: 24px; font-weight: bold; font-family: arial; text-align: right;">&nbsp;'.$currencyMark.$grandTotal.'</td>';
        $outString .= '</tr>';
        $outString .= '</tfoot>';
        $outString .= '</table>';

        /*
         * Invoice message
         */
        $outString .= '<div style="border: 1px solid rgb(189,219,249); background: rgb(235,244,253); width: 600px; margin-top: 12px; padding: 6px;">'.$invMessage.'</div>';
        return $outString;
	}

    /**
	 * getExpenseDetails : retrieve expense information for view or edit
	 *
	 * @access	public
	 * @param	int    $expenseID ID of the expense for which we want data
     * @param   string $renderType
     * @param   int    $return
	 * @return	mixed  PHP array of invoice data or JSON sting
	 */
	function getExpenseDetails($expenseID,$renderType=null,$return=1) {
		$CI =& get_instance();
		$CI->load->model('finances/Finance_view_expenses','FinanceViewExpenses',true);
        $CI->load->model('contacts/contacts','Contacts',TRUE);
		$CI->load->model('settings/Settings_update','',TRUE);
        $accountUserid = $CI->session->userdata('accountUserid');

		$CI->lang->load('app',$CI->session->userdata('language'));

        if (!$CI->FinanceViewExpenses->checkForValidExpense($expenseID,$accountUserid) || empty($expenseID)) {
			header('Location: '.site_url('finances/FinanceView'));
		}

		$expenseArray  = $CI->FinanceViewExpenses->getExpenseDetails($expenseID);
        $expenseArray  = $expenseArray[0];
        $settingsArray = $CI->Settings_update->getSettings($CI->session->userdata('accountUserid'),'','account','expense');
        $vendorID = $expenseArray['VendorID'];
        if ($vendorID>0) {
            $expenseArray['Vendor'] = $CI->Contacts->getContactPerson($vendorID);
        } else {
            $expenseArray['Vendor'] = NULL;
        }
        if ($expenseArray['UseridEntry']>0) {
            $expenseArray['Submitter'] = $CI->Contacts->getContactPerson(NULL,$expenseArray['UseridEntry']);
        } else {
            $expenseArray['Submitter'] = '';
        }

		/*
		 * Format some dates
		 */
		if (!empty($expenseArray['DateExpense']) && $expenseArray['DateExpense'] != '0000-00-00') {
			$expenseArray['DateExpenseDisplay'] = date('M j, Y', strtotime($expenseArray['DateExpense']));
		}  else {
			$expenseArray['DateExpenseDisplay'] = NULL;
		}

		$expenseArray['settings'] = array();
        if ($settingsArray != FALSE) {
            foreach($settingsArray as $setting) {
                $expenseArray['settings'][$setting['Setting']] = $setting['SettingValue'];
            }
        }

		if ($renderType == 'json') {
			if ($return == 1) {
				return json_encode($expenseArray);
			} else {
				echo json_encode($expenseArray);
			}
		} elseif ($renderType == 'html') {
			$expenseHTML = $this->renderExpenseHTML($expenseArray);

			if ($return == 1) {
				return $expenseHTML;
			} else {
				echo $expenseHTML;
			}
		} else {
			return $expenseArray;
		}
	}

    function renderExpenseHTML($expenseArray,$language=NULL,$currency=NULL) {
        $CI =& get_instance();

        if (empty($language)) {
            $language = $CI->session->userdata('language');
        }
        $CI->lang->load('invoice_'.$language,$language);

        if (empty($currency)) {
            $currencyMark = getCurrencyMark($CI->session->userdata('currency'));
        } else {
            $currencyMark = getCurrencyMark($currency);
        }

        $outString  = '';
        $outString .= '<input type="hidden" id="clientCompany" value="'.$expenseArray['Company'].'" />';
        $outString .= '<input type="hidden" id="clientEmail" value="'.$expenseArray['Email'].'" />';
        $outString .= '<input type="hidden" id="clientID" value="'.$expenseArray['ClientID'].'" />';
        $outString .= '<input type="hidden" id="clientLanguage" value="'.$expenseArray['Language'].'" />';
        $outString .= '<input type="hidden" id="clientUID" value="'.$expenseArray['ClientUserid'].'" />';

        $outString  .= '<div style="float: left; width: 70%;"><h2 style="margin-bottom: 6px;">'.$expenseArray['Title'].'</h2>';
        if (!empty($expenseArray['MainCat'])) {
            $outString .= '<span class="icon_category_small" style="display: inline-block;">'.$expenseArray['MainCat'].'</span>&nbsp;';
        }
        
        if ($expenseArray['UseridEntry']>0) {
            $outString .= '<span class="subText">'.lang('common_created_by').'</span>&nbsp;';
            $outString .= '<a href="#" class="personLink" id="'.$expenseArray['UseridEntry'].'" personid="'.$expenseArray['Submitter']['PID'].'">'.$expenseArray['Submitter']['NameFirst'].' '.$expenseArray['Submitter']['NameLast'].'</a>';
        }
        $outString .= '</div>';
        $outString .= '<div style="float: right; width: 30%;"><h2 style="text-align: right;">'.$currencyMark.$expenseArray['Amount'].'</h2></div>';
        $outString .= '<div style="clear: both; height: 12px;"></div>';
        $outString .= '<div style="float: left; width: 70%;">';
            $outString .= '<table class="infoDisplay">';
            $outString .= '<tr><td style="margin-right: 10px;"><strong>'.lang('finance_expense_purchase_date').'</strong></td><td>'.$expenseArray['DateExpenseDisplay'].'</td></tr>';
            $outString .= '<tr><td><strong>'.lang('finance_expense_payment_method').'</strong></td><td>'.lang('finance_expense_pay_method_'.$expenseArray['PaymentMethod']).'</td></tr>';
            if (!empty($expenseArray['AccountNumber'])) {
                $outString .= '<tr><td><strong>'.lang('finance_expense_pay_method_acct_no').'</strong></td><td>'.$expenseArray['AccountNumber'].'</td></tr>';
            }
            if (!empty($expenseArray['Markup'])) {
                if ($expenseArray['MarkupType'] == 'percent') {
                    $markup = $expenseArray['Markup'].'%';
                } else {
                    $markup = $currencyMark.$expenseArray['Markup'];
                }
                $outString .= '<tr><td><strong>'.lang('finance_expense_markup').'</strong></td><td>'.$markup.'</td></tr>';
            }

            switch ($expenseArray['Reimbursement']) {
                case 'yes':
                    $reim = lang('finance_expense_reimbursement_yes');
                    break;
                case 'no':
                    $reim = lang('finance_expense_reimbursement_no');
                    break;
                case 'personal':
                    $reim = lang('finance_expense_reimbursement_personal');
                    break;
                default:
                    $reim = '';
            }
            if (!empty($reim)) {
                $outString .= '<tr><td><strong>'.lang('finance_expense_reimbursement').'</strong></td><td>'.$reim.'</td></tr>';
            }
            if ($expenseArray['Reimbursement'] == 'yes' && ($expenseArray['ClientID']>0 || $expenseArray['ProjectID']>0)) {
                $outString .= '<tr><td><strong>'.lang('finance_invoice_bill_to').'</strong></td><td>';
                if ($expenseArray['ClientID']>0) {
                    $outString .= '<h2><a href="'.site_url('clients/ClientDetail/index/'.$expenseArray['ClientID']).'">'.$expenseArray['Company'].'</a></h2>';
                }
                if ($expenseArray['ClientID']>0 && $expenseArray['ProjectID']>0) {
                    $outString .= ' : ';
                }
                if ($expenseArray['ProjectID']>0) {
                    $outString .= '&nbsp;<a href="'.site_url('clients/ProjectDetail/index/'.$expenseArray['ProjectID']).'">'.$expenseArray['ProjectTitle'].'</a>';
                }
                $outString .= '</td></tr>';
            }
            $outString .= '</table>';
            $outString .= '<p style="margin: 12px 0 12px 0;">'.$expenseArray['Comments'].'</p>';
            if (!empty($expenseArray['Tags'])) {
                $outString .= '<p class="icon_tag" style="margin: 12px 0 12px 0;">'.$expenseArray['Tags'].'</p>';
            }
            
        $outString .= '</div>';
        $outString .= '<div style="float: right; width: 30%;">';
            if ($expenseArray['Vendor'] != NULL) {
                if (!empty($expenseArray['Vendor']['AvatarURL'])) {
                    $outString .= '<img class="avatar46" style="padding: 0 6px 6px 0; float: left;" src="'.$expenseArray['Vendor']['AvatarURL'].'" />';
                }
                if (!empty($expenseArray['Vendor']['Company'])) {
                    $outString .= '<p class="fn"><strong>'.$expenseArray['Vendor']['Company'].'</strong></p>';
                }
                if (!empty($expenseArray['Vendor']['NameFirst'])) {
                    $outString .= '<p class="fn">'.$expenseArray['Vendor']['NameFirst'].' '.$expenseArray['Vendor']['NameLast'].'</p>';
                }
                if (!empty($expenseArray['Vendor']['Address'])) {
                    $outString .= '<p>'.$expenseArray['Vendor']['Address'].'</p>';
                }
                if (!empty($expenseArray['Vendor']['City'])) {
                    $outString .= '<p>'.$expenseArray['Vendor']['City'].' '.$expenseArray['Vendor']['State'].' '.$expenseArray['Vendor']['Zip'].'</p>';
                }
                $outString .= '<p style="height: 12px;"></p>';
                foreach($expenseArray['Vendor']['ContactItems'] as $contactItem) {
                    if ($contactItem['ContactType'] == 'phone') {
                        $contactType = 'tel';
                    } else {
                        $contactType = $contactItem['ContactType'];
                    }

                    if ($contactItem['ContactType'] == 'email') {
                        $itemLinkPre = '<a href="mailto:'.$contactItem['ContactData'].'">';
                        $itemLinkAft = '</a>';
                    } elseif ($contactItem['ContactType'] == 'url') {
                        $itemLinkPre = '<a href="'.$contactItem['ContactData'].'">';
                        $itemLinkAft = '</a>';
                    } else {
                        $itemLinkPre = '';
                        $itemLinkAft = '';
                    }

                    $outString .= '<p class="'.$contactType.' icon_'.$contactItem['ContactType'].'" location="'.$contactItem['ContactLocation'].'">';
                    $outString .= $itemLinkPre.$contactItem['ContactData'].$itemLinkAft;
                    $outString .= '&nbsp;<span class="subText">'.$contactItem['ContactLocation'].'</span>';
                    $outString .= '</p>';
                }
            }
        $outString .= '</div>';
        $outString .= '<div style="clear: both;"></div>';

        return $outString;
    }

    function sendInvoiceEmail($emailArray) {
        $CI =& get_instance();
        $CI->load->library('application/CommonEmail');

        $invoiceID      = $emailArray['invoiceID'];
        $invoiceNumber  = $emailArray['invoiceNumber'];
        $toEmailAddress = $emailArray['emailAddress'];
        $message        = $emailArray['invoiceMessage'];
        $language       = $emailArray['language'];
        $payPalLink     = $emailArray['invoicePayPalLink'];
        $sendCopyToUser = $emailArray['sendCopyToUser'];
        $estimate       = $emailArray['estimate'];

        if (empty($language)) {
			$language = $CI->session->userdata('language');
		}
        if (empty($language)) {
            $language = 'en';
        }
        $CI->lang->load('email_'.$language,$language);
        
        if ($estimate == 1) {
            $invoiceSubject = sprintf(lang('email_estimate_subject'),$invoiceNumber,$CI->session->userdata('userCompany'));
        } else {
            $invoiceSubject = sprintf(lang('email_invoice_subject'),$invoiceNumber,$CI->session->userdata('userCompany'));
        }
        
        /*
         * Create invoice HTML message
         */
        $invoiceHTML = CommonFinance::getInvoiceDetails($invoiceID,'email',1,$language,$payPalLink);
        if (!empty($message)) {
            $invoiceMessageHTML  = '<div style="width: 600px; margin-bottom: 12px; border: 1px solid rgb(242,190,35); background: rgb(255,255,210);"><p style="padding: 6px; font-size: 12px; font-family: arial;">';
            $invoiceMessageHTML .= fieldToDB($message,FALSE,TRUE);
            $invoiceMessageHTML .= '</p></div>';
            $invoiceHTML = $invoiceMessageHTML.$invoiceHTML;
        }

        $invoiceSent = $CI->commonemail->sendInvoiceEmail($invoiceSubject,$invoiceHTML,$toEmailAddress,NULL);
        
        if ($sendCopyToUser == 1) {
            /*
             * Send a copy to the logged in user as well.
             */
            $userEmail = $CI->session->userdata('userEmail');
            $invoiceSentToUser = $CI->commonemail->sendInvoiceEmail($invoiceSubject,$invoiceHTML,$userEmail,NULL);
        }
        
        if ($invoiceSent == TRUE) {
            //echo lang('finance_invoice_emailed');
        } else {
            //echo lang('finance_invoice_not_emailed');
        }
    }

    function getInvoicePayments($invoiceID,$renderType='json') {
        $CI =& get_instance();
		$CI->load->model('finances/Finance_view','FinanceView',true);

        if (!empty($invoiceID) && $invoiceID>0) {
            $paymentArray = $CI->FinanceView->getInvoicePayments($invoiceID);
        } else {
            $paymentArray = FALSE;
        }

        if ($paymentArray != FALSE) {
            $a=0;
            $payTotal = 0;
            foreach($paymentArray as $payment) {
                if ($payment['DatePayment'] != '0000-00-00') {
                    $paymentArray[$a]['DatePayment'] = convertMySQLToGMT($payment['DatePayment'],'pull','M j, Y');
                } else {
                    $paymentArray[$a]['DatePayment'] = '';
                }
                $paymentArray[$a]['PaymentType'] = lang('finance_expense_pay_method_'.$payment['PaymentType']);
                if ($payment['PaymentAmount']>0) {
                    $paymentArray[$a]['PaymentAmt'] = number_format($payment['PaymentAmount'],2);
                    $paymentArray[$a]['PaymentAmount'] = getCurrencyMark($CI->session->userdata('currency')).number_format($payment['PaymentAmount'],2);
                } else {
                    $paymentArray[$a]['PaymentAmount'] = 0;
                }
                $payTotal = $payTotal+$payment['PaymentAmount'];
                $a++;
            }
        } else {
            $payTotal = '0';
            $paymentArray = 0;
        }
        $invoiceTotalDue = CommonFinance::getInvoiceTotalDue($invoiceID,$payTotal);
        $payArray['TotalDue'] = number_format($invoiceTotalDue,2);
        $payArray['TotalDueWithCurrency'] = getCurrencyMark($CI->session->userdata('currency')).number_format($invoiceTotalDue,2);
        $payArray['Payments'] = $paymentArray;

        if ($renderType == 'json') {
            return json_encode($payArray);
        } else {
            return $payArray;
        }
    }

    function getInvoiceTotalDue($invoiceID,$paymentsMade=NULL) {
        $CI =& get_instance();
		$CI->load->model('finances/Finance_view','FinanceView',true);
        if ($paymentsMade == NULL) {
            /*
             * Get payments
             */
        } else {
            $invoiceTotal = $CI->FinanceView->getInvoiceTotal($invoiceID);
            $invoiceTotalDue = $invoiceTotal-$paymentsMade;
        }

        return $invoiceTotalDue;
    }

    function copyInvoice($invoiceID,$emailToClient=TRUE) {
        $CI =& get_instance();
		$CI->load->model('finances/Finance_view','FinanceView',true);
        $CI->load->model('finances/Invoice_update','InvoiceUpdate',true);

        /*
         * Get our invoice details
         */
        $invoiceArray = $CI->FinanceView->getInvoiceDetails($invoiceID);
        $invoiceArray = $invoiceArray[0];
        /*
         * Create new invoice number
         */
        $invoiceArrayNew['invoiceNumber'] = '';
        $lastInvoiceNumber = $CI->FinanceView->getLastInvoiceNumber($invoiceArray['Userid']);
        if ($lastInvoiceNumber != false) {
            /*
             * Extract numeric portion of lastInvoiceNumber
             */
            $lastInvoiceNumberNumeric = getNumericPortion($lastInvoiceNumber);
            if ($lastInvoiceNumberNumeric != false) {
                $invoiceArrayNew['invoiceNumber'] = $lastInvoiceNumberNumeric+1;
            }
        } else {
            $invoiceArrayNew['invoiceNumber'] = date('m-d-Y');
        }
        
        /*
         * Calculate new payment due date.
         */
        if ($invoiceArray['PaymentDueDays']>0) {
            /*
             * Calculate payment due date.
             */
            $invoiceArray['PaymentDueDate'] = new_date_from_days(date('Y-m-d'),$invoiceArray['PaymentDueDays']);
        } else {
            /*
             * Invoice is due immediately.
             */
            $invoiceArray['PaymentDueDate'] = date('Y-m-d');
        }

        /*
         * Set up array to send to saveInvoice.
         */
        $invoiceArrayNew['action']          = 'add';
        $invoiceArrayNew['accountUserid']   = $invoiceArray['Userid'];
        $invoiceArrayNew['ToUID']           = $invoiceArray['ToUID'];
        $invoiceArrayNew['ToType']          = $invoiceArray['ToType'];
        $invoiceArrayNew['clientID']        = $invoiceArray['ClientID'];
        $invoiceArrayNew['projectID']       = $invoiceArray['ProjectID'];
        $invoiceArrayNew['invoiceCategory'] = $invoiceArray['CatID'];
        $invoiceArrayNew['invoiceTitle']    = $invoiceArray['Title'];
        $invoiceArrayNew['invoiceMessage']  = $invoiceArray['InvComments'];
        $invoiceArrayNew['printMessage']    = $invoiceArray['IncComments'];
        $invoiceArrayNew['amountSubTotal']  = $invoiceArray['InvSubTotal'];
        $invoiceArrayNew['amountShipping']  = $invoiceArray['ShippingAmount'];
        $invoiceArrayNew['amountDiscount']  = $invoiceArray['DiscountAmount'];
        $invoiceArrayNew['amountTax']       = $invoiceArray['TaxAmount'];
        $invoiceArrayNew['invoiceTotal']    = $invoiceArray['InvTotal'];
        $invoiceArrayNew['invoicePONumber'] = $invoiceArray['PONumber'];
        $invoiceArrayNew['invoiceTaxRate']  = $invoiceArray['TaxRate'];
        $invoiceArrayNew['invoiceTaxName']  = $invoiceArray['TaxName'];
        $invoiceArrayNew['invoiceTaxID']    = $invoiceArray['TaxID'];
        $invoiceArrayNew['invoiceDiscount']       = $invoiceArray['DiscountRate'];
        $invoiceArrayNew['invoicePaymentDue']     = $invoiceArray['PaymentDueDays'];
        $invoiceArrayNew['invoicePaymentDueDate'] = $invoiceArray['PaymentDueDate'];
        $invoiceArrayNew['invoiceLateFee']        = $invoiceArray['LateFeePercent'];
        $invoiceArrayNew['invoiceLateFeeOther']   = $invoiceArray['LateFeeAmount'];
        $invoiceArrayNew['invoiceTags']           = $invoiceArray['Tags'];
        $invoiceArrayNew['invoiceDate']           = date('Y-m-d');
        $invoiceArrayNew['userid']                = $invoiceArray['Userid'];

        /*
         * Invoice items
         */
        $invoiceItems = $invoiceArray['InvoiceItems'];
        $a=0;
        foreach($invoiceItems as $invoiceItem) {
            $invoiceArrayNew['items'][$a]['desc']        = $invoiceItem['ItemDesc'];
            $invoiceArrayNew['items'][$a]['type']        = $invoiceItem['Kind'];
            $invoiceArrayNew['items'][$a]['qty']         = $invoiceItem['Qty'];
            $invoiceArrayNew['items'][$a]['tax']         = $invoiceItem['Taxable'];
            $invoiceArrayNew['items'][$a]['rate']        = $invoiceItem['CostPer'];
            $invoiceArrayNew['items'][$a]['total']       = $invoiceItem['ItemTotal'];
            $invoiceArrayNew['items'][$a]['timesheetID'] = $invoiceItem['TimeSheetID'];
            $invoiceArrayNew['items'][$a]['expenseID']   = $invoiceItem['ExpenseID'];

            $a++;
        }
        $invoiceID = $CI->InvoiceUpdate->saveInvoice($invoiceArrayNew);
        if ($emailToClient == TRUE) {
            /*
             * Email this invoice
             */
            $emailArray['emailAddress']   = $invoiceArray['ClientEmail'];
            $emailArray['invoiceMessage'] = '';
            $emailArray['language']       = $invoiceArray['ClientLanguage'];
            $emailArray['invoiceID']      = $invoiceID;
            $emailArray['invoiceNumber']  = $invoiceArrayNew['invoiceNumber'];
            $emailInvoiceDetails = CommonFinance::sendInvoiceEmail($emailArray);
        }

        return $invoiceID;
    }

    function copyExpense($expenseID) {
        $CI =& get_instance();
		$CI->load->model('finances/Finance_view_expenses','FinanceViewExpenses',true);
        $CI->load->model('finances/Expense_update','ExpenseUpdate',true);

        /*
         * Get our invoice details
         */
        $expenseArray = $CI->FinanceViewExpenses->getExpenseDetails($expenseID);
        $expenseArray = $expenseArray[0];
        print_r($expenseArray);
        
        /*
         * Set up array to send to saveExpense.
         */
        $expenseArrayNew['action']               = 'add';
        $expenseArrayNew['accountUserid']        = $expenseArray['Userid'];
        $expenseArrayNew['projectID']            = $expenseArray['ProjectID'];
        $expenseArrayNew['clientID']             = $expenseArray['ClientID'];
        $expenseArrayNew['expenseCategory']      = $expenseArray['CatID'];
        $expenseArrayNew['expenseVendor']        = $expenseArray['VendorID'];
        $expenseArrayNew['expenseTitle']         = $expenseArray['Title'];
        $expenseArrayNew['expenseDate']          = date('Y-m-d');
        $expenseArrayNew['expensePaymentMethod'] = $expenseArray['PaymentMethod'];
        $expenseArrayNew['expenseAcctNo']        = $expenseArray['AccountNumber'];
        $expenseArrayNew['expenseReimburse']     = $expenseArray['Reimbursement'];
        $expenseArrayNew['expenseAmount']        = $expenseArray['Amount'];
        $expenseArrayNew['expenseMarkup']        = $expenseArray['Markup'];
        $expenseArrayNew['markupType']           = $expenseArray['MarkupType'];
        $expenseArrayNew['expenseNotes']         = $expenseArray['Comments'];
        $expenseArrayNew['expenseTags']          = $expenseArray['Tags'];
        $expenseArrayNew['userid']               = $expenseArray['Userid'];
        $expenseArrayNew['expenseRepeats']       = '';
        print_r($expenseArrayNew);
        
        $expenseID = $CI->ExpenseUpdate->saveExpense($expenseArrayNew);
        return $expenseID;
    }
}
?>