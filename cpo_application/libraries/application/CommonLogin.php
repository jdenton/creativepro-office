<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonLogin {
    function tryLogin($userid=NULL,$password=NULL,$authKey=NULL,$userType=NULL,$apiAuth=NULL,$passwordEncrypted=NULL,$remember=FALSE,$redirect=TRUE,$subdomain=NULL,$gadget=NULL) {
        $CI =& get_instance();
        $CI->load->model('clients/Client_view','ClientView',TRUE);
		$CI->load->model('users/User_login','',TRUE);
        
        $CI->load->library('encrypt');

        include(SITE_CODE_PATH);
		$error = 0;

        // Check for empty fields
		if ( (empty($userid) || empty($password)) && empty($authKey) ) {
			$error = 1;
			$errorType = 'missingField';
		} else {
            if (!empty($authKey)) {
                $loginArrayPersonal = $CI->User_login->checkAuthString($authKey,'personal');
                $loginArrayClient   = $CI->User_login->checkAuthString($authKey,'client');
                
                if ($loginArrayPersonal != FALSE) {
                    
                    if ($apiAuth == 1) {
                        $loginArray = $loginArrayPersonal;
                    } else {
                        CommonLogin::doLogin($loginArrayPersonal,FALSE,$gadget);
                        exit;
                    }    
                } elseif ($loginArrayClient != FALSE) {
                    CommonLogin::doLogin($loginArrayClient,FALSE,$gadget);
                    exit;
                } elseif ($loginArray != FALSE) {
                    // Don't really do anything here.
                } else {
                    $error = 1;
                    $errorType = 'noAccount';
                }
            } else {
                if ($passwordEncrypted != TRUE) {
                    $hashPass = sha1($password.$siteCode);
                } else {
                    $hashPass = $password;
                }
                
                /*
                 * Check for valid login information
                 */
                $loginArray = $CI->User_login->tryLogin($userid,$hashPass);
                if ($loginArray === false) {
                    $error = 1;
                    $errorType = 'noAccount';
                }
            }
		}
        if ($apiAuth == 1) {
            if ($error == 1) {
                echo '{"Error":"1","ErrorMessage":"'.$errorType.'"}';
            } else {
                $authKey = createAuthKey($loginArray['UID']);
                $sessionData = array('logged' => TRUE,'authKey' => $authKey);
                $CI->session->set_userdata($sessionData);
                $permissions = $loginArray['RolePermissions'];
                echo '{"AuthKey":"'.$authKey.'","UserType":"'.$loginArray['User_Type'].'","Permissions":"'.$permissions.'"}';
            }
            exit;
        } elseif ($error != 1) {
            $loginArray['subdomain'] = $subdomain;
            CommonLogin::doLogin($loginArray,$redirect,$gadget);

            /*
             * Do we want to remain logged in for two weeks?
             */
            if ($remember == TRUE) {
                /*
                 * Create GUID, save in cpo_login and store in cookie on client.
                 */
                $guid = random_string('alnum',30);
                $expireDate = date('Y-m-d',time()+REMEMBER_ME_EXPIRE);
                $CI->User_login->saveRememberMeGUID($loginArray['UID'],$expireDate,$guid);
                $cookieArray = array(
                               'name'   => 'rememberMe',
                               'value'  => $guid,
                               'expire' => REMEMBER_ME_EXPIRE,
                               'domain' => '.mycpohq.com'
                            );
                set_cookie($cookieArray);
            }
        } else {
            header('Location: '.site_url('login/index/'.$errorType.'/'.$subdomain));
        }
    }

    function doLogin($loginArray,$redirect=FALSE,$gadget=NULL) {
        $CI =& get_instance();
        $CI->load->model('clients/Client_view','ClientView',TRUE);
        $CI->load->model('trash/Trash','',TRUE);
        $CI->load->model('accounts/Accounts','',TRUE);
        $CI->load->model('contacts/Contacts','',TRUE);
        $CI->load->model('settings/Settings_update','',TRUE);
        $CI->load->model('users/User_login','',TRUE);
        $CI->load->model('users/User_information','',TRUE);
		$CI->load->model('team/Team','',TRUE);
        $CI->load->model('administration/Admin_alerts','',TRUE);

        $CI->load->library('application/CommonContacts');
        $CI->load->library('application/CommonWidgets');
        $CI->load->library('application/CommonTrash');

        /*
         * Let's start the login process
         * CLIENT LOGIN HERE!!!!!
         */
        if ($loginArray['User_Type'] == USER_TYPE_CLIENT) {
            $accountUserid = $loginArray['UseridAccount'];
            $userName      = $loginArray['Userid'];
            $userid        = $loginArray['UID'];
            $profileArray = $CI->ClientView->getClientInfoForLogin($userid,$accountUserid);

            $clientCompany = stripslashes($profileArray['Company']);
            $clientID      = $profileArray['ClientID'];
            $language      = $profileArray['Language'];
            $gmtOffset     = $profileArray['Timezone'];
            $CI->lang->load('app',$language);            

			/*
			 * Get some account owner information
			 */
			$acctOwnerArray = $CI->Accounts->getAccountInformation($accountUserid,1);
            $sessionData = array(
                'pid'           => 'client',
                'logged'        => TRUE,
                'userCompany'   => stripslashes($acctOwnerArray['Company']),
                'userDir'       => $acctOwnerArray['UserDir'],
                'clientCompany' => $clientCompany,
                'clientID'      => $clientID,
                'userType'      => USER_TYPE_CLIENT,
                'fullName'      => $clientCompany,
                'roleText'      => lang('menu_client'),
                'currency'      => $acctOwnerArray['Currency'],
                'language'      => $language,
                'accountUserid' => $accountUserid,
                'userid'        => $userid,
                'userName'      => $userName,
                'accountLevel'  => $acctOwnerArray['AccountLevel'],
                'gmtOffset'     => $gmtOffset,
                'password'      => $loginArray['Password'],
                'trashTotal'    => $CI->Trash->getNumberOfItemsInTrash(),
                'permissions'   => createPermissionsArray(PERM_CLIENT),
                'alerts'        => FALSE,
                'subdomain'     => $loginArray['subdomain']
			 );

             /*
              * 2. Some account settings
              */
             $settingsArray = $CI->Settings_update->getSettings($accountUserid,NULL,'account','user');
             if ($settingsArray != FALSE) {
                 foreach($settingsArray as $setting) {
                     $sessionData[$setting['Setting']] = $setting['SettingValue'];
                 }
             }

             /*
              * 3. Some client settings
              */
             $settingsMemberArray = $CI->Settings_update->getSettings($accountUserid,NULL,'client','user');
             $a=0;

             if ($settingsMemberArray != FALSE) {
                 foreach($settingsMemberArray as $settingMember) {
                     $sessionData[$settingMember['Setting']] = $settingMember['SettingValue'];
                     $a++;
                 }
             }

             /*
			  * Write data to session
			  */
			  $CI->session->set_userdata($sessionData);
              if ($redirect == TRUE) {
                 if (!empty($loginArray['subdomain'])) {
                     header('Location: http://'.$loginArray['subdomain'].'.'.SITE_DOMAIN.'/dashboard/DashboardClients');
                 } else {
                     header('Location: '.site_url('dashboard/DashboardClients'));
                 }
              }
        } else {
            $password      = $loginArray['Password'];
            $userType      = $loginArray['User_Type'];
            $roleID        = $loginArray['RoleID'];
            $roleName      = $loginArray['RoleName'];
            $permissions   = createPermissionsArray($loginArray['RolePermissions']);
            if ($permissions === FALSE) {
                header('Location: '.site_url('login/index/roleMissing/'.$loginArray['subdomain']));
                exit;
            }
            $assignment    = $loginArray['Assignment'];
            $accountUserid = $loginArray['UseridAccount'];
            $userid        = $loginArray['UID'];
            $userName      = $loginArray['Userid'];
            $persAuthString= $loginArray['AuthString'];

            $accountArray = $CI->Accounts->getAccountInformation($accountUserid,1);
		
            $userCompany       = stripslashes($accountArray['Company']);
            $userDir           = $accountArray['UserDir'];
            $accountLevel      = $accountArray['AccountLevel'];
            $accountNumber     = $accountArray['Account_No'];
            $currency          = $accountArray['Currency'];
            $accountAuthString = $accountArray['AuthString'];
            $dateSignup        = $accountArray['DateSignup'];
            $paymentProfileID  = $accountArray['PaymentProfileID'];
            $companyEmail      = $accountArray['Email'];
            $companyPhone      = $accountArray['PhoneOffice'];
            $companyFAX        = $accountArray['PhoneFax'];
            $companyURL        = $accountArray['URL'];
            $isPaid = FALSE;
            if ($accountArray['IsPaid'] == 1) {
                $isPaid = TRUE;
            }

            $userInformationArray = $CI->Contacts->getContactPerson(null,$userid);

            $pid       = $userInformationArray['PID'];
            $country   = $userInformationArray['Country'];
            $gmtOffset = $userInformationArray['Timezone'];
            $dstUser   = $userInformationArray['DaylightSavingsTime'];
            $avatar    = $userInformationArray['AvatarURL'];
            $dst = FALSE;
            if ($dstUser == 1) {
                $dst = TRUE;
            }
            $language  = $userInformationArray['Language'];
            $firstName = $userInformationArray['NameFirst'];
            $fullName  = $userInformationArray['NameFirst'].' '.$userInformationArray['NameLast'];

            /*
             * Find an email address for this user
             */
            $emailArray = array();
            $emailArray[0] = '';
            $a=0;
            foreach($userInformationArray['ContactItems'] as $contactItem) {
                if ($contactItem['ContactType'] == 'email') {
                    $emailArray[$a] = $contactItem['ContactData'];
                    $a++;
                }
            }
            $userEmail = $emailArray[0];

            if (empty($language)) {
                $language = 'en';
            }

            /*
             * Check and see if we should display any alerts.
             * First look for a noCPOAlerts cookie.
             */
             if (isset($_COOKIE['noCPOAlerts'])) {
                 $offDate = $_COOKIE['noCPOAlerts'];
             } else {
                 $offDate = '';
             }
            $alertsArray = $CI->Admin_alerts->getAlerts($offDate);
            
            $sessionData = array(
                'pid'                 => $pid,
                'userid'              => $userid,
                'userName'            => $userName,
                'accountUserid'       => $accountUserid,
                'password'            => $password,
                'permissions'         => $permissions,
                'userType'            => $userType,
                'logged'              => TRUE,
                'role'                => $roleID,
                'roleText'            => $roleName,
                'assignment'          => $assignment,
                'firstName'           => $firstName,
                'fullName'            => $fullName,
                'userCompany'         => $userCompany,
                'userDir'             => $userDir,                
                'country'             => $country,
                'currency'            => $currency,
                'gmtOffset'           => $gmtOffset,
                'daylightSavingsTime' => $dst,
                'timezone'            => $userInformationArray['Timezone'],
                'language'            => $language,
                'userEmail'           => $userEmail,                
                'persAuthString'      => $persAuthString,
                'accountAuthString'   => $accountAuthString,
                'avatarMember'        => $avatar,
                'importGUID'          => '',
                'companyEmail'        => $companyEmail,
                'companyPhone'        => $companyPhone,
                'companyFAX'          => $companyFAX,
                'companyURL'          => $companyURL,

                'accountLevel'        => $accountLevel,
                'accountNumber'       => $accountNumber,
                'isPaid'              => $isPaid,
                'dateSignup'          => $dateSignup,
                'paymentProfileID'    => $paymentProfileID,
                'subdomain'           => $loginArray['subdomain'],

                'alerts'              => $alertsArray,
                'gadget'              => $gadget
            );
            
            /*
             * Get footer widgets
             */
            $sessionData['footerWidgetArray'] = $CI->commonwidgets->getActiveFooterWidgets($userid);

            /*
             * Localization variables: timezone and currency
             */
            if (empty($currency)) {
                $currency = 'USD';
                $sessionData['currencyHTML'] = '$';
            } else {
                $currencyMark = getCurrencyMark($currency);
                $sessionData['currencyHTML'] = $currencyMark;
            }

            /*
             * 2. Some account settings
             */
            $settingsArray = $CI->Settings_update->getSettings($accountUserid,NULL,'account','user');
            //print_r($settingsArray);
            if ($settingsArray != FALSE) {
                foreach($settingsArray as $setting) {
                    $sessionData[$setting['Setting']] = $setting['SettingValue'];
                }
            }

            /*
             * 3. Some member settings
             */
            $settingsMemberArray = $CI->Settings_update->getSettings($accountUserid,NULL,'member','user');
            $a=0;

            if ($settingsMemberArray != FALSE) {
                foreach($settingsMemberArray as $settingMember) {
                    $sessionData[$settingMember['Setting']] = $settingMember['SettingValue'];
                    $a++;
                }
            }

            /*
             * Write data to session
             */
            $CI->session->set_userdata($sessionData);

            /*
             * Write trash total to session
             */
            $trashTotal = $CI->commontrash->countItemsInTrash(TRUE);
            $CI->session->set_userdata('trashTotal',$trashTotal);

            /*
             * Update login stats for this user
             */
            $CI->User_login->updateLoginStats($userid);

            /*
             ****** Redirect user *****
             * Are we coming from a third party widget (Gmail,iGoogle)?
             */
            if ( $gadget != '0' && $gadget != NULL ) {
                if ($gadget == 'gmail') {
                    header('Location: '.site_url('dashboard/DashboardOwners/gmailGadget'));
                } elseif ($gadget == 'igoogle') {
                    header('Location: '.site_url('dashboard/DashboardOwners/igoogleGadget'));
                }
            } else {
                /*
                 * Team member
                 */
                if ($redirect == TRUE) {
                    if (!empty($loginArray['subdomain'])) {
                        header('Location: http://'.$loginArray['subdomain'].'.'.SITE_DOMAIN.'/dashboard/DashboardOwners');
                    } else {
                        header('Location: '.site_url('dashboard/DashboardOwners'));
                    }   
                }
            }
        }
    }
}
?>