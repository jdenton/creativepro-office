<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CommonEmail {
    /**
    * sendEmail : site-wide send email method
    *
    * @access	public
    * @param	string       $message Message to be sent
    * @param   $recipUID    UID of recipient, this is the column UID from cpo_login NOT memberID
    * @param   $recipType   type of recipient: team, client, contact
    * @param   $messageType what type of email are we sending? Message, task notification, etc.
    * @return	void         For now
    */
    function sendEmail($message,$recipID,$recipType=NULL,$subject=NULL,$messageType=NULL,$fromAdmin=NULL) {
        $CI =& get_instance();
        $CI->load->model('settings/Settings_update','',true);
        $CI->load->model('contacts/Contacts','',true);
        $CI->load->model('clients/Client_view','',true);
        $CI->load->library('email');

        if ($fromAdmin == TRUE) {
            $fromName  = EMAIL_SUPPORT_FROM;
            $fromEmail = MAIL_NOBODY;
        } else {
            $fromName  = $CI->session->userdata('fullName');
            $fromEmail = $CI->session->userdata('userEmail');
        }

        $fromName  = EMAIL_SUPPORT_FROM;
        $fromEmail = MAIL_NOBODY;

		/*
		 * Check email message settings for this recipient.
		 * If they aren't going to receive an email, then don't
		 * proceed with the rest of this.
         *
         * But, if a message type isn't specified, go ahead
         * and send the messge.
		 */
        $getsEmail = false;
        if (!empty($messageType)) {
            $emailSettingsArray = $CI->Settings_update->getSettings($recipID,$messageType);
            if ($emailSettingsArray == false || $emailSettingsArray['SettingValue'] == 1) {
                /*
                 * There is no setting for this message type yet. Default to
                 * sending the message until told not to by the user.
                 * Or, the user has told the system to send the message type.
                 */
                $getsEmail = true;
            }
        } else {
            $getsEmail = true;
        }

		if ($getsEmail == true) {
            /*
			 * Get recipient name and email address
			 */
			switch ($recipType) {
				case 'team':
                    /*
                     * Commented out to make calendar team member assignment
                     * work.  We may regret this.
                     */
					$recipArray = $CI->Contacts->getContactPerson($recipID);                    
                    $recipName  = $recipArray['NameFirst'].' '.$recipArray['NameLast'];

                    /*
                     * Now find email address(es)
                     */
                    $emailArray = array();
					$emailArray[0] = '';
                    $a=0;
                    foreach($recipArray['ContactItems'] as $contactItem) {
                        if ($contactItem['ContactType'] == 'email') {
                            $emailArray[$a] = $contactItem['ContactData'];
                            $a++;
                        }
                    }
					$recipEmail = $emailArray[0];
					break;
                case 'owner':
                    /*
                     * Commented out to make calendar team member assignment
                     * work.  We may regret this.
                     */
					$recipArray = $CI->Contacts->getContactPerson($recipID);
                    $recipName  = $recipArray['NameFirst'].' '.$recipArray['NameLast'];

                    /*
                     * Now find email address(es)
                     */
                    $emailArray = array();
					$emailArray[0] = '';
                    $a=0;
                    foreach($recipArray['ContactItems'] as $contactItem) {
                        if ($contactItem['ContactType'] == 'email') {
                            $emailArray[$a] = $contactItem['ContactData'];
                            $a++;
                        }
                    }
					$recipEmail = $emailArray[0];
					break;
				case 'client':
                    $recipArray = $CI->Client_view->getClientInformation(NULL,$recipID);
                    $recipName  = $recipArray['Company'];
					$recipEmail = $recipArray['Email'];
					break;
				case 'contact':
                    $recipArray = $CI->Contacts->getContactPerson($recipID);
                    $recipName  = $recipArray['NameFirst'].' '.$recipArray['NameLast'];

                    /*
                     * Now find email address(es)
                     */
                    $emailArray = array();
					$emailArray[0] = '';
                    $a=0;
                    foreach($recipArray['ContactItems'] as $contactItem) {
                        if ($contactItem['ContactType'] == 'email') {
                            $emailArray[$a] = $contactItem['ContactData'];
                            $a++;
                        }
                    }
                    $recipEmail = $emailArray[0];
					break;
			}
			/*
			 * Format subject
			 */
			if (empty($subject)) {
				$subject = $fromName;
            }
			/*
			 * Send the email message!!!
			 */
			if (!empty($recipEmail)) {
                $config['wordwrap'] = FALSE;
                $CI->email->initialize($config);

                $CI->email->from($fromEmail,$fromName);
                $CI->email->to($recipEmail);
                $CI->email->subject($subject);
                $CI->email->message($message);
                $CI->email->send();
            }

            //echo $CI->email->print_debugger();
        }
    }

    function sendInvoiceEmail($invoiceSubject,$invoiceMessage,$recipEmail=NULL,$recipUID=NULL,$attach=NULL) {
        $CI =& get_instance();
		$CI->load->model('settings/Settings_update','',true);
        $CI->load->model('contacts/Contacts','',true);
        $CI->load->library('email');
        $CI->load->helper('templates');

        $config['mailtype'] = 'html';
        $CI->email->initialize($config);

		$fromName  = $CI->session->userdata('fullName');
		$fromEmail = $CI->session->userdata('userEmail');

        if (empty($fromName)) {
            $fromName = SITE_TITLE;
        }
        if (empty($fromEmail)) {
            $fromEmail = MAIL_NOBODY;
        }
        /*
         * Needed to change this to an address on the domain
         * otherwise, message goes to spam folder.
         */
        $fromEmail = MAIL_NOBODY;

        $messageHTML  = htmlEmailHeader($invoiceSubject);
        $messageHTML .= $invoiceMessage;
        $messageHTML .= htmlEmailFooter();

        $CI->email->from($fromEmail,$fromName);
        $CI->email->to($recipEmail);
        $CI->email->subject($invoiceSubject);
        $CI->email->message($messageHTML);
        if ($CI->email->send()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function sendPaymentEmail($subject,$message,$recipEmail=NULL,$recipUID=NULL,$attach=NULL) {
        $CI =& get_instance();
		$CI->load->model('settings/Settings_update','',true);
        $CI->load->model('contacts/Contacts','',true);
        $CI->load->library('email');

        $config['mailtype'] = 'html';
        $CI->email->initialize($config);

		$messageHTML  = htmlEmailHeaderCPO($subject);
        $messageHTML .= $message;
        $messageHTML .= htmlEmailFooterCPO();

        $CI->email->from(MAIL_SUPPORT,EMAIL_SUPPORT_FROM);
        $CI->email->to($recipEmail);
        $CI->email->subject($subject);
        $CI->email->message($messageHTML);
        if ($CI->email->send()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function sendNewAccountEmail($subject,$message,$recipEmail=NULL) {
        $CI =& get_instance();
		$CI->load->library('email');

        $config['mailtype'] = 'html';
        $CI->email->initialize($config);

		$messageHTML  = htmlEmailHeaderCPO($subject);
        $messageHTML .= $message;
        $messageHTML .= htmlEmailFooterCPO();

        $CI->email->from(MAIL_NOBODY,EMAIL_SUPPORT_FROM);
        $CI->email->to($recipEmail);
        $CI->email->subject($subject);
        $CI->email->message($messageHTML);
        if ($CI->email->send()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function sendExportEmail($subject,$message,$recipEmail=NULL) {
        $CI =& get_instance();
        $CI->load->library('email');

        $config['mailtype'] = 'html';
        $CI->email->initialize($config);

        $messageHTML = $message;

        $CI->email->from(MAIL_NOBODY,EMAIL_SUPPORT_FROM);
        $CI->email->to($recipEmail);
        $CI->email->subject($subject);
        $CI->email->message($messageHTML);
        if ($CI->email->send()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /*
     * createSystemMessage
     * Generates a system message in the recipients' language.
     * TODO: Create feature that allows user to customize all
     * system messages.
     *
     * @access public
	 * @param  string $messageType Type of system message we're going to send
     *                             (newTeamMember....)
	 * @return array  $messageArray Message and Subject in recipients' language.
     */
    function createSystemMessage($messageType,$recipLanguage=NULL) {
        $CI =& get_instance();
        $messageArray = array();
        if (empty($recipLanguage) || $recipLanguage == NULL || $recipLanguage == 'null') {
            $recipLanguage = $CI->session->userdata('language');
        }
        $CI->lang->load('email_'.$recipLanguage,$recipLanguage);
        switch ($messageType) {
            case 'newTeamMember':
                $messageArray['subject'] = lang('email_new_team_member_subject');
                $messageArray['message'] = lang('email_new_team_member_message');
                break;
            case 'taskMessage':
                $messageArray['subject'] = lang('email_task_subject');
                $messageArray['message'] = lang('email_task_message');
                break;
            case 'taskNew':
                $messageArray['subject'] = lang('email_task_new_subject');
                $messageArray['message'] = lang('email_task_new_message');
                break;
            case 'taskUpdate':
                $messageArray['subject'] = lang('email_task_edit_subject');
                $messageArray['message'] = lang('email_task_edit_message');
                break;
            case 'taskOverdue':
                $messageArray['subject'] = lang('email_task_overdue_subject');
                $messageArray['message'] = lang('email_task_overdue_message');
                break;
            case 'taskUpcoming':
                $messageArray['subject'] = lang('email_task_upcoming_subject');
                $messageArray['message'] = lang('email_task_upcoming_message');
                break;
            case 'projectMessage':
                $messageArray['subject'] = lang('email_project_subject');
                $messageArray['message'] = lang('email_project_message');
                break;
            case 'calendarNew':
                $messageArray['subject'] = lang('email_calendar_subject');
                $messageArray['message'] = lang('email_calendar_message');
                break;
            case 'clientNew':
                $messageArray['subject'] = lang('email_new_client_subject');
                $messageArray['message'] = lang('email_new_client_message');
                break;
            case 'clientUpdateUserid':
                $messageArray['subject'] = lang('email_update_client_userid_subject');
                $messageArray['message'] = lang('email_update_client_userid_message');
                break;
            case 'clientMessage':
                $messageArray['subject'] = lang('email_client_subject');
                $messageArray['message'] = lang('email_client_message');
                break;
            case 'invoiceMessage':
                $messageArray['subject'] = lang('email_invoice_message_subject');
                $messageArray['message'] = lang('email_invoice_message');
                break;
            case 'expenseMessage':
                $messageArray['subject'] = lang('email_expense_subject');
                $messageArray['message'] = lang('email_expense_message');
                break;
            case 'resetPassword':
                $messageArray['subject'] = lang('email_resetPassword_subject');
                $messageArray['message'] = lang('email_resetPassword_message');
                break;
            case 'assignToProject':
                $messageArray['subject'] = lang('email_assignToProject_subject');
                $messageArray['message'] = lang('email_assignToProject_message');
                break;
            case 'eventNew':
                $messageArray['subject'] = lang('email_event_new_subject');
                $messageArray['message'] = lang('email_event_new_message');
                break;
            case 'eventUpdate':
                $messageArray['subject'] = lang('email_event_edit_subject');
                $messageArray['message'] = lang('email_event_edit_message');
                break;
            case 'fileMessage':
                $messageArray['subject'] = lang('email_file_subject');
                $messageArray['message'] = lang('email_file_message');
                break;
            case 'attachedFileMessage':
                $messageArray['subject'] = lang('email_file_subject');
                $messageArray['message'] = lang('email_file_message');
                break;
            case 'messageFooter':
                $messageArray['message'] = lang('email_footer');
                break;
        }
        return $messageArray;
    }

    function sendEmailToAdmin($message,$subject,$fromName=NULL,$fromAddress=NULL) {
		$CI =& get_instance();
		$CI->load->library('email');

        if ($fromName == NULL) {
            $fromName = EMAIL_SUPPORT_FROM;
        }
        /*
        if ($fromAddress == NULL) {
            $fromAddress = MAIL_SUPPORT;
        }
        */
        
        /*
         * Jan 20, 2012: For some reason, sending FROM user email stopped working.
         * So we changed FROM address to mailsender@mycpohq.com.
         */
        $fromAddress = MAIL_NOBODY;
        $CI->email->from($fromAddress,$fromName);
		$CI->email->to(MAIL_ADMIN);
		$CI->email->subject($subject);
		$CI->email->message($message);
		$CI->email->send();
        echo $CI->email->print_debugger();
    }
    
     
    function sendContactEmail($post) {
        $CI =& get_instance();
		$CI->load->library('email');
        
        $subject = $post['contactProblemType'];        
        $message  = $post['contactProblemDescription'].Chr(10).'::-----::'.Chr(10);
        $message .= 'Browser: '.$CI->agent->browser().Chr(10);
        $message .= 'Version: '.$CI->agent->version().Chr(10);
        $message .= 'Platform: '.$CI->agent->platform();

        $email   = $post['contactEmail'];
        if (empty($email)) {
            $email = 'CreativePro Office Contact Form';
        }
        $CI->email->from($email,$email);
		$CI->email->to(MAIL_SUPPORT);
		$CI->email->subject($subject);
		$CI->email->message($message);
		$CI->email->send();
    }

    function sendResetPasswordEmail($message,$subject,$recipEmail) {
        $CI =& get_instance();
		$CI->load->library('email');
        $CI->email->from(MAIL_NOBODY,EMAIL_SUPPORT_FROM);
		$CI->email->to($recipEmail);
		$CI->email->subject($subject);
		$CI->email->message($message);
		$CI->email->send();
    }

    function sendWelcomeEmail($message,$subject,$recipEmail) {
        $CI =& get_instance();
		$CI->load->library('email');

        $CI->email->from(MAIL_SUPPORT,EMAIL_SUPPORT_FROM);
		$CI->email->to($recipEmail);
		$CI->email->subject($subject);
		$CI->email->message($message);
		$CI->email->send();
    }
    
    function sendPaymentReceiptEmail($message,$email,$subject) {
        $CI =& get_instance();
		$CI->load->library('email');
        $CI->email->from(MAIL_SUPPORT,EMAIL_SUPPORT_FROM);
		$CI->email->to($email);
		$CI->email->subject($subject);
		$CI->email->message($message);
		$CI->email->send();
    }
}
?>