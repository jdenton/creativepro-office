<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FileManagerS3 {
    function __construct() {
        include(APPPATH.'libraries/s3/S3.php');
    }
    
    /**
    * uploadFileHandler : upload files to Amazon S3 service
    *
    * @access	public
    * @param  array  $fileArray Array of files to be deleted
    * @return void
    */
    function uploadFileHandler() {
        $CI =& get_instance();
        $CI->load->model('files/File_maintenance');
        $CI->load->helper('file');
        $CI->load->helper('download');

        // Check the upload
        $fileNameTmp = $_FILES['file']['tmp_name'];
        $fileName    = $_FILES['file']['name'];
        $fileType    = $_FILES['file']['type'];
        $fileSize    = $_FILES['file']['size'];
        $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);

        /*
         * Check for duplicate filename
         */
        $fileName = $CI->File_maintenance->checkForDuplicateFileName($fileName);
        $fileName = cleanString($fileName);
        /*
         * Try creating a 'folder' and then putting the file into the folder
         */
        $folderName = $CI->session->userdata('userDir');
        $s3->putObject($folderName.'_$folder$', AWS_BUCKET_NAME, $folderName.'_$folder$', S3::ACL_PRIVATE);
        // Move the file
        
        if ($s3->putObjectFile($fileNameTmp, AWS_BUCKET_NAME, $folderName.'/'.$fileName, S3::ACL_PRIVATE,array("Content-Disposition" => "attachment"),"application/octet-stream")) {
            $status = 'Success';
			/*
			 * Enter file information into database
			 */
			$fileArray['FileNameActual']    = $fileName;
			$fileArray['FileSize']          = $fileSize;
			$fileArray['FileType']          = $fileType;
			$fileArray['action']            = 'add';
            $fileArray['ParentID']          = $CI->session->userdata('fileTreeNodeID');
            if (empty($fileArray['ParentID'])) {
                $fileArray['ParentID'] = 0;
            }
			$fileArray['accountUserid']     = $CI->session->userdata('accountUserid');
			$fileArray['userid']            = $CI->session->userdata('userid');

            $parentID = $fileArray['ParentID'];
			$fileID = $CI->File_maintenance->insertFileData($fileArray);
        } else {
			$fileID = 0;
            $folderName = 0;
            $parentID = 0;
		}
		unset($s3);
        return array($fileID,$folderName,$parentID);
     }

	 /**
      * deleteItemFromS3 : delete files from S3 bucket
	  *
      * @access	public
	  * @param  array  $fileArray Array of files to be deleted
      * @return void
      */
	 function deleteItemFromS3($fileArray,$folderName=NULL) {
		$CI =& get_instance();
        $CI->load->model('files/File_maintenance');
        include(APPPATH.'libraries/s3/S3.php');
		$CI->load->helper('file');
		$CI->load->helper('download');
        if (empty($folderName)) {
            $folderName = $CI->session->userdata('userDir');
        }

		if (is_array($fileArray)) {
			
			$s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
			$a=0;
			foreach($fileArray as $file) {
                $fileName = $file['FileNameActual'];
				//echo $folderName.'/'.$fileName;
				$s3->deleteObject(AWS_BUCKET_NAME, $folderName.'/'.$fileName);
				$a++;
			}
			unset($s3);
		 }
	 }

	 /**
      * createFileLink : creates a link to an S3 object that will expire after
	  *                  a period of time
	  *
      * @access	public
	  * @param  array  $fileName Name of the S3 object for which we want to create a link.
      * @return string $link
      */
    function createFileLink($fileName,$userDir=NULL) {
        $CI =& get_instance();
        $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
        
        $expires = time()+AWS_LINK_EXPIRE;
        if (empty($userDir)) {
            $userDir = $CI->session->userdata('userDir');
        }
        $s3URI = $userDir."/".$fileName;
        
        $headers = array("response-content-disposition"=>"attachment");
        $link = $s3->getAuthenticatedURL(AWS_BUCKET,$s3URI,AWS_LINK_EXPIRE,FALSE,FALSE,$headers);
        return $link;
    }

    function moveFileToS3($filePath,$fileName,$s3Folder,$security=NULL) {
	$CI =& get_instance();
        $CI->load->model('files/File_maintenance');
        if (!class_exists('S3')) {
                include(APPPATH.'libraries/s3/S3.php');
        }
        $CI->load->helper('file');
        $CI->load->helper('download');
        $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);

        $s3security = S3::ACL_PUBLIC_READ;
        if ($security == 'private') {
            $s3security = S3::ACL_PRIVATE;
        } elseif ($security == 'public_read') {
            $s3security = S3::ACL_PUBLIC_READ;
        }
        $s3->putObjectFile($filePath, AWS_BUCKET_NAME, $s3Folder.'/'.$fileName, $s3security);
        unset($s3);
    }
}

?>
