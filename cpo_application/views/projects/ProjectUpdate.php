<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="updateProject" class="helptag" data-help="projects">
    <form class="greyForm" id="formProject" name="formProject" action="<?=BASE_URL; ?>projects/ProjectUpdate/saveProject" method="POST">
    <input type="hidden" name="projectID" id="projectID" value="<?=$projectInformation['ProjectID']; ?><?=set_value('projectID'); ?>" />
    <input type="hidden" name="clientIDHolder" id="clientIDHolder" value="<?=$projectInformation['ClientID']; ?><?=set_value('clientIDHolder'); ?>" />
    <input type="hidden" name="origTemplateID" id="origTemplateID" value="<?=$projectInformation['TemplateID']; ?><?=set_value('origTemplateID'); ?>" />
    <input type="hidden" name="tagHolder" id="tagHolder" value="<? echo str_replace(' ',',',$projectInformation['Tags']); ?><?=set_value('tagHolder'); ?>" />
    <input type="hidden" name="action" id="action" value="<?=$action; ?><?=set_value('action'); ?>" />
	<input type="hidden" name="save" id="save" value="1" />
		<div style="float: left;">
		<p class="noBorder">
			<label class="required" for="clientID"><?=lang('project_form_select_client'); ?></label>
			<input type="text" class="vcardSearch" value="<?=$projectInformation['ClientCompany']; ?>" id="clientID" name="clientID" tabindex="1" style="width: 381px;" />
            <div class="formHelpText" style="margin-top: -6px;"><span class="subText"><?=lang('client_search'); ?></span></div>
			<?=form_error('clientID','<span class="formError">','</span>'); ?>
		</p>
        </div>
        <div style="float: left;">
                <?=help('help_formfield_client_search','clientID','iconHelpFormOutside'); ?>
        </div>
        <div style="clear: left;"></div>
        <p>
            <label class="required" for="projectTitle"><?=lang('project_form_title'); ?></label>
            <input type="text" value="<?=$projectInformation['Title']; ?><?=set_value('projectTitle'); ?>" name="projectTitle" id="projectTitle" tabindex="2" style="width: 400px;" />
            <?=form_error('projectTitle','<span class="formError">','</span>'); ?>
        </p>
        <p>
            <label><?=lang('project_form_notes'); ?></label>
            <textarea class="expanding" name="projectNotes" id="projectNotes" tabindex="3" style="width: 400px; height: 100px;"><?=$projectInformation['Description']; ?><?=set_value('projectNotes'); ?></textarea>
        </p>
        <p>
            <label><?=lang('project_form_budget'); ?></label>
            <input type="text" value="<?=$projectInformation['Budget']; ?><?=set_value('projectBudget'); ?>" name="projectBudget" id="projectBudget" tabindex="4" style="width: 200px;" />
            
        </p>
        <?
        if ($templates != false) {
            echo '<p>';
            echo '<label for="projectTemplates">'.lang('project_template').'</label>';
            echo '<select name="projectTemplate" id="projectTemplate" size="1" style=" width: 408px;">';
            echo '<option value="0">'.lang('common_field_select').'</option>';
            foreach($templates as $template) {
                echo '<option value="'.$template['TemplateID'].'"';
                if ($projectInformation['TemplateID'] == $template['TemplateID']) {
                    echo ' selected="selected" ';
                }
                echo '>'.$template['Title'].'</option>';
            }
            echo '</select>';
            echo '</p>';
        }
        ?>
		<p>
            <label for="projectURL"><?=lang('project_form_url'); ?></label>
            <input type="text" value="<?=$projectInformation['URL']; ?><?=set_value('projectURL'); ?>" name="projectURL" id="projectURL" tabindex="4" style="width: 400px;" />
		</p>
		<p>
			<label for="projectDates"><?=lang('project_dates'); ?></label>
			<input type="text" tabindex="5" id="projectDateStart" name="projectDateStart" class="dateField" value="<?=$projectInformation['ProjectDateStart']; ?><?=set_value('projectDateStart'); ?>" style="width: 95px; margin-right: 6px;" />
            <input type="text" tabindex="6" id="projectDateEnd" name="projectDateEnd" class="dateField" value="<?=$projectInformation['ProjectDateEnd']; ?><?=set_value('projectDateEnd'); ?>" style="width: 95px;" />
		</p>
        <p class="noBorder">
            <label for="projectCategory"><?=lang('project_form_category'); ?></label>
            <input type="text" name="projectCategorySelect" id="projectCategorySelect" value="<?=$projectInformation['Category']; ?>" tabindex="6" style="width: 400px;" />
			<input type="hidden" name="projectCategory" id="projectCategory" value="<?=$projectInformation['CatID']; ?>" />
        </p>
		<p>
			<label for="projectStatus"><?=lang('status_status'); ?></label>
			<select name="projectStatus" id="projectStatus" tabindex="7" style="width: 408px;">
            <option value="0" <? if ($projectInformation['Status'] == 0) { echo 'selected="selected"'; } ?>><?=lang('status_not_started'); ?></option>
			<option value="1" <? if ($projectInformation['Status'] == 1) { echo 'selected="selected"'; } ?>><?=lang('status_in_progress'); ?></option>
			<option value="2" <? if ($projectInformation['Status'] == 2) { echo 'selected="selected"'; } ?>><?=lang('status_completed'); ?></option>
			<option value="3" <? if ($projectInformation['Status'] == 3) { echo 'selected="selected"'; } ?>><?=lang('status_on_hold'); ?></option>
            </select>
		</p>
        <!--
        <div style="float: left;">
		<p class="noBorder">
			<label><?=lang('project_rate'); ?></label>
			<input type="text" name="projectRateAmount" id="projectRateAmount" tabindex="8" value="<?=lang('finance_expense_amount'); ?><?=$projectInformation['RateAmount']; ?><?=set_value('projectRateAmount'); ?>" style="float: left; margin-right: 6px; width: 100px;" />
            <select id="projectRatePer" name="projectRatePer" size="1" tabindex="9" style="float: left; width: 100px; margin-right: 6px;">
                <option value="hour" <? if ($projectInformation['RatePer'] == 'hour') { echo 'selected="selected"'; } ?>><?=lang('common_per_hour'); ?></option>
                <option value="day" <? if ($projectInformation['RatePer'] == 'day') { echo 'selected="selected"'; } ?>><?=lang('common_per_day'); ?></option>
            </select>
			<textarea name="projectRateDesc" id="projectRateDesc" tabindex="10" style="float: left; width: 180px; height: 50px;"><?=lang('finance_invoice_item_description'); ?><?=$projectInformation['RateDescription']; ?><?=set_value('projectRateDesc'); ?></textarea>
		</p>
		</div>
		<div style="float: left;">
			<?=help('help_rates','projectRateAmount','iconHelpFormOutside'); ?>
		</div>
		<div style="clear: left;"></div>
		<p></p>
        -->
		<p>
            <div style="float: left; margin-right: 18px;">
                <label><?=lang('client_form_tags'); ?></label>
                <div style="float: right;">
                    <input type="text" name="projectTags" id="projectTags" tabindex="12" value="<?=$projectInformation['Tags']; ?><?=set_value('projectTags'); ?>" />
                </div>
            </div>
            <div style="float: left;">
                <?=help('help_formfield_tags','clientTags','iconHelpFormOutside'); ?>
            </div>
        </p>
        
        <div style="clear: left;"></div>
		<p class="noBorder"></p>
		
		<div class="buttonContainer">
            <button class="buttonExpand action primary" id="buttonSave" tabindex="13"><span class="save"><?=lang('button_save'); ?></span></button>
            <button class="buttonExpand cancel secondary" id="buttonCancel" tabindex="14"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
        </div>
	</form>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>