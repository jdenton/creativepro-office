<? $this->load->view('includes/header'); ?>
<input type="hidden" id="tag" value="<?=$tag; ?>" />
<input type="hidden" id="numberOfMessages" value="<?=$numberMessages; ?>" />
<input type="hidden" id="archive" value="<?=$archive; ?>" />
<input type="hidden" id="maxProjects" value="<?=$maxProjects; ?>" />

<div id="pageLeftColumn" pageID="viewAllProjects">	
	<div id="tabsProject" class="ui-tabs">
		<ul class="ui-tabs-nav"> 
	        <li class="ui-tabs-selected"><a href="#windowViewProjects"><span><?=lang('project_page_title'); ?></span></a></li> 
	        <li><a href="#windowMessages"><span><?=lang('common_discussions'); ?></span> <span id="messageCount" class="tabCount lightGray">(0)</span></a></li>
	        <li class="projectUpdate hide"><a href="#windowTemplates"><span><?=lang('menu_project_templates'); ?></span></a></li>
	    </ul>
	    <div style="min-height: 200px;" class="windowFrame"> 
	    	<div id="windowViewProjects" class="helptag" data-help="projects">
		        <div class="barYellow bottom"><div style="padding: 6px;">
		        	<div style="float: left; margin-right: 12px;">
			        	<input type="text" style="width: 250px;" class="search" id="sProjectAutocompleter" />
						<input type="hidden" id="sProjectID" />
					</div>	
					<div style="float: left; margin-right: 12px;">
						<select id="selectProjectSort" class="sort" style="width: 150px;">
                            <option value="title"><?=lang('project_form_title'); ?></option>
                            <option value="company"><?=lang('menu_client'); ?></option>
                            <option value="invoice"><?=lang('common_invoice_total'); ?></option>
						</select>
					</div>
                    <button id="buttonArchivedOpen" class="smallButton projectViewAll hide" style="float: left;"><span class="archive" id="buttonArchivedOpenLabel"><?=lang('project_view_archived'); ?></span></button>
					<div style="clear: both;"></div>
		        </div></div>		        
		        <div id="projectListContainer" class="activityMessageBig"> <?=lang('common_loading'); ?></div>
			</div>        
	    	<div id="windowMessages" class="ui-tabs-hide">
                <div class="barYellow bottom">
					<div style="padding: 6px;">
						<div id="newMessage" itemID="0" itemType="project" class="messageContainer"></div>
					</div>
				</div>
				<div id="messageListProject" style="margin-top: 6px;"></div>
	   		</div>
            <div id="windowTemplates" class="ui-tabs-hide">
                <div id="templateListContainer" class="helptag activityMessageBig" data-help="templates"> <?=lang('common_loading'); ?></div>
	   		</div>
		    <div id="windowAlerts" class="ui-tabs-hide"> 
		    	<div style="padding: 6px;">
		    		Alerts will include: 1. Project or tasks overdue, 2. Invoice payment overdue.
		    	</div>
		    </div> 
		</div>	    
	</div>

    <? $this->load->view('includes/templateFormDialog'); ?>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>