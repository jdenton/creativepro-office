<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="viewProjectDetails">
	<input type="hidden" id="projectID" value="<?=$projectID; ?>" />
    <input type="hidden" id="contactType" value="P" />
	<script type="text/javascript" language="javascript">
	/* <![CDATA[ */
		var projectData = '<?=$projectInformation; ?>';
	/* ]]> */	
	</script>
	<div id="tabsProject" class="ui-tabs">
		<ul class="ui-tabs-nav noPrint">
	        <li class="ui-tabs-selected" id="tabProjectDetails"><a href="#windowProjectDetails"><span><?=lang('project_details'); ?></span></a></li> 
	        <li class="taskView hide"><a href="#windowTasks" id="tabProjectTasks"><span><?=lang('project_tasks'); ?></span> <span id="taskCount" class="tabCount lightGray">(0)</span></a> <span class="bubble yellow top right shadow">412</span></li>
	        <li class="invoiceViewAll invoiceViewOwn hide"><a href="#windowInvoices" id="tabProjectInvoices"><span><?=lang('finance_invoices'); ?></span> <span id="invoiceCount" class="tabCount lightGray">(0)</span></a> <span class="bubble green top right shadow">412</span></li>
	        <li><a href="#windowFiles" id="tabProjectFiles"><span><?=lang('files_files'); ?></span> <span id="fileCount" class="tabCount lightGray fileCount">(0)</span></a> <span class="bubble red top right shadow">412</span></li>
	        <li><a href="#windowMessages" id="tabProjectMessages"><span><?=lang('common_discussions'); ?></span> <span id="messageCount" class="tabCount lightGray">(0)</span></a></li>
	        <li><a href="#windowNotes" id="tabProjectNotes"><span><?=lang('widget_notes'); ?></span> <span id="noteCount" class="tabCount lightGray noteCount">(<span id="noteCountNumber">0</span>)</span></a></li>
	    </ul> 
	    <div class="windowFrame"> 
	    	<div id="windowProjectDetails" class="helptag" data-help="projects">
                <div style="padding: 6px;" id="projectInformationContainer"></div>
                <hr />
                <!-- Test notifications view -->
                <div id="notifications" class="notificationContainer"></div>
                <!-- End Test notifications view -->
                
                <? if ($permissions['contactView'] == 1) { ?>
                <!--
                <div id="projectContactPanel" style="padding: 12px;">
                    <div style="float: left; margin-right: 6px;"><?=help('help_project_team_contacts','',''); ?></div>
                    <h2 style="float: left;"><?=lang('project_project_contacts'); ?></h2>
                    <div style="clear: left;"></div>
                    
                    <div style="margin-top: 12px;">
                        <div style="margin: 2px 24px 0 0; float: left;">
                            <? if ($permissions['projectUpdate'] == 1) { ?>
                            <div id="contactSearchContainer">
                                <input type="text" style="width: 400px;" data-defaulttext="<?=lang('common_search_contacts'); ?>" class="autocompleteContacts" id="sContactsAutocompleter" /><br />
                            </div>
                            <? } ?>
                        </div>

                        <? if ($permissions['contactCreate'] == 1) { ?>
                            <button class="buttonExpand action" id="buttonNewContact"><span class="add"><?=lang('contact_new'); ?></span></button>
                        <? } ?>

                        <div style="clear: both;"></div>
                    </div>
                   <div id="formContactContainer" style="display: none;">
                    <? $this->load->view('includes/contactForm'); ?>
                   </div>
                   <p id="noContactLinkContainer" style="display: none;"><?=lang('contact_no_contacts'); ?> <a href="#" id="linkNewContact"><?=lang('contact_create'); ?></a></p>
                   <div id="contactsContainer" itemType="P" itemID="<?=$projectID; ?>"></div>
                   <div style="clear: left;"></div>
                </div>
                -->
                <? } ?>

            </div>
            <div id="windowTasks" class="helptag ui-tabs-hide" data-help="tasks">
                <input type="hidden" id="dateStart" />
                <input type="hidden" id="dateEnd" />
				<input type="hidden" id="userID" />
                <input type="hidden" id="selectedMilestoneID" />
                <input type="hidden" id="sTaskID" />
                <input type="hidden" id="taskAcdc" value="asc" />
                <input type="hidden" id="taskOrigSort" value="datedue" />

		<div class="barYellow bottom" style="min-height: 38px;">
            <div style="float: left; margin: 7px 0 0 12px;">
                <select id="selectTaskSort" class="sort" style="width: 150px;">
                    <option value="sortorder"><?=lang('common_sort_order'); ?></option>
                    <!--<option value="datedue"><?=lang('common_date_due'); ?></option>-->
                    <option value="title"><?=lang('common_title'); ?></option>
                    <option value="status"><?=lang('status_status'); ?></option>
                    <option value="priority"><?=lang('common_priority'); ?></option>
                    <option value="hours"><?=lang('common_hours'); ?></option>
                    <option value="taskno"><?=lang('task_number'); ?></option>                            
                </select>
            </div>
                    <div style="float: right; padding: 6px 12px 6px 6px;">
                        <button class="buttonExpand action taskCreate hide" id="buttonNewMilestone"><span class="flagBlue"><?=lang('task_new_milestone'); ?></span></button>
                    </div>
                    <div style="clear: both;"></div>
				</div>
				<div id="milestoneFormContainer_0" class="boxGray milestoneFormContainer" style="padding-left: 5px; display: none;"></div>
                <div style="padding: 3px;" id="taskInformationContainer">
                    <div class="activityMessageBig"> <?=lang('common_loading'); ?></div>
                </div>
                
                <? $this->load->view('includes/taskForm'); ?>
		    </div>

            <div id="windowInvoices" class="ui-tabs-hide">
                    <div id="invoiceInformationContainer" class="helptag" data-help="invoices"></div>
		     </div>
		    <div id="windowFiles" class="ui-tabs-hide">
                <div id="filesContainer" class="helptag" data-help="files">
                    <div class="barYellow bottom">
                        <div style="padding: 6px; float: left;">
                            <button itemID="<?=$projectID; ?>" itemType="project" class="smallButton fileUploadButton" id="project_<?=$projectID; ?>"><span class="upload"><?=lang('files_attach'); ?></span></button>
                        </div>
                        <div style="float: right; margin: 9px 18px 0 0;" class="infoMessageSmall"><?=lang('files_file_share'); ?></div>
                        <div style="clear: both;"></div>
                    </div>
                    <div style="padding: 6px;" id="filesInformationContainer">
                        <div id="project_<?=$projectID; ?>_fileContainer" class="fileAttachProject"></div>
                    </div>
                </div>
		    </div> 
	    	<div id="windowMessages" class="ui-tabs-hide">
				<div class="barYellow bottom">
					<div style="padding: 6px;">
						<div id="newMessage" itemID="<?=$projectID; ?>" itemType="project" class="messageContainer"></div>
					</div>
				</div>
				<div id="messageListProject" style="margin-top: 6px;"></div>
                <div id="messageMoreContainer" class="boxBlue" style="display: none; margin-top: 6px;"><a href="#" class="linkMoreMessages"><?=lang('common_more'); ?></a></div>
	   		</div>
            <div id="windowNotes" class="ui-tabs-hide">
                <div class="barYellow bottom">
					<div style="padding: 6px;">
                        <button itemID="<?=$projectID; ?>" itemType="project" class="smallButton" id="buttonNewNote"><span class="notes"><?=lang('notes_new_note'); ?></span></button>
					</div>
                    <div id="noteFormNewContainer" class="noteFormContainer hide" style="padding: 6px;"></div>
                    <?
                    $data['itemID'] = $projectID;
                    $data['itemType'] = 'P';
                    $this->load->view('includes/noteForm',$data);
                    ?>
				</div>
                <div id="noteListContainer"></div>
		    </div>
		    <div id="windowAlerts" class="ui-tabs-hide"> 
                   <div style="padding: 6px;">
                         
				   </div>
		    </div> 
		</div>	    
	</div>

    <? $this->load->view('includes/templateFormDialog'); ?>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>