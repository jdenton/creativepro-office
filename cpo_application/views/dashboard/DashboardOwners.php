<? $this->load->view('includes/header'); ?>

<div id="pageOneColumn" pageID="dashboard">
	<div id="columns" class="helptag" data-help="dashboard">
		
		<?
		$availableWidgets = availableWidgets();
        for($a=0;$a<=2;$a++) { 
            $b=$a+1;
			echo '<ul id="column'.$b.'" class="column">';
			foreach($activeWidgets as $widget) {
				if($widget['WColumn'] == $a) {
					$widgetID   = $widget['WID'];
                     /*
                      * Determing array index for current WID
                      */
                     for($c=0;$c<=sizeof($availableWidgets)-1;$c++) {
                          if ($availableWidgets[$c]['WID'] == $widgetID) {
                               $key = $c;
                               break;
                          }
                     }
					$widgetName = $availableWidgets[$key]['WidgetName'];

					/*
					 * createWidgetFrame is in the dashboard helper 
					 * (helpers/dashboard_helper.php)
					 */
					echo createWidgetFrame($widgetName,$widgetID);
				}
			}
			echo '</ul>';
		}		
		?>       
        
    </div>
    <div style="clear: both;"></div>    
</div>
<? $this->load->view('includes/footer'); ?>
