<? $this->load->view('includes/headerLight'); ?>
<div class="greenBar" style="width: 100%;">
    <select id="selectWidget" style="margin: 4px 0 0 4px; width: 97%;">
        <option value="tasks">Tasks</option>
        <option value="projects">Projects</option>
        <option value="invoices">Invoices</option>
        <option value="timer">Job Timer</option>
        <option value="calendar">Calendar</option>
        <option value="notes">Notes</option>
    </select>
</div>
<div id="widgetContent_invoices" class="widgetContainer" style="width: 150px; overflow: visible;"></div>
<div id="widgetContent_tasks" class="widgetContainer" style="overflow: visible;"></div>
<div id="widgetContent_projects" class="widgetContainer" style="overflow: visible;"></div>
<div id="widgetContent_timer" class="widgetContainer" style="overflow: visible;"></div>
<div id="widgetContent_notes" class="widgetContainer" style="overflow: visible;"></div>
<div id="widgetContent_calendar" class="widgetContainer" style="overflow: visible;"></div>
<?
$data['jsFileArray'] = array(
    'jquery/plugins/jquery.ui-1.8.1.js',
    'jquery/plugins/jquery.cookie.js',
    'jquery/plugins/jquery.elastic.js',
    'jquery/plugins/jquery.qtip-1.0.0-rc3.js',
    'jsGlobals.js',
    'jquery/plugins/jquery.commonAppMethods.js',
    'jsCommon.js',
    'jsDashboardOwners.js',
    'jsDashboardWidgets.js',
    'jsGadgetLoader.js',
    'jsTrash.js',
    'jsNotes.js'
);
$data['gadget'] = true;
?>

<? $this->load->view('includes/js'); ?>
<? $this->load->view('includes/footerLight',$data); ?>