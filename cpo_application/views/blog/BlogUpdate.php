<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="updatePosts">
	<div class="windowFrame" style="min-height: 80px;">
        <form action="<?=BASE_URL; ?>blog/Blog/saveBlogPost" method="POST">
            <input type="hidden" id="beID" name="beID" value="<?=$beID; ?>" />

            <h1 style="margin-bottom: 6px;">Blog post title</h1>
            <input type="text" style="width: 600px;" name="postTitle" id="postTitle" value="<?=$postTitle; ?>" />
            <div style="height: 12px;"></div>

            <h1 style="margin-bottom: 6px;">Tags for this post</h1>
            <input type="text" style="width: 600px;" name="postTags" id="postTags" value="<?=$postTags; ?>" />
            <p><?=$tagString; ?></p>
            <div style="height: 12px;"></div>

            <textarea class="tinymce" name="postContent"><?=$postContent; ?></textarea>
            <div class="buttonContainer" style="margin-left: 0;">
                <button class="buttonExpand action primary" id="buttonSave" tabindex="13"><span class="save"><?=lang('button_save'); ?></span></button>
                <button class="buttonExpand cancel secondary" id="buttonCancel" tabindex="14"><span class="cancel">Cancel</span></button>
                <div style="clear: left;"></div>
            </div>

        </form>
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
