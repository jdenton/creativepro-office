<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <?
    if (isset($blogPosts)) {
        foreach($blogPosts as $post) { ?>
        
            <h1 style="margin-bottom: 6px;"><a href="<?=BASE_URL; ?>blog/entry/<?=$post['EntryURL']; ?>"><?=stripslashes($post['Title']); ?></a></h1>

            <? if (!empty($post['AvatarURL'])) { ?>
                <img class="avatar33 blogAvatar" alt="<?=$post['NameFirst'].' '.$post['NameLast']; ?>" src="<?=$post['AvatarURL']; ?>" />
            <? } ?>
            <h2 class="gray" style="float: left;"><?=convertMySQLToGMT($post['DateEntry'],'pull','M j, Y'); ?>
                <? if (!empty($post['NameFirst'])) { ?>
                by <span class="brown"><?=$post['NameFirst']; ?></span>
                <? } ?>
            </h2>
            <span class="iconComments"><a href="<?=BASE_URL; ?>blog/entry/<?=$post['EntryURL']; ?>"><?=$post['commentCount']; ?>

            <?
            if ($post['commentCount'] == 1) {
                echo 'comment';
            } else {
                echo 'comments';
            }
            ?>

            </a></span><br />
            <span class="iconTags"><?=prepTagString($post['Tags'],BASE_URL.'blog/tag'); ?></span>
            <div style="clear: both;"></div>
            <div class="mTop12 text"><?=$post['BlogEntry']; ?></div>
            <div class="contentSpacer"></div>

        <? }
    } elseif (isset($blogPost)) { ?>
        <h1 style="margin-bottom: 6px;"><?=stripslashes($blogPost['Title']); ?></h1>

        <? if (!empty($blogPost['AvatarURL'])) { ?>
            <img class="avatar33 blogAvatar" alt="<?=$blogPost['NameFirst'].' '.$blogPost['NameLast']; ?>" src="<?=$blogPost['AvatarURL']; ?>" />
        <? } ?>
        <h2 class="gray" style="float: left;"><?=convertMySQLToGMT($blogPost['DateEntry'],'pull','M j, Y'); ?>
            <? if (!empty($blogPost['NameFirst'])) { ?>
            by <span class="brown"><?=$blogPost['NameFirst']; ?></span>
            <? } ?>
        </h2>
        <span class="iconComments"><a href="#comments"><?=$commentCount; ?>

        <?
        if ($commentCount == 1) {
            $commentWord = 'comment';
        } else {
            $commentWord = 'comments';
        }
        echo $commentWord;
        ?>

        </a></span><br />
        <span class="iconTags"><?=prepTagString($blogPost['Tags'],BASE_URL.'blog/tag'); ?></span>

        <div style="clear: both;"></div>
        <div class="mTop12 text"><?=$blogPost['BlogEntry']; ?></div>

        <div class="contentSpacer"></div>
        <a name="comments"></a>
        <div id="commentsContainer">
        <?
        if ($commentCount > 1) {
            echo '<h2 class="brown mBottom iconCommentsBig"><span>Discussion: '.$commentCount.' '.$commentWord.'</span></h2>';
        }

        $a=0;
        foreach ($comments as $comment) {
            $containerClass = 'commentAltContainer';
            if (($a%2) > 0) {
                $containerClass = 'commentContainer';
            }
        ?>
        <div class="commenterContainer">
            <?
            if (empty($comment['AvatarURL'])) {
                $avatarImageURL = getGravatar($comment['UserEmail'],46,BASE_URL.'images/avatars/stockAvatar46.png','x',FALSE);
            } else {
                $avatarImageURL = $comment['AvatarURL'];
            }
            ?>
            <span class="avatar46background" style="float: left; margin-right: 9px;"><img src="<?=$avatarImageURL; ?>" class="avatar46" alt="<?=$comment['UserName']; ?>" /></span>
            <div style="width: 130px; float: left;">
                <h3>
                <?
                if (!empty($comment['UserURL'])) {
                    echo '<a href="'.prep_url($comment['UserURL']).'" target="_blank" title="Visit '.prep_url($comment['UserURL']).'">'.$comment['UserName'].'</a>';
                } else {
                    echo $comment['UserName'];
                }
                ?>
                </h3>
                <span class="gray2"><?=convertMySQLToGMT($comment['DateComment'],'pull','M j, Y'); ?></span>

                <?
                if ($comment['Admin'] == 1) {
                    echo '<span class="cpoAdminIcon" style="margin-top: 6px;"></span>';
                }
                ?>
            </div>
        </div>
        <div class="<?=$containerClass; ?> commentContainer">
            <?=stripslashes($comment['Comment']); ?>
        </div>
        <div style="clear: both;"></div>
        <? $a++; } ?>
        </div>
        
        <h2 class="iconCommentAddBig" style="margin: 12px 0 12px 0;">Add your comment</h2>
        <form id="formComments">
            <input type="hidden" name="commentBlogPostID" id="commentBlogPostID" value="<?=$blogPost['BEID']; ?>" />

            <label for="commentName" class="inline left" style="width: 200px;">Your name</label>
            <input type="text" tabindex="1" class="bigField" name="commentName" id="commentName" style="width: 400px; margin-bottom: 6px;" value="<?=$this->session->userdata('fullName'); ?>" />

            <label for="commentEmail" class="inline left" style="width: 200px;">Your email address</label>
            <input type="text" tabindex="2" class="bigField" name="commentEmail" id="commentEmail" style="width: 400px; margin-bottom: 6px;" value="<?=$this->session->userdata('userEmail'); ?>" />

            <label for="commentURL" class="inline left" style="width: 200px;">Your website</label>
            <input type="text" tabindex="3" class="bigField" name="commentURL" id="commentURL" style="width: 400px; margin-bottom: 6px;" />

            <label for="commentText" class="inline left" style="width: 200px;">Your comments</label>
            <textarea tabindex="4" name="commentText" id="commentText" style="width: 400px; height: 200px;"></textarea>

            <button id="btnSaveComment" class="buttonExpand blueGray primary" style="margin: 12px 0 0 198px;" tabindex="5"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save">Save comment</span></span></button>
            <div style="float: right; margin-top: 6px;" id="commentMessageContainer"></div>
            <div style="clear: both;"></div>
        </form>

    <? } ?>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>