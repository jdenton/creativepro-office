<div id="tagsContainer">
    <h4 class="icon_tag mTop24 mBottom12"><?=lang('client_form_tags'); ?></h4>
    <div id="tagSelectContainer">
        <ul class="alphaList">
        <?
        $tagArray = $tagRenderArray['Tags'];
        $tagURL   = $tagRenderArray['tagURL'];
        if (count($tagArray) == 0) {
            echo '<li>'.lang('common_no_tags').'</li>';
        } else {
            foreach($tagArray as $key => $value) {
                echo '<li><p class="letter" style="float: left; text-align: top;">'.$key.'</p>';
                echo '<p class="content" style="float: left;">';
                foreach($tagArray[$key] as $tag) {
                    echo '<a href="'.$tagURL.'/'.$tag['Tag'].'" class="tag">'.$tag['Tag'].'</a> ';
                }
                echo '</p><p style="clear: left;"></p></li>';
            }
        }
        ?>
        </ul>
    </div>
    <div id="tagSelectContainer2" style="display: none;">
        <ul class="alphaList">
        <?
        if (isset($tagRenderArray2['Tags'])) {
            $tagArray = $tagRenderArray2['Tags'];
            $tagURL   = $tagRenderArray2['tagURL'];
            if (count($tagArray) == 0) {
                echo lang('common_no_tags');
            } else {
                foreach($tagArray as $key => $value) {
                    echo '<li><p class="letter" style="float: left; text-align: top;">'.$key.'</p>';
                    echo '<p class="content" style="float: left;">';
                    foreach($tagArray[$key] as $tag) {
                        echo '<a href="'.$tagURL.'/'.$tag['Tag'].'" class="tag">'.$tag['Tag'].'</a> ';
                    }
                    echo '</p><p style="clear: left;"></p></li>';
                }
            }
        }
        ?>
        </ul>
    </div>
    <div id="tagSelectContainer3" style="display: none;">
        <ul class="alphaList">
        <?
        if (isset($tagRenderArray3['Tags'])) {
            $tagArray = $tagRenderArray3['Tags'];
            $tagURL   = $tagRenderArray3['tagURL'];
            if (count($tagArray) == 0) {
                echo lang('common_no_tags');
            } else {
                foreach($tagArray as $key => $value) {
                    echo '<li><p class="letter" style="float: left; text-align: top;">'.$key.'</p>';
                    echo '<p class="content" style="float: left;">';
                    foreach($tagArray[$key] as $tag) {
                        echo '<a href="'.$tagURL.'/'.$tag['Tag'].'">'.$tag['Tag'].'</a> ';
                    }
                    echo '</p><p style="clear: left;"></p></li>';
                }
            }
        }
        ?>
        </ul>
    </div>
</div>
