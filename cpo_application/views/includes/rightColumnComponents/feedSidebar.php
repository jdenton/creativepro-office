<div id="feedContainer">
    <h2 class="icon_feed_small mTop24 mBottom12"><?=lang('widget_rss'); ?></h2>
    <?
    foreach($rssFeeds as $rssFeed) {
        echo '<a href="'.$rssFeed['Link'].'" type="rss" class="linkFeed linkRSS icon_rss_small" style="margin: 3px;">'.$rssFeed['Title'].'</a>';
    }
    foreach($icalFeeds as $icalFeed) {
        echo '<a href="'.$icalFeed['Link'].'" type="ical" class="linkFeed linkIcal icon_ical_small" style="margin: 3px;">'.$icalFeed['Title'].'</a>';
    }
    ?>
</div>
