<? if ($permissions['contactView'] == 1) { ?>
<div id="projectContactPanel">
    <h2 class="icon_client_small mTop24 mBottom12"><?=lang('project_people_on_project'); ?></h2>
    <p id="noContactLinkContainer" style="display: none;"><?=lang('contact_no_contacts'); ?> <a href="#" id="linkNewContact"><?=lang('contact_create'); ?></a></p>
    <div id="contactsContainer" itemType="P" itemID="<?=$projectID; ?>"></div>
    <div class="mTop12 mBottom12">
        <div>
            <? if ($permissions['projectUpdate'] == 1) { ?>
            <div id="contactSearchContainer">
                <input type="text" style="width: 215px;" data-defaulttext="<?=lang('common_add_people'); ?>" class="autocompleteContacts" id="sContactsAutocompleter" /><br />
            </div>
            <? } ?>
        </div>
    </div>
</div>
<? } ?>