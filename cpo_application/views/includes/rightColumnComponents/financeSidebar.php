<div id="sidebarLedger">
    <div class="colorBackground light-green" style="text-align: center; margin: 12px 9px 0 0;">
        <p style="text-align: center; margin-bottom: 6px;"><?=lang('reports_income'); ?></p>
        <span id="totalIncome" class="hugeTextEntry"></span>
    </div>

    <div class="colorBackground light-blue" style="text-align: center; margin: 9px 9px 9px 0;">
        <p style="text-align: center; margin-bottom: 6px;"><?=lang('finance_expenses'); ?></p>
        <span id="totalExpense" class="hugeTextEntry"></span>
    </div>    
    
    <div id="filterContainerLedger" class="center">
        <div id="rdoLedger">
            <input type="radio" id="radioAll" name="radio" checked="checked" /><label for="radioAll"><?=lang('common_all'); ?></label>
            <input type="radio" id="radioIncome" name="radio" /><label for="radioIncome"><?=lang('reports_income'); ?></label>
            <input type="radio" id="radioExpenses" name="radio" /><label for="radioExpenses"><?=lang('finance_expenses'); ?></label>
        </div>
    </div>    
</div>
<div id="sidebarInvoices" class="hide">
    <div class="colorBackground light-green" style="text-align: center;">
        <p style="text-align: center; margin-bottom: 6px;"><?=lang('reports_income'); ?></p>
        <span id="totalInvoiced" class="hugeTextEntry"></span>
    </div>

    <div class="colorBackground light-blue" style="text-align: center; margin: 9px 0 9px 0;">
        <p style="text-align: center; margin-bottom: 6px;"><?=lang('finance_estimates'); ?></p>
        <span id="totalEstimate" class="hugeTextEntry"></span>
    </div>
    
    <div class="colorBackground light-red" style="text-align: center; margin: 9px 0 9px 0;">
        <p style="text-align: center; margin-bottom: 6px;"><?=lang('finance_payments_due'); ?></p>
        <span id="totalDue" class="hugeTextEntry"></span>
    </div>

    <div id="filterContainerInvoices" class="center">
        <div id="rdoInvoices">
            <input type="radio" id="radioInvoiceAll" name="radio2" checked="checked" /><label for="radioInvoiceAll"><?=lang('common_all'); ?></label>
            <input type="radio" id="radioInvoiceOpen" name="radio2" /><label for="radioInvoiceOpen"><?=lang('common_open'); ?></label>
            <input type="radio" id="radioInvoiceClosed" name="radio2" /><label for="radioInvoiceClosed"><?=lang('common_closed'); ?></label>
            <input type="radio" id="radioInvoicePastDue" name="radio2" /><label for="radioInvoicePastDue"><?=lang('common_past_due'); ?></label>
        </div>  
    </div>
</div>

