<div id="emailInvoiceFormContainer" style="display: none;">
    <div>
        <strong><?=lang('finance_invoice_email_address'); ?></strong><br />
        <input type="text" style="width: 350px;" class="emailInvoiceTo" name="emailInvoiceTo" />
    </div>
    <div>
        <input type="checkbox" class="emailInvoiceCopyToUser" name="emailInvoiceCopyToUser" value="1" checked="checked" /> <strong><?=lang('common_send_copy_to_me'); ?></strong>
    </div>
    <div class="mTop12">
        <strong><?=lang('common_message'); ?></strong><br />
        <textarea class="emailInvoiceMessage" name="emailInvoiceMessage" style="width: 350px; height: 100px;"></textarea>
    </div>
    <div class="mBottom12">
        <? if ($estimate == false) { ?>
        <div class="emailPayPalLinkContainer">
            <? if (!empty($emailPayPal)) { ?>
                <input type="checkbox" class="emailInvoicePayPalLink" name="emailInvoicePayPalLink" value="1" checked="checked" /> <strong><?=lang('finance_show_paypal_link'); ?></strong>
            <? } else { ?>
                <input type="hidden" class="emailInvoicePayPalLink" name="emailInvoicePayPalLink" value="0" />
            <? } ?>
        </div>        
        <? } ?>        
    </div>
    <button class="smallButton emailInvoiceSend" style="float: left; margin-right: 12px;"><span class="email"><?=lang('common_send'); ?></span></button>
    <button class="smallButton emailInvoiceSendCancel" style="display: none; float: left;"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
    <div style="float: left;" class="emailInvoiceStatusMessage"></div>
    <div style="clear: left;"></div>
</div>