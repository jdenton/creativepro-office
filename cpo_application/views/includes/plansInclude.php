<h1 class="mBottom12">All plans include</h1>
<div style="width: 48%; margin-right: 24px; float: left;">
    <ul class="check">
        <li>Unlimited invoices</li>
        <li>Secure client area</li>
        <li>Group calendars</li>
        <li>Google calendar integration</li>
        <li>Team member permissions manager</li>
        <li>Email reminders for tasks</li>
        <li>Create tasks, projects, events from email</li>
    </ul>
</div>
<div style="width: 48%; float: left;">
    <ul class="check">
        <li>Project templates <span class="iconNew"></span></li>
        <li>Timesheets &amp; job timer</li>
        <li>Task manager</li>
        <li>Contact manager</li>
        <li>Expense tracker</li>
        <li>Client &amp; team message system</li>
        <li>Data import for clients &amp; contacts</li>
        <li>Account data export</li>
    </ul>
</div>