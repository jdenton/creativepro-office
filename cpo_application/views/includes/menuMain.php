<?
/*
|---------------------------------------------------------------
| APPLICATION SUB-MENUS
|---------------------------------------------------------------
|
*/
$subMenuArray['dashboard']  = array(lang('menu_add_dashboard_widgets'));
$subMenuArray['clients']    = array(lang('menu_view_clients'),lang('menu_new_client'));
$subMenuArray['projects']   = array(lang('menu_view_projects'),lang('menu_new_project'),lang('menu_project_templates'));
$subMenuArray['timesheets'] = array(lang('menu_view_timesheets'),lang('menu_view_timesheet_reports'));
$subMenuArray['finances']   = array(
    lang('menu_view_ledger'),
    lang('menu_new_invoice'),
    lang('menu_new_estimate'),
    lang('menu_new_expense'),
    lang('common_vendors'),
    lang('menu_finance_reports')
);
$subMenuArray['team']       = array();
$subMenuArray['calendar']   = array();
$subMenuArray['discussions']= array();
$subMenuArray['adminUsers'] = array('Find Users','User Metrics','Translation');

$subMenuLinksArray['dashboard']  = array('#');
$subMenuLinksArray['clients']    = array(site_url('clients/ClientView'),site_url('clients/ClientUpdate'));
$subMenuLinksArray['projects']   = array(site_url('projects/ProjectView'),site_url('projects/ProjectUpdate'),site_url('projects/ProjectView#windowTemplates'));
$subMenuLinksArray['timesheets'] = array(site_url('timesheets/TimesheetView'),site_url('timesheets/TimesheetView#windowTimesheetReports'));
$subMenuLinksArray['finances']   = array(
    site_url('finances/FinanceView'),
    site_url('finances/InvoiceUpdate'),
    site_url('finances/EstimateUpdate'),
    site_url('finances/ExpenseUpdate'),
    site_url('finances/FinanceView#windowManageVendors'),
    site_url('finances/FinanceView#windowViewFinanceReports')
);
$subMenuLinksArray['team']       = array();
$subMenuLinksArray['calendar']   = array();
$subMenuLinksArray['discussions']= array();
$subMenuLinksArray['adminUsers'] = array(site_url('administration/AdminUsers'),site_url('administration/AdminUserMetrics'),site_url('administration/AdminTranslation'));

$subMenuClassArray['dashboard']  = array('#');
$subMenuClassArray['clients']    = array('clientView','clientCreate');
$subMenuClassArray['projects']   = array('projectView','projectCreate');
$subMenuClassArray['timesheets'] = array('timesheetViewAll timesheetViewOwn','timesheetViewAll timesheetViewOwn');
$subMenuClassArray['finances']   = array('invoiceViewAll invoiceViewOwn','invoiceCreateSend invoiceCreateSendToAccountOwner','invoiceCreateSend invoiceCreateSendToAccountOwner','expenseViewAll','expenseCreate');
$subMenuClassArray['team']       = array();
$subMenuClassArray['calendar']   = array();
$subMenuClassArray['discussions']= array();
$subMenuClassArray['adminUsers'] = array(site_url('administration/AdminUsers'),site_url('administration/AdminUserMetrics'),site_url('administration/AdminTranslation'));
$permArray = $this->session->userdata('permissions');
?>
<div id="menuMain">
    <ul id="navmenu-h">
        <? if ($permArray['clientView'] == 1 || $permArray['clientCreate'] == 1) { ?>
        <li class="headlink">
            <a href="<?=site_url('clients/ClientView'); ?>" class="buttonMainMenu tab"><span><?=lang('menu_clients'); ?></span></a>
            <ul>
                <?
                $a=0;
                foreach($subMenuArray['clients'] as $clientMenuItem) {
                    echo '<li class="'.$subMenuClassArray['clients'][$a].'"><a href="'.$subMenuLinksArray['clients'][$a].'">'.$clientMenuItem.'</a></li>';
                    $a++;
                }
                ?>
            </ul>
        </li>
        <?
        }
        if ($permArray['projectViewAll'] == 1 || $permArray['projectViewOwn'] == 1 || $permArray['projectCreate'] == 1) {
        ?>
        <li class="headlink">
            <a href="<?=site_url('projects/ProjectView'); ?>" class="buttonMainMenu tab"><span><?=lang('menu_projects'); ?></span></a>
            <ul>
                <?
                $a=0;
                foreach($subMenuArray['projects'] as $projectMenuItem) {
                    echo '<li class="'.$subMenuClassArray['projects'][$a].'"><a href="'.$subMenuLinksArray['projects'][$a].'">'.$projectMenuItem.'</a></li>';
                    $a++;
                }
                ?>
            </ul>
        </li>
        <?
        }
        if (
            $permArray['invoiceViewAll'] == 1 ||
            $permArray['invoiceViewOwn'] == 1 ||
            $permArray['invoiceCreateSend'] == 1 ||
            $permArray['invoiceCreateSendToAccountOwner'] == 1 ||
            $permArray['expenseViewAll'] == 1 ||
            $permArray['expenseViewOwn'] == 1 ||
            $permArray['expenseCreate'] == 1
        ) {
        ?>
        <li class="headlink">
            <a href="<?=site_url('finances/FinanceView'); ?>" class="buttonMainMenu tab"><span><?=lang('menu_finances'); ?></span></a>
            <ul>
                <?
                $a=0;
                foreach($subMenuArray['finances'] as $financeMenuItem) {
                    echo '<li class="'.$subMenuClassArray['finances'][$a].'"><a href="'.$subMenuLinksArray['finances'][$a].'">'.$financeMenuItem.'</a></li>';
                    $a++;
                }
                ?>
            </ul>
        </li>
        <?
        }
        if ($permArray['timesheetViewAll'] == 1 || $permArray['timesheetViewOwn'] == 1 || $permArray['timesheetCreate'] == 1) {
        ?>
        <li class="headlink">
            <a href="<?=site_url('timesheets/TimesheetView'); ?>" class="buttonMainMenu tab"><span><?=lang('menu_timesheets'); ?></span></a>
            <ul>
                <?
                $a=0;
                foreach($subMenuArray['timesheets'] as $timesheetMenuItem) {
                    echo '<li class="'.$subMenuClassArray['timesheets'][$a].'"><a href="'.$subMenuLinksArray['timesheets'][$a].'">'.$timesheetMenuItem.'</a></li>';
                    $a++;
                }
                ?>
            </ul>
        </li>
        <? } ?>
        
        <? if ($permArray['messageViewAll'] == 1 || $permArray['messageViewOwn'] == 1) { ?>
        <li><a href="<?=site_url('messaging/Discussions'); ?>" class="buttonMainMenu"><span><?=lang('common_discussions'); ?></span></a></li>
        <? } ?>
        
        <?
        if ($permArray['calendarViewAll'] == 1 || $permArray['calendarViewOwn'] == 1 || $permArray['calendarCreate'] == 1) {
        ?>
        <li><a href="<?=site_url('calendar/CalendarView'); ?>" class="buttonMainMenu"><span><?=lang('calendar'); ?></span></a></li>

        <? if ($permArray['filesView'] == 1 && $this->session->userdata('accountLevel') > 0) { ?>
        <li><a href="<?=site_url('files/FileManager'); ?>" class="buttonMainMenu"><span><?=lang('files_files'); ?></span></a></li>
        <? } ?>
        <?
        }
        if ($permArray['teamView'] == 1 || $permArray['teamCreate'] == 1) {
        ?>
        <li><a href="<?=site_url('team/TeamUpdate'); ?>" class="buttonMainMenu"><span><?=lang('team_my_team'); ?></span></a></li>
        <? } ?>
        <!--<li><a href="" class="buttonMainMenu"><span>Discussions</span></a></li>-->
</ul>
</div>