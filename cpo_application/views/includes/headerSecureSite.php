<?
$randomString = random_string();

$headerClass = 'home';
if ($child == TRUE) {
    $headerClass = 'child';
}

if (!isset($pageTitle)) {
    $pageTitle = 'CreativePro Office';
} else {
    $pageTitle = $pageTitle.' : CreativePro Office';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title><?=$pageTitle; ?></title>
	<meta name="Description" content="CreativePro Office is a complete set of online office management tools for creative professionals. Manage your team, clients, projects, invoices, timesheets, and events from one web-based application with nothing to download or install.">
	<meta name="Keywords" content="project management, time tracking, invoices, quotes, tasks, online office, web, software, application, billable hours, creative professional, web developer, graphic artist">
	<meta name="Author" content="Jeff Denton @ UpStart Productions :: Portland OR :: info@UpStart-Productions.com">
	<META NAME="Rating" CONTENT="General">
	<META NAME="Distribution" CONTENT="global">
	<!-- Favicon -->
    <link rel="icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />

    <!-- Style Sheets -->
	<link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/browserReset.css?<?=$randomString; ?>" type="text/css" />
    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/stylesPublic.css?<?=$randomString; ?>" type="text/css" />
    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/buttons.css?<?=$randomString; ?>" type="text/css" />
    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/alerts.css?<?=$randomString; ?>" type="text/css" />

    <!-- RSS Feeds -->
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?=BASE_URL; ?>rss/Rss/productBlogFeed" />

    <!-- Print Style Sheet -->
	<link rel="STYLESHEET" media="print" href="<?=BASE_URL; ?>css/print.css?<?=$randomString; ?>" type="text/css" />

    <style type="text/css">
        #header .headerTextContainer {
            background: url('<?=BASE_URL; ?>images/<?=$headerTextImage; ?>') 0px 27px no-repeat;
        }
    </style>
</head>
<body>
    <div id="header" class="<?=$headerClass; ?>">
        <div class="blackBar">
            <div class="content">
                <a href="<?=BASE_URL; ?>" class="logoCPO" title="CreativePro Office : HOME"></a>
                <div class="menuContainer">
                    <div class="headerAvatar default">
                        <a href="<?=INSECURE_URL; ?>dashboard/Dashboard">Go to your account</a> or <a href="<?=INSECURE_URL; ?>login/logout">Log out</a></div>
                    <div style="clear: right;"></div>
                </div>
            </div>
        </div>
        <!-- Child page header field -->
        <div class="homeFieldBluePublic">
            <div class="content">
                <div class="headerTextContainer"></div>
            </div>            
        </div>        
    </div>
    <div id="pageBody">
        <div class="securePadlockHeader"></div>
