<div style="clear: left;"></div> <!-- Clear page left, right panel floats -->
</div> <!-- End #pageBody -->
    <div id="footer">
        <div class="content">
            <div style="float: left; width: 25%;"></div>
            <div style="float: left; width: 25%;"></div>
            <div style="float: left; width: 25%;"></div>
            <div style="float: left; width: 25%;"></div>
            <div class="mBottom" style="clear: left;"></div>
            <div class="copyright">CreativePro Office is an <a href="http://www.UpStart-Productions.com" title="UpStart Productions">UpStart Productions</a> service. &copy; 2003-<?=date('Y'); ?></div>
        </div>
    </div>

    <!-- JQuery -->
    <script language="JavaScript" type="text/javascript" src="<?=BASE_URL; ?>js/jquery/jquery-1.4.2.js"></script>
    <script language="JavaScript" type="text/javascript" src="<?=BASE_URL; ?>js/jquery/plugins/jquery.fancybox-1.2.6.packed.js"></script>
    <script language="JavaScript" type="text/javascript" src="<?=BASE_URL; ?>js/jquery/plugins/jquery.animateToClass.js"></script>
    <script language="JavaScript" type="text/javascript" src="<?=BASE_URL; ?>js/jquery/plugins/jquery.commonAppMethods.js"></script>

    <script language="JavaScript" type="text/javascript" src="<?=BASE_URL; ?>js/jsPublicSite.js"></script>
    <?
    if (is_array($jsFileArray)) {
        foreach ($jsFileArray as $jsFile) {
            echo '<script src="'.BASE_URL.'js/'.$jsFile.'" language="JavaScript" type=text/javascript></script>'."\n";
        }
    }
    ?>
    <script language="JavaScript" type="text/javascript">
    /* <![CDATA[ */
        var siteURL = '<?=BASE_URL; ?>';
        var insecureURL = '<?=INSECURE_URL; ?>';
        var thisPage  = '<?=$_SERVER['PHP_SELF']; ?>';

        /*
         * Google Analytics
         */
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1427307-6']);
        _gaq.push(['_setDomainName', '.mycpohq.com']);
        _gaq.push(['_trackPageview']);

        (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(ga, s);
        })();
    /* ]]> */
    </script>

    </body>
    </html>