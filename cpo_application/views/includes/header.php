<?
$randomString = random_string();
if (!isset($pageTitle)) {
    $pageMetaTitle = 'CreativePro Office';
} else {
    $pageMetaTitle = $pageTitle.' : CreativePro Office';
}
?>
<!DOCTYPE html>
<html lang="<?=$this->session->userdata('language'); ?>">
<head>
	<meta charset="utf-8">
	
	<title><?=$pageMetaTitle; ?></title>
	<meta name="Description" content="CreativePro Office is a complete set of online 
	office management tools for creative professionals.	Manage your team, clients, projects, invoices, quotes and events from one web-based 
	application with nothing to download or install.">
	<meta name="Keywords" content="project management, time tracking, invoices, quotes, tasks, online office, web, software, application, billable hours, creative professional, web developer, graphic artist">
	<meta name="Author" content="Jeff Denton @ UpStart Productions :: Portland OR :: info@UpStart-Productions.com">
	<META NAME="Rating" CONTENT="General">
	<META NAME="ROBOTS" CONTENT="INDEX, NOFOLLOW">
	<META NAME="Distribution" CONTENT="global">
    <META http-equiv="X-UA-Compatible" content="IE=9">
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon">
	
	<!-- Style Sheets -->
    <?=cachedFile('browserReset.css','media="screen"'); ?>
    <?=cachedFile('alerts.css','media="screen"'); ?>
    <?=cachedFile('buttons.css','media="screen"'); ?>
    <?=cachedFile('colors.css','media="screen"'); ?>
    <?=cachedFile('dashboardWidgets.css','media="screen"'); ?>
    <?=cachedFile('forms.css','media="screen"'); ?>
    <?=cachedFile('icons.css','media="screen"'); ?>
    <?=cachedFile('icons_mime.css','media="screen"'); ?>
    <?=cachedFile('jquery-ui-1.8.23.custom.css','media="screen"'); ?>
    <?=cachedFile('jquery.ui.custom.css','media="screen"'); ?>
    <?=cachedFile('layout.css','media="screen"'); ?>
    <?=cachedFile('menus.css','media="screen"'); ?>
    <?=cachedFile('fullcalendar.css','media="screen"'); ?>
    <?=cachedFile('stylesMaster.css','media="screen"'); ?>
    <?=cachedFile('plupload.queue.css','media="screen"'); ?>
    <?=cachedFile('jquery.wysiwyg.css','media="screen"'); ?>
    <?=cachedFile('autoSuggest.css','media="screen"'); ?>
    
    <?
    $cssAssets = array(
        'css/stylesMaster.css',
        'css/plupload.queue.css',
        'css/jquery.wysiwyg.css'
    );
    //$this->assets->add($cssAssets);
    //echo $this->assets->get_styles();
    ?>

    <? if (isset($charts) && $charts == 1) { ?>
    <?=cachedFile('jquery.jqplot.css','media="screen"'); ?>
    <? } ?>
	
	<!-- Print Style Sheet -->
    <?=cachedFile('print.css','media="print"'); ?>
	
	<!--[if IE]>
        <?=cachedFile('excanvas/excanvas.compiled.js'); ?>
	<![endif]-->
    <!--[if IE 8]>
        <?=cachedFile('stylesIE8.css','media="screen"'); ?>
    <![endif]-->

    <!-- RSS Feeds -->
    <?
    if (isset($rssFeeds) && is_array($rssFeeds)) {
        foreach ($rssFeeds as $feed) {
            echo '<link rel="alternate" type="application/rss+xml" title="'.$feed['Title'].'" href="'.$feed['Link'].'">'.Chr(10);
        }
    }
    ?>
    <!-- End RSS Feeds -->

    <!-- start Mixpanel --><script type="text/javascript">var mpq=[];mpq.push(["init","e7a2e26e7548ee0e68788dbb3fc0d72b"]);(function(){var b,a,e,d,c;b=document.createElement("script");b.type="text/javascript";b.async=true;b.src=(document.location.protocol==="https:"?"https:":"http:")+"//api.mixpanel.com/site_media/js/api/mixpanel.js";a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(b,a);e=function(f){return function(){mpq.push([f].concat(Array.prototype.slice.call(arguments,0)))}};d=["init","track","track_links","track_forms","register","register_once","identify","name_tag","set_config"];for(c=0;c<d.length;c++){mpq[d[c]]=e(d[c])}})();</script><!-- end Mixpanel -->
</head>

<?
if ($this->session->userdata('logged') == TRUE) {
    $loggedClass = 'logged';
} else {
    $loggedClass = '';
}

if ($this->session->userdata('headerStyle')) {
    $headerStyle = $this->session->userdata('headerStyle');
} else {
    $headerStyle = 'default';
}
?>
<body id="<?=$pageLayoutClass; ?>" class="page<?=$page; ?><?=$loggedClass; ?>">
    <? if ($this->session->userdata('userType') == USER_TYPE_CLIENT) { ?>
    <input type="hidden" name="clientArea" id="clientArea" value="1" />
    <input type="hidden" name="clientID" id="clientID" value="<?=$this->session->userdata('clientID'); ?>" />
    <input type="hidden" name="clientUserid" id="clientUserid" value="<?=$this->session->userdata('userid'); ?>" />
    <? } ?>
	<div id="header" class="noPrint">
        <div class="blackBar">
            <div class="content">
                <? if ($this->session->userdata('logged') == TRUE) { ?>
                    <?
                    if ($this->session->userdata('userType') != USER_TYPE_CLIENT) {
                        $this->load->view('includes/menuMain');
                    }
                    ?>
                    <div id="headerControls">
                        <div style="float: right;">
                            <ul class="smallButtonCluster">
                                <? if ($this->session->userdata('userType') == USER_TYPE_CLIENT) { ?>
                                <li><a href="<?=site_url('dashboard/DashboardClients'); ?>" title="<?=lang('dashboard_page_title'); ?>" class="headerButton"><span class="dashboard"></span></a></li>
                                <? } else { ?>
                                <li><a href="<?=site_url('dashboard/DashboardOwners'); ?>" title="<?=lang('dashboard_page_title'); ?>" class="headerButton"><span class="dashboard"></span></a></li>
                                <? } ?>
                                
                                <li><a href="<?=site_url('settings/Settings'); ?>" title="<?=lang('settings_page_title'); ?>" class="headerButton"><span class="settings"></span></a></li>
                                
                                <li><a href="<?=site_url('myaccount/MyAccount'); ?>" class="headerButton" title="<?=lang('common_your_account'); ?>"><span class="account"></span></a></li>
                                
                                <li><a href="#" title="<?=lang('common_language'); ?>" class="headerButton popUpMenuClick" content="contentLanguage" id="headerButtonLanguage"><span class="language"></span></a></li>
                                
                                <? if ($this->session->userdata('userType') == USER_TYPE_GOD) { ?>
                                <li><a href="#" title="<?=lang('common_help'); ?>" class="headerButton" content="contentHelp" id="headerButtonHelp"><span class="helpbutton"></span></a></li>
                                <? } ?>
                                
                                <li><a href="#" title="<?=lang('bugreport_page_title'); ?>" class="headerButton popUpMenuClick" content="contentBugs" id="headerButtonBugs"><span class="bugs"></span></a></li>
                                
                                <? if ($this->session->userdata('userType') == USER_TYPE_GOD) { ?>
                                <li><a href="<?=site_url('administration/AdminUsers'); ?>" title="Office Administration" class="headerButton"><span class="admin"></span></a></li>
                                <? } ?>
                                
                                <li><a href="<?=site_url('login/logout'); ?>" title="<?=lang('login_logout'); ?>" class="headerButton"><span class="logout"></span></a></li>
                            </ul>
                            
                            <div style="clear: left;"></div>

                            <div id="contentBugs" class="tooltip">
                                <?=lang('bugreport_instructions'); ?>
                                <textarea class="bugReportContent" style="width: 270px; height: 75px;"></textarea>
                                <button class="smallButton buttonSubmitBugReport" style="float: left;"><span class="decorator bug"><?=lang('bugreport_submit'); ?></span></button>
                                <p style="float: left; margin-left: 12px; width: 0px; display: none;" class="successMessageSmall"><?=lang('bugreport_success'); ?></p>
                            </div>
                            <div id="contentHelp" class="tooltip">Help</div>
                            <div id="contentLanguage" class="tooltip">
                            <?
                            $languages = languages();
                            echo '<ul class="language">';

                            foreach($languages as $key => $value) {
                                $activeState = '';
                                if ($key == $this->session->userdata('language')) {
                                    $activeState = 'active';
                                }
                                echo '<li class="'.$activeState.'"><a href="#" id="lang_'.$key.'" class="languageSelect">'.$value.'</a></li>';
                            }
                            echo '</ul>';
                            ?>
                            </div>
                        <div id="headerControlsMessage"></div>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                <? } else { ?>
                    <p style="color: #fff; font-size: 16px; font-weight: bold; padding-top: 6px; text-align: center;">CreativePro Office is a complete set of online
					office management tools for creative professionals.</p>
                <? } ?>
            </div>
        </div>
        <div class="field <?=$headerStyle; ?>">
            <div class="content <?=$headerStyle; ?>">
                <? if ($this->session->userdata('logged') == TRUE) { ?>
                <div class="accountIdentityContainer">
                    <?
                    $accountLogo = @fopen($this->session->userdata('logoLowRes'),'r');

                    if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
                        $logoURL = site_url('dashboard/DashboardClients');
                    } else {
                        $logoURL = site_url('dashboard/DashboardOwners');
                    }
                    if ($accountLogo == TRUE) {
                        $logoSizeArray = imageResize($this->session->userdata('logoLowRes'),NULL,92,FALSE);
                        $marginTop = floor((104-$logoSizeArray[1])/2);
                        echo '<div style="margin-top: '.$marginTop.'px;"><a href="'.$logoURL.'" title="'.lang('dashboard_page_title').': '.$this->session->userdata('userCompany').'"><img src="'.$this->session->userdata('logoLowRes').'" width="'.$logoSizeArray[0].'" height="'.$logoSizeArray[1].'" border="0" /></a></div>';
                    } else {
                        echo '<a href="'.$logoURL.'" title="'.lang('dashboard_page_title').'"><span>'.$this->session->userdata('userCompany').'</span></a>';
                    }
                    ?>
                </div>
                <div class="userAvatarContainer">
                    <p><? echo $this->session->userdata('fullName').', '.$this->session->userdata('roleText'); ?></p>
                    <?
                    $avatarMember = $this->session->userdata('avatarMember');
                    if (!empty($avatarMember)) { ?>
                        <img class="avatar22 default" src="<?=$this->session->userdata('avatarMember'); ?>" />
                    <? } else { ?>
                        <p class="avatar22 default"></p>
                    <? } ?>
                    <div style="float: left;"></div>
                </div>
                <? }  else { ?>
                    <center><div id="logoCPOTop"></div></center>                    
                <? } ?>
            </div>
        </div>
        <div class="greenBar">
            <div class="content">
                <div id="pageTitleContainer">
                    <h1 class="pageTitle <?=$pageIconClass; ?>">
                    <?
                    if (isset($pageTitleLink)) {
                        echo '<a href="'.$pageTitleLink.'" title="'.$pageTitle.'">'.$pageTitle.'</a>';
                    } else {
                        echo $pageTitle;
                    }
                    if (!empty($pageSubTitle)) {
                        echo ' / <span class="pageSubTitle" id="pageSubTitle">'.$pageSubTitle.'</span>';
                    }
                    ?>
                    </h1>
                </div>
                <div id="pageControls">
                    <div id="pageControlsContainer" style="float: right; margin-right: 6px;">
                        <?
                        if (isset($pageButtons)) {
                            foreach($pageButtons as $button) {
                                echo $button;
                            }
                        }
                        ?>
                    </div>&nbsp;
                </div>
                <? if ($this->session->userdata('logged') == TRUE) { ?>
                <div id="siteSearchContainer">
                    <form action="<?=site_url('search/SiteSearch'); ?>" method="POST" id="formSiteSearch">
                    <input type="text" class="appSearchBox clearOnFocus" id="appSearch" name="appSearch" value="<?=lang('common_search_office'); ?>" />
                    <button class="buttonAppSearch" id="buttonAppSearch"></button>
                    </form>
                </div>
                <? } ?>
            </div>
        </div>
	</div>	
	<div id="mainPageArea">
		<div id="contentArea" class="contentArea">
            <div id="fileViewContainer">
                <div class="barYellow bottom"><div style="padding: 6px;">
                    <button class="smallButton" id="closeFileView" style="float: left;" title="<?=lang('common_back'); ?>"><span class="back"><?=lang('common_back'); ?></span></button>
                    <button class="smallButton" id="downloadFileView" style="float: right;" title="<?=lang('common_download_file'); ?>"><span class="download"><?=lang('common_download_file'); ?></span></button>
                    <div style="clear: both;"></div>
                </div></div>
                <div id="fileViewInfomationContainer"></div>
                <div id="newMessageFile" itemID="" itemType="attachedFile" class="messageContainer"></div>
                <div id="messageListFile" style="margin-top: 6px;"></div>
            </div>