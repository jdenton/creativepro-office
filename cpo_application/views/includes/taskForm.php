<div id="taskFormContainer" class="hide">

    <input type="hidden" id="milestoneID" />
    <input type="hidden" id="taskID" />
    <input type="hidden" id="taskAction" value="add" />
    <input type="hidden" id="taskNo" value="0" />
    <input type="hidden" name="tagHolder" id="tagHolder" value="" />

    <div style="float: left;"><strong><?=lang('task_title'); ?></strong> <span class="subText">(<?=lang('help_task_title'); ?>)</span></div>
    <div style="float: right; height: 25px;" id="taskStatus"></div>
    <div style="clear: both;"></div>
    <input type="text" tabindex="1" class="taskTitle saverField bigText" id="taskTitle" name="taskTitle" style="width: 681px;" />
    
    <div class="mTop12" id="taskFormAdvanced">

        <div class="mBottom18">
            <strong><?=lang('task_description'); ?></strong><br />
            <div style="margin: 1px 0 0 1px;"><textarea tabindex="2" name="taskDescription" id="taskDescription" class="taskElement taskDescription expanding" style="width: 670px; height: 133px;"></textarea></div>
        </div>

        <div class="left mRight24">
            <span class="icon_calendar_small"><strong><?=lang('task_dates'); ?></strong></span>
            <input type="text" tabindex="4" class="taskDates saverField" id="taskDateStart" style="width: 100px; margin-right: 6px;" />
            <input type="text" tabindex="5" class="taskDates saverField" id="taskDateEnd" style="width: 100px;" />
        </div>  
    
        <div class="left mRight24">
            <span class="icon_timer_small"><strong><?=lang('task_hours'); ?></strong></span>
            <input type="text" tabindex="3" class="taskEstimatedTime saverField numberField" id="taskEstimatedTime" style="width: 100px;" />
        </div>

        <div class="left">
            <span class="icon_priority_small"><strong><?=lang('task_priority'); ?></strong></span>
            <div id="rdoTaskPriority" style="margin-top: 3px;">
                <input type="radio" class="taskPriority" id="radioPriorityLow" name="rdoPriority" value="1" /><label for="radioPriorityLow"><span class="icon_priority low"><?=lang('task_priority_low'); ?></span></label>
                <input type="radio" class="taskPriority" id="radioPriorityMedium" name="rdoPriority" value="2" /><label for="radioPriorityMedium"><span class="icon_priority medium"><?=lang('task_priority_medium'); ?></span></label>
                <input type="radio" class="taskPriority" id="radioPriorityHigh" name="rdoPriority" value="3" /><label for="radioPriorityHigh"><span class="icon_priority high"><?=lang('task_priority_high'); ?></span></label>
                <input type="radio" class="taskPriority" id="radioPriorityExtreme" name="rdoPriority" value="4" /><label for="radioPriorityExtreme"><span class="icon_priority extreme"><?=lang('task_priority_extreme'); ?></span></label>
            </div>
            <input type="hidden" name="priority" id="priority" value="1" />
        </div>
        <div style="clear: left;"></div>

        <div class="left mRight24 mTop12">
            <p class="icon_tag" style="margin-bottom: 3px;"><strong><?=lang('client_form_tags'); ?></strong></p>
            <input type="text" name="taskTags" id="taskTags" tabindex="24" value="" />
        </div>
        <div class="left mTop12">
            <p class="icon_client_small" style="margin-bottom: 3px;"><strong><?=lang('task_assigned_to'); ?></strong></p>
            <input type="text" style="width: 425px;" class="autocompleteContactsTask" id="sTeamAutocompleter" />
        </div>
        <div style="clear: left;"></div>
        
        <button class="smallButton fileUploadButton" id="taskFile" itemType="task" tabindex="25" style="margin: 6px 6px 0 0;"><span class="upload"><?=lang('files_attach'); ?></span></button>
        <ul class="fileList noBullet" style="margin-top: 6px;" id="taskFile_fileContainer"></ul>
        <div style="clear: left;"></div>

        <div style="float: right; margin: 6px 6px;">
            <button class="buttonExpand action taskCreate" id="buttonTaskSave" tabindex="26" style="margin-right: 42px;"><span class="save"><?=lang('button_save'); ?></span></button>
            <button class="buttonExpand cancel" id="buttonTaskCancel" tabindex="27"><span class="cancel"><?=lang('button_close'); ?></span></button>
        </div>
        <div style="clear: right;"></div>
    </div>    
</div>    

<div id="milestoneFormContainer" class="hide">
    <div style="padding: 6px;">
        <div style="float: left; margin-right: 8px;">
            <input type="hidden" id="milestoneFormID" />
            <input type="hidden" id="milestoneAction" value="add" />
            <strong><?=lang('task_title'); ?></strong><br />
            <input type="text" tabindex="100" id="milestoneTitle" name="milestoneTitle" class="milestoneSaverField bigText" style="width: 477px;" />
        </div>
        <div style="float: left;">
            <strong><?=lang('task_dates'); ?></strong><br />
            <input type="text" tabindex="101" class="milestoneDates milestoneSaverField bigText" id="milestoneDateStart" style="width: 87px; margin-right: 6px;" />
            <input type="text" tabindex="102" class="milestoneDates milestoneSaverField bigText" id="milestoneDateEnd" style="width: 87px;" />
        </div>
        <div style="clear: left;"></div>

        <div style="float: left; height: 25px;" id="milestoneStatus"></div>
        <div style="float: right; margin: 6px 6px;">
        <button class="buttonExpand action taskCreate" id="buttonMilestoneSave" tabindex="103" style="margin-right: 42px;"><span class="save"><?=lang('button_save'); ?></span></button>
        <button class="buttonExpand cancel" id="buttonMilestoneCancel" tabindex="104"><span class="cancel"><?=lang('button_close'); ?></span></button>
        <div style="clear: both;"></div>
        </div>
        <div style="clear: right;"></div>
    </div>
</div>

