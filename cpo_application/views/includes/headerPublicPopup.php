<?
if (!isset($pageTitle)) {
    $pageTitle = 'CreativePro Office';
} else {
    $pageTitle = $pageTitle.' : CreativePro Office';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title><?=$pageTitle; ?></title>
    <meta name="Description" content="CreativePro Office is a complete set of free online
	office management tools for creative professionals.	Manage your team, clients, projects, invoices, quotes and events from one web-based
	application with nothing to download or install.">
	<meta name="Keywords" content="project management, time tracking, invoices, quotes, tasks, online office, web, software, application, billable hours, creative professional, web developer, graphic artist">
	<meta name="Author" content="Jeff Denton @ UpStart Productions :: Portland OR :: info@UpStart-Productions.com">
	<META NAME="Rating" CONTENT="General">
	<META NAME="ROBOTS" CONTENT="INDEX, NOFOLLOW">
	<META NAME="Distribution" CONTENT="global">

    <!-- Favicon -->
	<link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />

    <!-- Style Sheets -->
	<link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/browserReset.css" type="text/css" />
    <link rel="STYLESHEET" media="screen" href="<?=BASE_URL; ?>css/stylesPublic.css" type="text/css" />

    <!-- Print Style Sheet -->
	<link rel="STYLESHEET" media="print" href="<?=BASE_URL; ?>css/print.css" type="text/css" />
    </head>
<body>
    <div id="pageBody">