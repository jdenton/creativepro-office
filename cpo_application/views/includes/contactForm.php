<form id="formContact" class="smallForm noBorder">
    <input type="hidden" id="contactAction" value="add" />
	<input type="hidden" id="contactID" />
    <input type="hidden" id="contactUID" />
    <div style="float: left; width: 510px;">
       <div style="float: left; width: 200px; margin-right: 20px;">
            <label class="labelVert"><?=lang('common_first_name'); ?></label>
            <input type="text" name="contactNameFirst" id="contactNameFirst" tabindex="200" style="width: 200px;" />
       </div>
       <div style="float: left; width: 203px;">
            <label class="labelVert"><?=lang('common_last_name'); ?></label>
            <input type="text" name="contactNameLast" id="contactNameLast" tabindex="201" style="width: 203px;" />
       </div>
       <div style="clear: left;"></div>

       <div style="float: left; width: 200px; margin-right: 20px;">
            <label class="labelVert"><?=lang('common_title'); ?></label>
            <input type="text" name="contactTitle" id="contactTitle" tabindex="202" style="width: 200px;" />
       </div>
       <div style="float: left; width: 203px;">
            <label class="labelVert"><?=lang('common_company'); ?></label>
            <input type="text" name="contactCompany" id="contactCompany" tabindex="203" style="width: 203px;" />
       </div>
       <div style="clear: left;"></div>
       
       <div id="contactTeamMemberRoleContainer" style="float: left;">
            <label class="labelVert" style="width: 200px;"><?=lang('team_select_role'); ?></label>
            <select id="contactTeamMemberRole" style="width: 212px;" tabindex="204">
				<option value="0"><?=lang('common_field_select'); ?></option>
			</select><br />
            <span class="subText"><?=lang('team_role_people_form'); ?></span>
       </div>
       <div id="contactTeamMemberRateContainer" class="left" style="width: 203px;">
            <label class="labelVert"><?=lang('common_hourly_rate'); ?></label>
            <span class="bigText"><?=getCurrencyMark($this->session->userdata('currency')); ?></span> <input type="text" name="contactRate" id="contactRate" class="numberField" tabindex="205" style="width: 100px;" />
            &nbsp;<?=lang('common_per_hour'); ?>
       </div>
       <div style="clear: left;"></div>

       <div style="width: 500px;" class="mTop24">
            <label class="labelVert"><?=lang('common_phone_number'); ?></label>
            <div id="contactContainerPhone" class="contactPhoneContainer">
                <div id="elementContainerPhone">
	            <input type="text" class="contactPhoneData" name="contactPhoneData0" tabindex="206" style="width: 353px; margin-right: 12px;" />
	       		<select class="contactPhoneLocation" name="contactPhoneLocation0" tabindex="206" style="width: 100px;">
	            	<option value="work"><?=lang('common_work'); ?></option>
	            	<option value="mobile"><?=lang('common_mobile'); ?></option>
	            	<option value="fax"><?=lang('common_fax'); ?></option>
	            	<option value="home"><?=lang('common_home'); ?></option>
	            	<option value="skype"><?=lang('common_skype'); ?></option>
	            	<option value="pager"><?=lang('common_pager'); ?></option>
	            	<option value="other"><?=lang('common_other'); ?></option>
	            </select>
                </div>
            </div>
            <div style="width: 475px; height: 20px;"><a href="#" style="float: right; margin-top: 3px;" tabindex="250" class="addAnother icon_add" id="Phone"><?=lang('common_add_another'); ?></a></div>
       </div>

       <div style="width: 500px;">
            <label class="labelVert"><?=lang('client_form_email'); ?></label>
            <div id="contactContainerEmail" class="contactEmailContainer">
                <div id="elementContainerEmail">
	            <input type="text" class="contactEmailData" tabindex="300" name="contactEmailData0" style="width: 353px; margin-right: 12px;" />
	       		<select class="contactEmailLocation" tabindex="300" name="contactEmailLocation0" style="width: 100px;">
	            	<option value="work"><?=lang('common_work'); ?></option>
	            	<option value="personal"><?=lang('common_personal'); ?></option>
	            	<option value="other"><?=lang('common_other'); ?></option>
	            </select>
                </div>
            </div>
            <div style="width: 475px; height: 20px;"><a href="#" style="float: right; margin-top: 3px;" tabindex="350" class="addAnother icon_add" id="Email"><?=lang('common_add_another'); ?></a></div>
       </div>

       <div style="width: 500px;">
            <label class="labelVert"><?=lang('client_form_url'); ?></label>
            <div id="contactContainerURL" class="contactURLContainer">
                <div id="elementContainerURL">
	            <input type="text" class="contactURLData" tabindex="400" name="contactURLData0" style="width: 353px; margin-right: 12px;" />
	       		<select class="contactURLLocation" tabindex="400" name="contactURLLocation0" style="width: 100px;">
	            	<option value="work"><?=lang('common_work'); ?></option>
	            	<option value="personal"><?=lang('common_personal'); ?></option>
	            	<option value="other"><?=lang('common_other'); ?></option>
	            </select>
                </div>
            </div>
            <div style="width: 475px; height: 20px;"><a href="#" style="float: right; margin-top: 3px;" tabindex="450" class="addAnother icon_add" id="URL"><?=lang('common_add_another'); ?></a></div>
       </div>

       <div style="width: 500px;">
            <label class="labelVert"><?=lang('client_form_im'); ?></label>
       		<div id="contactContainerIM" class="contactIMContainer">
                <div id="elementContainerIM">
	            <input type="text" class="contactIMData" tabindex="500" name="contactIMData0" style="width: 238px; margin-right: 12px;" />
	       		<select class="contactIMProtocol" name="contactIMProtocol0" tabindex="500" style="width: 100px; margin-right: 12px;">
	            	<option value="aim"><?=lang('common_aim'); ?></option>
	            	<option value="yahoo"><?=lang('common_yahoo'); ?></option>
	            	<option value="msn"><?=lang('common_msn'); ?></option>
	            	<option value="skype"><?=lang('common_skype'); ?></option>
	            	<option value="icq"><?=lang('common_icq'); ?></option>
	            	<option value="jabber"><?=lang('common_jabber'); ?></option>
	            	<option value="googletalk"><?=lang('common_googletalk'); ?></option>
	            </select>	
	       		
	       		<select class="contactIMLocation" name="contactIMLocation0" tabindex="500" style="width: 100px;">
	            	<option value="work"><?=lang('common_work'); ?></option>
	            	<option value="personal"><?=lang('common_personal'); ?></option>
	            	<option value="other"><?=lang('common_other'); ?></option>
	            </select>
                </div>
            </div>
            <div style="width: 475px; height: 20px;"><a href="#" style="float: right; margin-top: 3px;" tabindex="550" class="addAnother icon_add" id="IM"><?=lang('common_add_another'); ?></a></div>
       </div>
       
       <div>
       		<label class="labelVert"><?=lang('client_form_address'); ?></label>
       		<textarea name="contactAddress" id="contactAddress" tabindex="551" class="contactAddress" style="width: 468px; height: 30px;"></textarea>
       </div>
       <div>
       		<label class="labelVert"><?=lang('client_form_city'); ?></label>
       		<input type="text" name="contactCity" id="contactCity" tabindex="552" class="contactCity" style="width: 468px;" />
       </div>
       <div style="float: left; width: 200px; margin-right: 20px;">
            <label class="labelVert"><?=lang('client_form_state'); ?></label>
            <input type="text" name="contactState" id="contactState" tabindex="553" style="width: 200px;" />
       </div>
       <div style="float: left; width: 203px;">
            <label class="labelVert"><?=lang('client_form_zip'); ?></label>
            <input type="text" name="contactZip" id="contactZip" tabindex="554" style="width: 248px;" />
       </div>
       <div style="clear: left;"></div>

       <div style="float: left; width: 207px; margin-right: 12px;">
            <label class="labelVert"><?=lang('client_form_country'); ?></label>
            <select name="contactCountry" id="contactCountry" tabindex="555" class="contactCountry" style="width: 207px;">
            	<option><?=lang('form_field_select'); ?></option>
				<? $this->load->view('includes/country.html'); ?>
            </select>
       </div>
       <div style="float: left; width: 203px;">
            <label class="labelVert"><?=lang('client_form_timezone'); ?></label>
            <?=timezone_menu('','tzStyle','contactTimezone',257,556); ?>
       </div>
	   <div style="clear: left;"></div>

	   <div style="width: 207px; margin: 3px 12px 0 0;">
            <label class="labelVert"><?=lang('common_language'); ?></label>
            <select name="contactCountry" id="contactLanguage" tabindex="557" class="contactLanguage" style="width: 207px;">
            	<?
				$languages = languages();
                foreach($languages as $key => $value) {
					echo '<option value="'.$key.'"';
					if ($key == $this->session->userdata('language')) {
						echo ' selected="selected" ';
					}
					echo '>'.$value.'</option>';
                }
				?>
            </select>
       </div> 

	   <div class="buttonContainer" style="margin-left: 4px;">
	      <button class="buttonExpand blueGray action primary" id="buttonSaveContact" tabindex="560" style="float: left; margin-left: -6px;"><span class="save"><?=lang('button_save'); ?></span></button>
		  <button class="buttonExpand yellow cancel secondary" style="margin-left: 62px;" id="buttonCancelContact" tabindex="561"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
          <div style="clear: left;"></div>
       </div>
       <div style="margin-top: 9px;" id="messageContactForm"></div>
  </div>
  <div style="float: left; width: 180px;">
		<div id="containerAvatar" style="margin-left: auto; margin-right: auto; margin-top: 10px;" class="avatar80 default"></div>
		<div style="width: 180px; text-align: center;"><a href="#" id="linkChangeAvatar"><?=lang('common_change_photo'); ?></a></div>
		<div id="avatarUploadPanel" style="display: none;">
			<div>
				<label class="labelVert"><?=lang('common_photo_url'); ?></label>
				<input type="text" id="avatarFilename" style="width: 175px;" /><br />
                <span class="subText"><?=lang('files_avatar_size'); ?></span>
			</div>
			<div style="float: left;">
				<button class="buttonExpand blueGray action" id="buttonUploadAvatar" style="margin: 12px 0 0 0;">
				<span class="upload"><?=lang('files_upload_photo'); ?></span>
				</button>
			</div>
			<div style="float: left; margin: 16px 0 0 6px;">
				<?=help('help_formField_avatar','avatarFilename'); ?>
			</div>            
			<div style="clear: left;"></div>
            <div id="avatarErrorMessage"></div>
		</div>
  </div>
  <div style="clear: left;"></div>
  </form>
