<div id="noteFormContainer" class="hide">
    <input type="hidden" id="actionNote" value="add" />
    <input type="hidden" id="itemIDNote" value="<?=$itemID; ?>" />
    <input type="hidden" id="itemTypeNote" value="<?=$itemType; ?>" />
    <input type="hidden" id="noteID" value="" />
    <textarea name="noteContent" id="noteContentTextarea" style="width: 694px; height: 250px;"></textarea>
    <div style="margin-top: 6px;"><input type="checkbox" id="noteMakePublic" value="1" /> <label for="noteMakePublic" class="normal clickable"><?=lang('notes_show_client'); ?></label></div>
    <button class="smallButton" style="float: left; margin: 6px 6px 0 0;" id="buttonNoteSave"><span class="save"><?=lang('button_save'); ?></span></button>
    <button class="smallButton" style="margin-top: 6px; float: left;" id="buttonNoteCancel"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
    <div style="clear: left; height: 12px;"></div>
</div>
