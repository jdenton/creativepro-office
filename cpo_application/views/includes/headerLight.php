<!DOCTYPE html>
<html lang="en">
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title><?=$pageTitle; ?></title>
	<meta name="Description" content="CreativePro Office is a complete set of online office management tools for creative professionals. Manage your team, clients, projects, invoices, timesheets, and events from one web-based application with nothing to download or install.">
	<meta name="Keywords" content="project management, time tracking, invoices, quotes, tasks, online office, web, software, application, billable hours, creative professional, web developer, graphic artist">
	<meta name="Author" content="Jeff Denton @ UpStart Productions :: Portland OR :: info@UpStart-Productions.com">
	<META NAME="Rating" CONTENT="General">
	<META NAME="Distribution" CONTENT="global">
	<!-- Favicon -->
    <link rel="icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?=BASE_URL; ?>images/favicon.ico" type="image/x-icon" />

    <!-- Style Sheets -->
    <?=cachedFile('browserReset.css','media="screen"'); ?>
    <?=cachedFile('buttons.css','media="screen"'); ?>
    <?=cachedFile('icons.css','media="screen"'); ?>
    <?=cachedFile('alerts.css','media="screen"'); ?>

    <?
    if ($this->session->userdata('gadget') == true || !empty($gadgetType)) {
        echo cachedFile('jquery.ui.css','media="screen"');
        echo cachedFile('stylesLite.css','media="screen"');
        echo cachedFile('fullcalendar.css','media="screen"');
        echo cachedFile('forms.css','media="screen"');
    } else {
        echo cachedFile('stylesSubdomainLogin.css','media="screen"');
    }
    ?>
</head>
<body>
