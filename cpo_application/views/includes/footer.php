	<div style="height: 50px;"></div>
	</div> <!-- End contentArea -->
</div>     <!-- End id=mainContent  -->
<div id="widgetSelector" class="tooltip noPrint" style="width: 400px;">
	<div style="float: left; width: 48%;">
	<?
    $permArray = $this->session->userdata('permissions');
	$availableWidgets = availableWidgets();
	$b=0;
	$rows = ceil(count($availableWidgets)/2);
	for($a=1;$a<=$rows;$a++) {
		$wID   = $availableWidgets[$b]['WID'];
		$wName = $availableWidgets[$b]['WidgetName'];
        $wPermIndex = $availableWidgets[$b]['PermIndex'];
        echo '<a href="#" onClick="widgetRenderObj.widgetMenu(this); return false;" class="icon_'.$wName.'_small activateWidget" id="activate_'.$wName.'" widgetID="'.$wID.'">'.lang('widget_'.$wName).'</a>';
        echo '<div class="widgetListActive" id="active_'.$wName.'">'.lang('widget_'.$wName).'</div>';
        $b++;
	}
	?>
	</div>
	<div style="float: left;">
	<?
	for($a=$b;$a<=count($availableWidgets)-1;$a++) {
		if (is_array($availableWidgets[$b])) {
			$wID   = $availableWidgets[$b]['WID'];
			$wName = $availableWidgets[$b]['WidgetName'];
            $wPermIndex = $availableWidgets[$b]['PermIndex'];
			echo '<a href="#" onClick="widgetRenderObj.widgetMenu(this); return false;" class="icon_'.$wName.'_small activateWidget" id="activate_'.$wName.'" widgetID="'.$wID.'">'.lang('widget_'.$wName).'</a>';
			echo '<div class="widgetListActive" id="active_'.$wName.'">'.lang('widget_'.$wName).'</div>';
			$b++;
		}
	}
	?>
	</div>
	<div style="clear: left;"></div>
</div>

<? if ($this->session->userdata('logged') == TRUE) { ?>
    <!-- Messaging Modal Windows  -->
    <div id="popupGrowlWindow" class="noPrint" style="top: auto; right: 10px; bottom: 50px;">
        <div id="basic-template"><p>#{text}</p></div>
    </div>
    <div id="bigModal" class="noPrint">
        <div class="modalHeader bigModalClose" id="bigModalHeader">
            <h3 id="bigModalHeaderTitleContainer" style="float: left;"></h3>
            <div class="widgetControls" style="float: right; margin-top: -1px;">
                <a class="remove" title="<?=lang('common_close'); ?>" href="#" onClick="return false;" style="margin-left: 10px;"></a>
            </div>
            <div style="clear: both;"></div>
        </div>
        <div id="bigModalBody" class="modalBody">
            <div id="widgetContent_projects" class="widgetContentContainer scroller"></div>
            <div id="widgetContent_notes" class="widgetContentContainer scroller"></div>
            <div id="widgetContent_tasks" class="widgetContentContainer scroller"></div>
            <div id="widgetContent_timer" class="widgetContentContainer"></div>
            <div id="widgetContent_calendar" class="widgetContentContainer scroller"></div>
            <div id="widgetContent_invoices" class="widgetContentContainer scroller"></div>
            <div id="messageContent" class="widgetContentContainer scroller"></div>
            <div id="trashContent" class="widgetContentContainer scroller"><p class="icon_trash_small" style="font-weight: bold;"><?=lang('common_no_trash'); ?></p></div>
        </div>
    </div>

    <!-- ***** APP-WIDE HELP MODAL POPUP ***** -->
    <div id="modalHelp" class="noPrint" style="background: #fff; height: 500px;" title="<?=lang('common_help'); ?>">
        <div class="barYellow bottom">
			<div style="padding: 6px;">
                <input type="text" style="width: 400px;" class="search" id="sHelpAutocompleter" />
            </div>
        </div>
        <div id="helpTopicsContainer"></div>
        <div id="helpContentContainer"></div>
        <div style="clear: left;"></div>
        <button id="buttonHelpModalClose" class="smallButton" style="margin: 6px; float: right;"><span class="close"><?=lang('button_close'); ?></span></button>
        <div style="clear: right;"></div>
    </div>

	<!-- ***** APP-WIDE FILE MANAGER MODAL POPUP ***** -->
	<div id="modalFileUpload" class="noPrint" style="background: #fff; min-width: 600px;" title="<?=lang('files_file_manager'); ?>">
		<input type="hidden" id="fileManagerSelectedFolder" value="0" />

        <? if ($this->session->userdata('accountLevel') == 0) { ?>
        <div class="barYellow bottom">
			<div style="padding: 6px;">
                <? echo lang('help_no_files').' <strong><a href="'.SECURE_URL.'account/changeAccount/'.$this->session->userdata('accountUserid').'">'.lang('help_get_more_storage').'</a></strong>'; ?>
            </div>
        </div>
        <? } else { ?>
		<div class="barYellow bottom">
			<div style="padding: 6px; min-height: 30px;">
                 <div class="icon_folder_open_small" id="fileWindowNavContainer" style="float: left; margin: 6px 0 0 6px;" class="breadCrumb"></div>
                 <div style="float: left; margin-right: 12px; display: none; width: 350px;" class="infoMessageSmall lightText" id="spanButtonPlaceHolderFake">
                    <?
                    printf(lang('help_max_files'),byte_format(getAccountLevelSetting('fileSpace')));
                    echo '<br /><strong><a href="'.SECURE_URL.'account/changeAccount/'.$this->session->userdata('accountUserid').'">'.lang('help_upgrade_account_link').'</a></strong>';
                    ?>
                 </div>
				<div style="float: right;">
					<div style="float: left; margin-right: 12px;">
                        <? if ($this->session->userdata('accountLevel') != 0) { ?>
                        <button class="buttonExpand yellow cancel" id="buttonSiteFileSelect"><span class="attach"><?=lang('files_attach'); ?></span></button>
                        <? } ?>
                    </div>
					<div style="float: left;" class="fileManagerMeter">
                        <div class="fileManagerMeterBar"></div>
                        <div class="fileManagerMeterText subText"></div>
                    </div><br />
					<div style="clear: left;"></div>
				</div>
				<div style="clear: both;"></div>
			</div>	
		</div>
        <div id="fileManagerUploadingContainer" class="dragDropUploadContainer">
            <div id="dropFilesHereFileManager" style="width: 820px; margin-bottom: 1px;" class="fileUploadBox"><?=lang('common_drop_files_here'); ?></div>
        </div>
        <? } ?>
		<div style="background: #fff;">
            <? if ($this->session->userdata('accountLevel') != 0) { ?>
			<div id="fileListContainer"></div>
            <div id="fileUploaderContainer" class="fileUploaderContainer hide"></div>
            <? } ?>
			<div style="clear: both;"></div>
		</div>
	</div>
    
    <!-- ***** APP-WIDE DELETE MODAL POPUP ***** -->
    <div id="modalDelete" class="noPrint" style="background: #fff; min-width: 400px;" title="<?=lang('common_delete'); ?>">
        <div style="padding: 6px;" id="deleteContentsContainer">
            Do you want to delete this?
        </div>
        <button id="buttonDeleteModalDelete" class="smallButton" style="margin: 0 6px 0 6px; float: left;"><span class="delete"><?=lang('common_delete'); ?></span></button>
        <button id="buttonDeleteModalCancel" class="smallButton" style="margin-right: 6px; float: left;"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
        <div style="clear: left;"></div>
    </div>

    <!-- ***** APP-WIDE ACCOUNT LIMIT MODAL POPUP ***** -->
    <div id="modalAccountLimit" class="noPrint" style="background: #fff; width: 300px;" title="<?=lang('common_upgrade_account'); ?>">
        <h2><?=lang('upgrade_account'); ?></h2>
        <div style="padding: 6px;">
            <div style="display: none;" id="maxProjectsContainer">
                <? printf(lang('help_max_projects'),getAccountLevelSetting('projects')); ?>
            </div>
            <div style="display: none;" id="maxTeamMembersContainer">
                <? printf(lang('help_max_team_members'),getAccountLevelSetting('teamMembers')); ?>
            </div>
            <div style="display: none;" id="maxFilesContainer">
                <? printf(lang('help_max_files'),byte_format(getAccountLevelSetting('fileSpace'))); ?>
            </div>

            <div style="margin: 12px 0 12px 0;"><strong><a href="<?=SECURE_URL.'account/changeAccount/'.$this->session->userdata('accountUserid'); ?>"><?=lang('help_upgrade_account_link'); ?></a></strong></div>
            <button id="buttonAccountLimitModalClose" class="smallButton" style="margin-right: 6px; float: left;"><span class="close"><?=lang('button_close'); ?></span></button>
        </div>
    </div>

    <div id="modalWYSIWYGTableContainer">
        <div id="modalWYSIWYGTable" class="hide noPrint" style="padding: 6px; background: #fff; width: 300px;" title="<?=lang('insert_table'); ?>">
            <form class="wysiwyg">
                <table class="layout" style="margin-bottom: 12px;">
                    <tr>
                        <td>Number of columns:</td>
                        <td><input type="text" name="colCount" style="width: 50px;" value="3" /></td>
                    </tr>
                    <tr>
                        <td>Number of rows:</td>
                        <td><input type="text" name="rowCount" style="width: 50px;" value="3" /></td>
                    </tr>
                </table>
                <button class="smallButton submit" style="margin-right: 6px; float: left;"><span class="save"><?=lang('button_save'); ?></span></button>
                <button class="smallButton reset" style="margin-right: 6px; float: left;"><span class="close"><?=lang('button_close'); ?></span></button>
                <div style="clear: left;"></div>
            </form>
        </div>
    </div>

    <div id="modalWYSIWYGImageContainer">
        <div id="modalWYSIWYGImage" class="hide noPrint" style="padding: 6px; background: #fff; width: 300px;" title="<?=lang('insert_image'); ?>">
            <form class="wysiwyg">
                <table class="layout" style="margin-bottom: 12px;">
                    <tr>
                        <td>Image URL:</td>
                        <td><input type="text" name="url" style="width: 250px;" value="http://" /></td>
                    </tr>
                    <tr>
                        <td>Image Title:</td>
                        <td><input type="text" name="imagetitle" style="width: 250px;" value="" /></td>
                    </tr>
                    <tr>
                        <td>Image Description:</td>
                        <td><input type="text" name="description" style="width: 250px;" value="" /></td>
                    </tr>
                </table>
                <button class="smallButton submit" style="margin-right: 6px; float: left;"><span class="save"><?=lang('button_save'); ?></span></button>
                <button class="smallButton reset" style="margin-right: 6px; float: left;"><span class="close"><?=lang('button_close'); ?></span></button>
                <div style="clear: left;"></div>
            </form>
        </div>
    </div>

    <div id="modalInvoiceItems" class="hide noPrint" style="background: #fff; min-width: 400px; min-height: 250px;" title="">
        <input type="hidden" id="modalInvoiceItemType" />
        <div class="barYellow bottom">
            <div id="controlsTimesheetItems" style="padding: 6px;" class="hide">
                <div style="float: left;">
                    <input type="text" style="width: 100px; margin-right: 6px;" class="dateField" id="dateStartTimesheet" />
                    <input type="text" style="width: 100px; margin-right: 6px;" class="dateField" id="dateEndTimesheet" /><br />
                    <span class="subText"><?=lang('help_select_date_range'); ?></span>
                </div>
                <div style="float: left; margin-right: 6px;">
                    <? if (count($teamMembers)>0) { ?>
                    <select id="teamMemberTimesheet" style="width: 200px;">
                        <option value="0"><?=lang('team_select_team_member'); ?></option>
                        <option value="0"></option>
                        <?
                        if (is_array($teamMembers) && count($teamMembers)>0) {
                            foreach($teamMembers as $teamMember) {
                                $fullName = $teamMember['NameFirst'].' '.$teamMember['NameLast'];
                                $UID      = $teamMember['LoginUserid'];
                                $PID      = $teamMember['PID'];
                                echo '<option value="'.$UID.'">'.$fullName.'</option>';
                            }
                        }
                        ?>
                    </select>
                    <? } ?>
                </div>
                <button class="smallButton" id="buttonSearchTimesheets" style="margin-right: 6px; float: left;"><span class="search"><?=lang('common_search'); ?></span></button>
                <div style="clear: left;"></div>
            </div>
            <div id="controlExpenseItems" style="padding: 6px;" class="hide">
                <div style="float: left; margin-right: 24px;">
                    <input type="text" style="width: 220px;" class="search" id="sExpenseAutocompleter" /><br />
                    <span class="subText"><?=lang('finance_expense_search'); ?></span>
                    <div class="loadingInline"></div>
                    <input type="hidden" id="sExpenseID" />
                </div>
                <div style="float: left;">
                    <input type="text" style="width: 80px; margin-right: 6px;" class="dateField" id="dateStartExpense" />
                    <input type="text" style="width: 80px; margin-right: 6px;" class="dateField" id="dateEndExpense" /><br />
                    <span class="subText"><?=lang('help_select_date_range'); ?></span>
                </div>
                <button class="smallButton" id="buttonSearchExpenses" style="margin-right: 6px; float: left;"><span class="search"><?=lang('common_search'); ?></span></button>
                <div style="clear: left;"></div>
            </div>
        </div>
        <div id="modalInvoiceItemsContainer" style="max-height: 300px; overflow: auto;"></div>
        <div class="mTop12 mBottom12">
            <button class="smallButton" id="buttonSaveInvoiceItemsModal" style="margin-right: 6px; float: left;"><span class="add"><?=lang('common_add_selected_items'); ?></span></button>
            <button class="smallButton" id="buttonCloseInvoiceItemsModal" style="margin-right: 6px; float: left;"><span class="close"><?=lang('button_close'); ?></span></button>
            <div style="clear: left;"></div>
        </div>
    </div>

    <!-- ***** FOOTER TOOLBAR ***** -->
	<div id="footerToolbar" class="noPrint">
		<a href="<?=INSECURE_URL; ?>" id="logoCPO" title="CreativePro Office Homepage"></a>
        <?
        if ($this->session->userdata('alerts') != FALSE && $_COOKIE['noCPOAlerts'] != date('Y-m-d')) {
        ?>
        <div id="cpoUpdateBar">
            <? foreach ($this->session->userdata('alerts') as $alert) { ?>
            <div class="scrollerHeadline"><a class="iconAlert" href="<? echo $alert['Link']; ?>" target="_blank"><? echo $alert['Title']; ?></a></div>
            <? } ?>
            <a class="buttonCloseSmall" id="closeFooterAlerts" title="<?=lang('common_close'); ?>" style="float: right; margin: 5px 4px;"></a>

        </div>
        <? } ?>
        
		<div id="pageAlerts">
			<div style="float: left;" id="buttonSiteMail" class="iconMailInactive"><span id="siteMailCount"></span></div>
			<? if ($this->session->userdata('userType') != USER_TYPE_CLIENT && ($permArray['trashViewAll'] == 1 || $permArray['trashViewOwn'])) { ?>
			<div style="float: left;" id="buttonSiteTrash" class="iconTrashInactive" data-help="trash"><span id="siteTrashCount"></span></div>
			<? } ?>
			<div style="float: left;" id="ajaxIndicatorOff"><div id="ajaxIndicatorOn"></div></div>
			<div style="clear: left;"></div>
		</div>
		<div id="footerWidgets" style="margin-right: 36px;">
			<div style="float: left;">
				<div id="footerWidgetBar">
					<ul id="footerWidgetMenu">
                        <?
                        $widgetArray = availableWidgets();
                        foreach($widgetArray as $widget) {
                            if ($widget['InFooter'] == 1) {
                                $widgetType = $widget['WidgetName'];
                                if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
                                    if ($widget['Client'] == 1) {
                                        echo '<li class="icon'.$widgetType.' widgetFooter" title="'.lang('widget_'.$widgetType).'" widgetType="'.$widgetType.'" widgetUser="client"></li>';
                                    }
                                } else {
                                    $displayWidgetIcon = FALSE;
                                    if (count($widget['PermIndex'])<1) {
                                        $displayWidgetIcon = TRUE;
                                        echo '<li class="icon'.$widgetType.' widgetFooter" title="'.lang('widget_'.$widgetType).'" widgetType="'.$widgetType.'" widgetUser="user"></li>';
                                        $displayWidgetIcon = FALSE;
                                    } else {
                                        foreach($widget['PermIndex'] as $index) {
                                            if ($permArray[$index] == 1) {
                                                $displayWidgetIcon = TRUE;
                                            }
                                        }
                                    }
                                    if ($displayWidgetIcon === TRUE) {
                                        echo '<li class="icon'.$widgetType.' widgetFooter" title="'.lang('widget_'.$widgetType).'" widgetType="'.$widgetType.'" widgetUser="user"></li>';
                                    }
                                }
                            }
                        }
                        ?>
                        <? if ($this->session->userdata('userType') != USER_TYPE_CLIENT) { ?>
                        <li class="iconfilemanager widgetFooter fileUploadButton" style="width: 32px;" title="<?=lang('files_file_manager'); ?>" widgetType="filemanager" widgetUser="user"></li>
                        <? } ?>
                    </ul>
				</div>
				<div id="footerWidgetBarEnd"></div>
			</div>
            <!--<div style="float: left; padding: 10px 12px; margin-right: 36px;"><a href="#" class="buttonRoundAdd popUpMenuClick" content="widgetSelector" target="topMiddle" tooltip="bottomMiddle" id="addFooterWidgets"></a></div>-->
		</div>
	</div>
<? } else { ?>
	<!-- End Main Page Content :: Begin Footer -->
	<div id="footer">
		<div class="footerContentWrapper">
			
		</div>
	</div>
	<!-- End Footer -->
<? } ?>

<?=cachedFile('css.js'); ?>
<!-- JQuery -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<?
    $jsAssets = array(
        'js/jsGlobals.js',
        'js/swfobject.js',
        'css/jsNotes.js'
    );
    //$this->assets->add($jsAssets);
    //echo $this->assets->get_scripts();
    ?>
<?
    echo cachedFile('jquery/plugins/jquery.qtip-1.0.0-rc3.js');
    echo cachedFile('jsGlobals.js');
    echo cachedFile('swfobject.js');
    echo cachedFile('json.min.js');
    echo cachedFile('jsNotes.js');
    echo cachedFile('jquery/plugins/jquery.autoSuggest.js');
    
    if (is_array($jsFileArray)) {
        foreach ($jsFileArray as $jsFile) {
            echo cachedFile($jsFile);
        }
    }
?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<?
echo cachedFile('jquery/plugins/jquery.combobox.js');
?>

	<!-- Culture specific datejs file -->
	<?
	$langCode = 'en';
	if ($this->session->userdata('language')) {
		$langCode = $this->session->userdata('language');
	}
	?>
	
	<!-- JQuery plugins should go here -->
<?
    echo cachedFile('moment/moment.js');
    echo cachedFile('moment/livestamp.js');
    echo cachedFile('jquery/plugins/jquery.ui.notify.js');
    echo cachedFile('jquery/plugins/jquery.validate_1.8.min.js');
    echo cachedFile('jquery/plugins/jquery.elastic.js');
    echo cachedFile('jquery/plugins/jquery.ajaxUpload.js');
    echo cachedFile('fullCalendar/fullcalendar-1.4.10-min.js');
    echo cachedFile('jquery/plugins/jquery.cookie.js');
    echo cachedFile('jquery/plugins/jquery.knob.min.js');
    
    if (isset($charts) && $charts == 1) {
        echo '<!-- jQuery Sparkline Charts -->';
        echo cachedFile('jquery/plugins/jquery.sparkline.js');

        if (isset($jqCharts) && $jqCharts == 1) {
            echo '<!-- jQuery jQPlot Charts and Plugins -->';
            echo cachedFile('jquery/plugins/jqplot/jquery.jqplot.min.js');
            echo cachedFile('jquery/plugins/jqplot/plugins/jqplot.barRenderer.min.js');
            echo cachedFile('jquery/plugins/jqplot/plugins/jqplot.categoryAxisRenderer.min.js');
            echo cachedFile('jquery/plugins/jqplot/plugins/jqplot.pointLabels.min.js');
            echo cachedFile('jquery/plugins/jqplot/plugins/jqplot.highlighter.min.js');
            echo cachedFile('jquery/plugins/jqplot/plugins/jqplot.cursor.min.js');
        }
    }

    echo cachedFile('jsDashboardOwners.js');
    
    echo '<!-- PLUpload files -->';
    echo cachedFile('plupload/plupload.full.js');
    echo cachedFile('plupload/jquery.plupload.queue.js');
    echo '<!-- End PLUpload files -->';

    echo cachedFile('jsFileManager.js');
    echo cachedFile('jsAppActivity.js');
    echo cachedFile('jsMessages.js');
    echo cachedFile('jsCommon.js');
    echo cachedFile('jsTrash.js');
    echo cachedFile('jquery/plugins/jquery.commonAppMethods.js');

    if (isset($wysiwyg) && $wysiwyg == 1) {
        echo cachedFile('jwysiwyg/jquery.wysiwyg.min.js');
    }

    if (isset($wysiwygTinyMCE) && $wysiwygTinyMCE == 1) {
        echo cachedFile('tinyMCE3.3.8/jquery.tinymce.js');
        echo cachedFile('tinyMCE3.3.8/tinyMCEConfig.js');
    }
    echo cachedFile('jsCalendar.js');
?>


<? if (isset($map) && $map == 1) { ?>
	<!-- Google Maps stuff -->
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?=GOOGLE_MAP_KEY; ?>" type="text/javascript"></script>
	<!-- End Google Maps stuff -->
<? } ?>

<? $this->load->view('includes/js'); ?>

<!-- Google Analytics -->

<script type="text/javascript">
     var _gaq = _gaq || [];
     _gaq.push(['_setAccount', 'UA-1427307-6']);
     _gaq.push(['_setDomainName', '.mycpohq.com']);
     _gaq.push(['_trackPageview']);

     (function() {
       var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
       ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
       var s = document.getElementsByTagName('script')[0];
       s.parentNode.insertBefore(ga, s);
     })();
</script>

<iframe name="printFrame" id="printFrame" style="width: 0; height: 0;" frameborder="0"></iframe>
<iframe name="exportFrame" id="exportFrame" style="width: 0; height: 0;" frameborder="0"></iframe>
<iframe name="vCardFrame" id="vCardFrame" src="<?=BASE_URL; ?>contacts/AppContacts/makeVcardForm" style="display: none;"></iframe>
</body>
</html>