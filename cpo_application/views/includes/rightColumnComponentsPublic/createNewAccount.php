<div class="whiteBoxContent_300">
    <a href="<?=BASE_URL; ?>pricing" class="buttonNewAccount" style="margin-left: 39px;"><span>Create a new account here!</span></a>

    <ul class="sidebarIcons" style="margin-top: 12px;">
        <li class="iconClockBig bigText">Signup takes less than 1 minute.</li>
        <li class="iconCalendar30DayBig bigText">Free 30 day trial on all packages - no credit card up front.</li>
        <li class="iconPackageBig bigText">Low priced packages loaded with features.</li>
        <li class="iconNoMoneyBig bigText">Free entry level package.</li>
    </ul>
</div>
