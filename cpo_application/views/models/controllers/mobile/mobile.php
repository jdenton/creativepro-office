<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mobile extends Controller {
	function __construct()
	{
		parent::Controller();

	}

    function _init() {
        $data['child'] = TRUE;
        $logged = $this->session->userdata('logged');
        $data['isLogged'] = '0';
        $data['authKey'] = '0';
        if ($logged == TRUE) {
            $data['isLogged'] = '1';
            $data['authKey'] = $this->session->userdata('authKey');
        }
        return $data;
    }

    function index() {
		$data = Mobile::_init();
        $this->load->view('mobile/Mobile',$data);
    }
}
?>