<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Appinit extends Controller {
     function __construct()
	{
          parent::Controller();
          $this->load->model('common/public_site','',true);
          $this->load->model('blog/Blog_model','',true);
          //$this->load->library('twitter');
          $this->load->library('tweet');
	}

	function _init() {
        $data['child'] = FALSE;
        $data['jsFileArray'] = array();
        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(6);

        /*
         * Get latest tweets
         */
        $data['tweets']   = $this->twitter->call('statuses/user_timeline', array('id' => 'cpohq'));
        /*
         * Get latest blog posts
         */
        $data['blogRecentEntries']    = $this->Blog_model->getLatestEntries(7);

        $data['rightColumnComponents'] = array('whosUsingCPO_nobg');
        $data['headerTextImage']       = '';
        $data['showBlogLatest']        = TRUE;
        $data['jsFileArray']           = array('jquery/plugins/jquery.colorbox.js','jquery/plugins/jquery.fixfloat.js');

        noCache();
        return $data;
	}

	function index() {
        $data = Appinit::_init();
        $this->load->view('AppInit',$data);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
