<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ExpenseUpdate extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();

        $this->load->model('common/app_data','AppData',TRUE);
		$this->load->model('finances/expense_update','ExpenseUpdate',TRUE);
		$this->load->model('finances/finance_view','FinanceView',TRUE);
        $this->load->model('finances/finance_view_expenses','FinanceViewExpenses',TRUE);
		$this->load->model('clients/client_view','Clients',TRUE);
        $this->load->model('clients/client_update','ClientUpdate',TRUE);
        $this->load->model('settings/settings_update','SettingsUpdate',TRUE);
        $this->load->model('contacts/Contacts','',TRUE);

		$this->load->library('application/CommonFinance');
        $this->load->library('application/CommonAppData');
	}

	function _init($expenseID=NULL) {
		$data['page']            = 'finances';
		$data['pageTitle']       = $this->lang->line('finance_page_title');
		$data['pageSubTitle']    = $this->lang->line('finance_expense_new');
		$data['wysiwyg']         = 0;
        $data['validate']        = 1;
		$data['map']             = 0;
		$data['jsFileArray']     = array('jsFinances.js','jsExpenseUpdate.js','jsExpenses.js','jsInvoiceUpdate.js');
		$data['pageIconClass']   = 'iconPageFinances';
		$data['pageLayoutClass'] = 'withRightColumn';
        $data['rightColumnComponents'] = array();
		$data['currencyMark']          = getCurrencyMark($this->session->userdata('currency'));

        $data['recurringRange']    = '';
        $data['recurringDays']     = array();

        /*
		 * Create validation messages
		 */
		$messageArray = array(
						'expenseAmount' => lang('form_required_expense_amount'),
						'expenseTitle'  => lang('form_required_expense_title')
						);
		$data['validationJSON'] = json_encode($messageArray);

		/*
		 * Create tag string for expenses
		 */
		$tagArray = $this->AppData->getTags($this->session->userdata('accountUserid'),'expense');
		$data['tags'] = makeTagJSON($tagArray);

		/*
		 * Get client list for select box
		 */
		$data['clients'] = $this->Clients->getClientsForSelect();

        /*
		 * Get expense category list
         */
        $data['categories']  = $this->AppData->getCategories($this->session->userdata('accountUserid'),'expense');
		$data['catJSON']     = makeCategoryJSON($data['categories']);

        /*
         * Get vendors list
         */
        $data['vendors']     = $this->Contacts->getContacts($this->session->userdata('accountUserid'),NULL,NULL,FALSE,TRUE);

        $data['billToDisplay'] = 'style="display: none;"';
        
        if ($expenseID>0) {
            /*
             * We're editing, retrieve and format expense data
             */
            if (!$this->FinanceViewExpenses->checkForValidExpense($expenseID,$this->session->userdata('accountUserid'))) {
                header('Location: '.site_url('finances/FinanceView#windowViewExpenses'));
            } else {
                $data['action'] = 'edit';
                $data['expenseInformation'] = $this->commonfinance->getExpenseDetails($expenseID,'array');
				$data['expenseDate'] = MySQLDateToJS($data['expenseInformation']['DateExpense']);
                $data['Comments']    = DBToField($data['expenseInformation']['Comments']);

                if (!empty($data['expenseInformation']['Recurring'])) {
                    $recurringArray = explode('|',$data['expenseInformation']['Recurring']);
                    $data['recurringRange'] = $recurringArray[0];
                    $recurringDayArray = explode(',',$recurringArray[1]);
                    $a=0;
                    foreach($recurringDayArray as $day) {
                        if (!empty($day)) {
                            $data['recurringDays'][$a] = $day;
                            $a++;
                        }
                    }
                }
            }
            $data['pageSubTitle'] = lang('finance_expense_edit');

            if ($data['expenseInformation']['Reimbursement'] == 'yes') {
                $data['billToDisplay'] = '';
            }

        } else {
            /*
             * Get any account-wide settings for invoices
             */
            $settingsArray = $this->SettingsUpdate->getSettings($this->session->userdata('accountUserid'),'','account','expense');
            if ($settingsArray != FALSE) {
                foreach($settingsArray as &$setting) {
                    $setting['SettingValue'] = DBToField($setting['SettingValue']);
                }
            }
            $data['settingsJSON'] = json_encode($settingsArray);
            $data['expenseInformation'] = null;
            $data['expenseDate']        = MySQLDateToJS(date('Y-m-d'));
            $data['action']             = 'add';
            $data['pageSubTitle']       = lang('finance_expense_new');
        }

		return $data;
	}

	function index($expenseID=NULL) {
		$data = ExpenseUpdate::_init($expenseID);
		$this->load->view('finances/ExpenseUpdate',$data);
	}

    function saveExpense() {
        if (isset($_POST['save']) && $_POST['save'] == 1) {
			if ($this->form_validation->run() == FALSE)	{
				$data = ExpenseUpdate::_init();
				$this->load->view('finances/ExpenseUpdate',$data);
			} else {
                /*
				 * Save data
				 * fieldToDB($string,$escape=TRUE,$linebreak=FALSE,$prepURL=FALSE,$autoLink=FALSE,$autoLinkNewWindow=TRUE)
				 */
                $expenseArray['expenseID']       = $_POST['expenseID'];
                $expenseArray['accountUserid']   = $this->session->userdata('accountUserid');
                $expenseArray['userid']          = $this->session->userdata('userid');
				$expenseArray['clientID']        = $_POST['clientIDHolder'];
                if (isset($_POST['projectID'])) {
                    $expenseArray['projectID']       = $_POST['projectID'];
                } else {
                    $expenseArray['projectID'] = 0;
                }
                if (isset($_POST['invoiceID'])) {
                    $expenseArray['invoiceID']       = $_POST['invoiceID'];
                } else {
                    $expenseArray['invoiceID'] = 0;
                }
                $expenseArray['action']          = $_POST['action'];
				$expenseArray['expenseTitle']    = fieldToDB($this->input->post('expenseTitle', TRUE));
                $expenseArray['expenseAmount']   = fieldToDB($this->input->post('expenseAmount', TRUE));
                $expenseArray['expenseMarkup']   = fieldToDB($this->input->post('expenseMarkup', TRUE));

                $expenseArray['markupType'] = '';
                if (isset($_POST['markupType'])) {
                    $expenseArray['markupType']      = $_POST['markupType'];
                }
                
                /*
                 * Expense category
                 */
                $expenseArray['expenseCategory'] = $this->commonappdata->saveCategory(fieldToDB($this->input->post('expenseCategoryCombo', TRUE)),'expense');
                
                
				if (!empty($_POST['expenseDate'])) {
					$expenseArray['expenseDate'] = jsDateToMySQL($this->input->post('expenseDate', TRUE));
                }
                $expenseArray['expensePaymentMethod'] = $_POST['expensePaymentMethod'];
                $expenseArray['expenseAcctNo']        = fieldToDB($this->input->post('expenseAcctNo', TRUE));
                $expenseArray['expenseReimburse']     = $_POST['expenseReimburse'];
                
                $expenseArray['expenseVendor'] = '';
                if (isset($_POST['expenseVendor'])) {
                    $expenseArray['expenseVendor'] = $_POST['expenseVendor'];
                }
                $expenseArray['expenseTags']  = fieldToDB($this->input->post('expenseTagsHolder', TRUE));
                $expenseArray['expenseTags']  = str_replace(',',' ',$expenseArray['expenseTags']);
                
                $expenseArray['expenseNotes'] = fieldToDB($this->input->post('expenseNotes', TRUE), TRUE,TRUE,FALSE,TRUE);
                
                /*
                 * Recurring expense stuff
                 */
                $expenseArray['expenseRepeats'] = '';
                if ($_POST['expenseRepeats'] != '0') {
                    if ($_POST['expenseRepeats'] == 'week') {
                        $expenseArray['expenseRepeats'] = 'week|';
                        if ($_POST['recurringWeekSU'] == 'Sun') { $expenseArray['expenseRepeats'] .= $_POST['recurringWeekSU'].','; }
                        if ($_POST['recurringWeekMO'] == 'Mon') { $expenseArray['expenseRepeats'] .= $_POST['recurringWeekMO'].','; }
                        if ($_POST['recurringWeekTU'] == 'Tue') { $expenseArray['expenseRepeats'] .= $_POST['recurringWeekTU'].','; }
                        if ($_POST['recurringWeekWE'] == 'Wed') { $expenseArray['expenseRepeats'] .= $_POST['recurringWeekWE'].','; }
                        if ($_POST['recurringWeekTH'] == 'Thu') { $expenseArray['expenseRepeats'] .= $_POST['recurringWeekTH'].','; }
                        if ($_POST['recurringWeekFR'] == 'Fri') { $expenseArray['expenseRepeats'] .= $_POST['recurringWeekFR'].','; }
                        if ($_POST['recurringWeekSA'] == 'Sat') { $expenseArray['expenseRepeats'] .= $_POST['recurringWeekSA'].','; }
                    } elseif ($_POST['expenseRepeats'] == 'month') {
                        $expenseArray['expenseRepeats'] = 'month|';
                        for($a=0;$a<=5;$a++) {
                            if ($_POST['recurringMonthDay'.$a] > 0) {
                                $expenseArray['expenseRepeats'] .= $_POST['recurringMonthDay'.$a].',';
                            }
                        }
                    } elseif ($_POST['expenseRepeats'] == 'year') {
                        $expenseArray['expenseRepeats'] = 'year|';
                        for($a=0;$a<=5;$a++) {
                            if ($_POST['recurringYearMonth'.$a] > 0 && $_POST['recurringYearDay'.$a] > 0) {
                                $expenseArray['expenseRepeats'] .= $_POST['recurringYearMonth'.$a].'-'.$_POST['recurringYearDay'.$a].',';
                            }
                        }
                    }
                }
                $newExpenseID = $this->ExpenseUpdate->saveExpense($expenseArray);

                /*
                 * Save tags
                 */
                if (!empty($expenseArray['expenseTags'])) {
                    $this->commonappdata->saveTags($expenseArray['expenseTags'],'expense');
                }
				header('Location: '.BASE_URL.'finances/FinanceView/viewExpense/'.$newExpenseID);
            }
        } else {
			header('Location: '.BASE_URL.'finances/FinanceView');
		}
    }

    function saveExpenseNotes($expenseID) {
        $expenseNotes = fieldToDB($this->input->post('expenseNotes', TRUE), TRUE,TRUE,FALSE,TRUE);
        $this->ExpenseUpdate->saveExpenseNotes($expenseID,$expenseNotes);
    }
    
    function deleteExpense($expenseID,$tagForDelete=TRUE) {
        $this->ExpenseUpdate->deleteExpense($expenseID,$tagForDelete,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }

    function restoreExpense($expenseID) {
        $this->ExpenseUpdate->restoreExpense($expenseID,$this->session->userdata('userid'));
        $this->commontrash->countItemsInTrash();
    }
}
?>