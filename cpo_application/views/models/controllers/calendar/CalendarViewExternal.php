<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set("memory_limit","32M");

class CalendarViewExternal extends Controller {
	function __construct()
	{
		parent::Controller();

		$this->load->model('calendar/Calendar_view','',true);
        $this->load->model('clients/Client_view','',true);
        $this->load->model('contacts/Contacts','',true);
        $this->load->model('common/app_data','AppData',TRUE);

		$this->load->library('application/CommonAppData');
        $this->load->library('application/CommonCalendar');
        $this->load->helper('download');
	}

    function makeIcalendar($authKey,$calendarID=NULL) {
        $loginArray = checkAuthKeyFromGetRequest($_SERVER['PHP_SELF']);
        $userid        = $loginArray['UID'];
        $accountUserid = $loginArray['UseridAccount'];
        $permissions   = $loginArray['RolePermissions'];
		$langCode      = $loginArray['Language'];
		loadLanguageFiles($langCode);
        $permArray = createPermissionsArray($permissions);

		$error = FALSE;
		if ($calendarID>0) {
            /*
             * Do we have rights to view this calendar?
             */
            $viewAll = TRUE;
            $calendarRights = TRUE;
            if ($permArray['calendarViewAll'] != 1) {
                $viewAll = FALSE;
                $calendarRights = $this->Calendar_view->checkForValidCalendar($calendarID,$userid,$accountUserid,$viewAll);
            }
            if ($calendarRights == TRUE) {
                /*
                 * Then just get events for 1 calendar.
                 * Only get dates that are 1 year in the future.
                 */
                $filename = lang('calendar').'.ics';
                $endDate = new_date_from_days(date('Y-m-d'),365);
                $eventArray = $this->Calendar_view->getCalendarEntryList($calendarID,NULL,$endDate,$loginArray['UseridAccount']);
            } else {
                $error = TRUE;
            }
		} else {
			/*
			 * Get all calendars for this user
			 */
            if ($permArray['calendarViewAll'] == 1) {
                $filename = lang('calendar').'.ics';

                /*
                 * Only get dates that are 1 year in the future.
                 */
                $endDate = new_date_from_days(date('Y-m-d'),365);
                $eventArray = $this->Calendar_view->getCalendarEntryList(NULL,NULL,$endDate,$loginArray['UseridAccount']);
            } else {
                $error = TRUE;
            }
		}

		if ($error == FALSE) {
            $userTimezone = new DateTimeZone(phpTimezones($this->session->userdata('gmtOffset')));

            $data  = "BEGIN:VCALENDAR";
            $data .= "\r\nVERSION:2.0";
            $data .= "\r\nCALSCALE:GREGORIAN";
			$data .= "\r\nPRODID:-//CreativePro Office//Calendar 2.0//".strtoupper($loginArray['Language']);
			$data .= "\r\nX-WR-CALNAME:CreativePro Office";			
			$data .= "\r\nX-ORIGINAL-URL:http://www.creativeprooffice.com";
			$data .= "\r\nX-WR-CALDESC:CreativePro Office Calendar";

            /*
             * Timezone and DST stuff....
             */
            
            $data .= "\r\nX-WR-TIMEZONE:".phpTimezones($this->session->userdata('gmtOffset'));
            /*            
            $data .= "\r\nBEGIN:VTIMEZONE";
            $data .= "\r\nTZID:".phpTimezones($this->session->userdata('gmtOffset'));
            $data .= "\r\nX-LIC-LOCATION:".phpTimezones($this->session->userdata('gmtOffset'));
            $data .= "\r\nBEGIN:DAYLIGHT";
            $data .= "\r\nTZOFFSETFROM:-0800";
            $data .= "\r\nTZOFFSETTO:-0700";
            $data .= "\r\nTZNAME:PDT";
            $data .= "\r\nDTSTART:19700308T020000";
            $data .= "\r\nRRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=2SU";
            $data .= "\r\nEND:DAYLIGHT";

            $data .= "\r\nBEGIN:STANDARD";
            $data .= "\r\nTZOFFSETFROM:-0700";
            $data .= "\r\nTZOFFSETTO:-0800";
            $data .= "\r\nTZNAME:PST";
            $data .= "\r\nDTSTART:19701101T020000";
            $data .= "\r\nRRULE:FREQ=YEARLY;BYMONTH=11;BYDAY=1SU";
            $data .= "\r\nEND:STANDARD";
            $data .= "\r\nEND:VTIMEZONE";            
             * 
             */
            $data .= "\r\nMETHOD:PUBLISH";
			foreach($eventArray as $event) {
                $data .= "\r\nBEGIN:VEVENT";
                $data .= "\r\n".formatIcalData("SUMMARY:".$event['Title']);
                $data .= "\r\n" . formatIcalData("DESCRIPTION:".$event['Description']);

				if ($event['DateEnd'] != '0000-00-00' && $event['DateEnd'] != '0000-00-00 00:00:00') {
					$dateEnd = "\r\nDTEND:".convertMySQLToGMT($event['DateEnd'],'pull','Ymd\THis\Z');
                    //$convertedDate = new DateTime($event['DateEnd'],$userTimezone);
                    //$dateEnd = "\r\nDTEND:".date('Ymd\THis\Z', $convertedDate->format('U'));
				} else {
                    $dateEnd = '';
					//$dateEnd = "\r\nDTEND:";
				}

				$dateStart  = "\r\nDTSTART:".convertMySQLToGMT($event['DateStart'],'pull','Ymd\THis\Z');
                //$convertedDate = new DateTime($event['DateStart'],$userTimezone);
                //$dateStart  = "\r\nDTSTART:".date('Ymd\THis\Z', $convertedDate->format('U'));

                $data .= $dateStart;
                $data .= $dateEnd;
                $data .= "\r\nDTSTAMP:".date('Ymd\THis\Z');
                $data .= "\r\nCREATED:".convertMySQLToGMT($event['DateEntry'],'pull','Ymd\THis\Z');
                $data .= "\r\nUID:".generate_guid()."@creativeprooffice.com";
                $data .= "\r\nTRANSP:OPAQUE";
                $data .= "\r\nEND:VEVENT";
			}

            $data .= "\r\nEND:VCALENDAR";
            iCalHeader($filename);
            echo $data;
			//force_download($filename, $data);
		}
	}

	function viewEvent($authKey,$eventID) {
		$loginArray = checkAuthKeyFromGetRequest($_SERVER['PHP_SELF'],'item');
		$userid        = $loginArray['UID'];
        $accountUserid = $loginArray['UseridAccount'];
        $permissions   = $loginArray['RolePermissions'];
		$langCode      = $loginArray['Language'];
		loadLanguageFiles($langCode);

		/*
		 * Do we have rights to view this event?
		 */
		$permArray = createPermissionsArray($permissions);
		$viewAll = TRUE;
		if ($permArray['calendarViewOwn'] == 1) {
			$viewAll = FALSE;
		}
		$eventArray  = $this->Calendar_view->getEvent($eventID);
		$calendarID = $eventArray[0]['CalendarID'];
		$eventRights = $this->Calendar_view->checkForValidEvent($eventID,$calendarID,$userid,$accountUserid,$viewAll);

		$data['errorMessage']    = '';
		$data['page']            = 'external';
		$data['pageTitle']       = lang('calendar_view_event');
		$data['pageLayoutClass'] = 'withRightColumn';
		$data['pageIconClass']   = 'iconPageCalendar';
		$data['jsFileArray']     = array('jsEventViewExternal.js');
        $data['map']             = 1;
        $data['child']           = TRUE;

        $data['pageButtons']           = array(
                                      /*
                                       * Button text, button title, visual style, extra className, itemID, buttonID, initialState
                                       */
									   'print'       => array(lang('button_print'),lang('button_print'),'green','buttonPrint','','print','')
									);
        if ($this->session->userdata('logged') != TRUE) {
            $loginButton = array(lang('login_button'),lang('login_button'),'yellow','buttonLogin','','login','');
            $data['pageButtons']['login'] = $loginButton;
			/*
			 * Set timezone and DST in session for convertMySQLToGMT method.
			 */
			$this->session->set_userdata('gmtOffset',$loginArray['Timezone']);
			$this->session->set_userdata('daylightSavingsTime',$loginArray['DaylightSavingsTime']);
        }

		if ($eventRights == TRUE) {
            $data['eventInfo'] = array();
			if (isset($eventArray[0]) && is_array($eventArray[0])) {
                $data['eventInfo'] = $eventArray[0];
                
                if ($data['eventInfo']['DateStart'] == '0000-00-00 00:00:00' || $data['eventInfo']['DateStart'] == '0000-00-00') {
                    $dateStart = '';
                } else {
                    $dateStart = convertMySQLToGMT($data['eventInfo']['DateStart'],'pull','M j, g:i a');
                }
                if ($data['eventInfo']['DateEnd'] == '0000-00-00 00:00:00' || $data['eventInfo']['DateEnd'] == '0000-00-00') {
                    $dateEnd = '';
                } else {
                    $dateEnd   = convertMySQLToGMT($data['eventInfo']['DateEnd'],'pull','M j, g:i a');
                }

                $dateString = '';
                if (!empty($dateStart)) {
                    $dateString .= $dateStart;
                }
                if (!empty($dateEnd)) {
                    $dateString .= ' - '.$dateEnd;
                }
                $data['eventInfo']['DateString']    = $dateString;
			} else {
				$data['errorMessage'] = lang('error_event_deleted');
			}
		} else {
			$data['errorMessage'] = lang('error_cant_view_this');
		}
		$this->load->view('calendar/CalendarViewExternal',$data);
	}
}
?>
