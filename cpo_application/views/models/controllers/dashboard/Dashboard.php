<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST);
	}

    function index() {
        if ($this->session->userdata('userType') == USER_TYPE_CLIENT) {
            header('Location: '.site_url('dashboard/DashboardClients'));
        } else {
            header('Location: '.site_url('dashboard/DashboardOwners'));
        }
    }

    function changeAccount($accountUserid) {
        /*
         * Reload account information into session because
         * we've come back here from the secure site.
         */
        $this->load->model('accounts/accounts','',true);
        $this->load->helper('account');
        $accountArray = spinUpAccountSession($accountUserid);
        Dashboard::index();
    }
}
?>