<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TaskView extends Controller {
	function __construct()
	{
		parent::Controller();	
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('tasks/Task_view','',true);
        $this->load->model('timesheets/Timesheet_view','',true);
        $this->load->model('projects/Project_view','',true);
        $this->load->library('application/CommonTask.php');
        $this->load->library('application/CommonEmail');
	}
	
	/**
	 * getTasksForProject : retrieve tasks and milestones for a project
	 *
	 * @access	public
	 * @param	int    $projectID ID of the project for which we want tasks
	 * @param   string $renderType How we want the data returned: 'array' or 'json'
	 * @return	mixed  PHP array of task data or JSON sting
	 */	
	function getTasksForProject($projectID,$renderType=NULL,$return=0) {
        /*
		 * View all tasks?
		 */
		$permissionArray = $this->session->userdata('permissions');
		$viewAll = TRUE;

		if ($permissionArray['taskViewOwn'] == 1) {
            $viewAll = FALSE;
		}

		$milestoneArray = $this->Task_view->getMilestonesForProject($projectID,1,$viewAll,$this->session->userdata('pid'));
		$headerString = lang('common_title').'|'.lang('common_date_start').'|'.lang('common_date_due').'|'.lang('tasks_assigned_to').'|'.lang('common_priority').'|'.lang('status_status');
			
		if ($renderType == 'json') {
			if (count($milestoneArray)>0) {
				$jsonString = '{"HeaderLabels":"'.$headerString.'","MilestonesTasks":[';

				foreach($milestoneArray as $milestone) {
					$milestone = $this->commontask->renderMilestone($milestone,'array');
					$milestone['Tasks'] = $this->commontask->renderTasks($milestone['Tasks'],'array');
					$jsonString .= json_encode($milestone).',';
				}

				$jsonString = substr($jsonString,0,-1).']}';
			} else {
				$jsonString = '0';
			}
			if ($return == 1) {
				return $jsonString;
			} else {
				echo $jsonString;
			}
		} else {
			
		}
	}

     /**
	 * getTasksForMilestone : retrieve tasks for a milestone
	 *
	 * @access	public
	 * @param	int  $milestoneID ID of the milestone (story) for which we want tasks
	 * @return	mixed  PHP array of task data or JSON sting
	 */
     function getTasksForMilestone($milestoneID,$return=0) {
          $taskArray = $this->Task_view->getTasksForMilestone($milestoneID);
          $taskJSON  = TaskView::renderTasks($taskArray,'json');
          if ($return == 1) {
               return $taskJSON;
          } else {
               echo $taskJSON;
          }
     }

    /**
    * getTaskDigestView : retrieve tasks for the task widget and digest view.
    *                     This view shows overdue tasks at the top, task due today,
    *                     tasks due tomorrow, next week, etc. for ALL projects.
    *
    * @access	public
    * @param	int  $milestoneID ID of the milestone (story) for which we want tasks
    * @return	mixed  PHP array of task data or JSON sting
    */
    function getTaskDigestView($renderType='json',$return=0) {
        /*
        * View all tasks?
        */
        $permissionArray = $this->session->userdata('permissions');
        $viewAll = TRUE;
        $viewAllProjects = TRUE;
		 
        if ($permissionArray['taskViewOwn'] == 1) {
            $viewAll = FALSE;
        }
        
        /*
         * Can user only view specified projects? If so,
         * then limit their task view to these projects.
         */
        if ($permissionArray['projectViewOwn'] == 1) {
            /*
             * Get array of projects vieable by this user.
             */
            $viewAllProjects = FALSE;
            $projectArray = $this->Project_view->getProjectList($itemID = $this->session->userdata('pid'),'user','all','ASC',NULL,NULL,FALSE);
            foreach($projectArray as $project) {
                $projectIDArray[] = $project['ProjectID'];
            }
        }        
        
        $taskArray = $this->Task_view->getTaskDigestView($this->session->userdata('pid'),$this->session->userdata('accountUserid'),$viewAll,$viewAllProjects);
		$newTaskArray = array();
		$newTaskArray['TasksOverdue'] = array();
		if (sizeof($taskArray['TasksOverdue'])>0) {
             $j=0;
             foreach($taskArray['TasksOverdue'] as $task) {
                 if ( $viewAllProjects == TRUE || ( $viewAllProjects == FALSE && in_array($task['ProjectID'], $projectIDArray) ) )
                 {
                    $newTaskArray['TasksOverdue'][$j] = $task;

                    $statusArray = getProjectStatus($task['Status'],$task['Date_Start'],$task['Date_End'],TRUE);
                    $newTaskArray['TasksOverdue'][$j]['StatusHuman']   = $statusArray[0];
                    $newTaskArray['TasksOverdue'][$j]['StatusMachine'] = $statusArray[1];
                    $newTaskArray['TasksOverdue'][$j]['StatusText']    = $statusArray[2];
                    $newTaskArray['TasksOverdue'][$j]['Date_End']      = $task['Date_End'];

                    if ($task['Date_End'] != '0000-00-00' && $task['Date_End'] != '0000-00-00 00:00:00') {
                        $taskDateArray = explode(':',$task['Date_End']);
                        if (sizeof($taskDateArray) == 3 && $taskDateArray[2] == '59') {
                            $dateTemplate = 'M j';
                        } else {
                            $dateTemplate = 'M j, g:i a';
                        }
                        $newTaskArray['TasksOverdue'][$j]['DateEndRender'] = convertMySQLToGMT($task['Date_End'],'pull',$dateTemplate);

                    } else {
                        $newTaskArray['TasksOverdue'][$j]['DateEndRender'] = '';
                    }

                    $newTaskArray['TasksOverdue'][$j]['TaskLink']      = site_url('tasks/TaskViewExternal/viewTask/'.$this->session->userdata('persAuthString').'/'.$task['TaskID']);

                    $j++;
                 }   
             }
			 $newTaskArray['TasksOverdueHeader'] = lang('task_overdue');
		 }
		 $newTaskArray['TasksOverdueHeader'] = lang('task_overdue');

		 $newTaskArray['TasksToday'] = array();
         if (sizeof($taskArray['TasksToday'])>0) {
			 $j=0;
			 foreach($taskArray['TasksToday'] as $task) {
                 if ( $viewAllProjects == TRUE || ( $viewAllProjects == FALSE && in_array($task['ProjectID'], $projectIDArray) ) )
                 {
                    $newTaskArray['TasksToday'][$j] = $task;
                    $newTaskArray['TasksToday'][$j]['StatusText']    = lang('status_due_today');
                    $newTaskArray['TasksToday'][$j]['Date_End']      = $task['Date_End'];

                    if ($task['Date_End'] != '0000-00-00' && $task['Date_End'] != '0000-00-00 00:00:00') {
                        $taskDateArray = explode(':',$task['Date_End']);
                        if (sizeof($taskDateArray) == 3 && $taskDateArray[2] == '59') {
                            $dateTemplate = 'M j';
                        } else {
                            $dateTemplate = 'M j, g:i a';
                        }
                        $newTaskArray['TasksToday'][$j]['DateEndRender'] = convertMySQLToGMT($task['Date_End'],'pull',$dateTemplate);
                    } else {
                        $newTaskArray['TasksToday'][$j]['DateEndRender'] = '';
                    }

                    $newTaskArray['TasksToday'][$j]['TaskLink']      = site_url('tasks/TaskViewExternal/viewTask/'.$this->session->userdata('persAuthString').'/'.$task['TaskID']);

                    $j++;
                 }    
			 }			 
		 }
		 $newTaskArray['TasksTodayHeader'] = lang('date_today');

		 $newTaskArray['TasksTomorrow'] = array();
         if (sizeof($taskArray['TasksTomorrow'])>0) {
			 $j=0;
			 foreach($taskArray['TasksTomorrow'] as $task) {
                 if ( $viewAllProjects == TRUE || ( $viewAllProjects == FALSE && in_array($task['ProjectID'], $projectIDArray) ) )
                 {
                    $newTaskArray['TasksTomorrow'][$j] = $task;
                    $newTaskArray['TasksTomorrow'][$j]['StatusText']     = lang('status_due_tomorrow');
                    $newTaskArray['TasksTomorrow'][$j]['Date_End']      = $task['Date_End'];

                    if ($task['Date_End'] != '0000-00-00' && $task['Date_End'] != '0000-00-00 00:00:00') {
                        $taskDateArray = explode(':',$task['Date_End']);
                        if (sizeof($taskDateArray) == 3 && $taskDateArray[2] == '59') {
                            $dateTemplate = 'M j';
                        } else {
                            $dateTemplate = 'M j, g:i a';
                        }
                        $newTaskArray['TasksTomorrow'][$j]['DateEndRender'] = convertMySQLToGMT($task['Date_End'],'pull',$dateTemplate);
                    } else {
                        $newTaskArray['TasksTomorrow'][$j]['DateEndRender'] = '';
                    }

                    $newTaskArray['TasksTomorrow'][$j]['TaskLink']       = site_url('tasks/TaskViewExternal/viewTask/'.$this->session->userdata('persAuthString').'/'.$task['TaskID']);

                    $j++;
                 }   
			 }			 
		 }
		 $newTaskArray['TasksTomorrowHeader'] = lang('date_tomorrow');

		 $newTaskArray['TasksThisWeek'] = array();
         if (sizeof($taskArray['TasksThisWeek'])>0) {
			 $j=0;
			 foreach($taskArray['TasksThisWeek'] as $task) {
                 if ( $viewAllProjects == TRUE || ( $viewAllProjects == FALSE && in_array($task['ProjectID'], $projectIDArray) ) )
                 {
                    $newTaskArray['TasksThisWeek'][$j] = $task;

                    $statusArray = getProjectStatus($task['Status'],$task['Date_Start'],$task['Date_End'],TRUE);
                    $newTaskArray['TasksThisWeek'][$j]['StatusHuman']   = $statusArray[0];
                    $newTaskArray['TasksThisWeek'][$j]['StatusMachine'] = $statusArray[1];
                    $newTaskArray['TasksThisWeek'][$j]['StatusText']    = $statusArray[2];
                    $newTaskArray['TasksThisWeek'][$j]['Date_End']      = $task['Date_End'];

                    if ($task['Date_End'] != '0000-00-00' && $task['Date_End'] != '0000-00-00 00:00:00') {
                        $taskDateArray = explode(':',$task['Date_End']);
                        if (sizeof($taskDateArray) == 3 && $taskDateArray[2] == '59') {
                            $dateTemplate = 'M j';
                        } else {
                            $dateTemplate = 'M j, g:i a';
                        }
                        $newTaskArray['TasksThisWeek'][$j]['DateEndRender'] = convertMySQLToGMT($task['Date_End'],'pull',$dateTemplate);
                    } else {
                        $newTaskArray['TasksThisWeek'][$j]['DateEndRender'] = '';
                    }

                    $newTaskArray['TasksThisWeek'][$j]['TaskLink']      = site_url('tasks/TaskViewExternal/viewTask/'.$this->session->userdata('persAuthString').'/'.$task['TaskID']);

                    $j++;
                 }    
			 }		 
		 }
		 $newTaskArray['TasksThisWeekHeader'] = lang('date_this_week');

		 $newTaskArray['TasksNextWeek'] = array();
         if (sizeof($taskArray['TasksNextWeek'])>0) {
			 $j=0;
			 foreach($taskArray['TasksNextWeek'] as $task) {
                 if ( $viewAllProjects == TRUE || ( $viewAllProjects == FALSE && in_array($task['ProjectID'], $projectIDArray) ) )
                 {
                    $newTaskArray['TasksNextWeek'][$j] = $task;

                    $statusArray = getProjectStatus($task['Status'],$task['Date_Start'],$task['Date_End'],TRUE);
                    $newTaskArray['TasksNextWeek'][$j]['StatusHuman']   = $statusArray[0];
                    $newTaskArray['TasksNextWeek'][$j]['StatusMachine'] = $statusArray[1];
                    $newTaskArray['TasksNextWeek'][$j]['StatusText']    = $statusArray[2];
                    $newTaskArray['TasksNextWeek'][$j]['Date_End']      = $task['Date_End'];

                    if ($task['Date_End'] != '0000-00-00' && $task['Date_End'] != '0000-00-00 00:00:00') {
                        $taskDateArray = explode(':',$task['Date_End']);
                        if (sizeof($taskDateArray) == 3 && $taskDateArray[2] == '59') {
                            $dateTemplate = 'M j';
                        } else {
                            $dateTemplate = 'M j, g:i a';
                        }
                        $newTaskArray['TasksNextWeek'][$j]['DateEndRender'] = convertMySQLToGMT($task['Date_End'],'pull',$dateTemplate);
                    } else {
                        $newTaskArray['TasksNextWeek'][$j]['DateEndRender'] = '';
                    }

                    $newTaskArray['TasksNextWeek'][$j]['TaskLink']      = site_url('tasks/TaskViewExternal/viewTask/'.$this->session->userdata('persAuthString').'/'.$task['TaskID']);

                    $j++;
                 }    
			 }			 
		 }
		 $newTaskArray['TasksNextWeekHeader'] = lang('date_next_week');
         $newTaskArray['TotalTasks'] = $taskArray['TotalTasks'];
         $newTaskArray['NoDataMessage'] = lang('help_tasks_no_tasks_widget');
         $newTaskArray['TasksIncomplete'] = TaskView::countUserTasks('0|1|3|4');
         $newTaskArray['TasksArchive']    = TaskView::countUserTasks('2');

        if ($renderType == 'json') {
            $jsonString = json_encode($newTaskArray);

            if ($return == 1) {
                return $jsonString;
            } else {
                echo $jsonString;
            }
        } elseif ($returnType == 'array') {
            return $taskArray;
        }
    }

    function getTasksForAutoCompleter() {
		$q = fieldToDB($this->input->get('term', TRUE));
        if (!$q) return;
		$tasks = $this->Task_view->getTasksForAutoCompleter(strtolower($q));
		echo $tasks;
	}

    function searchTasks($autocomplete=1,$projectID) {
        $q = fieldToDB($this->input->post('q', TRUE));
        if ($autocomplete == 1) {
            if (!$q) return;
            $tasks = $this->Task_view->searchTasks(strtolower($q),$projectID,$autocomplete);
            echo $tasks;
        } else {

        }
    }   

    function getAllUserTasks($renderType='json',$return=0) {
        echo $this->commontask->getAllUserTasks();
    }

    function getTasksForSelect($projectID=NULL,$renderType='json',$return=1,$flattenMilestones=NULL) {
        if (empty($projectID) || $projectID == 0) {
            $projectID = $_POST['projectID'];
        }

        $doFlattenMilestones = FALSE;
        if ($flattenMilestones == 1) {
            $doFlattenMilestones = TRUE;
        }
        if ($projectID > 0) {
            echo $this->commontask->getTasksForSelect($projectID,$renderType,$return,$doFlattenMilestones);
        } else {
            echo '{}';
        }
    }

    function getMilestonesForSelect($projectID,$renderType='json',$return=1) {
        echo $this->commontask->getMilestonesForSelect($projectID,$renderType,$return);
    }

    function getTask($taskID,$renderType=NULL) {
        $taskJSON = $this->commontask->getTask($taskID,$renderType);
        echo $taskJSON;
    }

    function getTaskStats($projectID,$renderType=NULL) {
        $taskJSON = $this->commontask->getTaskStats($projectID,$renderType);
        echo $taskJSON;
    }
    
    function countUserTasks($status=null) {
        $permissionArray = $this->session->userdata('permissions');
        $viewAll = TRUE;
        $viewAllProjects = TRUE;

        if ($permissionArray['taskViewOwn'] == 1) {
            $viewAll = FALSE;
        }
        if ($permissionArray['projectViewOwn'] == 1) {
            $viewAllProjects = FALSE;
        }
        $taskCount = $this->Task_view->countUserTasks($this->session->userdata('pid'),$viewAll,$status,$this->session->userdata('accountUserid'),$viewAllProjects);
        return $taskCount;
    }
    
    function getAllUserTasksByStatus($status=null,$short=true,$renderType='json',$return=0) {
        $permissionArray = $this->session->userdata('permissions');
        $viewAll = TRUE;
        $viewAllProjects = TRUE;

        if ($permissionArray['taskViewOwn'] == 1) {
            $viewAll = FALSE;
        }
        
        if ($permissionArray['projectViewOwn'] == 1) {
            $viewAllProjects = FALSE;
        }
        
        if ($status == 'incomplete') {
            $status = '0|1|3|4';
        }
        $taskArray = $this->Task_view->getAllUserTasksByStatus($this->session->userdata('pid'),$viewAll,$status,$this->session->userdata('accountUserid'),$viewAllProjects);
        $j=0;
        $newTaskArray = array();
        foreach($taskArray as $task) {
            if (!empty($task['Title'])) {
                $newTaskArray[$j] = $task;
                $statusArray = getProjectStatus($task['Status'],$task['Date_Start'],$task['Date_End'],TRUE);
                $newTaskArray[$j]['StatusHuman']   = $statusArray[0];
                $newTaskArray[$j]['StatusMachine'] = $statusArray[1];
                $newTaskArray[$j]['StatusText']    = $statusArray[2];
                $newTaskArray[$j]['Date_End']      = $task['Date_End'];

                if ($task['Date_End'] != '0000-00-00' && $task['Date_End'] != '0000-00-00 00:00:00') {
                    $taskDateArray = explode(':',$task['Date_End']);
                    if (sizeof($taskDateArray) == 3 && $taskDateArray[2] == '59') {
                        $dateTemplate = 'M j';
                    } else {
                        $dateTemplate = 'M j, g:i a';
                    }
                    $newTaskArray[$j]['DateEndRender'] = convertMySQLToGMT($task['Date_End'],'pull',$dateTemplate);
                } else {
                    $newTaskArray[$j]['DateEndRender'] = '';
                }

                $newTaskArray[$j]['TaskLink'] = site_url('tasks/TaskViewExternal/viewTask/'.$this->session->userdata('persAuthString').'/'.$task['TaskID']);
                $j++;
            }    
        }
        if ($renderType == 'json') {
            $jsonString = json_encode($newTaskArray);

            if ($return == 1) {
                return $jsonString;
            } else {
                echo $jsonString;
            }
        } elseif ($returnType == 'array') {
            return $taskArray;
        }
    }

    function getNumberOfTasksForMilestone($milestoneID,$renderType=NULL) {
        $taskCount = $this->Task_view->getNumberOfTasks($milestoneID,'milestone');
        echo $taskCount['Tasks'];
    }
    
    function getAssignedUsersForTask($taskID) {
        $assignedArray = $this->Task_view->getAssignedUsersForTask($taskID);
        echo json_encode($assignedArray);
    }
}
?>