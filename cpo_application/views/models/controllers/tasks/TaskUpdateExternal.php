<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TaskUpdateExternal extends Controller {
	function __construct()
	{
		parent::Controller();

		$this->load->model('common/app_data','AppData',TRUE);
		$this->load->model('accounts/Accounts','',TRUE);
		$this->load->model('projects/Project_view','',TRUE);
        $this->load->model('contacts/Contacts','',TRUE);
		$this->load->model('tasks/Task_view','',TRUE);
        $this->load->model('tasks/Task_update','',TRUE);

        $this->load->library('application/CommonTask');
        $this->load->library('application/CommonMessages');
        $this->load->library('application/CommonEmail');

        $this->load->helper('zend');
	}

    function saveTasksFromEmail() {
        date_default_timezone_set('UTC');

        $mailArray = array(
			'host'     => 'pop.1and1.com',
			'user'     => 'task@creativeprooffice.com',
			'password' => 'addyG0@SE'
		);

		Zend_Loader::loadClass('Zend_Mail_Storage_Pop3');
		$mail = new Zend_Mail_Storage_Pop3($mailArray);
		$numberOfMessages = $mail->countMessages();

		$messageID = $numberOfMessages;
		for($i=0; $i<$numberOfMessages; $i++) {
			$message = $mail->getMessage($messageID);

            /*
             * Get from address and see which account this is.
             */
            $fromAddress = findText('<','>',$message->from);
            $userArray   = $this->Accounts->getAccountInfoFromEmail($fromAddress);
            $permissions = $userArray['RolePermissions'];
            $permArray   = createPermissionsArray($permissions);
            $taskTitle = $message->subject;

            if ($permArray['taskCreate'] == 1) {                
                foreach ($message->getHeaders() as $name => $value) {
                    if ($name == 'to') {
                        $toAddress = $value;
                    }                    
                }
                $body = '';

                if($message->isMultipart()) {
                    foreach (new RecursiveIteratorIterator($message) as $part) {
                        try {
                            if (strtok($part->contentType, ';') == 'text/plain') {
                                $body = trim($part);
                                break;
                            }
                        } catch (Zend_Mail_Exception $e) {
                            $body = '';
                        }
                    }
                }
                $taskBodyArray = explode(Chr(10),$body);
                $note = '';
                foreach($taskBodyArray as $element) {
                    // Project title or ID
                    if (stripos(trim($element),'P:') !== false || stripos(trim($element),'Project:') !== false) {
                        $elementArray = explode(':',$element);
                        $project = trim($elementArray[1]);
                    }

                    // Priority
                    elseif (stripos(trim($element),'PR:') !== false || stripos(trim($element),'Priority:') !== false) {
                        $elementArray = explode(':',$element);
                        $priority = trim($elementArray[1]);
                    }

                    // Tags
                    elseif (stripos(trim($element),'T:') !== false || stripos(trim($element),'Tags:') !== false) {
                        $elementArray = explode(':',$element);
                        $tags = trim($elementArray[1]);
                    }                   

                    // Due Date
                    elseif (stripos(trim($element),'D:') !== false || stripos(trim($element),'Due:') !== false) {
                        $elementArray = explode(':',$element);
                        $dueDate = trim($elementArray[1]);
                    }

                    // Estimated Hours
                    elseif (stripos(trim($element),'H:') !== false || stripos(trim($element),'Hours:') !== false) {
                        $elementArray = explode(':',$element);
                        $hours = trim($elementArray[1]);
                    }

                    // Note
                    elseif (stripos(trim($element),'N:') !== false || stripos(trim($element),'Note:') !== false) {
                        $elementArray = explode(':',$element);
                        $note .= $elementArray[1].' ';
                    }

                    else {
                        $note .= $element.' ';
                    }
                }                
            }

            if (!is_numeric($project)) {
                /*
                 * Try to get project ID from title
                 */
                $projectArray = $this->Project_view->getProjectFromTitle($project,$userArray['UseridAccount']);
                $projectID = $projectArray['ProjectID'];
            } else {
                $projectID = $project;
            }

            /*
             * Can we even add tasks to this project?
             */
            $canAdd = $this->Project_view->checkForValidProject($projectID,$userArray['UseridAccount']);
            if ($canAdd == true && !empty($taskTitle)) {
                /*
                 * Let's go ahead and add the task
                 */
                if (!empty($dueDate)) {
                    $dueDate = date('Y-m-d H:i:s',strtotime($dueDate));
                    $dueDate = convertMySQLToGMT($dueDate,'push','Y-m-d H:i:s');
                }
                if (!is_numeric($hours)) {
                    $hours = 0;
                }
                if (!is_numeric($priority)) {
                    $priority = 0;
                }
                $taskNumber = $this->Task_update->getNewTaskNumber($projectID);
                $taskArray['action']        = 'add';
                $taskArray['taskID']        = null;
                $taskArray['projectID']     = $projectID;
                $taskArray['milestoneID']   = 0;
                $taskArray['taskTitle']     = fieldToDB($taskTitle);
                $taskArray['accountUserid'] = $userArray['UseridAccount'];
                $taskArray['userid']        = $userArray['UID'];
                $taskArray['taskNumber']    = $taskNumber;
                $taskArray['taskDateStart'] = '';
                $taskArray['taskDateEnd']   = $dueDate;
                $taskArray['description']   = fieldToDB($note,TRUE,TRUE,FALSE,FALSE,TRUE,FALSE);
                $taskArray['estimatedTime'] = $hours;
                $taskArray['priority']      = $priority;
                $taskArray['taskTags']      = $tags;

                $taskIDArray = $this->Task_update->saveTask($taskArray);
            }

            print_r($taskArray);

            /*
             * Delete the email message from server
             */
            //$mail->removeMessage($messageID);
			$messageID = $messageID-1;
		}
    }
}
?>