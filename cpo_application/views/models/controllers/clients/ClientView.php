<?
class ClientView extends Controller {
	var $htmlString;
	
	function __construct()
	{
		parent::Controller();
        checkPermissionsForController($this->router->fetch_class());
		checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
		
		$this->load->model('clients/Client_view','',true);	
		$this->load->model('finances/Finance_view','',true);	
		$this->load->model('projects/Project_view','',true);	
		$this->load->model('contacts/Contacts','',true);
        $this->load->model('messaging/Site_messages','',true);

		$this->load->library('application/CommonAppData');
	}
	
	function _init($tag=NULL) {
		global $subMenuArray;
		$data['page']                  = 'clients';
		$data['pageTitle']             = lang('client_page_title');
		$data['pageSubTitle']          = lang('client_view_page_title');
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsClients.js','jsContacts.js');
		$data['pageIconClass']         = 'iconPageClients';
		$data['pageLayoutClass']       = 'withRightColumn';
		$data['rightColumnComponents'] = array('tagRender');
		$data['tagRenderArray']        = $this->commonappdata->renderTags('client','array');
        $data['tag']                   = $tag;
        $data['pageButtons'] = array();
        array_push($data['pageButtons'], '<button class="buttonExpand buttonAddClient action" id="buttonadd" title="'.lang('menu_new_client').'"><span class="add">'.lang('menu_new_client').'</span></button>');
        
        $permissionArray = $this->session->userdata('permissions');
		$viewAll = TRUE;

		if ($permissionArray['messageViewOwn'] == 1) {
            $viewAll = FALSE;
		}

        $numberMessagesArray = $this->Site_messages->getNumberOfMessagesForSomething(NULL,'client',$this->session->userdata('userid'),$this->session->userdata('accountUserid'),$this->session->userdata('pid'),$viewAll);
		$data['numberMessages'] = $numberMessagesArray['NoOfMessagesTotal'];
		return $data;
	}
	
	function index($tag=NULL) {
		$data = ClientView::_init($tag);
		$this->load->view('clients/ClientView',$data);
	}

    function tag($tag=NULL) {
  		$data = ClientView::_init($tag);
		$this->load->view('clients/ClientView',$data);
	}

    function deleted() {
        $data = ClientView::_init();
        $data['deleteMessage'] = lang('help_client_deleted');
		$this->load->view('clients/ClientView',$data);
    }
	
	function getClientsForUser($tag=NULL) {
		$clientsArray = $this->Client_view->getClientsForUser($this->session->userdata('accountUserid'),null,null,$tag);
        if (count($clientsArray)<1) {
            $jsonString = '{"Clients":[],"NoDataMessage":"'.lang('help_client_no_clients').'"}';
        } else {
            $jsonString = '{"Clients":[';
            foreach($clientsArray as $client) {
                $clientID = $client['ClientID'];
                $clientUserid = $client['ClientUserid'];

                $invoiceTotal = 0;
                $invoiceCount = 0;
                if (!empty($clientUserid) && $clientUserid != 0) {
                    $invoiceTotal = $this->Finance_view->getInvoiceTotals($clientUserid,'client');
                    $invoiceCount = $this->Finance_view->getInvoiceCount($clientUserid,'client',$this->session->userdata('accountUserid'));
                }
                $invoiceTotalString = number_format($invoiceTotal['Total'],2);
                if ($invoiceTotal['Total']>0) {
                    $client['InvoiceTotalString'] = getCurrencyMark($this->session->userdata('currency')).$invoiceTotalString;
                    $client['InvoiceTotal']       = $invoiceTotal['Total'];
                    $client['InvoiceCount']       = $invoiceCount['InvoiceCount'];
                } else {
                    $client['InvoiceTotalString'] = '';
                    $client['InvoiceTotal']       = '0';
                    $client['InvoiceCount']       = '';
                }
			
                $projectTotal = $this->Project_view->getNumberOfProjects($clientID,'client',NULL);
                $projectTotal = $projectTotal['Projects'];
                if ($projectTotal>0) {
                    $client['TotalProjects'] = $projectTotal;
                } else {
                    $client['TotalProjects'] = '0';
                }

                if ($client['Country'] == 0) {
                    $client['Country'] = '';
                }
                $contactArray = $this->Contacts->getContacts($this->session->userdata('userid'),'C',$clientID,FALSE);
                $client['Contacts'] = $contactArray;
                $client['Company']  = stripslashes($client['Company']);

                $jsonString .= json_encode($client).',';
            }
            $jsonString = substr($jsonString,0,-1).']}';
        }
        echo $jsonString;
	}
	
	function searchClients() {
        $q = cleanStringTag(strtolower($this->input->get('term', TRUE)));
		if (!$q) return;
		$clients = $this->Client_view->getClientsForAutoCompleter($q);
		echo $clients;
	}

    function getClientEmail($clientID,$renderType='json',$return=0) {
        $clientEmailArray = $this->Client_view->getClientInformation($clientID);
        $clientEmail    = $clientEmailArray['Email'];
        $clientLanguage = $clientEmailArray['Language'];
        if ($renderType == 'json') {
            $jsonString = '{"ClientEmail":"'.$clientEmail.'","Language":"'.$clientLanguage.'"}';
            if ($return == 0) {
                echo $jsonString;
            } else {
                return $jsonString;
            }
        }
    }

    function getClient($clientID,$renderType='json',$return=0) {
        $clientArray = $this->Client_view->getClientInformation($clientID);
        $htmlOut = '';

        if ($renderType == 'html') {
            $htmlOut .= '<div class="vcard" itemID="30" id="contactHolder'.$clientID.'"><p>';
            $htmlOut .= '<span class="org"><strong><a href="'.site_url('clients/ClientDetail/index/'.$clientID).'">'.$clientArray['Company'].'</a></strong></span><br />';
            
            if (!empty($clientArray['Phone1'])) {
                $htmlOut .= '<span class="icon_phone tel">'.$clientArray['Phone1'].'</span>';
            }
            if (!empty($clientArray['Phone2'])) {
                $htmlOut .= '<span class="icon_phone tel">'.$clientArray['Phone2'].'</span>';
            }
            if (!empty($clientArray['Email'])) {
                $htmlOut .= '<span class="icon_email email"><a href="mailto:'.$clientArray['Email'].'">'.$clientArray['Email'].'</a></span><br />';
            }
            $htmlOut .= '</p></div>';

            if ($return == 0) {
                echo $htmlOut;
            } else {
                return $htmlOut;
            }
        } elseif ($renderType == 'json') {
            if ($return == 0) {
                echo json_encode($clientArray);
            } else {
                return json_encode($clientArray);
            }
        }
    }
}	
?>