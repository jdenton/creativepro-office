<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TimesheetView extends Controller {
    function __construct() {
        parent::Controller();
        checkAuthentication($_POST,$_SERVER);
        loadLanguageFiles();
        
        $this->load->model('timesheets/Timesheet_view','',true);
        $this->load->model('timesheets/Timesheet_update','',true);
        $this->load->model('projects/Project_view','ProjectView',true);
        $this->load->model('clients/client_view','ClientView',true);
        $this->load->model('tasks/Task_view','TaskView',true);
        $this->load->model('tasks/Task_update','TaskUpdate',true);
        $this->load->model('calendar/calendar_view','CalendarView',TRUE);
        $this->load->model('contacts/Contacts','',true);
        $this->load->library('application/CommonTimesheet');        
    }

	function _init($projectID=NULL) {
		$monthNum    = date('m');
		$year        = date('Y');
		$daysInMonth = date('t');

		$data['page']              = 'timesheets';
		$data['pageTitle']         = lang('menu_view_timesheets');
		$data['wysiwyg']           = 0;
        $data['charts']            = 1;
		$data['map']               = 0;
		$data['jsFileArray']       = array('jsTimesheet.js');
		$data['pageIconClass']     = 'iconPageTimesheets';
		$data['pageLayoutClass']   = 'withRightColumn';
        $data['rightColumnComponents'] = array('timesheetSidebar');
        $data['pageButtons'] = array();
        array_push($data['pageButtons'], '<button class="buttonExpand buttonPrint green" id="buttonprint" title="'.lang('button_print').'"><span class="add">'.lang('button_print').'</span></button>');
        array_push($data['pageButtons'], '<button class="buttonExpand buttonExportExcel green" id="buttonexportExcel" title="'.lang('button_export_excel').'"><span class="add">'.lang('button_export_excel').'</span></button>');
        
		$data['clients']  = $this->ClientView->getClientsForSelect();

        /*
         * Get project list
         */
        $permArray = $this->session->userdata('permissions');
        $viewAll = TRUE;
        if ($permArray['projectViewOwn'] == 1) {
            $viewAll = FALSE;
        }
		$data['projects'] = $this->ProjectView->getProjectsForSelect(NULL,$viewAll,$this->session->userdata('pid'));
		$data['searchDateStart'] = $monthNum.'/01/'.$year;
		$data['searchDateEnd']   = $monthNum.'/'.$daysInMonth.'/'.$year;
        $data['teamMembers']     = $this->Contacts->getContacts($this->session->userdata('accountUserid'),NULL,NULL,TRUE);

        noCache();
		return $data;
	}

	function index() {
		$data = TimesheetView::_init();
		$this->load->view('timesheets/TimesheetView',$data);
	}

    function createTimesheetWeek($renderType='json',$return=1,$userid=NULL) {
        $date = $_POST['date'];
		$initDate = FALSE;
		if ($date == 0) {
			$date = date('Y-m-d H:i:s');
			$initDate = TRUE;
		} else {
			$date = jsDateToMySQL($date).' 00:00:00';
		}

        $weekArray = weekFromDate($date,$initDate);
		$weekDays  = $weekArray['weekDays'];
		$prevWeekStart = $weekArray['prevWeekStart'];
		$nextWeekStart = $weekArray['nextWeekStart'];

		/*
		 * Get timesheet records for this week
		 */
		$timeSheetArray = $this->commontimesheet->getTimesheetRecords($weekDays[0]['date'],$weekDays[6]['date'].' 23:59:59',$userid,$this->session->userdata('accountUserid'),NULL,NULL,NULL,'array',1);
        /*
		 * Format timesheet records
		 */
        $dateClockEnd = '';
		$timeSheetArrayFormatted = array();
		$j = -1;
		$i = 0;
		if ($timeSheetArray) {
			foreach ($timeSheetArray as $timeSheetEntry) {
                /*
                 * If we're NOT on the same date, then create a new
                 * set of entries
                 */
                if ($timeSheetEntry['DateClockEnd'] == '0000-00-00 00:00:00') {
                    $timeSheetEntry['DateClockEnd'] = $timeSheetEntry['DateClockStart'];
                }


                $dateEndArray = explode(' ',$timeSheetEntry['DateClockEnd']);
                $dateClockEndTS = $dateEndArray[0];
				if ($dateClockEndTS != $dateClockEnd) {
					$j++;
                    $dateClockEnd = $dateClockEndTS;
					$timeSheetArrayFormatted[$j]['Date'] = $timeSheetEntry['DateClockEnd'];
					$i = 0;
				}
                $timeSheetArrayFormatted[$j]['Entries'][$i]['ProjectID']      = $timeSheetEntry['ProjectID'];
                $timeSheetArrayFormatted[$j]['Entries'][$i]['TaskID']         = $timeSheetEntry['TaskID'];
                $timeSheetArrayFormatted[$j]['Entries'][$i]['ProjectTitle']   = $timeSheetEntry['ProjectTitle'];
                $timeSheetArrayFormatted[$j]['Entries'][$i]['TaskTitle']      = $timeSheetEntry['TaskTitle'];

                $timeSheetArrayFormatted[$j]['Entries'][$i]['ClientID']       = $timeSheetEntry['ClientID'];
                $timeSheetArrayFormatted[$j]['Entries'][$i]['Company']        = $timeSheetEntry['Company'];
                $timeSheetArrayFormatted[$j]['Entries'][$i]['DateClockStart'] = $timeSheetEntry['DateClockStart'];
                $timeSheetArrayFormatted[$j]['Entries'][$i]['DateClockEnd']   = $timeSheetEntry['DateClockEnd'];
                $timeSheetArrayFormatted[$j]['Entries'][$i]['PID']            = $timeSheetEntry['PID'];
                $timeSheetArrayFormatted[$j]['Entries'][$i]['NameFirst']      = $timeSheetEntry['NameFirst'];
                $timeSheetArrayFormatted[$j]['Entries'][$i]['NameLast']       = $timeSheetEntry['NameLast'];

				$timeSheetArrayFormatted[$j]['Entries'][$i]['TimeSheetID']    = $timeSheetEntry['TimeSheetID'];
                if ($timeSheetEntry['DateClockEnd'] == '0000-00-00 00:00:00') {
                    //$timeSheetArrayFormatted[$j]['Entries'][$i]['Date'] = '';
                    $timeSheetArrayFormatted[$j]['Entries'][$i]['Date'] = date('Y-m-d', strtotime($timeSheetEntry['DateClockStart']));
                } else {
                    $timeSheetArrayFormatted[$j]['Entries'][$i]['Date'] = date('Y-m-d', strtotime($timeSheetEntry['DateClockEnd']));
                }
                $timeSheetArrayFormatted[$j]['Entries'][$i]['ElapsedTime']    = $timeSheetEntry['ElapsedTime'];
				$timeSheetArrayFormatted[$j]['Entries'][$i]['Comments']       = DBToField($timeSheetEntry['Comments']);
				$timeSheetArrayFormatted[$j]['Entries'][$i]['Billable']       = $timeSheetEntry['Billable'];
				$i++;
			}
        }

		$jsonString = '{"WeekPrevStart":"'.$prevWeekStart.'","WeekNextStart":"'.$nextWeekStart.'","WeekDates":'.json_encode($weekDays).',"TimesheetEntries":';
		$jsonString .= json_encode($timeSheetArrayFormatted);
		$jsonString .= '}';
		if ($renderType == 'json') {
			$output = $jsonString;
		}
		if ($return == 1) {
			return $output;
		} else {
			echo decode_utf8($output);
		}
    }

	function createTimesheetWeekGrid($renderType='json',$return=1,$userid=NULL) {
		$date = $_POST['date'];
		$initDate = FALSE;
		if ($date == 0) {
			$date = date('Y-m-d G:i:s');
			$initDate = TRUE;
		} else {
			$date = jsDateToMySQL($date).' 00:00:00';
		}

		$weekArray = weekFromDate($date,$initDate);
		$weekDays  = $weekArray['weekDays'];
		$prevWeekStart = $weekArray['prevWeekStart'];
		$nextWeekStart = $weekArray['nextWeekStart'];

		/*
		 * Get timesheet records for this week
		 */
		$timeSheetArray = $this->commontimesheet->getTimesheetRecords($weekDays[0]['date'],$weekDays[6]['date'],$userid,$this->session->userdata('accountUserid'),NULL,NULL,NULL,'array',1,0,'grid');
		
		/*
		 * Format timesheet records
		 */
		$projectID = null;
		$taskID    = null;
		$timeSheetArrayFormatted = array();
		$j = -1;
		$i = 0;
		if ($timeSheetArray) {
			foreach ($timeSheetArray as $timeSheetEntry) {
                if ($timeSheetEntry['ProjectID'] != $projectID || $timeSheetEntry['TaskID'] != $taskID) {
                    /*
                     * Let's enter a new row to the grid.
                     */
					$j++;
					$projectID = $timeSheetEntry['ProjectID'];
					$taskID    = $timeSheetEntry['TaskID'];
                    $start     = $timeSheetEntry['Start'];
                    $stop      = $timeSheetEntry['Stop'];

					$timeSheetArrayFormatted[$j]['ProjectID']    = $projectID;
					$timeSheetArrayFormatted[$j]['TaskID']       = $taskID;
					$timeSheetArrayFormatted[$j]['ProjectTitle'] = $timeSheetEntry['ProjectTitle'];
					$timeSheetArrayFormatted[$j]['TaskTitle']    = $timeSheetEntry['TaskTitle'];
					
					$i = 0;
				}
				$timeSheetArrayFormatted[$j]['Entries'][$i]['TimeSheetID'] = $timeSheetEntry['TimeSheetID'];
				$timeSheetArrayFormatted[$j]['Entries'][$i]['Date']        = date('Y-m-d', strtotime($timeSheetEntry['DateClockEnd']));
				$timeSheetArrayFormatted[$j]['Entries'][$i]['ElapsedTime'] = $timeSheetEntry['ElapsedTime'];
				$timeSheetArrayFormatted[$j]['Entries'][$i]['Comments']    = DBToField($timeSheetEntry['Comments']);
				$timeSheetArrayFormatted[$j]['Entries'][$i]['Billable']    = $timeSheetEntry['Billable'];
				$i++;
			}
        }

		$jsonString = '{"WeekPrevStart":"'.$prevWeekStart.'","WeekNextStart":"'.$nextWeekStart.'","WeekDates":'.json_encode($weekDays).',"TimesheetEntries":';
		$jsonString .= json_encode($timeSheetArrayFormatted);
		$jsonString .= '}';
		if ($renderType == 'json') {
			$output = $jsonString;
		}
		if ($return == 1) {
			return $output;
		} else {
			echo decode_utf8($output);
		}
	}

	function getTimesheetRecords($dateStart=NULL,$dateEnd=NULL,$userid=NULL,$clientID=NULL,$projectID=NULL,$taskID=NULL,$renderType='json',$return=1,$report=0) {
        $this->commontimesheet->getTimesheetRecords($dateStart,$dateEnd,$userid,$this->session->userdata('accountUserid'),$clientID,$projectID,$taskID,$renderType,$return,$report);
	}

    function getTimesheetRecord($timeSheetID,$renderType='json',$return=1) {
        $this->commontimesheet->getTimesheetRecord($timeSheetID,$renderType,$return);
	}

    function saveTimesheetRecordFromJobTimer() {
        $timeSheetArray['action']        = $_POST['action'];
        $timeSheetArray['accountUserid'] = $this->session->userdata('accountUserid');
        $timeSheetArray['userid']        = $this->session->userdata('userid');
        $timeSheetArray['projectID']     = $_POST['projectID'];
        $timeSheetArray['taskID']        = $_POST['taskID'];
        $timeSheetArray['timeSheetID']   = $_POST['timeSheetID'];
        $timeSheetArray['comments']      = fieldToDB($this->input->post('comments', TRUE),TRUE,TRUE);
        $timeSheetArray['elapsedTime']   = fieldToDB($this->input->post('elapsedTime', TRUE));
        $timeSheetArray['billable']      = $_POST['billable'];
        $timeSheetArray['dateTime']      = mysqldate_to_localdate();
        
        if (isset($_POST['dateTime'])) {
            $timeSheetArray['dateTime'] = $_POST['dateTime'];
        } 
        if (isset($_POST['taskTitle']) && !empty($_POST['taskTitle']) && (empty($_POST['taskID']) || $_POST['taskID'] == '00')) {
            /*
             * We're adding a new task.
             */
            $taskArray['action']        = 'add';
            $taskArray['accountUserid'] = $this->session->userdata('accountUserid');
            $taskArray['milestoneID']   = 0;
            $taskArray['projectID']     = $timeSheetArray['projectID'];
            $taskArray['taskTitle']     = $_POST['taskTitle'];
            $taskArray['description']   = '';
            $taskArray['taskDateStart'] = '';
            $taskArray['taskDateEnd']   = '';
            $taskArray['estimatedTime'] = '';
            $taskArray['status']        = '';
            $taskArray['priority']      = '';
            $taskNumber = $this->TaskUpdate->getNewTaskNumber($taskArray['projectID']);
            $taskArray['taskNumber'] = $taskNumber;
            $calendarArray = $this->CalendarView->getCalendarDetailsFromType('task',$this->session->userdata('accountUserid'));
            $taskArray['calendarID']    = $calendarArray['CalendarID'];
            $taskArray['milestone']     = 0;
            $taskArray['tags']          = '';
            $taskArray['userid']        = $this->session->userdata('userid');
            
            $taskReturnArray = $this->TaskUpdate->saveTask($taskArray);
            $timeSheetArray['taskID'] = $taskReturnArray[0];
        }

        $timeSheetID = $this->Timesheet_update->saveTimesheetRecordFromJobTimer($timeSheetArray);

        echo '{"Status":"success","Message":"Time saved.","TimesheetID":"'.$timeSheetID.'","NewTaskID":"'.$timeSheetArray['taskID'].'"}';
    }

    function saveTimesheetRecordFromTimesheet() {
        $timeSheetArray['projectID']     = $_POST['projectID'];
        $timeSheetArray['userid']        = $this->session->userdata('userid');
        $timeSheetArray['accountUserid'] = $this->session->userdata('accountUserid');
		$timeSheetArray['taskID']        = $_POST['taskID'];
		$timeSheetArray['timeSheetID']   = $_POST['timeSheetID'];
		$timeSheetArray['elapsedTime']   = fieldToDB($this->input->post('elapsedTime', TRUE));
        $timeSheetArray['date']          = $_POST['timesheetDate'];
        $timeSheetArray['comments']      = fieldToDB($this->input->post('comments', TRUE));
        $timeSheetArray['billable']      = $_POST['billable'];

        $timeSheetID = $this->Timesheet_update->saveTimesheetRecordFromTimesheet($timeSheetArray);
        $this->commontimesheet->getTimesheetRecord($timeSheetID,'json',0);
    }

	function saveTimesheetComments() {
		$timeSheetID = $_POST['timeSheetID'];
		$billable    = $_POST['billable'];
		$comments    = fieldToDB($this->input->post('comments', TRUE),TRUE,TRUE);

		$this->Timesheet_update->saveTimesheetComments($comments,$billable,$timeSheetID);
        echo '{"TimeSheetID":"'.$timeSheetID.'","ElementID":"'.$_POST['elementID'].'","Date":"'.$_POST['date'].'"}';
	}

	function deleteTimesheetRecord($timeSheetID=NULL) {
		/*
		 * Check for timeSheetIDs in the POST collection
		 */
		if (!empty($_POST['timeSheetIDString'])) {
			$timeSheetIDArray = explode('|',$_POST['timeSheetIDString']);
			foreach($timeSheetIDArray as $timeSheetID) {
				if (!empty($timeSheetID)) {
					$this->Timesheet_update->deleteTimesheetRecord($timeSheetID);
				}
			}
		} else {
			$this->Timesheet_update->deleteTimesheetRecord($timeSheetID);
		}
		return true;
	}
}
?>