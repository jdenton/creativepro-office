<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AppActivity extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST);
        loadLanguageFiles();

		$this->load->model('common/app_activity','',true);
        $this->load->model('projects/project_view','',true);
        $this->load->model('finances/finance_view','',true);
        $this->load->model('clients/client_view','',true);
        $this->load->model('tasks/task_view','',true);
        $this->load->model('messaging/site_messages','',true);
	}

	function getAppActivity($renderType='json',$return=0) {
        $activityArray = $this->app_activity->getAppActivity($this->session->userdata('accountUserid'));
        $activityArrayOut = array();
        $permArray = $this->session->userdata('permissions');
        $accountUserid = $this->session->userdata('accountUserid');
        $userid = $this->session->userdata('userid');
        $viewAll = true;
        
        foreach($activityArray as $activity) {
            $addToCollection = false;
            switch ($activity['ItemType']) {
                case 'project':
                    if ($permArray['projectViewAll'] == 0) {
                        $viewAll = false;
                    }
                    if ($this->project_view->checkForValidProject($activity['ItemID'],$accountUserid,null,$viewAll)) {
                        $addToCollection = true;
                    }
                    break;
                case 'invoice':
                    if ($permArray['invoiceViewAll'] == 0) {
                        $viewAll = false;
                    }
                    if ($this->finance_view->checkForValidInvoice($activity['ItemID'],$userid,$accountUserid,null,$viewAll)) {
                        $addToCollection = true;
                    }
                    break;
                case 'estimate':
                    break;
                case 'expense':
                    break;
                case 'client':
                    if ($this->client_view->checkForValidClient($activity['ItemID'],$accountUserid) && $permArray['clientView'] == 1) {
                        $addToCollection = true;
                    }
                    break;
                case 'task':
                    if ($permArray['taskViewAll'] == 0) {
                        $viewAll = false;
                    }
                    if ($this->task_view->checkForValidTask($activity['ItemID'],$userid,$accountUserid,$viewAll)) {
                        $addToCollection = true;
                    }
                    break;
                case 'comment':
                    if ($permArray['messageViewAll'] == 0) {
                        $viewAll = false;
                    }
                    if ($this->site_messages->checkForValidMessage($activity['ItemID'],$this->session->userdata('pid'),$accountUserid,$viewAll)) {
                        $addToCollection = true;
                    }
                    break;    
            }
            $newDate = convertMySQLToGMT($activity['DateActivity'],$direction='pull');
            $activity['DateActivity'] = $newDate;
            if ($addToCollection == true) {
                array_push($activityArrayOut,$activity);
            }
        }        
        if ($renderType == 'json') {
            $jsonOut = json_encode($activityArrayOut);
            if ($return == 0) {
                echo $jsonOut;
            } else {
                return $jsonOut;
            }
        }
    }
}
?>
