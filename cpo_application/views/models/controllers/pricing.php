<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pricing extends Controller {
	function __construct()
	{
		parent::Controller();
        $this->load->model('common/public_site','',true);
	}

    function _init() {
        $data['child']           = TRUE;
        $data['headerTextImage'] = 'txtPricingSignupHeader_public.png';
        $data['rightColumnComponents'] = array('whosUsingCPO_nobg');
        $data['jsFileArray']           = array();
        $data['pageTitle']             = 'Pricing';

        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        return $data;
    }

    function index() {
        global $accountPricePrev,$accountPriceNext,$datePriceChange;
		$data = Pricing::_init();

        $data['currentPrices'] = $accountPricePrev;
        if (days_between_dates(strtotime($datePriceChange),time())>0) {
            $data['currentPrices'] = $accountPriceNext;
        }

        $this->load->view('public_site/Pricing',$data);
    }   
}
?>