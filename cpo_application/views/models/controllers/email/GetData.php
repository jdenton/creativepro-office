<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class GetData extends Controller {
    function __construct()
	{
		parent::Controller();

		$this->load->model('common/app_data','AppData',TRUE);
		$this->load->model('accounts/Accounts','',TRUE);
        $this->load->model('clients/Client_view','',TRUE);
        $this->load->model('clients/Client_update','',TRUE);
		$this->load->model('projects/Project_view','',TRUE);
        $this->load->model('projects/Project_update','',TRUE);
        $this->load->model('contacts/Contacts','',TRUE);
		$this->load->model('tasks/Task_view','',TRUE);
        $this->load->model('tasks/Task_update','',TRUE);
        $this->load->model('helpdesk/Helpdesk_model','',TRUE);

        $this->load->library('application/CommonTask');
        $this->load->library('application/CommonMessages');
        $this->load->library('application/CommonEmail');
        $this->load->library('application/CommonAppData');
        $this->load->library('twitter');

        include(APPPATH.'libraries/facebook/facebook.php');

        $this->load->helper('zend');
        $this->load->helper('string');
	}

    function saveTasksFromEmail() {
        date_default_timezone_set('UTC');

        $mailArray = array(
			'host'     => 'pop.1and1.com',
			'user'     => 'task@creativeprooffice.com',
			'password' => 'addyG0@SE'
		);

		Zend_Loader::loadClass('Zend_Mail_Storage_Pop3');
		$mail = new Zend_Mail_Storage_Pop3($mailArray);
		$numberOfMessages = $mail->countMessages();

		$messageID = $numberOfMessages;
		for($i=0; $i<$numberOfMessages; $i++) {
			$message = $mail->getMessage($messageID);

            /*
             * Get from address and see which account this is.
             */
            $fromAddress = findText('<','>',$message->from);
            $userArray   = $this->Accounts->getAccountInfoFromEmail($fromAddress);
            $permissions = $userArray['RolePermissions'];
            $permArray   = createPermissionsArray($permissions);
            $taskTitle = $message->subject;

            if ($permArray['taskCreate'] == 1) {
                foreach ($message->getHeaders() as $name => $value) {
                    if ($name == 'to') {
                        $toAddress = $value;
                    }
                }
                $body = '';

                if($message->isMultipart()) {
                    foreach (new RecursiveIteratorIterator($message) as $part) {
                        try {
                            if (strtok($part->contentType, ';') == 'text/plain') {
                                $body = trim($part);
                                break;
                            }
                        } catch (Zend_Mail_Exception $e) {
                            $body = '';
                        }
                    }
                }
                $taskBodyArray = explode(Chr(10),$body);
                $note = '';
                foreach($taskBodyArray as $element) {
                    // Project title or ID
                    if (stripos(trim($element),'P:') !== false || stripos(trim($element),'Project:') !== false) {
                        if (stripos(trim($element),'P:') !== false) {
                            $elementArray = explode('P:',$element);
                        } else {
                            $elementArray = explode('Project:',$element);
                        }
                        $project = trim($elementArray[1]);
                    }

                    // Priority
                    elseif (stripos(trim($element),'PR:') !== false || stripos(trim($element),'Priority:') !== false) {
                        if (stripos(trim($element),'PR:') !== false) {
                            $elementArray = explode('PR:',$element);
                        } else {
                            $elementArray = explode('Priority:',$element);
                        }
                        $priority = trim($elementArray[1]);
                    }

                    // Tags
                    elseif (stripos(trim($element),'T:') !== false || stripos(trim($element),'Tags:') !== false) {
                        if (stripos(trim($element),'T:') !== false) {
                            $elementArray = explode('T:',$element);
                        } else {
                            $elementArray = explode('Tags:',$element);
                        }
                        $tags = trim($elementArray[1]);
                    }

                    // Date Start
                    elseif (stripos(trim($element),'DS:') !== false || stripos(trim($element),'Date Start:') !== false) {
                        if (stripos(trim($element),'DS:') !== false) {
                            $elementArray = explode('DS:',$element);
                        } else {
                            $elementArray = explode('Date Start:',$element);
                        }
                        $dateStart = trim($elementArray[1]);
                    }

                    // Date End
                    elseif (stripos(trim($element),'DE:') !== false || stripos(trim($element),'Date End:') !== false) {
                        if (stripos(trim($element),'DE:') !== false) {
                            $elementArray = explode('DE:',$element);
                        } else {
                            $elementArray = explode('Date End:',$element);
                        }
                        $dateEnd = trim($elementArray[1]);
                    }

                    // Estimated Hours
                    elseif (stripos(trim($element),'H:') !== false || stripos(trim($element),'Hours:') !== false) {
                        if (stripos(trim($element),'H:') !== false) {
                            $elementArray = explode('H:',$element);
                        } else {
                            $elementArray = explode('Hours:',$element);
                        }
                        $hours = trim($elementArray[1]);
                    }

                    else {
                        $note .= $element.' ';
                    }
                }
            }

            if (!is_numeric($project)) {
                /*
                 * Try to get project ID from title
                 */
                $projectArray = $this->Project_view->getProjectFromTitle($project,$userArray['UseridAccount']);
                $projectID = $projectArray['ProjectID'];
            } else {
                $projectID = $project;
            }

            /*
             * Can we even add tasks to this project?
             */
            $canAdd = $this->Project_view->checkForValidProject($projectID,$userArray['UseridAccount']);
            if ($canAdd == true && !empty($taskTitle)) {
                /*
                 * Let's go ahead and add the task
                 */
                if (!empty($dateStart)) {
                    $dateStart = date('Y-m-d H:i:s',strtotime($dateStart));
                    $dateStart = convertMySQLToGMT($dateStart,'push','Y-m-d H:i:s');
                }
                if (!empty($dateEnd)) {
                    $dateEnd = date('Y-m-d H:i:s',strtotime($dateEnd));
                    $dateEnd = convertMySQLToGMT($dateEnd,'push','Y-m-d H:i:s');
                }
                if (!is_numeric($hours)) {
                    $hours = 0;
                }
                if (!is_numeric($priority)) {
                    $priority = 0;
                }
                $taskNumber = $this->Task_update->getNewTaskNumber($projectID);
                $taskArray['action']        = 'add';
                $taskArray['taskID']        = null;
                $taskArray['projectID']     = $projectID;
                $taskArray['milestoneID']   = 0;
                $taskArray['taskTitle']     = fieldToDB($taskTitle);
                $taskArray['accountUserid'] = $userArray['UseridAccount'];
                $taskArray['userid']        = $userArray['UID'];
                $taskArray['taskNumber']    = $taskNumber;
                $taskArray['taskDateStart'] = $dateStart;
                $taskArray['taskDateEnd']   = $dateEnd;
                $taskArray['description']   = fieldToDB($note,TRUE,TRUE,FALSE,FALSE,TRUE,FALSE);
                $taskArray['estimatedTime'] = $hours;
                $taskArray['priority']      = $priority;
                $taskArray['taskTags']      = $tags;

                $taskIDArray = $this->Task_update->saveTask($taskArray);
                $taskID = $taskIDArray[0];

                /*
                 * Save tags
                 */
                if (!empty($taskArray['taskTags'])) {
                    $this->commonappdata->saveTags($taskArray['taskTags'],'task');
                }

                /*
                 * Save team member assignments
                 */
                if (isset($message->cc)) {
                    $teamMembersArray = array();
                    $cc = $message->cc;
                    $ccArray = explode(',',$cc);
                    $a=0;
                    foreach($ccArray as $ccPerson) {
                        $email = findText('<','>',$ccPerson);
                        if (empty($email)) {
                            $email = $ccPerson;
                        }
                        $teamMemberArray = $this->Contacts->getContactFromLoginEmail($email,$userArray['UseridAccount']);
                        $teamMembersArray[$a] = $teamMemberArray;
                        $a++;
                    }
                }
                if (sizeof($teamMembersArray)>0 && $teamMembersArray != FALSE) {
                    foreach($teamMembersArray as $teamMember) {
                        $teamMemberPID      = $teamMember['PID'];
                        $teamMemberLanguage = $teamMember['Language'];

                        if ($teamMemberPID>0) {
                            $this->Task_update->assignUserToTask($teamMemberPID,$taskID);
                            $userArray = $this->Contacts->getContactPerson($teamMemberPID);
                            $teamMemberUID = $userArray['UID'];

                            $messageType = 'taskNew';
                            $messageTypeEmail = 'emailReceiveTask';
                            if ($taskArray['action'] == 'edit') {
                                $messageType = 'taskUpdate';
                                $messageTypeEmail = 'emailReceiveTaskUpdate';
                            }
                            $taskNumberTitle = $projectTitle.': #'.$taskArray['taskNumber'].' '.$taskArray['taskTitle'];
                            $messageArray = $this->commonemail->createSystemMessage($messageType,$teamMemberLanguage);
                            $subject = DBToField(sprintf($messageArray['subject'],$taskNumberTitle));

                            $messageIntro = sprintf($messageArray['message'],$userArray['NameFirst'].' '.$userArray['NameLast'],$taskNumberTitle);
                            $footer = $this->commonemail->createSystemMessage('messageFooter',$teamMemberLanguage);

                            $message = strtoupper($taskNumberTitle).Chr(10);
                            if (!empty($taskArray['description'])) {
                                $message .= Chr(10).$taskArray['description'].Chr(10);
                            }

                            $message .= Chr(10).lang('project_project').': '.$projectTitle;
                            $message .= Chr(10).lang('common_date_due').': '.$_POST['taskDateEnd'];
                            $estimatedHours = '';
                            if ($taskArray['estimatedTime']>0) {
                                $estimatedHours = $taskArray['estimatedTime'].' '.lang('common_hours');
                            }

                            $message .= Chr(10).lang('task_estimated_time').': '.$estimatedHours;
                            $message .= Chr(10).lang('common_priority').': '.$priorityArray[0];
                            $message .= Chr(10).Chr(10).lang('email_task_link').Chr(10);
                            $message .= BASE_URL.'tasks/TaskViewExternal/viewTask/'.$userArray['AuthString'].'/'.$taskID;

                            $finalMessage = DBToField($messageIntro.Chr(10).Chr(10).$message.Chr(10).Chr(10).$footer['message'],TRUE);
                            $this->commonemail->sendEmail($finalMessage,$teamMemberUID,'team',$subject,$messageTypeEmail);
                        }
                    }
                } else {
                    /*
                     * Enter creator as person task is assigned to
                     */
                    $this->Task_update->assignUserToTask($userArray['PID'],$taskID);
                }
            }

            //print_r($taskArray);

            /*
             * Delete the email message from server
             */
            $mail->removeMessage($messageID);
			$messageID = $messageID-1;
		}
    }

    function saveProjectsFromEmail() {
        date_default_timezone_set('UTC');

        $mailArray = array(
			'host'     => 'pop.1and1.com',
			'user'     => 'project@creativeprooffice.com',
			'password' => 'addyG0@SE'
		);

		Zend_Loader::loadClass('Zend_Mail_Storage_Pop3');
		$mail = new Zend_Mail_Storage_Pop3($mailArray);
		$numberOfMessages = $mail->countMessages();

		$messageID = $numberOfMessages;
		for($i=0; $i<$numberOfMessages; $i++) {
			$message = $mail->getMessage($messageID);

            /*
             * Get from address and see which account this is.
             */
            $fromAddress = findText('<','>',$message->from);
            $userArray   = $this->Accounts->getAccountInfoFromEmail($fromAddress);
            $permissions = $userArray['RolePermissions'];
            $permArray   = createPermissionsArray($permissions);
            $projectTitle = $message->subject;

            if ($permArray['projectCreate'] == 1) {
                foreach ($message->getHeaders() as $name => $value) {
                    if ($name == 'to') {
                        $toAddress = $value;
                    }
                }
                $body = '';

                if($message->isMultipart()) {
                    foreach (new RecursiveIteratorIterator($message) as $part) {
                        try {
                            if (strtok($part->contentType, ';') == 'text/plain') {
                                $body = trim($part);
                                break;
                            }
                        } catch (Zend_Mail_Exception $e) {
                            $body = '';
                        }
                    }
                }
                $projectBodyArray = explode(Chr(10),$body);
                $note = '';
                foreach($projectBodyArray as $element) {
                    // Client
                    if (stripos(trim($element),'C:') !== false || stripos(trim($element),'Client:') !== false) {
                        if (stripos(trim($element),'C:') !== false) {
                            $elementArray = explode('C:',$element);
                        } else {
                            $elementArray = explode('Client:',$element);
                        }
                        $client = trim($elementArray[1]);
                    }

                    // Tags
                    elseif (stripos(trim($element),'T:') !== false || stripos(trim($element),'Tags:') !== false) {
                        if (stripos(trim($element),'T:') !== false) {
                            $elementArray = explode('T:',$element);
                        } else {
                            $elementArray = explode('Tags:',$element);
                        }
                        $tags = trim($elementArray[1]);
                    }

                    // Date Start
                    elseif (stripos(trim($element),'DS:') !== false || stripos(trim($element),'Date Start:') !== false) {
                        if (stripos(trim($element),'DS:') !== false) {
                            $elementArray = explode('DS:',$element);
                        } else {
                            $elementArray = explode('Date Start:',$element);
                        }
                        $dateStart = trim($elementArray[1]);
                    }

                    // Date End
                    elseif (stripos(trim($element),'DE:') !== false || stripos(trim($element),'Date End:') !== false) {
                        if (stripos(trim($element),'DE:') !== false) {
                            $elementArray = explode('DE:',$element);
                        } else {
                            $elementArray = explode('Date End:',$element);
                        }
                        $dateEnd = trim($elementArray[1]);
                    }

                    // Status
                    elseif (stripos(trim($element),'S:') !== false || stripos(trim($element),'Status:') !== false) {
                        if (stripos(trim($element),'S:') !== false) {
                            $elementArray = explode('S:',$element);
                        } else {
                            $elementArray = explode('Status:',$element);
                        }
                        $status = trim($elementArray[1]);
                    }

                    // Link URL
                    elseif (stripos(trim($element),'L:') !== false || stripos(trim($element),'Link:') !== false) {
                        if (stripos(trim($element),'L:') !== false) {
                            $elementArray = explode('L:',$element);
                        } else {
                            $elementArray = explode('Link:',$element);
                        }
                        $link = trim($elementArray[1]);
                    }

                    else {
                        $note .= $element.' ';
                    }
                }
            }

            if (!is_numeric($client)) {
                /*
                 * Try to get client ID from client name
                 */
                $clientArray = $this->Client_view->getClientFromCompany($client,$userArray['UseridAccount']);
                $clientID = $clientArray['ClientID'];

                if ($clientArray === false) {
                    /*
                     * Create client
                     */
                    $clientArray = array(
                        'userid'        => $userArray['UseridAccount'],
                        'clientCompany' => $client,
                        'action'        => 'add',
                        'clientCategory'=> '',
                        'clientAddress' => '',
                        'clientCity'    => '',
                        'clientState'   => '',
                        'clientZip'     => '',
                        'clientCountry' => '',
                        'clientPhone1'  => '',
                        'clientPhone2'  => '',
                        'clientPhone3'  => '',
                        'clientEmail'   => '',
                        'clientURL'     => '',
                        'clientNotes'   => '',
                        'clientArchive' => '',
                        'clientStatus'  => '',
                        'clientIndustry'=> '',
                        'clientSince'   => '',
                        'clientTimezone'=> '',
                        'clientLanguage'=> '',
                        'clientTags'    => ''
                    );
                    $clientID = $this->Client_update->saveClient($clientArray);
                }
            } else {
                $clientID = $client;
            }

            if (!empty($clientID) && !empty($projectTitle)) {
                /*
                 * Let's go ahead and add the project
                 */
                if (!empty($dateStart)) {
                    $dateStart = date('Y-m-d H:i:s',strtotime($dateStart));
                    $dateStart = convertMySQLToGMT($dateStart,'push','Y-m-d H:i:s');
                }
                if (!empty($dateEnd)) {
                    $dateEnd = date('Y-m-d H:i:s',strtotime($dateEnd));
                    $dateEnd = convertMySQLToGMT($dateEnd,'push','Y-m-d H:i:s');
                }
                if (!is_numeric($status)) {
                    $status = 0;
                }
                $projectArray['action']         = 'add';
                $projectArray['clientID']       = $clientID;
				$projectArray['projectTitle']   = fieldToDB($projectTitle);
				$projectArray['projectNotes']   = fieldToDB(strip_tags($note), TRUE,TRUE,FALSE,TRUE);
				$projectArray['projectURL']     = fieldToDB($link, TRUE,FALSE,TRUE);
                $projectArray['projectDateStart'] = $dateStart;
                $projectArray['projectDateEnd']   = $dateEnd;
                $projectArray['projectCategory']  = '';
                $projectArray['projectStatus']    = $status;
                $projectArray['projectTags']      = fieldToDB($tags);
                $projectArray['projectTemplateID']= '';
                $projectArray['userid']           = $userArray['UID'];
                $projectArray['accountUserid']    = $userArray['UseridAccount'];

                $newProjectID = $this->Project_update->saveProject($projectArray);

                /*
                 * Save tags
                 */
                if (!empty($projectArray['projectTags'])) {
                    $this->commonappdata->saveTags($projectArray['projectTags'],'project');
                }
            }

            //print_r($projectArray);

            /*
             * Delete the email message from server
             */
            $mail->removeMessage($messageID);
			$messageID = $messageID-1;
		}
    }

    function saveEventsFromEmail() {
        date_default_timezone_set('UTC');

        $mailArray = array(
			'host'     => 'pop.1and1.com',
			'user'     => 'calendar@creativeprooffice.com',
			'password' => 'addyG0@SE'
		);

		Zend_Loader::loadClass('Zend_Mail_Storage_Pop3');
		$mail = new Zend_Mail_Storage_Pop3($mailArray);
		$numberOfMessages = $mail->countMessages();

		$messageID = $numberOfMessages;
		for($i=0; $i<$numberOfMessages; $i++) {
			$message = $mail->getMessage($messageID);

            /*
             * Get from address and see which account this is.
             */
            $fromAddress = findText('<','>',$message->from);
            $userArray   = $this->Accounts->getAccountInfoFromEmail($fromAddress);
            $permissions = $userArray['RolePermissions'];
            $permArray   = createPermissionsArray($permissions);
            $taskTitle = $message->subject;

            if ($permArray['taskCreate'] == 1) {
                foreach ($message->getHeaders() as $name => $value) {
                    if ($name == 'to') {
                        $toAddress = $value;
                    }
                }
                $body = '';

                if($message->isMultipart()) {
                    foreach (new RecursiveIteratorIterator($message) as $part) {
                        try {
                            if (strtok($part->contentType, ';') == 'text/plain') {
                                $body = trim($part);
                                break;
                            }
                        } catch (Zend_Mail_Exception $e) {
                            $body = '';
                        }
                    }
                }
                $taskBodyArray = explode(Chr(10),$body);
                $note = '';
                foreach($taskBodyArray as $element) {
                    // Project title or ID
                    if (stripos(trim($element),'P:') !== false || stripos(trim($element),'Project:') !== false) {
                        $elementArray = explode(':',$element);
                        $project = trim($elementArray[1]);
                    }

                    // Priority
                    elseif (stripos(trim($element),'PR:') !== false || stripos(trim($element),'Priority:') !== false) {
                        $elementArray = explode(':',$element);
                        $priority = trim($elementArray[1]);
                    }

                    // Tags
                    elseif (stripos(trim($element),'T:') !== false || stripos(trim($element),'Tags:') !== false) {
                        $elementArray = explode(':',$element);
                        $tags = trim($elementArray[1]);
                    }

                    // Due Date
                    elseif (stripos(trim($element),'D:') !== false || stripos(trim($element),'Due:') !== false) {
                        $elementArray = explode(':',$element);
                        $dueDate = trim($elementArray[1]);
                    }

                    // Estimated Hours
                    elseif (stripos(trim($element),'H:') !== false || stripos(trim($element),'Hours:') !== false) {
                        $elementArray = explode(':',$element);
                        $hours = trim($elementArray[1]);
                    }

                    // Note
                    elseif (stripos(trim($element),'N:') !== false || stripos(trim($element),'Note:') !== false) {
                        $elementArray = explode(':',$element);
                        $note .= $elementArray[1].' ';
                    }

                    else {
                        $note .= $element.' ';
                    }
                }
            }

            if (!is_numeric($project)) {
                /*
                 * Try to get project ID from title
                 */
                $projectArray = $this->Project_view->getProjectFromTitle($project,$userArray['UseridAccount']);
                $projectID = $projectArray['ProjectID'];
            } else {
                $projectID = $project;
            }

            /*
             * Can we even add tasks to this project?
             */
            $canAdd = $this->Project_view->checkForValidProject($projectID,$userArray['UseridAccount']);
            if ($canAdd == true && !empty($taskTitle)) {
                /*
                 * Let's go ahead and add the task
                 */
                if (!empty($dueDate)) {
                    $dueDate = date('Y-m-d H:i:s',strtotime($dueDate));
                    $dueDate = convertMySQLToGMT($dueDate,'push','Y-m-d H:i:s');
                }
                if (!is_numeric($hours)) {
                    $hours = 0;
                }
                if (!is_numeric($priority)) {
                    $priority = 0;
                }
                $taskNumber = $this->Task_update->getNewTaskNumber($projectID);
                $taskArray['action']        = 'add';
                $taskArray['taskID']        = null;
                $taskArray['projectID']     = $projectID;
                $taskArray['milestoneID']   = 0;
                $taskArray['taskTitle']     = fieldToDB($taskTitle);
                $taskArray['accountUserid'] = $userArray['UseridAccount'];
                $taskArray['userid']        = $userArray['UID'];
                $taskArray['taskNumber']    = $taskNumber;
                $taskArray['taskDateStart'] = '';
                $taskArray['taskDateEnd']   = $dueDate;
                $taskArray['description']   = fieldToDB($note,TRUE,TRUE,FALSE,FALSE,TRUE,FALSE);
                $taskArray['estimatedTime'] = $hours;
                $taskArray['priority']      = $priority;
                $taskArray['taskTags']      = $tags;

                $taskIDArray = $this->Task_update->saveTask($taskArray);
            }

            //print_r($taskArray);

            /*
             * Delete the email message from server
             */
            $mail->removeMessage($messageID);
			$messageID = $messageID-1;
		}
    }

    function saveHelpdeskPostsFromEmail() {
        date_default_timezone_set('UTC');

        $mailArray = array(
			'host'     => 'pop.1and1.com',
			'user'     => 'helpdesk@creativeprooffice.com',
			'password' => 'addyG0@SE'
		);

		Zend_Loader::loadClass('Zend_Mail_Storage_Pop3');
		$mail = new Zend_Mail_Storage_Pop3($mailArray);
		$numberOfMessages = $mail->countMessages();

		$messageID = $numberOfMessages;
		for($i=0; $i<$numberOfMessages; $i++) {
			$message = $mail->getMessage($messageID);

            /*
             * Get from address and see which account this is.
             */
            $fromAddress = findText('<','>',$message->from);
            $userArray   = $this->Accounts->getAccountInfoFromEmail($fromAddress);
            $subject  = $message->subject;

            foreach ($message->getHeaders() as $name => $value) {
                if ($name == 'to') {
                    $toAddress = $value;
                }
            }
            $body = '';

            if($message->isMultipart()) {
                foreach (new RecursiveIteratorIterator($message) as $part) {
                    try {
                        if (strtok($part->contentType, ';') == 'text/plain') {
                            $body = trim($part);
                            break;
                        }
                    } catch (Zend_Mail_Exception $e) {
                        $body = '';
                    }
                }
            }

            $mainBodyArray = explode('###',$body);
            $helpdeskBodyArray = explode(Chr(10),$mainBodyArray[0]);
            $note = '';
            foreach($helpdeskBodyArray as $element) {
                // Tags
                if (stripos(trim($element),'T:') !== false || stripos(trim($element),'Tags:') !== false) {
                    $elementArray = explode(':',$element);
                    $tags = trim($elementArray[1]);
                }

                elseif (stripos(trim($element),'S:') !== false || stripos(trim($element),'Subject:') !== false) {
                    $elementArray = explode(':',$element);
                    $entryTitle = trim($elementArray[1]);
                }

                // Post to Facebook
                elseif (stripos(trim($element),'FB:') !== false || stripos(trim($element),'Facebook:') !== false) {
                    $elementArray = explode(':',$element);
                    $facebook = trim($elementArray[1]);
                }

                // Post to Twitter
                elseif (stripos(trim($element),'TW:') !== false || stripos(trim($element),'Twiter:') !== false) {
                    $elementArray = explode(':',$element);
                    $twitter = trim($elementArray[1]);
                }

                // Post to Blog
                elseif (stripos(trim($element),'BL:') !== false || stripos(trim($element),'Blog:') !== false) {
                    $elementArray = explode(':',$element);
                    $blog = trim($elementArray[1]);
                }

                // Note
                elseif (stripos(trim($element),'N:') !== false || stripos(trim($element),'Note:') !== false) {
                    $elementArray = explode(':',$element);
                    $note .= $elementArray[1].' ';
                }

                elseif (substr($element,0,5) != 'Hello' && substr($element,0,5) != 'Thank') {
                    $note .= $element.' ';
                }
            }

            /*
             * Package up and save entry
             */
            if (empty($entryTitle)) {
                $entryTitle = $subject;
            }
            $entryArray['AuthorUserid'] = $userArray['UseridAccount'];
            $entryArray['Title']        = fieldToDB($entryTitle);
            $entryArray['Content']      = fieldToDB($note,TRUE,TRUE,FALSE,FALSE,TRUE,FALSE);
            $entryArray['Tags']         = $tags;
            $entryArray['EntryURL']     = cleanStringBlog($entryTitle);

            $beID = $this->Helpdesk_model->saveHelpdeskPost($entryArray);

            /*
             * Save tags
             */
            if (!empty($entryArray['Tags'])) {
                $this->commonappdata->saveHelpdeskTags($entryArray['Tags']);
            }

            if ($twitter == '1' || $twitter == 'yes') {
                $twMessage = $entryArray['Title'].' '.$entryArray['Content'];

                if (strlen($twMessage) >= 140) {
                    $postURL = makeIsGdURL('http://dev.mycpohq.com/helpdesk/Helpdesk/entry/'.$entryArray['EntryURL']);
                    // postURL is 19 characters.
                    $twMessage = substr($twMessage,0,115).'... '.$postURL;
                }
                
                $conn1 = $this->twitter->oauth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, TWITTER_OAUTH_TOKEN, TWITTER_OAUTH_SECRET);
                $conn2 = $this->twitter->call('account/verify_credentials');
                $this->twitter->call('statuses/update', array('status' => $twMessage));
            }

            if ($blog == '1' || $blog == 'yes') {
                
            }

            /*
            if ($facebook == '1' || $facebook == 'yes') {
                $fbMessage = $entryArray['Title'].Chr(10).$entryArray['Content'];
                $fb = new Facebook(array(
                    'appId'  => FACEBOOK_APP_ID,
                    'secret' => FACEBOOK_APP_SECRET,
                    'cookie' => true
                ));

                $loginURL = $fb->getLoginUrl(
                    array(
                        'scope'        => 'manage_pages,user_status,publish_stream,user_photos,offline_access',
                        'redirect_url' => FACEBOOK_BASE_URL,
                        'cookie'       => true
                    )
                );

                $logoutURL = $fb->getLogoutUrl();
                $user = $fb->getUser();
                if (!$user) {
                    echo $loginURL;
                    header('Location: '.$loginURL);
                } else {
                    $status = $fb->api('/'.FACEBOOK_PAGE_ID.'/feed', 'POST', array('message' => $fbMessage));
                    var_dump($user);
                }
            }
             * 
             */

            print_r($entryArray);

            /*
             * Delete the email message from server
             */
            $mail->removeMessage($messageID);
			$messageID = $messageID-1;
		}
    }
}
?>