<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Signup extends Controller {
	function __construct()
	{
		parent::Controller();
        $this->load->model('common/public_site','',true);

        $this->load->helper('number');
        $this->load->helper('templates');

        $this->load->library('application/CommonLogin');
        $this->load->library('application/CommonEmail');
	}

    function _init($plan=NULL) {
        global $accountLevelArray;

        $data['child']           = TRUE;
        $data['headerTextImage'] = 'txtSignupHeader_public.png';
        $data['jsFileArray']     = array();
        $data['pageTitle']       = 'Signup';
        
        /*
         * Get some customer logos
         */
        $data['logoArray'] = $this->public_site->getCPOCustomerInfo(8);
        $data['rightColumnComponents'] = array('checkoutInfo');
		
        /*
         * Create account descriptions
         */
        $data['planDataArray'] = createPlanData($plan);
        $data['linkSelectNewPlan'] = '/pricing';

        /*
         * Calculate payment date
         */
        $data['billingDate'] = new_date_from_days(date('Y-m-d'),30,'F j, Y');

        noCache();
        return $data;
    }

    function index($plan=NULL) {
		$data = Signup::_init($plan);
        $this->load->view('public_site/Signup',$data);
    }

    function free() {
        header('Location: /pricing');
		$data = Signup::_init('free');
        $this->load->view('public_site/Signup',$data);
    }
    
    function solo() {
		$data = Signup::_init('solo');
        $this->load->view('public_site/Signup',$data);
    }

    function shop() {
		$data = Signup::_init('shop');
        $this->load->view('public_site/Signup',$data);
    }

    function team() {
		$data = Signup::_init('team');
        $this->load->view('public_site/Signup',$data);
    }

    function changeAccount($plan=NULL) {
        $data = Signup::_init($plan);
        $data['headerTextImage'] = 'txtAccountHeader_public.png';
        $data['plan'] = $plan;
        $data['linkSelectNewPlan'] = SECURE_URL.'account/changeAccount/'.$this->session->userdata('accountUserid');
        $paymentProfileID = $this->session->userdata('paymentProfileID');
        
        $daysFromSignup = days_between_dates(strtotime($this->session->userdata('dateSignup')),time());

        //if ($daysFromSignup >= 31 && $this->session->userdata('userType') == USER_TYPE_GOD) {
        if ($daysFromSignup >= 31) {
            if (empty($paymentProfileID) && $plan == 'free') {
                /*
                 * If we've selected the free plan, and we don't already
                 * have a profile ID, then just go to the plan change
                 * confirmation screen, not the payment screen.
                 */
                $accountConfirm = TRUE;
            } else {
                /*
                 * Redirect to payment screen because user will need to confirm
                 * account change by entering credit card number.
                 * Or if they've had a free plan for > 30 days and they're upgrading
                 * to a paid plan, they'll need to enter their CC info.
                 */
                header('Location: '.SECURE_URL.'payment/paymentInfo/'.$this->session->userdata('accountUserid').'/'.$plan);
            }
        } else {
            $accountConfirm = TRUE;
        }

        if ($accountConfirm == TRUE) {
            /*
             * Don't force the user to complete any payment info yet
             * because they're still within their 30 day trial period.
             */
            $daysLeftInTrial = 30-$daysFromSignup;
            $dateTrialExpires = '';
            if ($daysLeftInTrial>0) {
                $dateTrialExpires = new_date_from_days(date('Y-m-d'),$daysLeftInTrial,'F j, Y');
            }
            $data['dateTrialExpires'] = $dateTrialExpires;
            $data['daysLeftInTrial']  = $daysLeftInTrial;
            $data['paymentProfileID'] = $this->session->userdata('paymentProfileID');
            $data['accountUserid']    = $this->session->userdata('accountUserid');
            $this->load->view('public_site/ConfirmAccountChange',$data);
        }
    }

    function changeAccountConfirm($accountUserid=NULL,$plan=NULL) {
        global $accountLevelArray;
        /*
         * We don't get here unless:
         * 1.  We are still in our 30 day trial period or
         * 2.  We have a successful change payment profile ID
         *     through the payment form.
         */
        switch ($plan) {
			case 'free':
                $newAccountLevel = 0;
                break;
            case 'solo':
                $newAccountLevel = 1;
                break;
            case 'shop':
                $newAccountLevel = 2;
                break;
            case 'team':
                $newAccountLevel = 3;
                break;
        }
        $newAccountPrice = $accountLevelArray[$newAccountLevel]['cost'];
        $this->session->set_userdata('accountLevel',$newAccountLevel);
        /*
         * Save new account level to database
         */
        $this->public_site->saveNewAccountLevel($accountUserid,$newAccountLevel,$newAccountPrice);
    }

    function createAccount() {
        global $accountLevelArray;
        $this->lang->load('email_en','en');
        
        if (!isset($_POST['createAccount'])) {
            $data = Signup::_init();
            $this->load->view('public_site/Signup',$data);
        } else {
            /*
             * Get our variables
             */
            $signupArray['selectedPlan']       = $_POST['selectedPlan'];
            if (!empty($signupArray['selectedPlan'])) {
                $planArray = explode('_',$signupArray['selectedPlan']);
                $signupArray['selectedPlanNumber'] = $planArray[1];
                $signupArray['planCost'] = $accountLevelArray[$signupArray['selectedPlanNumber']]['cost'];
                $signupArray['dateExp']  = '0000-00-00';

                if ($signupArray['selectedPlanNumber']>0) {
                    /*
                     * Get expiration date and plan cost
                     */
                    $signupArray['dateExp']  = new_date_from_days(date('Y-m-d'),30,'F j, Y');
                }
            }

            $signupArray['companyName']        = fieldToDB($this->input->post('companyName', TRUE));
            $signupArray['webAddress']         = fieldToDB($this->input->post('webAddress', TRUE));
            $signupArray['timezone']           = $_POST['companyTimezone'];
            $signupArray['currency']           = $_POST['companyCurrency'];
            $signupArray['language']           = $_POST['companyLanguage'];

            $signupArray['firstName']          = fieldToDB($this->input->post('firstName', TRUE));
            $signupArray['lastName']           = fieldToDB($this->input->post('lastName', TRUE));
            $signupArray['email']              = fieldToDB($this->input->post('email', TRUE));
            $signupArray['webAddress']         = fieldToDB($this->input->post('webAddress', TRUE));
            $signupArray['password']           = fieldToDB($this->input->post('password', TRUE));
            $signupArray['passwordConfirm']    = fieldToDB($this->input->post('passwordConfirm', TRUE));
            $signupArray['authStringAccount']  = random_string('alnum',20);
            $signupArray['authStringPersonal'] = random_string('alnum',20);

            /*
             * Get new account number
             */
            $maxAccountNo = $this->public_site->getLastAccountNumber();
            $signupArray['newAccountNumber'] = $maxAccountNo['MaxAccountNo']+1;

            include(SITE_CODE_PATH);
            $hashPass = sha1($signupArray['password'].$siteCode);
            $signupArray['hashPassword']    = $hashPass;

            /*
             * Load language files for selected language
             */
            loadLanguageFiles($signupArray['language']);
            $signupArray['lang']['roleName']        = lang('role_administrator');
            $signupArray['lang']['projectCalendar'] = lang('calendar_office');
            $signupArray['lang']['taskCalendar']    = lang('widget_tasks');
            
            /*
             * Do some error checking
             */
            $error = FALSE;
            if (empty($signupArray['selectedPlan'])) {
                $error = TRUE;
            }
            if (empty($signupArray['companyName'])) {
                $error = TRUE;
            }
            if (empty($signupArray['firstName'])) {
                $error = TRUE;
            }
            if (empty($signupArray['password'])) {
                $error = TRUE;
            }
            if ($signupArray['password'] != $signupArray['passwordConfirm']) {
                $error = TRUE;
            }
            if (Signup::checkWebAddress($signupArray['webAddress'],1) == FALSE) {
                //$error = TRUE;
            }
            if (Signup::checkEmailPassword($signupArray['email'],$signupArray['password'],1) == FALSE) {
                $error = TRUE;
            }

            if ($error == FALSE) {
                /*
                 * Save new user account information
                 */
                $this->public_site->saveNewAccount($signupArray);

                /*
                 * Send welcome email to user.
                 */                
                $p = $signupArray['selectedPlanNumber'];
                $planPrice = $accountLevelArray[$p]['cost'];
                $plan      = $accountLevelArray[$p]['name'];
                if ($planPrice == 0) {
                    $planPrice = 'Free!';
                } else {
                    $planPrice = '$'.$planPrice.' per month';
                }
                $subject = lang('email_new_client_subject');
                
                $planData = createPlanData($plan);
                $templateData['plan']        = $plan;
                $templateData['cost']        = $planData['acctVars']['cost'];
                $templateData['projects']    = $planData['acctVars']['projects'];
                $templateData['teamMembers'] = $planData['acctVars']['teamMembers'];
                $templateData['storage']     = $planData['acctVars']['storage'];
                $templateData['accountNo']   = $signupArray['newAccountNumber'];
                $templateData['userid']      = $signupArray['email'];
                $templateData['webAddress']  = $signupArray['webAddress'];
                $templateData['expDate']     = $signupArray['dateExp'];

                $messageHTML = templateNewAccountEmail($templateData);
                $messageSent = $this->commonemail->sendNewAccountEmail($subject,$messageHTML,$signupArray['email']);

                /*
                 * Send email to administrator
                 */
                $messageAdmin = $signupArray['companyName']." signed up for the CreativePro Office ".$accountLevelArray[$p]['name']." plan.".Chr(10);
                $subjectAdmin = "New CreativePro Office Account: ".$accountLevelArray[$p]['name'];
                $this->commonemail->sendEmailToAdmin($messageAdmin,$subjectAdmin);

                /*
                 * Log this person into the application and redirect
                 * to dashboard screen
                 */
                $this->commonlogin->tryLogin($signupArray['email'],$signupArray['password'],NULL,NULL,NULL,NULL,FALSE,TRUE,$signupArray['webAddress']);
            } else {
                /*
                 * If there was an error, do something here.
                 */
                $data = Signup::_init($plan);
                $this->load->view('public_site/Signup',$data);
            }
        }        
    }
    
    function testEmail($email) {
        $subject = 'This is a test';
        $messageHTML = templateNCDB();
        $emailAddress = $email;
        $messageSent = $this->commonemail->sendTestEmail($subject,$messageHTML,$emailAddress);
        echo 'Email send to '.$email;

    }

    function checkWebAddress($webAddress=NULL,$return=0) {
        global $disallowedSubdomains;
        $noGood = false;
        if (empty($webAddress)) {
            $webAddress = $this->input->post('webAddress', TRUE);
        }

        /*
         * Check that the subdomain is not in our disallow list.
         */
        foreach($disallowedSubdomains as $disSub) {
            if ($webAddress == $disSub) {
                if ($return == 1) {
                    return FALSE;
                } else {
                    echo '{"Status":"false","Message":"This web address is not allowed. Please choose another one."}';
                }
                $noGood = true;
            }
        }

        /*
         * Check for illegal characters first
         */
        if ($noGood == false) {
            if (alphaNumOnly($webAddress)) {
                /*
                 * We're ok so far....
                 * Now check that this web address does not already exist
                 */
                $status = $this->public_site->checkWebAddress($webAddress);
                if ($status == FALSE) {
                    if ($return == 1) {
                        return FALSE;
                    } else {
                        echo '{"Status":"false","Message":"This web address is being used by another account."}';
                    }
                } else {
                    if ($return == 1) {
                        return TRUE;
                    } else {
                        echo '{"Status":"true","Message":""}';
                    }
                }
            } else {
                /*
                 * Letters and numbers only kids, try again!
                 */
                if ($return == 1) {
                    return FALSE;
                } else {
                    echo '{"Status":"false","Message":"Your web address contains bad characters. Letters and numbers only."}';
                }
            }
        }
    }

    function checkEmailAddress($emailAddress) {
        
    }

    function checkEmailPassword($emailAddress=NULL,$password=NULL,$return=0) {
        if (empty($emailAddress) || empty($password)) {
            $emailAddress = $this->input->post('email', TRUE);
            $password     = $this->input->post('password', TRUE);
        }

        /*
         * See if this email/password combination already exists in the login table
         */
        include(SITE_CODE_PATH);
        $hashPass = sha1($password.$siteCode);
        $validCombo = $this->public_site->checkEmailPassword($emailAddress,$hashPass);
        if ($validCombo == FALSE) {
            if ($return == 1) {
                return FALSE;
            } else {
                echo '{"Status":"false","Message":"Your email address and password are in use by another account."}';
            }
        } else {
            if ($return == 1) {
                return TRUE;
            } else {
                echo '{"Status":"true","Message":""}';
            }
        }

    }    
}
?>
