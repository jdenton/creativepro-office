<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AdminTranslation extends Controller {
	function __construct()
	{
		parent::Controller();
		checkAuthentication($_POST);
        loadLanguageFiles();
	}

	function _init($projectID=NULL) {
		$data['page']                  = 'adminUsers';
		$data['siteName']              = 'CreativePro Office';
		$data['pageTitle']             = 'Administration: Translation';
		$data['wysiwyg']               = 0;
		$data['map']                   = 0;
		$data['jsFileArray']           = array('jsTranslation.js');
		$data['pageIconClass']         = 'iconPageAdministration';
		$data['pageLayoutClass']       = 'withRightColumn';
        $data['rightColumnComponents'] = array('menuAdministration');

		return $data;
	}

	function index() {
		$data = AdminTranslation::_init();
		$data['fileArray'] = AdminTranslation::getTranslationFiles();
		$data['langArray'] = languages();
		$this->load->view('administration/AdminTranslation',$data);
	}

	function getTranslationFiles() {
		$rootDir = LANGUAGE_FILE_PATH;
		$fileArray = array();
		$a=0;
		if ($handle = opendir($rootDir.'en')) {
			while ( false !== ($file = readdir($handle)) ) {
				if ( ($file != 'index.html') && ($file != '.') && ($file != '..') ) {
					$fileArray[$a] = $file;
					$a++;
				}
			}
		}
		return $fileArray;
	}

	function getTranslation($term,$langPair) {
		$url = "http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=".urlencode($term).'&langpair='.urlencode($langPair);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, SITE_URL);
		$body = curl_exec($ch);
		curl_close($ch);

		// now, process the JSON string
		$json = json_decode($body,true);
		return $json;
	}

	function translateFile($fileName,$langTo) {
		$rootDir = LANGUAGE_FILE_PATH;
		include($rootDir.'en/'.$fileName);
        $fileHandle = fopen($rootDir.$langTo.'/'.$fileName, "w");
        $fileContent = "<?\n";
		foreach($lang as $key => $word) {
			$translation = AdminTranslation::getTranslation($word,"en|".$langTo);
			$translation = $translation['responseData']['translatedText'];
			$fileContent .= '$lang[\''.$key.'\'] = \''.$translation.'\';'."\n";
		}
		$fileContent .= "?>";
		$fileContent = trim($fileContent);
		//fwrite($fileHandle, pack("CCC",0xef,0xbb,0xbf));
		fwrite($fileHandle, $fileContent);
		fclose($fileHandle);
        echo $fileName.' translation is complete!';
	}
}
?>
