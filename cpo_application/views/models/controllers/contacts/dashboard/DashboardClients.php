<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DashboardClients extends Controller {
	function __construct()
	{
		parent::Controller();	
        loadLanguageFiles();

		$this->load->model('clients/Client_view','',true);
		$this->load->model('finances/Finance_view','',true);	
		$this->load->model('projects/Project_view','',true);
        $this->load->model('contacts/Contacts','',true);
        $this->load->model('messaging/Site_messages','',true);
        $this->load->model('notes/Site_notes','',true);

		$this->load->helper('dashboard');
	}
	
	function _init() {
        //ini_set('display_errors',1);
        //ini_set('error_reporting',E_ALL);

		$data['page']            = 'dashboard';
		$data['siteName']        = 'CreativePro Office';
		$data['pageTitle']       = $this->lang->line('dashboard_page_title');
		$data['wysiwyg']         = 0;
		$data['map']             = 0;
		$data['jsFileArray']     = array('jsDashboardClients.js','jsProjects.js','jsFinances.js','jsFileManager.js');
		$data['pageIconClass']   = 'iconDashboard';
		$data['pageLayoutClass'] = 'withRightColumn';
		$data['userid']          = $this->session->userdata('userid');
		$data['clientID']        = $this->session->userdata('clientID');
		$data['accountUserid']   = $this->session->userdata('accountUserid');
        $data['accountCompany']  = $this->session->userdata('userCompany');

        $accountCreator          = $this->Contacts->getAccountCreator($this->session->userdata('accountUserid'));
        $accountArray            = array(
                                    'NameFull' => $data['accountCompany'],
                                    'PID' => $accountCreator[0]['PID'],
                                    'UID' => $this->session->userdata('accountUserid'),
                                    'Language' => $accountCreator[0]['Language'],
                                    'Type'     => 'owner'
                                    );
        $data['accountData']    = json_encode($accountArray);
		/*
		 * Get total invoices, projects, messages
		 */
		$data['invoiceCount'] = $this->Finance_view->getInvoiceCount($this->session->userdata('userid'),'client',$this->session->userdata('accountUserid'));
		$data['projectCount'] = $this->Project_view->getNumberOfProjects($this->session->userdata('clientID'),'client');
        $data['messageCount'] = $this->Site_messages->getNumberOfMessagesForSomething(NULL,NULL,$this->session->userdata('userid'));
        $data['notesCount']   = $this->Site_notes->getNumberOfNotesForSomething(NULL,NULL,NULL,$this->session->userdata('accountUserid'));

		return $data;
	}
	
	function index() {
		$data = DashboardClients::_init();
		$this->load->view('dashboard/DashboardClients',$data);
	}
}
?>