<?
class Site_messages extends Model {
    function Site_messages() {
        // Call the Model constructor
        parent::Model();
    }    
    
    /**
	 * saveMessage : saves application messages.
	 *
	 * @access	public
	 * @param	string $to list of | delimited userid's for whom this message is intended
	 * @param   int    $from ID of person sending the message. If it's a client, it will be clientID.
	 *                       If it's a team member or account owner, it will be thier UID. 
	 * @param   string $fromType client,team,owner
	 * @param   string $message the text of the message.
	 * @param   string $itemType if this message is in reference to something: invoice,project,etc.
	 * @param   int    $itemID the ID for the item mentioned in $itemType
	 * @param   int    $replyTo the MID of the message we're replying to if this is a reply.
	 * @return	int    $messageID ID of recently inserted message
	 */	
    function saveMessage($to,$fromType,$message,$itemType=NULL,$itemID=NULL,$replyTo=NULL) {
    	/*
    	 * Save message first
    	 */
		$sql = "INSERT INTO cpo_messages (
				UIDFrom,
				TypeFrom,
				ItemID,
				ItemType,
				Message,
				DateMessage,
				ReplyToMID
				) values ( 
				'".$this->session->userdata('userid')."',
				'$fromType',
				'$itemID',
				'$itemType',
				'$message',
				'".date('Y-m-d G:i:s')."',
				'$replyTo'
				)";
		$query = $this->db->query($sql); 
		$messageID = $this->db->insert_id();

		/*
		 * Now save message to
		 */
		$toArray = explode('|',$to);
		foreach($toArray as $to) {
			if ($to>0) {
				$sql = "INSERT INTO cpo_linkUIDMessage (
						UIDTo,
						MID
						) values (
						'$to',
						'$mID'
						)";
				$query = $this->db->query($sql); 
			}	
		}

		return $messageID;
    }
    
    function getMessages($itemID=NULL,$itemType=NULL,$toOnly=NULL,$unread=NULL) {
		$where = '';
		if ($itemID>0) {
			$where .= " M.ItemID = '$itemID' AND M.ItemType = '$itemType' AND ";
		}
        if ($toOnly == 1) {
            $where .= " LM.UIDTo = '".$this->session->userdata('userid')."' ";
        } else {
            $where .= " (LM.UIDTo = '".$this->session->userdata('userid')."' OR M.UIDFrom = '".$this->session->userdata('userid')."') ";
        }
    	$sql = "SELECT 
				M.MID,M.UIDFrom,M.TypeFrom,M.ItemID,M.ItemType,M.Subject,M.Message,
				UNIX_TIMESTAMP(M.DateMessage) AS DateMessageUnix, UNIX_TIMESTAMP(M.DateRead) AS DateReadUnix, M.ReplyToMID
				FROM cpo_messages M
				LEFT JOIN cpo_linkUIDMessage LM ON LM.MID = M.MID
				WHERE
				$where";
		//echo $sql;
    	if ($unread == 1) {
    		$sql .= " AND M.DateRead IS NULL";
    	}
    	$query = $this->db->query($sql);
    	$j=0;
    	$messageArray = array();
    	foreach ($query->result_array() as $row) {    		
    		if($row['TypeFrom'] == 'client') {
				/*
				 * We have a client
				 */
				$sql2 = "SELECT Company 
							FROM cpo_clients 
							WHERE ClientID = '".$row['UIDFrom']."'";
				$query2 = $this->db->query($sql2);	
				$row2 = $query2->row_array();
				$row['FromName'] = '';
				$row['Company']  = $row2['Company'];
			} else {
				/*
				 * We have an account owner or team member
				 */
				$sql2 = "SELECT Company,First_Name,Last_Name,Email 
							FROM cpo_user_profile 
							WHERE Userid = '".$row['UIDFrom']."'";
				$query2 = $this->db->query($sql2);	
				$row2 = $query2->row_array();
				$row['FromName'] = $row2['First_Name'].' '.$row2['Last_Name'];
				$row['Company']  = $row2['Company'];
			}
			$messageArray[$j]['MID']             = $row['MID'];
			$messageArray[$j]['ItemType']        = $row['ItemType'];
			$messageArray[$j]['ItemID']          = $row['ItemID'];
			$messageArray[$j]['UIDFrom']         = $row['UIDFrom'];
			$messageArray[$j]['TypeFrom']        = $row['TypeFrom'];
			$messageArray[$j]['FromName']        = $row['FromName'];
			$messageArray[$j]['Company']         = $row['Company'];
			$messageArray[$j]['Message']         = $row['Message'];
			$messageArray[$j]['DateMessageUnix'] = $row['DateMessageUnix'];
			$messageArray[$j]['DateReadUnix']    = $row['DateReadUnix'];
			$messageArray[$j]['ReplyTo']         = $row['ReplyToMID'];
			$j++;
    	}
    	return $messageArray;
    }

	/**
      * getNumberOfMessagesForSomething
	  *
      * @access	public
	  * @param  int    $itemID ID of the item for which we want the file count
	  * @param  string $itemType the type of item for which we want the file count
	  *                This could be project, task, message, etc.
      * @return array  [The number of total messages],[The number of unread messages]
      */
	function getNumberOfMessagesForSomething($itemID,$itemType) {
		/*
		 * Get total number of messages
		 */
		$sql = "SELECT COUNT(MID) AS NoOfMessagesTotal
				FROM cpo_messages
				WHERE
					ItemID   = $itemID AND
					ItemType = '$itemType'";
		$query = $this->db->query($sql);
		$row1 = $query->row_array();

		$sql = "SELECT COUNT(MID) AS NoOfMessagesUnread
				FROM cpo_messages
				WHERE
					ItemID   = $itemID AND
					ItemType = '$itemType' AND
					DateRead = '0000-00-00 00:00:00' AND
					UIDFrom <> ".$this->session->userdata('userid');
		$query = $this->db->query($sql);
		$row2 = $query->row_array();

		return array(
						'NoOfMessagesTotal'  => $row1['NoOfMessagesTotal'],
						'NoOfMessagesUnread' => $row2['NoOfMessagesUnread']
					);
	}
}    
?>