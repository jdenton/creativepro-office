<?php
class Project_update extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
        $this->load->model('projects/Project_view','ProjectView',true);
    }

     /**
	 * saveProject : inserts new project entries or updates existing entries
	 *
	 * @access	public
	 * @param	array $data PHP array of data to save
	 * @return	int $projectID the record id of the inserted or updated entry
	 */
    function saveProject($data) {
        if ($data['action'] == 'add') {
            $prevProjectID = '';
            if (isset($data['prevProjectID'])) {
                $prevProjectID = $data['prevProjectID'];
            }
    		$sql = "INSERT into cpo_project (
                        PrevProjectID,
						Userid,
						ClientID,
						CatID,
						Title,
						URL,
						Description,
                        TemplateID,
						Status,
						Date_Start,
						Date_End,
                        Tags,
						UseridUpdate,
                        DateEntry,
						DateUpdate,
                        ImportGUID
					) values (
                        '$prevProjectID',
                        '".$data['accountUserid']."',
                        '".$data['clientID']."',
                        '".$data['projectCategory']."',
						'".$data['projectTitle']."',
						'".$data['projectURL']."',
						'".$data['projectNotes']."',
                        '".$data['projectTemplateID']."',
						'".$data['projectStatus']."',
                        '".$data['projectDateStart']."',
                        '".$data['projectDateEnd']."',
                        '".$data['projectTags']."',
						'".$data['userid']."',
                        '".date('Y-m-d')."',
                        '".date('Y-m-d')."',
                        '".$this->session->userdata('importGUID')."'
					)";
    		$query = $this->db->query($sql);
    		$projectID = $this->db->insert_id();
            
		} elseif ($data['action'] == 'edit') {
    		$sql = "UPDATE cpo_project SET
						ClientID    = '".$data['clientID']."',
						CatID       = '".$data['projectCategory']."',
						Title       = '".$data['projectTitle']."',
						URL         = '".$data['projectURL']."',
						Description = '".$data['projectNotes']."',
                        TemplateID  = '".$data['projectTemplateID']."',
						Status      = '".$data['projectStatus']."',
						Date_Start  = '".$data['projectDateStart']."',
                        Date_End    = '".$data['projectDateEnd']."',
                        Tags        = '".$data['projectTags']."',
                        UseridUpdate= '".$data['userid']."',
                        DateUpdate  = '".date('Y-m-d H:i:s')."'
					WHERE
						ProjectID = '".$data['projectID']."'";
    		$query = $this->db->query($sql);
    		$projectID = $data['projectID'];
		}
		return $projectID;
    }

	function updateProjectStatus($projectID,$status,$date) {
		$sql = "UPDATE cpo_project SET 
				Status        = '$status',
				Date_Complete = '$date',
				UseridUpdate  = '".$this->session->userdata('userid')."',
				DateUpdate    = '".date('Y-m-d H:i:s')."'
				WHERE ProjectID = '$projectID'";
		$query = $this->db->query($sql);
	}

    /**
	 * deleteProject : delete a project
	 *
	 * @access	public
	 * @param	int  $projectID ID of project we wish to delete.
     * @param   bool $tagForDelete If TRUE, just tag this item for deletion.
     *              If FALSE, completely delete the item.
	 * @return	void
	 */
    function deleteProject($projectID,$tagForDelete=TRUE,$userid) {
         if ($tagForDelete == TRUE) {
             $sql = "UPDATE cpo_project SET UseridUpdate = '$userid', Deleted = 1 WHERE ProjectID = '$projectID'";
             $query = $this->db->query($sql);
         } else {
             $sql = "DELETE from cpo_project WHERE ProjectID = '$projectID'";
             $query = $this->db->query($sql);

             /*
              * Delete file links and people links.
              */
            $sql = "DELETE from cpo_linkFileItemType WHERE ItemID = '$projectID' AND ItemType = 'project'";
            $query = $this->db->query($sql);

            $sql = "DELETE from cpo_linkPeopleItemType WHERE ItemID = '$projectID' AND ItemType = 'P'";
            $query = $this->db->query($sql);

			 /*
			  * TODO: Delete all tasks and messages as well??? I don't know yet.
			  */
         }
    }

    function restoreProject($projectID,$userid) {
        $sql = "UPDATE cpo_project SET UseridUpdate = '$userid', Deleted = 0 WHERE ProjectID = '$projectID'";
        $query = $this->db->query($sql);
    }

    function updateArchive($projectID,$archive,$accountUserid=NULL,$userid=NULL) {
        $sql = "UPDATE cpo_project SET 
                    Archive = '$archive',
                    UseridUpdate = '$userid'
                WHERE
                    ProjectID = '$projectID' AND
                    Userid = '$accountUserid'";
        $query = $this->db->query($sql);
    }

    function enforceProjectLimit($maxProjects,$accountUserid) {
        $activeProjectsArray = $this->ProjectView->getNumberOfProjects($accountUserid,'user','0');
        $activeProjects = $activeProjectsArray['Projects'];
        if ($activeProjects>$maxProjects) {
            /*
             * We need to deactivate all illegitimate projects.
             */
            $numberToDeactivate = $activeProjects - $maxProjects;

            $sql = "UPDATE cpo_project
                        SET Archive = 1
                    WHERE
                        Userid = '$accountUserid' AND
                        Deleted = 0 AND
                        Archive = 0
                        ORDER BY ProjectID ASC
                        LIMIT $numberToDeactivate";
            $query = $this->db->query($sql);
        }
    }
}
?>