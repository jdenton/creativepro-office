<?php
class Admin_users extends Model {

    function Admin_users() {
        // Call the Model constructor
        parent::Model();
    }
    
    function getUsersForAutoCompleter($q) {
        $searchResults = '';

    	$sql = "SELECT AP.AccountID,AP.Company,AP.Email,P.NameFirst,P.NameLast
				FROM cpo_account_profile AP
                LEFT JOIN cpo_people P ON P.Userid = AP.AccountID
				WHERE 
                    (
                        P.NameFirst LIKE '$q%' OR P.NameFirst LIKE '% $q%' OR
                        P.NameLast LIKE '$q%' OR P.NameLast LIKE '% $q%' OR
                        AP.Email LIKE '$q%' OR AP.Email LIKE '% $q%' OR
                        AP.Company LIKE '$q%' OR AP.Company LIKE '% $q%' OR
                        AP.PaymentProfileID LIKE '$q%' OR AP.PaymentProfileID LIKE '% $q%'
                    )
                AND P.AccountCreator = 1
				ORDER BY AP.Company";
		$query = $this->db->query($sql); 
		$names = '';
        if ($query->num_rows()>0) {
            foreach ($query->result_array() as $row) {
                $fullDisplay = '';
                if (!empty($row['NameFirst']) || !empty($row['NameLast'])) {
                    $fullDisplay .= $row['NameFirst'].' '.$row['NameLast'].', ';
                }
                $fullDisplay .= $row['Company'].' ['.$row['Email'].']';
                $names .= $fullDisplay.'|'.$row['AccountID']."\n";
            }
        }
		return $names;
    }  

    function getUserInformation($accountUserid) {
    	$sql = "SELECT * 
				FROM cpo_account_profile AP
                LEFT JOIN cpo_people P ON P.Userid = AP.AccountID
				WHERE AP.AccountID = '$accountUserid' AND P.AccountCreator = 1";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
    function getNumberUserProjects($accountUserid) {
    	$sql = "SELECT COUNT(ProjectID) AS Projects from cpo_project WHERE Userid = '$accountUserid'";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
	function getNumberUserInvoices($accountUserid) {
    	$sql = "SELECT COUNT(InvoiceID) AS Invoices from cpo_invoice WHERE Userid = '$accountUserid'";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
	function getNumberUserClients($accountUserid) {
    	$sql = "SELECT COUNT(ClientID) AS Clients from cpo_clients WHERE Userid = '$accountUserid'";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
	function getNumberUserTeam($accountUserid) {
    	$sql = "SELECT COUNT(UID) AS TeamMembers from cpo_login 
				WHERE 
					User_Type = 2 AND
					UseridAccount = '$accountUserid'";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
	function getUserLoginInfo($accountUserid) {
    	$sql = "SELECT Userid,Logins,LastLogin from cpo_login 
				WHERE UseridAccount = '$accountUserid'";
		$query = $this->db->query($sql); 		
		
		return $query->row_array();
    }
    
	function toggleUserStatus($accountID) {
    	$sql = "UPDATE cpo_account_profile SET IsActive = 1 - IsActive
				WHERE AccountID = '$accountID'";
		$query = $this->db->query($sql); 
    }
}
?>