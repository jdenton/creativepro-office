<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contacts extends Model {
    function __construct() {
        // Call the Model constructor
        parent::Model();
    }
    
    /**
	 * getContacts : retrieve contact information for people 
	 * associated with Clients, Projects or Vendors.
	 *
	 * @access	public
	 * @param	int    $userid Userid of account owner.
	 * @param   string $itemType Type of contact we're looking for (C=client,P=project,V=vendor) 
	 * @param   int    $itemID Client ID, Project ID, etc.
	 * @param   int    $primaryContact Return only the primary contact
	 * @return	array
	 */
    function getContactsTest($accountUserid) {
        $sql = "SELECT NameFirst,NameLast,ContactTitle from cpo_people WHERE Userid = '$accountUserid' LIMIT 40";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getContacts($userid,$itemType=NULL,$itemID=NULL,$teamMember=FALSE,$vendor=FALSE,$max=NULL) {
    	$sql = "SELECT 
					P.PID,P.LoginUserid,P.NameFirst,P.NameLast,P.ContactTitle,P.Company,P.Address,P.City,P.State,P.Zip,P.Country,
                    P.Timezone,P.DaylightSavingsTime,P.Language,P.Information,P.PrimaryContact,P.AvatarURL,P.TeamMember,P.Vendor,P.AccountCreator,
			 		PC.CID,PC.ContactType,PC.ContactData,PC.ContactLocation,
					L.RoleID,
                    R.RoleName,R.RolePermissions
				FROM ";

				if ($itemID>0) {
					$sql .= "
					cpo_linkPeopleItemType PIT
					LEFT JOIN
						cpo_people P ON P.PID = PIT.PeopleID ";
				} elseif ($teamMember == TRUE || $vendor == TRUE || $teamMember == 1 || $vendor == 1) {
					$sql .= " cpo_people P ";
				} else {
                    $sql .= " cpo_people P ";
                }

				$sql .= "
					LEFT JOIN
						cpo_login L ON P.LoginUserid = L.UID
                    LEFT JOIN
                        cpo_user_roles R ON R.RoleID = L.RoleID
					LEFT JOIN
						cpo_people_contact_info PC ON PC.PID = P.PID	
					WHERE
						P.Userid = '$userid' AND";

				if (!empty($itemType) && $itemID>0) {
					$sql .= " PIT.ItemType = '$itemType' AND ";
				}
				if ($teamMember == TRUE || $teamMember == 1) {
					$sql .= " (P.TeamMember = '1' OR P.AccountCreator = 1) AND ";
				} elseif ($vendor == TRUE || $vendor == 1) {
                    $sql .= " P.Vendor = '1' AND ";
                }
					
				$sql .= " P.Deleted = 0 ";
    			if ($itemID>0) {
    				$sql .= " AND PIT.ItemID = '$itemID'";
    			}
    			$sql .= " ORDER BY P.NameLast,PC.CID";
    	$query = $this->db->query($sql);
    	$pID = 0;
		$cID = 0;
    	$j = 0;
    	$contactArray = array();
        $totalContacts = 100000;
        if ($max > 0) {
            $totalContacts = $max;
        }
   		foreach ($query->result_array() as $row) {
            if ($j<=$totalContacts) {

                $newPID = $row['PID'];
                $newCID = $row['CID'];
                if ($newPID != $pID) {
                    $pID = $newPID;
                    $k=$j;
                    $l=0;
                    $contactArray[$j]['PID']            = $row['PID'];
                    $contactArray[$j]['LoginUserid']    = $row['LoginUserid'];
                    $contactArray[$j]['NameFirst']      = $row['NameFirst'];
                    $contactArray[$j]['NameLast']       = $row['NameLast'];
                    $contactArray[$j]['Title']          = $row['ContactTitle'];
                    $contactArray[$j]['Company']        = $row['Company'];
                    $contactArray[$j]['Address']        = $row['Address'];
                    $contactArray[$j]['City']           = $row['City'];
                    $contactArray[$j]['State']          = $row['State'];
                    $contactArray[$j]['Zip']            = $row['Zip'];
                    $contactArray[$j]['Country']        = $row['Country'];
                    $contactArray[$j]['Timezone']       = $row['Timezone'];
                    $contactArray[$j]['DST']            = $row['DaylightSavingsTime'];
                    $contactArray[$j]['Language']       = $row['Language'];
                    $contactArray[$j]['Info']           = $row['Information'];
                    $contactArray[$j]['Primary']        = $row['PrimaryContact']; 
                    $contactArray[$j]['RoleName']       = $row['RoleName'];
                    $contactArray[$j]['TeamMember']     = $row['TeamMember'];
                    $contactArray[$j]['Vendor']         = $row['Vendor'];
                    $contactArray[$j]['AccountCreator'] = $row['AccountCreator'];

                    if (empty($row['AvatarURL'])) {
                        $row['AvatarURL'] = BASE_URL.'images/avatars/stockAvatar46.png';
                        $row['AvatarURLBig'] = BASE_URL.'images/avatars/stockAvatar80.png';
                    }
                    $contactArray[$j]['AvatarURL']    = $row['AvatarURL'];
                    $contactArray[$j]['AvatarURLBig'] = $row['AvatarURL'];

                    $contactArray[$j]['ContactItems'] = array();
                    $j++;
                }
                if ($newCID != $cID) {
                    $cID = $newCID;
                    $contactArray[$k]['ContactItems'][$l]['CID']             = $row['CID'];
                    $contactArray[$k]['ContactItems'][$l]['ContactType']     = $row['ContactType'];
                    $contactArray[$k]['ContactItems'][$l]['ContactData']     = $row['ContactData'];
                    $contactArray[$k]['ContactItems'][$l]['ContactLocation'] = lang('common_'.$row['ContactLocation']);
                    $l++;
                }
            }
   		}
		return $contactArray;		
    }

    function getContactsLimited($userid,$itemType=NULL,$itemID=NULL,$teamMember=FALSE,$vendor=FALSE) {
    	$sql = "SELECT
					P.PID,P.LoginUserid,P.NameFirst,P.NameLast,P.ContactTitle,P.Company,P.AvatarURL,P.TeamMember,P.Vendor,P.AccountCreator
				FROM ";

				if ($itemID>0) {
					$sql .= "
					cpo_linkPeopleItemType PIT
					LEFT JOIN
						cpo_people P ON P.PID = PIT.PeopleID ";
				} elseif ($teamMember == TRUE || $vendor == TRUE || $teamMember == 1 || $vendor == 1) {
					$sql .= " cpo_people P ";
				} else {
                    $sql .= " cpo_people P ";
                }

				$sql .= "WHERE P.Userid = '$userid' AND";

				if (!empty($itemType) && $itemID>0) {
					$sql .= " PIT.ItemType = '$itemType' AND ";
				}
				if ($teamMember == TRUE || $teamMember == 1) {
					$sql .= " (P.TeamMember = '1' OR P.AccountCreator = 1) AND ";
				} elseif ($vendor == TRUE || $vendor == 1) {
                    $sql .= " P.Vendor = '1' AND ";
                }

				$sql .= " P.Deleted = 0 ";
    			if ($itemID>0) {
    				$sql .= " AND PIT.ItemID = '$itemID'";
    			}
    			$sql .= " ORDER BY P.NameLast";
    	$query = $this->db->query($sql);
   		return $query->result_array();
    }

    function getContactPerson($personID=NULL,$userid=NULL) {
		if (!empty($personID)) {
			$sql = "SELECT * from cpo_people WHERE PID = '$personID'";
			$query = $this->db->query($sql);
			$contactArray = $query->row_array();			
		} elseif (!empty($userid)) {
			$sql = "SELECT * from cpo_people WHERE LoginUserid = '$userid'";
			$query = $this->db->query($sql);
			$contactArray = $query->row_array();
			$personID = $contactArray['PID'];
		}
        $contactArray['RoleID'] = '';
        if (isset($contactArray['TeamMember']) && $contactArray && $contactArray['TeamMember'] == 1) {
            $sql3 = "SELECT UID,RoleID FROM cpo_login WHERE UID = '".$contactArray['LoginUserid']."'";
            $query = $this->db->query($sql3);
            $roleArray = $query->row_array();
            if ($query->num_rows() == 1) {
                $contactArray['RoleID'] = $roleArray['RoleID'];
                $contactArray['UID']    = $roleArray['UID'];
            }
        }

        $sql2 = "SELECT * from cpo_people_contact_info WHERE PID = '$personID'";
		$query2 = $this->db->query($sql2);        
        $contactArray['ContactItems'] = $query2->result_array();
        return $contactArray;
    }

    function getContactFromLoginEmail($emailAddress,$accountUserid,$userType=NULL) {
        $sql = "SELECT UID from cpo_login
                WHERE
                Userid = '$emailAddress' AND
                UseridAccount = $accountUserid";
        if ($userType != NULL) {
            $sql .= " AND User_Type = '$userType'";
        } else {
            $sql .= " AND User_Type > 1";
        }
        $sql .= " LIMIT 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $userArray = $query->row_array();
            $contactArray = Contacts::getContactPerson(NULL,$userArray['UID']);
            return $contactArray;
        } else {
            return false;
        }
    }

    function getPIDFromLoginUserid($loginUserid) {
        $sql = "SELECT PID from cpo_people WHERE LoginUserid = '$loginUserid'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['PID'];
    }

    /**
	 * saveContactPerson : inserts new contact entries or updates existing entries.
	 *
	 * @access public
	 * @param	 array $data PHP array of data to save
	 * @return int   $contactID the record id of the inserted or updated entry
	 */
    function saveContactPerson($data) {
		if ($data['action'] == 'add') {
            $prevPID = '';
            if (isset($data['prevPID'])) {
                $prevPID = $data['prevPID'];
            }
    		$sql = "INSERT into cpo_people (
                        PrevPID,
                        Userid,
						LoginUserid,
                        NameFirst,
                        NameLast,
                        ContactTitle,
                        Company,
                        Address,
                        City,
                        State,
                        Zip,
                        Country,
                        Timezone,
						Language,
                        Rate,
                        Information,
                        PrimaryContact,
						AvatarURL,
						TeamMember,
                        Vendor,
                        Contact,
                        UseridUpdate,
                        DateEntry,
                        ImportGUID
                        ) values (
                        '$prevPID',
                        '".$data['accountUserid']."',
						'".$data['loginUID']."',
                        '".$data['nameFirst']."',
                        '".$data['nameLast']."',
                        '".$data['title']."',
                        '".$data['company']."',
                        '".$data['address']."',
                        '".$data['city']."',
                        '".$data['state']."',
                        '".$data['zip']."',
                        '".$data['country']."',
                        '".$data['timezone']."',
						'".$data['language']."',
                        '".$data['rate']."',    
                        '',
                        '',
						'".$data['avatarFilename']."',
						'".$data['teamMember']."',
                        '".$data['vendor']."',
                        '".$data['contact']."',
                        '".$this->session->userdata('userid')."',
                        '".date('Y-m-d H:i:s')."',
                        '".$this->session->userdata('importGUID')."'
				)";
			$query = $this->db->query($sql);
    		$contactID = $this->db->insert_id();
           /*
            * Associate this person with an item
            */
            if ($data['itemID']>0) {
                Contacts::associatePersonWithItem($contactID,$data['itemID'],$data['itemType']);
            }
		} elseif ($data['action'] == 'edit') {
    		$sql = "UPDATE cpo_people SET
                        NameFirst      = '".$data['nameFirst']."',
                        NameLast       = '".$data['nameLast']."',
                        ContactTitle   = '".$data['title']."',
                        Company        = '".$data['company']."',
                        Address        = '".$data['address']."',
                        City           = '".$data['city']."',
                        State          = '".$data['state']."',
                        Zip            = '".$data['zip']."',
                        Country        = '".$data['country']."',
                        Timezone       = '".$data['timezone']."',
						Language       = '".$data['language']."',
                        Rate           = '".$data['rate']."',    
                        Information    = '',
                        PrimaryContact = '',
						AvatarURL      = '".$data['avatarFilename']."',
						TeamMember     = '".$data['teamMember']."',
                        Vendor         = '".$data['vendor']."',
                        Contact        = '".$data['contact']."',
                        UseridUpdate   = '".$this->session->userdata('userid')."',
                        DateUpdate     = '".date('Y-m-d H:i:s')."'
                    WHERE PID = '".$data['contactID']."'";
    		$query = $this->db->query($sql);
    		$contactID = $data['contactID'];
		}

        /*
         * Now enter contact data
         */
        Contacts::saveContactData($data['phone'],'phone',$contactID);
        Contacts::saveContactData($data['email'],'email',$contactID);
        Contacts::saveContactData($data['url'],'url',$contactID);
        Contacts::saveContactData($data['im'],'im',$contactID);

		return $contactID;
    }

    /**
	* saveContactData : inserts new contact item (phone number, email, url, etc.)
    *                   for a contact person.
	*
	* @access	public
	* @param	array  $dataArray PHP array of data to save
    * @param	string $type Type of contact data (phone,email,url,im)
    * @param	int    $contactID ID of Contact Person to which this entry belongs
	* @return	void
	*/
    function saveContactData($dataArray,$type,$contactID) {
        /*
         * First delete all existing entries for this contact
         */
        $sql = "DELETE from cpo_people_contact_info 
                WHERE
                    PID = '$contactID' AND
                    ContactType = '$type'";
        $query = $this->db->query($sql);

        for($a=0;$a<=(count($dataArray)-1);$a++) {
            if (isset($dataArray['data'][$a]) && !empty($dataArray['data'][$a])) {
                $sql = "INSERT into cpo_people_contact_info (
                        PID,
                        ContactType,
                        ContactData,
                        ContactLocation,
                        UseridUpdate,
                        DateUpdate,
                        ImportGUID
                        ) values (
                        '$contactID',
                        '$type',
                        '".$dataArray['data'][$a]."',
                        '".$dataArray['type'][$a]."',
                        '".$this->session->userdata('UID')."',
                        '".date('Y-m-d')."',
                        '".$this->session->userdata('importGUID')."'
                        )";
                $query = $this->db->query($sql);
            }
        }        
    }

    /**
	* associatePersonWithItem : associates a personID with an item and itemID
    *                           (e.g. C=Client,P=Project,etc.)
	*
	* @access	public
	* @param	array  $dataArray PHP array of data to save
    * @param	string $type Type of contact data (phone,email,url,im)
    * @param	int    $contactID ID of Contact Person to which this entry belongs
	* @return	void
	*/
    function associatePersonWithItem($personID,$itemID,$itemType) {
         $sql = "INSERT into cpo_linkPeopleItemType (
                    PeopleID,
                    ItemID,
                    ItemType,
                    ImportGUID
                    ) values (
                    '$personID',
                    '$itemID',
                    '$itemType',
                    '".$this->session->userdata('importGUID')."'
                    )";
         $query = $this->db->query($sql);
    }

    function removePersonFromItem($personID,$itemID,$itemType) {
        $sql = "DELETE from cpo_linkPeopleItemType
                WHERE
                PeopleID = '$personID' AND
                ItemID   = '$itemID' AND
                ItemType = '$itemType'";
        $query = $this->db->query($sql);
    }

    function getContactsForSelect() {
        $sql = "SELECT PID,NameFirst,NameLast,Language
				FROM cpo_people
				WHERE Userid = '".$this->session->userdata('accountUserid')."'
				ORDER BY NameLast";
		$query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
    		return $query->result_array();
    	}
    }

    function getAccountCreator($accountUserid) {
        $sql = "SELECT * from cpo_people WHERE Userid = '$accountUserid' AND AccountCreator = '1'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
    		return $query->result_array();
    	}
    }

    /**
	* deleteContact : delete a contact
	*
	* @access	public
	* @param	int  $contactID ID of contact we wish to delete.
    * @param    bool $tagForDelete If TRUE, just tag this contact for deletion.
    *                              If FALSE, completely delete the contact.
	* @return	void
	*/
    function deleteContact($personID,$tagForDelete=TRUE,$userid) {
         if ($tagForDelete == TRUE) {
             $sql = "UPDATE cpo_people SET UseridUpdate = '$userid', Deleted = 1 WHERE PID = '$personID'";
             $query = $this->db->query($sql);
         } else {
             $sql = "DELETE from cpo_people WHERE PID = '$personID'";
             $query = $this->db->query($sql);

             $sql = "DELETE from cpo_people_contact_info WHERE PID = '$personID'";
             $query = $this->db->query($sql);

             $sql = "DELETE from cpo_linkPeopleItemType WHERE PeopleID = '$personID'";
             $query = $this->db->query($sql);
         }
    }

    function restoreContact($personID,$userid) {
        $sql = "UPDATE cpo_people SET UseridUpdate = '$userid', Deleted = 0 WHERE PID = '$personID'";
        $query = $this->db->query($sql);
    }

    function getContactsForAutoCompleter($q,$accountUserid,$clients,$contacts,$team,$vendors) {
        $contactList = '';
        $sqlContacts = '';
        $sqlVendors  = '';
        $sqlTeam     = '';

        /*
         * First let's look for matching people (team members, contacts, vendors)
         */
        $sql = "SELECT PID,NameFirst,NameLast,Company,Language,AvatarURL,TeamMember,Vendor,Client,Contact
				FROM cpo_people
				WHERE
				(
					NameFirst LIKE '$q%' OR NameFirst LIKE '% $q%' OR
                    NameLast  LIKE '$q%' OR NameLast  LIKE '% $q%' OR
                    Company   LIKE '$q%' OR Company   LIKE '% $q%'
				)
                AND Userid = '".$accountUserid."' AND ";

        if ($contacts == 1) {
            $sqlContacts = " Contact = '1' OR";
        }
        if ($team == 1) {
            $sqlTeam = " TeamMember = '1' OR AccountCreator = '1' OR";
        }
        if ($vendors == 1) {
            $sqlVendors = " Vendor = '1' OR";
        }

        $sqlTypeString = $sqlContacts.$sqlTeam.$sqlVendors;

        /*
         * Remove trailing OR
         */
        if (!empty($sqlTypeString)) {
            $sql .= '('.substr($sqlTypeString,0,-2).') AND ';
        }

                $sql .= "Deleted = 0
				ORDER BY NameLast";
		$query = $this->db->query($sql);
        
        $contactArray = array();
        $a=0;
        if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                $type = '';
                $systemType = '';

                if ($row['TeamMember'] == 1) {
                    $type .= lang('team_member').', ';
                    $systemType = 'team';
                }
                if ($row['Vendor'] == 1) {
                    $type .= lang('common_vendor').', ';
                    if ($systemType != 'team') {
                        $systemType = 'vendor';
                    }
                }
                if ($row['Contact'] == 1) {
                    $type .= lang('common_contact').', ';
                    if ($systemType != 'team') {
                        $systemType = 'contact';
                    }
                }
                /* Remove trailing space and comma */
                $type = substr($type,0,-2);
                
                $contactArray[$a]['contactName']    = $row['NameFirst'].' '.$row['NameLast'];
                $contactArray[$a]['type']    = $type;
                $contactArray[$a]['systemType'] = $systemType;
                $contactArray[$a]['company'] = $row['Company'];
                $contactArray[$a]['pid']     = $row['PID'];
                $contactArray[$a]['language']= $row['Language'];
                $contactArray[$a]['avatar']  = $row['AvatarURL'];
                $contactArray[$a]['value']   = $row['PID'].'-'.$systemType.'-'.$row['Language'];
                $a++;
            }
		}

        if ($clients == 1) {
            /*
             * Then look for clients as well
             */
            $sql = "SELECT ClientID,ClientUserid,Company,Language from cpo_clients
                    WHERE
                    (
                        Company LIKE '$q%' OR Company LIKE '% $q%'
                    )
                    AND Userid = '".$accountUserid."'
                    AND Deleted = 0
                    ORDER BY Company";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                foreach ($query->result_array() as $row) {
                    $contactArray[$a]['contactName']    = $row['Company'];
                    $contactArray[$a]['type']       = 'Client';
                    $contactArray[$a]['systemType'] = 'client';
                    $contactArray[$a]['company'] = $row['Company'];
                    $contactArray[$a]['pid']     = $row['ClientUserid'];
                    $contactArray[$a]['language']= $row['Language'];
                    $contactArray[$a]['avatar']  = '';
                    $contactArray[$a]['value']   = $row['ClientUserid'].'-client-'.$row['Language'];
                    $a++;
                }
            }
        }
        $contactList = json_encode($contactArray);
        return $contactList;
    }

    function getPersonFromPrevPersonID($prevPID,$accountUserid) {
        $sql = "SELECT * from cpo_people WHERE PrevPID = '$prevPID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->result_array();
    	}
    }

    function getNumberOfContacts($accountUserid,$contactType=NULL) {
        $sql = "SELECT COUNT(PID) AS Contacts from cpo_people
                WHERE
                Userid = '$accountUserid'
                ";
                if ($contactType != NULL && !empty($contactType)) {
                    switch ($contactType) {
                        case 'team':
                            $sqlType = "TeamMember = 1";
                            break;
                        case 'vendor':
                            $sqlType = "Vendor = 1";
                            break;
                        case 'contact':
                            $sqlType = "Contact = 1";
                            break;
                        default:
                            $sqlType = "Contact = 1";
                    }
                    $sql .= " AND ".$sqlType;
                    $sql .= " AND Deleted = 0";
                }
         $query = $this->db->query($sql);
         return $query->row_array();
    }

    function getContactsForImportConfirm() {
        $sql = "SELECT * from cpo_people WHERE ImportGUID = '".$this->session->userdata('importGUID')."' LIMIT 8";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->result_array();
        }
    }

    function makeTemplateRecords($templateProjectID,$newProjectID,$userid,$team=false) {
        if ($team == true) {
            $sql = "SELECT PIT.PeopleID from cpo_linkPeopleItemType PIT
                    LEFT JOIN cpo_people P ON P.PID = PIT.PeopleID
                    WHERE
                    PIT.ItemID = $templateProjectID AND
                    PIT.ItemType = 'P' AND
                    P.TeamMember = 1 AND
                    P.Deleted = 0";
        } else {
            $sql = "SELECT PIT.PeopleID from cpo_linkPeopleItemType PIT
                    LEFT JOIN cpo_people P ON P.PID = PIT.PeopleID
                    WHERE
                    PIT.ItemID = $templateProjectID AND
                    PIT.ItemType = 'P' AND
                    P.Contact = 1 AND
                    P.Deleted = 0";
        }
        $query = $this->db->query($sql);

        if ($query->num_rows()>0) {
            foreach ($query->result_array() as $row) {
                $sql = "INSERT into cpo_linkPeopleItemType (
                        PeopleID,
                        ItemID,
                        ItemType,
                        IsTemplate
                        ) values (
                        '".$row['PeopleID']."',
                        '".$newProjectID."',
                        'P',
                        '1'
                        )";
                $query = $this->db->query($sql);
            }
        }
    }
}    
?>