<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site_search extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
        $this->load->model('contacts/contacts');
    }

    function performSiteSearch($searchTerm=NULL,$accountUserid=NULL,$userid=NULL,$clientID=NULL) {
        $searchArrayClients  = array();
        $searchArrayProjects = array();
        $searchArrayInvoices = array();
        $searchArrayExpenses = array();
        $searchArrayEvents   = array();
        $searchArrayMessages = array();
        $searchArrayTags     = array();
        $clientSQL = NULL;

        if ($clientID == NULL) {
            $sql = "SELECT ClientID,Company
                    FROM cpo_clients
                    WHERE
                    (
                        Company LIKE '$searchTerm%' OR Company LIKE '% $searchTerm%'
                    )
                    AND Userid = '".$accountUserid."'
                    AND Deleted = 0
                    ORDER BY Company";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                $searchArrayClients = $query->result_array();
            }
        }

        if ($clientID>0) {
            $clientSQL = " AND P.ClientID = $clientID";
        }
        $sql = "SELECT P.ProjectID,P.Title,C.ClientID,C.Company
				FROM cpo_project P
                LEFT JOIN cpo_clients C ON P.ClientID = C.ClientID
				WHERE
				(
					P.Title LIKE '$searchTerm%' OR P.Title LIKE '% $searchTerm%' OR
                    P.Description LIKE '$searchTerm%' OR P.Description LIKE '% $searchTerm%'
				)
				AND P.Userid = '".$accountUserid."'
                $clientSQL
				AND P.Deleted = 0
				ORDER BY P.Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			$searchArrayProjects = $query->result_array();
		}

        if ($clientID>0) {
            $clientSQL = " AND I.ClientID = $clientID";
        }
        $sql = "SELECT I.InvoiceID,I.Title,I.InvTotal,I.DateInvoice,C.ClientID,C.Company
				FROM cpo_invoice I
               LEFT JOIN cpo_clients C ON I.ClientID = C.ClientID
				WHERE
				(
					I.Title LIKE '$searchTerm%' OR I.Title LIKE '% $searchTerm%' OR
					I.InvComments LIKE '$$searchTerm%' OR I.InvComments LIKE '% $searchTerm%'
				)
				AND I.Userid = '".$accountUserid."'
                $clientSQL
				AND I.Deleted = 0
				ORDER BY I.Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			$searchArrayInvoices = $query->result_array();
		}

        if ($clientID>0) {
            $clientSQL = " AND ClientID = $clientID";
        }
        $sql = "SELECT ExpenseID,Title,Amount,DateEntry
				FROM cpo_expense
				WHERE
				(
					Title LIKE '$searchTerm%' OR Title LIKE '% $searchTerm%'
				)
				AND Userid = '".$accountUserid."'
                $clientSQL
				AND Deleted = 0
				ORDER BY Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			$searchArrayExpenses = $query->result_array();
		}

        if ($clientID == NULL) {
            $sql = "SELECT EventID,Title,DateStart,DateEnd
                    FROM cpo_calendar_events
                    WHERE
                    (
                        Title LIKE '$searchTerm%' OR Title LIKE '% $searchTerm%' OR
                        Description LIKE '$searchTerm%' OR Description LIKE '% $searchTerm%'
                    )
                    AND Userid = '".$accountUserid."'
                    AND Deleted = 0
                    ORDER BY Title";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                $searchArrayEvents = $query->result_array();
            }
        }

        if ($clientID == NULL) {
            $sql = "SELECT M.MID,M.UIDFrom,M.TypeFrom,M.ItemID,M.ItemType,M.Subject,M.Message,
                    M.DateMessage,M.DateRead,M.ReplyToMID
                    FROM cpo_messages M
                    LEFT JOIN cpo_linkMemberMessage LM ON LM.MID = M.MID
                    WHERE
                    (
                        M.Message LIKE '$searchTerm%' OR M.Message LIKE '% $searchTerm%'
                    )
                    AND M.Userid = '".$accountUserid."'
                    AND M.Deleted = 0
                    ORDER BY M.DateMessage DESC";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                $j=0;
                $messageArray = array();
                $mID = '';
                foreach ($query->result_array() as $row) {
                    if ($row['MID'] != $mID) {
                        if($row['TypeFrom'] == USER_TYPE_CLIENT) {
                            /*
                             * We have a client
                             */
                            $sql2 = "SELECT Company
                                        FROM cpo_clients
                                        WHERE ClientUserid = '".$row['UIDFrom']."'";
                            $query2 = $this->db->query($sql2);
                            $row2 = $query2->row_array();
                            $row['FromName'] = '';
                            $row['Company']  = $row2['Company'];
                            $row['PIDFrom']  = '';
                            $avatarURL = '';
                        } else {
                            /*
                             * We have a team member
                             */
                            $contactArray = Contacts::getContactPerson(NULL,$row['UIDFrom']);
                            $row['FromName'] = $contactArray['NameFirst'].' '.$contactArray['NameLast'];
                            $row['Company']  = $contactArray['Company'];
                            $row['PIDFrom']  = $contactArray['PID'];
                            $avatarURL       = $contactArray['AvatarURL'];
                        }

                        $messageArray[$j]['MID']             = $row['MID'];
                        $messageArray[$j]['ItemType']        = $row['ItemType'];
                        $messageArray[$j]['ItemID']          = $row['ItemID'];
                        $messageArray[$j]['UIDFrom']         = $row['UIDFrom'];
                        $messageArray[$j]['PIDFrom']         = $row['PIDFrom'];
                        $messageArray[$j]['TypeFrom']        = $row['TypeFrom'];
                        $messageArray[$j]['FromName']        = $row['FromName'];
                        $messageArray[$j]['Company']         = $row['Company'];
                        $messageArray[$j]['Message']         = $row['Message'];
                        $messageArray[$j]['DateMessage']     = $row['DateMessage'];
                        $messageArray[$j]['DateRead']        = $row['DateRead'];
                        $messageArray[$j]['ReplyTo']         = $row['ReplyToMID'];
                        $messageArray[$j]['Avatar']          = $avatarURL;
                        $j++;

                        $mID = $row['MID'];
                    }
                }
                $searchArrayMessages = $messageArray;
            }
        }
        $searchArrayResults = array(
            'Clients'  => $searchArrayClients,
            'Projects' => $searchArrayProjects,
            'Invoices' => $searchArrayInvoices,
            'Expenses' => $searchArrayExpenses,
            'Events'   => $searchArrayEvents,
            'Messages' => $searchArrayMessages,
            'Tags'     => $searchArrayTags
        );

        return $searchArrayResults;

    }
    
    function getSearchResultsForAutoCompleter($q,$accountUserid=NULL,$userid=NULL,$clientID=NULL) {
        $searchArray = array();
        $a=0;
        $clientSQL = NULL;
		/*
		 * First let's look for matching invoices
		 */
        if ($clientID>0) {
            $clientSQL = " AND I.ClientID = $clientID";
        }
    	$sql = "SELECT I.InvoiceID,I.InvNo,I.Title
				FROM cpo_invoice I
				WHERE
				(
					I.Title LIKE '$q%' OR I.Title LIKE '% $q%' OR
					I.InvComments LIKE '$q%' OR I.InvComments LIKE '% $q%'
				)
				AND I.Userid = '".$accountUserid."'
                $clientSQL
				AND I.Deleted = 0
				ORDER BY I.Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                $searchArray[$a]['id'] = $row['InvoiceID'];
                $searchArray[$a]['label'] = $row['InvNo'].' '.$row['Title'];
                $searchArray[$a]['type']  = 'invoice';
                $searchArray[$a]['icon']  = 'icon_invoice_small';
                $a++;
			}
		}		

		/*
		 * Now, let's look for any project titles that match
		 */
        if ($clientID>0) {
            $clientSQL = " AND ClientID = $clientID";
        }
		$sql = "SELECT ProjectID,Title
				FROM cpo_project
				WHERE
				(
					Title LIKE '$q%' OR Title LIKE '% $q%'
				)
				AND Userid = '".$accountUserid."'
                $clientSQL
				AND Deleted = 0
				ORDER BY Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                $searchArray[$a]['id'] = $row['ProjectID'];
                $searchArray[$a]['label'] = $row['Title'];
                $searchArray[$a]['type']  = 'project';
                $searchArray[$a]['icon']  = 'icon_project_small';
                $a++;
            }
		}    
        
        /*
		 * Now, let's look for any tasks that match
		 */
        if ($clientID>0) {
            $clientSQL = " AND ClientID = $clientID";
        }
		$sql = "SELECT TaskID,Title
				FROM cpo_project_tasks
				WHERE
				(
					Title LIKE '$q%' OR Title LIKE '% $q%' OR
					Description LIKE '$q%' OR Description LIKE '% $q%'
				)
				AND Userid = '".$accountUserid."'
                $clientSQL
				AND Deleted = 0
				ORDER BY Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                $searchArray[$a]['id'] = $row['TaskID'];
                $searchArray[$a]['label'] = $row['Title'];
                $searchArray[$a]['type']  = 'task';
                $searchArray[$a]['icon']  = 'icon_task_small';
                $a++;
            }
		}    

		/*
		 * Now, let's look for any clients that match
		 */
        if (empty($clientID)) {
            $sql = "SELECT ClientID,Company
                    FROM cpo_clients
                    WHERE
                    (
                        Company LIKE '$q%' OR Company LIKE '% $q%'
                    )
                    AND Userid = '".$accountUserid."'
                    AND Deleted = 0
                    ORDER BY Company";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                foreach ($query->result_array() as $row) {
                    $searchArray[$a]['id'] = $row['ClientID'];
                    $searchArray[$a]['label'] = $row['Company'];
                    $searchArray[$a]['type']  = 'client';
                    $searchArray[$a]['icon']  = 'icon_client_small item';
                    $a++;
                }
            }
        }

        /*
		 * Now, let's look for any expenses that match
		 */
        if ($clientID>0) {
            $clientSQL = " AND ClientID = $clientID";
        }
		$sql = "SELECT ExpenseID,Title
				FROM cpo_expense
				WHERE
				(
					Title LIKE '$q%' OR Title LIKE '% $q%'
				)
				AND Userid = '".$accountUserid."'
                $clientSQL
				AND Deleted = 0
				ORDER BY Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                $searchArray[$a]['id'] = $row['ExpenseID'];
                $searchArray[$a]['label'] = $row['Title'];
                $searchArray[$a]['type']  = 'expense';
                $searchArray[$a]['icon']  = 'icon_expense_small item';
                $a++;
            }
		}
        
        /*
		 * Now, let's look for any events that match
		 */
        if (empty($clientID)) {
            $sql = "SELECT EventID,Title
                    FROM cpo_calendar_events
                    WHERE
                    (
                        Title LIKE '$q%' OR Title LIKE '% $q%' OR
                        Description LIKE '$q%' OR Description LIKE '% $q%'
                    )
                    AND Userid = '".$accountUserid."'
                    AND Deleted = 0
                    ORDER BY Title";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                foreach ($query->result_array() as $row) {
                    $searchArray[$a]['id'] = $row['EventID'];
                    $searchArray[$a]['label'] = $row['Title'];
                    $searchArray[$a]['type']  = 'event';
                    $searchArray[$a]['icon']  = 'icon_event_small item';
                    $a++;
                }
            }
        }

        /*
		 * Now, let's look for matching tags
		 */
        /*
		$sql = "SELECT Tag
				FROM cpo_tags
				WHERE
				(Tag LIKE '$q%' OR Tag LIKE '% $q%')
				AND Userid = '".$accountUserid."'
				ORDER BY Tag";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				$searchResults .= '<span class="icon_tag tag">'.$row['Tag'].'</span>|tag_'.$row['Tag']."\n";
			}
		}
        */

        /*
		 * Now, let's look for any contacts that match
		 */
        /*
		 $sql = "SELECT PID,NameFirst,NameLast,Company,Language,TeamMember,Vendor,Client,Contact
				FROM cpo_people
				WHERE
				(
					NameFirst LIKE '$q%' OR NameFirst LIKE '% $q%' OR
                    NameLast  LIKE '$q%' OR NameLast  LIKE '% $q%' OR
                    Company   LIKE '$q%' OR Company   LIKE '% $q%'
				)
                AND Userid = '".$accountUserid."'
                AND Deleted = 0
                ORDER BY NameLast";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                if (!empty($row['Company'])) {
                    $company = ' <span class="subText">['.$row['Company'].']</span>';
                } else {
                    $company = '';
                }
                if ($row['TeamMember'] == 1) {
                    $icon = 'icon_team_small';
                } else {
                    $icon = 'icon_vcard';
                }
				$searchResults .= '<span class="'.$icon.' item">'.$row['NameFirst'].' '.$row['NameLast'].$company.'</small>|contact_'.$row['PID']."\n";
			}
		}
        */
		return json_encode($searchArray);
    }
}
?>