<?php
class Team extends Model {
    function __construct() {
        // Call the Model constructor
        parent::Model();
    }
    
    /**
	 * getTeamMembersForOwner : retrieve all team member information  
	 * for an account.
	 *
	 * @access	public
	 * @param	int    $userid Userid of account owner.
	 * @param   string $data 'full' for complete records, 'short' for only ID and Full Name
	 * @return	array
	 */	
    function getTeamMembersForOwner($userid,$data=NULL) {
    	if ($data == 'short') {
    		$sql = "SELECT 
					NameFirst AS First_Name,NameLast AS Last_Name,LoginUserid AS UID
					FROM cpo_people
					WHERE
                    TeamMember = 1 AND
                    Userid = '$userid'";
    	} else {
    		$sql = "SELECT 
					First_Name,Last_Name,PhoneOffice,
					Email,DateSignup,Comments,GMTOffset,
					L.Userid,L.User_Type,L.Role,L.UID		
					FROM cpo_user_profile P 
					LEFT JOIN cpo_login L ON P.Userid = L.UID 
					WHERE
						L.User_Type = '".USER_TYPE_TEAM."' AND
						L.Parent_Userid = '$userid' AND
						P.IsActive = 1
						ORDER BY P.Last_Name";
    	}
        
    	$query = $this->db->query($sql);
    	if ($query->num_rows()>0) {
    		return $query->result_array();
    	} else {
    		return false;
    	}
    }

	/**
	 * getRolesPermissions : retrieve all team member information
	 * for an account.
	 *
	 * @access	public
	 * @param	int    $userid Userid of account owner.
	 * @param   string $data 'full' for complete records, 'short' for only ID and Full Name
	 * @return	array
	 */
	 function getRolesPermissions($accountUserid) {
        $sql = "SELECT RoleID,RoleName,RolePermissions,
                (SELECT COUNT(UID) FROM cpo_login L WHERE L.RoleID = R.RoleID) AS UserCount
                FROM cpo_user_roles R
                WHERE
                Userid = '$accountUserid'
                ORDER BY R.RoleName";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
    		return $query->result_array();
    	} else {
    		return false;
    	}
	 }

     function getRole($roleID) {
        $sql = "SELECT * FROM cpo_user_roles
				WHERE RoleID = '$roleID'";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
    		return $query->row_array();
    	} else {
    		return false;
    	}
     }

     /**
	 * getRolesPermissions : retrieve all team member information
	 * for an account.
	 *
	 * @access	public
	 * @param	int    $userid Userid of account owner.
	 * @param   string $data 'full' for complete records, 'short' for only ID and Full Name
	 * @return	array
	 */
     function saveRolesPermissions($data) {
         $action        = $data['action'];
         $roleID        = $data['roleID'];
         $roleName      = $data['roleName'];
         $permString    = $data['permString'];
         $userid        = $data['userid'];
         $accountUserid = $data['accountUserid'];

         if ($action == 'add') {
            $sql = "INSERT into cpo_user_roles (
                    Userid,
                    RoleName,
                    RolePermissions,
                    UseridUpdate,
                    DateUpdate
                    ) values (
                    '$accountUserid',
                    '$roleName',
                    '$permString',
                    '$userid',
                    '".date('Y-m-d')."'
                    )";
             $query = $this->db->query($sql);
             $roleID = $this->db->insert_id();
         } else {
             $sql = "UPDATE cpo_user_roles SET
                        RoleName        = '$roleName',
                        RolePermissions = '$permString',
                        UseridUpdate    = '$userid',
                        DateUpdate      = '".date('Y-m-d')."'
                     WHERE RoleID = $roleID";
             $query = $this->db->query($sql);
         }
         return $roleID;
     }

     function deleteRolesPermissions($roleID) {
         $sql = "DELETE from cpo_user_roles WHERE RoleID = '$roleID'";
         $query = $this->db->query($sql);
     }

     function makeTemplateRecords($templateProjectID,$newProjectID,$userid) {
         /*
        $sql = "SELECT * from cpo_linkFileItemType WHERE ItemID = $templateProjectID AND ItemType = 'project'";
        $query = $this->db->query($sql);

        if ($query->num_rows()>0) {
            foreach ($query->result_array() as $row) {
                $sql = "INSERT into cpo_linkFileItemType (
                        FileID,
                        ItemID,
                        ItemType,
                        IsTemplate,
                        UseridUpdate
                        ) values (
                        '".$row['FileID']."',
                        '".$newProjectID."',
                        '".$row['ItemType']."',
                        '1',
                        '".$userid."'
                        )";
                $query = $this->db->query($sql);
            }
        }
        */
    }
}    
?>