<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Blog_model extends Model {
    function __construct() {
        // Call the Model constructor
        parent::Model();
        
    }

    function getBlogPosts($count=NULL,$month=NULL,$year=NULL,$tag=NULL,$active=NULL) {
        $limit = NULL;
        $searchByTagSQL  = NULL;
        $searchByDateSQL = NULL;
        $activeSQL    = " (BE.Active = 1 OR BE.Active = 0) ";
        if ($count != NULL) {
            $limit = " LIMIT $count ";
        }

        if (!empty($tag)) {
            $searchByTagSQL = " AND (BE.Tags LIKE '% $tag%' OR BE.Tags LIKE '$tag%') ";
            $limit = NULL;
        }

        if (!empty($month)) {
            $searchByDateSQL = " AND MONTHNAME(BE.DateEntry) = '$month' AND YEAR(BE.DateEntry) = '$year' ";
            $limit = NULL;
        }

        if (!empty($active)) {
            $activeSQL = " BE.Active = $active ";
        }

        $sql  = "SELECT BE.BEID,BE.Userid,BE.Title,BE.BlogEntry,BE.DateEntry,BE.Tags,BE.Active,BE.EntryURL,BE.Views,
                P.NameFirst,P.NameLast,P.AvatarURL,
                (SELECT COUNT(CID) FROM blogComments BC WHERE BC.BEID = BE.BEID) AS commentCount
                FROM blogEntry BE
                LEFT JOIN cpo_people P ON P.LoginUserid = BE.Userid
                WHERE
                    $activeSQL
                    $searchByTagSQL
                    $searchByDateSQL
                AND BE.Deleted = 0
                ORDER BY BE.DateEntry DESC $limit";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function searchBlogPosts($searchTerm) {
        $sql  = "SELECT BE.BEID,BE.Userid,BE.Title,BE.BlogEntry,BE.DateEntry,BE.Tags,BE.Active,BE.EntryURL,BE.Views,
                P.NameFirst,P.NameLast,P.AvatarURL,
                (SELECT COUNT(CID) FROM blogComments BC WHERE BC.BEID = BE.BEID) AS commentCount
                FROM blogEntry BE
                LEFT JOIN cpo_people P ON P.LoginUserid = BE.Userid
                WHERE
                    (
                    BE.Title LIKE '% $searchTerm%' OR 
                    BE.Title LIKE '$searchTerm%' OR
                    BE.BlogEntry LIKE '% $searchTerm%' OR 
                    BE.BlogEntry LIKE '$searchTerm%'
                    )
                AND BE.Deleted = 0
                AND BE.Active = 1
                ORDER BY BE.DateEntry DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getBlogPost($title,$beID=NULL) {
        $sql = "SELECT BE.BEID,BE.Userid,BE.Title,BE.BlogEntry,BE.DateEntry,BE.Tags,BE.Active,BE.EntryURL,BE.Views,
                P.NameFirst,P.NameLast,P.AvatarURL
                FROM blogEntry BE
                LEFT JOIN cpo_people P ON P.LoginUserid = BE.Userid";

        if ($beID>0) {
            $sql .= " WHERE BEID = '$beID'";
        } else {
            $sql .= " WHERE EntryURL = '$title'";
        }
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getLatestEntries($count=10) {
        $sql = "SELECT BEID,Title,EntryURL FROM blogEntry 
                WHERE Active = 1 AND Deleted = 0
                ORDER BY DateEntry DESC LIMIT $count";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getBlogPostComments($beID) {
        $sql = "SELECT C.CID,C.PID,C.UserName,C.UserEmail,C.UserURL,C.Comment,C.DateComment,C.Admin,P.AvatarURL
                FROM blogComments C
                LEFT JOIN cpo_people P ON P.PID = C.PID
                WHERE C.BEID = '$beID'
                ORDER BY C.DateComment ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getBlogPostComment($cID) {
        $sql = "SELECT C.CID,C.PID,C.UserName,C.UserEmail,C.UserURL,C.Comment,C.DateComment,C.Admin,P.AvatarURL
                FROM blogComments C
                LEFT JOIN cpo_people P ON P.PID = C.PID
                WHERE C.CID = '$cID'
                ORDER BY C.DateComment ASC";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function saveBlogPostComment($commentArray) {
        $sql = "INSERT into blogComments (
                PID,
                UserName,
                UserEmail,
                UserURL,
                Comment,
                DateComment,
                BEID,
                Admin
                ) values (
                '".$commentArray['PID']."',
                '".$commentArray['commentName']."',
                '".$commentArray['commentEmail']."',
                '".$commentArray['commentURL']."',
                '".$commentArray['commentText']."',
                '".date('Y-m-d G:i:s')."',
                '".$commentArray['BEID']."',
                '".$commentArray['Admin']."'
                )";
        $query = $this->db->query($sql);
        return $this->db->insert_id();
    }

    function saveBlogPost($postArray) {
        if ($postArray['BEID']>0) {
            $beID = $postArray['BEID'];

            $sql = "UPDATE blogEntry SET
                    Title        = '".$postArray['Title']."',
                    Tags         = '".$postArray['Tags']."',
                    BlogEntry    = ".$postArray['Content'].",
                    EntryURL     = '".$postArray['EntryURL']."',
                    DateUpdate   = '".date('Y-m-d G:i:s')."',
                    UseridUpdate = '".$postArray['AuthorUserid']."'
                    WHERE
                    BEID = '$beID'";
            $query = $this->db->query($sql);
        } else {
            $sql = "INSERT into blogEntry (
                    Userid,
                    Title,
                    BlogEntry,
                    DateEntry,
                    Tags,
                    Active,
                    EntryURL,
                    DateUpdate,
                    UseridUpdate
                    ) values (
                    '".$postArray['AuthorUserid']."',
                    '".$postArray['Title']."',
                    ".$postArray['Content'].",
                    '".date('Y-m-d G:i:s')."',
                    '".$postArray['Tags']."',
                    '0',
                    '".$postArray['EntryURL']."',
                    '".date('Y-m-d G:i:s')."',
                    '".$postArray['AuthorUserid']."'
                    )";
            $query = $this->db->query($sql);
            $beID = $this->db->insert_id();
        }

        return $beID;
    }

    function updatePostStatus($beID,$newStatus) {
        if ($newStatus == 1) {
            $sql = "UPDATE blogEntry SET
                    DateEntry = '".date('Y-m-d G:i:s')."',
                    Active = '$newStatus'
                WHERE BEID = '$beID'";
        } else {
            $sql = "UPDATE blogEntry SET Active = '$newStatus' WHERE BEID = '$beID'";
        }
        $query = $this->db->query($sql);
    }

    function getBlogTags($withCount=NULL) {
        if ($withCount == TRUE) {
            $sql = "SELECT T.TID,T.TagName,T.TagURL,
                    (SELECT COUNT(BEID) FROM blogEntry BE WHERE BE.Tags LIKE concat('%',T.TagName,'%') AND BE.Deleted = 0) AS tagCount
                    FROM blogTags T
                    ORDER BY TagName ASC";
        } else {
            $sql = "SELECT * from blogTags ORDER BY TagName ASC";
        }
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function deleteBlogPost($beID,$tagForDelete=TRUE,$userid) {
         if ($tagForDelete == TRUE) {
             $sql = "UPDATE blogEntry SET UseridUpdate = '$userid', Deleted = 1 WHERE BEID = '$beID'";
             $query = $this->db->query($sql);
         } else {
             $sql = "DELETE from blogEntry WHERE BEID = '$beID'";
             $query = $this->db->query($sql);
         }
    }

    function restoreBlogPost($beID,$userid) {
        $sql = "UPDATE blogEntry SET UseridUpdate = '$userid', Deleted = 0 WHERE BEID = '$beID'";
        $query = $this->db->query($sql);
    }

    function incrementEntryViews($beID) {
        $sql = "UPDATE blogEntry SET Views = Views+1 WHERE BEID = '$beID'";
        $query = $this->db->query($sql);
    }
}
?>