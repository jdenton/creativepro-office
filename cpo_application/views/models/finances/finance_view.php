<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Finance_view extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }
    
    /**
	 * getInvoiceTotals : get invoice sum for an account owner, client or project.
	 *
	 * @access	public
	 * @param	int    $itemID Userid, ClientID, ProjID depending on if we want all invoices 
	 *                         for account owner, all invoices for a client or all invoices for a project.
	 * @param   string $group  user, client, project	 
	 * @return	array  Array of project data
	 */	
    function getInvoiceTotals($itemID,$group=null) {
    	$sql = "SELECT SUM(InvTotal) AS Total FROM cpo_invoice
				WHERE ";
    	switch ($group) {
			case 'user':
			    $sql .= "Userid = '$itemID'";
			    break;
			case 'project':
			    $sql .= "ProjectID = '$itemID'";
			    break;
			case 'client':
			    $sql .= "ToUID = '$itemID' AND ToType = 'client' ";
			    break;
			default:
				$sql .= "Userid = '$itemID'";
			    break;    
		}
		$sql .= " AND Deleted = 0";
		$query = $this->db->query($sql); 			
		return $query->row_array();
    }

    function getInvoiceTotal($invoiceID) {
        $sql = "SELECT InvTotal from cpo_invoice WHERE InvoiceID = '$invoiceID'";
        $query = $this->db->query($sql);
        $invoiceTotalArray = $query->row_array();
        return $invoiceTotalArray['InvTotal'];
    }
    
    /**
	 * getInvoiceCount : get invoice count for an account owner, client or project.
	 *
	 * @access	public
	 * @param	int    $itemID Userid, ClientID, ProjID depending on if we want all invoices 
	 *                         for account owner, all invoices for a client or all invoices for a project.
	 * @param   string $group  user, client, project	 
	 * @return	array  Array of project data
	 */	
	function getInvoiceCount($itemID,$group=null,$accountUserid=null) {
    	$sql = "SELECT COUNT(InvoiceID) AS InvoiceCount FROM cpo_invoice
				WHERE ";
    	switch ($group) {
			case 'user':
			    $sql .= "Userid = '$itemID'";
			    break;
			case 'project':
			    $sql .= "ProjectID = '$itemID'";
			    break;
			case 'client':
			    $sql .= "ToUID = '$itemID' AND ToType = 'client' ";
			    break;
			default:
				$sql .= "Userid = '$itemID'";
			    break;    
		}
		$sql .= " AND Userid = '".$accountUserid."' AND Deleted = 0";
		$query = $this->db->query($sql); 			
		return $query->row_array();
    }
    
    function getInvoiceListForUser($accountUserid) {
    	$sql = "SELECT * FROM cpo_invoice
				WHERE
				Userid = '$accountUserid'
				AND Deleted = 0";
    	$query = $this->db->query($sql); 	
    	return $query->result_array();		
    }
    
	function getInvoiceList($itemID,$group=NULL,$tag=NULL,$status=NULL,$filterArray=NULL,$accountUserid=NULL,$term=null) {
		$sql = "SELECT 
					I.InvoiceID,I.InvNo,I.ProjectID,I.Title,I.DateInvoice,I.InvTotal,
					I.Sent,I.DateEntry,I.Status,I.DateSent,I.Paid,I.DatePaid,
                    I.PaymentDueDate,I.PaymentDueDays,I.Recurring,I.Estimate,
					C.ClientID,C.Company,
                    P.Title AS Project,
                    (SELECT COUNT(MID) FROM cpo_messages M WHERE M.ItemID = I.InvoiceID AND M.ItemType = 'invoice') AS Messages,
                    (SELECT SUM(PaymentAmount) FROM cpo_invoice_payment IP WHERE IP.InvoiceID = I.InvoiceID) AS TotalPayments
                FROM cpo_invoice I
				LEFT JOIN cpo_clients C ON C.ClientID = I.ClientID
                LEFT JOIN cpo_project P ON P.ProjectID = I.ProjectID
				WHERE ";
    	switch ($group) {
			case 'user':
			    $sql .= "I.Userid = '$itemID'";
			    break;
			case 'client':
                $sql .= "I.ToUID = '$itemID' AND I.ToType = 'client' ";
			    break;
			case 'project':
			    $sql .= "I.ProjectID = '$itemID'";
			    break;
			case 'tag':
			    $sql .= "(I.Tags LIKE '$itemID%' OR I.Tags LIKE '% $itemID%')";
			    break;
			default:
				$sql .= "I.Userid = '$itemID'";
			    break;    
		}
        
        if (!empty($term)) {
            $sql .= " AND (
                        I.Title LIKE '$term%' OR I.Title LIKE '% $term%' OR
                        I.InvComments LIKE '$term%' OR I.InvComments LIKE '% $term%' OR
                        I.InvNo LIKE '$term%' OR I.InvNo LIKE '% $term%' OR
                        C.Company LIKE '$term%' OR C.Company LIKE '% $term%' OR
                        P.Title LIKE '$term%' OR P.Title LIKE '% $term%'
                    ) ";
        }

		if (!empty($tag)) {
			$sql .= " AND (I.Tags LIKE '$tag%' OR I.Tags LIKE '% $tag%') ";
		}
        if ($status != 'all' && $status != NULL && !isset($filterArray)) {
            $sql .= " AND I.Status = $status ";
        }

        /*
         * Look at filterArray
         */
        if (isset($filterArray)) {
            if ($filterArray['statusAll'] != 'true') {
                $statusSearchOpen = 0;
                $statusSearchClosed = 0;
                if ($filterArray['statusOpen'] == 'true') {
                    $sql .= " AND I.Status = 0 ";
                } elseif ($filterArray['statusClosed'] == 'true') {
                    $sql .= " AND I.Status = 1 ";
                }
                if ($filterArray['statusPastDue'] == 'true') {
                    $sql .= " AND (I.DatePaid Is Null OR I.DatePaid = '0000-00-00') AND I.PaymentDueDate < '".date('Y-m-d')."' ";
                }                
            }
            if (!empty($filterArray['dateStart'])) {
                $sql .= " AND I.DateInvoice >= '".$filterArray['dateStart']."' ";
            }
            if (!empty($filterArray['dateEnd'])) {
                $sql .= " AND I.DateInvoice <= '".$filterArray['dateEnd']."' ";
            }
        }

		$sql .= " AND I.Userid = '".$accountUserid."' AND I.Deleted = 0 ";
		$sql .= " ORDER BY I.InvoiceID DESC";
        $query = $this->db->query($sql);
		return $query->result_array();
    }
    
    function getInvoicesForLedger($dateStart=null,$dateEnd=null,$itemID=null,$group=null,$tag=null,$term=null,$accountUserid=null) {
        /*
         * Get invoices and estimates
         */
        $sql = "SELECT 
					I.InvoiceID,I.InvNo,I.ProjectID,I.Title,I.DateInvoice,I.InvTotal,
					I.Sent,I.DateEntry,I.Status,I.DateSent,I.Paid,I.DatePaid,
                    I.PaymentDueDate,I.PaymentDueDays,I.Recurring,I.InvComments,I.Tags,
					C.ClientID,C.Company,
                    P.Title AS Project,
                    (SELECT COUNT(MID) FROM cpo_messages M WHERE M.ItemID = I.InvoiceID AND M.ItemType = 'invoice') AS Messages,
                    (SELECT SUM(PaymentAmount) 
                        FROM cpo_invoice_payment IP 
                        WHERE IP.InvoiceID = I.InvoiceID AND 
                        IP.DatePayment >= '$dateStart' AND IP.DatePayment <= '$dateEnd') 
                    AS TotalPaymentsForDateRange,
                    (SELECT SUM(PaymentAmount) 
                        FROM cpo_invoice_payment IP 
                        WHERE IP.InvoiceID = I.InvoiceID) 
                    AS TotalPayments
                FROM cpo_invoice I
				LEFT JOIN cpo_clients C ON C.ClientID = I.ClientID
                LEFT JOIN cpo_project P ON P.ProjectID = I.ProjectID
				WHERE ";
    	switch ($group) {
			case 'user':
			    $sql .= "I.Userid = '$itemID'";
			    break;
			case 'client':
                $sql .= "I.ToUID = '$itemID' AND I.ToType = 'client' ";
			    break;
			case 'project':
			    $sql .= "I.ProjectID = '$itemID'";
			    break;
			case 'tag':
			    $sql .= "(I.Tags LIKE '$itemID%' OR I.Tags LIKE '% $itemID%')";
			    break;
			default:
				$sql .= "I.Userid = '$itemID'";
			    break;    
		}

		if (!empty($tag)) {
			$sql .= " AND (I.Tags LIKE '$tag%' OR I.Tags LIKE '% $tag%') ";
		}
        if ($status != 'all' && $status != NULL && !isset($filterArray)) {
            $sql .= " AND I.Status = $status ";
        }
        
        $sql .= " AND I.Userid = '".$accountUserid."' AND I.Deleted = 0 ";
        $sql .= " AND I.DateInvoice >= '$dateStart' AND I.DateInvoice <= '$dateEnd' ";
        $sql .= " AND I.Estimate = 0 "; 
		$sql .= " ORDER BY I.InvoiceID DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function getIncomeForLedger($dateStart=null,$dateEnd=null,$itemID=null,$tag=null,$term=null,$group=null,$accountUserid=null) {
        /*
         * We changed the invoice status update method to put the invoice total into
         * the payments table IF the invoice had no prior payments. So, now all we 
         * have to do is look at the payments table for all income. Aren't we smart!
         */
        $sql = "SELECT 
					I.InvoiceID,I.InvNo,I.ProjectID,I.Title,I.DateInvoice,
					I.Sent,I.DateEntry,I.Status,I.DateSent,I.Paid,I.DatePaid,
                    I.PaymentDueDate,I.PaymentDueDays,I.Recurring,I.InvComments,I.Tags,
					C.ClientID,C.Company,
                    P.Title AS Project,
                    IP.PaymentID,IP.PaymentAmount AS Total, IP.DatePayment
                FROM cpo_invoice_payment IP
                LEFT JOIN cpo_invoice I ON I.InvoiceID = IP.InvoiceID
				LEFT JOIN cpo_clients C ON C.ClientID = I.ClientID
                LEFT JOIN cpo_project P ON P.ProjectID = I.ProjectID
				WHERE 
                I.Userid = '".$accountUserid."' AND I.Deleted = 0
                ";
        if (!empty($dateStart) && !empty($dateEnd)) {
            $sql .= " AND IP.DatePayment >= '$dateStart' AND IP.DatePayment <= '$dateEnd' ";
        }
        
        if (!empty($itemID)) {
            switch ($group) {
                case 'project':
                    $sql .= " AND P.ProjectID = $itemID ";
                    break;
                case 'client':
                    $sql .= " AND C.ClientID = $itemID ";
                    break;
                case 'user':
                    $sql .= " AND I.UseridEntry = $itemID ";
                    break;
            }
        } elseif (!empty($term)) {
            $sql .= " AND (
                        I.Title LIKE '$term%' OR I.Title LIKE '% $term%' OR
                        I.InvComments LIKE '$term%' OR I.InvComments LIKE '% $term%' OR
                        I.InvNo LIKE '$term%' OR I.InvNo LIKE '% $term%' OR
                        C.Company LIKE '$term%' OR C.Company LIKE '% $term%' OR
                        P.Title LIKE '$term%' OR P.Title LIKE '% $term%'
                    ) ";
        }
        
        if (!empty($tag)) {
            $sql .= " AND (I.Tags LIKE '$tag%' OR I.Tags LIKE '% $tag%') ";
        }
        
        $sql .= " AND I.Estimate = 0
                ORDER BY I.InvoiceID DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    /**
	 * getInvoiceDetails : get all invoice data for 1 invoice.
	 *
	 * @access	public
	 * @param	int    $invoiceID the ID of the invoice we want.
	 * @return	array  Array of project data
	 */	
    function getInvoiceDetails($invoiceID) {
    	$sql = "SELECT
					I.InvoiceID,I.Userid,I.InvNo,I.ToUID,I.ToType,I.ClientID,I.ProjectID,I.CatID,I.Title,I.InvComments,I.IncComments,
                    I.InvSubTotal,I.ShippingAmount,I.DiscountAmount,I.TaxAmount,I.InvTotal,
					I.Paid,I.PayNo,I.PONumber,I.Sent,I.PaymentMethod,I.Status,I.TaxRate,I.TaxName,I.TaxID,
                    I.DiscountRate,I.PaymentDueDays,I.PaymentDueDate,I.LateFeePercent,I.LateFeeAmount,
                    I.Tags,I.DateInvoice,I.DateSent,I.DatePaid,I.DateEntry,I.Recurring,I.Estimate,
					II.InvoiceItemID,II.ItemDesc,II.Kind,II.Qty,II.Taxable,II.CostPer,II.ItemTotal,II.TimeSheetID,
					C.ClientUserid,C.Company,C.Address,C.City,C.State,C.Zip,C.Country,C.Phone1,C.Email,C.URL,C.Language,
                    CAT.MainCat
				FROM cpo_invoice I 
				LEFT JOIN cpo_invoice_items II ON II.InvoiceID = I.InvoiceID
				LEFT JOIN cpo_clients C ON C.ClientID = I.ClientID
                LEFT JOIN cpo_main_cat CAT ON CAT.CatID = I.CatID
				WHERE 
                I.InvoiceID = '$invoiceID'
                ORDER BY II.InvoiceItemID ASC";
    	$query = $this->db->query($sql);
    	
    	$invID = 0;
    	$j = 0;
    	$contactArray = array();
   		foreach ($query->result_array() as $row) {
   			$newInvID = $row['InvoiceID'];
   			if ($newInvID != $invID) {
   				$invID = $newInvID;
   				$k=$j;
   				$l=0;
   				$invoiceArray[$j]['InvoiceID']      = $row['InvoiceID'];
   				$invoiceArray[$j]['Userid']         = $row['Userid'];
   				$invoiceArray[$j]['InvNo']          = $row['InvNo'];
   				$invoiceArray[$j]['ClientID']       = $row['ClientID'];
                $invoiceArray[$j]['ToUID']          = $row['ToUID'];
                $invoiceArray[$j]['ToType']         = $row['ToType'];
				$invoiceArray[$j]['ProjectID']      = $row['ProjectID'];
   				$invoiceArray[$j]['CatID']          = $row['CatID'];
                $invoiceArray[$j]['Category']       = $row['MainCat'];
                $invoiceArray[$j]['PONumber']       = $row['PONumber'];
   				$invoiceArray[$j]['Title']          = $row['Title'];
   				$invoiceArray[$j]['InvComments']    = $row['InvComments'];
   				$invoiceArray[$j]['IncComments']    = $row['IncComments'];
                $invoiceArray[$j]['InvSubTotal']    = $row['InvSubTotal'];
                $invoiceArray[$j]['ShippingAmount'] = $row['ShippingAmount'];
                $invoiceArray[$j]['DiscountAmount'] = $row['DiscountAmount'];
                $invoiceArray[$j]['TaxAmount']      = $row['TaxAmount'];
                $invoiceArray[$j]['InvTotal']       = $row['InvTotal'];
                $invoiceArray[$j]['Paid']           = $row['Paid']; 
                $invoiceArray[$j]['PayNo']          = $row['PayNo'];
                $invoiceArray[$j]['PaymentMethod']  = $row['PaymentMethod'];
                $invoiceArray[$j]['Sent']           = $row['Sent'];
                $invoiceArray[$j]['DateSent']       = $row['DateSent'];
                $invoiceArray[$j]['DatePaid']       = $row['DatePaid'];
                $invoiceArray[$j]['Status']         = $row['Status'];
                $invoiceArray[$j]['TaxRate']        = $row['TaxRate'];
   				$invoiceArray[$j]['TaxName']        = $row['TaxName'];
                $invoiceArray[$j]['TaxID']          = $row['TaxID'];
                $invoiceArray[$j]['DiscountRate']   = $row['DiscountRate'];
                $invoiceArray[$j]['PaymentDueDays'] = $row['PaymentDueDays'];
   				$invoiceArray[$j]['PaymentDueDate'] = $row['PaymentDueDate'];
                $invoiceArray[$j]['LateFeePercent'] = $row['LateFeePercent'];
   				$invoiceArray[$j]['LateFeeAmount']  = $row['LateFeeAmount'];
                $invoiceArray[$j]['Tags']           = $row['Tags'];
   				$invoiceArray[$j]['DateInvoice']    = $row['DateInvoice'];
   				$invoiceArray[$j]['DateEntry']      = $row['DateEntry'];
                $invoiceArray[$j]['Recurring']      = $row['Recurring'];
                $invoiceArray[$j]['Estimate']       = $row['Estimate'];

                $invoiceArray[$j]['ClientUserid']   = $row['ClientUserid'];
				$invoiceArray[$j]['ClientCompany']  = $row['Company'];
				$invoiceArray[$j]['ClientAddress']  = $row['Address'];
				$invoiceArray[$j]['ClientCity']     = $row['City'];
				$invoiceArray[$j]['ClientState']    = $row['State'];
				$invoiceArray[$j]['ClientZip']      = $row['Zip'];
				$invoiceArray[$j]['ClientCountry']  = $row['Country'];
				$invoiceArray[$j]['ClientPhone']    = $row['Phone1'];
				$invoiceArray[$j]['ClientEmail']    = $row['Email'];
                $invoiceArray[$j]['ClientURL']      = $row['URL'];
				$invoiceArray[$j]['ClientLanguage'] = $row['Language'];
   				$j++;
   			}
   			$invoiceArray[$k]['InvoiceItems'][$l]['InvoiceItemID'] = $row['InvoiceItemID'];
   			$invoiceArray[$k]['InvoiceItems'][$l]['ItemDesc']      = $row['ItemDesc'];
   			$invoiceArray[$k]['InvoiceItems'][$l]['Kind']          = $row['Kind'];
   			$invoiceArray[$k]['InvoiceItems'][$l]['Qty']           = $row['Qty'];
			$invoiceArray[$k]['InvoiceItems'][$l]['CostPer']       = $row['CostPer'];
            $invoiceArray[$k]['InvoiceItems'][$l]['Taxable']       = $row['Taxable'];
			$invoiceArray[$k]['InvoiceItems'][$l]['ItemTotal']     = $row['ItemTotal'];
			$invoiceArray[$k]['InvoiceItems'][$l]['TimeSheetID']   = $row['TimeSheetID'];
   			$l++;
   		}
		return $invoiceArray;		
    }

    function getInvoiceItems($invoiceID) {
        $sql = "SELECT * from cpo_invoice_items WHERE InvoiceID = '$invoiceID'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getInvoiceStatus($invoiceID) {
        $sql = "SELECT * FROM cpo_invoice WHERE InvoiceID = '$invoiceID'";
    	$query = $this->db->query($sql);
        $invoiceArray = $query->row_array();
        $statusArray = getInvoiceStatus($invoiceArray['Status'],$invoiceArray['Paid'],$invoiceArray['Sent'],$invoiceArray['DateInvoice'],$invoiceArray['DatePaid'],$invoiceArray['DateSent'],$invoiceArray['PaymentDueDate'],$invoiceArray['PaymentDueDays']);
        return $statusArray;
    }

	/**
	 * checkForValidInvoice : Make sure that user has view rights for this invoice.
     *                        This prevents users from seeing other users invoices by
     *                        simply typing a random invoiceID in the URL.
	 *
	 * @access public
	 * @param  int     $invoiceID Invoice ID of invoice we're wanting to verify.
	 * @return boolean true if invoice can be viewed by this user
	 */
    function checkForValidInvoice($invoiceID,$userid=NULL,$accountUserid=NULL,$clientUserid=NULL,$viewAll=FALSE) {
        $valid = FALSE;
        if ($clientUserid>0) {
            /*
             * Make sure this client has the rights to view this invoice.
             */
            $sql = "SELECT InvoiceID
				  FROM cpo_invoice
				  WHERE
					InvoiceID = '$invoiceID' AND
					ToUID = '$clientUserid' AND
                    ToType = 'client'";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                $valid = TRUE;
            }
        } elseif ($viewAll == FALSE) {
            /*
             * This is a team member that can only view their own invoices.
             */
            $sql = "SELECT InvoiceID
				    FROM cpo_invoice
				    WHERE
                        InvoiceID = '$invoiceID' AND
                        ToUID = '$userid' AND
                        ToType = 'team'";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                $valid = TRUE;
            }
        } else {
            /*
             * We're an account owner...see if this invoice belongs to our account.
             */
            $sql = "SELECT InvoiceID
				    FROM cpo_invoice
				    WHERE
                        InvoiceID = '$invoiceID' AND
                        Userid = '".$accountUserid."'";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
               $valid = TRUE;
            }
        }
        return $valid;
     }

     function checkForValidExpense($expenseID,$userid=NULL,$accountUserid=NULL,$clientUserid=NULL,$viewAll=FALSE) {
        $valid = FALSE;
        if ($clientUserid>0) {
            /*
             * Make sure this client has the rights to view this expense.
             */
            $sql = "SELECT ExpenseID
				  FROM cpo_expense
				  WHERE
					ExpenseID = '$expenseID' AND
					ClientID = '$clientUserid'";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                $valid = TRUE;
            }
        } elseif ($viewAll == FALSE) {
            /*
             * This is a team member that can only view their own expenses.
             */
            $sql = "SELECT ExpenseID
				    FROM cpo_expense
				    WHERE
                        ExpenseID = '$invoiceID' AND
                        UseridEntry = '$userid' AND
                        Userid = '".$accountUserid."'";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
                $valid = TRUE;
            }
        } else {
            /*
             * We're an account owner...see if this expense belongs to our account.
             */
            $sql = "SELECT ExpenseID
				    FROM cpo_expense
				    WHERE
                        ExpenseID = '$expenseID' AND
                        Userid = '".$accountUserid."'";
            $query = $this->db->query($sql);
            if ($query->num_rows()>0) {
               $valid = TRUE;
            }
        }
        return $valid;
     }
     
	 /**
	 * getLastInvoiceNumber : Retrieve the last invoice number used by this user account.
	 *
	 * @access public
	 * @return string $invoiceNumber The last invoice number used.
	 */
	 function getLastInvoiceNumber($accountUserid) {
		 $sql = "SELECT InvNo from cpo_invoice
				WHERE Userid = '$accountUserid'
				ORDER BY InvoiceID DESC
				LIMIT 1";
		 $query = $this->db->query($sql);
		 if ($query->num_rows()>0) {
			 $row = $query->row_array();
			 return $row['InvNo'];
		 } else {
			 return false;
		 }
	 }

	 /**
	 * getLedgerForAutoCompleter : get payments and expenses for autocomplete searchbox
	 *
	 * @access	public
	 * @param   string $q search term
	 * @return	string pipe delimited string of projects
	 */
    function getLedgerForAutoCompleter($q,$accountUserid) {
		$ledgerArray = '';
        $a=0;
		/*
		 * First let's look for matching invoices (only invoices with payments, remember)
		 */
    	$sql = "SELECT I.InvoiceID,I.InvNo,I.Title
                FROM cpo_invoice_payment IP
                LEFT JOIN cpo_invoice I ON I.InvoiceID = IP.InvoiceID
				WHERE
				(
					I.Title LIKE '$q%' OR I.Title LIKE '% $q%' OR
                    I.InvNo LIKE '$q%' OR I.InvNo LIKE '% $q%' OR
					I.InvComments LIKE '$q%' OR I.InvComments LIKE '% $q%'
				)
				AND I.Userid = '".$accountUserid."'
                AND I.Estimate = 0    
				AND I.Deleted = 0
				ORDER BY I.Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                $ledgerArray[$a]['id'] = $row['InvoiceID'];
                $ledgerArray[$a]['label'] = $row['InvNo'].' '.$row['Title'];
                $ledgerArray[$a]['type']  = 'invoice';
                $ledgerArray[$a]['ledgerType'] = 'income';
                $ledgerArray[$a]['count'] = '';
                $ledgerArray[$a]['icon']  = 'icon_invoice_small';
                $a++;
            }
		}
        
        /*
         * Now let's look for any matching expenses
         */
        $sql = "SELECT E.ExpenseID,E.Title
				FROM cpo_expense E
				WHERE
				(	
                    E.Title LIKE '$q%' OR E.Title LIKE '% $q%' OR
					E.Comments LIKE '$q%' OR E.Comments LIKE '% $q%'
				)
				AND E.Userid = '".$accountUserid."'
				AND E.Deleted = 0
				ORDER BY E.Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                $ledgerArray[$a]['id'] = $row['ExpenseID'];
                $ledgerArray[$a]['label'] = $row['Title'];
                $ledgerArray[$a]['type']  = 'expense';
                $ledgerArray[$a]['ledgerType'] = 'expense';
                $ledgerArray[$a]['count'] = '';
                $ledgerArray[$a]['icon']  = 'icon_expense_small';
                $a++;
            }
		}

		/*
		 * Now, let's look for matching tags (Invoices - only invoices with payments, Expenses)
		 */
        $sql = "SELECT T.Tag,
                (
                    SELECT COUNT(IP.InvoiceID) FROM cpo_invoice_payment IP 
                    LEFT JOIN cpo_invoice I ON I.InvoiceID = IP.InvoiceID 
                    WHERE I.Deleted = 0 AND I.Estimate = 0 AND IP.PaymentAmount IS NOT NULL AND 
                    (I.Tags LIKE '$q%' OR I.Tags LIKE '% $q%')
                ) AS Invoices,
                (
                    SELECT COUNT(E.ExpenseID) FROM cpo_expense E 
                    WHERE (E.Tags LIKE '$q%' OR E.Tags LIKE '% $q%') AND E.Deleted = 0
                ) AS Expenses
                FROM cpo_tags T
				WHERE
				(
					T.Tag LIKE '$q%' OR T.Tag LIKE '% $q%'
				)
				AND T.Userid = '".$accountUserid."'
                AND (T.SiteArea = 'expense' OR T.SiteArea = 'invoice')
                GROUP BY T.TID, T.Tag
				ORDER BY T.Tag";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				if ($row['Invoices']>0 || $row['Expenses']) {
                    $totalCount = $row['Invoices'] + $row['Expenses'];
                    $ledgerArray[$a]['id'] = $row['Tag'];
                    $ledgerArray[$a]['label'] = $row['Tag'];
                    $ledgerArray[$a]['type']  = 'tag';
                    $ledgerArray[$a]['ledgerType'] = 'income';
                    $ledgerArray[$a]['count'] = $totalCount;
                    $ledgerArray[$a]['icon']  = 'icon_tag tag';
                    $a++;
				}
			}
		}
        
        /*
		 * Now, let's look for any project titles that match (Invoices - only those with payments, Expenses)
		 */
        $sql = "SELECT P.ProjectID,P.Title,
                (
                    SELECT COUNT(IP.InvoiceID) FROM cpo_invoice_payment IP 
                    LEFT JOIN cpo_invoice I ON I.InvoiceID = IP.InvoiceID 
                    WHERE IP.PaymentAmount IS NOT NULL AND I.ProjectID = P.ProjectID AND I.Deleted = 0 AND I.Estimate = 0
                ) AS Invoices,
                (
                    SELECT COUNT(E.ExpenseID) FROM cpo_expense E 
                    WHERE E.ProjectID = P.ProjectID AND E.Deleted = 0
                ) AS Expenses
                FROM cpo_project P
				WHERE
				(
					P.Title LIKE '$q%' OR P.Title LIKE '% $q%'
				)
				AND P.Userid = '".$accountUserid."'
                AND P.Deleted = 0
                GROUP BY P.ProjectID, P.Title
				ORDER BY P.Title";
        $query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				if ($row['Invoices']>0 || $row['Expenses']) {
                    $totalCount = $row['Invoices']+$row['Expenses'];
                    $ledgerArray[$a]['id'] = $row['ProjectID'];
                    $ledgerArray[$a]['label'] = $row['Title'];
                    $ledgerArray[$a]['type']  = 'project';
                    $ledgerArray[$a]['ledgerType'] = 'expense';
                    $ledgerArray[$a]['count'] = $totalCount;
                    $ledgerArray[$a]['icon']  = 'icon_project_small';
                    $a++;
                }
			}
		}

		/*
		 * Now, let's look for any clients that match (Invoices - Only those with payments & Expenses)
		 */
        $sql = "SELECT C.ClientID,C.Company,
                (
                    SELECT COUNT(IP.InvoiceID) FROM cpo_invoice_payment IP 
                    LEFT JOIN cpo_invoice I ON I.InvoiceID = IP.InvoiceID 
                    WHERE IP.PaymentAmount IS NOT NULL AND I.ClientID = C.ClientID AND I.Deleted = 0 AND I.Estimate = 0
                ) AS Invoices,
                (
                    SELECT COUNT(E.ExpenseID) FROM cpo_expense E 
                    WHERE E.ClientID = C.ClientID AND E.Deleted = 0
                ) AS Expenses
                FROM cpo_clients C
				WHERE
				(
					C.Company LIKE '$q%' OR C.Company LIKE '% $q%'
				)
				AND C.Userid = '".$accountUserid."'
                AND C.Deleted = 0
                GROUP BY C.ClientID, C.Company
				ORDER BY C.Company";
        $query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				if ($row['Invoices']>0 || $row['Expenses']) {
                    $totalCount = $row['Invoices']+$row['Expenses'];
                    $ledgerArray[$a]['id'] = $row['ClientID'];
                    $ledgerArray[$a]['label'] = $row['Company'];
                    $ledgerArray[$a]['type']  = 'client';
                    $ledgerArray[$a]['ledgerType'] = 'expense';
                    $ledgerArray[$a]['count'] = $totalCount;
                    $ledgerArray[$a]['icon']  = 'icon_client_small';
                    $a++;
                }
			}
		}
		return json_encode($ledgerArray);
    }
    
    /**
	 * getInvoicesForAutoCompleter : get Invoices and Estimates for autocomplete searchbox
	 *
	 * @access	public
	 * @param   string $q search term
	 * @return	string pipe delimited string of projects
	 */
    function getInvoicesForAutoCompleter($q,$accountUserid) {
		$ledgerArray = '';
        $a=0;
		/*
		 * First let's look for matching invoices (only invoices with payments, remember)
		 */
    	$sql = "SELECT I.InvoiceID,I.InvNo,I.Title
                FROM cpo_invoice I
				WHERE
				(
					I.Title LIKE '$q%' OR I.Title LIKE '% $q%' OR
                    I.InvNo LIKE '$q%' OR I.InvNo LIKE '% $q%' OR
					I.InvComments LIKE '$q%' OR I.InvComments LIKE '% $q%'
				)
				AND I.Userid = '".$accountUserid."'
				AND I.Deleted = 0
				ORDER BY I.Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                $ledgerArray[$a]['id'] = $row['InvoiceID'];
                $ledgerArray[$a]['label'] = $row['InvNo'].' '.$row['Title'];
                $ledgerArray[$a]['type']  = 'invoice';
                $ledgerArray[$a]['count'] = '';
                $ledgerArray[$a]['icon']  = 'icon_invoice_small';
                $a++;
            }
		}

		/*
		 * Now, let's look for matching tags
		 */
        $sql = "SELECT T.Tag,
                (
                    SELECT COUNT(I.InvoiceID) FROM cpo_invoice I WHERE I.Deleted = 0 AND
                    (I.Tags LIKE '$q%' OR I.Tags LIKE '% $q%')
                ) AS Invoices
                FROM cpo_tags T
				WHERE
				(
					T.Tag LIKE '$q%' OR T.Tag LIKE '% $q%'
				)
				AND T.Userid = '".$accountUserid."'
                AND T.SiteArea = 'invoice'
                GROUP BY T.TID, T.Tag
				ORDER BY T.Tag";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				if ($row['Invoices']>0) {
                    $totalCount = $row['Invoices'];
                    $ledgerArray[$a]['id'] = $row['Tag'];
                    $ledgerArray[$a]['label'] = $row['Tag'];
                    $ledgerArray[$a]['type']  = 'tag';
                    $ledgerArray[$a]['count'] = $totalCount;
                    $ledgerArray[$a]['icon']  = 'icon_tag tag';
                    $a++;
				}
			}
		}
        
        /*
		 * Now, let's look for any project titles that match (Invoices)
		 */
        $sql = "SELECT P.ProjectID,P.Title,
                (
                    SELECT COUNT(I.InvoiceID) FROM cpo_invoice I 
                    WHERE I.ProjectID = P.ProjectID AND I.Deleted = 0
                ) AS Invoices
                FROM cpo_project P
				WHERE
				(
					P.Title LIKE '$q%' OR P.Title LIKE '% $q%'
				)
				AND P.Userid = '".$accountUserid."'
                AND P.Deleted = 0
                GROUP BY P.ProjectID, P.Title
				ORDER BY P.Title";
        $query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				if ($row['Invoices']>0) {
                    $totalCount = $row['Invoices'];
                    $ledgerArray[$a]['id'] = $row['ProjectID'];
                    $ledgerArray[$a]['label'] = $row['Title'];
                    $ledgerArray[$a]['type']  = 'project';
                    $ledgerArray[$a]['count'] = $totalCount;
                    $ledgerArray[$a]['icon']  = 'icon_project_small';
                    $a++;
                }
			}
		}

		/*
		 * Now, let's look for any clients that match (Invoices)
		 */
        $sql = "SELECT C.ClientUserid,C.Company,
                (
                    SELECT COUNT(I.InvoiceID) FROM cpo_invoice I
                    WHERE I.ToUID = C.ClientUserid AND I.Deleted = 0
                ) AS Invoices
                FROM cpo_clients C
				WHERE
				(
					C.Company LIKE '$q%' OR C.Company LIKE '% $q%'
				)
				AND C.Userid = '".$accountUserid."'
                AND C.Deleted = 0
                GROUP BY C.ClientUserid, C.Company
				ORDER BY C.Company";
        $query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				if ($row['Invoices']>0) {
                    $totalCount = $row['Invoices'];
                    $ledgerArray[$a]['id'] = $row['ClientUserid'];
                    $ledgerArray[$a]['label'] = $row['Company'];
                    $ledgerArray[$a]['type']  = 'client';
                    $ledgerArray[$a]['count'] = $totalCount;
                    $ledgerArray[$a]['icon']  = 'icon_client_small';
                    $a++;
                }
			}
		}
       
		return json_encode($ledgerArray);
    }

    function getInvoiceListForSelect($clientID=NULL,$projectID=NULL,$status=NULL,$accountUserid=null) {
        switch ($status) {
            case 'open_not_sent':
                $statusWhere = " AND Sent <> 1 AND Status = 0 ";
                break;
            case 'open sent':
                break;
            case 'sent':
                break;
            case 'open':
                break;
            case 'closed':
                break;
        }
        
        $where = '';
        if ($clientID>0) {
            $where .= " ToUID = '$clientID' AND ToType = 'client' ";
        }
        if ($projectID>0) {
            if ($clientID>0) {
                $where .= " AND ";
            }
            $where .= " ProjectID = '$projectID'";
        }

        $sql = "SELECT InvoiceID,InvNo,Title
                FROM cpo_invoice
                WHERE
                $where
                $statusWhere
                AND Userid = '".$accountUserid."'
                ORDER BY DateEntry";
        $query = $this->db->query($sql);

        if ($query->num_rows()>0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getInvoicePayments($invoiceID) {
        $sql = "SELECT * from cpo_invoice_payment
                WHERE
                    InvoiceID = '$invoiceID' AND
                    Deleted = 0
                ORDER BY DatePayment DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /**
	 * getInvoicesForCalendar
	 * @return	array PHP array of invoice information
	 */
	function getInvoicesForCalendar($dateStart,$dateEnd=NULL,$accountUserid) {

	}

    function getInvoiceFromPrevInvoiceID($prevInvoiceID,$accountUserid) {
        $sql = "SELECT * from cpo_invoice WHERE PrevInvoiceID = '$prevInvoiceID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->result_array();
    	}
    }

    function getRecurringInvoices() {
        $sql = "SELECT * from cpo_invoice I
                LEFT JOIN cpo_account_profile A ON A.AccountID = I.Userid
                WHERE
                    I.Recurring IS NOT NULL AND
                    I.Recurring <> '' AND
                    A.IsActive = 1 AND
                    I.Deleted = 0";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getInvoicesForExport($accountUserid) {
        $sql = "SELECT
					I.InvoiceID,I.InvNo,I.Title,I.DateInvoice,I.InvTotal,
					I.Sent,I.DateEntry,I.Status,I.DateSent,I.Paid,I.DatePaid,
                    I.PaymentDueDate,I.PaymentDueDays,I.Recurring,
					C.ClientID,C.Company,
                    P.Title AS ProjectTitle,MC.MainCat
                FROM cpo_invoice I
				LEFT JOIN cpo_clients C ON C.ClientID = I.ClientID
                LEFT JOIN cpo_project P ON P.ProjectID = I.ProjectID
                LEFT JOIN cpo_main_cat MC ON MC.CatID = I.CatID
				WHERE
                    I.Userid = '$accountUserid' AND
                    I.Deleted = 0
                ORDER BY
                    I.DateInvoice DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    /*
     * getInvoiceCountForDateRange: gets a count of open and closed
     * invoices over a date range. To count, an invoice must be assigned
     * a due date within the date range. If no end date is assigned, use the
     * start date.
     */
    function getInvoiceCountForDateRange($dateStart,$dateEnd,$userid,$viewAll,$accountUserid) {
        if ($viewAll == TRUE) {
            $sqlPaid = "SELECT COUNT(I.InvoiceID) AS Paid
                        FROM cpo_invoice I
                        WHERE
                        I.Userid = '".$accountUserid."' AND
                        I.Deleted = 0 AND
                        I.Paid = 1 AND
                        (I.DatePaid >= '$dateStart' AND I.DatePaid <= '$dateEnd') OR
                        (I.PaymentDueDate >= '$dateStart' AND I.PaymentDueDate <= '$dateEnd')";
            $sqlUnpaid = "SELECT COUNT(I.InvoiceID) AS Unpaid
                        FROM cpo_invoice I
                        WHERE
                        I.Userid = '".$accountUserid."' AND
                        I.Deleted = 0 AND
                        I.Paid = 0 AND
                        I.PaymentDueDate >= '$dateStart' AND 
                        I.PaymentDueDate <= '$dateEnd'";
        } else {
            $sqlPaid = "SELECT COUNT(I.InvoiceID) AS Paid
                        FROM cpo_invoice I
                        WHERE
                        I.Userid = '".$accountUserid."' AND
                        ToUID = '$userid' AND
                        ToType = 'team' AND
                        I.Deleted = 0 AND
                        I.Paid = 1 AND
                        (I.DatePaid >= '$dateStart' AND I.DatePaid <= '$dateEnd') OR
                        (I.PaymentDueDate >= '$dateStart' AND I.PaymentDueDate <= '$dateEnd')";
            $sqlUnpaid = "SELECT COUNT(I.InvoiceID) AS Unpaid
                        FROM cpo_invoice I
                        WHERE
                        I.Userid = '".$accountUserid."' AND
                        ToUID = '$userid' AND
                        ToType = 'team' AND
                        I.Deleted = 0 AND
                        I.Paid = 0 AND
                        I.PaymentDueDate >= '$dateStart' AND 
                        I.PaymentDueDate <= '$dateEnd'";
        }
        $queryPaid = $this->db->query($sqlPaid);
        $resultPaidArray = $queryPaid->row_array();
        
        $queryUnpaid = $this->db->query($sqlUnpaid);
        $resultUnpaidArray = $queryUnpaid->row_array();
        
        $finalArray = array(
            'Paid' => $resultPaidArray['Paid'],
            'Unpaid' => $resultUnpaidArray['Unpaid']
        );
        return $finalArray;
    }
}    
?>