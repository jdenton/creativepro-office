<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Invoice_update extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
        $this->load->model('finances/finance_view','FinanceView',TRUE);
    }

	/**
	 * saveInvoice : inserts new invoice entries or updates existing entries
	 *
	 * @access	public
	 * @param	array $data PHP array of data to save
	 * @return	int   $invoiceID the record id of the inserted or updated entry
	 */
    function saveInvoice($data) {
        if ($data['action'] == 'add') {
            $prevInvoiceID = '';
            if (isset($data['prevInvoiceID'])) {
                $prevInvoiceID = $data['prevInvoiceID'];
            }
            $sql = "INSERT INTO cpo_invoice (
                        PrevInvoiceID,
                        Userid,
                        Estimate,
                        ToUID,
                        ToType,
                        InvNo,
                        ClientID,
                        ProjectID,
                        CatID,
                        Title,
                        InvComments,
                        IncComments,
                        InvSubTotal,
                        ShippingAmount,
                        DiscountAmount,
                        TaxAmount,
                        InvTotal,
                        PONumber,
                        TaxRate,
                        TaxName,
                        TaxID,
                        DiscountRate,
                        PaymentDueDays,
                        PaymentDueDate,
                        LateFeePercent,
                        LateFeeAmount,
                        Tags,
                        Recurring,
                        DateEstimate,
                        DateInvoice,
                        UseridEntry,
                        UseridUpdate,
                        DateEntry,
                        DateUpdate,
                        ImportGUID
                    ) values (
                        '$prevInvoiceID',
                        '".$data['accountUserid']."',
                        '".$data['estimate']."',    
                        '".$data['ToUID']."',
                        '".$data['ToType']."',
                        '".$data['invoiceNumber']."',
                        '".$data['clientID']."',
                        '".$data['projectID']."',
                        '".$data['invoiceCategory']."',
                        '".$data['invoiceTitle']."',
                        '".$data['invoiceMessage']."',
                        '".$data['printMessage']."',
                        '".$data['amountSubTotal']."',
                        '".$data['amountShipping']."',
                        '".$data['amountDiscount']."',
                        '".$data['amountTax']."',
                        '".$data['invoiceTotal']."',
                        '".$data['invoicePONumber']."',
                        '".$data['invoiceTaxRate']."',
                        '".$data['invoiceTaxName']."',
                        '".$data['invoiceTaxID']."',
                        '".$data['invoiceDiscount']."',
                        '".$data['invoicePaymentDue']."',
                        '".$data['invoicePaymentDueDate']."',
                        '".$data['invoiceLateFee']."',
                        '".$data['invoiceLateFeeOther']."',
                        '".$data['invoiceTags']."',
                        '".$data['invoiceRecurring']."',
                        '".$date['estimateDate']."',    
                        '".$data['invoiceDate']."',
                        '".$data['userid']."',
                        '".$data['userid']."',
                        '".date('Y-m-d G:i:s')."',
                        '".date('Y-m-d G:i:s')."',
                        '".$this->session->userdata('importGUID')."'
                    )";
            $query = $this->db->query($sql);
    		$invoiceID = $this->db->insert_id();
		} elseif ($data['action'] == 'edit') {
            $sql = "UPDATE cpo_invoice SET                        
                        ToUID          = '".$data['ToUID']."',
                        ToType         = '".$data['ToType']."',
                        InvNo          = '".$data['invoiceNumber']."',
                        ClientID       = '".$data['clientID']."',
                        ProjectID      = '".$data['projectID']."',
                        CatID          = '".$data['invoiceCategory']."',
                        Title          = '".$data['invoiceTitle']."',
                        InvComments    = '".$data['invoiceMessage']."',
                        IncComments    = '".$data['printMessage']."',
                        InvSubTotal    = '".$data['amountSubTotal']."',
                        ShippingAmount = '".$data['amountShipping']."',
                        DiscountAmount = '".$data['amountDiscount']."',
                        TaxAmount      = '".$data['amountTax']."',
                        InvTotal       = '".$data['invoiceTotal']."',
                        PONumber       = '".$data['invoicePONumber']."',
                        TaxRate        = '".$data['invoiceTaxRate']."',
                        TaxName        = '".$data['invoiceTaxName']."',
                        TaxID          = '".$data['invoiceTaxID']."',
                        DiscountRate   = '".$data['invoiceDiscount']."',
                        PaymentDueDays = '".$data['invoicePaymentDue']."',
                        PaymentDueDate = '".$data['invoicePaymentDueDate']."',
                        LateFeePercent = '".$data['invoiceLateFee']."',
                        LateFeeAmount  = '".$data['invoiceLateFeeOther']."',
                        Tags           = '".$data['invoiceTags']."',
                        Recurring      = '".$data['invoiceRecurring']."',
                        DateInvoice    = '".$data['invoiceDate']."',
                        UseridUpdate   = '".$data['userid']."',
                        DateUpdate     = '".date('Y-m-d G:i:s')."'
                    WHERE
                        InvoiceID = '".$data['invoiceID']."'";
            $query = $this->db->query($sql);
            $invoiceID = $data['invoiceID'];
            /*
             * Delete existing invoice items
             */
            Invoice_update::deleteInvoiceItems($invoiceID);
		}

        /*
         * Insert invoice items
         */
        if (count($data['items']>0)) {
            foreach($data['items'] as $item) {
                $sql = "INSERT INTO cpo_invoice_items (
                            InvoiceID,
                            ItemDesc,
                            Kind,
                            Qty,
                            Taxable,
                            CostPer,
                            ItemTotal,
                            TimeSheetID,
                            ExpenseID,
                            ImportGUID
                        ) values (
                            '".$invoiceID."',
                            '".$item['desc']."',
                            '".$item['type']."',
                            '".$item['qty']."',
                            '".$item['tax']."',
                            '".$item['rate']."',
                            '".$item['total']."',
                            '".$item['timesheetID']."',
                            '".$item['expenseID']."',
                            '".$this->session->userdata('importGUID')."'
                        )";
                $query = $this->db->query($sql);
            }
        }

		return $invoiceID;
	}

    function saveInvoiceItem($data) {
        /*
         * This method is here only for updating the invoice_items table from
         * a CPO 1.0 account.
         */
        $sql = "INSERT INTO cpo_invoice_items (
                    InvoiceID,
                    ItemDesc,
                    Kind,
                    Qty,
                    Taxable,
                    CostPer,
                    ItemTotal,
                    TimeSheetID,
                    ImportGUID
                ) values (
                    '".$data['invoiceID']."',
                    '".$data['desc']."',
                    '".$data['type']."',
                    '".$data['qty']."',
                    '".$data['tax']."',
                    '".$data['rate']."',
                    '".$data['total']."',
                    '".$data['timeSheetID']."',
                    '".$this->session->userdata('importGUID')."'
                )";
        $query = $this->db->query($sql);
    }

    function saveInvoicePayment($data) {
        $sql = "INSERT into cpo_invoice_payment (
                InvoiceID,
                PaymentType,
                AcctCheckNo,
                PaymentAmount,
                DatePayment,
                PaymentComment,
                DateUpdate,
                UseridUpdate,
                ImportGUID
                ) values (
                '".$data['invoiceID']."',
                '".$data['payInvoicePaymentType']."',
                '".$data['payInvoiceAccountNumber']."',
                '".$data['payInvoiceAmount']."',
                '".$data['payInvoiceDate']."',
                '".$data['payComments']."',
                '".date('Y-m-d')."',
                '".$data['userid']."',
                '".$this->session->userdata('importGUID')."'
                )";
        $query = $this->db->query($sql);
        $paymentID = $this->db->insert_id();

        Invoice_update::updateInvoiceStatus($data['invoiceID'],$data['invoiceStatus'],$data['payStatus'],$data['payInvoiceDate']);

        return $paymentID;
    }
    
    function updateInvoicePayment($data) {
        $sql = "UPDATE cpo_invoice_payment SET
                PaymentAmount = '".$data['payInvoiceAmount']."',
                DateUpdate = '".date('Y-m-d')."',
                UseridUpdate = '".$data['userid']."'
                WHERE
                PaymentID = ".$data['paymentID'];
        $query = $this->db->query($sql);
    }

    function deleteInvoicePayment($paymentID) {
        $sql = "DELETE from cpo_invoice_payment WHERE PaymentID = '$paymentID'";
        $query = $this->db->query($sql);
    }

    function updateInvoiceStatus($invoiceID,$invoiceStatus=NULL,$payStatus=NULL,$payDate=NULL) {
        if ($payDate == NULL) {
            $payDate = date('Y-m-d');
        }

        if ($invoiceStatus == 1 && $payStatus == 1) {
            $sql = "UPDATE cpo_invoice SET
                Paid = '$payStatus',
                Status = '$invoiceStatus',
                DatePaid = '".$payDate."'
                WHERE InvoiceID = '$invoiceID'
                ";
        } else {
            $sql = "UPDATE cpo_invoice SET
                    Paid = '$payStatus',
                    Status = '$invoiceStatus'
                    WHERE InvoiceID = '$invoiceID'
                    ";
        }
        $query = $this->db->query($sql);
        
        /*
         * If new status is paid, let's see if we have any payments
         * made for this invoice. If not, get the invoice total 
         * and put that in our payments table.
         */
        if ($payStatus == 1) {
            if (!$this->FinanceView->getInvoicePayments($invoiceID)) {
                $invoiceTotal = $this->FinanceView->getInvoiceTotal($invoiceID);
                $data['invoiceID'] = $invoiceID;
                $data['payInvoicePaymentType'] = '';
                $data['payInvoiceAccountNumber'] = '';
                $data['payInvoiceAmount'] = $invoiceTotal;
                $data['payInvoiceDate'] = $payDate;
                $data['payComments'] = '';
                Invoice_update::saveInvoicePayment($data);
            }
        }
    }

    function updateInvoiceSentStatus($invoiceID,$sendStatus=NULL,$dateSent=NULL) {
        if ($dateSent == NULL) {
            $dateSent = date('Y-m-d');
        }
        $sql = "UPDATE cpo_invoice SET
                Sent     = '$sendStatus',
                DateSent = '$dateSent'
                WHERE InvoiceID = '$invoiceID'
                ";
        $query = $this->db->query($sql);
    }

	/**
	 * deleteInvoice : delete an invoice
	 *
	 * @access	public
	 * @param	int  $invoiceID ID of invoice we wish to delete.
     * @param   bool $tagForDelete If TRUE, just tag this item for deletion.
     *              If FALSE, completely delete the item.
	 * @return	void
	 */
    function deleteInvoice($invoiceID,$tagForDelete=TRUE,$userid) {
         if ($tagForDelete == TRUE) {
             $sql = "UPDATE cpo_invoice SET UseridUpdate = '$userid', Deleted = 1 WHERE InvoiceID = '$invoiceID'";
             $query = $this->db->query($sql);
         } else {
             $sql = "DELETE from cpo_invoice WHERE InvoiceID = '$invoiceID'";
             $query = $this->db->query($sql);

			 $sql = "DELETE from cpo_invoice_items WHERE InvoiceID = '$invoiceID'";
			 $query = $this->db->query($sql);
         }
    }

    function deleteInvoiceItems($invoiceID) {
        $sql = "DELETE FROM cpo_invoice_items WHERE InvoiceID = '$invoiceID'";
        $query = $this->db->query($sql);
    }

    function restoreInvoice($invoiceID,$userid) {
        $sql = "UPDATE cpo_invoice SET UseridUpdate = '$userid', Deleted = 0 WHERE InvoiceID = '$invoiceID'";
        $query = $this->db->query($sql);
    }

    function stopRecurringInvoice($invoiceID) {
        $sql = "UPDATE cpo_invoice SET Recurring = '' WHERE InvoiceID = '$invoiceID'";
        $query = $this->db->query($sql);
    }
    
    function convertEstimateToInvoice($invoiceID) {
        /*
         * Calculate new payment due date.
         */
        $sql = "SELECT PaymentDueDays from cpo_invoice WHERE InvoiceID = '$invoiceID'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $paymentDueDays = $row['PaymentDueDays'];
        
        if ($paymentDueDays > 0) {
            $paymentDueDate = new_date_from_days(date('Y-m-d'),$paymentDueDays);
        }
        
        $sql = "UPDATE cpo_invoice SET 
                    Estimate = '0',
                    Sent = '0',
                    DateSent = '0000-00-00'";
        
        if (!empty($paymentDueDate)) {
            $sql .= ", PaymentDueDate = '".$paymentDueDate."' ";
        }
        
        $sql .= "WHERE InvoiceID = '$invoiceID'";
        $query = $this->db->query($sql);
    }
}
?>