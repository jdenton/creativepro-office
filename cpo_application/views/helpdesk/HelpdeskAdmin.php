<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="viewPosts">
	<div class="windowFrame" style="min-height: 80px;">
        
        <div id="helpdeskListContainer">
            <table class="dataTable" id="helpdeskPostGrid">
            <thead><tr>
                <th style="width: 50px;"></th>
                <th>Title</th>
                <th style="text-align: center;">Status</th>
                <th style="text-align: right;">Views</th>
                <th style="text-align: right;">Comments</th>
            </tr></thead>
            <tbody>
            <? foreach($helpdeskPosts as $post) { ?>
            <tr class="postRow" id="helpdeskPostRow<?=$post['BEID']; ?>" itemID="<?=$post['BEID']; ?>">
                <td class="controls"></td>
                <td><a href="<?=BASE_URL; ?>helpdesk/entry/<?=$post['EntryURL']; ?>"><?=stripslashes($post['Title']); ?></a></td>
                <td><?=convertMySQLToGMT($post['DateEntry'],'pull','M j, Y'); ?></td>
                <td style="text-align: center;">
                    <? if ($post['Active'] == 1) { ?>
                    <a class="postStatus status_completed active" style="cursor: pointer;" title="Update status"></a>
                    <? } else { ?>
                    <a class="postStatus status_not_started inactive" style="cursor: pointer;" title="Update status"></a>
                    <? } ?>
                </td>
                <td style="text-align: right;"><?=$post['Views']; ?></td>
                <td style="text-align: right;"><?=$post['commentCount']; ?></td>
            </tr>            
            <? } ?>
            </tbody>
            </table>
        </div>
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>