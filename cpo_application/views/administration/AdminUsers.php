<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn">
    <input type="hidden" name="userAccountID" id="userAccountID" />
    <input type="hidden" name="userUserid" id="userUserid" />
    <input type="hidden" name="userIsActive" id="userIsActive" />
    
	<div class="boxYellow" style="margin-bottom: 10px;">
		<div class="boxYellowInner">
			<p>
			<label>Search  Users:</label>	
			<input type="text" style="width: 300px;" class="search clearOnFocus" id="sUserAutocompleter" />
			<div class="loadingInline"></div>
			<input type="hidden" id="sUserID" />
			</p>
		</div>	
	</div>
	<div class="windowFrame" style="min-height: 80px;" id="userInformationContainer">
		<div id="loadingIndicator"></div>
		<div id="userInformationInner" style="display: none;">
            <button class="buttonExpand blueGray" id="buttonLogin" tabindex="13"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator login">Login as this user</span></span></button>

            <button class="buttonExpand green hide buttonStatus" id="buttonActive" title="Click to deactivate this user" tabindex="13"><span class="buttonOuterSpan green"><span class="buttonDecorator ok">Active</span></span></button>
            <button class="buttonExpand red hide buttonStatus" id="buttonInactive" title="Click to activate this user" tabindex="13"><span class="buttonOuterSpan red"><span class="buttonDecorator delete">Inactive</span></span></button>

            <div id="changePassword" style="margin-left: 90px; float: left;">
                <input type="text" style="width: 200px; float: left;" id="textChangePassword" value="Change password..." />
                <div style="float: left;">
                    <button class="buttonExpand blueGray" id="buttonChangePassword"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator login">Change Password</span></span></button>
                </div>
            </div>
            <div style="clear: left; height: 10px;"></div>

			<div style="width: 40%; padding: 9px; float: left;" id="userInformation"><p></p></div>
			<div style="width: 50%; padding: 9px; float: left;">                
				<div id="userStats"></div>
			</div>
			<div style="clear: left;"></div>
		</div>	
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>