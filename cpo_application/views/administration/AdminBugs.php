<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn">
    <div id="tabsBugs">
		<ul class="ui-tabs-nav noPrint">
	        <li class="ui-tabs-selected" id="tabBugsOpen"><a href="#windowBugsOpen"><span>Open Bugs</span> <span id="bugCountOpen">(0)</span></a></li>
	        <li><a href="#windowBugsClosed" id="tabBugsClosed"><span>Bugs Closed</span> <span id="bugCountClosed">(0)</span></a></li>
	    </ul>
	    <div style="margin-top: -1px; min-height: 200px;" class="windowFrame">
            <div id="windowBugsOpen">
                <div id="bugListContainerOpen"></div>
            </div>
            <div id="windowBugsClosed" class="ui-tabs-hide">
                <div id="bugListContainerClosed"></div>
            </div>
        </div>
    </div>    
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
