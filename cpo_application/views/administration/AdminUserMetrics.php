<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn">
	<div class="boxYellow" style="margin-bottom: 10px;">
		<div class="boxYellowInner">
			<p>
            <h2>Total users: <?=$cumulativeStats['TotalUsers']; ?></h2>
            <h2>Total account value: $<?=$cumulativeStats['TotalAccountValue']; ?>/month from <?=$cumulativeStats['TotalPayingUsers']; ?> users (<?=$payingPercentage; ?>%).</h2>
			</p>
		</div>	
	</div>
	
    <div id="chartContainerSignup" class="windowFrame"></div>
    <div style="height: 24px;"></div>
    <div id="chartContainerPieAccounts" style="float: left; width: 45%;"></div>
    <div id="chartContainerPiePrice" style="float: left; width: 45%;"></div>
    <div style="clear: left;"></div>
    <div>
        <p>Future Metrics: (have up/down arrows, red and green. Green is good, red is bad.)</p>

        <p>1. Number of active accounts. (more is better)</p>
        <p>2. Number and percent of paying accounts. (more is better)</p>
        <p>3. Account value per month. (more is better)</p>
        <p>4. Average account revenue. (#3 / #2) (more is better)</p>
        <p>5. Number of user required to reach $10k / month. (less is better)</p>
    </div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>