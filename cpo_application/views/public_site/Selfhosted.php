<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <h1 class="mBottom12">Host CreativePro Office on your own server</h1>

    <p class="mBottom12" style="padding-right: 12px;">
        Now you can use CreativePro Office in the security of your own intranet or the hosting platform
        of your choice.  Our self-hosted option gives you the complete open source code, a perpetual license,
        1 year of support and updates, and complete peace of mind for one low price.
    </p>

    <div style="clear: both;" class="boxGrey">
        <p>Benefits of the self-hosted option include ...</p>
    </div>
    <div class="contentSpacer"></div>
    <ul class="noBullet">
        <li class="iconSecurity"><strong>Security.</strong> CreativePro Office runs on your own server. You decide how to handle data security and backups.</li>
        <li class="iconTools"><strong>Customization.</strong> You can modify the source code to add or change features.</li>
        <li class="iconCloud"><strong>No limits.</strong> There are no limits on the number of users, projects, or file storage.</li>
        <li class="iconNoMoneyBig"><strong>No monthly fees.</strong>  You pay once and you can use the application forever.</li>
        <li class="iconUpdate"><strong>Free updates.</strong>  Download free code updates and patches for 1 year. Update subscriptions
            cost $199.00 per year after your first free year.</li>
    </ul>

    <h2 class="mTop12 mBottom12">Server requirements</h2>
    <p>You will need a server environment with the following:
    <ul class="check" style="margin: 6px 0 0 36px;">
        <li>A Linux operating system (we currently do not support Windows installations).</li>
        <li>The Apache web server or something similar.</li>
        <li>PHP 5.2 or greater.</li>
        <li>MySQL 4 database or greater.</li>
        <li>At least 30MB of disk space.</li>
        <li>The ability to modify your .htaccess files.</li>
        <li>The ability to execute CRON jobs on your server if you wish to take advantage of things
            like recurring invoices, auto-task reminders, etc.</li>
        <li>The ability to set up a temporary FTP account should you require our assistance in
            getting your installation running.</li>
    </ul>

    <h2 class="mTop12 mBottom12">A note to current CreativePro Office subscribers!</h2>
    <p>The current version of the self-hosted code <u>DOES NOT</u> allow you to import data from
        your hosted CreativePro Office account. We are currently working on an import feature that
        will allow you to import data from a CreativePro Office hosted account into your
        self-hosted installation.  This should be available as an update in late June, 2011.</p>

    <h2 class="mTop12">More questions?</h2>
    <p><a href="selfhosted/faq">See the self-hosted FAQ page here.</a></p>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>
