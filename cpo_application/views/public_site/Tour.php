<? $this->load->view('includes/headerPublic'); ?>
<div>
    <div id="toolbar">
        <a href="#dashboard" class="nav dashboard">Dashboard</a>
        <a href="#projects" class="nav projects">Projects</a>
        <a href="#tasks" class="nav tasks">Tasks</a>
        <a href="#timesheets" class="nav timeclock">Timesheets</a>
        <a href="#invoices" class="nav invoices">Invoices</a>
        <a href="#calendar" class="nav calendar">Calendar</a>
        <a href="#files" class="nav fileshare">Files</a>
        <a href="#apps" class="nav apps">Apps</a>
        <a href="#reports" class="nav reports">Reports</a>
        <a href="/pricing" class="buttonSignupNow tour" title="Sign up now for FREE! No credit card required."></a>

    </div>    
    
    <!-- DASHBOARD -->
    <div class="tourContainer" id="dashboard">
        <div class="content">
            <h2>Office Dashboard</h2>
            <p>Stay informed at a glance with dashboard widgets that show your calendar, open invoices, tasks,
                current projects and much more.</p>
        </div>    
        <div class="flexslider imageContainer">
            <ul class="slides">
                <li>
                    <div class="tourDashboard1">
                        <p class="flex-caption">The office dashboard consists of widgets that you can add, remove, or reposition as you need.</p>
                    </div>
                </li>
                <li>
                    <div class="tourDashboard2">
                        <p class="flex-caption">Selecting dashboard widgets to display.</p>
                    </div>
                </li>
                <li>
                    <div class="tourDashboard3">
                        <p class="flex-caption">The calendar, task, time tracker widgets in edit mode.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div style="clear: both;"></div>
    </div>
    
    <div class="tourContainer" id="projects">
        <div class="content">
            <h2>Project Manager</h2>
            <p>Critical project data like status, tasks, hours spent, amounts invoiced, project contacts, notes, files, and messages
                are all presented in one tabbed view.</p>
            <p>When you're done with a project, archive it with one click to remove it from your workflow.</p>
        </div> 
        <div class="flexslider imageContainer">
            <ul class="slides">
                <li>
                    <div class="tourProjects1">
                        <p class="flex-caption">Sort by project name, client, or amount invoiced in the list view. The auto-suggest search box helps locate projects quickly.</p>
                    </div>
                </li>
                <li>
                    <div class="tourProjects2">
                        <p class="flex-caption">Project detail view showing assigned team members, messages, files, tasks, notes, and invoices.</p>
                    </div>
                </li>
                <li>
                    <div class="tourProjects3">
                        <p class="flex-caption">Project notes that you can share with clients or just with your team members.</p>
                    </div>
                </li>
                <li>
                    <div class="tourProjects4">
                        <p class="flex-caption">Send messages about the project to team members and your clients.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div style="clear: both;"></div>
    </div>
    
    <div class="tourContainer" id="tasks">
        <div class="content">
            <h2>Task Manager</h2>
            <p>Tasks, milestones, simple to-do lists...CreativePro Office supports them all. Assign tasks to
                multiple team members, attach files to tasks, create comment threads, and track time on tasks.
            </p>
            <p>Easily filter tasks and view only incomplete tasks, completed tasks, or tasks assigned to you. A
            little report in your task list view shows you how much work is left to do.</p>

            <p>You can also assign multiple team members to tasks, subsctibe to a task RSS feed, export task
            lists to Excel, and create nice PDF printouts.</p>
        </div> 
        <div class="flexslider imageContainer">
            <ul class="slides">
                <li>
                    <div class="tourTasks1">
                        <p class="flex-caption">Tasks, milestones, simple to-do lists...CreativePro Office supports them all. Assign tasks to multiple team members, attach files to tasks, create comment threads, and track time on tasks.</p>
                    </div>
                </li>
                <li>
                    <div class="tourTasks2">
                        <p class="flex-caption">In-place editing using the WYSIWYG editor to include links or lists. Update who's assigned to a task and attached files.</p>
                    </div>
                </li>
                <li>
                    <div class="tourTasks4">
                        <p class="flex-caption">The task widget displays upcoming and overdue tasks.</p>
                    </div>
                </li>
                <li>
                    <div class="tourTasks3">
                        <p class="flex-caption">Create easy to read task lists in PDF format or export tasks to Excel.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div style="clear: both;"></div>
    </div>
    
    <div class="tourContainer" id="timesheets">
        <div class="content">
            <h2>Timesheets &amp; Job Timer</h2>
            <p>Never lose another hour of billable work! The time clock widget is always 
                close by while timesheets give you a daily view of time spent on specific 
                projects and tasks.</p>
        </div> 
        <div class="flexslider imageContainer">
            <ul class="slides">
                <li>
                    <div class="tourTimesheets1">
                        <p class="flex-caption">Adding a new timesheet entry. Tabs at the top allow a quick view of the entire week. Or use the date field on the right to jump to a particular week.</p>
                    </div>
                </li>
                <li>
                    <div class="tourTimesheets2">
                        <p class="flex-caption">The daily hours chart gives you a quick view of billable hours for the week.</p>
                    </div>
                </li>
                <li>
                    <div class="tourTimesheets3">
                        <p class="flex-caption">You also get a grid view with totals for each day and task.</p>
                    </div>
                </li>
                <li>
                    <div class="tourTimesheets4">
                        <p class="flex-caption">Produce timesheet charts by client, project, task, date range, and team member.</p>
                    </div>
                </li>
                <li>
                    <div class="tourTimesheets5">
                        <p class="flex-caption">The job timer is available on every page and keeps track of your hours in real time.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div style="clear: both;"></div>
    </div>
    
    <div class="tourContainer" id="invoices">
        <div class="content">
            <h2>Invoices</h2>
            <p>Quickly create professional looking invoices for your clients and send them via email.
               Create recurring invoices and invoices directly from timesheet entries.
            </p>
            <p>Your clients can view their invoices securely without having to first login to their
            CreativePro Office account. If you have a PayPal account, your clients can also
            pay you while viewing their online invoice.
            </p>
            <p>Or download your invoices as a PDF file and print professional looking paper invoices
                complete with your company logo.</p>
        </div> 
        <div class="flexslider imageContainer">
            <ul class="slides">
                <li>
                    <div class="tourInvoices1">
                        <p class="flex-caption">Quickly create professional looking invoices for your clients and send them via email. Create recurring invoices and invoices directly from timesheet entries.</p>
                    </div>
                </li>
                <li>
                    <div class="tourInvoices2">
                        <p class="flex-caption">The auto-suggest invoice search feature will search invoice title, tag, client, and project.</p>
                    </div>
                </li>
                <li>
                    <div class="tourInvoices3">
                        <p class="flex-caption">Download your invoices as a PDF file and print professional looking paper invoices complete with your company logo.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div style="clear: both;"></div>
    </div>
    
    <div class="tourContainer" id="calendar">
        <div class="content">
            <h2>Calendar</h2>
            <p>Share multiple calendars with your team or clients. Export calendars in 
                iCal format, RSS, or pull them into your Google calendar account.</p>
        </div> 
        <div class="flexslider imageContainer">
            <ul class="slides">
                <li>
                    <div class="tourCalendar1">
                        <p class="flex-caption">Project start &amp; end dates, task dates, and personal entries. Drag events to change dates.</p>
                    </div>
                </li>
                <li>
                    <div class="tourCalendar2">
                        <p class="flex-caption">The calendar supports week and daily agenda views. Drag across cells to create or edit events.</p>
                    </div>
                </li>
                <li>
                    <div class="tourCalendar3">
                        <p class="flex-caption">Adding a new event.</p>
                    </div>
                </li>
                <li>
                    <div class="tourCalendar4">
                        <p class="flex-caption">You can create your own custom, color-coded calendars.</p>
                    </div>
                </li>
                <li>
                    <div class="tourCalendar5">
                        <p class="flex-caption">Export your calendars to PDF for printing or emailing.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div style="clear: both;"></div>
    </div>
    
    <div class="tourContainer" id="files">
        <div class="content">
            <h2>File Manager</h2>
            <p>Share files with clients and team members. You can attach files to projects, 
                tasks, invoices, expenses, or messages.</p>
        </div> 
        <div class="flexslider imageContainer">
            <ul class="slides">
                <li>
                    <div class="tourFiles1">
                        <p class="flex-caption">The main file manager view. Drag &amp; drop files into folders and folders into other folders.</p>
                    </div>
                </li>
                <li>
                    <div class="tourFiles2">
                        <p class="flex-caption">Drag &amp; drop multiple files from your desktop to the file uploader.</p>
                    </div>
                </li>
                <li>
                    <div class="tourFiles3">
                        <p class="flex-caption">View image files right in the application without downloading first.</p>
                    </div>
                </li>
                <li>
                    <div class="tourFiles4">
                        <p class="flex-caption">The file manager widget is available in all screens so you can attach files to projects, tasks, invoices, or messages.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div style="clear: both;"></div>
    </div>
    
    <div class="tourContainer" id="apps">
        <div class="content">
            <h2>Mobile and Desktop Apps</h2>
            <p>Don't want to be tied to your browser? Track your time and manage your tasks from 
                wherever you are right on your mobile iOS device.</p>
            <p>The CreativePro Office desktop timer for Mac and Windows allows you to quickly 
                enter time for your projects and tasks throughout the day without ever 
                logging in to the web app.</p>
            <p>These apps are all free for all CPO users.</p>
        </div> 
        <div class="flexslider imageContainer">
            <ul class="slides">
                <li>
                    <div class="tourApps1">
                        <p class="flex-caption">Time tracker and task manager in the iPhone app.</p>
                    </div>
                </li>
                <li>
                    <div class="tourApps2">
                        <p class="flex-caption">Keep track of time for multiple projects or tasks with the desktop timer.</p>
                    </div>
                </li>
                <li>
                    <div class="tourApps3">
                        <p class="flex-caption">Adding a new timer to the desktop app.</p>
                    </div>
                </li>
                <li>
                    <div class="tourApps4">
                        <p class="flex-caption">Jump to a particular date to see time logged for that day.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div style="clear: both;"></div>
    </div>
    
    <div class="tourContainer" id="reports">
        <div class="content">
            <h2>Reports</h2>
            <p>See what's happening over time with finance, task, time-sheet, and team member productivity reports.</p>
        </div> 
        <div class="flexslider imageContainer">
            <ul class="slides">
                <li>
                    <div class="tourReports1">
                        <p class="flex-caption">See what your cash flow is doing month to month and year to year.</p>
                    </div>
                </li>
                <li>
                    <div class="tourReports2">
                        <p class="flex-caption">Cash flow totals for the year.</p>
                    </div>
                </li>
                <li>
                    <div class="tourReports3">
                        <p class="flex-caption">Who are your most valuable clients?</p>
                    </div>
                </li>
                <li>
                    <div class="tourReports4">
                        <p class="flex-caption">View your data using several report formats to get a better picture of your business.</p>
                    </div>
                </li>
                <li>
                    <div class="tourReports5">
                        <p class="flex-caption">Export your reports in PDF format for printing or presentations.</p>
                    </div>
                </li>
            </ul>
        </div>
        <div style="clear: both;"></div>
    </div>
    
    

    <!-- CLIENT AREA -->
    <!--
    <div class="tourHeader mTop18 mBottom18" id="client"><h2>Client manager</h2></div>
    <a href="/pricing" class="buttonSignupNow" title="Sign up now for FREE! No credit card required." style="float: right; margin-top: 15px;"></a>

    <div class="tourLeft">
        <a href="images/screenshots/bigClientList.png" rel="client" class="tourPic tourLarge" title="The client list view shows you all of your clients. You can sort by client name, number or projects, or amount invoiced. The auto-suggest search box helps locate a client quickly.">
            <img src="images/screenshots/medClientList.png" class="tourMedium" title="Client list" border="0" />
        </a>
    </div>
    <div class="tourRight">        
        <div class="tourTextArea">
            <p>See all of your client information like projects, amounts invoiced, messages, and client contacts in a
                convenient tabbed view.</p>
            <p>Your clients have their own account area as well letting them see project status,
                send messages, and pay your invoices through PayPal if you choose.
            </p>
        </div>
        <a href="images/screenshots/bigClientDetail.png" rel="client" class="tourThumb left tourPic" title="Client detail view showing client contacts, invoices, projects, and messages.">
            <img src="images/screenshots/thumbClientDetail.png" class="tourThumb left" title="Client detail" border="0" />
        </a>
        <a href="images/screenshots/bigClientInvoices.png" rel="client" class="tourThumb tourPic" title="The client invoice list view.">
            <img src="images/screenshots/thumbClientInvoices.png" class="tourThumb" title="Client invoices" border="0" />
        </a>
        <div style="clear: left;"></div>
    </div>
    <div style="clear: both;"></div>
    -->
    
</div>
<? $this->load->view('includes/footerPublic'); ?>