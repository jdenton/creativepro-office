<? $this->load->view('includes/headerPublic'); ?>
<div id="loginBox" class="leftPane" pageID="accountLogin">
	<div class="windowFrame" style="margin-bottom: 10px;">
		<div id="messageContainer" class="boxYellow" style="padding: 12px;">
			<?=$message; ?>
		</div>
        <div class="hSpacer_12"></div>
        <div style="width: 660px;">
            <div style="float: left; width: 320px;">
		<form id="formLogin" class="noBorder" action="<?=site_url('login/tryLogin'); ?>" method="POST">
                <div style="margin-left: 9px;">
                    <p>
                    <label>Email / User Name</label>
                    <input class="bigText" type="text" style="width: 250px;" id="userid" name="userid" />
                    </p>
                    <p class="mTop12">
                    <label>Password</label>
                    <input class="bigText" type="password" style="width: 250px;" id="password" name="password" />
                    </p>
                    <p style="margin: 6px 0 6px 0;">
                    <input type="checkbox" id="remember" name="remember" value="1" />
                    <label class="inline" for="remember">&nbsp;Remember me for 30 days</label>
                    </p>
                </div>
				<div style="margin: 9px 0 0 6px;">
                    <button class="buttonExpand action" id="buttonLogin" tabindex="9" style="margin-right: 24px;"><span class="login">Login</span></button>
                    <a href="#" id="linkFindPassword" title="Find my password">I forgot my password.</a>
                </div>
			</form>           

			<form id="formFindPassword" action="<?=site_url('login/findPassword'); ?>" method="POST" style="display: none;">
                <div style="margin-left: 9px;">
                    <p>
                    <label style="width: 120px;">Email address</label>
                    <input class="bigText" type="text" style="width: 250px;" id="email" name="email" />
                    </p>
                </div>
				<div style="margin: 9px 0 0 6px;">
                    <button class="buttonExpand action" id="buttonFindPassword" style="margin-right: 24px;"><span class="search">Find my password</span></button>
                    <a href="#" id="buttonCancelFindPassword" style="float: left;">Cancel</a>
                </div>
            <p style="height: 20px;"></p>
			</form>

            <div style="margin: 48px 0 0 9px; clear: both;">
                <p style="margin-bottom: 6px;">CreativePro Office works best with</p>
                <span class="browserIcons firefox">3</span>
                <span class="browserIcons ie">8+</span>
                <span class="browserIcons chrome">10+</span>
                <span class="browserIcons safari">4+</span>
                <span class="browserIcons opera">10+</span>
            </div>
		</div>
        
		<div style="float: right; margin-top: 5px;">
            <div class="whiteBoxTop_300"></div>
                <div class="whiteBoxContentContainter_300">
                    <? $this->load->view('includes/rightColumnComponentsPublic/createNewAccount'); ?>
                </div>
            <div class="whiteBoxBottom_300"></div>
		</div>	
		<div style="clear: both;"></div>
        </div>
	</div>
</div>
<div class="rightPane">
</div>
<? $this->load->view('includes/footerPublic'); ?>