<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <h1 class="mBottom12">Approve your purchase.</h1>
    <p class="mBottom12">Please make sure that the following information is correct and then click the
    <strong>Confirm Payment</strong> button below.</p>

    <h2 class="blue mBottom12">Product Information</h2>
    <table>
        <tr>
            <td style="width: 200px;"><strong>Product name</strong></td>
            <td><?=$formData['description']; ?></td>
        </tr>
        <tr>
            <td style="width: 200px;"><strong>Price</strong></td>
            <td>$<?=$formData['amount']; ?></td>
        </tr>
    </table>

    <h2 class="blue mBottom12 mTop12">Company Information</h2>
    <table>
        <tr>
            <td style="width: 200px;"><strong>Company name</strong></td>
            <td><?=$formData['company']; ?></td>
        </tr>
        <tr>
            <td style="width: 200px;"><strong>Company website</strong></td>
            <td><?=$formData['url']; ?></td>
        </tr>
        <tr>
            <td style="width: 200px;"><strong>Company email address</strong></td>
            <td><?=$formData['email']; ?></td>
        </tr>
    </table>

    <h2 class="blue mBottom12 mTop12">Billing Information</h2>
    <table>
        <tr>
            <td style="width: 200px;"><strong>Full name</strong></td>
            <td><?=$formData['firstName'].' '.$formData['lastName']; ?></td>
        </tr>
        <tr>
            <td style="width: 200px;"><strong>Address</strong></td>
            <td>
                <?
                echo $formData['address'].'<br />';
                echo $formData['city'].' '.$formData['stateRegion'].' '.$formData['zip'].'<br />';
                echo $formData['country'];
                ?>
            </td>
        </tr>
        <tr>
            <td style="width: 200px;"><strong>Currency</strong></td>
            <td><?=$formData['currency']; ?></td>
        </tr>
    </table>

    <h2 class="blue mBottom12 mTop12">Credit Card Information</h2>
    <table>
        <tr>
            <td style="width: 200px;"><strong>Credit card type</strong></td>
            <td><?=$formData['cardtype']; ?></td>
        </tr>
        <tr>
            <td style="width: 200px;"><strong>Card number</strong></td>
            <td><?=$formData['ccNoSecure']; ?></td>
        </tr>
        <tr>
            <td style="width: 200px;"><strong>Expiration date</strong></td>
            <td><?=$formData['expmonth'].'/'.$formData['expyear']; ?></td>
        </tr>
    </table>

    <p class="mTop12 mBottom12"><strong>I agree to the CreativePro Office
    <a href="<?=BASE_URL ;?>refund/popup" class="popup">Refund Policy</a>,
    <a href="<?=BASE_URL ;?>terms/popup" class="popup">Terms of Service</a>, and
    <a href="<?=BASE_URL ;?>privacy/popup" class="popup">Privacy Policy</a>.</strong></p>

    <div style="height: 12px;"></div>
    <form action="<?=BASE_URL; ?>payment/selfHostedSendPayment/approved" method="POST">
        <?
        foreach ($_POST as $key => $value) {
            echo '<input type="hidden" id="'.$key.'" name="'.$key.'" value="'.$value.'" />'.Chr(10);
        }
        ?>
        <input type="submit" value="" id="confirmPayment" style="float: left; margin-top: 12px;" class="buttonConfirmPayment" />
        <div style="float: left; margin: 24px 0 0 24px;"><strong><a href="<?=INSECURE_URL; ?>selfhosted/paymentCancel">Cancel Payment</a></strong></div>
        <div style="clear: left;"></div>
    </form>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>