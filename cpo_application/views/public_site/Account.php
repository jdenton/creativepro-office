<? $this->load->view('includes/headerSecureSite'); ?>
<div class="leftPane">
    <div style="width: 615px; float: left;">
        <div class="whiteBoxTop_615"></div>
        <div class="whiteBoxContentContainter_615" style="padding-left: 5px;">
            <? if ($currentAccountLevel == 0) { ?>
            <div style="margin: 0 0 12px 150px;"><h1>You currently have the FREE plan.</h1>Please select from a premium plan below.</strong></div>
            <? } ?>
            <table class="pricing">
                <tr>
                    <th class="pricingItem"></th>
                    <th class="<?=$class2; ?>">
                        <h2>SOLO</h2>
                        <sup>$</sup><span class="biggerText white"><?=$currentPrices[1]; ?></span> monthly</th>
                    <th class="<?=$class3; ?>">
                        <h2>SHOP</h2>
                        <sup>$</sup><span class="biggerText white"><?=$currentPrices[2]; ?></span> monthly</th>
                    <th class="<?=$class4; ?>" style="border-right: none;">
                        <h2>TEAM</h2>
                        <sup>$</sup><span class="biggerText white"><?=$currentPrices[3]; ?></span> monthly</th>
                </tr>
                <tr>
                    <td class="pricingItem">Active projects</td>
                    <td class="<?=$class2; ?>"><span class="bigText white">10</span></td>
                    <td class="<?=$class3; ?>"><span class="bigText white">50</span></td>
                    <td class="<?=$class4; ?>" style="border-right: none;"><span class="bigText white">Unlimited</span></td>
                </tr>
                <tr>
                    <td class="pricingItem">Storage</td>
                    <td class="<?=$class2; ?>"><span class="bigText white">1 GB</span></td>
                    <td class="<?=$class3; ?>"><span class="bigText white">5 GB</span></td>
                    <td class="<?=$class4; ?>" style="border-right: none;"><span class="bigText white">10 GB</span></td>
                </tr>
                <tr>
                    <td class="pricingItem">Team members</td>
                    <td class="<?=$class2; ?>"><span class="bigText white">3</span></td>
                    <td class="<?=$class3; ?>"><span class="bigText white">10</span></td>
                    <td class="<?=$class4; ?>" style="border-right: none;"><span class="bigText white">Unlimited</span></td>
                </tr>
                <tr>
                    <td class="pricingItem">Support</td>
                    <td class="<?=$class2; ?>"><strong>Priority email</strong></td>
                    <td class="<?=$class3; ?>"><strong>Priority email</strong></td>
                    <td class="<?=$class4; ?>" style="border-right: none;"><strong>Priority email</strong></td>
                </tr>
                <tr style="height: 55px;">
                    <td class="pricingItem" style="border: none; background: none;"></td>
                    
                    <? if ($buttonState2 == 'none') { ?>
                        <td class="green"><h2 class="white" style="margin: 0 0 0 30px;">Your plan</h2></td>
                    <? } else { ?>
                        <td class="black"><a href="<?=SECURE_URL; ?>signup/changeAccount/solo" class="orangeButton" style="margin: 10px 0 0 34px;"><span>Select</span></a></td>
                    <? } ?>

                    <? if ($buttonState3 == 'none') { ?>
                        <td class="green"><h2 class="white" style="margin: 0 0 0 30px;">Your plan</h2></td>
                    <? } else { ?>
                        <td class="black"><a href="/signup/changeAccount/shop" class="orangeButton" style="margin: 10px 0 0 34px;"><span>Select</span></a></td>
                    <? } ?>

                    <? if ($buttonState4 == 'none') { ?>
                        <td class="green"><h2 class="white" style="margin: 0 0 0 30px;">Your plan</h2></td>
                    <? } else { ?>
                        <td class="black"><a href="/signup/changeAccount/team" class="orangeButton" style="margin: 10px 0 0 34px;"><span>Select</span></a></td>
                    <? } ?>
                </tr>
                <tr>
                    <td style="border: none;"></td>
                    <td colspan="3" class="footer">
                        <? $this->load->view('includes/plansInclude'); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="whiteBoxBottom_615"></div>
    </div>
    <div style="clear: left; height: 24px;"></div>
</div>
<div class="rightPane">
</div>
<? $this->load->view('includes/footerSecureSite'); ?>