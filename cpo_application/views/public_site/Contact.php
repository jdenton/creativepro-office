<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <? if($mailSent == true) { ?>
    <div class="boxYellow" style="width: 500px; padding: 6px; margin-bottom: 24px;">
            <h2 style="margin-bottom: 0px;">Thank you for contacting CreativePro Office!</h2>
            <p>We will get back to you with answers as soon as we can.</p>
    </div>
    <? } ?>
    <form action="<?=BASE_URL; ?>contact/sendMessage" method="post">
        <div class="messageBlock mBottom">
            <label style="margin-bottom: 6px;" for="contactProblemType">What are you writing about?</label>
            <select id="contactProblemType" tabindex="1" class="bigField" style="width: 508px;" name="contactProblemType">
                <option value="No subject">Please select one...</option>
                <option value="I have a question before I sign up">I have a question before I sign up</option>
                <option value="I want to request a feature">I want to request a feature</option>
                <option value="I have a billing question">I have a billing question</option>
                <option value="I am confused on how something works">I am confused on how something works</option>
                <option value="I think something is broken">I think something is broken</option>
                <option value="CreativePro Office didn't work out for me">CreativePro Office didn't work out for me</option>
                <option value="Other...">Other...</option>
            </select>
        </div>
        <div class="messageBlock mBottom">
            <label style="margin-bottom: 6px;" for="contactProblemDescription">Describe the problem, request, or question.</label>
            <textarea id="contactProblemDescription" tabindex="2" class="bigField" style="height: 150px; width: 500px;" name="contactProblemDescription"></textarea>
            <span class="instText"></span>
        </div>
        <div class="messageBlock mBottom">
            <label style="margin-bottom: 6px;" for="contactEmail">Your email address.</label>
            <input id="contactEmail" tabindex="3" class="bigField" type="text" style="width: 500px;" name="contactEmail"/>
            <span class="instText"></span>
        </div>
        <!--<div class="messageBlock mBottom"
             <label style="margin-bottom: 6px;" for="contactAccountUrl">What is you account URL?</label>
            <input id="contactAccountUrl" tabindex="4" class="bigField" type="text" style="width: 400px;" name="contactAccountUrl"/>
            <span class="instText">For example: <em>http://mysite.<?=SITE_DOMAIN;?></em></span>
            <span class="instText">If you don't have one or you don't know what this means, leave it blank.</span>
        </div>
        -->
        <div style="margin-left: -2px;">
            <button tabindex="5" class="buttonExpand blueGray" id="buttonSave" tabindex="13"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator email">Send Message</span></span></button>
        </div>
    </form>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>