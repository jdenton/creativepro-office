<? $this->load->view('includes/headerLight'); ?>
<div id="loginBox" pageID="accountLogin">
    <div class="logo"><?=$logoValue; ?></div>

	<div class="top"><h1 style="margin-left: 3px;"><?=$pageTitle; ?></h1></div>
    <div class="middle" style="padding-top: 6px;">
        <? if (!empty($message)) { ?>
		<div id="messageContainer" style="width: 486px; margin-left: 15px;" class="boxYellow">
			<?=$message; ?>
		</div>
        <? } ?>
        <div style="padding: 24px; float: left;">
			<form id="formLogin" class="noBorder" action="<?=site_url('login/tryLogin'); ?>" method="POST">
                <input type="hidden" name="subdomain" value="<?=$subdomain; ?>" />
                <h2>Email / User Name</h2>
                <input class="bigText" type="text" style="width: 250px;" id="userid" name="userid" />

                <h2 style="margin-top: 12px;">Password</h2>
                <input class="bigText" type="password" style="width: 250px;" id="password" name="password" />

                <p style="margin: 6px 0 6px 0;">
                    <input type="checkbox" id="remember" name="remember" value="1" />
                    <label for="remember">&nbsp;Remember me for 30 days</label>
                </p>
				<div style="margin: 12px 0 0 -2px;">
                    <button class="buttonExpand action" id="buttonLogin" tabindex="9" style="margin-right: 24px;"><span class="login">Login</span></button>
                    <a href="#" id="linkFindPassword" title="Find my password">I forgot my password.</a>
                </div>
			</form>

			<form id="formFindPassword" action="<?=site_url('login/findPassword'); ?>" method="POST" style="display: none;">
                <input type="hidden" name="subdomain" value="<?=$subdomain; ?>" />
                <div>
                    <h2>Email address</h2>
                    <input class="bigText" type="text" style="width: 250px;" id="email" name="email" />
                </div>
				<div style="margin-top: 12px;">
                    <button class="buttonExpand action" id="buttonFindPassword" style="margin-right: 24px;"><span class="buttonDecorator search">Find my password</span></button>
                    <a href="#" id="buttonCancelFindPassword" style="float: left;">Cancel</a>
				</div>
                    <p style="height: 20px;"></p>
			</form>
		</div>
        <div style="float: left; margin-top: 60px;">
                <div class="cpoLogo"></div>
                <div style="margin: 12px 0 0 20px;">
                    <a href="<?=INSECURE_URL.'contact'; ?>">Contact us</a>&nbsp;
                    <a href="<?=INSECURE_URL.'pricing'; ?>">Create an account</a>
                </div>
        </div>
        <div style="clear: left;"></div>
	</div>
    <div class="bottom"></div>
    <div style="margin: 6px 0 0 24px; clear: both;">
        <p style="margin-bottom: 6px;">CreativePro Office works best with</p>
        <span class="browserIcons firefox">4+</span>
        <span class="browserIcons ie">9+</span>
        <span class="browserIcons chrome">10+</span>
        <span class="browserIcons safari">4+</span>
        <span class="browserIcons opera">10+</span>
    </div>
</div>
<? $this->load->view('includes/footerLight'); ?>