<?
$this->load->view('includes/headerPublic');
?>
<div class="leftPane">
    <form action="<?=BASE_URL; ?>signup/createAccount" id="formSignup" method="POST">
    <input type="hidden" name="createAccount" value="1" />
    <input type="hidden" name="selectedPlan" id="selectedPlan" value="<?=$planDataArray['selectedPlan']; ?>" />
    <input type="hidden" name="webAddress" value="0" />

    <!-- <h2 class="blue mBottom">Selected Plan</h2> -->
    
    <? $this->load->view('includes/planDetailBox'); ?>
    
    <h2 class="blue mBottom">Enter Account Information</h2>

    <div class="messageBlock mBottom">
        <span id="errorCompanyName" class="errorText"></span>
        <label for="companyName" style="margin-bottom: 6px;">Company name</label>
        <input type="text" tabindex="5" style="width: 400px;" class="bigField" name="companyName" id="companyName" />
    </div>

    <div class="messageBlock mBottom">
        <span id="errorWebAddress" class="errorText"></span>
        <label for="webAddress" id="labelWebAddress" style="margin-bottom: 6px;">Select your account web address</label>
        <span class="bigText">http:// </span><input type="text" tabindex="6" style="width: 250px;" class="bigField" name="webAddress" id="webAddress" /><span class="bigText"> .mycpohq.com</span>
    </div>

    <label for="companyTimezone" style="margin-bottom: 6px;">Default timezone</label>
    <?=timezone_menu('','bigField','companyTimezone',408,7); ?>
    <div class="mBottom"></div>

    <div style="width: 200px; float: left; margin-right: 12px;">
        <label for="companyCurrency" style="margin-bottom: 6px;">Default currency</label>
        <select name="companyCurrency" tabindex="8" id="companyCurrency" class="bigField" style="width: 196px;">
        <?
        $currencies = currencies();
        foreach($currencies as $key => $value) {
            echo '<option value="'.$key.'"';
            if ($key == 'USD') {
                echo ' selected="selected" ';
            }
            echo '>'.$value[0].' ['.$value[1].']</option>';
        }
        ?>
        </select>
    </div>
    <div style="width: 200px; float: left;">
        <label for="companyLanguage" style="margin-bottom: 6px;">Default language</label>
        <select name="companyLanguage" tabindex="9" id="companyLanguage" class="bigField" style="width: 196px;">
        <?
        $languages = languages();
        foreach($languages as $key => $value) {
            echo '<option value="'.$key.'"';
            if ($key == 'en') {
                echo ' selected="selected" ';
            }
            echo '>'.$value.'</option>';
        }
        ?>
        </select>
    </div>
    <div style="clear: left;" class="hSpacer_12"></div>   

    <h2 class="blue mTop mBottom">Account Owner Setup</h2>
    
    <div class="messageBlock mBottom">
        <span id="errorName" class="errorText"></span>
        <div style="width: 200px; float: left; margin-right: 12px;">
            <label for="firstName" style="margin-bottom: 6px;">First name</label>
            <input type="text" tabindex="10" style="width: 188px;" class="bigField" name="firstName" id="firstName" />
        </div>
        <div style="width: 200px; float: left;">
            <label for="lastName" style="margin-bottom: 6px;">Last name</label>
            <input type="text" tabindex="11" style="width: 188px;" class="bigField" name="lastName" id="lastName" />
        </div>
        <div style="clear: left;"></div>
    </div>

    <div class="messageBlock mBottom">
        <span id="errorEmail" class="errorText"></span>
        <label for="email" style="margin-bottom: 6px;">Email address</label>
        <input type="text" tabindex="12" style="width: 400px;" class="bigField" name="email" id="email" />
    </div>

    <div class="messageBlock">
        <span id="errorPassword" class="errorText"></span>
        <div style="width: 200px; float: left; margin-right: 12px;">
            <label for="password" style="margin-bottom: 6px;">Password</label>
            <input type="password" tabindex="13" style="width: 188px;" class="bigField" name="password" id="password" />
        </div>
        <div style="width: 200px; float: left;">
            <label for="passwordConfirm" style="margin-bottom: 6px;">Confirm password</label>
            <input type="password" tabindex="14" style="width: 188px;" class="bigField" name="passwordConfirm" id="passwordConfirm" />
        </div>
        <div style="clear: left;"></div>
    </div>

    <h2 class="blue mTop mBottom">Billing Information</h2>
    <p id="billingText">
        <span id="billingTextFree" style="display: <?=$planDataArray['billingTxtFreeDisplay']; ?>;"><span class="bigText">Your plan is free forever! Enjoy!</span></span>
        <span id="billingTextPaid" style="display: <?=$planDataArray['billingTxtPaidDisplay']; ?>;">
            <span class="bigText">Your free trial will expire at midnight on <?=$billingDate; ?>.</span><br />We will not collect your billing information until your free trial expires.
        </span>
    </p>

    <div class="boxBlue mTop" id="boxSubmit" style="margin-left: -6px;">
        <p style="margin: 6px 0 0 6px;">
            <input type="checkbox" tabindex="15" id="acceptTOS" name="acceptTOS" value="1" class="checkbox" />&nbsp;&nbsp;
            <label for="acceptTOS" class="inline">
                I have read and accepted the CreativePro Office
                <a href="<?=BASE_URL ;?>refund/popup" class="popup">Refund Policy</a>,
                <a href="<?=BASE_URL ;?>terms/popup" class="popup">Terms of Service</a>, and
                <a href="<?=BASE_URL ;?>privacy/popup" class="popup">Privacy Policy</a>.</label>
        </p>
        <input type="submit" value="" id="createAccount" tabindex="30" style="margin: 12px 0 4px 4px;" class="buttonLetsGetGoing" />
    </div>
    </form>

    <div id="planDescriptors">
        <div class="planfree"><?=$planDataArray['accountDescriptions']['free']; ?></div>
        <div class="plansolo"><?=$planDataArray['accountDescriptions']['solo']; ?></div>
        <div class="planshop"><?=$planDataArray['accountDescriptions']['shop']; ?></div>
        <div class="planteam"><?=$planDataArray['accountDescriptions']['team']; ?></div>
    </div>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>