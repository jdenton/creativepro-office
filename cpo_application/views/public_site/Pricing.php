<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <div style="width: 615px; float: left;">
        <div class="whiteBoxTop_615"></div>
        <div class="whiteBoxContentContainter_615" style="padding-left: 5px;">
            <table class="pricing">
                <tr>
                    <th class="pricingItem"></th>
                    <th class="blue1">
                        <h2>SOLO</h2>
                        <sup>$</sup><span class="biggerText white"><?=$currentPrices[1]; ?></span> monthly</th>
                    <th class="blue2">
                        <h2>SHOP</h2>
                        <sup>$</sup><span class="biggerText white"><?=$currentPrices[2]; ?></span> monthly</th>
                    <th class="blue3" style="border-right: none;">
                        <h2>TEAM</h2>
                        <sup>$</sup><span class="biggerText white"><?=$currentPrices[3]; ?></span> monthly</th>
                </tr>
                <tr>
                    <td class="pricingItem">Active projects</td>
                    <td class="blue1"><span class="bigText white">10</span></td>
                    <td class="blue2"><span class="bigText white">50</span></td>
                    <td class="blue3" style="border-right: none;"><span class="bigText white">Unlimited</span></td>
                </tr>
                <tr>
                    <td class="pricingItem">Storage</td>
                    <td class="blue1"><span class="bigText white">10 GB</span></td>
                    <td class="blue2"><span class="bigText white">50 GB</span></td>
                    <td class="blue3" style="border-right: none;"><span class="bigText white">100 GB</span></td>
                </tr>
                <tr>
                    <td class="pricingItem">Team members</td>
                    <td class="blue1"><span class="bigText white">3</span></td>
                    <td class="blue2"><span class="bigText white">10</span></td>
                    <td class="blue3" style="border-right: none;"><span class="bigText white">Unlimited</span></td>
                </tr>
                <tr>
                    <td class="pricingItem">Support</td>
                    <td class="blue1"><strong>Priority email</strong></td>
                    <td class="blue2"><strong>Priority email</strong></td>
                    <td class="blue3" style="border-right: none;"><strong>Priority email</strong></td>
                </tr>
                <tr>
                    <td class="pricingItem" style="border: none; background: none;">
                        <span class="iconNoCCRequired" style="font-size: 1em; font-weight: normal;">No credit card required!</span>
                    </td>
                    <td class="black"><a href="/signup/solo" class="orangeButton" style="margin: 0 0 0 32px;"><span>Sign up</span></a></td>
                    <td class="black"><a href="/signup/shop" class="orangeButton" style="margin: 0 0 0 32px;"><span>Sign up</span></a></td>
                    <td class="black" style="border-right: none;"><a href="/signup/team" class="orangeButton" style="margin: 0 0 0 32px;"><span>Sign up</span></a></td>
                </tr>
            </table>
        </div>
        <div class="whiteBoxBottom_615"></div>
    </div>
    <div style="clear: left; height: 24px;"></div>
    <? $this->load->view('includes/plansInclude'); ?>
    <div style="clear: left; height: 24px;"></div>

    <h1 class="mBottom">All paid plans come with a 30-day FREE trial!</h1>
    <p class="text">If you sign up for a premium plan and don't like it for any reason, you can cancel your
    account within 30 days and you won't be billed anything.</p>

    <h2 class="brown mBottom">Upgrade/downgrade your plan at any time.</h2>
    <p class="text">You're never locked into a plan that doesn't fit your business. Buy a small plan today and
    upgrade as your business expands and your team or project needs increase.</p>

    <h2 class="brown mBottom">Cancel your account at any time.</h2>
    <p class="text">You can cancel your account at any time with one click - no emails to support, no waiting.
        You will receive verification that your account has been cancelled and that your bank account is no longer being billed
        (for paid plans only).
    </p>

    <!--<h2 class="blue">Export your data at any time.</h2>
    <p class="text">hgjklgh ghdfk jlhdsgkl hfjkslg hjfklghf jkglhfjkglh f hg fhjgklsdfhgjklfsghjfklsh</p>-->

    <h2 class="brown mBottom">Payment options.</h2>
    <p class="text">We currently accept Visa, MasterCard, American Express, and PayPal. We accept online payment only so
    we cannot accept a P.O. and we will not invoice you.</p>
    
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>