<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <h1 class="mBottom12">Please login to access your downloads</h1>
    
        <div id="messageContainer" class="boxYellow" style="padding: 12px;">
			<?=$message; ?>
		</div>
        <div class="hSpacer_12"></div>
        <div style="width: 660px;">
            <div style="float: left; width: 280px;">
                <form id="formLogin" class="noBorder" action="<?=site_url('selfhosteddownload/tryLogin'); ?>" method="POST">
                    <div style="margin-left: 9px;">
                        <p>
                        <label style="width: 120px;">Order Number</label>
                        <input class="bigText" tabindex="1" type="text" style="width: 250px;" id="orderNo" name="orderNo" />
                        </p>
                        <p>
                        <label style="width: 120px;">Installation Key</label>
                        <input class="bigText" tabindex="2" type="text" style="width: 250px;" id="installationKey" name="installationKey" />
                        </p>                        
                    </div>
                    <div style="margin: 9px 0 0 6px;">
                        <button class="buttonExpand blueGray" id="buttonLogin" tabindex="3" style="margin-right: 24px;"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator login">Login</span></span></button>
                    </div>
                </form>
            </div>
        </div>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>
<? $this->load->view('includes/footerPublic'); ?>
