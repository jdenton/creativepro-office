<? $this->load->view('includes/headerPublic'); ?>
<div class="leftPane">
    <div class="homePageFeatureBox left"><div class="dashboard">
        <h2>Office Dashboard</h2>
        <p>Stay informed at a glance with dashboard widgets that show your calendar, open invoices, tasks,
            current projects and much more.</p>
    </div></div>
    <div class="homePageFeatureBox"><div class="projects">
        <h2>Project Manager</h2>
        <p>Critical project data like tasks, hours spent, amounts invoiced, project contacts,
            notes and messages are all presented in one concise view.</p>
    </div></div>

    <div class="homePageFeatureBox left"><div class="invoices">
        <h2>Invoices &amp; Expenses</h2>
        <p>Quickly create professional looking invoices for your clients and send them via email.
           Quickly add timesheet entries and expense items to your invoices.</p>
    </div></div>
    <div class="homePageFeatureBox"><div class="calendar">
        <h2>Group Calendars</h2>
        <p>Share multiple calendars with your team or clients.  Export calendars in iCal format,
            RSS, or pull them into your Google calendar account.</p>
    </div></div>

    <div id="moreFeaturesContainer" class="hide">

    <div class="homePageFeatureBox left"><div class="timeclock">
        <h2>Timesheets &amp; Time Clock</h2>
        <p>Never lose another hour of billable work! The time clock widget is always close by while timesheets give you a
            daily view of time spent on specific projects and tasks.</p>
    </div></div>
    <div class="homePageFeatureBox"><div class="users">
        <h2>Team Permissions</h2>
        <p>Team members should only see what they need to see! Administrators have very fine control
            over team member permissions and access.</p>
    </div></div>

    <div class="homePageFeatureBox left"><div class="tasks">
        <h2>Task Management</h2>
        <p>Tasks, milestones, simple to-do lists...CreativePro Office supports them all.  Assign tasks to multiple
            team members, attach files, create comment threads, and much more.</p>
    </div></div>
    <div class="homePageFeatureBox"><div class="international">
        <h2>International</h2>
        <p>CreativePro Office currently supports almost 10 languages and 20 currencies with more being added soon.</p>
    </div></div>

    <div class="homePageFeatureBox left"><div class="fileshare">
        <h2>File Sharing</h2>
        <p>Share files with clients and team members. You can attach files to projects, tasks or messages.</p>
    </div></div>
    <div class="homePageFeatureBox"><div class="reports">
        <h2>Office Reports</h2>
        <p>See what's happening over time with finance, task, time-sheet, and team member productivity reports.</p>
    </div></div>

    <div class="homePageFeatureBox left"><div class="rss">
        <h2>Office RSS Feeds</h2>
        <p>View tasks, calendar entries, messages, and more in your favorite RSS feed reader without having to
            log into your CreativePro Office account.</p>
    </div></div>
    <div class="homePageFeatureBox"><div class="search">
        <h2>Search</h2>
        <p>Locate anything in your office quickly including tasks, invoices, contacts, projects, and
            messages by using the account wide search feature.</p>
    </div></div>

    <div class="homePageFeatureBox left"><div class="messages">
        <h2>Team &amp; Client Messaging</h2>
        <p>An integral commenting and messaging system allows account owners, team members, and clients to
            communicate.</p>
    </div></div>
    <div class="homePageFeatureBox"><div class="trash">
        <h2>Recycle Bin &amp; Restore</h2>
        <p>Never accidentally lose data again! Everything you delete in CreativePro Office first goes to the recycle
            bin.</p>
    </div></div>

    </div>

    <div style="clear: both;" class="boxGrey">
        <p>...and a whole lot <a href="#" id="homeMoreLink">more</a>.
        <a href="tour">Take a tour</a> or <a href="pricing">See our plans and pricing.</a></p>
    </div>
    <div class="contentSpacer"></div>
    
    <div style="width: 50%; float: left;">
        <a href="http://blog.<?=SITE_DOMAIN; ?>" class="h2link brown iconBlogBlock" style="margin: 0 0 12px 6px;">Recent blog entries</a>
        <ul class="arrow1 mBottom" style="margin-left: 25px;">
        <?
        foreach($blogRecentEntries as $navEntry) {
            echo '<li><a href="'.BASE_URL.'blog/entry/'.$navEntry['EntryURL'].'">'.$navEntry['Title'].'</a></li>';
        }
        ?>
        </ul>
    </div>
    <div style="width: 48%; float: left;">
        <a href="http://twitter.com/cpohq" target="_blank" class="h2link brown iconTwitterBlock" style="margin: 0 0 12px 6px;">Follow us on Twitter!</a>
        <?
        $a = 0;
        if (is_array($tweets) && count($tweets)>0) {
            foreach($tweets as $tweet) {
                if ($a<3) {
                    echo '<p style="margin: 0 0 12px 6px;"><img src="'.$tweet->user->profile_image_url.'" style="float: left; padding: 0 6px 0 0; width: 27px; height: 27px;" />';
                    $tweet = twitterReplaceHashTags(twitterReplaceMention($tweet->text)).'</p>';
                    echo auto_link($tweet,'url');
                    $a++;
                }
            }
        }
        ?>
    </div>
</div>
<div class="rightPane">
    <? $this->load->view('includes/rightColumnPublic'); ?>
</div>

<div style="clear: both;"></div>
<div class="contentSpacer mTop18"></div>
<div class="containerScreenshots" style="padding-top: 6px; margin-left: 6px;">
    <a href="images/screenshots/bigDashboardWidgets.png" rel="hometour" class="tourThumb tourPic" title="Selecting dashboard widgets to display.">
        <img src="images/screenshots/thumbDashboardWidgets.png" class="tourThumb left" title="Selecting dashboard widgets to display." border="0" />
    </a>
    <a href="images/screenshots/bigDashboardWidgetsOpen.png" rel="hometour" class="tourThumb tourPic" title="The calendar, task, time tracker widgets in edit mode.">
        <img src="images/screenshots/thumbDashboardWidgetsOpen.png" class="tourThumb" title="The calendar, task, time tracker widgets in edit mode." border="0" />
    </a>
    <a href="images/screenshots/bigProjectDetail.png" rel="hometour" class="tourThumb tourPic" title="Project detail view showing assigned team members, messages, files, tasks, notes, and invoices.">
        <img src="images/screenshots/thumbProjectDetail.png" class="tourThumb left" title="Project detail" border="0" />
    </a>
    <a href="images/screenshots/bigProjectNotes.png" rel="hometour" class="tourThumb tourPic" title="Project notes">
        <img src="images/screenshots/thumbProjectNotes.png" class="tourThumb" title="Project notes give you an easy way to capture important project information." border="0" />
    </a>
    <a href="images/screenshots/bigTaskPDF.png" rel="hometour" class="tourThumb tourPic" title="Create easy to read task lists in PDF format.">
        <img src="images/screenshots/thumbTaskPDF.png" class="tourThumb" title="Task PDF export" border="0" />
    </a>
    <a href="pricing" style="margin: -10px 0 0 6px; float: left;" class="promoBadge5"></a>
</div>

<? $this->load->view('includes/footerPublic'); ?>
