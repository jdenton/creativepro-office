<?php
class TeamView extends Controller {
	function TeamView()
	{
		parent::Controller();
		$this->config->set_item('language',$this->session->userdata('language'));
		if($this->session->userdata('logged') != TRUE) {
			header('Location: '.site_url('Login'));
		}
	}

	function _init() {
		$data['page']              = 'team';
		$data['pageTitle']         = lang('menu_view_timesheet_reports');
		$data['wysiwyg']           = 0;
		$data['map']               = 0;
		$data['jsFileArray']       = array('jsTeam.js');
		$data['pageIconClass']     = 'iconPageTeam';
		$data['pageLayoutClass']   = 'withRightColumn';

		return $data;
	}

	function index() {
		$data = TeamView::_init();
		$this->load->view('team/TeamView',$data);
	}
}
?>