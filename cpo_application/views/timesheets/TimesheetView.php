<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="timesheetView">
	<div id="tabsTimesheet" class="ui-tabs">
		<ul class="ui-tabs-nav" style="float: left;">
	        <li class="ui-tabs-selected" id="tabTimesheet"><a href="#windowTimesheet"><span><?=lang('timesheets'); ?></span></a></li>
	        <li><a href="#windowTimesheetReports" id="tabTimesheetReports"><span><?=lang('timesheets_reports'); ?></span></a></li>
	    </ul>
        <div style="float: right;" id="timesheetViewControls">
             <button id="btnViewGrid" class="smallButton" style="float: right; margin-right: 6px;"><span class="timesheet"><?=lang('common_grid_view'); ?></span></button>
             <button id="btnViewEntry" class="smallButton" style="display: none; float: right; margin-right: 6px;"><span class="entry"><?=lang('common_entry_view'); ?></span></button>

        </div>
        <div style="clear: both;"></div>
	    <div style="min-height: 200px;" class="windowFrame">
            <div id="windowTimesheet">
                <input type="hidden" id="dateStart" />
                <input type="hidden" id="dateEnd" />
                <input type="hidden" id="userID" />
                <input type="hidden" id="today" value="<?=convertMySQLToGMT(date('Y-m-d'),'pull','Y-m-d'); ?>" />
                <div id="timesheetWeekContainer" style="margin-top: 18px;">
                    <ul id="weekTabs" class="ui-tabs-nav noPrint" style="float: left;">
                        <li id="tabMo" class="tab"><a href="#windowMo"><span>Mon</span></a></li>
                        <li id="tabTu" class="tab"><a href="#windowTu"><span>Tue</span></a></li>
                        <li id="tabWe" class="tab"><a href="#windowWe"><span>Wed</span></a></li>
                        <li id="tabTh" class="tab"><a href="#windowTh"><span>Thu</span></a></li>
                        <li id="tabFr" class="tab"><a href="#windowFr"><span>Fri</span></a></li>
                        <li id="tabSa" class="tab"><a href="#windowSa"><span>Sat</span></a></li>
                        <li id="tabSu" class="tab"><a href="#windowSu"><span>Sun</span></a></li>
                    </ul>
                    <div style="float: right; width: 170px;">
                        <ul class="smallButtonCluster left mTop3">
                            <li><a href="#" id="goToWeekPrev" title="<?=lang('timesheets_last_week'); ?>"><span class="prev"></span></a></li>
                            <li><a href="#" id="goToWeekNext" title="<?=lang('timesheets_next_week'); ?>"><span class="next"></span></a></li>
                        </ul>
						<input type="text" id="goToWeek" title="<?=lang('timesheets_select_date'); ?>" class="dateField right" value="<?=date('m/d/Y'); ?>" style="width: 100px; margin-left: 6px;" />
                    </div>
                    <div style="clear: both;"></div>
                    <div id="timesheetWeekEntryContainer" style="clear: both; min-height: 200px;" class="windowFrame">
                        <div id="timesheetEntryControl" class="barYellow bottom" style="padding: 6px 0 6px;">
                            <button id="btnAddEntry" class="smallButton" style="margin-left: 6px;"><span class="clock"><?=lang('common_new_entry'); ?></span></button>
                        </div>
                        <div id="mainTimesheetForm" class="hide barBlue bottom">
                            <div class="timesheetEntryForm" style="position: relative;">
                                <div  style="padding: 6px 0 6px 6px;">
                                <input type="hidden" class="timesheetID" value="0" />
                                <div>
                                    <input type="text" style="cursor: pointer; border: none; margin-left: -3px; padding-left: 22px !important;" class="hide iconEditSmall txtDate bigText" />
                                </div>
                                <div style="float: left; margin-right: 12px;">
                                    <b><?=lang('timesheets_project_task'); ?></b><br />
                                    <select class="selectProject" tabindex="1" style="width: 270px;">
                                        <option><?=lang('project_select_project'); ?></option>
                                        <option></option>
                                        <?
                                        if (is_array($projects) && count($projects)>0) {
                                            foreach($projects as $project) {
                                                echo '<option value="'.$project['ProjectID'].'">'.decode_utf8(stripslashes($project['Title'].' ('.$project['Company'].')')).'</option>';
                                            }
                                        }
                                        ?>
                                    </select><br />
                                    <select class="selectTask" tabindex="2" style="margin-top: 9px; width: 270px;" disabled="disabled"></select>
                                </div>
                                <div style="float: left; margin-right: 12px;">
                                    <b><?=lang('finance_invoice_type_hours'); ?></b><br />
                                    <input type="text" tabindex="3" class="txtTime hugeTextEntry" style="text-align: right; height: 50px; width: 80px;" />
                                </div>
                                <div style="float: left; margin-right: 9px;">
                                    <b><?=lang('common_comments'); ?></b><br />
                                    <textarea class="txtComments" tabindex="4" style="margin-top: 0px; width: 230px; height: 50px;"></textarea><br />
                                    <input type="checkbox" class="chkBillable" value="1" checked="checked" /> <label class="lblBillable normal"><?=lang('common_billable'); ?></label>
                                </div>
                                <div style="clear: left;"></div>
                                <ul class="smallButtonCluster bottomRight" style="bottom: 30px;">
                                    <li><a href="#" tabindex="5" class="btnTimeSave"><span class="add"></span></a></li>
                                    <li><a href="#" tabindex="6" class="btnTimeCancel"><span class="delete"></span></a></li>
                                </ul>
                            </div>

                            </div>
                        </div>

                        <div id="windowMo" class="entryWindow ui-tabs-hide"></div>
                        <div id="windowTu" class="entryWindow ui-tabs-hide"></div>
                        <div id="windowWe" class="entryWindow ui-tabs-hide"></div>
                        <div id="windowTh" class="entryWindow ui-tabs-hide"></div>
                        <div id="windowFr" class="entryWindow ui-tabs-hide"></div>
                        <div id="windowSa" class="entryWindow ui-tabs-hide"></div>
                        <div id="windowSu" class="entryWindow ui-tabs-hide"></div>
                    </div>
                </div>    
				<div id="timesheetViewOnlyGridContainer" style="margin-top: 6px;" class="hide"></div>
			</div>

            <div id="windowTimesheetReports" class="ui-tabs-hide">
				<div class="barYellow bottom">
					<div style="float: left; margin: 7px 0 0 12px;">
						<div>
							<select id="timesheetSearchClient" style="width: 300px; margin: 3px;">
							<option value="0"><?=lang('project_form_select_client'); ?></option>
							<option value="0"></option>
							<?
                            if (is_array($clients) && count($clients)>0) {
                                foreach($clients as $client) {
                                    echo '<option value="'.$client['ClientID'].'">'.decode_utf8(stripslashes($client['Company'])).'</option>';
                                }
                            }
							?>
							</select>
						</div>
						<div>
							<select id="timesheetSearchProject" style="width: 300px; margin: 3px;">
							<option value="0"><?=lang('project_select_project'); ?></option>
							<option value="0"></option>
							<?
                            if (is_array($projects) && count($projects)>0) {
                                foreach($projects as $project) {
                                    echo '<option value="'.$project['ProjectID'].'">'.decode_utf8(stripslashes($project['Title'].' ('.$project['Company'].')')).'</option>';
                                }
                            }
							?>
							</select>
						</div>
						<div>
							<select id="timesheetSearchTask" disabled="disabled" style="width: 300px; margin: 3px;">
							</select>
						</div>
					</div>
					<div style="float: right; padding: 6px 12px 6px 6px;">
						<div style="margin: 3px;">
							<div>
							<input type="text" tabindex="2" id="timesheetSearchDateStart" class="dateField" value="<?=$searchDateStart; ?>" style="width: 95px; margin-right: 6px;" />
							<strong>to</strong>&nbsp;
							<input type="text" tabindex="3" id="timesheetSearchDateEnd" class="dateField" value="<?=$searchDateEnd; ?>" style="width: 95px;" />
							</div>
						</div>
						<div>
                            <? if (count($teamMembers)>0) { ?>
							<select id="timesheetSearchTeamMember" style="width: 300px; margin: 3px;">
                                <option value="0"><?=lang('team_select_team_member'); ?></option>
                                <option value="0"></option>
                                <?
                                if (is_array($teamMembers) && count($teamMembers)>0) {
                                    foreach($teamMembers as $teamMember) {
                                        $fullName = $teamMember['NameFirst'].' '.$teamMember['NameLast'];
                                        $UID      = $teamMember['LoginUserid'];
                                        $PID      = $teamMember['PID'];
                                        echo '<option value="'.$UID.'">'.$fullName.'</option>';
                                    }
                                }
                                ?>
							</select>
                            <? } ?>
						</div>
						<button class="buttonExpand blueGray action" style="margin-top: 3px;" id="buttonCreateTimesheetReport"><span class="chart"><?=lang('common_create_report'); ?></span></button>
					</div>
                    <div style="clear: both;"></div>
				</div>
				<div style="padding: 6px;" id="timesheetReportContainer"></div>
		    </div>
		</div>
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
