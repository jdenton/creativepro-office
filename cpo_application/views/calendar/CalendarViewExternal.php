<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="viewEvents">
	<div style="padding: 6px; width: 100%;">
	<?
	if (!empty($errorMessage)) {
		echo '<div class="errorMessageBig">'.$errorMessage.'</div>';
	} else { ?>
        <div>
			<h1 style="margin-bottom: 12px;"><?=$eventInfo['Title']; ?></h1>
			<div class="icon_calendar_small">
				<strong><?=$eventInfo['DateString']; ?></strong>
				<span class="linkCalendarText eventBlock <?=$eventInfo['CalendarColor']; ?>" style="margin-left: 9px;"><strong><?=$eventInfo['CalendarTitle']; ?></strong></span>
			</div>
		<?
		if (!empty($eventInfo['Description'])) {
			echo '<p style="margin-top: 18px;">'.$eventInfo['Description'].'</p>';
		}
		if (!empty($eventInfo['Location'])) {
			echo '<h2>'.lang('common_location').'</h2>';
			echo '<p>'.$eventInfo['Location'].'</p>';
		}
		?>
        </div>
        <div id="mapContainer" style="width: 708px; height: 500px; margin-top: 24px;"></div>
        <div id="mapGoogleAddressContainer" style="display: none;"><?=$eventInfo['Location']; ?></div>
	<? } ?>
	</div>
</div>
<?
$this->load->view('includes/rightColumn');
$this->load->view('includes/footer');
?>