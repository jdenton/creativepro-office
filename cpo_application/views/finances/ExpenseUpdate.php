<? $this->load->view('includes/header'); ?>

<div id="pageLeftColumn" pageID="expenseUpdate">
    <div id="formExpenseContainer">
        <form class="greyForm" id="formExpense" name="formExpense" action="<?=BASE_URL; ?>finances/ExpenseUpdate/saveExpense" method="POST">

        <div id="pageMessage"></div>
        <input type="hidden" name="expenseID" id="expenseID" value="<?=$expenseInformation['ExpenseID']; ?><?=set_value('expenseID'); ?>" />
        <input type="hidden" name="clientIDHolder" id="clientIDHolder" value="<?=$expenseInformation['ClientID']; ?><?=set_value('clientIDHolder'); ?>" />
        <input type="hidden" name="projectIDHolder" id="projectIDHolder" value="<?=$expenseInformation['ProjectID']; ?><?=set_value('projectIDHolder'); ?>" />
        <input type="hidden" name="action" id="action" value="<?=$action; ?><?=set_value('action'); ?>" />
        <input type="hidden" name="tagHolder" id="tagHolder" value="<? echo str_replace(' ',',',$expenseInformation['Tags']); ?><?=set_value('tagHolder'); ?>" />
        <input type="hidden" name="save" id="save" value="1" />

        <p>
            <label class="required" for="expenseTitle"><?=lang('finance_expense_name'); ?></label>
            <input type="text" value="<?=$expenseInformation['Title']; ?><?=set_value('expenseTitle'); ?>" id="expenseTitle" name="expenseTitle" tabindex="1" style="width: 400px;" />
            <?=form_error('expenseTitle','<span class="formError">','</span>'); ?>
        </p>
        <p>
            <label class="required" for="expenseAmount"><?=lang('finance_expense_amount'); ?></label>
            <input type="text" id="expenseAmount" name="expenseAmount" class="numberField" tabindex="2" value="<?=$expenseInformation['Amount']; ?><?=set_value('expenseAmount'); ?>" style="width: 100px; margin-right: 6px;" />
            <?=form_error('expenseAmount','<span class="formError">','</span>'); ?>
        </p>
        <p>
            <label for="expenseMarkup"><?=lang('finance_expense_markup'); ?></label>
            <input type="text" id="expenseMarkup" settingType="expenseMarkup_account_expense" name="expenseMarkup" class="numberField settingsField" tabindex="3" value="<?=$expenseInformation['Markup']; ?><?=set_value('expenseAmount'); ?>" style="width: 100px; margin-right: 6px;" />
            <input type="radio" name="markupType" settingType="markupType_account_expense_radio" value="percent" class="settingsField" id="markupType_percent" tabindex="4" <? if ($expenseInformation['MarkupType'] == 'percent') { echo 'checked'; } ?> /> <?=lang('common_percent'); ?>
            <input type="radio" name="markupType" settingType="markupType_account_expense_radio" id="markupType_amount" tabindex="5" class="settingsField" value="amount" <? if ($expenseInformation['MarkupType'] == 'amount') { echo 'checked'; } ?> /> <?=lang('finance_expense_amount'); ?>
        </p>
        <p>
            <label for="expenseDate"><?=lang('finance_expense_purchase_date'); ?></label>
            <input type="text" id="expenseDate" name="expenseDate" class="dateField" tabindex="6" value="<?=$expenseDate; ?><?=set_value('expenseDate'); ?>" style="width: 100px; margin-right: 6px;" />
        </p>
        <p class="noBorder">
            <label for="expenseCategory"><?=lang('finance_expense_category'); ?></label>
            <input type="text" name="expenseCategorySelect" id="expenseCategorySelect" value="<?=$expenseInformation['Category']; ?>" tabindex="7" style="width: 400px;" />
			<input type="hidden" name="expenseCategory" id="expenseCategory" value="<?=$expenseInformation['CatID']; ?>" />
        </p>
        <p>
            <label><?=lang('finance_expense_notes'); ?></label>
            <textarea class="expanding" name="expenseNotes" id="expenseNotes" tabindex="8" style="width: 400px; height: 40px;"><?=$expenseInformation['Comments']; ?><?=set_value('expenseNotes'); ?></textarea>
        </p>
        <p>
            <label for="expensePaymentMethod"><?=lang('finance_expense_payment_method'); ?></label>
            <select name="expensePaymentMethod" id="expensePaymentMethod" size="1" tabindex="9" style="width: 150px;">
                <option value="0"><?=lang('common_field_select'); ?></option>
                <option value="cash" <? if ($expenseInformation['PaymentMethod'] == 'cash') { echo 'selected="selected"'; } ?>><?=lang('finance_expense_pay_method_cash'); ?></option>
                <option value="check" <? if ($expenseInformation['PaymentMethod'] == 'check') { echo 'selected="selected"'; } ?>><?=lang('finance_expense_pay_method_check'); ?></option>
                <option value="cc_company" <? if ($expenseInformation['PaymentMethod'] == 'cc_company') { echo 'selected="selected"'; } ?>><?=lang('finance_expense_pay_method_cc_company'); ?></option>
                <option value="cc_personal" <? if ($expenseInformation['PaymentMethod'] == 'cc_personal') { echo 'selected="selected"'; } ?>><?=lang('finance_expense_pay_method_cc_personal'); ?></option>
                <option value="paypal" <? if ($expenseInformation['PaymentMethod'] == 'paypal') { echo 'selected="selected"'; } ?>><?=lang('finance_expense_pay_method_paypal'); ?></option>
            </select>
            &nbsp;&nbsp;<strong><?=lang('finance_expense_pay_method_acct_no'); ?></strong> <input type="text" id="expenseAcctNo" name="expenseAcctNo" tabindex="10" value="<?=$expenseInformation['AccountNumber']; ?><?=set_value('expenseAcctNo'); ?>" style="width: 112px;" />
        </p>
        <div id="billableContainer">
            <p class="noPad">
                <label for="expenseReimburse"><?=lang('finance_expense_reimbursement'); ?></label>
                <select name="expenseReimburse" id="expenseReimburse" size="1" tabindex="11" style="width: 408px;">
                    <option value="0"><?=lang('common_field_select'); ?></option>
                    <option value="yes" <? if ($expenseInformation['Reimbursement'] == 'yes') { echo 'selected="selected"'; } ?>><?=lang('finance_expense_reimbursement_yes'); ?></option>
                    <option value="no" <? if ($expenseInformation['Reimbursement'] == 'no') { echo 'selected="selected"'; } ?>><?=lang('finance_expense_reimbursement_no'); ?></option>
                    <option value="personal" <? if ($expenseInformation['Reimbursement'] == 'personal') { echo 'selected="selected"'; } ?>><?=lang('finance_expense_reimbursement_personal'); ?></option>
                </select>
            </p>
        </div>
        <div id="billableToContainer" <?=$billToDisplay; ?>>
            <div style="float: left;">
                <p class="noPad">
                    <label for="clientID"><?=lang('project_form_select_client'); ?></label>
                    <input type="text" class="" value="<?=$expenseInformation['Company']; ?>" id="clientID" name="clientID" tabindex="12" style="width: 400px;" />
                    <div class="formHelpText" style="margin-top: -6px;"><span class="subText"><?=lang('client_search'); ?></span></div>
                </p>
            </div>
            <div style="float: left;">
                <?=help('help_formfield_client_search','clientID','iconHelpFormOutside'); ?>
            </div>
            <div style="clear: left;"></div>
            <p  class="noPad" style="display: none;" id="projectIDContainer">
                <label for="projectID"><?=lang('project_select_project'); ?></label>
                <select id="projectID" name="projectID" tabindex="13" style="width: 408px;"></select>
            </p>
            <p class="noPad" id="invoiceIDContainer" style="display: none;">
                <label for="invoiceID"><?=lang('finance_expense_invoice'); ?></label>
                <select id="invoiceID" name="invoiceID" tabindex="14" style="display: none; width: 408px;"></select>
                <span style="display: block; margin-left: 261px;"><input type="checkbox" id="createNewInvoice" name="createNewInvoice" value="1" /> <span class="checkboxTrigger icon_invoice_small_right"><?=lang('finance_expense_invoice_new'); ?></span></span>
            </p>
        </div>        
        <p>
            <label for="expenseVendor"><?=lang('finance_expense_vendor'); ?></label>
            <? if (count($vendors)>0) { ?>
            <select name="expenseVendor" id="expenseVendor" size="1" tabindex="20" style="width: 408px;">
                <option value="0"><?=lang('common_field_select'); ?></option>
                <?
                foreach($vendors as $vendor) {
                    if (empty($vendor['Company'])) {
                        $vendorName = $vendor['NameFirst'].' '.$vendor['NameLast'];
                    } else {
                        $vendorName = $vendor['Company'];
                    }
                    echo '<option value="'.$vendor['PID'].'"';
                    if ($expenseInformation['VendorID'] == $vendor['PID']) { echo 'selected="selected"'; }
                    echo '>'.$vendorName.'</option>';
                }
                ?>
            </select>            
            <? } else { ?>
                <?=lang('finance_no_vendor'); ?> <a href="#" id="linkAddNewVendor" title="<?=lang('finance_add_vendor'); ?>"><?=lang('finance_add_vendor'); ?></a>
            <? } ?>
        </p>
        <p>
            <label><?=lang('finance_expense_repeats'); ?></label>
            <select id="expenseRepeats" name="expenseRepeats" tabindex="21" size="1" style="width: 300px;">
                <option value="0"><?=lang('finance_expense_repeats'); ?></option>
                <option value="week" <? if ($recurringRange == 'week') { echo 'selected="selected"'; } ?>><?=lang('calendar_repeats_week'); ?></option>
                <option value="month" <? if ($recurringRange == 'month') { echo 'selected="selected"'; } ?>><?=lang('calendar_repeats_month'); ?></option>
                <option value="year" <? if ($recurringRange == 'year') { echo 'selected="selected"'; } ?>><?=lang('calendar_repeats_year'); ?></option>
            </select>
            <div id="containerWeek" style="margin-left: 265px;" class="hide">
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Sun') { $sunChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekSU" name="recurringWeekSU" value="Sun" checked="<?=$sunChecked; ?>" /> <?=lang('cal_su') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Mon') { $monChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekMO" name="recurringWeekMO" value="Mon" <?=$monChecked; ?> /> <?=lang('cal_mo') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Tue') { $tueChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekTU" name="recurringWeekTU" value="Tue" <?=$tueChecked; ?> /> <?=lang('cal_tu') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Wed') { $wedChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekWE" name="recurringWeekWE" value="Wed" <?=$wedChecked; ?> /> <?=lang('cal_we') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Thu') { $thuChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekTH" name="recurringWeekTH" value="Thu" <?=$thuChecked; ?> /> <?=lang('cal_th') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Fri') { $friChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekFR" name="recurringWeekFR" value="Fri" <?=$friChecked; ?> /> <?=lang('cal_fr') ;?>
                <?
                foreach($recurringDays as $day) {
                    if ($day == 'Sat') { $satChecked = 'checked'; }
                }
                ?>
                <input type="checkbox" id="recurringWeekSA" name="recurringWeekSA" value="Sat" <?=$satChecked; ?> /> <?=lang('cal_sa') ;?>
            </div>
            <div id="containerMonth" style="margin-left: 265px;" class="hide">
                <? for($a=0;$a<=5;$a++) { ?>
                <select id="recurringMonthDay<?=$a; ?>" name="recurringMonthDay<?=$a; ?>" style="margin-right: 3px; width: 65px;">
                    <option value="0"><?=lang('date_day'); ?></option>
                    <?
                    for($b=1;$b<=31;$b++) {
                        $selected = '';
                        if (isset($recurringDays[$a]) && $recurringDays[$a] == $b) {
                            $selected = 'selected="selected"';
                        }
                        echo '<option value="'.$b.'" '.$selected.'>'.$b.'</option>';
                    }
                    ?>
                </select>
                <? } ?>
            </div>
            <div id="containerYear" style="margin-left: 265px;" class="hide">
                <? for($a=0;$a<=5;$a++) { ?>
                <div style="margin-bottom: 3px;">
                    <select id="recurringYearMonth<?=$a; ?>" name="recurringYearMonth<?=$a; ?>" style="margin-right: 3px; width: 100px;">
                        <option value="0"><?=lang('date_month'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '01') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="01" <?=$selected; ?>><?=lang('cal_january'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '02') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="02" <?=$selected; ?>><?=lang('cal_february'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '03') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="03" <?=$selected; ?>><?=lang('cal_march'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '04') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="04" <?=$selected; ?>><?=lang('cal_april'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '05') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="05" <?=$selected; ?>><?=lang('cal_may'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '06') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="06" <?=$selected; ?>><?=lang('cal_june'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '07') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="07" <?=$selected; ?>><?=lang('cal_july'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '08') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="08" <?=$selected; ?>><?=lang('cal_august'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '09') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="09" <?=$selected; ?>><?=lang('cal_september'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '10') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="10" <?=$selected; ?>><?=lang('cal_october'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '11') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="11" <?=$selected; ?>><?=lang('cal_november'); ?></option>
                        <?
                        if (isset($recurringDays[$a]) && substr($recurringDays[$a],0,2) == '12') {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="12" <?=$selected; ?>><?=lang('cal_december'); ?></option>
                    </select>
                    <select id="recurringYearDay<?=$a; ?>" name="recurringYearDay<?=$a; ?>" style="width: 70px;">
                    <option value="0"><?=lang('date_day'); ?></option>
                    <?
                    for($b=1;$b<=31;$b++) {
                        $selected = '';
                        if (isset($recurringDays[$a]) && $recurringDays[$a] == $b) {
                            $selected = 'selected="selected"';
                        }
                        echo '<option value="'.$b.'" '.$selected.'>'.$b.'</option>';
                    }
                    ?>
                    </select>
                </div>
                <? } ?>
            </div>
        </p>
        <p>
        <div style="float: left; margin-right: 18px;">
            <label><?=lang('client_form_tags'); ?></label>
            <div style="float: right;">
            <input type="text" name="expenseTags" id="expenseTags" tabindex="22" value="<?=$expenseInformation['Tags']; ?><?=set_value('expenseTags'); ?>" />
            </div>
        </div>
        <div style="float: left;">
            <?=help('help_formfield_tags','clientTags','iconHelpFormOutside'); ?>
        </div>
        </p>
        <div style="clear: left;"></div>
        <div class="buttonContainer">
            <button class="buttonExpand blueGray action primary" id="buttonSave" tabindex="25"><span class="save"><?=lang('button_save'); ?></span></button>
            <button class="buttonExpand yellow secondary cancel" id="buttonCancel" tabindex="26"><span class="cancel"><?=lang('button_cancel'); ?></span></button>
            <div style="clear: left;"></div>
        </div>
        </form>
    </div>   
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>