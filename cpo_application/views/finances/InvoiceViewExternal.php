<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="viewInvoice">
	<div style="padding: 6px; width: 100%;">
	<?
    if (!empty($payPalResponse)) {
        echo '<div class="infoMessageBig">'.$payPalResponse.'</div>';
    }
	if (!empty($errorMessage)) {
		echo '<div class="errorMessageBig">'.$errorMessage.'</div>';
	} else {
        if (!empty($emailPayPal) && $totalDue>0) {
            echo '<div class="boxYellow" style="margin-bottom: 24px; width: 705px;"><div style="padding: 6px;">';
            echo '<h2 style="margin-bottom: 12px;" class="icon_paypal_small">'.lang('finance_pay_with_paypal').'</h2>';

            echo '<input type="hidden" name="business" id="business" value="'.$emailPayPal.'">';
			echo '<input type="hidden" name="item_name" id="item_name" />';
            echo '<input type="hidden" name="currency_code" id="currency_code" value="'.$currencyCode.'" />';
            echo '<input type="hidden" name="return" id="returnURL" value="'.SITE_URL.$_SERVER['PHP_SELF'].'/success">';
			echo '<input type="hidden" name="cancel_return" id="cancel_return" value="'.SITE_URL.$_SERVER['PHP_SELF'].'/cancel">';
			
            echo '<div style="float: left; margin: 0 12px 0 22px;">'.lang('finance_enter_payment_amount').'&nbsp;&nbsp;&nbsp;<span class="bigText">'.getCurrencyMark($this->session->userdata('currency')).'&nbsp;</span>';
            echo '<input type="text" name="amount" id="amount" style="width: 100px;" class="bigText" value="'.$totalDue.'" />';
            echo '</div>';
            echo '<div style="float: left;"><button id="buttonMakePayPalPayment" class="smallButton" title="'.lang('finance_make_payment').'"><span class="money">'.lang('finance_make_payment').'</span></button></div>';

            echo '<div style="clear: left;"></div>';
            echo '</div></div>';
        }

        if ($payments != 0) {
            echo '<h2 style="margin: 6px 0 6px 0;">'.lang('finance_current_payments').'</h2>';
            echo '<div class="boxBlue" style="margin-bottom: 24px; width: 705px;">';
            foreach($payments as $payment) {
                $payID     = $payment['PaymentID'];
                $payAmount = $payment['PaymentAmount'];
                $payDate   = $payment['DatePayment'];
                $payType   = $payment['PaymentType'];
                $payAcct   = $payment['AcctCheckNo'];
                $payComment= $payment['PaymentComment'];
                $payString = $payType;

                if ($payAcct != '' && $payAcct != NULL) {
                    $payString .= ' '.$payAcct;
                }
                if ($payComment == '' || $payComment == NULL) {
                    $payComment = '';
                }

                echo '<p id="payRow_'.$payID.'">';
                echo '<span style="display: inline-block; width: 20%;"><strong>'.$payAmount.'</strong></span>';
                echo '<span style="display: inline-block; width: 25%;">'.$payDate.'</span>';
                echo '<span style="display: inline-block; width: 45%;">'.$payString.'</span><br />';
                echo '<span style="display: block; margin-left: 20px;">'.$payComment.'</span>';
                echo '</p>';
            }
            echo '<div class="boxYellow" style="margin: 3px; width: 691px; padding: 3px;"><h2>'.lang('finance_total_due').': '.$totalDueWithCurrency.'</h2></div>';
            echo '</div>';
        }

        echo '<input type="hidden" id="invoiceID" value="'.$invoiceID.'" />';
        echo $invoiceHTML;        
	} ?>
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>
