<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="financeView" class="helptag" data-help="finances">
    <script type="text/javascript" language="javascript">
	/* <![CDATA[ */
		var vendorData = '<?=$vendors; ?>';
	/* ]]> */
	</script>
    <input type="hidden" id="tag" value="<?=$tag; ?>" />
    <input type="hidden" id="tagExpense" value="<?=$tagExpense; ?>" />
    <input type="hidden" id="printInvoice" value="<?=$printInvoice; ?>" />
    <input type="hidden" id="invoiceID" value="<?=$invoiceID; ?>" />
    <input type="hidden" id="expenseID" value="<?=$expenseID; ?>" />
    <input type="hidden" id="contactType" value="V" />
    <input type="hidden" id="numberOfMessagesInvoice" value="<?=$numberMessagesInvoice; ?>" />
    <input type="hidden" id="numberOfMessagesExpense" value="<?=$numberMessagesExpense; ?>" />
	<div id="tabsFinances" class="ui-tabs">
		<ul class="ui-tabs-nav"> 
	        <li class="ui-tabs-selected"><a href="#windowViewLedger"><span><?=lang('finance_ledger'); ?></span></a></li> 
	        <li class="invoiceViewOwn invoiceViewAll hide"><a href="#windowViewInvoicesEstimates"><span><?=lang('finance_invoices_estimates'); ?></span></a></li> 
	        <li><a href="#windowMessagesInvoice"><span><?=lang('common_messages_invoice'); ?></span> <span id="messageCountInvoice" class="tabCount lightGray">(0)</span></a></li>
	        <li><a href="#windowManageVendors"><span><?=lang('common_vendors'); ?></span></a></li>
	        <li><a href="#windowViewFinanceReports"><span><?=lang('widget_reports'); ?></span></a></li>
		</ul> 
	    <div class="windowFrame"> 
	    	<div id="windowViewLedger" class="helptag" data-help="invoices">
		        <div class="barYellow bottom"><div style="padding: 6px;">
		        	<div style="float: left;">
			        	<input type="text" style="width: 300px;" class="search" id="sLedgerAutocompleter" />
			        </div>
                    <div style="float: right;" id="pnlDates">
                        <input type="text" id="ledgerDateStart" name="ledgerDateStart" class="dateField" value="<?=$ledgerDateStart; ?>" style="width: 75px; margin-right: 6px;" />
                        <input type="text" id="ledgerDateEnd" name="ledgerDateEnd" class="dateField" value="<?=$ledgerDateEnd; ?>" style="width: 75px;" />
                    </div>
					<div style="clear: both;"></div>
		        </div></div>
		        <div id="ledgerInformationContainer" class="activityMessageBig"> <?=lang('common_loading'); ?></div>
			</div>
            
            <div id="windowViewInvoicesEstimates" class="ui-tabs-hide helptag" data-help="invoices">
                <div class="barYellow bottom"><div style="padding: 6px;">
		        	<div style="float: left;">
			        	<input type="text" style="width: 300px;" class="search" id="sInvoiceAutocompleter" />
			        	<input type="hidden" id="sInvoiceID" />						
					</div>
                    <div style="float: right;" id="pnlDatesInvoices">
                        <input type="text" id="invoiceDateStart" name="invoiceDateStart" class="dateField" value="<?=$ledgerDateStart; ?>" style="width: 75px; margin-right: 6px;" />
                        <input type="text" id="invoiceDateEnd" name="invoiceDateEnd" class="dateField" value="<?=$ledgerDateEnd; ?>" style="width: 75px;" />
                    </div> 
					<div style="clear: both;"></div>
		        </div></div>
                
		        <div id="invoiceInformationContainer" class="activityMessageBig"> <?=lang('common_loading'); ?></div>
			</div>
            
            <div id="windowMessagesInvoice" class="ui-tabs-hide helptag" data-help="messages">
                <div class="barYellow bottom">
					<div style="padding: 6px;">
						<div id="newMessageInvoiceAll" itemID="0" itemType="invoice" class="messageContainer"></div>
					</div>
				</div>
				<div id="messageListInvoiceAll" style="margin-top: 6px;"></div>
	   		</div>
            
			<div id="windowManageVendors" class="ui-tabs-hide">
		        <div class="barYellow bottom"><div style="padding: 6px;">
		        	<div style="float: left;">
                        <!--
			        	<input type="text" style="width: 300px;" class="search" id="sVendorAutocompleter" />
			        	<div class="loadingInline"></div>
						<input type="hidden" id="sVendorID" />
                        -->
					</div>
					<div style="padding: 6px; float: right;">
						<button class="buttonExpand action" id="buttonNewContact"><span class="buttonDecorator add"><?=lang('finance_add_vendor'); ?></span></button>
						<div style="clear: left;"></div>
					</div>
					<div style="clear: both;"></div>
		        </div></div>
		        <div id="formContactContainer" style="display: none; padding: 6px;">
                    <? $this->load->view('includes/contactForm'); ?>
                </div>
                <div id="contactsContainer" style="padding: 0 0 6px 6px;"></div>
			</div>
	    	<div id="windowViewFinanceReports" class="ui-tabs-hide helptag" data-help="finance-reports" style="background: #fff;">
	        	<div class="barYellow bottom"><div style="padding: 6px;">
                    <div style="float: left;">
                        <select id="selectReportType" style="width: 300px; margin-right: 6px;" class="chart">
                            <option value="repInvoicedReceived"><?=lang('reports_invoiced_received'); ?></option>
                            <option value="repIncomeExpenses"><?=lang('reports_income_expenses'); ?></option>
                            <option value="repInvoicedByProject"><?=lang('reports_invoiced_by_project'); ?></option>
                            <option value="repInvoicedByClient"><?=lang('reports_invoiced_by_client'); ?></option>
                            <option value="repExpensesByProject"><?=lang('reports_expenses_by_project'); ?></option>
                            <option value="repExpensesByClient"><?=lang('reports_expenses_by_client'); ?></option>
                            <option value="repExpensesByCategory"><?=lang('reports_expenses_by_category'); ?></option>
                            <option value="repExpensesByVendor"><?=lang('reports_expenses_by_vendor'); ?></option>
                        </select>
                        <select id="selectReportYear" style="width: 100px;">
                        <?
                        if ($rptMinYearInvoice<date('Y')) {
                            for ($a=date('Y');$a>=$rptMinYearInvoice;$a--) {
                                echo '<option value="'.$a.'">'.$a.'</option>';
                            }
                        } else {
                            echo '<option value="'.$rptMinYearInvoice.'">'.$rptMinYearInvoice.'</option>';
                        }
                        ?>
                        </select>
                    </div>
                    <span class="hide" id="reportDateRangeContainer">
                        <div style="float: left;">
                            <input type="text" id="reportDateStart" name="reportDateStart" class="dateField" style="width: 80px; margin-right: 6px;" />
                            <input type="text" id="reportDateEnd" name="reportDateEnd" class="dateField" style="width: 80px; margin-right: 6px;" />
                        </div>
                        <button class="smallButton" id="submitFinanceReport" style="float: left;" title="<?=lang('common_create_report'); ?>"><span class="chart"><?=lang('common_create_report'); ?></span></button>
                    </span>
                    <div style="clear: left;"></div>
                </div></div>
                <div style="margin: 6px;"><div id="chartContainer"></div></div>
                <div style="margin: 18px 6px 6px 6px;"><div id="gridContainer"></div></div>
	   		</div>
		</div>	    
	</div>
    <div id="itemDetailContainer" class="hide">
        <div id="invoiceControls" class="hide mBottom12 barYellow bottom"><div style="padding: 6px;">
            <div style="float: left;" id="invoiceStatusContainer">
                <p id="invoiceStatus" class="colorBackground"></p>
            </div>
            <div style="float: right;">
                <button class="smallButton buttonEmailMain" id="buttonemail" style="float: left; margin-right: 6px;" title="<?=lang('common_email'); ?>"><span class="email"><?=lang('common_email'); ?></span></button>
                <button class="smallButton hide" id="buttonConvertToInvoice" style="float: left; margin-right: 6px;" title="<?=lang('finance_convert_to_invoice'); ?>"><span class="money"><?=lang('finance_convert_to_invoice'); ?></span></button>
                
                <button class="smallButton" id="buttonViewPayments" style="float: left; margin-right: 6px;" title="<?=lang('finance_payments'); ?>"><span class="money"><?=lang('finance_payments'); ?></span></button>
                <button class="smallButton closeDetails" id="buttonCloseInvoiceDetails" style="float: left;" title="<?=lang('common_list_view_return'); ?>"><span class="listView"><?=lang('common_list_view_return'); ?></span></button>
                <div style="clear: left;"></div>
            </div>
            <div style="clear: both;"></div>
        </div></div>
        <div id="expenseControls" class="hide mBottom12 barYellow bottom"><div style="padding: 6px;">
            <div style="float: left;">
                <p class="colorBackground light-green bold"><?=lang('finance_expense'); ?></p>
            </div>
            <div style="float: right;">
                <button class="smallButton closeDetails" id="buttonCloseExpenseDetails" style="float: left;" title="<?=lang('common_list_view_return'); ?>"><span class="listView"><?=lang('common_list_view_return'); ?></span></button>
            </div>
            <div style="clear: both;"></div>    
        </div></div>
        
        <div class="barBlue bottom hide" id="invoicePaymentsContainer">
            <div style="float: left; width: 50%; padding: 6px;">
                <h2 style="margin-left: 5px;"><?=lang('finance_current_payments'); ?></h2>
                <div id="paymentContainer"></div>
            </div>
            <div style="float: left; width: 46%; padding: 6px;" id="paymentFormContainer">
                <? $this->load->view('includes/payInvoiceForm.php'); ?>
            </div>
            <div style="clear: left;"></div>
        </div>
        
        <!-- Item HTML -->        
        <div id="itemDetailContents"></div>
        <div id="fileUploadingContainer" class="dragDropUploadContainer">
            <div id="dropFilesHere" class="fileUploadBox"><?=lang('common_drop_files_here'); ?></div>
        </div>
        <div id="invFile_fileContainer"></div>
        <div style="clear: both;"></div>
        <div style="margin-top: 12px;" class="barBlue bottom">
            <div style="padding: 6px;">
                <div id="newMessageContainer" itemID="" itemType="" class="messageContainer"></div>
            </div>
        </div>
        <div id="itemMessageConents" class="mTop12"></div>
    </div>
	<? $this->load->view('includes/emailInvoiceForm.php'); ?>    
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>