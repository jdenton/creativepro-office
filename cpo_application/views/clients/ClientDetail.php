<? $this->load->view('includes/header'); ?>
<div id="pageLeftColumn" pageID="viewClientDetails" class="helptag" data-help="clients">
	<input type="hidden" id="clientID" value="<?=$clientID; ?>" />
    <input type="hidden" id="clientUserid" value="<?=$clientUserid; ?>" />
    <input type="hidden" id="contactType" value="C" />
	<input type="hidden" id="googleMapAddress" value="<?=$clientInformation['googleLocation']; ?>" />
	<script type="text/javascript" language="javascript">
	/* <![CDATA[ */
		var clientData = '<?=$clientInformation; ?>';
	/* ]]> */	
	</script>
	<div id="tabsClient" class="ui-tabs">
		<ul class="ui-tabs-nav"> 
	        <li class="ui-tabs-selected" id="tabClientDetails"><a href="#windowClientDetails"><span><?=lang('client_details'); ?></span></a></li> 
	        <li><a href="#windowProjects" id="tabClientProjects"><span><?=lang('project_page_title'); ?></span> <span id="projectCount" class="lightGray">(0)</span></a></li> 
	        <li><a href="#windowInvoices" id="tabClientInvoices"><span><?=lang('finance_invoices'); ?></span> <span id="invoiceCount" class="lightGray">(0)</span></a></li> 
	        <li><a href="#windowMessages" id="tabClientMessages"><span><?=lang('common_messages'); ?></span> <span class="lightGray">(0)</span></a></li> 
	    </ul>
	    <div style="min-height: 200px;" class="windowFrame"> 
	    	<div id="windowClientDetails">
	    		<div style="padding: 6px;" id="clientInformationContainer"></div>
		        <!-- <div id="clientMap" class="activityMessageBig"> <?=lang('common_loading_map'); ?></div> -->
                  <hr />

                  <? if ($permissions['contactView'] == 1) { ?>

                  <div id="clientContactPanel" style="padding: 12px;">
                      <div style="float: left; margin-right: 6px;"><?=help('help_client_team_contacts','',''); ?></div>
                      <h2 style="float: left;"><?=lang('client_client_contacts'); ?></h2>
                      <div style="clear: left;"></div>
                      
                      <div style="margin-top: 12px;">
                        <div style="margin: 2px 24px 0 0; float: left;">

                            <? if ($permissions['clientUpdate'] == 1) { ?>
                            <div id="contactSearchContainer">
                                <input type="text" style="width: 400px;" data-defaulttext="<?=lang('common_search_contacts'); ?>" class="autocompleteContacts" id="sContactsAutocompleter" /><br />
                            </div>
                            <? } ?>

                        </div>

                        <? if ($permissions['contactCreate'] == 1) { ?>
                            <button class="buttonExpand action" id="buttonNewContact"><span class="add"><?=lang('contact_new'); ?></span></button>
                        <? } ?>

                        <div style="clear: both;"></div>
                    </div>
                       <div style="clear: both;"></div>
					   <div id="formContactContainer" style="display: none;">
					   	<? $this->load->view('includes/contactForm'); ?>	
					   </div>

                       <? if ($permissions['contactCreate'] == 1) { ?>
                        <p id="noContactLinkContainer" style="display: none;"><?=lang('contact_no_contacts'); ?> <a href="#" id="linkNewContact"><?=lang('contact_create'); ?></a></p>
					   <? } ?>
                       <div id="contactsContainer" itemType="C" itemID="<?=$clientID; ?>"></div>
					   <div style="clear: left;"></div>                       
				  </div>

                  <? } ?>

			</div>    
			<div id="windowProjects" class="ui-tabs-hide" class="helptag" data-help="projects">
		    	<div id="projectInformationContainer"></div>
		    </div> 
		    <div id="windowInvoices" class="ui-tabs-hide" class="helptag" data-help="invoices">
		    	<div id="invoiceInformationContainer"></div>
		    </div>     
	    	<div id="windowMessages" class="ui-tabs-hide" class="helptag" data-help="messages">
                <div class="barYellow bottom">
					<div style="padding: 6px;">
						<div id="newMessage" itemID="<?=$clientID; ?>" itemType="client" class="messageContainer"></div>
					</div>
				</div>
				<div id="messageListClient" style="margin-top: 6px;"></div>
                <div id="messageMoreContainer" class="boxBlue" style="display: none; margin-top: 6px;"><a href="#" class="linkMoreMessages"><?=lang('common_more'); ?></a></div>
	   		</div>
		    <div id="windowAlerts" class="ui-tabs-hide"> 
		    	<div style="padding: 6px;">
		    		Alerts will include: 1. Project or tasks overdue, 2. Invoice payment overdue.
		    	</div>
		    </div>
        </div>	    
	</div>
</div>
<? $this->load->view('includes/rightColumn'); ?>
<? $this->load->view('includes/footer'); ?>