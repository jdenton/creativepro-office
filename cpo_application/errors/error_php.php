<?php
/*
 * This is a bit of a hack because it changes the URL in the
 * browser, potentially confusing the user.  We'll go with it
 * for now because alternatives are just not working.
 * WE HAVE NO CI CONTEXT HERE!!!!!
 * 01/14/2010
 */

/*
 * Send email to admin
 */
/*
$headers = 'From: support@creativeprooffice.com'."\r\n".
           'Reply-To: support@creativeprooffice.com'."\r\n".
           'X-Mailer: PHP/'.phpversion();

$messageEmail  = "Severity: $severity".Chr(10).Chr(10);
$messageEmail .= "Message: $message".Chr(10).Chr(10);
$messageEmail .= "Filename: $filepath".Chr(10).Chr(10);
$messageEmail .= "Line Number: $line".Chr(10).Chr(10);

if ($severity != 'Notice') {
    mail('support@CreativeProOffice.com','A PHP Error was encountered',$messageEmail,$headers);
}

header("HTTP/1.1 404 Not Found");
header("Location: http://www.mycpohq.com/error/php");
exit;
 * 
 */
?>
<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: <?php echo $severity; ?></p>
<p>Message:  <?php echo $message; ?></p>
<p>Filename: <?php echo $filepath; ?></p>
<p>Line Number: <?php echo $line; ?></p>

</div>