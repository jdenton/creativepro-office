<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Finance_view_expenses extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    /**
	 * checkForValidExpense : Make sure that user has view rights for this expense.
     *                        This prevents users from seeing other users expenses by
     *                        simply typing a random invoiceID in the URL.
	 *
	 * @access public
	 * @param  int     $expenseID Expense ID of expense record we're wanting to verify.
	 * @return boolean true if record can be viewed by this user
	 */
    function checkForValidExpense($expenseID,$accountUserid) {
          $sql = "SELECT ExpenseID
				  FROM cpo_expense
				  WHERE
					ExpenseID = '$expenseID' AND
					Userid = '".$accountUserid."'";
          $query = $this->db->query($sql);
          if ($query->num_rows()<1) {
               return false;
          }
          return true;
     }

     function getExpenseList($itemID,$group=NULL,$tag=NULL,$status=NULL,$filterArray=NULL) {
		$sql = "SELECT 
                    E.ExpenseID,E.ProjectID,E.ClientID,E.VendorID,E.Title,E.DateExpense,E.PaymentMethod,
                    E.Reimbursement,E.Amount,E.Markup,E.MarkupType,E.Comments,E.Tags,E.Recurring,E.UseridEntry,
                    E.UseridUpdate,E.DateEntry,E.DateUpdate,MC.MainCat,
                    I.InvoiceID,I.InvNo AS InvoiceNo,I.DateEntry AS InvoiceDate
                FROM cpo_expense E
                LEFT JOIN cpo_main_cat MC ON MC.CatID = E.CatID
                LEFT JOIN cpo_invoice I ON I.InvoiceID = E.BilledInvoiceID
				WHERE ";
    	switch ($group) {
			case 'user':
			    $sql .= "E.Userid = '$itemID'";
			    break;
			case 'client':
			    $sql .= "E.ClientID = '$itemID' ";
			    break;
			case 'project':
			    $sql .= "E.ProjectID = '$itemID'";
			    break;
            case 'vendor':
			    $sql .= "E.VendorID = '$itemID'";
			    break;
			case 'tag':
			    $sql .= "(E.Tags LIKE '$itemID%' OR E.Tags LIKE '% $itemID%')";
			    break;
            case 'category':
			    $sql .= "E.CatID = '$itemID'";
			    break;
            case 'expense':
			    $sql .= "E.ExpenseID = '$itemID'";
			    break;
			default:
				$sql .= "E.Userid = '$itemID'";
			    break;
		}

		if (!empty($tag)) {
			$sql .= " AND (E.Tags LIKE '$tag%' OR E.Tags LIKE '% $tag%') ";
		}

        if ($status != 'all' && !isset($filterArray)) {
            //$sql .= " AND I.Status = $status ";
        }

        /*
         * Look at filterArray
         */
        if (isset($filterArray)) {            
            if (!empty($filterArray['dateStart'])) {
                $sql .= " AND E.DateExpense >= '".$filterArray['dateStart']."' ";
            }
            if (!empty($filterArray['dateEnd'])) {
                $sql .= " AND E.DateExpense <= '".$filterArray['dateEnd']."' ";
            }
        }

		$sql .= " AND E.Deleted = 0 ";
		$sql .= " ORDER BY E.DateExpense DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
    }
    
    function getExpensesForLedger($dateStart=null,$dateEnd=null,$itemID=null,$group=NULL,$tag=NULL,$term=null,$accountUserid=null) {
        $sql = "SELECT 
                    E.ExpenseID,E.ProjectID,E.ClientID,E.VendorID,E.Title,E.DateExpense,E.PaymentMethod,
                    E.Reimbursement,E.Amount,E.Markup,E.MarkupType,E.Comments,E.Tags,E.Recurring,E.UseridEntry,
                    E.UseridUpdate,E.DateEntry,E.DateUpdate,MC.MainCat,
                    C.ClientID,C.Company,
                    P.Title AS Project,
                    PE.NameFirst AS VendorNameFirst,PE.NameLast AS VendorNameLast,PE.Company AS VendorCompany
                FROM cpo_expense E
                LEFT JOIN cpo_main_cat MC ON MC.CatID = E.CatID
                LEFT JOIN cpo_clients C ON C.ClientID = E.ClientID
                LEFT JOIN cpo_project P ON P.ProjectID = E.ProjectID
                LEFT JOIN cpo_people PE ON PE.PID = E.VendorID
				WHERE ";
        $sql .= " E.Userid = '".$accountUserid."' AND E.Deleted = 0 ";
        
        if (!empty($itemID)) {
            switch ($group) {
                case 'project':
                    $sql .= " AND P.ProjectID = $itemID ";
                    break;
                case 'client':
                    $sql .= " AND C.ClientID = $itemID ";
                    break;
                case 'user':
                    $sql .= " AND E.UseridEntry = $itemID ";
                    break;
            }
        } elseif (!empty($term)) {        
            $sql .= " AND (
                        E.Title LIKE '$term%' OR E.Title LIKE '% $term%' OR
                        E.Comments LIKE '$term%' OR E.Comments LIKE '% $term%' OR
                        PE.Company LIKE '$term%' OR PE.Company LIKE '% $term%' OR
                        C.Company LIKE '$term%' OR C.Company LIKE '% $term%'
                    ) ";
        }
        
        if (!empty($tag)) {
            $sql .= " AND (E.Tags LIKE '$tag%' OR E.Tags LIKE '% $tag%') ";
        }
                
        if (!empty($dateStart) && !empty($dateEnd)) {
            $sql .= " AND E.DateExpense >= '$dateStart' AND E.DateExpense <= '$dateEnd' ";
        }
        
		$sql .= " ORDER BY E.DateExpense DESC";
		$query = $this->db->query($sql);
        return $query->result_array();
    }

    function getExpensesForExport($accountUserid) {
        $sql = "SELECT
                    E.ExpenseID,E.ProjectID,E.ClientID,E.VendorID,E.Title,E.DateExpense,E.PaymentMethod,
                    E.Reimbursement,E.Amount,E.AccountNumber,E.Markup,E.MarkupType,E.Comments,E.Tags,E.Recurring,E.UseridEntry,
                    E.UseridUpdate,E.DateEntry,E.DateUpdate,MC.MainCat,
                    P.Title AS ProjectTitle,C.Company,
                    PP.NameFirst,PP.NameLast,PP.Company AS VendorCompany
                FROM cpo_expense E
                LEFT JOIN cpo_main_cat MC ON MC.CatID = E.CatID
                LEFT JOIN cpo_clients C ON C.ClientID = E.ClientID
                LEFT JOIN cpo_project P ON P.ProjectID = E.ProjectID
                LEFT JOIN cpo_people PP ON PP.PID = E.VendorID
				WHERE
                    E.Userid = '$accountUserid'
                ORDER BY E.DateEntry DESC";
        $query = $this->db->query($sql);
		return $query->result_array();
    }

     function getExpenseDetails($expenseID) {
         $sql = "SELECT 
                    E.ExpenseID,E.Userid,E.ProjectID,E.ClientID,E.CatID,E.VendorID,E.Title,E.DateExpense,E.PaymentMethod,E.Reimbursement,
                    E.Amount,E.AccountNumber,E.Markup,E.MarkupType,E.Comments,E.Tags,E.Recurring,
                    E.UseridEntry,E.UseridUpdate,
                    C.ClientUserid,C.Company,C.Address,C.City,C.State,C.Zip,C.Country,C.Phone1,C.Email,C.URL,C.Language,
                    P.ProjectID,P.Title AS ProjectTitle,
                    Cat.MainCat AS Category
                 FROM cpo_expense E
                 LEFT JOIN cpo_clients C ON C.ClientID = E.ClientID
                 LEFT JOIN cpo_project P ON P.ProjectID = E.ProjectID
                 LEFT JOIN cpo_main_cat Cat ON Cat.CatID = E.CatID
                 WHERE E.ExpenseID = '$expenseID'";
         $query = $this->db->query($sql);
         return $query->result_array();
     }

     function getRecurringExpenses() {
        $sql = "SELECT * from cpo_expense E
                LEFT JOIN cpo_account_profile A ON A.AccountID = E.Userid
                WHERE
                    E.Recurring IS NOT NULL AND
                    E.Recurring <> '0' AND
                    E.Recurring <> '' AND
                    A.IsActive = 1 AND
                    E.Deleted = 0";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function getTotalExpensesForProject($projectID) {
        $sql = "SELECT SUM(Amount) AS Total from cpo_expense WHERE ProjectID = $projectID";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

     /**
	 * getExpensesForAutocompleter : get expense items for autocomplete searchbox
	 *
	 * @access	public
	 * @param   string $q search term
	 * @return	string pipe delimited string of expenses
	 */
    function getExpensesForAutoCompleter($q,$accountUserid) {
		$expensesArray = '';
        $a=0;
		/*
		 * First let's look for matching expenses
		 */
    	$sql = "SELECT E.ExpenseID,E.Title
				FROM cpo_expense E
				WHERE
				(
					E.Title LIKE '$q%' OR E.Title LIKE '% $q%' OR
					E.Comments LIKE '$q%' OR E.Comments LIKE '% $q%'
				)
				AND E.Userid = '".$accountUserid."'
				AND E.Deleted = 0
				ORDER BY E.Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                $expensesArray[$a]['id'] = $row['ExpenseID'];
                $expensesArray[$a]['label'] = $row['Title'];
                $expensesArray[$a]['count'] = '';
                $expensesArray[$a]['type']  = 'expense';
                $expensesArray[$a]['icon']  = 'icon_expense_small';
                $a++;
            }
		}

		/*
		 * Now, let's look for matching tags
		 */
        
        
		$sql = "SELECT T.Tag,COUNT(E.ExpenseID) AS Expenses
				FROM cpo_expense E
				LEFT JOIN cpo_tags T ON (T.Tag LIKE '$q%' OR T.Tag LIKE '% $q%')
				WHERE
				(E.Tags LIKE '$q%' OR E.Tags LIKE '% $q%')
				AND T.SiteArea = 'expense'
				AND E.Userid = '".$accountUserid."'
				AND E.Deleted = 0
				ORDER BY T.Tag";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
                if ($row['Expenses']>0) {
                    $expensesArray[$a]['id'] = $row['Tag'];
                    $expensesArray[$a]['label'] = $row['Tag'];
                    $expensesArray[$a]['count'] = $row['Expenses'];
                    $expensesArray[$a]['type']  = 'tag';
                    $expensesArray[$a]['icon']  = 'icon_tag tag';
                    $a++;
                }  
			}
		}

		/*
		 * Now, let's look for any project titles that match
		 */
		 $sql = "SELECT P.ProjectID,P.Title,COUNT(E.ExpenseID) AS Expenses
				FROM cpo_project P
				LEFT JOIN cpo_expense E ON E.ProjectID = P.ProjectID
				WHERE
				(
					P.Title LIKE '$q%' OR P.Title LIKE '% $q%'
				)
				AND P.Userid = '".$accountUserid."'
				AND E.Deleted = 0
				ORDER BY P.Title";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				$expenseDisplay = $row['Title'];
				if ($row['Expenses']>0) {
                    $expensesArray[$a]['id'] = $row['ProjectID'];
                    $expensesArray[$a]['label'] = $row['Title'];
                    $expensesArray[$a]['count'] = $row['Expenses'];
                    $expensesArray[$a]['type']  = 'project';
                    $expensesArray[$a]['icon']  = 'icon_project_small';
                    $a++;
				}
			}
		}

		/*
		 * Now, let's look for any clients that match
		 */
		 $sql = "SELECT C.ClientID,C.Company,COUNT(E.ExpenseID) AS Expenses
				FROM cpo_clients C
				LEFT JOIN cpo_expense E ON E.ClientID = C.ClientID
				WHERE
				(
					C.Company LIKE '$q%' OR C.Company LIKE '% $q%'
				)
				AND C.Userid = '".$accountUserid."'
				AND E.Deleted = 0
				ORDER BY C.Company";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				if ($row['Expenses']>0) {
                    $expensesArray[$a]['id'] = $row['ClientID'];
                    $expensesArray[$a]['label'] = $row['Company'];
                    $expensesArray[$a]['count'] = $row['Expenses'];
                    $expensesArray[$a]['type']  = 'client';
                    $expensesArray[$a]['icon']  = 'icon_client_small';
                    $a++;
                }
			}
		}

        /*
		 * Now, let's look for any vendors that match
		 */
		 $sql = "SELECT P.PID,P.Company,COUNT(E.ExpenseID) AS Expenses
				FROM cpo_people P
				LEFT JOIN cpo_expense E ON E.VendorID = P.PID
				WHERE
				(
					P.Company LIKE '$q%' OR P.Company LIKE '% $q%' OR
                    P.NameFirst LIKE '$q%' OR P.NameFirst LIKE '% $q%' OR
                    P.NameLast LIKE '$q%' OR P.NameLast LIKE '% $q%'
				)
				AND P.Userid = '".$accountUserid."'
				AND E.Deleted = 0
				ORDER BY P.Company";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				$expenseDisplay = $row['Company'];
				if ($row['Expenses']>0) {
                    $expensesArray[$a]['id'] = $row['PID'];
                    $expensesArray[$a]['label'] = $row['Company'];
                    $expensesArray[$a]['count'] = $row['Expenses'];
                    $expensesArray[$a]['type']  = 'vendor';
                    $expensesArray[$a]['icon']  = 'icon_team_small';
                    $a++;
                }
			}
		}

        /*
		 * Now, let's look for any categories that match
		 */
		 $sql = "SELECT C.CatID,C.MainCat,COUNT(E.ExpenseID) AS Expenses
				FROM cpo_main_cat C
				LEFT JOIN cpo_expense E ON E.CatID = C.CatID
				WHERE
				(
					C.MainCat LIKE '$q%' OR C.MainCat LIKE '% $q%'
				)
				AND C.Userid = '".$accountUserid."'
                AND C.SiteArea = 'expense'
				AND E.Deleted = 0
				ORDER BY C.MainCat";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			foreach ($query->result_array() as $row) {
				$expenseDisplay = $row['MainCat'];
				if ($row['Expenses']>0) {
                    $expensesArray[$a]['id'] = $row['CatID'];
                    $expensesArray[$a]['label'] = $row['MainCat'];
                    $expensesArray[$a]['count'] = $row['Expenses'];
                    $expensesArray[$a]['type']  = 'category';
                    $expensesArray[$a]['icon']  = 'icon_category_small';
                    $a++;
                }
			}
		}
		return json_encode($expensesArray);
    }
}
?>