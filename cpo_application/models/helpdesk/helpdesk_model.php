<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Helpdesk_model extends Model {
    function __construct() {
        // Call the Model constructor
        parent::Model();
        $this->load->model('blog/Blog_model','',true);
    }

    function getHelpdeskPosts($count=NULL,$month=NULL,$year=NULL,$tag=NULL,$active=NULL) {
        $limit = NULL;
        $searchByTagSQL  = NULL;
        $searchByDateSQL = NULL;
        $activeSQL    = " (BE.Active = 1 OR BE.Active = 0) ";
        if ($count != NULL) {
            $limit = " LIMIT $count ";
        }

        if (!empty($tag)) {
            $searchByTagSQL = " AND (BE.Tags LIKE '% $tag%' OR BE.Tags LIKE '$tag%') ";
            $limit = NULL;
        }

        if (!empty($month)) {
            $searchByDateSQL = " AND MONTHNAME(BE.DateEntry) = '$month' AND YEAR(BE.DateEntry) = '$year' ";
            $limit = NULL;
        }

        if (!empty($active)) {
            $activeSQL = " BE.Active = $active ";
        }

        $sql  = "SELECT BE.BEID,BE.Userid,BE.Title,BE.BlogEntry,BE.DateEntry,BE.Tags,BE.Active,BE.EntryURL,BE.Views,
                P.NameFirst,P.NameLast,P.AvatarURL,
                (SELECT COUNT(CID) FROM helpdeskComments BC WHERE BC.BEID = BE.BEID) AS commentCount
                FROM helpdeskEntry BE
                LEFT JOIN cpo_people P ON P.LoginUserid = BE.Userid
                WHERE
                    $activeSQL
                    $searchByTagSQL
                    $searchByDateSQL
                AND BE.Deleted = 0
                ORDER BY BE.DateEntry DESC $limit";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getHelpdeskPost($title,$beID=NULL) {
        $sql = "SELECT BE.BEID,BE.Userid,BE.Title,BE.BlogEntry,BE.DateEntry,BE.Tags,BE.Active,BE.EntryURL,BE.Views,
                P.NameFirst,P.NameLast,P.AvatarURL
                FROM helpdeskEntry BE
                LEFT JOIN cpo_people P ON P.LoginUserid = BE.Userid";

        if ($beID>0) {
            $sql .= " WHERE BEID = '$beID'";
        } else {
            $sql .= " WHERE EntryURL = '$title'";
        }
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getLatestEntries($count=10) {
        $sql = "SELECT BEID,Title,EntryURL FROM helpdeskEntry
                WHERE Active = 1
                ORDER BY DateEntry DESC LIMIT $count";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getHelpdeskPostComments($beID) {
        $sql = "SELECT C.CID,C.PID,C.UserName,C.UserEmail,C.UserURL,C.Comment,C.DateComment,C.Admin,P.AvatarURL
                FROM helpdeskComments C
                LEFT JOIN cpo_people P ON P.PID = C.PID
                WHERE C.BEID = '$beID'
                ORDER BY C.DateComment ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getHelpdeskPostComment($cID) {
        $sql = "SELECT C.CID,C.PID,C.UserName,C.UserEmail,C.UserURL,C.Comment,C.DateComment,C.Admin,P.AvatarURL
                FROM helpdeskComments C
                LEFT JOIN cpo_people P ON P.PID = C.PID
                WHERE C.CID = '$cID'
                ORDER BY C.DateComment ASC";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function saveHelpdeskPostComment($commentArray) {
        $sql = "INSERT into helpdeskComments (
                PID,
                UserName,
                UserEmail,
                UserURL,
                Comment,
                DateComment,
                BEID,
                Admin
                ) values (
                '".$commentArray['PID']."',
                '".$commentArray['commentName']."',
                '".$commentArray['commentEmail']."',
                '".$commentArray['commentURL']."',
                '".$commentArray['commentText']."',
                '".date('Y-m-d G:i:s')."',
                '".$commentArray['BEID']."',
                '".$commentArray['Admin']."'
                )";
        $query = $this->db->query($sql);
        return $this->db->insert_id();
    }

    function saveHelpdeskPost($postArray) {
        if ($postArray['BEID']>0) {
            $beID = $postArray['BEID'];

            $sql = "UPDATE helpdeskEntry SET
                    Title        = '".$postArray['Title']."',
                    Tags         = '".$postArray['Tags']."',
                    BlogEntry    = ".$postArray['Content'].",
                    EntryURL     = '".$postArray['EntryURL']."',
                    DateUpdate   = '".date('Y-m-d G:i:s')."',
                    UseridUpdate = '".$postArray['AuthorUserid']."'
                    WHERE
                    BEID = '$beID'";
            $query = $this->db->query($sql);
        } else {
            $sql = "INSERT into helpdeskEntry (
                    Userid,
                    Title,
                    BlogEntry,
                    DateEntry,
                    Tags,
                    Active,
                    EntryURL,
                    DateUpdate,
                    UseridUpdate
                    ) values (
                    '".$postArray['AuthorUserid']."',
                    '".$postArray['Title']."',
                    ".$postArray['Content'].",
                    '".date('Y-m-d G:i:s')."',
                    '".$postArray['Tags']."',
                    '0',
                    '".$postArray['EntryURL']."',
                    '".date('Y-m-d G:i:s')."',
                    '".$postArray['AuthorUserid']."'
                    )";
            $query = $this->db->query($sql);
            $beID = $this->db->insert_id();
        }

        return $beID;
    }

    function updatePostStatus($beID,$newStatus) {
        if ($newStatus == 1) {
            $sql = "UPDATE helpdeskEntry SET
                    DateEntry = '".date('Y-m-d G:i:s')."',
                    Active = '$newStatus'
                WHERE BEID = '$beID'";
        } else {
            $sql = "UPDATE helpdeskEntry SET Active = '$newStatus' WHERE BEID = '$beID'";
        }
        $query = $this->db->query($sql);
    }

    function saveHelpdeskTag($tagID=null,$tag,$action) {
		if ($action == 'add') {
			$sql = "INSERT into helpdeskTags (
					TagName,
					TagURL
					) values (
					'$tag',
					'$tag'
					)";
			$query = $this->db->query($sql);
			$tagID = $this->db->insert_id();
		} elseif ($action == 'edit') {
			$sql = "UPDATE helpdeskTags SET
					Tag    = '$tag',
                    TagURL = '$tag'
					WHERE
					TID = $tagID";
			$query = $this->db->query($sql);
		}
		return $tagID;
    }

    function deleteHelpdeskTag($tagID) {
		$sql = "DELETE FROM helpdeskTags
				WHERE
				TID = $tagID";
		$query = $this->db->query($sql);
    }

    function getHelpdeskTag($tag) {
        $sql = "SELECT * FROM helpdeskTags
                WHERE
                TagName = '$tag'";
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
    }

    function getHelpdeskTags($withCount=NULL) {
        if ($withCount == TRUE) {
            $sql = "SELECT T.TID,T.TagName,T.TagURL,
                    (SELECT COUNT(BEID) FROM helpdeskEntry BE WHERE BE.Tags LIKE concat('%',T.TagName,'%') AND BE.Deleted = 0) AS tagCount
                    FROM helpdeskTags T
                    ORDER BY TagName ASC";
        } else {
            $sql = "SELECT * from helpdeskTags ORDER BY TagName ASC";
        }
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function deleteHelpdeskPost($beID) {
         $sql = "DELETE from helpdeskEntry WHERE BEID = '$beID'";
         $query = $this->db->query($sql);
    }

    function incrementEntryViews($beID) {
        $sql = "UPDATE helpdeskEntry SET Views = Views+1 WHERE BEID = '$beID'";
        $query = $this->db->query($sql);
    }

    function getHelpdeskEntries($topics) {
        $topicsArray = explode(' ',$topics);

        $sql = "SELECT BEID,Title,Tags,EntryURL from helpdeskEntry WHERE ";

        foreach($topicsArray as $topic) {
            $sql .= "Tags LIKE '% $topic%' OR Tags LIKE '$topic%' OR ";
        }

        $sql = substr($sql,0,-3);
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getHelpdeskEntry($beID=NULL,$entryURL=NULL) {
        if ($beID > 0) {
            $sql = "SELECT * from helpdeskEntry WHERE BEID = '$beID'";
        } elseif ($entryURL != '0') {
            $sql = "SELECT * from helpdeskEntry WHERE EntryURL = '$entryURL'";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
    }
    
    function searchHelpdeskPosts($searchTerm) {
        $sql  = "SELECT BE.BEID,BE.Userid,BE.Title,BE.BlogEntry,BE.DateEntry,BE.Tags,BE.Active,BE.EntryURL,BE.Views,
                P.NameFirst,P.NameLast,P.AvatarURL,
                (SELECT COUNT(CID) FROM helpdeskComments BC WHERE BC.BEID = BE.BEID) AS commentCount
                FROM helpdeskEntry BE
                LEFT JOIN cpo_people P ON P.LoginUserid = BE.Userid
                WHERE
                    (
                    BE.Title LIKE '% $searchTerm%' OR 
                    BE.Title LIKE '$searchTerm%' OR
                    BE.BlogEntry LIKE '% $searchTerm%' OR 
                    BE.BlogEntry LIKE '$searchTerm%'
                    )
                AND BE.Deleted = 0
                AND BE.Active = 1
                ORDER BY BE.DateEntry DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getHelpdeskEntriesForAutoCompleter($q) {
    	$sql = "SELECT BEID,Title
				FROM helpdeskEntry
				WHERE
				(
                    Title LIKE '% $q%' OR Title LIKE '$q%' OR
					Tags LIKE '% $q%' OR Tags LIKE '$q%' OR
					BlogEntry LIKE '%$q%' OR BlogEntry LIKE '% $q%'
				)
				ORDER BY Title";
		$query = $this->db->query($sql);
		$entriesArray = array();
        $a=0;
		foreach ($query->result_array() as $row) {
            $entriesArray[$a]['value'] = $row['Title'];
            $entriesArray[$a]['id'] = $row['BEID'];
            $a++;
		}
        
        /*
         * Get stuff from the blog
         */
        $blogArray = $this->Blog_model->getBlogPosts(null,null,null,'Help');
        foreach ($blogArray as $row) {
            $entriesArray[$a]['value'] = 'From the blog: '.$row['Title'];
            $entriesArray[$a]['id'] = $row['BEID'];
            $a++;
		}
        
		return json_encode($entriesArray);
    }
}
?>