<?php
class Rates extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    /**
	 * getRate : Gets the hourly rate for an itemType given an itemID.
     *           Defaults to shop rate if nothing is found for given itemType and itemID
     *           or if no itemType or itemID is given.
     *           itemType can be client,project,shop,vendor,service
	 *
	 * @access	public
	 * @param	string $itemType
     * @param   int    $itemID
     * @param   int    $userID
	 * @return	float  $rate 
	 */
    function getRate($itemType=NULL,$itemID=NULL,$userid=NULL) {
        $rate = 0;
        if (empty($itemType)) {
            $getShopRate = 1;
        } else {
            /*
             * Attempt to get rate from itemType and itemID
             */
            $sql = "SELECT * from cpo_rates
                    WHERE
                    Userid = '$userid'
                    AND Item = '$itemType'
                    AND ItemID = '$itemID'";
            $query = $this->db->query($sql);
            $rateRows = $query->num_rows();
            if ($rateRows>0) {
                $rateRow = $query->row_array();
            }
        }
        if ($rateRows<1 || $getShopRate == 1) {
            $sql = "SELECT * from cpo_rates
                    WHERE
                    Userid = '$userid'
                    AND Item = 'shop'";
            $query = $this->db->query($sql);
            $rateRows = $query->num_rows();
        }
        return $rate;
    }
}
?>