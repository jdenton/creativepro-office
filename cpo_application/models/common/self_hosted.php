<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Self_hosted extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function checkInstall($orderNumber,$installKey) {
        $sql = "SELECT * from cpo_account_profile_product
                WHERE
                OrderNumber = '$orderNumber' AND
                InstallationKey = '$installKey'";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }

    function updateInstallRecord($data) {
        $sql = "UPDATE cpo_account_profile_product SET
                    NumberOfLicensesInstalled = '".$data['NumberOfLicensesInstalled']."',
                    InstallationIPAddress     = '".$data['IPAddresses']."',
                    DateInstall               = '".date('Y-m-d')."'
                WHERE
                OrderNumber = '".$data['orderNumber']."' AND
                InstallationKey = '".$data['installKey']."'";
        $query = $this->db->query($sql);
    }

    function checkForUpdates($versionNumber) {
        $sql = "SELECT * from cpo_product_updates WHERE VersionNumber > '$versionNumber' LIMIT 1";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }

    function checkVersion() {
        $sql = "SELECT * from cpo_product_updates ORDER BY UpdateID DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getUpdates($getFullVersion=false) {
        if ($getFullVersion == true) {
            $sql = "SELECT * from cpo_product_updates WHERE FullVersion = 1";
            $query = $this->db->query($sql);
            return $query->row_array();
        } else {
            $sql = "SELECT * from cpo_product_updates WHERE FullVersion = 0 ORDER BY ReleaseDate DESC";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }
}
?>