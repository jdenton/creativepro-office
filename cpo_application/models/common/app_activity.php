<?php
class App_activity extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

	function logAppActivity($data) {
		$sql = "INSERT into cpo_activity (
                Userid,		
                UserPID,
                ProjectID,
                UserType,
				Activity,
				ItemID,
				ItemType,
                ItemTitle,
                DateActivity,
                ReportToUser
				) values (
                '".$data['AccountUserid']."',
                '".$data['UserPID']."',
                '".$data['ProjectID']."',    
                '".$data['UserType']."',
				'".$data['Activity']."',
				'".$data['ItemID']."',
				'".$data['ItemType']."',
				'".$data['ItemTitle']."',
				'".date('Y-m-d G:i:s')."',
                '".$data['ReportToUser']."'    
				)";
		$query = $this->db->query($sql);
	}

	/**
	* getAppActivity 
	*/
	function getAppActivity($accountUserid,$projectID=0,$fromTime=null,$start=null,$limit=null) {
        if ($start == null) {
            $start = 0;
        }
        if ($limit == null) {
            $limit = 30;
        }
        $projectWhere = '';
        if ($projectID > 0) {
            $projectWhere = " A.ProjectID = '".$projectID."' AND ";
        }
        
        $appTimeWhere = '';
        if ($fromTime != null) {
            $appTimeWhere = " UNIX_TIMESTAMP(A.DateActivity) >= $fromTime AND ";
        }
        $sql = "SELECT 
                    A.ActivityID,A.Userid,A.UserPID,A.UserType,A.Activity,A.ItemID,A.ItemType,A.ItemTitle,A.DateActivity,
                    UNIX_TIMESTAMP(A.DateActivity) AS Timestamp,
                    P.NameFirst,P.NameLast,P.AvatarURL
                    from cpo_activity A
                LEFT JOIN cpo_people P ON P.PID = A.UserPID
                WHERE 
                    $projectWhere
                    $appTimeWhere    
                    A.Userid = ".$accountUserid." AND
                    A.ReportToUser = 1
                ORDER BY DateActivity DESC
                LIMIT $start,$limit";
        $query = $this->db->query($sql);
        return $query->result_array();
	}
}
?>