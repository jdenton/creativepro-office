<?php
class Calendar_view extends Model {

    function Calendar_view() {
        // Call the Model constructor
        parent::Model();
    }

	function getCalendars($userid,$accountUserid,$viewAll=FALSE) {
        if ($viewAll == TRUE) {
            $where = " C.Userid = '$accountUserid' ";
        } else {
            $where = " C.UseridEntry = '$userid' ";
        }
		$sql = "SELECT 
					C.CalendarID,C.Title,C.Color,C.DefaultCalendar,
					LCM.MemberID,P.NameFirst,P.NameLast,P.Language
				FROM cpo_calendar C
				LEFT JOIN cpo_linkMemberCalendar LCM ON LCM.CalendarID = C.CalendarID
                LEFT JOIN cpo_people P ON P.PID = LCM.MemberID
				WHERE 
                $where AND
                C.Deleted = 0
				ORDER BY Title";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function getCalendarDetail($calendarID) {
		$sql = "SELECT
					C.CalendarID,C.Title,C.Color,
					LCM.MemberID
				FROM cpo_calendar C
				LEFT JOIN cpo_linkMemberCalendar LCM ON LCM.CalendarID = C.CalendarID
				WHERE C.CalendarID = '$calendarID'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function getCalendarDetailsFromType($calendarType,$accountUserid) {
        $sql = "SELECT * from cpo_calendar
                WHERE
                    CalendarType = '$calendarType' AND
                    Userid = '$accountUserid'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function getEventsForCalendar($dateStart,$dateEnd=NULL,$accountUserid=NULL,$userid=NULL,$pID=NULL,$clientID=NULL,$viewAll=FALSE,$viewOwn=FALSE) {
        if (!empty($dateEnd)) {
            $whereDateFilter = " ((E.DateStart >= '$dateStart' AND E.DateStart <= '$dateEnd') OR (E.DateEnd <= '$dateEnd' AND E.DateEnd >= '$dateStart')) ";
        } else {
            $whereDateFilter  = " E.DateStart = '$dateStart' ";
        }

        /*
         * If we're a client, we should only see events for which we are guests.
         * TODO: Allow account owners to decide whether clients can see project
         * or task dates on their calendar.
         */
        if ($clientID>0) {
            $sql = "SELECT
                        E.EventID,E.CalendarID,E.DateStart AS StartDateTime,E.DateEnd AS EndDateTime,
                        E.Title AS Title,E.Description,E.Location,E.Tags,E.Recurring,E.EmailReminder,
                        E.UseridEntry,E.UseridUpdate,
                        P.NameFirst,P.NameLast,
                        C.Title AS CalendarTitle,C.Color AS CssClass
                    FROM cpo_calendar_events E
                    LEFT JOIN cpo_calendar C ON C.CalendarID = E.CalendarID
                    LEFT JOIN cpo_people P ON P.LoginUserid = E.Userid
                    LEFT JOIN cpo_linkMemberEvent ME ON ME.EventID = E.EventID
                    WHERE
                    $whereDateFilter
                    AND E.Userid = '$accountUserid'
                    AND ME.ItemID = '$clientID'
                    AND ME.ItemType = 'C'
                    AND E.Deleted = 0
                    AND C.Deleted = 0
                    ORDER BY E.DateStart ASC";
        }

        /*
         * If we're a team member, we only see events from calendars that we have been
         * assigned to AND events for which we are guests.
         */
        elseif ($viewAll == FALSE) {
            $whereViewOwn = '';
            if ($viewOwn == TRUE) {
                $whereViewOwn = " OR E.UseridEntry = '$userid' ";
            }
            $sql = "SELECT
                        E.EventID,E.CalendarID,E.DateStart AS StartDateTime,E.DateEnd AS EndDateTime,
                        E.Title AS Title,E.Description,E.Location,E.Tags,E.Recurring,E.EmailReminder,
                        E.UseridEntry,E.UseridUpdate,
                        P.NameFirst,P.NameLast,
                        C.Title AS CalendarTitle,C.Color AS CssClass
                    FROM cpo_calendar_events E
                    LEFT JOIN cpo_calendar C ON C.CalendarID = E.CalendarID
                    LEFT JOIN cpo_people P ON P.LoginUserid = E.UseridEntry
                    LEFT JOIN cpo_linkMemberCalendar MC ON MC.CalendarID = E.CalendarID
                    LEFT JOIN cpo_linkMemberEvent ME ON ME.EventID = E.EventID
                    WHERE
                    $whereDateFilter
                    
                    AND E.Userid = '$accountUserid'
                    AND
                    (
                        MC.MemberID = '$pID'
                        OR (ME.ItemID = '$pID' AND ME.ItemType = 'P')
                        $whereViewOwn
                    )
                    AND E.Deleted = 0
                    ORDER BY E.DateStart ASC";

        }

        /*
         * If we're account owner, we get to see everything.
         */
        else {
            $sql = "SELECT
                        E.EventID,E.CalendarID,E.DateStart AS StartDateTime,E.DateEnd AS EndDateTime,
                        E.Title AS Title,E.Description,E.Location,E.Tags,E.Recurring,E.EmailReminder,
                        E.UseridEntry,E.UseridUpdate,
                        P.NameFirst,P.NameLast,
                        C.Title AS CalendarTitle,C.Color AS CssClass
                    FROM cpo_calendar_events E
                    LEFT JOIN cpo_calendar C ON C.CalendarID = E.CalendarID
                    LEFT JOIN cpo_people P ON P.LoginUserid = E.Userid
                    WHERE
                    $whereDateFilter
                    AND E.Userid = '$accountUserid'
                    AND E.Deleted = 0
                    AND C.Deleted = 0
                    ORDER BY E.DateStart ASC";
        }
        $query = $this->db->query($sql);
		return $query->result_array();
    }

    function getNumberOfEventsForCalendar($calendarID) {
        $sql = "SELECT COUNT(EventID) AS EventCount FROM cpo_calendar_events WHERE CalendarID = '$calendarID'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

	/**
	 * getCalendarEntryList : gets a list of calendar entries
	 *
	 * @access	public
	 * @param	int $calendarID ID of calendar for which we want entries
	 * @return	int $limit The number of records we want to get
	 */
	function getCalendarEntryList($calendarID=NULL,$limit=NULL,$dateEnd=NULL,$accountUserid=NULL) {
        /*
         * REMOVED JOIN ON PEOPLE TABLE AS IT WAS CAUSING MYSQL MEMORY ERRORS
         * AND WE DON'T NEED IT ANYWAY.
		$sql = "SELECT 
					E.EventID,E.DateStart,E.DateEnd,E.Title,E.Description,E.Location,E.Tags,E.Recurring,
                    E.UseridEntry,E.UseridUpdate,
					P.PID,P.NameFirst,P.NameLast,
					C.Title AS CalendarTitle
				FROM cpo_calendar_events E
				LEFT JOIN cpo_people P ON P.LoginUserid = E.UseridUpdate
				LEFT JOIN cpo_calendar C ON C.CalendarID = E.CalendarID
				WHERE
					E.DateStart >= '".date('Y-m-d')."' ";
        */
        $sql = "SELECT
					E.EventID,E.DateStart,E.DateEnd,E.Title,E.Description,E.Location,E.Tags,E.Recurring,
                    E.UseridEntry,E.UseridUpdate,
					C.Title AS CalendarTitle
				FROM cpo_calendar_events E
				LEFT JOIN cpo_calendar C ON C.CalendarID = E.CalendarID
				WHERE
					E.DateStart >= '".date('Y-m-d')."' ";

		if (!empty($dateEnd)) {
			$sql .= " AND E.DateStart <= '$dateEnd' ";
		}
		if ($calendarID>0) {
			$sql .= " AND E.CalendarID = '$calendarID' ";
		}
		$sql .= "
                AND E.Deleted = 0
                AND C.Deleted = 0
                AND E.Userid = '$accountUserid'";
        $sql .= " ORDER BY E.DateStart ASC ";

		if ($limit>0) {
			$sql .= " LIMIT $limit";
		}
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function getEventGuests($eventID) {
        $sql = "SELECT
                P.LoginUserid,P.NameFirst,P.NameLast,P.Language,
                U.EventID,U.ItemID,U.ItemType,
                C.Company
				FROM cpo_linkMemberEvent U
                    LEFT JOIN cpo_people P ON (P.PID = U.ItemID AND U.ItemType = 'P')
                    LEFT JOIN cpo_clients C ON (C.ClientID = U.ItemID AND U.ItemType = 'C')
                WHERE
                    U.EventID = '$eventID'";
        $query = $this->db->query($sql);
		return $query->result_array();
    }

	function checkForValidEvent($eventID,$calendarID,$userid,$accountUserid,$viewAll=FALSE) {
		$valid = FALSE;
		if ($viewAll == FALSE) {
            /*
			 * Make sure this event is viewable by userid
			 */
			$sql = "SELECT * from cpo_linkMemberEvent WHERE ItemID = '$userid' AND EventID = '$calendarID'";
			$query = $this->db->query($sql);
			if ($query->num_rows()>0) {
				$valid = TRUE;
			}
		} else {
			/*
             * If we can view all events, then make sure this event
             * belongs to a calendar that we can view.
             */
            $sql = "SELECT * from cpo_calendar WHERE Userid = '$accountUserid' AND CalendarID = '$calendarID'";
			$query = $this->db->query($sql);
			if ($query->num_rows()>0) {
				$valid = TRUE;
			}
		}
		return $valid;
	}

    function checkForValidCalendar($calendarID,$userid,$accountUserid,$viewAll=FALSE) {
        $valid = FALSE;
		if ($viewAll == FALSE) {
			/*
			 * Make sure this calendar is viewable by userid
			 */
			$sql = "SELECT * from cpo_linkMemberCalendar WHERE MemberID = '$userid' AND CalendarID = '$calendarID'";
			$query = $this->db->query($sql);
			if ($query->num_rows()>0) {
				$valid = TRUE;
			}
		} else {
			$valid = TRUE;
		}
        return $valid;
    }

	function getEvent($eventID) {
		$sql = "SELECT 
				E.EventID,E.CalendarID,E.Userid,E.ItemID,E.ItemType,E.DateStart,E.DateEnd,E.Title,E.Description,E.Location,E.PublicEvent,E.Tags,E.Recurring,
				E.EmailReminder,E.UseridEntry,E.UseridUpdate,E.Deleted,C.Title AS CalendarTitle,C.Color AS CalendarColor
				FROM cpo_calendar_events E
				LEFT JOIN cpo_calendar C ON C.CalendarID = E.CalendarID
				WHERE E.EventID = '$eventID'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function getDefaultCalendar($accountUserid,$calendarType) {
        $sql = "SELECT * from cpo_calendar
                WHERE
                Userid          = '$accountUserid' AND
                DefaultCalendar = 1 AND
                CalendarType    = '$calendarType'";
        $query = $this->db->query($sql);
		return $query->result_array();
    }

    function getEventsForExport($accountUserid) {
        $sql = "SELECT
                    E.DateStart,E.DateEnd,E.Title,E.Description,E.Location,E.PublicEvent,E.Tags,
                    C.Title AS Calendar
                FROM
                    cpo_calendar_events E
                LEFT JOIN
                    cpo_calendar C ON C.CalendarID = E.CalendarID
                WHERE
                    E.Userid = '$accountUserid' AND
                    E.Deleted = 0
                ORDER BY
                    DateEnd DESC";
        $query = $this->db->query($sql);
		return $query->result_array();
    }
}
?>
