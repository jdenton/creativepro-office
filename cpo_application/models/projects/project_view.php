<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Project_view extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
        loadLanguageFiles();
    }

    /**
      * checkForValidProject : Make sure that user has view rights for this project.
      *                        This prevents other accounts from seeing other users projects by
      *                        simply typing a random projectID in the URL.
    *
    * @access public
    * @param	 int     $projectID Project ID of project we're wanting to verify.
    * @return boolean true if project can be viewed by this user
    */
    function checkForValidProject($projectID,$accountUserid,$excludeDeleted=false,$viewAll=true) {
        $from = " cpo_project P";
        $viewAllJoin = '';
        $viewAllWhere = '';
        if ($viewAll == FALSE) {
            $from  = " cpo_linkPeopleItemType IT";
            $viewAllJoin = " LEFT JOIN cpo_project P ON P.ProjectID = IT.ItemID";
            $viewAllWhere = " IT.PeopleID = ".$this->session->userdata('pid')." AND ";
        }
        
        $sql = "SELECT P.ProjectID
                FROM $from
                $viewAllJoin
                WHERE
                $viewAllWhere
                ProjectID = '$projectID' AND
                Userid = '".$accountUserid."'";

        if ($excludeDeleted == true) {
            $sql .= " AND Deleted = 0";
        }
        
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
            return FALSE;
        }
        return TRUE;
     }

    /**
	 * getNumberOfProjects : retrieve number of projects for a user or client.
	 *
	 * @access	public
	 * @param	int    $itemID Userid or ClientID depending on if we want all projects 
	 *                         for account owner or all projects for a client.
	 * @param   string $group  user or client 
	 * @return	array  Array of project count data
	 */	
    function getNumberOfProjects($itemID,$group=null,$archive=NULL) {
        if ($archive != NULL && !empty($archive)) {
            $archive = " AND Archive = $archive ";
        }

        if ($archive == '0') {
            $archive = " AND Archive = 0 ";
        }

    	$sql = "SELECT COUNT(ProjectID) AS Projects FROM cpo_project
				WHERE ";
    	switch ($group) {
			case 'user':
			    $sql .= "Userid = '$itemID'";
			    break;
			case 'client':
			    $sql .= "ClientID = '$itemID'";
			    break;
			default:
				$sql .= "Userid = '$itemID'";
			    break;    
		}
		$sql .= $archive." AND Deleted = 0";
		$query = $this->db->query($sql); 			
		return $query->row_array();
    }
    
    /**
	 * getProjectList : retrieve project information.
	 *
	 * @access	public
	 * @param	int    $itemID Userid or ClientID depending on if we want all projects 
	 *                         for account owner or all projects for a client.
	 * @param   string $group  user or client 
	 * @param   string $status All for projects of any status. Else status|status|status (e.g get status of 1|2|3)
	 * @param   string $acdc   Sort ASC or DESC
	 * @param   int    $start  For LIMIT search
	 * @param   int    $limit  For LIMIT search
	 * @return	array  Array of project data
	 */	
    function getProjectList($itemID=NULL,$group=NULL,$status='all',$acdc='ASC',$tag=NULL,$archive=NULL,$viewAll=NULL) {
        $from = " cpo_project P";
        $viewAllJoin = "";
        if ($viewAll == FALSE) {
            $from  = " cpo_linkPeopleItemType IT";
            $viewAllJoin = " LEFT JOIN cpo_project P ON P.ProjectID = IT.ItemID";
        }
		$sql = "SELECT
					P.ProjectID,P.Title,P.URL,P.Description,P.Status,P.Budget,P.Date_Start,P.Date_End,P.Date_Complete,P.Archive,P.Tags,
					C.ClientID,C.ClientUserid,C.Company,C.Language,
					SUM(PT.ElapsedTime) AS ProjectTime,
                    MC.MainCat
				FROM $from
                $viewAllJoin
				LEFT JOIN cpo_clients C ON C.ClientID = P.ClientID
                LEFT JOIN cpo_main_cat MC ON MC.CatID = P.CatID
				LEFT JOIN cpo_project_time PT ON PT.ProjectID = P.ProjectID
				WHERE ";
    	switch ($group) {
			case 'user':
                if ($viewAll == FALSE) {
                    $sql .= "IT.PeopleID = $itemID AND IT.ItemType = 'P' ";
                } else {
                    $sql .= "P.Userid = '$itemID'";
                }
                $sort = "P.Title $acdc";
			    break;
			case 'client':
			    $sql .= "P.ClientID = '$itemID'";
                $sort = "P.Date_End $acdc";
			    break;
			default:
				$sql .= "P.Userid = '$itemID'";
                $sort = "P.Title $acdc";
			    break;    
		}

		if (!empty($tag)) {
			$sql .= " AND (P.Tags LIKE '$tag%' OR P.Tags LIKE '% $tag%') ";
		}

		if ($status != 'all') {
			$sql .= ' AND (';
			foreach(explode('|',$status) as $statusID) {
				$sql .= "P.Status = '$statusID' OR ";
			}	
			$sql = substr($sql,0,-3);
			$sql .= ') ';		
		}

        if ($archive == NULL || empty($archive)) {
            $archive = '0';
        }

        if ($archive != 'all') {
            $sql .= " AND P.Archive = $archive";
        }

        $sql .= " AND P.Deleted = 0";
        $sql .= " GROUP BY P.ProjectID ORDER BY ".$sort;
		$query = $this->db->query($sql); 			
		return $query->result_array();
    }	
    
    /**
	 * getProjectForAutocompleter : get project titles for autocomplete searchbox
	 *
	 * @access	public
	 * @param   string $q search term
	 * @return	string pipe delimited string of projects
	 */	
    function getProjectsForAutoCompleter($q) {
    	$sql = "SELECT ProjectID,Title,Status
				FROM cpo_project
				WHERE 
				(
					Title LIKE '%$q%' OR
					Description LIKE '%$q%' OR 
					URL LIKE '%$q%'
				)
				AND Userid = '".$this->session->userdata('accountUserid')."'
				AND Deleted = 0
				ORDER BY Title";
		$query = $this->db->query($sql); 
        $projectArray = array();
        $a=0;
		foreach ($query->result_array() as $row) {
            $projectArray[$a]['id'] = $row['ProjectID'];
            $projectArray[$a]['label'] = $row['Title'];
            $projectArray[$a]['value'] = $row['Title'];
            $a++;
		}
		return json_encode($projectArray);
    }

    /**
	 * getProjectInformation : get project information
	 *
	 * @access	public
	 * @param      int $projectID ID of project we want
	 * @return	array PHP array of project information
	 */
    function getProjectInformation($projectID) {
        $sql = "SELECT
                   P.ProjectID,P.CatID,P.Title,P.URL,P.Description,P.Budget,P.TemplateID,P.Status,P.Date_Start,P.Date_End,P.Archive,P.Tags,
                   C.ClientID,C.ClientUserid,C.Company,C.Language,
                   SUM(PT.ElapsedTime) AS ProjectTime, CAT.MainCat AS Category
               FROM cpo_project P
               LEFT JOIN cpo_clients C ON C.ClientID = P.ClientID
               LEFT JOIN cpo_main_cat CAT ON CAT.CatID = P.CatID
               LEFT JOIN cpo_project_time PT ON PT.ProjectID = P.ProjectID
               WHERE
                   P.ProjectID = '$projectID'";
         $query = $this->db->query($sql);
         return $query->row_array();
    }

	/**
	 * getProjectsForSelect : get project information for select menus
	 *
	 * @access	public
	 * @param   int   $clientID ID of client for whom we want projects.
	 *                If $clientID == NULL, return all projects for user.
	 * @return	array PHP array of project information
	 */
    function getProjectsForSelect($clientID=NULL,$viewAll=NULL,$pID=NULL) {
	$where = '';
        $from  = " cpo_project P ";
        $viewAllJoin = "";
        if ($viewAll == FALSE && empty($clientID)) {
            $from  = " cpo_linkPeopleItemType IT";
            $viewAllJoin = " LEFT JOIN cpo_project P ON P.ProjectID = IT.ItemID";
        }
        if ($clientID>0) {
            $where .= " P.ClientID = $clientID ";
        } else {
            if ($viewAll == FALSE && $pID>0) {
                $where .= " IT.PeopleID = $pID ";
            } else {
                $where .= " P.Userid = '".$this->session->userdata('accountUserid')."' ";
            }
        }
        $sql = "SELECT P.ProjectID,P.Title,C.Company
        FROM $from                
        $viewAllJoin
        LEFT JOIN cpo_clients C ON C.ClientID = P.ClientID
                        WHERE
                        $where
                        AND P.Archive = 0
                        AND P.Deleted = 0
                        ORDER BY P.Title";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /**
	 * getProjectsForCalendar
	 * @return	array PHP array of project information
	 */
	function getProjectsForCalendar($dateStart,$dateEnd=NULL,$accountUserid,$viewAll=NULL,$pID=NULL,$clientID=NULL) {
        if (!empty($dateEnd)) {
            $where = " P.Date_Start >= '$dateStart' AND P.Date_Start <= '$dateEnd' ";
        } else {
            $where  = " P.Date_Start = '$dateStart' ";
        }
        
        $viewAllJoin = "";
        if ($viewAll == FALSE) {
            $from  = " cpo_linkPeopleItemType IT";
            $viewAllJoin = " LEFT JOIN cpo_project P ON P.ProjectID = IT.ItemID";
        } else {
            $from = " cpo_project P ";
        }

        if ($viewAll == FALSE) {
            $viewAllWhere = " AND IT.PeopleID = $pID ";
        } else {
            $viewAllWhere = " AND P.Userid = $accountUserid ";
        }

        /*
         * Get projects that start in this date range.
         */
        if ($clientID>0) {
            $sql = "SELECT
                        P.ProjectID,P.CalendarID,P.Date_Start AS StartDateTime,P.Title AS Title,P.Description,
                        C.Title AS CalendarTitle,C.Color AS CssClass
                    FROM
                        cpo_project P
                        LEFT JOIN cpo_calendar C ON C.CalendarID = P.CalendarID
                    WHERE
                        $where
                        AND P.ClientID = $clientID
                        AND P.Deleted = 0
                        ORDER BY P.Date_Start ASC";
        } else {
            $sql = "SELECT
                        P.ProjectID,P.CalendarID,P.Date_Start AS StartDateTime,P.Title AS Title,P.Description,
                        C.Title AS CalendarTitle,C.Color AS CssClass
                    FROM
                        $from
                        $viewAllJoin
                        LEFT JOIN cpo_calendar C ON C.CalendarID = P.CalendarID
                    WHERE
                        $where
                        $viewAllWhere
                        AND P.Deleted = 0
                        ORDER BY P.Date_Start ASC";
        }
        $query = $this->db->query($sql);
        $resultArray1 = $query->result_array();

        foreach($resultArray1 as &$result) {
          $result['Title'] = $result['Title'].' ['.lang('project_start').']';
        }		

        if (!empty($dateEnd)) {
            $where = " P.Date_End >= '$dateStart' AND P.Date_End <= '$dateEnd' ";
        } else {
            $where  = " P.Date_End = '$dateStart' ";
        }

        /*
         * Get projects that end in this date range.
         */
        if ($clientID>0) {
            $sql = "SELECT
                        P.ProjectID,P.CalendarID,P.Date_End AS StartDateTime,P.Title AS Title,P.Description,
                        C.Title AS CalendarTitle,C.Color AS CssClass
                    FROM
                        cpo_project P
                        LEFT JOIN cpo_calendar C ON C.CalendarID = P.CalendarID
                    WHERE
                        $where
                        AND P.ClientID = $clientID
                        AND P.Deleted = 0
                        ORDER BY P.Date_Start ASC";
        } else {
            $sql = "SELECT
                        P.ProjectID,P.CalendarID,P.Date_End AS StartDateTime,P.Title AS Title,P.Description,
                        C.Title AS CalendarTitle,C.Color AS CssClass
                    FROM
                        $from
                        $viewAllJoin
                        LEFT JOIN cpo_calendar C ON C.CalendarID = P.CalendarID
                    WHERE
                        $where
                        $viewAllWhere
                        AND P.Deleted = 0
                        ORDER BY P.Date_Start ASC";
        }
        $query = $this->db->query($sql);
        $resultArray2 = $query->result_array();

        foreach($resultArray2 as &$result) {
          $result['Title'] = $result['Title'].' ['.lang('project_complete').']';
        }

        return array_merge($resultArray1,$resultArray2);
	}

    function getProjectFromPrevProjectID($prevProjectID,$accountUserid) {
        $sql = "SELECT * from cpo_project WHERE PrevProjectID = '$prevProjectID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->result_array();
    	}
    }

    function getProjectFromTitle($projectTitle,$accountUserid) {
        $sql = "SELECT * from cpo_project
                WHERE
                (Title LIKE '$projectTitle%' OR
                Title LIKE '% $projectTitle%') AND
                Userid = '$accountUserid' AND
                Deleted = 0
                LIMIT 1";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->row_array();
    	}
    }

    function getArchiveStatus($projectID) {
        $sql = "SELECT Archive from cpo_project WHERE ProjectID = '$projectID'";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->result_array();
    	}
    }
    
    function canViewProject($projectID) {
        
    }
}    
?>