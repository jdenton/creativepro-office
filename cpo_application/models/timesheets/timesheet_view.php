<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Timesheet_view extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function getTimesheetRecords($dateStart=NULL,$dateEnd=NULL,$userid=NULL,$accountUserid=NULL,$clientID=NULL,$projectID=NULL,$taskID=NULL,$report=0,$orderFor='entry',$viewAll=FALSE) {
        /*
         * Create our WHERE clause
         */
        $where   = '';
        $orderBy = '';
        /*
         * Are we meta-searching by Project or by Client?
         */
        if ($projectID > 0) {
            $where .= " PT.ProjectID = '$projectID' AND ";
        } elseif ($clientID > 0) {
            $where .= " P.ClientID = '$clientID' AND ";
        }

        if ($viewAll == TRUE && empty($userid)) {
            $where .= " PT.UseridAccount = '$accountUserid' AND ";
        } elseif ($userid > 0) {
            $where .= " PT.Userid = '$userid' AND ";
        }
        if ($taskID > 0) {
            $where .= " PT.TaskID = '$taskID' AND ";
        }
        if (!empty($dateStart) && !empty($dateEnd)) {
            $where .= " ((PT.DateClockEnd >= '$dateStart' AND PT.DateClockEnd <= '$dateEnd 23:59:59') OR (PT.DateClockStart >= '$dateStart' AND PT.DateClockStart <= '$dateEnd 23:59:59')) AND ";
        } elseif (!empty($dateStart)) {
            $where .= " (PT.DateClockEnd >= '$dateStart' OR PT.DateClockStart >= '$dateStart') AND ";
        } elseif (!empty($dateEnd)) {
            $where .= " (PT.DateClockEnd <= '$dateEnd' OR PT.DateClockStart <= '$dateEnd 23:59:59') AND ";
        }

        if ($report == 1) {
            $where .= " PT.ElapsedTime > 0 AND ";
        }

        if ($orderFor == 'entry') {
            $orderBy = ' ORDER BY PT.DateClockEnd ASC ';
        } else {
            $orderBy = ' ORDER BY PT.ProjectID,PT.TaskID,PT.DateClockEnd DESC ';
        }
        if ($report == 1) {
            $orderBy = ' ORDER BY PT.ProjectID,PT.TaskID,PT.DateClockEnd DESC ';
        }
        if ($projectID > 0) {
            $orderBy = ' ORDER BY PT.DateClockEnd DESC ';
        }

		/*
		 * Strip trailing AND from WHERE
		 */
		$where = substr($where,0,-4);
		$sql = "SELECT
					PT.TimeSheetID,PT.Userid,PT.ProjectID,PT.TaskID,PT.Start,PT.Stop,PT.ElapsedTime,
                    PT.DateClockStart,PT.DateClockEnd,PT.Comments,PT.Billable,PT.BilledInvoiceID,
					P.Title AS ProjectTitle,T.Title AS TaskTitle,C.ClientID,C.Company,
					PL.PID,PL.NameFirst,PL.NameLast,PL.Rate,
                    I.InvoiceID,I.InvNo,I.DateEntry
				FROM cpo_project_time PT
				LEFT JOIN cpo_project P ON PT.ProjectID = P.ProjectID
				LEFT JOIN cpo_project_tasks T ON PT.TaskID = T.TaskID
				LEFT JOIN cpo_clients C ON P.ClientID = C.ClientID
				LEFT JOIN cpo_people PL ON PT.Userid = PL.LoginUserid
                LEFT JOIN cpo_invoice I ON I.InvoiceID = PT.BilledInvoiceID
				WHERE
					$where
					AND PT.Deleted = 0
                    AND PT.Userid > 0
                    AND PT.ElapsedTime > 0
				$orderBy
				";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			return $query->result_array();
        } else {
			return false;
		}
	}

    function getTimesheetRecordsForExport($accountUserid) {
        $sql = "SELECT
					PT.TimeSheetID,PT.Userid,PT.ProjectID,PT.TaskID,PT.ElapsedTime,PT.DateClockStart,PT.DateClockEnd,PT.Comments,PT.Billable,
					P.Title AS ProjectTitle,T.Title AS TaskTitle,C.ClientID,C.Company,
					PL.PID,PL.NameFirst,PL.NameLast
				FROM cpo_project_time PT
				LEFT JOIN cpo_project P ON PT.ProjectID = P.ProjectID
				LEFT JOIN cpo_project_tasks T ON PT.TaskID = T.TaskID
				LEFT JOIN cpo_clients C ON P.ClientID = C.ClientID
				LEFT JOIN cpo_people PL ON PT.Userid = PL.LoginUserid
				WHERE
					P.Userid = '$accountUserid'
					AND PT.Deleted = 0
                    AND PT.Userid > 0
				ORDER BY PT.ProjectID,PT.TaskID,PT.DateClockEnd DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getTimesheetRecord($timeSheetID) {
        $sql = "SELECT
					PT.TimeSheetID,PT.Userid,PT.ProjectID,PT.TaskID,PT.ElapsedTime,PT.DateClockStart,PT.DateClockEnd,PT.Comments,PT.Billable,
					P.Title AS ProjectTitle,T.Title AS TaskTitle,C.ClientID,C.Company,
					PL.PID,PL.NameFirst,PL.NameLast
				FROM cpo_project_time PT
				LEFT JOIN cpo_project P ON PT.ProjectID = P.ProjectID
				LEFT JOIN cpo_project_tasks T ON PT.TaskID = T.TaskID
				LEFT JOIN cpo_clients C ON P.ClientID = C.ClientID
				LEFT JOIN cpo_people PL ON PT.Userid = PL.LoginUserid
				WHERE
                    TimeSheetID = $timeSheetID";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

	function getTimesheetEntry($timeSheetID) {
		$sql = "SELECT * from cpo_project_time WHERE TimeSheetID = '$timeSheetID'";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

    function checkForExistingTimesheetEntry($userid,$date,$projectID,$taskID=0) {
        $sql = "SELECT COUNT(TimeSheetID) FROM cpo_project_time
                WHERE
                    DateClockEnd = '$date' AND
                    ProjectID    = '$projectID' AND
                    TaskID       = '$taskID' AND
                    Userid       = '$userid' AND
                    Deleted      = 0";
        $query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			return $query->result_array();
        } else {
			return false;
		}
    }

	function getTotalHours($group=NULL,$groupID=NULL) {
		if ($group == 'user') {
			$sql = "SELECT SUM(ElapsedTime) AS TotalHours FROM cpo_project_time WHERE Userid = '$groupID'";
		} elseif ($group == 'project') {
			$sql = "SELECT SUM(ElapsedTime) AS TotalHours FROM cpo_project_time WHERE ProjectID = '$groupID'";
        } elseif ($group == 'task') {
			$sql = "SELECT SUM(ElapsedTime) AS TotalHours FROM cpo_project_time WHERE TaskID = '$groupID'";
		} elseif ($group == 'client') {
			$sql = "SELECT SUM(PT.ElapsedTime) AS TotalHours FROM cpo_project_time PT
					LEFT JOIN cpo_project P ON PT.ProjectID = P.ProjectID
					LEFT JOIN cpo_clients C ON P.ClientID = C.ClientID
					WHERE C.ClientID = '$groupID'";
		}
		$query = $this->db->query($sql);
		$totalHoursArray = $query->row_array();
		$totalHours = $totalHoursArray['TotalHours'];
		return $totalHours;
	}

    function getTimeSheetEntryFromPrevTimeSheetID($prevTimeSheetID,$accountUserid) {
        $sql = "SELECT * from cpo_project_time WHERE PrevTimeSheetID = '$prevTimeSheetID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->result_array();
    	}
    }
    
    function getRateTotalForHours($itemID, $itemType = 'project', $accountUserid) {
        switch ($itemType) {
            case 'project':
                $where = " AND PT.ProjectID = $itemID ";
                break;
            case 'user':
                $where = " AND PT.Userid = $itemID ";
                break;
            case 'client':
                $where = " AND PR.ClientID = $itemID ";
                break;
        }
        $sql = "SELECT PT.ElapsedTime,P.Rate from cpo_project_time PT 
                LEFT JOIN cpo_people P ON P.Userid = PT.Userid 
                LEFT JOIN cpo_project PR ON PR.ProjectID = PT.ProjectID
                WHERE 
                    UseridAccount = $accountUserid AND
                    P.Rate > 0 " . $where;
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $total = 0;
        foreach($result as $item) {
            $itemTotal = $item['ElapsedTime'] * $item['Rate'];
            $total = $total + $itemTotal;
        }        
        return $total;
    }
    
    function getHoursCountForDateRange($dateStart,$dateEnd,$userid,$viewAll,$accountUserid) {
        if ($viewAll == TRUE) {
            $sqlBillable = "SELECT FORMAT(SUM(T.ElapsedTime),1) AS Billable
                            FROM cpo_project_time T
                            WHERE
                            T.UseridAccount = '".$accountUserid."' AND
                            T.DateClockEnd >= '$dateStart' AND
                            T.DateClockEnd <= '$dateEnd' AND
                            T.Billable = 1 AND
                            T.Deleted = 0";
            $sqlNotBillable = "SELECT FORMAT(SUM(T.ElapsedTime),1) AS NotBillable
                            FROM cpo_project_time T
                            WHERE
                            T.UseridAccount = '".$accountUserid."' AND
                            T.DateClockEnd >= '$dateStart' AND
                            T.DateClockEnd <= '$dateEnd' AND
                            T.Billable = 0 AND
                            T.Deleted = 0";    
        } else {
            
        } 
        $queryBillable = $this->db->query($sqlBillable);
        $resultBillableArray = $queryBillable->row_array();
        
        $queryNotBillable = $this->db->query($sqlNotBillable);
        $resultNotBillableArray = $queryNotBillable->row_array();
        
        $finalArray = array(
            'Billable' => $resultBillableArray['Billable'],
            'NotBillable' => $resultNotBillableArray['NotBillable']
        );
        return $finalArray;
    }
}
?>