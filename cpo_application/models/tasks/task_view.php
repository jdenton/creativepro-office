<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Task_view extends Model {
    function __construct() {
         parent::Model();
         $this->load->model('users/User_information','',true);
         $this->load->model('timesheets/Timesheet_view','',true);
    }
    
    /**
	 * getMilestonesForProject : retrieve milestones for a project
	 *
	 * @access	public
	 * @param	int    $projectID ID of the project for which we want milestones
	 * @param   int    $tasksAlso Set = 1 to pull tasks for each milestone as well.
	 * @return	array  Array of milestone/task data
	 */	
     function getMilestonesForProject($projectID,$tasksAlso=0,$viewAll=TRUE,$pid=NULL) {
          $sql = "SELECT 
					T.TaskID,T.Title,T.Description,T.Date_Start,T.Date_End,
                    T.Date_Completed,T.HoursEstimated,T.Status,T.UseridEntry,
					P.PID,P.LoginUserid AS UID,P.NameFirst,P.NameLast
					FROM cpo_project_tasks T
					LEFT JOIN
						cpo_people P ON P.LoginUserid = T.UseridEntry
					WHERE
						T.ProjectID = $projectID AND
						T.Milestone = 1 AND
						T.Deleted = 0
					ORDER BY
						T.Date_Start DESC,T.TaskID ASC";
          $query = $this->db->query($sql);
          $returnArray = $query->result_array();
    	
          if ($tasksAlso == 1) {
          	   $j=0; 	
               foreach ($query->result_array() as $row) {
               		$returnArray[$j]['MilestoneID'] = $row['TaskID'];
               		$returnArray[$j]['Title']       = $row['Title'];
                    $returnArray[$j]['ProjectID']   = $projectID;
               		$returnArray[$j]['Description'] = $row['Description'];
               		$returnArray[$j]['Date_Start']  = $row['Date_Start']; 
               		$returnArray[$j]['Date_End']    = $row['Date_End'];
					$returnArray[$j]['UseridEntry'] = $row['UseridEntry'];
					$returnArray[$j]['NameFirst']   = $row['NameFirst'];
					$returnArray[$j]['NameLast']    = $row['NameLast'];
                    $returnArray[$j]['UID']         = $row['UID'];
                    $returnArray[$j]['Tasks']       = Task_View::getTasksForMilestone($row['TaskID'],$projectID,$viewAll,$pid);
					$returnArray[$j]['TaskCount']   = count($returnArray[$j]['Tasks']);
               		$j++;
               }

               /*
                * Now get miscellaneous tasks for this project
                */
                $returnArray[$j]['MilestoneID'] = 0;
                $returnArray[$j]['Title']       = lang('task_miscellaneous_tasks');
                $returnArray[$j]['Description'] = '';
                $returnArray[$j]['Date_Start']  = '';
                $returnArray[$j]['Date_End']    = '';
                $returnArray[$j]['UseridEntry'] = '';
                $returnArray[$j]['NameFirst']   = '';
                $returnArray[$j]['NameLast']    = '';
                $returnArray[$j]['UID']         = '';
                $returnArray[$j]['Tasks']       = Task_View::getTasksForMilestone(0,$projectID,$viewAll,$pid);
                $returnArray[$j]['TaskCount']   = count($returnArray[$j]['Tasks']);

          }
          return $returnArray;
    }
    
    /**
	 * getTasksForProject : retrieve tasks for a project
	 *
	 * @access	public
	 * @param	int    $milestoneID ID of the milestone for which we want tasks
	 * @return	array  Array of task data
	 */	
    function getTasksForMilestone($milestoneID=NULL,$projectID=NULL,$viewAll=TRUE,$pid=NULL) {
        $viewAllWhere = '';
        if ($viewAll == FALSE) {
            $viewAllWhere = " AND UT.MemberID = '".$pid."' ";
        }

        if ($milestoneID>0) {
            $sql = "SELECT
                    T.TaskID,T.Userid AS TUserid,T.ParentTaskID AS MilestoneID,T.ProjectID,T.Title,T.Description,T.Date_Start,
                    T.Date_End,T.Date_Completed,T.HoursEstimated,T.Status,T.Priority,T.TaskNo,T.Tags,
                    P.PID,P.LoginUserid AS UID,P.NameFirst,P.NameLast,P.Language,
                    CP.PID AS CreatorPID,CP.NameFirst AS CreatorNameFirst,CP.NameLast AS CreatorNameLast, 
                    (SELECT COUNT(MID) FROM cpo_messages M WHERE M.ItemID = T.TaskID AND M.ItemType = 'task' AND (DateRead Is Null OR DateRead = '0000-00-00 00:00:00')) AS UnreadMessages,                
                    (SELECT COUNT(MID) FROM cpo_messages M WHERE M.ItemID = T.TaskID AND M.ItemType = 'task') AS Messages
                FROM
                    cpo_project_tasks T
                LEFT JOIN
                    cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
                LEFT JOIN
                    cpo_people P ON P.PID = UT.MemberID
                LEFT JOIN
                    cpo_people CP ON CP.LoginUserid = T.UseridEntry
                WHERE
                    T.ParentTaskID = $milestoneID AND
                    T.ProjectID = $projectID AND
                    T.Deleted = 0
                    $viewAllWhere
                ORDER BY
                    T.DisplayOrder ASC,T.Date_Start ASC,T.TaskID ASC";
        } elseif ($projectID>0) {
            /*
             * Removed T.ParentTaskID = '0' AND
             * because Misc tasks weren't rendering correctly.
             */
            $sql = "SELECT
                    T.TaskID,T.Userid AS TUserid,T.ParentTaskID AS MilestoneID,T.ProjectID,T.Title,T.Description,T.Date_Start,
                    T.Date_End,T.Date_Completed,T.HoursEstimated,T.Status,T.Priority,T.TaskNo,T.Tags,
                    P.PID,P.LoginUserid AS UID,P.NameFirst,P.NameLast,P.Language,
                    CP.PID AS CreatorPID,CP.NameFirst AS CreatorNameFirst,CP.NameLast AS CreatorNameLast,
                    (SELECT COUNT(MID) FROM cpo_messages M WHERE M.ItemID = T.TaskID AND M.ItemType = 'task') AS Messages,
                    (SELECT COUNT(MID) FROM cpo_messages M WHERE M.ItemID = T.TaskID AND M.ItemType = 'task' AND (DateRead Is Null OR DateRead = '0000-00-00 00:00:00')) AS UnreadMessages
                FROM
                    cpo_project_tasks T
                LEFT JOIN
                    cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
                LEFT JOIN
                    cpo_people P ON P.PID = UT.MemberID
                LEFT JOIN
                    cpo_people CP ON CP.LoginUserid = T.UseridEntry
                WHERE
                    T.ProjectID = $projectID AND
                    T.ParentTaskID = 0 AND
                    T.Milestone = 0 AND
                    T.Deleted = 0
                    $viewAllWhere
                ORDER BY
                    T.DisplayOrder ASC,T.Date_Start ASC,T.TaskID ASC";
        }
        $query = $this->db->query($sql);
        $resultArray = $query->result_array();
        $taskArray = Task_view::createTaskArray($resultArray);
        return $taskArray;
    }

    function createTaskArray($resultArray) {
         $j = 0;
         $taskArray = array();
         $taskID = 0;
         foreach ($resultArray as $row) {
            $newTaskID = $row['TaskID'];
            if ($newTaskID != $taskID) {                
			  	 $taskInformationRow = $j;
			     $taskID = $newTaskID;
                 $k=$j;
			     $l=0;
                 
			     $taskArray[$j]['TaskID']         = $row['TaskID'];
			     $taskArray[$j]['Userid']         = $row['TUserid'];
			     $taskArray[$j]['MilestoneID']    = $row['MilestoneID'];
				 $taskArray[$j]['ProjectID']      = $row['ProjectID'];
			     $taskArray[$j]['Title']          = $row['Title'];
			     $taskArray[$j]['Description']    = $row['Description'];
			     $taskArray[$j]['Date_Start']     = $row['Date_Start'];
			     $taskArray[$j]['Date_End']       = $row['Date_End'];
			     $taskArray[$j]['Date_Completed'] = $row['Date_Completed'];
			     $taskArray[$j]['Status']         = $row['Status'];
			     $taskArray[$j]['Priority']       = $row['Priority'];
			     $taskArray[$j]['TaskNo']         = $row['TaskNo'];
			     $taskArray[$j]['Tags']           = $row['Tags'];
                 $taskArray[$j]['HoursEstimated'] = $row['HoursEstimated'];
                 $taskArray[$j]['Messages']       = $row['Messages'];
                 $taskArray[$j]['UnreadMessages'] = $row['UnreadMessages'];
                 $taskArray[$j]['SortOrder']      = $j;

                 if (isset($row['ProjectTitle'])) {
                     $taskArray[$j]['ProjectTitle'] = $row['ProjectTitle'];
                 }

			     /*
				  * Task created by...
				  */
				 $taskArray[$j]['CreatorName'] = $row['CreatorNameFirst'].' '.$row['CreatorNameLast'];
                 $taskArray[$j]['CreatorPID']  = $row['CreatorPID'];

                 $hoursActual = Task_view::getCompletedHoursForTask($row['TaskID']);
                 if ($hoursActual > 0) {
                     $taskArray[$j]['HoursActual'] = $hoursActual;
                 } else {
                     $taskArray[$j]['HoursActual'] = '0.00';
                 }
                 
			     $j++;
			}

			/*
			 * This is the Assigned Team Member information
			 */
			$taskArray[$k]['TeamMembers'][$l]['UID']       = $row['UID'];
            $taskArray[$k]['TeamMembers'][$l]['PID']       = $row['PID'];
			$taskArray[$k]['TeamMembers'][$l]['NameFirst'] = $row['NameFirst'];
			$taskArray[$k]['TeamMembers'][$l]['NameLast']  = $row['NameLast'];
            $taskArray[$k]['TeamMembers'][$l]['Language']  = $row['Language'];
			$l++;
          }
          return $taskArray;
    }

    function createTaskArrayAllUserTasks($resultArray) {
         $m = -1;
         $taskArray = array();
         $projectID = 0;
         foreach ($resultArray as $row) {
            $newProjectID = $row['MainProjectID'];
            if ($newProjectID != $projectID) {
                 $m++;
                 $j=0;
                 $taskArray['Projects'][$m]['ProjectTitle'] = $row['ProjectTitle'];
                 $taskArray['Projects'][$m]['ProjectID']    = $row['MainProjectID'];
                 $taskArray['Projects'][$m]['Tasks'] = array();
            }

             $taskInformationRow = $j;
             $projectID = $newProjectID;
             $l=0;
             $taskArray['Projects'][$m]['Tasks'][$j]['TaskID']         = $row['TaskID'];
             $taskArray['Projects'][$m]['Tasks'][$j]['Userid']         = $row['Userid'];
             $taskArray['Projects'][$m]['Tasks'][$j]['MilestoneID']    = $row['MilestoneID'];
             $taskArray['Projects'][$m]['Tasks'][$j]['ProjectID']      = $row['ProjectID'];
             $taskArray['Projects'][$m]['Tasks'][$j]['TaskTitle']      = $row['TaskTitle'];
             $taskArray['Projects'][$m]['Tasks'][$j]['Description']    = $row['Description'];
             $taskArray['Projects'][$m]['Tasks'][$j]['Date_Start']     = $row['Date_Start'];
             $taskArray['Projects'][$m]['Tasks'][$j]['Date_End']       = $row['Date_End'];
             $taskArray['Projects'][$m]['Tasks'][$j]['Date_Completed'] = $row['Date_Completed'];
             $taskArray['Projects'][$m]['Tasks'][$j]['Status']         = $row['Status'];
             $taskArray['Projects'][$m]['Tasks'][$j]['Priority']       = $row['Priority'];
             $taskArray['Projects'][$m]['Tasks'][$j]['TaskNo']         = $row['TaskNo'];
             $taskArray['Projects'][$m]['Tasks'][$j]['Tags']           = $row['Tags'];
             $taskArray['Projects'][$m]['Tasks'][$j]['HoursEstimated'] = $row['HoursEstimated'];
             //$taskArray['Projects'][$m]['Tasks'][$j]['Messages']       = $row['Messages'];
             //$taskArray['Projects'][$m]['Tasks'][$j]['UnreadMessages'] = $row['UnreadMessages'];

             /*
              * Task created by...
              */
             $taskArray['Projects'][$m]['Tasks'][$j]['CreatorName'] = $row['CreatorNameFirst'].' '.$row['CreatorNameLast'];
             $taskArray['Projects'][$m]['Tasks'][$j]['CreatorPID']  = $row['CreatorPID'];

             /*
              * Task assigned to
              */
             $taskArray['Projects'][$m]['Tasks'][$j]['AssignedName']     = $row['NameFirst'].' '.$row['NameLast'];
             $taskArray['Projects'][$m]['Tasks'][$j]['AssignedPID']      = $row['UID'];
             $taskArray['Projects'][$m]['Tasks'][$j]['AssignedLanguage'] = $row['Language'];

             $hoursActual = Task_view::getCompletedHoursForTask($row['TaskID']);
             if ($hoursActual > 0) {
                 $taskArray['Projects'][$m]['Tasks'][$j]['HoursActual'] = $hoursActual;
             } else {
                 $taskArray['Projects'][$m]['Tasks'][$j]['HoursActual'] = '0.00';
             }

             $j++;
		  }
          return $taskArray;
    }
    
    function getAssignedUsersForTask($taskID) {
        $sql = "SELECT P.PID,P.NameFirst,P.NameLast
                FROM cpo_linkUIDTask UT 
                LEFT JOIN cpo_people P ON P.PID = UT.MemberID
                WHERE UT.TaskID = $taskID";
        $query = $this->db->query($sql);
        $resultArray = $query->result_array();
        return $resultArray;
    }

    /**
	 * getTask : get task data
	 *
	 * @acces   public
	 * @param   int    $taskID ID of task for which we want data
	 * @retur   array  PHP array of data
	 */
    function getTask($taskID) {
         $sql = "SELECT
					T.TaskID,T.Userid AS TUserid,T.ParentTaskID AS MilestoneID,T.ProjectID,T.Title,T.Description,T.Date_Start,
					T.Date_End,T.Date_Completed,T.HoursEstimated,T.HoursActual,T.Status,T.Priority,T.TaskNo,T.Tags,
					P.PID,P.LoginUserid AS UID,P.NameFirst,P.NameLast,P.Language,
                    CP.PID AS CreatorPID,CP.NameFirst AS CreatorNameFirst,CP.NameLast AS CreatorNameLast,
                    (SELECT COUNT(MID) FROM cpo_messages M WHERE M.ItemID = T.TaskID AND M.ItemType = 'task') AS Messages,
                    (SELECT COUNT(MID) FROM cpo_messages M WHERE M.ItemID = T.TaskID AND M.ItemType = 'task' AND (DateRead Is Null OR DateRead = '0000-00-00 00:00:00')) AS UnreadMessages
				FROM
					cpo_project_tasks T
				LEFT JOIN
					cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
				LEFT JOIN
					cpo_people P ON P.PID = UT.MemberID
                LEFT JOIN
                    cpo_people CP ON CP.LoginUserid = T.UseridEntry
				WHERE
					T.TaskID = '$taskID' AND
                    T.Deleted = 0";
         $query = $this->db->query($sql);
         $resultArray = $query->result_array();
         $taskArray = Task_view::createTaskArray($resultArray);
         return $taskArray;
    }
    
	/**
	 * getMilestone : get milestone data
	 *
	 * @acces   public
	 * @param   int    $milestoneID ID of milestone for which we want data
	 * @retur   array  PHP array of data
	 */
    function getMilestone($milestoneID) {
         $sql = "SELECT
					T.TaskID AS MilestoneID,T.Title,T.Description,T.Date_Start,T.Date_End,
                    T.Date_Completed,T.Status,T.UseridEntry,
					P.PID,P.NameFirst,P.NameLast
				FROM
					cpo_project_tasks T
				LEFT JOIN
					cpo_people P ON P.LoginUserid = T.UseridEntry
				WHERE
					T.TaskID = '$milestoneID' AND
                    T.Deleted = 0";
         $query = $this->db->query($sql);
         $milestoneArray = $query->row_array();
         return $milestoneArray;
    }

    /**
	 * getTasksForAutocompleter : get task titles for autocomplete searchbox
	 *
	 * @access	public
	 * @param   string $q search term
	 * @return	string pipe delimited string of tasks
	 */
    function getTasksForAutoCompleter($q) {
    	$sql = "SELECT DISTINCT(Title),TaskID,COUNT(TaskID) AS Frequency
				FROM cpo_project_tasks
				WHERE
				(
                    Title LIKE '$q%' OR Title LIKE '% $q%'
				) AND
				Userid = '".$this->session->userdata('accountUserid')."' AND
                Milestone = 0 AND
                Deleted = 0
                GROUP BY Title
				ORDER BY Frequency DESC";
		$query = $this->db->query($sql);
		$taskArray = array();
        $a=0;
		foreach ($query->result_array() as $row) {
            $taskArray[$a]['id'] = $row['TaskID'];
            $taskArray[$a]['label'] = $row['Title'];
            $taskArray[$a]['value'] = $row['Title'];
            $a++;
		}
		return json_encode($taskArray);
    }

    function searchTasks($q,$projectID,$autocomplete=1) {
        if ($autocomplete == 1) {
            $sql = "SELECT DISTINCT(Title),TaskID,COUNT(TaskID) AS Frequency
                    FROM cpo_project_tasks
                    WHERE
                    (Title LIKE '$q%' OR Title LIKE '% $q%' OR Description LIKE '$q%' OR Description LIKE '% $q%')
                    AND ProjectID = '$projectID'
                    AND Userid = '".$this->session->userdata('accountUserid')."' AND
                        Deleted = 0
                        GROUP BY Title
                    ORDER BY Frequency DESC";
            $query = $this->db->query($sql);
            $tasks = '';
            foreach ($query->result_array() as $row) {
                   $taskDisplay = $row['Title'];
                   $tasks .= $taskDisplay.'|'.$row['TaskID']."\n";
            }
            return $tasks;
        } else {

        }
    }

	/**
	 * getNumberOfTasks : get number of tasks for user or project
	 *
	 * @param	int    $itemID Userid or ProjectID or ClientID depending on if we want all tasks
	 *                 for account owner or all tasks for a project.
	 * @param   string $group  user or project
	 * @param   int    $status default = all
	 *                 incomplete, complete, overdue
	 * @return	array  Array of task count data
	 */
	function getNumberOfTasks($itemID,$group=NULL,$status=NULL,$viewAll=NULL,$pID=NULL) {
		$where = '';
        $from  = " cpo_project_tasks T ";
        $viewAllJoin = '';

        if ($viewAll == FALSE) {
            $from  = " cpo_linkUIDTask UT";
            $viewAllJoin = " LEFT JOIN cpo_project_tasks T ON T.TaskID = UT.TaskID";
            $where = " AND UT.MemberID = $pID ";
        }

		if ($group == 'client') {
			
		} else {
            /*
			$sql = "SELECT 
					COUNT(T.TaskID) AS Tasks,
					SUM(T.HoursEstimated) AS HoursEstimated
					FROM $from
                    $viewAllJoin
					WHERE ";
            */
            $sql = "SELECT
                    DISTINCT(T.TaskID), T.HoursEstimated
					FROM $from
                    $viewAllJoin
					WHERE ";
			switch ($group) {
				case 'user':
					$sql .= "T.Userid = '$itemID'";
					break;
				case 'project':
					$sql .= "T.ProjectID = '$itemID'";
					break;
				case 'milestone':
					$sql .= "T.ParentTaskID = '$itemID'";
					break;
				default:
					$sql .= "T.Userid = '$itemID'";
					break;
			}

			if ($status != NULL) {
				switch ($status) {
					case 'incomplete':
						$sql .= " AND T.Status <> 2 ";
						break;
					case 'complete':
						$sql .= " AND T.Status = 2 ";
						break;
					case 'overdue':
						$sql .= " AND ((T.Status < 2 AND T.Date_End < '".date('Y-m-d')."' AND T.Date_End IS NOT NULL AND T.Date_End <> '0000-00-00') OR T.Status = 4) ";
						break;
				}
			}

            $sql .= " AND T.Milestone = 0 AND T.Deleted = 0";
		}
		$query = $this->db->query($sql);
        $tasks = 0;
        $hours = 0;
        foreach($query->result_array() as $item) {
            $tasks++;
            $hours = $hours + $item['HoursEstimated'];
        }
		return array('Tasks' => $tasks,'HoursEstimated' => $hours);
	}

	/**
	 * getTasksForSelectFromProject : get tasks for a select menu
	 *
	 * @access	public
	 * @param   int   $projectID ID of project for which we want tasks.
	 * @return	array PHP array of task information
	 */
	function getTasksForSelectFromProject($projectID,$viewAll=NULL,$pID=NULL) {
        /*
        $sql = "SELECT TaskID,ParentTaskID,Title,Milestone FROM cpo_project_tasks
				WHERE
				ProjectID = '$projectID' AND
                Deleted = 0
				ORDER BY Title";
		$query = $this->db->query($sql);
		return $query->result_array();
        */
        $where = '';
        $from  = " cpo_project_tasks T ";
        $viewAllJoin = "";
        if ($viewAll == FALSE) {
            $from  = " cpo_linkUIDTask UT";
            $viewAllJoin = " LEFT JOIN cpo_project_tasks T ON T.TaskID = UT.TaskID";
        }
        if ($viewAll == FALSE && $pID>0) {
            $where .= " UT.MemberID = $pID AND T.ProjectID = '".$projectID."' ";
        } else {
            $where .= " T.ProjectID = '".$projectID."' ";
        }
        $sql = "SELECT T.TaskID,T.ParentTaskID,T.Title,T.Milestone
                FROM $from
                $viewAllJoin
				WHERE
                $where
                AND T.Deleted = 0
				ORDER BY T.Title";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/**
	 * getEstimatedTimeForProjectTasks
	 * @return	array PHP array of task information
	 */
	function getEstimatedTimeForProjectTasks($projectID) {
		$sql = "SELECT SUM(HoursEstimated) AS HoursEstimated FROM cpo_project_tasks
				WHERE
				ProjectID = '$projectID'";
		$query = $this->db->query($sql);
		$totalHoursArray = $query->row_array();
		$totalHours = $totalHoursArray['HoursEstimated'];
		return $totalHours;
	}

    function getTaskStatus($taskID) {
        $sql = "SELECT Status FROM cpo_project_tasks WHERE TaskID = '$taskID'";
        $query = $this->db->query($sql);
        $statusArray = $query->row_array();
        return $statusArray['Status'];
    }

	function getCompletedHoursForProject($projectID) {
		$sql = "SELECT SUM(ElapsedTime) AS CompletedHours 
                FROM cpo_project_time
                WHERE
                    ProjectID = '$projectID' AND
                    Deleted = 0
                ";
		$query = $this->db->query($sql);
        $resultArray = $query->row_array();
        return $resultArray['CompletedHours'];
	}

    function getCompletedHoursForTask($taskID) {
		$sql = "SELECT SUM(ElapsedTime) AS CompletedHours 
                FROM cpo_project_time
                WHERE
                    TaskID = '$taskID' AND
                    Deleted = 0";
		$query = $this->db->query($sql);
        $resultArray = $query->row_array();
        return $resultArray['CompletedHours'];
	}

	function getTotalTasks($projectID) {
		$sql = "SELECT SUM(HoursActual) AS CompletedHours FROM cpo_project_tasks WHERE ProjectID = '$projectID'";
		$query = $this->db->query($sql);
        $resultArray = $query->row_array();
        return $resultArray['CompletedHours'];
	}

    /**
	 * getTasksForCalendar
	 * @return	array PHP array of task information
	 */
	function getTasksForCalendar($dateStart,$dateEnd=NULL,$accountUserid,$viewAll=NULL,$pID=NULL) {
        if (!empty($dateEnd)) {
            $where = " T.Date_Start >= '$dateStart' AND T.Date_Start <= '$dateEnd' ";
        } else {
            $where  = " T.Date_Start = '$dateStart' ";
        }
        if ($viewAll == TRUE) {
            $sql = "SELECT
                        T.TaskID,T.CalendarID,T.ProjectID,T.Date_Start AS StartDateTime,T.Date_End AS EndDateTime,
                        T.Title AS Title,T.Description,
                        C.Title AS CalendarTitle,C.Color AS CssClass,
                        P.PID,P.LoginUserid AS UID,P.NameFirst,P.NameLast,
                        PR.Title AS ProjectTitle
                    FROM cpo_project_tasks T
                    LEFT JOIN cpo_calendar C ON C.CalendarID = T.CalendarID
                    LEFT JOIN cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
                    LEFT JOIN cpo_people P ON P.PID = UT.MemberID
                    LEFT JOIN cpo_project PR ON PR.ProjectID = T.ProjectID
                    WHERE
                    $where
                    AND T.Milestone = 0
                    AND T.Userid = '$accountUserid'
                    AND T.Deleted = 0
                    ORDER BY T.Date_Start ASC";
        } else {
            $sql = "SELECT
                        T.TaskID,T.CalendarID,T.Date_Start AS StartDateTime,T.Date_End AS EndDateTime,
                        T.Title AS Title,T.Description,
                        C.Title AS CalendarTitle,C.Color AS CssClass
                    FROM cpo_project_tasks T
                    LEFT JOIN cpo_calendar C ON C.CalendarID = T.CalendarID
                    LEFT JOIN cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
                    WHERE
                    $where
                    AND T.Milestone = 0
                    AND UT.MemberID = '".$pID."'
                    AND T.Deleted = 0
                    ORDER BY T.Date_Start ASC";
        }
        $query = $this->db->query($sql);
        $resultArray = $query->result_array();

        $j = 0;
        $taskArray = array();
        $taskID = 0;
        foreach ($resultArray as $row) {
            $newTaskID = $row['TaskID'];
            if ($newTaskID != $taskID) {
			  	 $taskInformationRow = $j;
			     $taskID = $newTaskID;
                 $k=$j;
			     $l=0;
			     $taskArray[$j]['TaskID']         = $row['TaskID'];
				 $taskArray[$j]['ProjectID']      = $row['ProjectID'];
			     $taskArray[$j]['Title']          = $row['Title'];
			     $taskArray[$j]['Description']    = $row['Description'];
			     $taskArray[$j]['StartDateTime']  = $row['StartDateTime'];
			     $taskArray[$j]['EndDateTime']    = $row['EndDateTime'];
                 $taskArray[$j]['CalendarTitle']  = $ror['CalendarTitle'];
                 $taskArray[$j]['CssClass']       = $row['CssClass'];

                 if (isset($row['ProjectTitle'])) {
                     $taskArray[$j]['ProjectTitle'] = $row['ProjectTitle'];
                 }
			     $j++;
			}

			/*
			 * This is the Assigned Team Member information
			 */
			$taskArray[$k]['TeamMembers'][$l]['UID']       = $row['UID'];
            $taskArray[$k]['TeamMembers'][$l]['PID']       = $row['PID'];
			$taskArray[$k]['TeamMembers'][$l]['FullName']  = $row['NameFirst'].' '.$row['NameLast'];
			$l++;
          }
          return $taskArray;
	}

    function getTasksForExport($accountUserid) {
         $sql = "SELECT
					T.TaskID,T.Userid AS TUserid,T.ParentTaskID AS MilestoneID,T.ProjectID,T.Title,T.Description,T.Date_Start,
					T.Date_End,T.Date_Completed,T.HoursEstimated,T.HoursActual,T.Status,T.Priority,T.TaskNo,T.Milestone,T.Tags,
					P.PID,P.LoginUserid AS UID,P.NameFirst,P.NameLast,P.Language,
                    CP.PID AS CreatorPID,CP.NameFirst AS CreatorNameFirst,CP.NameLast AS CreatorNameLast,
                    PR.Title AS ProjectTitle
				FROM
					cpo_project_tasks T
                LEFT JOIN
                    cpo_project PR ON PR.ProjectID = T.ProjectID
				LEFT JOIN
					cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
				LEFT JOIN
					cpo_people P ON P.PID = UT.MemberID
                LEFT JOIN
                    cpo_people CP ON CP.LoginUserid = T.UseridEntry
				WHERE
					T.Userid = '$accountUserid' AND
                    T.Deleted = 0
                ORDER BY
                    T.ProjectID,T.Milestone DESC
                    ";
         $query = $this->db->query($sql);
         $resultArray = $query->result_array();
         $taskArray = Task_view::createTaskArray($resultArray);
         return $taskArray;
    }

    /*
     * 2010-08-12 We changed $userid to $pid because I think
     * we are saving task assignments in the cpo_linkUIDTask
     * table as MemberID = PID
     */
    function getTaskDigestView($pid,$accountUserid=NULL,$viewAll=TRUE,$viewAllProjects=TRUE) {
	$taskArray = array();
        $totalTasks = 0;
        $sql  = NULL;
        $sql2 = NULL;
        $sql3 = NULL;
        $sql4 = NULL;
        $sql5 = NULL;

        $taskArray['TasksOverdue']  = array();
        $taskArray['TasksToday']    = array();
        $taskArray['TasksTomorrow'] = array();
        $taskArray['TasksThisWeek'] = array();
        $taskArray['TasksNextWeek'] = array();

        /*
         * Get overdue tasks
         */
        if ($viewAll == TRUE) {
                $sql = "SELECT
                            T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
                            P.ProjectID,P.Title AS ProjectTitle,
                            T2.Title AS Milestone
                        FROM cpo_project_tasks T
                        LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                        LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
                        WHERE
                            T.Deleted = 0
                            AND T.Milestone = 0
                            AND T.Userid = '".$accountUserid."'
                            AND ((
                                T.Status < 2
                                AND T.Date_End < '".convertMySQLToGMT(date('Y-m-d 23:59:00'),'push','Y-m-d H:i:s')."'
                                AND T.Date_End IS NOT NULL
                                AND T.Date_End <> '0000-00-00 00:00:00')
                                OR T.Status = 4
                            )
                            ORDER BY
                            T.Date_End";
        } else {
            /*
             * Get only tasks assigned to this user
             */
            $sql = "SELECT
						T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
						P.ProjectID,P.Title AS ProjectTitle,
                                                T2.Title AS Milestone
					FROM cpo_project_tasks T
					LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                                        LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
                    LEFT JOIN cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
					WHERE
						T.Deleted = 0
						AND T.Milestone = 0
                        AND UT.MemberID = '".$pid."'
						AND ((
                                T.Status < 2
                                AND T.Date_End < '".convertMySQLToGMT(date('Y-m-d 23:59:00'),'push','Y-m-d H:i:s')."'
                                AND T.Date_End IS NOT NULL
                                AND T.Date_End <> '0000-00-00 00:00:00')
                                OR T.Status = 4
                            )
					ORDER BY
						T.Date_End";
		}
        if ($sql != NULL) {
            $query = $this->db->query($sql);
            $resultArray = $query->result_array();
            if (sizeof($resultArray)>0) {
                $totalTasks = $totalTasks+$query->num_rows();
                $taskArray['TasksOverdue'] = $resultArray;
            }
        }

        /*
         * Get tasks for today
         */
        if ($viewAll == TRUE) {
			$sql2 = "SELECT
						T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
						P.ProjectID,P.Title AS ProjectTitle,
                                                T2.Title AS Milestone
					FROM cpo_project_tasks T
					LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                                        LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
					WHERE
						T.Deleted = 0
						AND T.Milestone = 0
						AND T.Status < 2
                        AND T.Date_End >= '".convertMySQLToGMT(date('Y-m-d 00:00:00'),'push','Y-m-d H:i:s')."'
                        AND T.Date_End < '".convertMySQLToGMT(date('Y-m-d 23:59:00'),'push','Y-m-d H:i:s')."'
                        AND T.Date_End IS NOT NULL
                        AND T.Date_End <> '0000-00-00 00:00:00'
                        AND T.Userid = '".$accountUserid."'
					ORDER BY
						T.Date_End";
		} else {
            /*
             * Get only tasks assigned to this user
             */
            $sql2 = "SELECT
						T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
						P.ProjectID,P.Title AS ProjectTitle,
                                                T2.Title AS Milestone
					FROM cpo_project_tasks T
					LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                                        LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
                    LEFT JOIN cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
					WHERE
						T.Deleted = 0
						AND T.Milestone = 0
						AND T.Status < 2
                        AND T.Date_End >= '".convertMySQLToGMT(date('Y-m-d 00:00:00'),'push','Y-m-d H:i:s')."'
                        AND T.Date_End < '".convertMySQLToGMT(date('Y-m-d 23:59:00'),'push','Y-m-d H:i:s')."'
                        AND T.Date_End IS NOT NULL
                        AND T.Date_End <> '0000-00-00 00:00:00'
                        AND UT.MemberID = '".$pid."'
					ORDER BY
						T.Date_End";
		}

        if ($sql2 != NULL) {
            $query = $this->db->query($sql2);
            $resultArray = $query->result_array();
            if (sizeof($resultArray)>0) {
                $totalTasks = $totalTasks+$query->num_rows();
                $taskArray['TasksToday'] = $resultArray;
            }
        }

        /*
         * Get tasks for tomorrow
         */
        $tomorrowDate = date('Y-m-d',(time()+86400));
        if ($viewAll == TRUE) {
            /*
             * Get MySQL formatted date for tomorrow
             */            
            $sql3 = "SELECT
                                    T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
                                    P.ProjectID,P.Title AS ProjectTitle,
                                    T2.Title AS Milestone
                            FROM cpo_project_tasks T
                            LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                            LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
                            WHERE
                                    T.Deleted = 0
                                    AND T.Milestone = 0
            AND T.Userid = '".$accountUserid."'
                                    AND T.Status < 2
            AND T.Date_End >= '".convertMySQLToGMT($tomorrowDate,'push','Y-m-d')." 00:01:00'
            AND T.Date_End <= '".convertMySQLToGMT($tomorrowDate,'push','Y-m-d')." 23:59:00'
            AND T.Date_End IS NOT NULL
            AND T.Date_End <> '0000-00-00 00:00:00'
                            ORDER BY
                                    T.Date_End";
		} else {
            /*
             * Get only tasks assigned to this user
             */
            $sql3 = "SELECT
						T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
						P.ProjectID,P.Title AS ProjectTitle,
                                                T2.Title AS Milestone
					FROM cpo_project_tasks T
					LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                                        LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
                    LEFT JOIN cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
					WHERE
						T.Deleted = 0
						AND T.Milestone = 0
                        AND UT.MemberID = '".$pid."'
						AND T.Status < 2
                        AND T.Date_End >= '".convertMySQLToGMT($tomorrowDate,'push','Y-m-d')." 00:01:00'
                        AND T.Date_End <= '".convertMySQLToGMT($tomorrowDate,'push','Y-m-d')." 23:59:00'
                        AND T.Date_End IS NOT NULL
                        AND T.Date_End <> '0000-00-00 00:00:00'
					ORDER BY
						T.Date_End";
		}

        if ($sql3 != NULL) {
            $query = $this->db->query($sql3);
            $resultArray = $query->result_array();
            if (sizeof($resultArray)>0) {
                $totalTasks = $totalTasks+$query->num_rows();
                $taskArray['TasksTomorrow'] = $resultArray;
            }
        }

        /*
         * Get tasks for this week
         */
        $today = convertMySQLToGMT(date('Y-m-d'),'pull','Y-m-d');
        $weekArray = weekFromDate($today,NULL,'S');
        $dateWeekStart = $weekArray['weekDays'][0]['date'];
        $dateWeekEnd   = $weekArray['weekDays'][6]['date'];
        if ($viewAll == TRUE) {
			/*
             * Get dates for this week
             */            
			$sql4 = "SELECT
						T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
						P.ProjectID,P.Title AS ProjectTitle,
                                                T2.Title AS Milestone
					FROM cpo_project_tasks T
					LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                                        LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
					WHERE
						T.Deleted = 0
						AND T.Milestone = 0
                        AND T.Userid = '".$accountUserid."'
						AND T.Status < 2
                        AND T.Date_End >= '".$dateWeekStart."'
                        AND T.Date_End <= '".$dateWeekEnd."'
                        AND T.Date_End IS NOT NULL
                        AND T.Date_End <> '0000-00-00 00:00:00'
					ORDER BY
						T.Date_End";
		} else {
            /*
             * Get only tasks assigned to this user
             */
            $sql4 = "SELECT
						T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
						P.ProjectID,P.Title AS ProjectTitle,
                                                T2.Title AS Milestone
					FROM cpo_project_tasks T
					LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                                        LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
                    LEFT JOIN cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
					WHERE
						T.Deleted = 0
						AND T.Milestone = 0
                        AND UT.MemberID = '".$pid."'
						AND T.Status < 2
                        AND T.Date_End >= '".$dateWeekStart."'
                        AND T.Date_End <= '".$dateWeekEnd."'
                        AND T.Date_End IS NOT NULL
                        AND T.Date_End <> '0000-00-00 00:00:00'
					ORDER BY
						T.Date_End";
		}

        if ($sql4 != NULL) {
            $query = $this->db->query($sql4);
            $resultArray = $query->result_array();
            if (sizeof($resultArray)>0) {
                $totalTasks = $totalTasks+$query->num_rows();
                $taskArray['TasksThisWeek'] = $resultArray;
            }
        }

        /*
         * Get tasks for next week
         */
        $dateInNextWeek = date('Y-m-d',(time()+604800)).' 12:00:00';
        $weekArray = weekFromDate($dateInNextWeek,NULL,'S');
        $dateWeekStart = $weekArray['weekDays'][0]['date'];
        $dateWeekEnd   = $weekArray['weekDays'][6]['date'];
        if ($viewAll == TRUE) {
			/*
             * Get dates for next week
             */            
			$sql5 = "SELECT
						T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
						P.ProjectID,P.Title AS ProjectTitle,
                                                T2.Title AS Milestone
					FROM cpo_project_tasks T
					LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                                        LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
					WHERE
						T.Deleted = 0
						AND T.Milestone = 0
                        AND T.Userid = '".$accountUserid."'
						AND T.Status < 2
                        AND T.Date_End >= '".$dateWeekStart."'
                        AND T.Date_End <= '".$dateWeekEnd."'
                        AND T.Date_End IS NOT NULL
                        AND T.Date_End <> '0000-00-00 00:00:00'
					ORDER BY
						T.Date_End";
		} else {
            /*
             * Get only tasks assigned to this user
             */
            $sql5 = "SELECT
						T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
						P.ProjectID,P.Title AS ProjectTitle,
                                                T2.Title AS Milestone
					FROM cpo_project_tasks T
					LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                                        LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
                    LEFT JOIN cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
					WHERE
						T.Deleted = 0
						AND T.Milestone = 0
                        AND UT.MemberID = '".$pid."'
						AND T.Status < 2
                        AND T.Date_End >= '".$dateWeekStart."'
                        AND T.Date_End <= '".$dateWeekEnd."'
                        AND T.Date_End IS NOT NULL
                        AND T.Date_End <> '0000-00-00 00:00:00'
					ORDER BY
						T.Date_End";
		}

        if ($sql5 != NULL) {
            $query = $this->db->query($sql5);
            $resultArray = $query->result_array();
            if (sizeof($resultArray)>0) {
                $totalTasks = $totalTasks+$query->num_rows();
                $taskArray['TasksNextWeek'] = $resultArray;
            }
        }
        $taskArray['TotalTasks'] = $totalTasks;
		return $taskArray;
	}

	function checkForValidTask($taskID,$userid,$accountUserid,$viewAll=FALSE) {
		$valid = FALSE;
		if ($viewAll == FALSE) {
			/*
			 * Make sure this task is assigned to userid
			 */
			$sql = "SELECT * FROM cpo_linkUIDTask WHERE MemberID = '$userid' AND TaskID = '$taskID'";
			$query = $this->db->query($sql);
			if ($query->num_rows()>0) {
				$valid = TRUE;
			}
		} else {
			/*
			 * Make sure this task belongs to accountUserid
			 */
			$sql = "SELECT * FROM cpo_project_tasks WHERE TaskID = '$taskID' AND Userid = '$accountUserid'";
			$query = $this->db->query($sql);
			if ($query->num_rows()>0) {
				$valid = TRUE;
			}
		}
		return $valid;
	}

    function getAllUserTasks($userid) {
        $sql = "SELECT
                    PR.ProjectID AS MainProjectID,PR.Title AS ProjectTitle,
                    PT.TaskID,PT.Userid,PT.ParentTaskID AS MilestoneID,PT.ProjectID,PT.Title AS TaskTitle,
                    PT.Description,PT.Date_Start,PT.Date_End,PT.Date_Completed,PT.HoursEstimated,
                    PT.HoursActual,PT.Status,PT.Priority,PT.TaskNo,PT.Milestone,PT.DisplayOrder,
                    PT.Tags,PT.CalendarID,PT.UseridEntry,PT.DateEntry,PT.DateUpdate,
                    P.PID,P.LoginUserid AS UID,P.NameFirst,P.NameLast,P.Language,
                    CP.PID AS CreatorPID,CP.NameFirst AS CreatorNameFirst,CP.NameLast AS CreatorNameLast
                FROM 
                    cpo_project_tasks PT
                LEFT JOIN
                    cpo_project PR ON PT.ProjectID = PR.ProjectID
                LEFT JOIN 
                    cpo_linkUIDTask UT ON UT.TaskID = PT.TaskID
                LEFT JOIN
                    cpo_people P ON P.LoginUserid = UT.MemberID
                LEFT JOIN
                    cpo_people CP ON CP.LoginUserid = PT.UseridEntry
                WHERE 
                    UT.MemberID = '$userid'
                    ORDER BY PT.ProjectID,PT.Date_Start";
        $query = $this->db->query($sql);
        $resultArray = $query->result_array();
        $taskArray = Task_view::createTaskArrayAllUserTasks($resultArray);

        return $taskArray;
    }

    function getTaskFromPrevTaskID($prevTaskID,$accountUserid) {
        $sql = "SELECT * from cpo_project_tasks WHERE PrevTaskID = '$prevTaskID' AND Userid = '$accountUserid'";
        $query = $this->db->query($sql);
		if ($query->num_rows()<1) {
    		return false;
    	} else {
            return $query->result_array();
    	}
    }

    /*
     * Get userid's of all people associated with overdue tasks.
     */
    function getOverdueTaskUserids($dateOneWeekAgo) {
        $sql = "SELECT
                    DISTINCT(L.MemberID),P.Language,LO.AuthString,LO.UID,
                    A.IsPaid,A.DateSignup,A.IsActive
                FROM cpo_project_tasks T
                LEFT JOIN cpo_linkUIDTask L ON T.TaskID = L.TaskID
                LEFT JOIN cpo_people P ON P.PID = L.MemberID
                LEFT JOIN cpo_settings S ON S.Userid = P.LoginUserid
                LEFT JOIN cpo_login LO ON LO.UID = P.LoginUserid
                LEFT JOIN cpo_account_profile A ON A.AccountID = LO.UseridAccount
                WHERE
                    A.IsActive = 1 AND
                    S.Setting = 'emailReceiveTaskOverdue' AND
                    S.SettingValue = 1 AND
                    T.Status <> 2 AND
                    T.Status <> 3 AND
                    T.Date_End < '".date('Y-m-d')."' AND
                    T.Date_End IS NOT NULL AND
                    T.Date_End <> '0000-00-00 00:00:00' AND
                    (
                        T.DateOverdueNotify < '$dateOneWeekAgo' OR
                        T.DateOverdueNotify = '0000-00-00' OR
                        T.DateOverdueNotify IS NULL
                    ) AND
                    L.MemberID > 0 AND
                    (
                        T.Date_Completed IS NULL OR
                        T.Date_Completed = '0000-00-00'
                    )";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getOverdueTasksByMemberID($memberID) {
        $sql = "SELECT T.TaskID,T.Title,T.Date_End,T.TaskNo,I.ContactData AS Email,P.Title AS Project
                FROM cpo_project_tasks T
                LEFT JOIN cpo_linkUIDTask L ON L.TaskID = T.TaskID
                LEFT JOIN cpo_people_contact_info I ON I.PID = '$memberID'
                LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                WHERE
                    L.MemberID = '$memberID' AND
                    I.ContactType = 'email' AND
                    T.Status <> 2 AND
                    T.Status <> 3 AND
                    T.Date_End < '".date('Y-m-d')."' AND
                    T.Date_End IS NOT NULL AND
                    T.Date_End <> '0000-00-00 00:00:00' AND
                    (
                        T.Date_Completed IS NULL OR
                        T.Date_Completed = '0000-00-00'
                    ) AND
                    T.Deleted = 0 AND
                    P.Deleted = 0
                ORDER BY P.ProjectID,T.TaskID";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*
     * Get userid's of all people associated with overdue tasks.
     */
    function getUpcomingTaskUserids($dateOneWeekFromNow) {
        $sql = "SELECT
                    DISTINCT(L.MemberID),P.Language,LO.AuthString,
                    A.IsPaid,A.DateSignup,A.IsActive
                FROM cpo_project_tasks T
                LEFT JOIN cpo_linkUIDTask L ON T.TaskID = L.TaskID
                LEFT JOIN cpo_people P ON P.PID = L.MemberID
                LEFT JOIN cpo_settings S ON S.Userid = P.LoginUserid
                LEFT JOIN cpo_login LO ON LO.UID = P.LoginUserid
                LEFT JOIN cpo_account_profile A ON A.AccountID = LO.UseridAccount
                WHERE
                    S.Setting = 'emailReceiveTaskOverdue' AND
                    S.SettingValue = 1 AND
                    T.Date_End >= '".date('Y-m-d')."' AND
                    T.Date_End <= '$dateOneWeekFromNow' AND
                    T.Date_End IS NOT NULL AND
                    T.Date_End <> '0000-00-00 00:00:00' AND
                    L.MemberID > 0 AND
                    (
                        T.Date_Completed IS NULL OR
                        T.Date_Completed = '0000-00-00'
                    )";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getUpcomingTasksByUserid($userid,$dateOneWeekFromNow) {
        $sql = "SELECT T.TaskID,T.Title,T.Date_End,T.TaskNo,I.ContactData AS Email,P.Title AS Project
                FROM cpo_project_tasks T
                LEFT JOIN cpo_linkUIDTask L ON L.TaskID = T.TaskID
                LEFT JOIN cpo_people_contact_info I ON I.PID = '$userid'
                LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                WHERE
                    L.MemberID = '$userid' AND
                    I.ContactType = 'email' AND
                    T.Date_End >= '".date('Y-m-d')."' AND
                    T.Date_End <= '$dateOneWeekFromNow' AND
                    T.Date_End IS NOT NULL AND
                    T.Date_End <> '0000-00-00 00:00:00' AND
                    (
                        T.Date_Completed IS NULL OR
                        T.Date_Completed = '0000-00-00'
                    ) AND
                    T.Deleted = 0 AND
                    P.Deleted = 0
                ORDER BY P.ProjectID,T.TaskID";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    function countUserTasks($pid,$viewAll,$status=null,$accountUserid=null,$viewAllProjects=TRUE) {
        /*
         * STATUS: 0=Not Started,1=In Progress,2=Completed,3=On Hold,4=Overdue
         */
        if ($status != null) {
            $whereStatus = ' AND (';
            foreach(explode('|',$status) as $statusID) {
                $whereStatus .= "PT.Status = '$statusID' OR ";
            }	
            $whereStatus = substr($whereStatus,0,-3);
            $whereStatus .= ') ';
        }
        
        if ($viewAll == TRUE) {
            $whereMember = " PT.Userid = '$accountUserid' ";
        } else {
            $whereMember = " UT.MemberID = '$pid' ";
        }
        
        if ($viewAllProjects == FALSE) {
            $joinProjects = " LEFT JOIN cpo_linkPeopleItemType PIT ON PIT.PeopleID = $pid ";
            $whereProjects = " AND PIT.ItemID = PT.ProjectID AND PIT.ItemType = 'P' ";
        } else {
            $joinProjects = '';
            $whereProjects = '';
        }
        
        $sql = "SELECT COUNT(DISTINCT(PT.TaskID)) AS TotalTasks
                FROM 
                    cpo_project_tasks PT
                LEFT JOIN 
                    cpo_linkUIDTask UT ON UT.TaskID = PT.TaskID
                    $joinProjects
                WHERE 
                    PT.Milestone = 0 AND
                    PT.Deleted = 0 AND
                    ".$whereMember.$whereStatus.$whereProjects;
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['TotalTasks'];
    }
    
    function getAllUserTasksByStatus($pid,$viewAll,$status=null,$accountUserid,$viewAllProjects=TRUE) {
        /*
         * STATUS: 0=Not Started,1=In Progress,2=Completed,3=On Hold,4=Overdue
         */
        if ($status != null) {
            $whereStatus = ' AND (';
            foreach(explode('|',$status) as $statusID) {
                    $whereStatus .= "T.Status = '$statusID' OR ";
            }	
            $whereStatus = substr($whereStatus,0,-3);
            $whereStatus .= ') ';
        }
        
        if ($viewAllProjects == FALSE) {
            $joinProjects = " LEFT JOIN cpo_linkPeopleItemType PIT ON PIT.PeopleID = $pid ";
            $whereProjects = " AND PIT.ItemID = T.ProjectID AND PIT.ItemType = 'P' ";
        } else {
            $joinProjects = '';
            $whereProjects = '';
        }
        
        if ($viewAll == TRUE) {
            $sql = "SELECT
                    T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
                    P.ProjectID,P.Title AS ProjectTitle,
                    T2.Title AS Milestone
                    FROM cpo_project_tasks T
                    LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                    LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
                    $joinProjects
                    WHERE
                    T.Deleted = 0
                    AND T.Milestone = 0
                    AND T.Userid = '".$accountUserid."'
                    $whereStatus
                    $whereProjects
                    ORDER BY
                    ProjectTitle";
                    // changed from T.Date_End for mobile app
        } else {
            /*
             * Get only tasks assigned to this user
             */
            $sql = "SELECT
                    T.TaskID,T.ParentTaskID,T.Title,T.Description,T.Date_Start,T.Date_End,T.TaskNo,T.Status,T.HoursEstimated,T.HoursActual,
                    P.ProjectID,P.Title AS ProjectTitle,
                    T2.Title AS Milestone
                    FROM cpo_project_tasks T
                    LEFT JOIN cpo_project P ON P.ProjectID = T.ProjectID
                    LEFT JOIN cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
                    LEFT JOIN cpo_project_tasks T2 ON T2.TaskID = T.ParentTaskID
                    $joinProjects
                    WHERE
                        T.Deleted = 0
                        AND T.Milestone = 0
                        AND UT.MemberID = '".$pid."'
                        $whereProjects
			$whereStatus
                    ORDER BY
                    ProjectTitle";
                    // changed from T.Date_End for mobile app
        }
        $query = $this->db->query($sql);
        $resultArray = $query->result_array();
        return $resultArray;
    }
    
    /*
     * getTaskCountForDateRange: gets a count of completed and incomplete
     * tasks for a user over a date range. To count, a task must be assigned
     * an end date within the date range. If no end date is assigned, use the
     * start date.
     */
    function getTaskCountForDateRange($dateStart,$dateEnd,$pid,$viewAll,$accountUserid) {
        if ($viewAll == TRUE) {
            $sqlComplete = "SELECT COUNT(T.TaskID) AS Complete 
                    FROM cpo_project_tasks T
                    WHERE
                        T.Deleted = 0 AND
                        T.Milestone = 0 AND
                        T.Userid = '".$accountUserid."' AND
                        T.Status = 2 AND
                        (
                            (T.Date_Start >= '$dateStart' AND T.Date_End <= '$dateEnd') OR
                            (T.Date_Start = '0000-00-00 00:00:00' AND (T.Date_End >= '$dateStart' AND T.Date_End <= '$dateEnd'))
                        )";
            $sqlIncomplete = "SELECT COUNT(T.TaskID) AS Incomplete
                    FROM cpo_project_tasks T
                    WHERE
                        T.Deleted = 0 AND
                        T.Milestone = 0 AND
                        T.Userid = '".$accountUserid."' AND
                        (T.Status = 0 OR T.Status = 1 OR T.Status = 4) AND
                        (
                            (T.Date_Start >= '$dateStart' AND T.Date_End <= '$dateEnd') OR
                            (T.Date_Start = '0000-00-00 00:00:00' AND (T.Date_End >= '$dateStart' AND T.Date_End <= '$dateEnd'))
                        )";
        } else {
            $sqlComplete = "SELECT COUNT(T.TaskID) AS Complete
                    FROM cpo_project_tasks T
                    LEFT JOIN cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
                    WHERE
                        T.Deleted = 0 AND
                        T.Milestone = 0 AND
                        UT.MemberID = '".$pid."' AND
                        T.Status = 2 AND
                        (
                            (T.Date_Start >= '$dateStart' AND T.Date_End <= '$dateEnd') OR
                            (T.Date_Start = '0000-00-00 00:00:00' AND (T.Date_End >= '$dateStart' AND T.Date_End <= '$dateEnd'))
                        )";
            $sqlIncomplete = "SELECT COUNT(T.TaskID) AS Incomplete
                    FROM cpo_project_tasks T
                    LEFT JOIN cpo_linkUIDTask UT ON UT.TaskID = T.TaskID
                    WHERE
                        T.Deleted = 0 AND
                        T.Milestone = 0 AND
                        UT.MemberID = '".$pid."' AND
                        (T.Status = 0 OR T.Status = 1 OR T.Status = 4) AND
                        (
                            (T.Date_Start >= '$dateStart' AND T.Date_End <= '$dateEnd') OR
                            (T.Date_Start = '0000-00-00 00:00:00' AND (T.Date_End >= '$dateStart' AND T.Date_End <= '$dateEnd'))
                        )";
        }
        $queryComplete = $this->db->query($sqlComplete);
        $resultCompleteArray = $queryComplete->row_array();
        
        $queryIncomplete = $this->db->query($sqlIncomplete);
        $resultIncompleteArray = $queryIncomplete->row_array();
        
        $finalArray = array(
            'Complete' => $resultCompleteArray['Complete'],
            'Incomplete' => $resultIncompleteArray['Incomplete']
        );
        return $finalArray;
    }
}
?>