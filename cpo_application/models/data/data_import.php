<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Data_import extends Model {

    function __construct() {
        // Call the Model constructor
        parent::Model();
    }

    function checkForImportGUID($accountUserid) {
        $sql = "SELECT * FROM cpo_import_guid WHERE Userid = '$accountUserid'";
        $query = $this->db->query($sql);
        if ($query->num_rows()<1) {
    		return false;
    	} else {
    		return $query->result_array();
    	}
    }

    function deletePreviousImportData($importGUID) {
        $tableArray = array(
            'cpo_clients',
            'cpo_linkPeopleItemType',
            'cpo_login',
            'cpo_main_cat',
            'cpo_people',
            'cpo_people_contact_info',
            'cpo_project',
            'cpo_project_tasks',
            'cpo_invoice',
            'cpo_invoice_items',
            'cpo_invoice_payment',
            'cpo_project_time',
            'cpo_notes',
            'cpo_calendar_events',
            'cpo_expense',
            'cpo_files',
            'cpo_linkFileItemType'
        );

        for($a=0;$a<=count($tableArray)-1;$a++) {
            $sql = "DELETE from ".$tableArray[$a]." WHERE ImportGUID = '$importGUID'";
            $query = $this->db->query($sql);
        }
    }

    function saveImportGUID($data) {
        if ($data['action'] == 'add') {
            $sql = "INSERT into cpo_import_guid (
                    ImportGUID,
                    Userid,
                    DateCreated
                    ) values (
                    '".$data['importGUID']."',
                    '".$data['accountUserid']."',
                    '".date('Y-m-d G:i:s')."'
                    )";
        } else {
            $sql = "UPDATE cpo_import_guid SET
                    ImportGUID  = '".$data['importGUID']."',
                    DateCreated = '".date('Y-m-d G:i:s')."'
                    WHERE
                    Userid = '".$data['accountUserid']."'";
        }
        $query = $this->db->query($sql);
    }
}