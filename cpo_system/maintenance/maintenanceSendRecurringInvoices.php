<?php
/* require_once "invoice email template"
 *
 * Frequency: 1 per day at midnight.
 *
 * This file will handle the cron task for sending recurring
 * invoices.
 *
 * 1.  Find which invoices are to be recurring.
 * 2.  Check how often each one recurrs.
 * 3.  Find out the last time it was sent.
 * 4.  If last_time_sent = recurring_days then send another one
 * 5.  Duplicate invoice information in the invoice database
 *     but with a new invoice number.
 */
mail('jeff@upstart-productions.com', 'Your invoice', 'Your recurring invoice has been sent.', 'From: MyCPO Admin <webmaster@mycpohq.com>\n', '-fwebmaster@mycpohq.com'); //mail command

?>