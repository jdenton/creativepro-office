<?php
/* require_once "email_language_file.php"
 *
 * Frequency: 1 per day at midnight.
 *
 * This file will handle the cron task for task email reminders.
 * We are going to send email reminders to task owners xx days
 * before a task is due. The number of days should be variable or
 * set by the CPO user.
 *
 * 1.  Find all tasks in database whos due date > today.
 * 2.  Look for task owners info and find settings:
 *     a. If they want to be notified
 *	   b. How many days prior to due date to be notified
 *     c. Email address
 * 4.  Does b = today?
 * 5.  Send email to task owner contained in email_language_file
 */
mail('jeff@upstart-productions.com', 'Task reminder', 'This is your task reminder. Your task is due tomorrow.', 'From: MyCPO Admin <webmaster@mycpohq.com>\n', '-fwebmaster@mycpohq.com'); //mail command


?>
