<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Download Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/download_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Force Download
 *
 * Generates headers that force a download to happen
 *
 * @access	public
 * @param	string	filename
 * @param	mixed	the data to be downloaded
 * @return	void
 */	
if ( ! function_exists('force_download'))
{
	function force_download($filename = '', $data = '')
	{
		if ($filename == '' OR $data == '')
		{
			return FALSE;
		}

		// Try to determine if the filename includes a file extension.
		// We need it in order to set the MIME type
		if (FALSE === strpos($filename, '.'))
		{
			return FALSE;
		}
	
		// Grab the file extension
		$x = explode('.', $filename);
		$extension = end($x);

		// Load the mime types
		@include(APPPATH.'config/mimes'.EXT);
	
		// Set a default mime if we can't find it
		if ( ! isset($mimes[$extension]))
		{
			$mime = 'application/octet-stream';
		}
		else
		{
			$mime = (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
		}
		//echo 'Mime: '.$mime;
	
		// Generate the server headers
		if (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE"))
		{
			header('Content-Type: '.$mime.';');
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header("Content-Transfer-Encoding: binary");
			header('Pragma: public');
			header("Content-Length: ".strlen($data));
		}
		else
		{
			header('Content-Type: '.$mime.';');
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header("Content-Transfer-Encoding: binary");
			header('Expires: 0');
			header('Pragma: no-cache');
			header("Content-Length: ".strlen($data));
		}
	
		exit($data);
	}
}

function iCalHeader($filename=NULL) {
    if (empty($filename)) {
        $filename = 'calendar.ics';
    }
    //header('Content-type: text/calendar; method=PUBLISH');
  	//header('Content-Disposition: attachment; filename="'.$filename.'"');

    header("Content-Type: text/Calendar");
    header("Content-Disposition: inline; filename=my_ical.ics");
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

}

function ob_clean_all() {
	$ob_active = ob_get_length ()!== FALSE;
	while($ob_active) {
		ob_end_clean();
		$ob_active = ob_get_length ()!== FALSE;
	}
	return FALSE;
}

/**
 * Force Download File
 *
 * Generates headers that force a download to happen
 * WITHOUT streaming the file to memory and crashing
 * the f***ing server first.
 *
 * @access	public
 * @param	string	fileName
 * @param   string  filePath
 * @param   string  fileSize
 * @param   string  fileExt
 * @return	void
 */
if ( ! function_exists('force_download_file'))
{
	function force_download_file($fileName, $filePath, $fileSize, $fileExt) {
		//Required for IE, otherwise Content-disposition is ignored
		if(ini_get('zlib.output_compression'))
		ini_set('zlib.output_compression', 'Off');

		switch($fileExt) {
			case "pdf": $ctype="application/pdf"; break;
			case "exe": $ctype="application/octet-stream"; break;
			case "zip": $ctype="application/zip"; break;
			case "doc": $ctype="application/msword"; break;
			case "xls": $ctype="application/vnd.ms-excel"; break;
			case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
			case "gif": $ctype="image/gif"; break;
			case "png": $ctype="image/png"; break;
			case "jpe": case "jpeg":
			case "jpg": $ctype="image/jpg"; break;
			case "txt": $ctype="text/plain"; break;
			case "mp3": $ctype="audio/mpeg"; break;
			case "wav": $ctype="audio/x-wav"; break;
			case "mpg": case "mpeg":
			case "mpe": $ctype="video/mpeg"; break;
			case "mov": $ctype="video/quicktime"; break;
			case "avi": $ctype="video/x-msvideo"; break;
			case "txt": $ctype="text/plain"; break;
			default: $ctype="application/force-download";
		}
		if (file_exists($filePath)) {
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			header("Content-Type: $ctype");
			header("Content-Disposition: attachment; filename=" . $fileName . ";" );
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".$fileSize);
			ob_clean();
			flush();
			readfile($filePath);
			exit;
		} else {
			echo $filePath;
		}
	}
}
/* End of file download_helper.php */
/* Location: ./system/helpers/download_helper.php */