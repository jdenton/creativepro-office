<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('createWidgetFrame')) {
	function createWidgetFrame($widgetName,$widgetID,$size=NULL)	{
        $CI =& get_instance();
        if ($CI->session->userdata('userType') == USER_TYPE_CLIENT) {
            $userType = 'client';
        } else {
            $userType = 'user';
        }
		$widgetString  = '<li class="widget" widgetName="'.$widgetName.'" id="widget_'.$widgetID.'" widgetUser="'.$userType.'">';
		$widgetString .= '<div class="widgetHead">';
	    $widgetString .= '<div class="widgetIcon_'.$widgetName.'"><h3>'.lang('widget_'.$widgetName).'</h3></div>';
	    $widgetString .= '<div class="widgetControls"></div>';
	    $widgetString .= '</div>';
		$widgetString .= '<div class="widgetContent windowFrame dashboardWidget">';
		$widgetString .= '<p id="widgetContent_'.$widgetName.'"></p>';
		$widgetString .= '</div>';
		$widgetString .= '</li>';	
		
		return $widgetString;
	}
}

function availableWidgets() {
	$widgetArray = array(
						array(
							'WID'        => '1',
							'WName'      => 'Invoices',
							'WidgetName' => 'invoices',
                            'PermIndex'  => array('invoiceViewAll','invoiceViewOwn'),
                            'InFooter'   => 1,
                            'Client'     => 1
						 ),
						 array(
							'WID'        => '3',
							'WName'      => 'Calendar',
							'WidgetName' => 'calendar',
                            'PermIndex'  => array('calendarViewAll','calendarViewOwn'),
                            'InFooter'   => 1,
                            'Client'     => 1
						 ),						
						 array(
							'WID'        => '5',
							'WName'      => 'Notes',
							'WidgetName' => 'notes',
                            'PermIndex'  => array(),
                            'InFooter'   => 1,
                            'Client'     => 1
						 ),
						 array(
							'WID'        => '7',
							'WName'      => 'Projects',
							'WidgetName' => 'projects',
                            'PermIndex'  => array('projectViewAll','projectViewOwn'),
                            'InFooter'   => 1,
                            'Client'     => 1
						 ),
						array(
							'WID'        => '8',
							'WName'      => 'Tasks',
							'WidgetName' => 'tasks',
                            'PermIndex'  => array('taskViewAll','taskViewOwn'),
                            'InFooter'   => 1,
                            'Client'     => 0
						 ),
                         /*
                         array(
							'WID'        => '4',
							'WName'      => 'RSS Feeds',
							'WidgetName' => 'rss',
                            'InFooter'   => 0,
                            'Client'     => 0
						 ),
						 array(
							'WID'        => '9',
							'WName'      => 'Messages',
							'WidgetName' => 'messages',
                            'InFooter'   => 0,
                            'Client'     => 1
						 ),
						array(
							'WID'        => '10',
							'WName'      => 'Reports',
							'WidgetName' => 'reports',
                            'InFooter'   => 0,
                            'Client'     => 0
						 ),
                         */
						array(
							'WID'        => '14',
							'WName'      => 'Job Timer',
							'WidgetName' => 'timer',
                            'PermIndex'  => array(),
                            'InFooter'   => 1,
                            'Client'     => 0
						 )
	);

	return $widgetArray;
}
?>