<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Currency array, this is where we define our currency selection.
 * Look at http://www.xe.com/symbols.php to find others.
 */
function currencies() {
    $currencies['ALL'] = array('Albania, Leke','Lek');
	$currencies['ARS'] = array('Argentina, Pesos','$');
	$currencies['AUD'] = array('Australia, Dollars','$');
    $currencies['BYR'] = array('Belarus, Rubles','p.');
	$currencies['BRL'] = array('Brazil, Reais','R$');
    $currencies['BGN'] = array('Bulgaria, Leva','&#1083;&#1074;');
    $currencies['KHR'] = array('Cambodia, Riels','f');
	$currencies['CAD'] = array('Canada, Dollars','$');
	$currencies['CNY'] = array('China, Yuan Renminbi','&yen;');
    $currencies['CLP'] = array('Chile, Pesos','$');
    $currencies['COP'] = array('Colombia, Pesos','$');
    $currencies['CRC'] = array('Costa Rica, Colón','&#8353;');
    $currencies['HRK'] = array('Croatia, Kuna','kn');
    $currencies['CUP'] = array('Cuba, Pesos','&#8369;');
	$currencies['CZK'] = array('Czech Republic, Koruny','K&#269;');
    $currencies['DKK'] = array('Denmark, Kroner','kr');
    $currencies['DOP'] = array('Dominican Republic, Pesos','RD$');
    $currencies['EGP'] = array('Egypt, Pounds','&pound;');
    $currencies['SVC'] = array('El Salvador, Colones','$');
	$currencies['GBP'] = array('England (United Kingdom), Pounds','&#xA3;');
    $currencies['EEK'] = array('Estonia, Krooni','kr');
    $currencies['EUR'] = array('Euro','&euro;');
	$currencies['HKD'] = array('Hong Kong, Dollars','$');
	$currencies['HUF'] = array('Hungary, Forint','Ft');
    $currencies['ISK'] = array('Iceland, Kronur','kr');
    $currencies['ILS'] = array('Israel, New Sheqel','&#8362;');
    $currencies['INR'] = array('India, Rupees','Rp');
    $currencies['IDR'] = array('Indonesia, Rupiahs','Rp');
    $currencies['JMD'] = array('Jamaica, Dollars','J$');
	$currencies['JPY'] = array('Japan, Yen','&yen;');
	$currencies['MYR'] = array('Malaysia, Ringgits','RM');
	$currencies['MXN'] = array('Mexico, Pesos','$');
    $currencies['NOK'] = array('Norway, Krone','kr');
    $currencies['NZD'] = array('New Zealand, Dollar','$');
    $currencies['PHP'] = array('Philippines, Pesos','Php');
	$currencies['PLN'] = array('Poland, Zlotych','z'.'&#322;');
    $currencies['RON'] = array('Romania, New Lei','lei');
    $currencies['RUB'] = array('Russian, Rubles','&#1088;&#1091;&#1073;');
	$currencies['RSD'] = array('Serbia, Dinars','&#1044;&#1080;&#1085;.');
    $currencies['SGD'] = array('Singapore, Dollars','$');
    $currencies['ZAR'] = array('South Africa, Rand','R');
    $currencies['KRW'] = array('South Korea, Won','&#8361;');
    $currencies['SEK'] = array('Sweden, Kronor','kr');
    $currencies['CHF'] = array('Switzerland, Francs','CHF');
    $currencies['SYP'] = array('Syria, Pounds','&pound;');
    $currencies['TWD'] = array('Taiwan, New Dollars','NT$');
    $currencies['THB'] = array('Thailand, Baht','&#3647;');
    $currencies['TRY'] = array('Turkey, Lira','TL');
    $currencies['UAH'] = array('Ukraine, Hryvnia','&#8372;');
    $currencies['UYU'] = array('Uruguay, Pesos','$U');
    $currencies['AED'] = array('United Arab Emirates, Dirham','&#1583;.&#1573;');
    $currencies['USD'] = array('United States, Dollar','$');
	$currencies['UZS'] = array('Uzbekistan, Sums','&#1083;&#1074;');
    $currencies['VEF'] = array('Venezuela, Bolivares Fuertes','Bs');

	return $currencies;
}

function currenciesPayPal() {
    $currencies['AUD'] = array('Australia, Dollars','$');
    $currencies['CAD'] = array('Canada, Dollars','$');
    $currencies['CZK'] = array('Czech Republic, Koruna','K&#269;');
    $currencies['DKK'] = array('Denmark, Kroner','kr');
    $currencies['EUR'] = array('Euro','&euro;');
    $currencies['HKD'] = array('Hong Kong, Dollars','$');
    $currencies['HUF'] = array('Hungary, Forint','Ft');
    $currencies['ILS'] = array('Israel, New Sheqel','&#8362;');
    $currencies['JPY'] = array('Japan, Yen','&yen;');
    $currencies['MXN'] = array('Mexico, Pesos','$');
    $currencies['NOK'] = array('Norway, Krone','kr');
    $currencies['NZD'] = array('New Zealand, Dollar','$');
    $currencies['PHP'] = array('Philippines, Pesos','Php');
    $currencies['PLN'] = array('Poland, Zlotych','z'.'&#322;');
    $currencies['GBP'] = array('England (United Kingdom), Pounds','&pound;');
    $currencies['SGD'] = array('Singapore, Dollars','$');
    $currencies['SEK'] = array('Sweden, Kronor','kr');
    $currencies['CHF'] = array('Switzerland, Francs','CHF');
    $currencies['TWD'] = array('Taiwan, New Dollars','NT$');
    $currencies['THB'] = array('Thailand, Baht','&#3647;');
    $currencies['USD'] = array('United States, Dollar','$');

    return $currencies;
}

if ( ! function_exists('getCurrencyMark')) {
	function getCurrencyMark($currencyCode = 'USD')	{
		$currencyArray = currencies();
		return $currencyArray[$currencyCode][1];
	}
}

/**
 * Currency Menu
 *
 * Generates a drop-down menu of currencies.
 *
 * @access	public
 * @param	string	default industry
 * @param	string	classname
 * @param	string	menu name
 * @return	string
 */
if ( ! function_exists('currency_menu')) {
	function currency_menu($default = "", $class = "", $name = 'currencies')
	{
		$menu = '<select name="'.$name.'" id="'.$name.'"';

		if ($class != '')
		{
			$menu .= ' class="'.$class.'"';
		}

		$menu .= ">\n";
        $menu .= "<option></option>";

		foreach (currencies() as $key => $val) {
			$displayValue = $val[0];
			$selected = ($default == $key) ? " selected='selected'" : '';
			$menu .= "<option value='{$key}'{$selected}>".$displayValue."</option>\n";
		}

		$menu .= "</select>";

		return $menu;
	}
}

if ( ! function_exists('getCurrencyMark2')) {
	function getCurrencyMark2()	{
		/**************************************************
		Currency Chr equivalents (for charting and PDF's)
		/**************************************************/
		// The chr's each contain 2 characters of the unicode for the currency symbol
		// For example, the euro's unicode is E282AC
		// the British pound's unicode is C2A3...get it?
		if ($_SESSION[currencyHTML] == '&euro;') {
			$currencyMarkChart = chr(0xE2).chr(0x82).chr(0xAC);	
			$currencyMarkPDF   = chr(128);
		} elseif ($_SESSION[currencyHTML] == '&pound;') {
			$currencyMarkChart = chr(0xC2).chr(0xA3);
			$currencyMarkPDF   = chr(0xA3);
		} elseif ($_SESSION[currencyHTML] == 'K&#269;') {
			$currencyMarkChart = chr(0x10D);
			$currencyMarkPDF   = chr(0x10D); // <- This doesn't work
		} elseif ($_SESSION[currencyHTML] == 'z&#322;') {
			$currencyMarkChart = chr(0x142);
			$currencyMarkPDF   = chr(0x142); // <- This doesn't work
		} elseif ($_SESSION[currencyHTML] == '&yen;') {
			$currencyMarkChart = chr(0xC2).chr(0xA5);
			$currencyMarkPDF   = chr(0xA5);
		} else {
			$currencyMarkPDF   = $_SESSION[currencyHTML];
			$currencyMarkChart = $_SESSION[currencyHTML];
		}				
	}
}
?>