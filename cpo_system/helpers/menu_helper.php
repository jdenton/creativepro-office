<?php
function createAppMenu() {
	/*
	|---------------------------------------------------------------
	| APPLICATION SUB-MENUS
	|---------------------------------------------------------------
	|
	*/
	//$subMenuArray['clients']    = array('View all clients','New client');
	$subMenuArray['clients']    = array($this->lang->line('menu_view_clients'),$this->lang->line('menu_new_clients'));
	$subMenuArray['projects']   = array('View all projects','New project');
	$subMenuArray['timesheets'] = array('View timesheets','View timesheet reports');	
	$subMenuArray['finances']   = array('View invoices','New invoice','View expenses','New expense','Finance reports');	
	$subMenuArray['team']       = array();
	$subMenuArray['calendar']   = array();
	$subMenuArray['adminUsers'] = array('Find Users','User Metrics');
	
	$subMenuLinksArray['clients']    = array('ViewClients','UpdateClients');
	$subMenuLinksArray['projects']   = array('Create New Invoice','View All Invoices');	
	$subMenuLinksArray['timesheets'] = array();
	$subMenuLinksArray['finances']   = array('Create New Invoice','View All Invoices');	
	$subMenuLinksArray['team']       = array();
	$subMenuLinksArray['calendar']   = array();
	$subMenuLinksArray['adminUsers'] = array('AdminUsers','AdminUserMetrics');
}
?>