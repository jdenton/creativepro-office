<?php
function getHumanTransactionErrorMessage($errorCode) {
    /*
     * Find PayPal error codes and details at
     * https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_errorcodes
     */

    switch ($errorCode) {
        case '10502':
            $humanErrorMessage = 'Oops! It appears that your credit card is expired. Please use another card.';
            break;
        case '10504':
            $humanErrorMessage = 'Oops! It appears that your security (CVV) code is invalid.';
            break;
        case '10748':
            $humanErrorMessage = 'Oops! It appears that your security (CVV) code is invalid.';
            break;
        case '10762':
            $humanErrorMessage = 'Oops! It appears that your security (CVV) code is invalid.';
            break;
        case '10508':
            $humanErrorMessage = 'Oops! It appears that your credit card expiration date is invalid.';
            break;
        case '10510':
            $humanErrorMessage = 'Oops! It appears that your credit card is not supported.';
            break;
        case '10512':
            $humanErrorMessage = 'Oops! It appears that there is a problem with your First Name.';
            break;
        case '10519':
            $humanErrorMessage = 'Oops! You have to enter a credit card number.';
            break;
        case '10521':
            $humanErrorMessage = 'Oops! The credit card entered is invalid.';
            break;
        case '10527':
            $humanErrorMessage = 'Oops! The credit card entered is invalid.';
            break;
        case '10535':
            $humanErrorMessage = 'Oops! The credit card entered is invalid.';
            break;
        case '10561':
            $humanErrorMessage = 'Oops! Your billing address is incomplete.';
            break;
        case '10562':
            $humanErrorMessage = 'Oops! There is a problem with your expiration date.';
            break;
        case '10563':
            $humanErrorMessage = 'Oops! There is a problem with your expiration date.';
            break;
        case '10752':
            $humanErrorMessage = 'Oops! Your bank declined this transaction. Please try another credit card.';
            break;
        case '10755':
            $humanErrorMessage = 'Oops! The currency you selected is not supported.';
            break;
        default:
            $humanErrorMessage = 'Oops! There was a problem with your transaction.';
    }

    return $humanErrorMessage;
}

function determineWhyPay($whyPay=null) {
    $CI =& get_instance();
    $whyPayArray = array();

    if ($whyPay === null) {
        $whyPay = $CI->session->userdata('whyPay');
    }

    switch ($whyPay) {
        case 'newAccount':
            $whyPayArray['buttonClass'] = 'buttonConfirmPayment';
            $whyPayArray['emailSubject'] = 'CreativePro Office: Payment Confirmation';
            break;
        case 'changeAccount':
            $whyPayArray['buttonClass'] = 'buttonConfirmAccountChange';
            $whyPayArray['emailSubject'] = 'CreativePro Office: Account Change Confirmation';
            break;
        case 'paymentError':
            $whyPayArray['buttonClass'] = 'buttonConfirmPayment';
            $whyPayArray['emailSubject'] = 'CreativePro Office: Payment Confirmation';
            break;
        case 'selfHosted':
            $whyPayArray['buttonClass'] = 'buttonConfirmPayment';
            $whyPayArray['emailSubject'] = 'CreativePro Office: Payment Confirmation';
            break;
        default:
            $whyPayArray['buttonClass'] = 'buttonConfirmPayment';
            $whyPayArray['emailSubject'] = 'CreativePro Office: Payment Confirmation';
    }

    return $whyPayArray;
}

function PPHttpPost($methodName_, $nvpStr_) {
    /*
     * Set up your API credentials, PayPal end point, and API version.
     * PAYPAL_USER_NAME, PAYPAL_PASSWORD, PAYPAL_SIGNATURE you get from
     * PayPal.
     */
    $API_UserName  = urlencode(PAYPAL_USER_NAME);
    $API_Password  = urlencode(PAYPAL_PASSWORD);
    $API_Signature = urlencode(PAYPAL_SIGNATURE);
    $API_Endpoint  = PAYPAL_ENDPOINT_LIVE;
    if (PAYPAL_ENVIRONMENT === "sandbox" || PAYPAL_ENVIRONMENT === "beta-sandbox") {
        $API_Endpoint = PAYPAL_ENDPOINT_SANDBOX;
    }
    $version = urlencode(PAYPAL_API_VERSION);

    /*
     * Set the curl parameters.
     */
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);

    /*
     * Turn off the server and peer verification (TrustManager Concept).
     */
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);

    /*
     * Set the API operation, version, and API signature in the request.
     */
    $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

    /*
     * Set the request as a POST FIELD for curl.
     */
    curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

    /*
     * Get response from the server.
     */
    $httpResponse = curl_exec($ch);

    if(!$httpResponse) {
        return "$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')';
    }

    /*
     * Extract the response details.
     */
    $httpResponseAr = explode("&", $httpResponse);

    $httpParsedResponseAr = array();
    foreach ($httpResponseAr as $i => $value) {
            $tmpAr = explode("=", $value);
            if(sizeof($tmpAr) > 1) {
                    $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
            }
    }

    if((sizeof($httpParsedResponseAr) == 0) || !array_key_exists('ACK', $httpParsedResponseAr)) {
        return;
    }

    return $httpParsedResponseAr;
}
?>
