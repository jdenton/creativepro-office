<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Date Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/date_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Get "now" time
 *
 * Returns time() or its GMT equivalent based on the config file preference
 *
 * @access	public
 * @return	integer
 */	
if ( ! function_exists('now'))
{
	function now()
	{
		$CI =& get_instance();
	
		if (strtolower($CI->config->item('time_reference')) == 'gmt')
		{
			$now = time();
			$system_time = mktime(gmdate("H", $now), gmdate("i", $now), gmdate("s", $now), gmdate("m", $now), gmdate("d", $now), gmdate("Y", $now));
	
			if (strlen($system_time) < 10)
			{
				$system_time = time();
				log_message('error', 'The Date class could not set a proper GMT timestamp so the local time() value was used.');
			}
	
			return $system_time;
		}
		else
		{
			return time();
		}
	}
}
	
// ------------------------------------------------------------------------

/**
 * Convert MySQL Style Datecodes
 *
 * This function is identical to PHPs date() function,
 * except that it allows date codes to be formatted using
 * the MySQL style, where each code letter is preceded
 * with a percent sign:  %Y %m %d etc...
 *
 * The benefit of doing dates this way is that you don't
 * have to worry about escaping your text letters that
 * match the date codes.
 *
 * @access	public
 * @param	string
 * @param	integer
 * @return	integer
 */	
if ( ! function_exists('mdate'))
{
	function mdate($datestr = '', $time = '')
	{
		if ($datestr == '')
			return '';
	
		if ($time == '')
			$time = now();
		
		$datestr = str_replace('%\\', '', preg_replace("/([a-z]+?){1}/i", "\\\\\\1", $datestr));
		return date($datestr, $time);
	}
}
	
// ------------------------------------------------------------------------

/**
 * Standard Date
 *
 * Returns a date formatted according to the submitted standard.
 *
 * @access	public
 * @param	string	the chosen format
 * @param	integer	Unix timestamp
 * @return	string
 */	
if ( ! function_exists('standard_date'))
{
	function standard_date($fmt = 'DATE_RFC822', $time = '')
	{
		$formats = array(
						'DATE_ATOM'		=>	'%Y-%m-%dT%H:%i:%s%Q',
						'DATE_COOKIE'	=>	'%l, %d-%M-%y %H:%i:%s UTC',
						'DATE_ISO8601'	=>	'%Y-%m-%dT%H:%i:%s%O',
						'DATE_RFC822'	=>	'%D, %d %M %y %H:%i:%s %O',
						'DATE_RFC850'	=>	'%l, %d-%M-%y %H:%m:%i UTC',
						'DATE_RFC1036'	=>	'%D, %d %M %y %H:%i:%s %O',
						'DATE_RFC1123'	=>	'%D, %d %M %Y %H:%i:%s %O',
						'DATE_RSS'		=>	'%D, %d %M %Y %H:%i:%s %O',
						'DATE_W3C'		=>	'%Y-%m-%dT%H:%i:%s%Q',
                        'DATE_ICAL'     =>  '%Y%m%dT%H%i%sZ',
                        'DATE_PAYPAL'   =>  '%Y-%m-%dT%H:%i:%sZL',
                        'DATE_ISO8601FC'=>	'%Y-%m-%dT%H:%i:%s'.'Z',
						);

		if ( ! isset($formats[$fmt]))
		{
			return FALSE;
		}
        return mdate($formats[$fmt], $time);
	}
}
	
// ------------------------------------------------------------------------

/**
 * Timespan
 *
 * Returns a span of seconds in this format:
 *	10 days 14 hours 36 minutes 47 seconds
 *
 * @access	public
 * @param	integer	a number of seconds
 * @param	integer	Unix timestamp
 * @return	integer
 */	
if ( ! function_exists('timespan'))
{
	function timespan($seconds = 1, $time = '')
	{
		$CI =& get_instance();
		$CI->lang->load('date');

		if ( ! is_numeric($seconds))
		{
			$seconds = 1;
		}
	
		if ( ! is_numeric($time))
		{
			$time = time();
		}
	
		if ($time <= $seconds)
		{
			$seconds = 1;
		}
		else
		{
			$seconds = $time - $seconds;
		}
		
		$str = '';
		$years = floor($seconds / 31536000);
	
		if ($years > 0)
		{	
			$str .= $years.' '.$CI->lang->line((($years	> 1) ? 'date_years' : 'date_year')).', ';
		}	
	
		$seconds -= $years * 31536000;
		$months = floor($seconds / 2628000);
	
		if ($years > 0 OR $months > 0)
		{
			if ($months > 0)
			{	
				$str .= $months.' '.$CI->lang->line((($months	> 1) ? 'date_months' : 'date_month')).', ';
			}	
	
			$seconds -= $months * 2628000;
		}

		$weeks = floor($seconds / 604800);
	
		if ($years > 0 OR $months > 0 OR $weeks > 0)
		{
			if ($weeks > 0)
			{	
				$str .= $weeks.' '.$CI->lang->line((($weeks	> 1) ? 'date_weeks' : 'date_week')).', ';
			}
		
			$seconds -= $weeks * 604800;
		}			

		$days = floor($seconds / 86400);
	
		if ($months > 0 OR $weeks > 0 OR $days > 0)
		{
			if ($days > 0)
			{	
				$str .= $days.' '.$CI->lang->line((($days	> 1) ? 'date_days' : 'date_day')).', ';
			}
	
			$seconds -= $days * 86400;
		}
	
		$hours = floor($seconds / 3600);
	
		if ($days > 0 OR $hours > 0)
		{
			if ($hours > 0)
			{
				$str .= $hours.' '.$CI->lang->line((($hours	> 1) ? 'date_hours' : 'date_hour')).', ';
			}
		
			$seconds -= $hours * 3600;
		}
	
		$minutes = floor($seconds / 60);
	
		if ($days > 0 OR $hours > 0 OR $minutes > 0)
		{
			if ($minutes > 0)
			{	
				$str .= $minutes.' '.$CI->lang->line((($minutes	> 1) ? 'date_minutes' : 'date_minute')).', ';
			}
		
			$seconds -= $minutes * 60;
		}
	
		if ($str == '')
		{
			$str .= $seconds.' '.$CI->lang->line((($seconds	> 1) ? 'date_seconds' : 'date_second')).', ';
		}
			
		return substr(trim($str), 0, -1);
	}
}
	
// ------------------------------------------------------------------------

/**
 * Number of days in a month
 *
 * Takes a month/year as input and returns the number of days
 * for the given month/year. Takes leap years into consideration.
 *
 * @access	public
 * @param	integer a numeric month
 * @param	integer	a numeric year
 * @return	integer
 */	
if ( ! function_exists('days_in_month'))
{
	function days_in_month($month = 0, $year = '')
	{
		if ($month < 1 OR $month > 12)
		{
			return 0;
		}
	
		if ( ! is_numeric($year) OR strlen($year) != 4)
		{
			$year = date('Y');
		}
	
		if ($month == 2)
		{
			if ($year % 400 == 0 OR ($year % 4 == 0 AND $year % 100 != 0))
			{
				return 29;
			}
		}

		$days_in_month	= array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		return $days_in_month[$month - 1];
	}
}
	
// ------------------------------------------------------------------------

/**
 * Converts a local Unix timestamp to GMT
 *
 * @access	public
 * @param	integer Unix timestamp
 * @return	integer
 */	
if ( ! function_exists('local_to_gmt'))
{
	function local_to_gmt($time = '')
	{
		if ($time == '')
			$time = time();
	
		return mktime( gmdate("H", $time), gmdate("i", $time), gmdate("s", $time), gmdate("m", $time), gmdate("d", $time), gmdate("Y", $time));
	}
}

// ------------------------------------------------------------------------

/**
 * Converts a local Unix timestamp, timezone and DST value to actual GMT NOT GMT of the server as
 * local_to_GMT does
 *
 * If converting FROM local TO GMT WITH daylight savings time,
 * we need to Subtract 1 hour. (ex. PacNW is GMT-8 but with DST it's
 * GMT-7.
 *
 * @access	public
 * @param	integer Unix timestamp
 * @return	integer
 */
if ( ! function_exists('convert_to_gmt')) {
    function convert_to_gmt($time = '', $timezone = 'UTC', $dst = FALSE)
    {
        if ($time == '')
        {
            return now();
        }

        $tz = phpTimezones($timezone);
        $date = date('Y-m-d H:i:s',$time);
        $dateTimeZone = new DateTimeZone($tz);
        $dateTime = new DateTime($date, $dateTimeZone);
        $offset = $dateTime->getOffset();
        $time = $time-$offset;
        /*
        $time -= timezones($timezone) * 3600;

        if ($dst == TRUE)
        {
            $time -= 3600;
        }
        */
        return $time;
    }
}
	
// ------------------------------------------------------------------------

/**
 * Converts GMT time to a localized value
 *
 * Takes a Unix timestamp (in GMT) as input, and returns
 * the local value based on the timezone and DST setting
 * submitted
 *
 * If converting FROM GMT to local WITH daylight savings time,
 * we need to ADD 1 hour. (ex. PacNW is GMT-8 but with DST it's
 * GMT-7.)
 *
 * @access	public
 * @param	integer Unix timestamp
 * @param	string	timezone
 * @param	bool	whether DST is active
 * @return	integer
 */	
if ( ! function_exists('gmt_to_local'))
{
	function gmt_to_local($time = '', $timezone = 'UTC', $dst = FALSE)
	{			
		if ($time == '')
		{
			return now();
		}
        $tz = phpTimezones($timezone);
        $date = date('Y-m-d H:i:s',$time);
        $dateTimeZone = new DateTimeZone($tz);
        $dateTime = new DateTime($date, $dateTimeZone);
        $offset = $dateTime->getOffset();
        $time = $time+$offset;
		/*
        $time += timezones($timezone) * 3600;

		if ($dst == TRUE)
		{
			$time += 3600;
		}
        */
        
		return $time;
	}
}
	
// ------------------------------------------------------------------------

/**
 * Converts a MySQL Timestamp to Unix
 *
 * @access	public
 * @param	integer Unix timestamp
 * @return	integer
 */	
if ( ! function_exists('mysql_to_unix'))
{
	function mysql_to_unix($time = '')
	{
		// We'll remove certain characters for backward compatibility
		// since the formatting changed with MySQL 4.1
		// YYYY-MM-DD HH:MM:SS
	
		$time = str_replace('-', '', $time);
		$time = str_replace(':', '', $time);
		$time = str_replace(' ', '', $time);
	
		// YYYYMMDDHHMMSS
		return  mktime(
						substr($time, 8, 2),
						substr($time, 10, 2),
						substr($time, 12, 2),
						substr($time, 4, 2),
						substr($time, 6, 2),
						substr($time, 0, 4)
						);
	}
}
	
// ------------------------------------------------------------------------

/**
 * Unix to "Human"
 *
 * Formats Unix timestamp to the following prototype: 2006-08-21 11:35 PM
 *
 * @access	public
 * @param	integer Unix timestamp
 * @param	bool	whether to show seconds
 * @param	string	format: us or euro
 * @return	string
 */	
if ( ! function_exists('unix_to_human'))
{
	function unix_to_human($time = '', $seconds = FALSE, $fmt = 'us')
	{
		$r  = date('Y', $time).'-'.date('m', $time).'-'.date('d', $time).' ';
		
		if ($fmt == 'us')
		{
			$r .= date('h', $time).':'.date('i', $time);
		}
		else
		{
			$r .= date('H', $time).':'.date('i', $time);
		}
	
		if ($seconds)
		{
			$r .= ':'.date('s', $time);
		}
	
		if ($fmt == 'us')
		{
			$r .= ' '.date('A', $time);
		}
		
		return $r;
	}
}
	
// ------------------------------------------------------------------------

/**
 * Convert "human" date to GMT
 *
 * Reverses the above process
 *
 * @access	public
 * @param	string	format: us or euro
 * @return	integer
 */	
if ( ! function_exists('human_to_unix'))
{
	function human_to_unix($datestr = '')
	{
		if ($datestr == '')
		{
			return FALSE;
		}
	
		$datestr = trim($datestr);
		$datestr = preg_replace("/\040+/", "\040", $datestr);

		if ( ! preg_match('/^[0-9]{2,4}\-[0-9]{1,2}\-[0-9]{1,2}\s[0-9]{1,2}:[0-9]{1,2}(?::[0-9]{1,2})?(?:\s[AP]M)?$/i', $datestr))
		{
			return FALSE;
		}

		$split = preg_split("/\040/", $datestr);

		$ex = explode("-", $split['0']);
	
		$year  = (strlen($ex['0']) == 2) ? '20'.$ex['0'] : $ex['0'];
		$month = (strlen($ex['1']) == 1) ? '0'.$ex['1']  : $ex['1'];
		$day   = (strlen($ex['2']) == 1) ? '0'.$ex['2']  : $ex['2'];

		$ex = explode(":", $split['1']);
	
		$hour = (strlen($ex['0']) == 1) ? '0'.$ex['0'] : $ex['0'];
		$min  = (strlen($ex['1']) == 1) ? '0'.$ex['1'] : $ex['1'];

		if (isset($ex['2']) AND ereg("[0-9]{1,2}", $ex['2']))
		{
			$sec  = (strlen($ex['2']) == 1) ? '0'.$ex['2'] : $ex['2'];
		}
		else
		{
			// Unless specified, seconds get set to zero.
			$sec = '00';
		}
	
		if (isset($split['2']))
		{
			$ampm = strtolower($split['2']);
		
			if (substr($ampm, 0, 1) == 'p' AND $hour < 12)
				$hour = $hour + 12;
			
			if (substr($ampm, 0, 1) == 'a' AND $hour == 12)
				$hour =  '00';
			
			if (strlen($hour) == 1)
				$hour = '0'.$hour;
		}
			
		return mktime($hour, $min, $sec, $month, $day, $year);
	}
}
	
// ------------------------------------------------------------------------

/**
 * Timezone Menu
 *
 * Generates a drop-down menu of timezones.
 *
 * @access	public
 * @param	string	timezone
 * @param	string	classname
 * @param	string	menu name
 * @return	string
 */	
if ( ! function_exists('timezone_menu'))
{
	function timezone_menu($default = 'UTC', $class = "", $name = 'timezones', $width="", $tabindex=NULL)
	{
		$CI =& get_instance();
		$CI->lang->load('date');
	
		if ($default == 'GMT')
			$default = 'UTC';

		$menu = '<select name="'.$name.'" id="'.$name.'"';
	
		if ($class != '')
		{
			$menu .= ' class="'.$class.'"';
		}
		if ($width != '')
		{
			$menu .= ' style="width: '.$width.'px;"';
		}
		if ($tabindex>0) {
			$menu .= ' tabindex="'.$tabindex.'"';
		}
	
		$menu .= ">\n";
        $menu .= "<option></option>";
	
		foreach (timezones() as $key => $val)
		{
			$selected = ($default == $key) ? " selected='selected'" : '';
			$menu .= "<option value='{$key}'{$selected}>".$CI->lang->line($key)."</option>\n";
		}

		$menu .= "</select>";

		return $menu;
	}
}
	
// ------------------------------------------------------------------------

/**
 * Timezones
 *
 * Returns an array of timezones.  This is a helper function
 * for various other ones in this library
 *
 * @access	public
 * @param	string	timezone
 * @return	string
 */	
if ( ! function_exists('timezones'))
{
	function timezones($tz = '')
	{
		// Note: Don't change the order of these even though
		// some items appear to be in the wrong order
		
		$zones = array( 
						'UM12'		=> -12,
						'UM11'		=> -11,
						'UM10'		=> -10,
						'UM95'		=> -9.5,
						'UM9'		=> -9,
						'UM8'		=> -8,
						'UM7'		=> -7,
                        'UM7AZ'		=> -7,
						'UM6'		=> -6,
						'UM5'		=> -5,
						'UM45'		=> -4.5,
						'UM4'		=> -4,
						'UM35'		=> -3.5,
						'UM3'		=> -3,
						'UM2'		=> -2,
						'UM1'		=> -1,
						'UTC'		=> 0,
						'UP1'		=> +1,
						'UP2'		=> +2,
						'UP3'		=> +3,
						'UP35'		=> +3.5,
						'UP4'		=> +4,
						'UP45'		=> +4.5,
						'UP5'		=> +5,
						'UP55'		=> +5.5,
						'UP575'		=> +5.75,
						'UP6'		=> +6,
						'UP65'		=> +6.5,
						'UP7'		=> +7,
						'UP8'		=> +8,
						'UP875'		=> +8.75,
						'UP9'		=> +9,
						'UP95'		=> +9.5,
						'UP10'		=> +10,
						'UP105'		=> +10.5,
						'UP11'		=> +11,
						'UP115'		=> +11.5,
						'UP12'		=> +12,
						'UP1275'	=> +12.75,
						'UP13'		=> +13,
						'UP14'		=> +14
					);
				
		if ($tz == '')
		{
			return $zones;
		}
	
		if ($tz == 'GMT')
			$tz = 'UTC';
	
		return ( ! isset($zones[$tz])) ? 0 : $zones[$tz];
	}
}

if ( !function_exists('phpTimezones')) {
	function phpTimezones($tz = '') {
        $zones = array(
						'UM12'		=> 'Pacific/Kwajalein',
						'UM11'		=> 'Pacific/Samoa',
						'UM10'		=> 'Pacific/Honolulu',
						'UM95'		=> 'Australia/Darwin',
						'UM9'		=> 'America/Juneau',
						'UM8'		=> 'America/Los_Angeles',
						'UM7'		=> 'America/Denver',
                        'UM7AZ'		=> 'America/Phoenix',
						'UM6'		=> 'America/Mexico_City',
						'UM5'		=> 'America/New_York',
						'UM45'		=> 'America/Caracas',
						'UM4'		=> 'America/Caracas',
						'UM35'		=> 'America/St_Johns',
						'UM3'		=> 'America/Argentina/Buenos_Aires',
						'UM2'		=> 'Atlantic/Azores',
						'UM1'		=> 'Atlantic/Azores',
						'UTC'		=> 'Europe/London',
						'UP1'		=> 'Europe/Paris',
						'UP2'		=> 'Europe/Helsinki',
						'UP3'		=> 'Europe/Moscow',
						'UP35'		=> 'Asia/Tehran',
						'UP4'		=> 'Asia/Baku',
						'UP45'		=> 'Asia/Kabul',
						'UP5'		=> 'Asia/Karachi',
						'UP55'		=> 'Asia/Calcutta',
						'UP575'		=> '+5.75',
						'UP6'		=> 'Asia/Colombo',
						'UP65'		=> '+6.5',
						'UP7'		=> 'Asia/Bangkok',
						'UP8'		=> 'Asia/Singapore',
						'UP875'		=> '+8.75',
						'UP9'		=> 'Asia/Tokyo',
						'UP95'		=> 'Australia/Darwin',
						'UP10'		=> 'Pacific/Guam',
						'UP105'		=> '+10.5',
						'UP11'		=> 'Asia/Magadan',
						'UP115'		=> '+11.5',
						'UP12'		=> 'Asia/Kamchatka',
						'UP1275'	=> '+12.75',
						'UP13'		=> '+13',
						'UP14'		=> '+14'
					);

		if ($tz == '')
		{
			return $zones;
		}

		if ($tz == 'GMT') {
			$tz = 'UTC';
        }
		return $zones[$tz];
    }
}

/**
 * convertTimezoneFormat
 *
 * Takes a timezone in the form of -06:00 or 8:00 and coverts
 * it into the form of UM6 or UP8 repsectively.
 *
 * @access  public
 * @param   string timezone
 * @return  string convertedTimezone
 */
if ( ! function_exists('convertTimezoneFormat')) {
    function convertTimezoneFormat($timezone) {
        $convertedTimezone = '';
        if (substr($timezone,0,1) == '-' || substr($timezone,0,1) == '+') {
            if ($timezone != '0') {
                $pre = 'UP';
                if (substr($timezone,0,1) == '-') {
                    $pre = 'UM';
                }
                if (substr($timezone,1,1) == '0') {
                    $hour = substr($timezone,2,1);
                } else {
                    $hour = substr($timezone,1,2);
                }

                $convertedTimezone = $pre.$hour;
            }
        }
        return $convertedTimezone;
    }
}

if (!function_exists('convertIcalToHuman')) {
    function convertIcalToHuman($iCalDate,$format='Y-m-d G:i:s') {
        $dateTotalArray = explode('T',$iCalDate);
        $dateArray = explode('-',$dateTotalArray[0]);
        $timeArray = explode(':',$dateTotalArray[1]);

        $year  = $dateArray[0];
        $month = $dateArray[1];
        $day   = $dateArray[2];
        $hour  = $timeArray[0];
        $min   = $timeArray[1];
        $timeStamp = strtotime($iCalDate);

        return date($format,$timeStamp);
    }
}

/**
 * Days Between Dates
 *
 * Returns the number of days between 2 dates.
 *
 * @access	public
 * @param	integer	$timestampStart Unix timestamp
 * @param	integer	$timestampEnd Unix timestamp
 * @return	string  $days The number of days
 */	
if ( ! function_exists('days_between_dates'))
{
	function days_between_dates($timestampStart,$timestampEnd) {
		$offset = $timestampEnd-$timestampStart; //Depending on which offset type you want, switch order
	
		$days = floor($offset/3600/24);
		return $days;
	}	
}

/**
 * new_date_from_days
 *
 * Returns a date based upon a start date and number of days.
 *
 * @access	public
 * @param	string	$dateStart The start date
 * @param	integer	$noOfDays  The number of days from start date
 * @return	string  $newDate   The new date
 */
if ( ! function_exists('new_date_from_days'))
{
	function new_date_from_days($dateStart,$noOfDays,$format=NULL) {
		$startTimestamp = strtotime($dateStart);
		if ($noOfDays<0) {
			$endTimestamp = $startTimestamp-(abs($noOfDays)*60*60*24);
		} else {
			$endTimestamp = $startTimestamp+($noOfDays*60*60*24);
		}
        if (empty($format)) {
            $format = 'Y-m-d';
        }
		$newDate = date($format,$endTimestamp);
		return $newDate;
	}
}

if ( ! function_exists('jsDateToMySQL')) {
	function jsDateToMySQL($date=null) {
		/*
		 * jsDateToMySQL converts a date from the jQuery
		 * date-picker control to MySQL format
		 * MM/DD/YY --> YYYY-MM-DD
		 */
		if (empty($date)) {
			$newDate = '0000-00-00';
		} elseif (substr($date,2,1) == '/') {
			$dateArray = explode('/',$date);
			$m = $dateArray[0];
			$d = $dateArray[1];
			$y = $dateArray[2];

			$newDate = $y.'-'.$m.'-'.$d;
		} else {
			$newDate = $date;
		}
		return $newDate;
	}
}

if ( ! function_exists('MySQLDateToJS')) {
	function MySQLDateToJS($date=null) {
		/*
		 * MySQLDateToJS converts a date from the MySQL
		 * format of YYYY-MM-DD to jQuery date picker
           * control format.
		 * YYYY-MM-DD --> MM/DD/YY
		 */

		if (empty($date) || $date == '0000-00-00') {
			$newDate = '';
		} else {
			$dateArray = explode('-',$date);
			$y = $dateArray[0];
			$m = $dateArray[1];
			$d = $dateArray[2];
			$newDate = $m.'/'.$d.'/'.$y;
		}
		return $newDate;
	}
}

if ( ! function_exists('getDateString')) {
	function getDateString($dateStart=NULL,$dateEnd=NULL,$itemType=NULL) {
		if (!empty($dateStart) && empty($dateEnd)) {
		   $dateString = sprintf(lang('status_start_on'),$dateStart);
	   } elseif (!empty($dateStart) && !empty($dateEnd)) {
           if ($dateStart != $dateEnd) {
               $dateString = $dateStart.' to '.$dateEnd;
           } else {
               $dateString = $dateStart;
           }
	   } elseif (empty($dateStart) && !empty($dateEnd)) {
		   $dateString = sprintf(lang('status_due_on'),$dateEnd);
	   } else {
		   $dateString = '';
	   }
	   return $dateString;
	}
}

/*
 * weekFromDate creates an array of dates for one
 *              week based on a date that's passed in.
 * @access	public
 * @param	string	$dateIn    The date around which we want to create a week
 * @param   string  $startDay  M = Monday, S = Sunday
 * @return	array   $weekArray PHP array with dates and day text
 */
if (!function_exists('weekFromDate')) {
	function weekFromDate($dateIn,$initDate=FALSE,$startDay='M') {
		if ($initDate == TRUE) {
			$dateIn = mysqldate_to_localdate($dateIn);
		}
		$datePart = explode(' ',$dateIn);
		$dateArray = explode('-',$datePart[0]);
		$year  = $dateArray[0];
		$month = $dateArray[1];
		$day   = $dateArray[2];
		$dayNumberInWeek = date("w",mktime(0,0,0,$month,$day,$year));

        if ($startDay == 'M') {
            $lowEnd = -$dayNumberInWeek+1;
            /*
             * So Sunday (day 0) doesn't shift us to the next week before we're ready.
             */
            if ($dayNumberInWeek == 0) {
                $dayNumberInWeek = 7;
            }
        } elseif ($startDay == 'S') {
            $lowEnd = -$dayNumberInWeek;
        }
        
		$highEnd=$lowEnd + 6;
		$weekday=0;
		$weekArray['prevWeekStart']   = date('Y-m-d G:i:s',mktime(0,0,0,$month,$day-6,$year));
		$weekArray['nextWeekStart']   = date('Y-m-d G:i:s',mktime(0,0,0,$month,$day+7,$year));
        
		for ($i=$lowEnd; $i<=$highEnd; $i++) {
            $dayTextShort = strtolower(date('D',mktime(0,0,0,$month,$day+$i,$year)));
            $dayTextShort = substr($dayTextShort,0,-1);
			$weekArray['weekDays'][$weekday]['dayDisplay'] = lang('cal_'.$dayTextShort).' '.date('n/d',mktime(0,0,0,$month,$day+$i,$year));
			$weekArray['weekDays'][$weekday]['date']       = date('Y-m-d',mktime(0,0,0,$month,$day+$i,$year));
			$weekday++;
		}
		return $weekArray;
	}
}

/*
 * mysqldate_to_localdate ... I think this has been replaced by convertMySQLToGMT
 */
if (!function_exists('mysqldate_to_localdate')) {
	function mysqldate_to_localdate($date=NULL,$timezone='UTC',$dst=TRUE) {
		$CI =& get_instance();

		if ($CI->session->userdata('daylightSavingsTime')) {
            $dst = $CI->session->userdata('daylightSavingsTime');
        }
        if ($CI->session->userdata('gmtOffset')) {
            $timezone = $CI->session->userdata('gmtOffset');
        }

		if (empty($date)) {
			$date = date('Y-m-d G:i:s');
			$unixDate = time();
		} else {
			$dateArray = explode(' ',$date);
			if (is_array($dateArray)) {
				$timePart = explode(':',$dateArray[1]);
				$datePart = explode('-',$dateArray[0]);
				$unixDate = mktime($timePart[0],$timePart[1],$timePart[2],$datePart[1],$datePart[2],$datePart[0]);
			}
		}
		$localDate = date('Y-m-d G:i:s', gmt_to_local($unixDate, $timezone, $dst));
		return $localDate;
	}
}

if (!function_exists('translateDateString')) {
	function translateDateString($date,$format='monthtext_short_daynum') {
		$dateArray = explode('-',$date);
		$year  = $dateArray[0];
		$month = $dateArray[1];
		$day   = $dateArray[2];
		$timeStamp = mktime(0,0,0,$month,$day,$year);
		switch ($format) {
			case 'month_short_day':
				break;
			case 'month_day_year':
				break;
			case 'daytext_monthtext_daynum':
				$daytext = lang('cal_'.strtolower(date('l',$timeStamp)));
				$monthtext = lang('cal_'.strtolower(date('F',$timeStamp)));
				$transDate = $daytext.' '.$monthtext.' '.ltrim($day,'0');
				break;
		}

		return $transDate;
	}
}

if (!function_exists('convertTimeToMySQL')) {
	function convertTimeToMySQL($time) {
        $time = trim($time);
        $timeArray = explode(':',$time);
        $hour  = trim($timeArray[0]);
        $min   = substr(trim($timeArray[1]),0,2);
		$ampm  = strtolower(substr($time,-2));
		$ampm2 = strtolower(substr($time,-1));

		/*
		 * If no am/pm entered, make a best guess
		 */
		if (empty($ampm) && $hour<8) {
			$ampm = 'pm';
		} elseif (empty($ampm) && $hour>12) {
			$ampm = 'pm';
		} elseif (empty($ampm) && $hour>8) {
			$ampm = 'am';
		}

		if ($ampm == 'pm' || $ampm2 == 'p') {
			/*
             * Add 12 hours to the entered time
             */
            if (ltrim($hour,0)<12) {
                $hour = ltrim($hour,0)+12;
            }
            $time = $hour.':'.$min.':00';
        } else {
            $time = $hour.':'.$min.':00';
        }
        return $time;
    }
}

/*
 * convertMySQLToGMT converts a date in MySQL format
 * YYYY-MM-DD HH:mm:ss into GMT date
 * @access	public
 * @param	string	$datetime    The date we wish to convert in MySQL format
 * @param	array   $direction   To the database or from the database (push|pull)
 * @param   string  $format      Format in which to return the converted date (see PHP data format options 
 *                               http://www.php.net/manual/en/function.date.php)
 * @return  string  Converted date in the format specified
 */
if (!function_exists('convertMySQLToGMT')) {
	function convertMySQLToGMT($datetime,$direction='pull',$format='Y-m-d G:i:s',$timezone='UTC',$dst=FALSE) {
        if ($datetime == '0000-00-00 00:00:00' || $datetime == '0000-00-00') {
            return $datetime;
        }
		$CI =& get_instance();
        if ($CI->session->userdata('daylightSavingsTime')) {
            $dst = $CI->session->userdata('daylightSavingsTime');
        }
        if ($CI->session->userdata('gmtOffset')) {
            $timezone = $CI->session->userdata('gmtOffset');
        }

        /*
         * Do we have a time component when pushing to database? If not, add 11:59:59 in order to
         * adjust dates properly.
         * ### ADDED 11-5-09
         *     Not so sure about adding 11:59:59. What's the purpose. Commented lines 1006,1007 below.
         */
        $dateTimeArray = explode(' ',$datetime);
        $secondComponent = '';
		if (sizeof($dateTimeArray) == 2) {
			$secondComponent = substr($dateTimeArray[1],-2);
		}

        /*
         * We're going from the UI to the database
         */
		if ($direction == 'push') {
            if (sizeof($dateTimeArray)<2) {
				$datetime = $dateTimeArray[0].' 11:59:59';
				$secondComponent = '59';
			} elseif ($dateTimeArray[1] == '00:00:00') {
				//$datetime = $dateTimeArray[0].' 11:59:59';
				//$secondComponent = '59';
			}
            $timestamp = strtotime($datetime);
            $newDate = date($format, convert_to_gmt($timestamp,$timezone,$dst));
            return $newDate;
		}		

        if ($direction == 'pull') {
            /*
             * We're going from the database/server to the UI
			 * TODO: All this jerking around trying to get dates right is a complete
			 * hack. Rewrite this soon!!!!
             */
            if (sizeof($dateTimeArray)<2) {
				$datetime = $dateTimeArray[0].' 11:59:59';
			}
            $timestamp = strtotime($datetime);
			$newDate = date($format, gmt_to_local($timestamp,$timezone,$dst));
            
			if ($secondComponent == '59' && substr($format,-5) == 'H:i:s') {
				$newDateArray = explode(' ',$newDate);
				$newDate = $newDateArray[0].' 00:00:00';
			}
            return $newDate;
        }
    }
}

/*
 * dateTextFromDate returns a natural language identifier when given a date.
 * Ex. Tomorrow, yesterday, this week, next week, last week, next month, etc.
 *
 * @access	public
 * @param	string	$dateIn    The input date in MySQL YYYY-MM-DD format
 * @return  array   PHP array of all possible identifiers for a date
 */
if (!function_exists('dateTextFromDate')) {
	function dateTextFromDate($dateIn=NULL) {
        $datePart = explode(' ',$dateIn);
        $dateIn = $datePart[0];
        if (empty($dateIn) || $dateIn == '0000-00-00') {
            return false;
        }

        $dateIdArray = array(
                        'today'     => FALSE,
                        'tomorrow'  => FALSE,
                        'yesterday' => FALSE,

                        'thisWeek'  => FALSE,
                        'nextWeek'  => FALSE,
                        'lastWeek'  => FALSE,
                        
                        'thisMonth' => FALSE,
                        'nextMonth' => FALSE,
                        'lastMonth' => FALSE
                        );

        /*
         * Is this date today?
         */
        if ($dateIn == date('Y-m-d')) {
            $dateIdArray['today'] = TRUE;
        }

        /*
         * Is this date tomorrow?
         */
        if ($dateIn == date('Y-m-d',time()+86400) && $dateIdArray['today'] == FALSE) {
            $dateIdArray['tomorrow'] = TRUE;
        }

        /*
         * Is this date yesterday?
         */
        if ($dateIn == date('Y-m-d',time()-86400) && $dateIdArray['today'] == FALSE  && $dateIdArray['tomorrow'] == FALSE) {
            $dateIdArray['yesterday'] = TRUE;
        }

        /*
         * Is this date this week?
         */
        if ($dateIdArray['today'] == TRUE) {
            $dateIdArray['thisWeek'] = TRUE;
        } else {
            $weekArray = weekFromDate(date('Y-m-d'),NULL,'S');
            foreach($weekArray['weekDays'] as $day) {
                if ($dateIn == $day['date']) {
                    $dateIdArray['thisWeek'] = TRUE;
                }
            }
        }


        /*
         * Is this date next week?
         */
        if ($dateIdArray['thisWeek'] == FALSE) {
            $dayNextWeek = date('Y-m-d',time()+691200);
            $weekArray = weekFromDate($dayNextWeek,NULL,'S');
            foreach($weekArray['weekDays'] as $day) {
                if ($dateIn == $day['date']) {
                    $dateIdArray['nextWeek'] = TRUE;
                }
            }
        }

        /*
         * Is this date last week?
         */
        if ($dateIdArray['thisWeek'] == FALSE && $dateIdArray['lastWeek'] == FALSE) {
            $dayLastWeek = date('Y-m-d',time()-691200);
            $weekArray = weekFromDate($dayLastWeek,NULL,'S');
            foreach($weekArray['weekDays'] as $day) {
                if ($dateIn == $day['date']) {
                    $dateIdArray['lastWeek'] = TRUE;
                }
            }
        }

        /*
         * Is this date this month?
         */

        /*
         * Is this date next month?
         */

        /*
         * Is this date last month?
         */

        return $dateIdArray;
    }
}

if (!function_exists('determineAllDay')) {
	function determineAllDay($dateTimeStart,$dateTimeEnd=NULL) {
        $allDay = false;

        /*
         * If no end date, allDay = true by default.
         */
        if (empty($dateTimeEnd) || $dateTimeEnd == '0000-00-00 00:00:00' || $dateTimeEnd == '0000-00-00') {
            $allDay = true;
        } else {
            $dateStartArray = explode(' ',$dateTimeStart);
            $dateEndArray   = explode(' ',$dateTimeEnd);
            $dateStart      = $dateStartArray[0];
            $timeStart      = $dateStartArray[1];
            $dateEnd        = $dateEndArray[0];
            $timeEnd        = $dateEndArray[1];

            if ($dateStart == $dateEnd && $timeEnd == '00:00:00') {
                /*
                 * If we're on the same day but no end TIME, then allDay = true.
                 */
                $allDay = true;
            } elseif ($dateStart != $dateEnd) {
                /*
                 * If our end date is different from our start date,
                 * allDay = true always.
                 */
                $allDay = true;
            }
        }

        return $allDay;
    }
}

/* End of file date_helper.php */
/* Location: ./system/helpers/date_helper.php */