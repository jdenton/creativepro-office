<?php
$lang['finance_invoice_number_abbr']     = 'Invoice #';

$lang['finance_invoice_message']         = 'You can view and print this invoice from your <a href="%s"><strong>client page</strong></a>.';
$lang['finance_invoice_message_paypal']  = 'You can also <a href="%s"><strong>pay this invoice</strong></a> using PayPal.';
?>