<?
$lang['no_records_found'] = 'Нет записей не обнаружено.';
$lang['status_not_started'] = 'Не выполнено';
$lang['status_in_progress'] = 'В ходе';
$lang['status_due_in'] = 'Из-за %s %s';          //Due in 6 days.
$lang['status_due_on'] = 'В силу %s года'; //In force on 1 January 2000.
$lang['status_start_on'] = '%s %s опоздал';      //6 Days late.
$lang['status_completed'] = 'Завершен';
$lang['status_overdue'] = 'Просроченные';
$lang['status_on_hold'] = 'О проведении';
$lang['status_not_sent'] = 'Не отправлено';
$lang['status_sent'] = 'Отправленный';
$lang['status_paid'] = 'Платные';
$lang['status_past_due'] = 'Ранее из-за';
$lang['status_past_due_text'] = '%s %s опоздал';      //6 Days late.
$lang['status_past_due_text_year'] = 'Более чем на 1 год опоздал';
$lang['status_not_read'] = 'Не читать';
$lang['status_read'] = 'Читать';
$lang['status_urgent'] = 'Срочно';
$lang['status_needs_attention'] = 'Требует внимания';
$lang['status_open_sent_on'] = 'Часы работы: направил на';
$lang['status_open_created_on'] = 'Часы работы: из';
$lang['status_closed_paid_on'] = 'Закрыто: выплачиваются';
$lang['status_closed_not_paid'] = 'Закрыто: не оплачены';
$lang['status_status'] = 'Статус';
$lang['menu_add_dashboard_widgets'] = 'Добавить Dashboard Widgets';
$lang['menu_clients'] = 'Клиенты';
$lang['menu_view_clients'] = 'Список клиентов';
$lang['menu_new_client'] = 'Новый клиент';
$lang['menu_projects'] = 'Проекты';
$lang['menu_view_projects'] = 'Список проектов';
$lang['menu_new_project'] = 'Новый проект';
$lang['menu_timesheets'] = 'Табель';
$lang['menu_view_timesheets'] = 'Посмотреть Табель';
$lang['menu_view_timesheet_reports'] = 'Список докладов табель';
$lang['menu_finances'] = 'Финансы';
$lang['menu_view_finances'] = 'Финансовый обзор';
$lang['menu_new_invoice'] = 'Новые счета-фактуры';
$lang['menu_new_expense'] = 'Новый счет';
$lang['menu_finance_reports'] = 'Финансовый Отчет';
$lang['widget_invoices'] = 'Счета-фактуры';
$lang['widget_quotes'] = 'Цитаты';
$lang['widget_calendar'] = 'Календарь';
$lang['widget_rss'] = 'RSS-каналы';
$lang['widget_notes'] = 'Примечания';
$lang['widget_bugs'] = 'Ошибки';
$lang['widget_projects'] = 'Проекты';
$lang['widget_tasks'] = 'Задание';
$lang['widget_messages'] = 'Сообщения';
$lang['widget_reports'] = 'Отчеты';
$lang['widget_delicious'] = 'Delicious.com';
$lang['widget_today'] = 'Что происходит?';
$lang['widget_timer'] = 'Работа по таймеру';
$lang['common_account'] = 'Учетная запись';
$lang['common_email'] = 'Электронная почта';
$lang['common_form_more_link'] = 'Показать больше полей';
$lang['common_field_select'] = 'Выбрать ...';
$lang['common_first_name'] = 'Имя';
$lang['common_last_name'] = 'Фамилия';
$lang['common_messages'] = 'Сообщения';
$lang['common_language'] = 'Изменение языка';
$lang['common_alerts'] = 'Предупреждения';
$lang['common_search'] = 'Поиск';
$lang['common_company_name'] = 'Название компании';
$lang['common_company_info'] = 'Информация о компании';
$lang['common_invoice_total'] = 'Счет Всего';
$lang['common_no_projects'] = 'Количество проектов';
$lang['common_loading_clients'] = 'Загрузка клиентов ...';
$lang['common_loading_map'] = 'Загрузка карты ...';
$lang['common_hours'] = 'Часов';
$lang['common_status'] = 'Статус';
$lang['common_totals'] = 'Итоги';
$lang['common_total'] = 'Всего';
$lang['common_subtotal'] = 'Итого';
$lang['common_title'] = 'Название';
$lang['common_number'] = 'Количество';
$lang['common_help'] = 'Помощь';
$lang['common_working'] = 'Рабочая ...';
$lang['common_loading'] = 'Загрузка ...';
$lang['common_company'] = 'Компания';
$lang['common_phone'] = 'Телефон';
$lang['common_phone_number'] = 'Номер телефона';
$lang['common_welcome'] = 'Добро пожаловать';
$lang['common_search_office'] = 'Поиск офиса';
$lang['common_send_message'] = 'Отправить сообщение';
$lang['common_send_message_to'] = 'Отправить сообщение';
$lang['common_send'] = 'Отправлять';
$lang['common_date'] = 'Свидание';
$lang['common_date_start'] = 'Дата начала';
$lang['common_date_end'] = 'Дата окончания';
$lang['common_date_due'] = 'Срок';
$lang['common_priority'] = 'Очередность';
$lang['common_add_another'] = 'Добавить еще';
$lang['common_download_file'] = 'Загрузить файл';
$lang['common_vcard'] = 'Скачать электронную визитку';
$lang['common_other'] = 'Другой';
$lang['common_filter_by'] = 'Фильтр по';
$lang['common_all'] = 'Все';
$lang['common_open'] = 'Открывать';
$lang['common_closed'] = 'Закрытые';
$lang['common_past_due'] = 'Ранее из-за';
$lang['common_dates'] = 'Даты';
$lang['common_work'] = 'Работа';
$lang['common_mobile'] = 'Мобильные';
$lang['common_fax'] = 'Факс';
$lang['common_pager'] = 'Пейджер';
$lang['common_home'] = 'Дом';
$lang['common_skype'] = 'Skype';
$lang['common_personal'] = 'Личное';
$lang['common_url'] = 'Веб-сайт';
$lang['common_aim'] = 'AIM';
$lang['common_msn'] = 'MSN';
$lang['common_icq'] = 'ICQ';
$lang['common_jabber'] = 'Jabber';
$lang['common_yahoo'] = 'Yahoo';
$lang['common_googletalk'] = 'Google Talk';
$lang['common_immediately'] = 'Немедленно';
$lang['common_specific_amount'] = 'Конкретная сумма';
$lang['common_specific_date'] = 'Конкретная дата';
$lang['common_per_month'] = 'За месяц';
$lang['common_billable'] = 'Платежные';
$lang['common_create_report'] = 'Создать отчет';
$lang['common_comments'] = 'Комментарии';
$lang['common_user'] = 'Пользователь';
$lang['common_yes'] = 'Да';
$lang['common_no'] = 'Нет';
$lang['message_sent_concerning'] = '%s отправил вам сообщение, касающееся счета на %s %s %s'; //Janice has sent you a message on an account at 1 January 2009.
$lang['message_sent'] = '%s отправили сообщение на %s %s %s'; //Janice posted a message on 1 January 2009.
$lang['message_concerning_invoice'] = 'счет-фактуру';
$lang['message_concerning_project'] = 'проекта';
$lang['message_concerning_file'] = 'файл';
$lang['role_loggedinas'] = 'Зарегистрированный в качестве';
$lang['role_administrator'] = 'Администратор';
$lang['role_employee'] = 'Работник';
$lang['role_manager'] = 'Менеджер';
$lang['role_contractor'] = 'Подрядчик';
$lang['button_save'] = 'Сохранить';
$lang['button_cancel'] = 'Отмена';
$lang['button_add'] = 'Новый';
$lang['button_edit'] = 'Изменить';
$lang['button_delete'] = 'Удалить';
$lang['button_options'] = 'Функции';
$lang['button_print'] = 'Версия для печати';
$lang['button_pdf'] = 'Создать PDF';
$lang['button_new_contact'] = 'Новый контакт';
$lang['button_close'] = 'Закрыть';
$lang['button_clear'] = 'Ясный';
$lang['button_remove'] = 'Удалить';
$lang['button_export_excel'] = 'Excel экспорт';
$lang['login_page_title'] = 'Управление Логин';
$lang['login_user_name'] = 'Имя пользователя';
$lang['login_password'] = 'Пароль';
$lang['login_button'] = 'Войти';
$lang['login_logout'] = 'Выход';
$lang['login_forget_password'] = 'Я забыл свой пароль.';
$lang['login_message_logout'] = 'Вы вошли в.';
$lang['login_message_inactiveAccount'] = 'Ваша учетная запись не активна.';
$lang['login_message_missingField'] = 'У вас не хватает некоторых областях.';
$lang['login_message_noAccount'] = 'Вы не имеете учетной записи.';
$lang['login_client_page_title'] = 'Логин клиента';
$lang['login_find_password'] = 'Найти свой пароль';
$lang['login_default'] = 'Добро пожаловать! Пожалуйста, войдите в ваш аккаунт CreativePro Управления ниже.';
$lang['login_message_expired'] = 'По соображениям безопасности сеанса истекло. Пожалуйста, зарегистрируйтесь снова.';
$lang['dashboard_page_title'] = 'Управление Пульт управления';
$lang['client_page_title'] = 'Клиенты';
$lang['client_client'] = 'Клиент';
$lang['client_add_page_title'] = 'Добавить клиента';
$lang['client_view_page_title'] = 'Список клиентов';
$lang['client_new_client'] = 'Новый клиент';
$lang['client_edit_client'] = 'Изменить клиента';
$lang['client_delete_client'] = 'Удаление клиента';
$lang['client_details'] = 'Клиент подробности';
$lang['client_form_user_name'] = 'Клиент имя пользователя';
$lang['client_form_company'] = 'Название компании';
$lang['client_form_industry'] = 'Промышленность и бизнес типа';
$lang['client_form_client_since'] = 'Клиент, поскольку';
$lang['client_form_address'] = 'Адрес';
$lang['client_form_city'] = 'Город';
$lang['client_form_state'] = 'Государство или регион';
$lang['client_form_zip'] = 'Почтовый код';
$lang['client_form_country'] = 'Страна';
$lang['client_form_timezone'] = 'Часовой пояс';
$lang['client_form_contact_person'] = 'Основным контактным лицом';
$lang['client_form_phone1'] = 'Первичный телефонный номер';
$lang['client_form_phone2'] = 'Телефон 2';
$lang['client_form_phone3'] = 'Телефон 3';
$lang['client_form_email'] = 'Адрес электронной почты';
$lang['client_form_url'] = 'Веб-сайт';
$lang['client_form_category'] = 'Категория клиентов';
$lang['client_form_im'] = 'Для обмена мгновенными сообщениями';
$lang['client_form_notes'] = 'Заметки для этого клиента';
$lang['client_form_tags'] = 'Теги';
$lang['client_client_contacts'] = 'Клиент контакты';
$lang['finance_page_title'] = 'Финансы';
$lang['finance_view_page_title'] = 'Финансовый обзор';
$lang['finance_view_invoices'] = 'Просмотр счета-фактуры';
$lang['finance_view_expenses'] = 'Вид расходов';
$lang['finance_reports'] = 'Финансы доклады';
$lang['finance_total_invoiced'] = 'Всего к оплате';
$lang['finance_invoices'] = 'Счета-фактуры';
$lang['finance_invoice'] = 'Счет';
$lang['finance_date_sent'] = 'Дата отправки';
$lang['finance_date_created'] = 'Дата создания';
$lang['finance_expense_new'] = 'Новый счет';
$lang['finance_expense_edit'] = 'Изменить счет';
$lang['finance_invoice_new'] = 'Новые счета-фактуры';
$lang['finance_invoice_edit'] = 'Изменить счет-фактура';
$lang['finance_invoice_details'] = 'Информация о счете-фактуре';
$lang['finance_invoice_number'] = 'Номер счета-фактуры';
$lang['finance_invoice_number_abbr'] = '№ счета-фактуры';
$lang['finance_invoice_ponumber'] = 'Купить номер заказа';
$lang['finance_invoice_category'] = 'Счет категории';
$lang['finance_invoice_date'] = 'Дата счета-фактуры';
$lang['finance_last_invoice_number'] = 'Последний номер счета:';
$lang['finance_invoice_title'] = 'Счет титула';
$lang['finance_invoice_message'] = 'Сообщения и комментарии';
$lang['finance_invoice_message_print'] = 'Версия для печати сообщения о счете-фактуре.';
$lang['finance_tax_information'] = 'Налоговая информация';
$lang['finance_tax_rate'] = 'Налоговая ставка';
$lang['finance_tax_name'] = 'Налоговые имя';
$lang['finance_tax_id'] = 'ИНН';
$lang['finance_invoice_freight'] = 'Грузовые заряд';
$lang['finance_invoice_payment_due'] = 'Оплата из-за';
$lang['finance_invoice_late_fee'] = 'Поздний сбор';
$lang['finance_invoice_discount'] = 'Скидка';
$lang['finance_invoice_email'] = 'Email счета';
$lang['finance_invoice_item_description'] = 'Описание';
$lang['finance_invoice_item_type'] = 'Печатать';
$lang['finance_invoice_item_qty'] = 'Количество';
$lang['finance_invoice_item_cost'] = 'Стоимость / Оценить';
$lang['finance_invoice_item_tax'] = 'Налоговые';
$lang['finance_invoice_bill_to'] = 'Законопроект';
$lang['finance_invoice_sales_tax'] = 'Налог';
$lang['finance_invoice_create_invoice'] = 'Создать счет-фактуру';
$lang['finance_invoice_add_to_invoice'] = 'Добавить в счете-фактуре';
$lang['finance_invoice_type_service'] = 'Службы';
$lang['finance_invoice_type_product'] = 'Продукт';
$lang['finance_invoice_type_hours'] = 'Часов';
$lang['finance_invoice_type_days'] = 'Дни';
$lang['project_page_title'] = 'Проекты';
$lang['project_add_page_title'] = 'Новый проект';
$lang['project_edit_page_title'] = 'Редактирование проекта';
$lang['project_total'] = 'Всего проектов';
$lang['project_completion_date'] = 'Срок сдачи';
$lang['project_project_contacts'] = 'Проект контакты';
$lang['project_details'] = 'Детали проекта';
$lang['project_tasks'] = 'Задание';
$lang['project_dates'] = 'Сроки проекта';
$lang['project_form_select_client'] = 'Выбор клиента';
$lang['project_form_title'] = 'Название проекта';
$lang['project_select_project'] = 'Выбор проекта';
$lang['project_form_notes'] = 'Замечания по этому проекту';
$lang['project_form_status'] = 'Статус проекта';
$lang['project_form_category'] = 'Проект категории';
$lang['project_form_url'] = 'Проект веб-сайта';
$lang['project_form_team_members'] = 'Выбор членов группы по этому проекту';
$lang['contact_no_contacts'] = 'У вас еще нет контактов.';
$lang['contact_create'] = 'Создание нового контакта.';
$lang['contact_new'] = 'Новый контакт';
$lang['task_assigned_to'] = 'Назначить задание';
$lang['task_description'] = 'Описание';
$lang['task_dates'] = 'Сроки (начало: конец)';
$lang['task_estimated_time'] = 'Ориентировочная часов';
$lang['task_title'] = 'Имя или название';
$lang['task_priority_low'] = 'Низко';
$lang['task_priority_medium'] = 'Средне';
$lang['task_priority_high'] = 'Высокий';
$lang['task_priority_extreme'] = 'Экстрим';
$lang['task_priority'] = 'Целевая приоритет';
$lang['task_task'] = 'Задача';
$lang['task_tasks'] = 'Задание';
$lang['task_incomplete'] = 'Неполные задач';
$lang['task_new_milestone'] = 'Новая веха';
$lang['task_task_saved'] = 'Задача была сохранена.';
$lang['task_milestone_saved'] = 'Поэтапные была сохранена.';
$lang['task_select_task'] = 'Выбор задачи';
$lang['calendar'] = 'Календарь';
$lang['calendar_new'] = 'Новые события';
$lang['files_files'] = 'Файлы';
$lang['files_file_manager'] = 'Файловый менеджер';
$lang['files_upload_files'] = 'Загрузка файлов';
$lang['files_folder'] = 'Папка';
$lang['files_new_folder'] = 'Новая папка';
$lang['files_upload'] = 'Загрузка файлов';
$lang['files_upload_logo'] = 'Загрузка логотипа';
$lang['files_upload_avatar'] = 'Загрузить аватар';
$lang['files_select'] = 'Выберите файлы';
$lang['files_attach'] = 'Вложить файл';
$lang['team_my_team'] = 'Наша команда';
$lang['team_select_team_member'] = 'Выбор членов группы';
$lang['team_member'] = 'Член команды';
$lang['timesheets'] = 'Табель';
$lang['timesheets_reports'] = 'Табель доклады';
$lang['timesheets_project_task'] = 'Проект или задача';
$lang['timesheets_select_project'] = 'Пожалуйста, выберите один проект.';
$lang['timesheets_select_project_only'] = 'Выберите проект только';
$lang['timesheets_view_your_timesheet'] = 'Просмотр табель';
$lang['timesheets_elapsed_time'] = 'Истекшее время';
$lang['admin_page_title'] = 'Администрация';
$lang['settings_page_title'] = 'Управление Настройки';
$lang['settings_logos'] = 'Компания логотипы';
$lang['bugreport_page_title'] = 'Сообщить об ошибках';
$lang['bugreport_instructions'] = 'Пожалуйста, опишите ошибку в пространстве ниже.';
$lang['bugreport_submit'] = 'Добавить ошибке';
$lang['bugreport_success'] = 'Ошибка была представлена.';
?>