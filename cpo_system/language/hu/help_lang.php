<?
$lang['help_formfield_clientUserid'] = 'Az ügyfele van szükség annak érdekében, hogy a felhasználói hozzáférés a CreativePro Hivatal számlájára. Meg kell hagyja el az ügyfél tudja, melyik userid kiválasztott. <br /> <b>CreativePro Hivatal nem az Ön e-mail kliens a userid.</b>';
$lang['help_formfield_tags'] = 'Tags egy módja annak, hogy kategorizáld tételek, mint a projektek, feladatok, illetve az ügyfelek, hogy van értelme veled. Tags is kereshetők.';
$lang['help_tag_format'] = 'Separate tags szóközökkel.';
$lang['help_revise_search'] = 'Próbáld meg a keresési feltételek felülvizsgálatát és a keresést újra.';
?>