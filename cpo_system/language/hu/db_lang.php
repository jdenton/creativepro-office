<?
$lang['db_invalid_connection_str'] = 'Nem sikerült meghatározni az adatbázis beállítások alapján a kapcsolat string you benyújtani.';
$lang['db_unable_to_connect'] = 'Nem lehet kapcsolódni az adatbázishoz segítségével a megadott beállításokat.';
$lang['db_unable_to_select'] = 'Nem sikerült kiválasztani a megadott adatbázisban:% s';
$lang['db_unable_to_create'] = 'Nem sikerült létrehozni a megadott adatbázisban:% s';
$lang['db_invalid_query'] = 'A lekérdezés meg benyújtott nem érvényes.';
$lang['db_must_set_table'] = 'Meg kell állítani az adatbázist táblázatot kell használni a query.';
$lang['db_must_set_database'] = 'Állítsd be az adatbázis nevét az adatbázishoz config file.';
$lang['db_must_use_set'] = 'Ki kell használni a &quot;set&quot; módszer, hogy frissítse a bejegyzést.';
$lang['db_must_use_where'] = 'A frissítések nem megengedettek, hacsak nem tartalmazza a &quot;ha&quot; záradék.';
$lang['db_del_must_use_where'] = 'Törli nem megengedettek, hacsak nem tartalmazza a &quot;ha&quot;, vagy &quot;hasonló&quot; záradék.';
$lang['db_field_param_missing'] = 'Ahhoz, hogy letöltse területen megköveteli, hogy a neve a táblázat a paramétert.';
$lang['db_unsupported_function'] = 'Ez a funkció nem elérhető az adatbázis használata.';
$lang['db_transaction_failure'] = 'Tranzakciós hiba: Rollback végeztek.';
$lang['db_unable_to_drop'] = 'Nem lehet bontani a megadott adatbázisban.';
$lang['db_unsuported_feature'] = 'Unsupported jellemzője az adatbázis-platform használata.';
$lang['db_unsuported_compression'] = 'A fájl formátum Ön úgy döntött, nem támogatja a szerver.';
$lang['db_filepath_error'] = 'Nem lehet írni az adatokat, hogy a fájl elérési útját, akkor benyújtani.';
$lang['db_invalid_cache_path'] = 'A cache-path you benyújtott érvényes vagy nem írható.';
$lang['db_table_name_required'] = 'A tábla név szükséges, hogy a műveletet.';
$lang['db_column_name_required'] = 'Az oszlop neve van szükség, hogy a műveletet.';
$lang['db_column_definition_required'] = 'A oszlopban a meghatározás szükséges, hogy a műveletet.';
$lang['db_unable_to_set_charset'] = 'Nem sikerült megállapítani az ügyfél kapcsolat karakterkészlet:% s';
$lang['db_error_heading'] = 'A Database Error Occurred';
?>