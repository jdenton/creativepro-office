<?
$lang['email_must_be_array'] = 'Az e-mail validálás módszerét kell tenni egy tömbben.';
$lang['email_invalid_address'] = 'Érvénytelen e-mail cím:% s';
$lang['email_attachment_missing'] = 'Nem találom a következő e-mail mellékletként:% s';
$lang['email_attachment_unreadable'] = 'Nem sikerült megnyitni ezt a mellékletet:% s';
$lang['email_no_recipients'] = 'Meg kell tartalmaznia címzettek: To, CC, vagy a Titkos másolat';
$lang['email_send_failure_phpmail'] = 'Nem lehet elküldeni e-mail PHP mail (). A kiszolgáló nem lehet beállítani, hogy küldjön mailt használja ezt a módszert.';
$lang['email_send_failure_sendmail'] = 'Nem lehet elküldeni e-mail PHP Sendmail. A kiszolgáló nem lehet beállítani, hogy küldjön mailt használja ezt a módszert.';
$lang['email_send_failure_smtp'] = 'Nem lehet elküldeni e-mail PHP SMTP. A kiszolgáló nem lehet beállítani, hogy küldjön mailt használja ezt a módszert.';
$lang['email_sent'] = 'Üzenetét sikeresen elküldte a következő protokoll:% s';
$lang['email_no_socket'] = 'Nem sikerült megnyitni a socket a Sendmail. Kérjük ellenőrizze a beállításokat.';
$lang['email_no_hostname'] = 'Ön nem adta meg az SMTP hostname.';
$lang['email_smtp_error'] = 'A következő SMTP hibát találtam:% s';
$lang['email_no_smtp_unpw'] = 'Hiba: Nem kell hozzá SMTP felhasználónevet és jelszót.';
$lang['email_failed_smtp_login'] = 'Nem sikerült elküldeni AUTH LOGIN parancsot. Hiba:% s';
$lang['email_smtp_auth_un'] = 'Nem sikerült hitelesíteni a felhasználónevet. Hiba:% s';
$lang['email_smtp_auth_pw'] = 'Nem sikerült hitelesíteni a jelszót. Hiba:% s';
$lang['email_smtp_data_failure'] = 'Nem sikerült adatokat küldeni:% s';
?>