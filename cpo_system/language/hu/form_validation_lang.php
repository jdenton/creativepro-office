<?
$lang['required'] = 'A% s területen van szükség.';
$lang['isset'] = 'A% s területen kell lennie értéknek.';
$lang['valid_email'] = 'A% s mező tartalmaznia kell egy érvényes e-mail címét.';
$lang['valid_emails'] = 'A% s mező tartalmazza az összes érvényes e-mail címeket.';
$lang['valid_url'] = 'A% s mező tartalmaznia kell egy érvényes URL-t.';
$lang['valid_ip'] = 'A% s mező tartalmaz érvényes IP.';
$lang['min_length'] = 'A% s mező legalább% s karakter hosszú.';
$lang['max_length'] = 'A% s mező nem haladhatja meg:% s karakter hosszúságú.';
$lang['exact_length'] = 'A% s mező pontosan% s karakter hosszú.';
$lang['alpha'] = 'A% s területen csak alfabetikus karaktereket.';
$lang['alpha_numeric'] = 'A% s területen csak alfanumerikus karakter.';
$lang['alpha_dash'] = 'A% s területen csak alfanumerikus karakterek, aláhúzások és kötőjelek.';
$lang['numeric'] = 'A% s mező csak számokat.';
$lang['is_numeric'] = 'A% s mező csak numerikus karaktereket.';
$lang['integer'] = 'A% s mező tartalmazza az egész.';
$lang['matches'] = 'A% s terület nem egyezik a% s mező.';
$lang['is_natural'] = 'A% s mező csak pozitív számok.';
$lang['is_natural_no_zero'] = 'A% s mező tartalmazza a szám nagyobb, mint nulla.';
$lang['form_required_clientUserid'] = 'Vendégek userid nem lehet üres.';
$lang['form_istaken_clientUserid'] = 'Vendégek userid% s alreay venni. Kérjük, válasszon egy másik.';
$lang['form_required_clientCompany'] = 'Vendégek társaság nem lehet üres.';
$lang['form_required_client'] = 'Vendégek nem lehet üres.';
$lang['form_required_project_title'] = 'A projekt címe nem lehet üres.';
$lang['form_required_email'] = 'Kérem adjon meg egy érvényes e-mail címét.';
$lang['form_minlength_clientEmail'] = 'Ügyfél e-mail címet kell 10 karakter.';
?>