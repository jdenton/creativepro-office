<?
$lang['db_invalid_connection_str'] = 'Nie można określić ustawienia bazy danych w oparciu o ciąg połączenia zgłoszenia.';
$lang['db_unable_to_connect'] = 'Nie można połączyć się z serwerem bazy danych za pomocą dostarczonego ustawienia.';
$lang['db_unable_to_select'] = 'Nie można wybrać określony bazy:% s';
$lang['db_unable_to_create'] = 'Nie można utworzyć określonej bazy danych:% s';
$lang['db_invalid_query'] = 'Zapytanie nie są ważne.';
$lang['db_must_set_table'] = 'Musisz ustawić tabeli bazy danych, które mają być stosowane z zapytanie.';
$lang['db_must_set_database'] = 'Musisz ustawić nazwę bazy danych w bazie danych w pliku konfiguracyjnym.';
$lang['db_must_use_set'] = 'Musisz użyć &quot;zestaw&quot; metody aktualizacji wpisu.';
$lang['db_must_use_where'] = 'Aktualizacje są niedozwolone, chyba że zawierają &quot;, gdzie&quot; klauzuli.';
$lang['db_del_must_use_where'] = 'Usuwa są niedozwolone, chyba że zawierają &quot;gdzie&quot; lub &quot;podobny&quot; klauzuli.';
$lang['db_field_param_missing'] = 'Aby pobrać dziedzinach wymaga nazwę tabeli jako parametr.';
$lang['db_unsupported_function'] = 'Ta funkcja nie jest dostępna dla baz danych, którego używasz.';
$lang['db_transaction_failure'] = 'Transakcja awarii: Rollback wykonywane.';
$lang['db_unable_to_drop'] = 'Nie można usunąć określonej bazy danych.';
$lang['db_unsuported_feature'] = 'Nieobsługiwana funkcja bazy danych platformy używasz.';
$lang['db_unsuported_compression'] = 'Plik kompresji wybranego formatu nie jest obsługiwana przez serwer.';
$lang['db_filepath_error'] = 'Nie można zapisać danych do pliku ścieżka masz złożony.';
$lang['db_invalid_cache_path'] = 'Cache ścieżki nie są ważne lub zapisywalny.';
$lang['db_table_name_required'] = 'A nazwa tabeli jest wymagany dla tej operacji.';
$lang['db_column_name_required'] = 'Kolumna nazwa jest wymagany dla tej operacji.';
$lang['db_column_definition_required'] = 'Kolumna definicji jest wymagany dla tej operacji.';
$lang['db_unable_to_set_charset'] = 'Nie można ustawić połączenie klienta zestawu znaków:% s';
$lang['db_error_heading'] = 'Baza danych błąd';
?>