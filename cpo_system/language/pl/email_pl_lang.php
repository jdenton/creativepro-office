<?
$lang['email_must_be_array'] = 'E-mail walidacji metody musi być przekazana tablica.';
$lang['email_invalid_address'] = 'Błędny adres e-mail:% s';
$lang['email_attachment_missing'] = 'Nie można zlokalizować następujący załącznik e-mail:% s';
$lang['email_attachment_unreadable'] = 'Nie można otworzyć tego zajęcia:% s';
$lang['email_no_recipients'] = 'Musisz podać odbiorców: Do, DW lub UDW';
$lang['email_send_failure_phpmail'] = 'Nie można wysłać e-mail za pomocą PHP mail (). Twój serwer może nie być skonfigurowane do wysyłania poczty przy użyciu tej metody.';
$lang['email_send_failure_sendmail'] = 'Nie można wysłać e-mail za pomocą PHP Sendmail. Twój serwer może nie być skonfigurowane do wysyłania poczty przy użyciu tej metody.';
$lang['email_send_failure_smtp'] = 'Nie można wysłać e-mail za pomocą PHP SMTP. Twój serwer może nie być skonfigurowane do wysyłania poczty przy użyciu tej metody.';
$lang['email_sent'] = 'Twoja wiadomość została wysłana za pomocą następujących protokołów:% s';
$lang['email_no_socket'] = 'Nie można otworzyć gniazda do Sendmail. Proszę sprawdzić ustawienia.';
$lang['email_no_hostname'] = 'Nie określa SMTP hostname.';
$lang['email_smtp_error'] = 'Następujące SMTP napotkano na błąd:% s';
$lang['email_no_smtp_unpw'] = 'Błąd: Musisz przypisać SMTP użytkownika i hasło.';
$lang['email_failed_smtp_login'] = 'Nie można wysłać polecenia AUTH LOGIN. Błąd:% s';
$lang['email_smtp_auth_un'] = 'Nie powiodło się uwierzytelnienie użytkownika. Błąd:% s';
$lang['email_smtp_auth_pw'] = 'Nie powiodło się uwierzytelnienie hasła. Błąd:% s';
$lang['email_smtp_data_failure'] = 'Nie można wysłać dane:% s';
?>