<?
$lang['finance_invoice_number_abbr'] = '# Faktura';
$lang['finance_invoice_message'] = 'Możesz przeglądać i wydrukować fakturę ze <strong><a href="%s">strony klienta</a></strong> .';
$lang['finance_invoice_message_paypal'] = 'Można również <strong><a href="%s">płacić w niniejszej fakturze,</a></strong> za pośrednictwem systemu PayPal.';
?>