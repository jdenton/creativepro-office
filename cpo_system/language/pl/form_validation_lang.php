<?
$lang['required'] = '% S pole jest wymagane.';
$lang['isset'] = '% S pole musi mieć wartość.';
$lang['valid_email'] = '% S dziedzinie musi zawierać prawidłowy adres e-mail.';
$lang['valid_emails'] = '% S pole musi zawierać wszystkie adresy e-mailowe.';
$lang['valid_url'] = '% S dziedzinie musi zawierać prawidłowy adres URL.';
$lang['valid_ip'] = '% S dziedzinie musi zawierać ważnych IP.';
$lang['min_length'] = '% S pole musi mieć co najmniej% s znaków.';
$lang['max_length'] = '% S pola nie może przekraczać% s znaków.';
$lang['exact_length'] = '% S dziedzinie musi być dokładnie% s znaków.';
$lang['alpha'] = '% S pole może zawierać jedynie znaki alfabetycznym.';
$lang['alpha_numeric'] = '% S pole może zawierać jedynie znaki alfanumeryczne.';
$lang['alpha_dash'] = '% S pole może zawierać jedynie znaki alfanumeryczne, podkreślenia i myślniki.';
$lang['numeric'] = '% S pole musi zawierać tylko cyfry.';
$lang['is_numeric'] = '% S pole musi zawierać tylko znaki numeryczne.';
$lang['integer'] = '% S dziedzinie musi zawierać integer.';
$lang['matches'] = '% S dziedzinie nie pasuje do% s dziedzinie.';
$lang['is_natural'] = '% S pola muszą zawierać jedynie pozytywne numery.';
$lang['is_natural_no_zero'] = '% S dziedzinie musi zawierać pewną liczbę większą niż zero.';
$lang['form_required_clientUserid'] = 'Userid Klient nie może być puste.';
$lang['form_istaken_clientUserid'] = 'Klient userid% s jest alreay podjęte. Proszę wybrać inną.';
$lang['form_required_clientCompany'] = 'Klient firmy nie może być puste.';
$lang['form_required_client'] = 'Klient nie może być puste.';
$lang['form_required_project_title'] = 'Tytuł projektu nie może być puste.';
$lang['form_required_email'] = 'Wprowadź prawidłowy adres e-mail.';
$lang['form_minlength_clientEmail'] = 'Klient e-mail musi być 10 znaków.';
?>