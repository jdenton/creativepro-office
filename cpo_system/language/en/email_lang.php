<?php

$lang['email_must_be_array'] = "The email validation method must be passed an array.";
$lang['email_invalid_address'] = "Invalid email address: %s";
$lang['email_attachment_missing'] = "Unable to locate the following email attachment: %s";
$lang['email_attachment_unreadable'] = "Unable to open this attachment: %s";
$lang['email_no_recipients'] = "You must include recipients: To, Cc, or Bcc";
$lang['email_send_failure_phpmail'] = "Unable to send email using PHP mail().  Your server might not be configured to send mail using this method.";
$lang['email_send_failure_sendmail'] = "Unable to send email using PHP Sendmail.  Your server might not be configured to send mail using this method.";
$lang['email_send_failure_smtp'] = "Unable to send email using PHP SMTP.  Your server might not be configured to send mail using this method.";
$lang['email_sent'] = "Your message has been successfully sent using the following protocol: %s";
$lang['email_no_socket'] = "Unable to open a socket to Sendmail. Please check settings.";
$lang['email_no_hostname'] = "You did not specify a SMTP hostname.";
$lang['email_smtp_error'] = "The following SMTP error was encountered: %s";
$lang['email_no_smtp_unpw'] = "Error: You must assign a SMTP username and password.";
$lang['email_failed_smtp_login'] = "Failed to send AUTH LOGIN command. Error: %s";
$lang['email_smtp_auth_un'] = "Failed to authenticate username. Error: %s";
$lang['email_smtp_auth_pw'] = "Failed to authenticate password. Error: %s";
$lang['email_smtp_data_failure'] = "Unable to send data: %s";

/*
 * Canned system messages.
 */
$lang['email_shared_event'] = "Someone wants to share an event with you.";

$lang['email_new_team_member_message'] = "Hello! %s has added you as a team member to their CreativePro Office account with a level of %s. To access your accout, go to ".SITE_URL." and login."
    .Chr(10)
    .Chr(10)."Your user name is: %s".Chr(10).Chr(10)."Your password is:  %s"
    .Chr(10)
    .Chr(10)."You can change your password after login by clicking on the 'Settings' icon in the upper-right portion of the screen. If you need help or have questions, please contact ".MAIL_SUPPORT."."
    .Chr(10)
    .Chr(10)."Welcome to CreativePro Office!";
$lang['email_new_team_member_subject'] = "Welcome to the team on CreativePro Office!";

$lang['email_new_client_message'] = "Hello!"
    .Chr(10)."%s has added %s as a client to their CreativePro Office account. Your CreativePro Office client account allows you to quickly communicate with %s about projects, invoices, payments, and more. To learn more about your CreativePro Office account, go to ".SITE_URL."/clientAccount."
    .Chr(10).Chr(10)
    ."To access your accout, go to ".SITE_URL." and login."
    .Chr(10).Chr(10)."Your user name is %s".Chr(10).Chr(10)."Your password is  %s"
    .Chr(10)
    .Chr(10)."You can change your password after login by clicking on the 'Settings' icon in the upper-right portion of the screen. If you need help or have questions, please contact ".MAIL_SUPPORT."."
    .Chr(10)
    .Chr(10)."Welcome to CreativePro Office!";
$lang['email_new_client_subject'] = "Welcome to your CreativePro Office account!";

$lang['email_task_subject']            = "Task Message: %s";
$lang['email_task_message']            = "%s sent the following message regarding task %s.";

$lang['email_task_new_subject']        = "New Task: %s";
$lang['email_task_new_message']        = "%s has assigned task %s to you.";

$lang['email_task_edit_subject']       = "Updated task: %s";
$lang['email_task_edit_message']       = "%s has updated task %s.";

$lang['email_task_link']                = "To view details for this task, click the following link:";

$lang['email_calendar_subject']        = "Shared Calendar: %s";
$lang['email_calendar_message']        = "%s has shared a calendar with you: %s.";

$lang['email_resetPassword_subject']   = "Password Reset: CreativePro Office";
$lang['email_resetPassword_message']   = "Your password for the following accounts have been reset. To change your password once you login, click on the Settings icon in the upper-right portion of the screen.";


$lang['email_footer']                  = ".:-----:.".Chr(10)."Please contact us at ".MAIL_SUPPORT." with any questions.".Chr(10)."To control which messages your receive from CreativePro Office, go to:".Chr(10).EMAIL_SETTINGS_LINK;

$lang['email_invoice_subject']         = "Invoice %s from %s";

$lang['email_new_account_message']     = "Thank you for giving CreativePro Office a try. You can access your account by going to:"
    .Chr(10).Chr(10).BASE_URL."login"
    .Chr(10)."User Name: %s"
    .Chr(10)."Password:  <private>"
    .Chr(10).Chr(10)."You have selected the %s plan:"
    .Chr(10)."    Cost:            %s"
    .Chr(10)."    Active projects: %s"
    .Chr(10)."    Storage:         %s"
    .Chr(10)."    Team members:    %s"
    .Chr(10).Chr(10)."You can upgrade or downgrade your account at any time by going to:"
    .Chr(10).BASE_URL."account"
    .Chr(10).Chr(10)."If you need help, please contact support at ".MAIL_SUPPORT;

$lang['email_assignToProject_subject'] = "Project assignment: %s";
$lang['email_assignToProject_message'] = "%s has assigned you to the project, %s."
    .Chr(10).Chr(10)."To view details of this project, please log in to your CreativePro Office account.";

$lang['email_event_new_subject']        = "Shared Event: %s";
$lang['email_event_new_message']        = "%s has shared an event with you.";

$lang['email_event_edit_subject']       = "Updated event: %s";
$lang['email_event_edit_message']       = "%s has updated an event.";

$lang['email_event_link']               = "To view details for this event, click the following link:";


/* End of file email_lang.php */
/* Location: ./system/language/english/email_lang.php */