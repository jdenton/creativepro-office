<?php
/*
 * ENGLISH TRANSLATION
 */

/*
 * ERROR/SUCCESS MESSAGES
 */
$lang['error_page_title']     = 'Error';
$lang['no_records_found']     = 'No records were found.';
$lang['error_save']           = 'There was an error saving.'; // **
$lang['error_file_too_large'] = 'The file you tried to upload is too large.'; // **
$lang['error_incorrect_file_type'] = 'You are trying to upload an incorrect file type. Supported file types are %s';
$lang['error_unknown_error']  = 'There was an error but we don\'t know why!';
$lang['error_cant_view_this'] = 'You do not have permission to view this item. Sorry about that!';
$lang['error_task_deleted']   = 'This task had been deleted!'; // **
$lang['error_event_deleted']  = 'This event had been deleted!'; // **
$lang['error_password_missing_field'] = 'You must enter your current password and new password.'; // **
$lang['error_password_incorrect']     = 'Your current password is not right.'; // **
$lang['error_account_exists']         = 'An account with this password already exists. Please select a new password.'; //**
$lang['error_title_404']              = 'Uh oh! The page you want is not here!';
$lang['error_title_PHP']              = 'Uh oh! There was a programming error somewhere!';
$lang['error_title_general']          = 'Uh oh! Some kind of error happened! (We\'ll look into it.)';
$lang['error_title_database']         = 'Uh oh! The database is acting funny!';
$lang['error_title_offline']          = 'Uh oh! CreativePro Office is down for maintenance!';
$lang['error_site_text']              = "We'll do our best to fix the problem soon. In the meantime you can <strong><a href=\"contact\">contact us</a></strong> with any questions or comments.<br /><br />Thanks for your patience!";
$lang['error_login_empty_field']      = 'Please enter a User Name and Password.';

$lang['success_password_saved']       = 'Your new password has been saved.';
$lang['success_data_import']          = 'Your data was imported successfully!';

/*
 * STATUS INDICATORS
 */
$lang['status_not_started']     = 'Not started';
$lang['status_in_progress']     = 'In progress';
$lang['status_due_in']          = 'Due in %s %s'; // Due in 6 days.  or Due in 1 day.
$lang['status_due_on']          = 'Due on %s';    // Due on Jan 1, 2009.
$lang['status_due_today']       = 'Due today';    // **
$lang['status_due_tomorrow']    = 'Due tomorrow';    // **
$lang['status_due_yesterday']   = 'Due yesterday';    // **
$lang['status_start_on']        = 'Start on %s';
$lang['status_completed']       = 'Completed';
$lang['status_incomplete']      = 'Incomplete'; // **
$lang['status_overdue']         = 'Overdue';
$lang['status_on_hold']         = 'On hold';
$lang['status_not_sent']        = 'Not sent';
$lang['status_sent']            = 'Sent';
$lang['status_paid']            = 'Paid';
$lang['status_past_due']        = 'Past due';
$lang['status_due_awhile_ago']  = 'Due awhile ago';
$lang['status_past_due_text']    = '%s %s past due'; // 6 days past due. Or 1 year past due.
$lang['status_past_due_text_year'] = 'More than 1 year past due';
$lang['status_not_read']        = 'Not read';
$lang['status_read']            = 'Read';
$lang['status_urgent']          = 'Urgent';
$lang['status_needs_attention'] = 'Needs attention';
$lang['status_open_sent_on']    = 'Open: sent on';
$lang['status_open_created_on'] = 'Open: created on';
$lang['status_closed_paid']     = 'Closed: paid';
$lang['status_closed_paid_on']  = 'Closed: paid on';
$lang['status_closed_not_paid'] = 'Closed: not paid';
$lang['status_status']          = 'Status';
$lang['status_click_to_update'] = 'Click to update status'; // **

/*
 * MAIN, SUB MENU ITEMS
 */
$lang['menu_add_dashboard_widgets'] = 'Add dashboard widgets';
$lang['menu_clients']               = 'Clients';
$lang['menu_client']                = 'Client'; // **
$lang['menu_view_clients']          = 'View clients';
$lang['menu_new_client']            = 'New client';

$lang['menu_projects']              = 'Projects';
$lang['menu_view_projects']         = 'View projects';
$lang['menu_new_project']           = 'New project';
$lang['menu_project_templates']     = 'Project templates';

$lang['menu_timesheets']            = 'Timesheets';
$lang['menu_view_timesheets']       = 'View timesheets';
$lang['menu_view_timesheet_reports']= 'View timesheet reports';

$lang['menu_finances']        = 'Finances';
$lang['menu_view_ledger']     = 'View ledger';
$lang['menu_view_finances']   = 'Finance overview';
$lang['menu_new_invoice']     = 'New invoice';
$lang['menu_new_expense']     = 'New expense';
$lang['menu_add_income']      = 'Add income';
$lang['menu_add_expense']     = 'Add expense';
$lang['menu_new_estimate']    = 'New estimate';
$lang['menu_finance_reports'] = 'Finance reports';

/*
 * REPORTS
 */
$lang['reports_invoiced_received']        = 'Amounts invoiced and received by month'; // **
$lang['reports_invoiced_by_project']      = 'Amounts invoiced by project'; // **
$lang['reports_invoiced_by_client']       = 'Amounts invoiced by client'; // **
$lang['reports_most_profitable_projects'] = 'Most profitable projects'; // **
$lang['reports_most_profitable_clients']  = 'Most profitable clients'; // **
$lang['reports_income_expenses']          = 'Income and expenses by month'; // **
$lang['reports_expenses_by_project']      = 'Expenses by project'; // **
$lang['reports_expenses_by_client']       = 'Expenses by clients'; // **
$lang['reports_expenses_by_category']     = 'Expenses by category'; // **
$lang['reports_expenses_by_vendor']       = 'Expenses by vendor'; // **
$lang['reports_invoiced']                 = 'Invoiced'; // **
$lang['reports_received']                 = 'Received'; // **
$lang['reports_income']                   = 'Income'; // **
$lang['reports_finance_excel_export']     = 'Export report in Excel format'; // **

/*
 * DASHBOARD WIDGET TITLES
 */
$lang['widget_invoices']  = 'Invoices open';
$lang['widget_quotes']    = 'Quotes';
$lang['widget_calendar']  = 'Calendar';
$lang['widget_rss']       = 'RSS Feeds';
$lang['widget_notes']     = 'Notes';
$lang['widget_bugs']      = 'Bugs';
$lang['widget_projects']  = 'Projects';
$lang['widget_tasks']     = 'Upcoming &amp; Overdue Tasks';
$lang['widget_messages']  = 'Messages';
$lang['widget_reports']   = 'Reports';
$lang['widget_delicious'] = 'Delicious.com';
$lang['widget_today']     = 'What\'s Happening?';
$lang['widget_timer']     = 'Job Timer';

/*
 * COMMON WORDS, PHRASES AND TERMS
 */
$lang['common_account']        = 'Account';
$lang['common_your_account']   = 'My account';
$lang['common_email']          = 'Email';
$lang['common_form_more_link'] = 'Show more fields';
$lang['common_field_select']   = 'Select...';
$lang['common_first_name']     = 'First Name';
$lang['common_last_name']      = 'Last Name';
$lang['common_messages']       = 'Messages';
$lang['common_discussions']    = 'Discussions';
$lang['common_messages_invoice'] = 'Invoice messages';
$lang['common_messages_expense'] = 'Expense messages';
$lang['common_language']       = 'Change language';
$lang['common_alerts']         = 'Alerts';
$lang['common_search']         = 'Search';
$lang['common_company_name']   = 'Company Name';
$lang['common_company_info']   = 'Company Information';
$lang['common_invoice_total']  = 'Invoice Total';
$lang['common_estimate_total'] = 'Estimate Total';
$lang['common_number_projects']= 'Number of Projects';
$lang['common_loading_clients']= 'Loading clients...';
$lang['common_loading_map']    = 'Loading map...';
$lang['common_loading_data']   = 'Loading data...';
$lang['common_hours']          = 'Hours';
$lang['common_status']         = 'Status';
$lang['common_status_update']  = 'Update status';
$lang['common_totals']         = 'Totals';
$lang['common_total']          = 'Total';
$lang['common_subtotal']       = 'Sub total';
$lang['common_title']          = 'Title';
$lang['common_number']         = 'Number';
$lang['common_help']           = 'Help';
$lang['common_working']        = 'Working...';
$lang['common_loading']        = 'Loading...';
$lang['common_title']          = 'Title';
$lang['common_company']        = 'Company';
$lang['common_phone']          = 'Phone';
$lang['common_phone_number']   = 'Phone number';
$lang['common_welcome']        = 'Welcome';
$lang['common_search_office']  = 'Search your office';
$lang['common_send_message']   = 'Send a message';
$lang['common_send_message_to']= 'Send this message to';
$lang['common_send']           = 'Send';
$lang['common_date']           = 'Date';
$lang['common_date_start']     = 'Date start';
$lang['common_date_end']       = 'Date end';
$lang['common_date_due']       = 'Date due';
$lang['common_priority']       = 'Priority';
$lang['common_add_another']    = 'Add another';
$lang['common_download_file']  = 'Download file';
$lang['common_vcard']          = 'Download vCard';
$lang['common_other']          = 'Other';
$lang['common_filter_by']      = 'Filter by';

$lang['common_all']            = 'All';
$lang['common_open']           = 'Open';
$lang['common_closed']         = 'Closed';
$lang['common_past_due']       = 'Past due';
$lang['common_dates']          = 'Dates';

$lang['common_work']           = 'Work';
$lang['common_mobile']         = 'Mobile';
$lang['common_fax']            = 'Fax';
$lang['common_pager']          = 'Pager';
$lang['common_home']           = 'Home';
$lang['common_skype']          = 'Skype';
$lang['common_other']          = 'Other';
$lang['common_personal']       = 'Personal';
$lang['common_url']            = 'Website';

$lang['common_aim']            = 'AIM';
$lang['common_msn']            = 'MSN';
$lang['common_icq']            = 'ICQ';
$lang['common_jabber']         = 'Jabber';
$lang['common_yahoo']          = 'Yahoo';
$lang['common_googletalk']     = 'Google Talk';

$lang['common_immediately']    = 'Immediately';
$lang['common_specific_amount']= 'Specific amount';
$lang['common_specific_date']  = 'Specific date';
$lang['common_per_month']      = 'Per month';
$lang['common_billable']       = 'Billable';
$lang['common_create_report']  = 'Create report';
$lang['common_comments']       = 'Comments';
$lang['common_user']           = 'User';
$lang['common_yes']            = 'Yes';
$lang['common_no']             = 'No';
$lang['common_month']          = 'Month';

$lang['common_view']           = 'View'; // **
$lang['common_view_all']       = 'View all'; // **
$lang['common_view_personal_messages'] = 'View personal messages only'; // **
$lang['common_create']         = 'Create';  // **
$lang['common_update']         = 'Update'; // **
$lang['common_delete']         = 'Delete'; // **
$lang['common_select_all']     = 'Select all'; // **
$lang['common_photo_url']      = 'Photo URL'; // **
$lang['common_when']           = 'When'; // **
$lang['common_location']       = 'Location'; // **
$lang['common_no_tags']        = 'You have no tags here yet.'; // **
$lang['common_more']           = 'More'; // **
$lang['common_less']           = 'Less'; // **
$lang['common_add_guests']     = 'Add guests'; // **
$lang['common_contacts']       = 'Contacts'; // **
$lang['common_view_contacts']  = 'View contacts'; // **
$lang['common_find_contacts']  = 'Find contacts'; // **
$lang['common_contact']        = 'Contact'; // **
$lang['common_percent']        = 'Percent'; // **
$lang['common_created_by']     = 'Created by'; // **
$lang['common_page']           = 'Page'; // **
$lang['common_view_personal']  = 'View personal entries only'; // **
$lang['common_view_personal_trash']  = 'View personal trash items only'; // **
$lang['common_upload']         = 'Upload'; // **
$lang['common_per_hour']       = 'Per hour'; // **
$lang['common_per_day']        = 'Per day'; // **
$lang['common_language']       = 'Language'; // **
$lang['common_change_photo']   = 'Change photo'; // **
$lang['common_account_creator']= 'Account creator'; // **
$lang['common_vendor']         = 'Vendor'; // **
$lang['common_vendors']        = 'Vendors'; // **
$lang['common_trashcan']       = 'Trash can'; // **
$lang['common_no_trash']       = 'There are no items in the trash can.'; // **
$lang['common_message']        = 'Message'; // **
$lang['common_list_view']      = 'List view'; // **
$lang['common_list_view_return'] = 'Return to list view'; // **
$lang['common_warning']          = 'Warning'; // **
$lang['common_restore']          = 'Restore'; // **
$lang['common_new_password']     = 'New password'; // **
$lang['common_current_password'] = 'Your current password'; // **
$lang['common_no_projects']      = 'There are no projects right now.'; // **
$lang['common_no_invoices']      = 'There are no invoices right now.'; // **
$lang['common_no_invoices_for_date_range'] = 'There are no invoices for the selected date range.'; // **
$lang['common_no_ledger_for_date_range']   = 'There are no ledger entries for the selected date range.';
$lang['common_no_messages']      = 'There are no messages right now.'; // **
$lang['common_no_expenses']      = 'There are no expenses right now.'; // **
$lang['common_no_clients']       = 'There are no clients right now.'; // **
$lang['common_no_tasks']         = 'There are no tasks right now.'; // **
$lang['common_assigned_to']      = 'Assigned to'; // **
$lang['common_category']         = 'Category'; // **
$lang['common_theme']            = 'Theme'; // **
$lang['common_address_1']        = 'Address 1'; // **
$lang['common_address_2']        = 'Address 2'; // **
$lang['common_currency']         = 'Currency'; // **
$lang['common_data']             = 'Data'; // **
$lang['common_data_import']      = 'Import data'; // **
$lang['common_data_export']      = 'Export data'; // **
$lang['common_import_summary']   = 'Import summary'; // **
$lang['common_archive']          = 'Archive'; // **
$lang['common_upgrade_account']  = 'Upgrade your account.';
$lang['common_mark_message_read']= 'Mark message as read.';
$lang['common_export_excel']     = 'Export in Excel format';
$lang['common_export_options']   = 'Export options';
$lang['common_add_files']        = 'Add files';
$lang['common_start_upload']     = 'Start upload';
$lang['common_add_selected_items'] = 'Add selected items';
$lang['common_information']        = 'Information';
$lang['common_avatar']             = 'Avatar';
$lang['common_amount_due']         = 'Due';
$lang['common_new_entry']          = 'New entry';
$lang['common_grid_view']          = 'Grid view';
$lang['common_entry_view']         = 'Entry view';
$lang['common_previous']           = 'Previous';
$lang['common_next']               = 'Next';
$lang['common_no_entries_this_week'] = 'No entries for this week.';
$lang['common_back']                 = 'Back';
$lang['common_select_project_files'] = 'Select a project to view files.';
$lang['common_actions']              = 'Actions';
$lang['common_no_templates']         = 'You have no project templates right now.';
$lang['common_edit_template']        = 'Edit template';
$lang['common_sort_order']           = 'Sort order';
$lang['common_search_contacts']      = 'Search contacts';
$lang['common_drop_files_here']      = 'Drop files here to attach.';
$lang['common_send_copy_to_me']      = 'Send copy to me as well.';
$lang['common_hourly_rate']          = 'Hourly rate';
$lang['common_add_people']           = 'Add people';

$lang['insert_table']            = 'Insert table';
$lang['insert_image']            = 'Insert image';

/*
 * MESSAGE TEXT
 */
$lang['message_sent_concerning']    = '%s sent you a message concerning %s on %s.'; // Janice sent you a message concerning invoice on Jan 1, 2009.
$lang['message_sent']               = '%s sent you a message on %s.'; // Janice sent you a message on Jan 1, 2009.
$lang['message_concerning_invoice'] = 'an invoice'; 
$lang['message_concerning_project'] = 'a project'; 
$lang['message_concerning_file']    = 'a file'; 

/*
 * BUTTON TEXT
 */
$lang['button_save']         = 'Save';
$lang['button_cancel']       = 'Cancel';
$lang['button_add']          = 'New';
$lang['button_edit']         = 'Edit';
$lang['button_delete']       = 'Delete';
$lang['button_options']      = 'Options';
$lang['button_print']        = 'Print';
$lang['button_pdf']          = 'Create PDF';
$lang['button_new_contact']  = 'New contact';
$lang['button_close']        = 'Close';
$lang['button_clear']        = 'Clear';
$lang['button_remove']       = 'Remove';
$lang['button_export_excel'] = 'Excel';
$lang['button_im_done']      = 'I\'m done';

/*
 * LOGIN AREA MESSAGES
 */
$lang['login_page_title']               = 'Office Login';
$lang['login_user_name']                = 'User Name';
$lang['login_password']                 = 'Password';
$lang['login_button']                   = 'Login';
$lang['login_logout']                   = 'Logout';
$lang['login_forget_password']          = 'I forgot my password.'; 
$lang['login_message_logout']           = 'You are logged out.';
$lang['login_message_inactiveAccount']  = 'Your account is not active.';
$lang['login_message_roleMissing']      = 'You have not been assigned a role yet. A role is a user designation like Contractor, Super User, Accountant, etc. Please contact the person who set up your account and ask them to assign you to a user role.';
$lang['login_message_missingField']     = 'You are missing some fields.';
$lang['login_message_noAccount']        = 'We cannot find your account. Perhaps your email or password are incorrect or your account has been deactivated. Please contact <a href="mailto:'.MAIL_SUPPORT.'">'.MAIL_SUPPORT.'</a> with any questions.';
$lang['login_client_page_title']        = 'Client Login';
$lang['login_find_password']            = 'Find my password';
$lang['login_default']                  = 'Welcome! Please login to your CreativePro Office account below.';
$lang['login_message_expired']          = 'For security reasons, your session has expired. Please login again.';
$lang['login_message_noAccount2']       = 'We cannot find your account. Please try again.';

/*
 * DASHBOARD AREA TERMS
 */
$lang['dashboard_page_title'] = 'Office Dashboard';

/*
 * CLIENT AREA MESSAGES
 */
$lang['client_page_title']          = 'Clients';
$lang['client_client']              = 'Client';
$lang['client_add_page_title']      = 'Add client';
$lang['client_view_page_title']     = 'View clients';
$lang['client_new_client']          = 'New client';
$lang['client_edit_client']         = 'Edit client';
$lang['client_delete_client']       = 'Delete client';
$lang['client_details']             = 'Client details';
$lang['client_form_user_name']      = 'Client user name';
$lang['client_form_company']        = 'Company name';
$lang['client_form_industry']       = 'Industry or business type';
$lang['client_form_client_since']   = 'Client since';
$lang['client_form_address']        = 'Address';
$lang['client_form_city']           = 'City';
$lang['client_form_state']          = 'State or region';
$lang['client_form_zip']            = 'Zip/Postal code';
$lang['client_form_country']        = 'Country';
$lang['client_form_timezone']       = 'Time zone';
$lang['client_form_contact_person'] = 'Primary contact person';
$lang['client_form_phone1']         = 'Primary phone number';
$lang['client_form_phone2']         = 'Phone 2';
$lang['client_form_phone3']         = 'Phone 3';
$lang['client_form_email']          = 'Email address';
$lang['client_form_url']            = 'Website';
$lang['client_form_category']       = 'Client category';
$lang['client_form_im']             = 'Instant messenger';
$lang['client_form_im_type']        = 'Instant messenger type';
$lang['client_form_notes']          = 'Notes for this client';
$lang['client_form_tags']           = 'Tags';
$lang['client_client_contacts']     = 'Client contacts';
$lang['client_rate']                = 'Client rate';
$lang['client_search']              = "Start typing to search your clients";
$lang['client_send_login_email']    = 'Send welcome email';


/*
 * FINANCE AREA MESSAGES
 */
$lang['finance_page_title']              = 'Finances';
$lang['finance_view_page_title']         = 'Finance overview';
$lang['finance_view_invoices']           = 'View invoices';
$lang['finance_view_expenses']           = 'View expenses';
$lang['finance_reports']                 = 'Finance reports';
$lang['finance_total_invoiced']          = 'Total invoiced';
$lang['finance_invoices']                = 'Invoices';
$lang['finance_invoice']                 = 'Invoice';
$lang['finance_date_sent']               = 'Date sent';
$lang['finance_date_created']            = 'Date created';
$lang['finance_expense_new']             = 'New expense';
$lang['finance_expense_edit']            = 'Edit expense';
$lang['finance_invoice_new']             = 'New invoice';
$lang['finance_invoice_edit']            = 'Edit invoice';
$lang['finance_invoice_details']         = 'Other invoice details';
$lang['finance_invoice_number']          = 'Invoice number';
$lang['finance_invoice_ponumber']        = 'Purchase order number';
$lang['finance_invoice_category']        = 'Invoice category';
$lang['finance_invoice_date']            = 'Invoice date';
$lang['finance_last_invoice_number']     = 'Last invoice number:';
$lang['finance_invoice_title']           = 'Invoice title';
$lang['finance_invoice_message_form']    = 'Message or comments';
$lang['finance_invoice_message_print']   = 'Print message on invoice.';
$lang['finance_tax_information']         = 'Tax information';
$lang['finance_tax_rate']                = 'Tax rate';
$lang['finance_tax_name']                = 'Tax name';
$lang['finance_tax_id']                  = 'Tax ID';
$lang['finance_invoice_freight']         = 'Freight charge';
$lang['finance_invoice_payment_due']     = 'Payment due';
$lang['finance_invoice_late_fee']        = 'Late fee';
$lang['finance_invoice_discount']        = 'Discount';
$lang['finance_invoice_email']           = 'Email invoice';
$lang['finance_estimate_email']          = 'Email estimate';
$lang['finance_invoice_item_description']= 'Description';
$lang['finance_invoice_item_type']       = 'Type';
$lang['finance_invoice_item_qty']        = 'Qty';
$lang['finance_invoice_item_cost']       = 'Cost/Rate';
$lang['finance_invoice_item_tax']        = 'Tax';
$lang['finance_invoice_bill_to']         = 'Bill to';
$lang['finance_invoice_sales_tax']       = 'Sales tax';
$lang['finance_invoice_create_invoice']  = 'Create invoice';
$lang['finance_invoice_create_invoices'] = 'Create invoices';
$lang['finance_invoice_add_to_invoice']  = 'Add to invoice';
$lang['finance_view_this_invoice']       = 'View this invoice'; // **
$lang['finance_view_this_expense']       = 'View this expense item'; // **

$lang['finance_invoice_type_service']    = 'Service';
$lang['finance_invoice_type_product']    = 'Product';
$lang['finance_invoice_type_hours']      = 'Hours';
$lang['finance_invoice_type_days']       = 'Days';
$lang['finance_invoice_create_send']     = 'Create/Send'; // **
$lang['finance_invoice_create_send_own'] = 'Create/Send invoices to you only'; // **
$lang['finance_invoice_paypal_title']    = 'Accept PayPal payments';
$lang['finance_invoice_paypal_email']    = 'PayPal email address';

$lang['finance_expenses']                = 'Expenses';
$lang['finance_expense_notes']           = 'Notes for this expense'; // **
$lang['finance_expense_purchase_date']   = 'Purchase date'; // **
$lang['finance_expense_amount']          = 'Amount'; // **
$lang['finance_expense_vendor']          = 'Select a vendor'; // **
$lang['finance_add_vendor']              = 'New vendor'; // **
$lang['finance_no_vendor']               = 'You have no vendors listed.'; // **
$lang['finance_expense_payment_method']  = 'Payment method'; // **
$lang['finance_manage_vendors']          = 'Manage vendors'; // **
$lang['finance_expense_repeats']         = 'Expense repeats'; // **
$lang['finance_expense_invoice']         = 'Invoice this expense'; // **
$lang['finance_expense_invoice_new']     = 'Create new invoice'; // **

$lang['finance_expense_category_computer'] = 'Computer or Electronics'; // **
$lang['finance_expense_category_auto']     = 'Fuel or Auto'; // **
$lang['finance_expense_category_meals']    = 'Meals'; // **
$lang['finance_expense_category_misc']     = 'Miscellaneous'; // **
$lang['finance_expense_category_office']   = 'Office supplies'; // **
$lang['finance_expense_category_postage']  = 'Postage and shipping'; // **
$lang['finance_expense_category_rental']   = 'Rental car'; // **
$lang['finance_expense_category_software'] = 'Software'; // **
$lang['finance_expense_category_hosting']  = 'Website hosting'; // **

$lang['finance_expense_category']          = 'Expense category'; // **
$lang['finance_expense_name']              = 'Expense item name'; // **
$lang['finance_expense_markup']            = 'Markup'; // **
$lang['finance_expense_pay_method']        = 'Payment method'; // **
$lang['finance_expense_reimbursement']     = 'Reimbursement'; // **
$lang['finance_expense_reimbursement_yes'] = 'Reimbursable'; // **
$lang['finance_expense_reimbursement_no']  = 'Not reimbursable'; // **
$lang['finance_expense_reimbursement_personal'] = 'Personal'; // **

$lang['finance_expense_pay_method_cash']          = 'Cash'; // **
$lang['finance_expense_pay_method_check']         = 'Company check'; // **
$lang['finance_expense_pay_method_cc_company']    = 'Company credit card'; // **
$lang['finance_expense_pay_method_cc_personal']   = 'Personal credit card'; // **
$lang['finance_expense_pay_method_paypal']        = 'PayPal'; // **
$lang['finance_expense_pay_method_bank_transfer'] = 'Bank transfer'; // **
$lang['finance_expense_pay_method_money_order']   = 'Money order'; // **
$lang['finance_expense_search']                   = "Start typing to search your expenses";

$lang['finance_expense_pay_method_acct_no']     = 'Check no.'; // **
$lang['finance_invoice_email_address']          = 'Send to (email address)'; // **
$lang['finance_view_invoice_number']            = 'View invoice number'; // **
$lang['finance_invoices_created']               = 'Your invoices have been created'; // **
$lang['finance_enter_payment']                  = 'Enter payment'; // **
$lang['finance_enter_payment_amount']           = 'Enter payment amount'; // **
$lang['finance_payment_type']                   = 'Payment type'; // **
$lang['finance_payments']                       = 'Payments'; // **
$lang['finance_payment']                        = 'Payment'; // **
$lang['finance_date_paid']                      = 'Date paid'; // **
$lang['finance_current_payments']               = 'Current payments'; // **
$lang['finance_save_payment']                   = 'Save payment'; // **
$lang['finance_invoice_emailed']                = 'Your invoice has been sent.'; // **
$lang['finance_invoice_not_emailed']            = 'Error! Your invoice was not sent.'; // **
$lang['finance_estimate_emailed']               = 'Your estimate has been sent.'; // **
$lang['finance_estimate_not_emailed']           = 'Error! Your estimate was not sent.'; // **
$lang['finance_pay_with_paypal']                = 'Pay this invoice with PayPal'; // **
$lang['finance_make_payment']                   = 'Make PayPal payment'; // **
$lang['finance_invoice_for']                    = 'Invoice for'; // **
$lang['finance_pay_pal_payment_received']       = 'PayPal payment from client.';
$lang['finance_pay_pay_payment_canceled']       = 'Your PayPal transaction has been canceled.';
$lang['finance_pay_pay_payment_completed']      = 'Your PayPal transaction has been completed.  Thank you!';
$lang['finance_total_due']                      = 'Total due';
$lang['finance_select_expenses']                = 'Select expenses';
$lang['finance_select_timesheet_entries']       = 'Select timesheet entries';
$lang['finance_invoice_recurring']              = 'Recurring invoice settings';
$lang['finance_invoice_repeats']                = 'Invoice repeats...';
$lang['finance_invoice_stop_recurring']         = 'Stop recurring';
$lang['finance_vendor_name']                    = 'Vendor name';
$lang['finance_vendor_company']                 = 'Vendor company';
$lang['finance_view_ledger']                    = 'View ledger';
$lang['finance_ledger']                         = 'Ledger';
$lang['finance_invoices_estimates']             = 'Invoices &amp; Estimates';
$lang['finance_estimates']                      = 'Estimates';
$lang['finance_estimate']                       = 'Estimate';
$lang['finance_estimate_title']                 = 'Estimate title';
$lang['finance_estimate_number']                = 'Estimate number';
$lang['finance_estimate_date']                  = 'Estimate date';
$lang['finance_estimate_details']               = 'Other estimate details';
$lang['finance_estimate_category']              = 'Estimate category';
$lang['finance_estimate_message_print']         = 'Print message on estimate.';
$lang['finance_expense']                        = 'Expense';
$lang['finance_show_paypal_link']               = 'Show PayPal link on your invoice';
$lang['finance_payments_due']                   = 'Payments due';
$lang['finance_convert_to_invoice']             = 'Convert to invoice';

/*
 * PROJECT AREA MESSAGES
 */
$lang['project_page_title']         = 'Projects';
$lang['project_add_page_title']     = 'New project';
$lang['project_edit_page_title']    = 'Edit project';
$lang['project_total']              = 'Total projects';
$lang['project_completion_date']    = 'Completion date';
$lang['project_project_contacts']   = 'Team members and project contacts';
$lang['project_people_on_project']  = 'People on this project';
$lang['project_details']            = 'Project details';
$lang['project_tasks']              = 'Tasks';
$lang['project_dates']              = 'Project dates (start, finish)';
$lang['project_start']              = 'Project start'; // **
$lang['project_complete']           = 'Project complete'; // **
$lang['project_form_select_client'] = 'Select client';
$lang['project_form_title']         = 'Project title';
$lang['project_select_project']     = 'Select project';
$lang['project_form_notes']         = 'Notes for this project';
$lang['project_form_status']        = 'Project status';
$lang['project_form_category']      = 'Project category';
$lang['project_form_url']           = 'Project website URL';
$lang['project_form_team_members']  = 'Select team members for this project';
$lang['project_form_budget']        = 'Project budget';
$lang['project_view_this_project']  = 'View this project'; // **
$lang['project_add_project_only']   = 'Add project only'; // **
$lang['project_view_assigned']      = 'View assigned only'; // **
$lang['project_rate']               = 'Project rate'; // **
$lang['project_project']            = 'Project'; // **
$lang['project_view_archived']      = 'View archived projects';
$lang['project_view_open']          = 'View open projects';

$lang['project_template_name']      = 'Template name';
$lang['project_template_desc']      = 'Template description';
$lang['project_template_create']    = 'Create new template';
$lang['project_template_items']     = 'Which items do you want to store in the template?';
$lang['project_template_saved']     = 'Template is saved.';
$lang['project_template']           = 'Project template';

/*
 * CONTACTS AREA
 */
$lang['contact_no_contacts']    = "You have no contacts yet.";
$lang['contact_create']         = "Create a new contact.";
$lang['contact_new']            = "Add new contact";
$lang['contact_error_no_email'] = "You must enter at least 1 email address for a team member.";
$lang['contact_error_no_role']  = "You must select a role for a team member.";
$lang['contact_search']         = "Start typing to search your contacts";
$lang['contact_search_projects']= "Start typing to search your team members or contacts";

/*
 * TASKS AREA
 */
$lang['task_assigned_to']          = 'Assign task to';
$lang['task_description']          = 'Description';
$lang['task_dates']                = 'Dates (start : end)';
$lang['task_hours']                = 'Estimated hours';
$lang['task_actual_hours']         = 'Actual hours';
$lang['task_estimated_time']       = 'Estimated hours';
$lang['task_title']                = 'Name or title';
$lang['task_priority_low']         = 'Low';
$lang['task_priority_medium']      = 'Medium';
$lang['task_priority_high']        = 'High';
$lang['task_priority_extreme']     = 'Extreme';
$lang['task_priority']             = 'Task priority';
$lang['task_task']                 = 'Task';
$lang['task_comments']             = 'Task &amp; Comments';
$lang['task_tasks']                = 'Tasks';
$lang['task_incomplete']           = 'Incomplete tasks';
$lang['task_new_milestone']        = 'New milestone';
$lang['task_task_saved']           = 'Task has been saved.';
$lang['task_milestone_saved']      = 'Milestone has been saved.';
$lang['task_select_task']          = 'Select task';
$lang['task_new_task']             = 'New task'; // **
$lang['task_view_my_tasks']        = 'See only my tasks.'; // **
$lang['task_view_complete_tasks']  = 'See only completed tasks.'; // **
$lang['task_view_incomplete_tasks']= 'See only incomplete tasks.'; // **
$lang['task_complete']             = 'Completed tasks'; // **
$lang['task_hours_complete']       = 'Completed hours'; // **
$lang['task_overdue']              = 'Overdue tasks'; // **
$lang['task_total']                = 'Total tasks'; // **
$lang['task_view_task']            = 'View this task'; // **
$lang['task_message']              = 'Task message'; // **
$lang['task_milestone']            = 'Milestone'; // **
$lang['task_delete_milestone']     = 'Delete milestone'; // **
$lang['task_select_milestone']     = 'Select milestone'; // **
$lang['task_number']               = 'Task number'; // **
$lang['task_miscellaneous_tasks']  = 'Miscellaneous tasks'; // **

/*
 * CALENDAR AREA MESSAGES
 */
$lang['calendar']                  = 'Calendar';
$lang['calendars']                 = 'Calendars'; // **
$lang['calendar_events']           = 'Events'; // **
$lang['calendar_new']              = 'New event';
$lang['calendar_reminder']         = 'Send me an email 1 hour before event begins.'; // **
$lang['calendar_all_day']          = 'All day'; // **
$lang['calendar_repeats']          = 'Event repeats'; // **
$lang['calendar_repeats_none']     = 'Not repeating'; // **
$lang['calendar_repeats_day']      = 'Every day'; // **
$lang['calendar_repeats_week']     = 'Every week'; // **
$lang['calendar_repeats_month']    = 'Every month'; // **
$lang['calendar_repeats_year']     = 'Every year'; // **
$lang['calendar_office']           = 'Office calendar'; // **
$lang['calendar_your_calendars_short'] = 'Your calendars'; // **
$lang['calendar_more_details']     = 'Add more details'; // **
$lang['calendar_your_calendars']   = 'Your CreativePro Office calendars'; // **
$lang['calendar_new_calendar']     = 'New calendar'; // **
$lang['calendar_agenda']           = 'Agenda'; // **
$lang['calendar_name']             = 'Calendar name'; // **
$lang['calendar_select_color']     = 'Select a color'; // **
$lang['calendar_no_events_today']  = 'No events for today.'; // **
$lang['calendar_delete']           = 'Delete calendar'; // **
$lang['calendar_post_info']        = 'Posted by %s in %s'; // **
$lang['calendar_view_event']       = 'View event'; // **
$lang['calendar_month_view']       = 'Month view';
$lang['calendar_week_view']        = 'Week view';
$lang['calendar_daily_view']       = 'Daily agenda view';
$lang['calendar_today_view']       = 'View today';

/*
 * FILES AREA MESSAGES
 */
$lang['files_files']               = 'Files';
$lang['files_file_manager']        = 'File manager';
$lang['files_upload_files']        = 'Upload files';
$lang['files_folder']              = 'Folder';
$lang['files_new_folder']          = 'New folder';
$lang['files_new_folder_name']     = 'New folder name';
$lang['files_upload']              = 'Upload files';
$lang['files_upload_logo_small']   = 'Upload small logo'; // **
$lang['files_upload_logo_hires']   = 'Upload hi-res logo'; // **
$lang['files_upload_photo']        = 'Upload photo';
$lang['files_select']              = 'Select files';
$lang['files_attach']              = 'Attach files';
$lang['files_view_this_file']      = 'View this file'; // **
$lang['files_avatar_size']         = '100 x 100 pixels max.'; // **
$lang['file_import_folder']        = 'Imported Files'; // **
$lang['files_all_files']           = 'All files';
$lang['files_delete_folder']       = 'Delete folder';
$lang['files_delete_file']         = 'Delete file';
$lang['files_file_share']          = 'Click on a file below to share it with your client.';

/*
 * TEAM AREA MESSAGES
 */
$lang['team_my_team']              = 'My Team';
$lang['team_select_team_member']   = 'My Team';
$lang['team_select_team_member']   = 'Select team member';
$lang['team_member']               = 'Team member';
$lang['team_members']              = 'Team members'; // **
$lang['team_set_roles']            = 'Roles and permissions';    // **
$lang['team_role_create']          = 'Create new role'; // **
$lang['team_role_edit']            = 'Update existing role'; // **
$lang['team_role_edit_this']       = 'Update this role'; // **
$lang['team_role_delete_this']     = 'Delete role'; // **
$lang['team_role_saved']           = 'Role and permissions were saved.'; // **
$lang['team_new_team_member']      = 'New team member'; // **
$lang['team_select_role']          = 'Select role';    // **
$lang['team_make_team_member']     = 'This person is a team member.'; // **
$lang['team_role_people_form']     = 'If this person is a team member'; // **
$lang['team_select_team_members']  = 'Select team members'; // **

/*
 * ROLE TEXT
 */
$lang['role_loggedinas']       = 'Logged in as ';
$lang['role_administrator']    = 'Administrator';
$lang['role_employee']         = 'Employee';
$lang['role_manager']          = 'Manager';
$lang['role_contractor']       = 'Contractor';

/*
 * TIMESHEET AREA MESSAGES
 */
$lang['timesheets']                     = 'Timesheets';
$lang['timesheets_reports']             = 'Timesheet reports';
$lang['timesheets_report']              = 'Timesheet report';
$lang['timesheets_project_task']        = 'Project and task';
$lang['timesheets_select_project']      = 'Please select a project.';
$lang['timesheets_select_project_only'] = 'Select project only';
$lang['timesheets_view_your_timesheet'] = 'View your timesheet';
$lang['timesheets_elapsed_time']        = 'Elapsed time';

$lang['timer_start_timer']              = 'Start timer';
$lang['timer_stop_timer']               = 'Stop timer';
$lang['timer_time_saved']               = 'Your time has been saved.';
$lang['timesheets_last_week']           = 'Last week'; // **
$lang['timesheets_next_week']           = 'Next week'; // **
$lang['timesheets_select_date']         = 'Select timesheet date'; // **
$lang['timesheets_entries']             = 'Timesheet entries';
$lang['timesheet_task_comments']        = 'Tasks / Comments';
$lang['timesheets_no_invoice']          = 'You did not select any timesheet entries to invoice.';
$lang['timesheets_total_hours']         = 'Total hours';
$lang['timesheets_total_weekly']        = 'Weekly total';

/*
 * ADMINISTRATION AREA MESSAGES
 */
$lang['admin_page_title'] = 'Administration';

/*
 * SETTINGS AREA TERMS
 */
$lang['settings_page_title']         = 'Office Settings';
$lang['settings_logos']              = 'Company logos';
$lang['settings_email_project']      = 'I am assigned to a project.';
$lang['settings_email_project_updated'] = 'My projects are updated.';
$lang['settings_email_message']      = 'Someone sends me a message.';
$lang['settings_email_event']        = 'An event is shared with me.';
$lang['settings_email_task']         = 'I am assigned to a task.';
$lang['settings_email_task_update']  = 'My tasks are updated.';
$lang['settings_email_task_overdue'] = 'My tasks are overdue or coming due.';
$lang['settings_email_calendar']     = 'A calendar is shared with me.';
$lang['settings_email_timer']        = 'I leave a job timer clock running.';
$lang['settings_email_notification'] = 'Receive email notifications when...';
$lang['settings_email_send_notification'] = 'Send email notifications when...';
$lang['settings_email_client_create']     = 'A new client is created (send client area access details)';
$lang['settings_change_password']         = 'Change your password';
$lang['settings_web_address']             = 'Your account web address';

$lang['settings_account_security']            = 'Account Security';
$lang['settings_upload_logo']                 = 'Upload your company logo';
$lang['settings_update_company_information']  = 'Update your company information';
$lang['settings_update_invoice']              = 'Update your invoice settings';
$lang['settings_update_calendar']             = 'Update your calendar settings';
$lang['settings_account_login_on_item_view']  = 'Automatically log me into my account when I view an item from my account RSS feed (like an event or task RSS item).';
$lang['settings_account_login_from_email']    = 'Automatically log me into my account when I click a link in an email message sent from my account (like task reminders).';
$lang['settings_update_all']                  = 'Update all office settings';
$lang['settings_update_own']                  = 'Update personal settings (email settings, password, avatar, etc.)';
$lang['settings_success']                     = 'Your settings have been saved.';
$lang['settings_cancel_account']              = 'Cancel my account';
$lang['settings_account_view_payments']       = 'View account payments';
$lang['settings_account_payments']            = 'Payments made for your account.';

$lang['settings_dbase_host']                  = 'Database host name';
$lang['settings_dbase_name']                  = 'Database name';
$lang['settings_dbase_username']              = 'Database user name';
$lang['settings_dbase_password']              = 'Database password';
$lang['settings_file_path']                   = 'Version 1.0 user_files file path';
$lang['settings_no_file_import']              = 'Do not import files';
$lang['settings_no_payments']                 = 'You have no payments yet.';

/*
 * NOTES
 */
$lang['notes_new_note']    = 'New note';
$lang['notes_make_public'] = 'Make this note public.';
$lang['notes_show_client'] = 'Make visible to client.';
$lang['notes_visible_to_client'] = 'This note can be seen by the client.';
$lang['notes_project_notes']     = 'Project notes';

/*
 * BUG REPORT AREA TERMS
 */
$lang['bugreport_page_title']   = 'Report Bugs';
$lang['bugreport_instructions'] = 'Please describe the bug in the space below.';
$lang['bugreport_submit']       = 'Submit bug';
$lang['bugreport_success']      = 'Bug has been submitted.';

/*
 * Plan information
 */
$lang['plan_cost']              = 'Cost per month';
$lang['plan_active_projects']   = 'Active projects';
$lang['plan_team']              = 'Team members';
$lang['plan_storage']           = 'Storage space';
$lang['plan_update_plan']       = 'Change your plan';
$lang['plan_update_cc']         = 'Update credit card';
$lang['plan_update_plan_or_cc'] = 'Change your plan or update credit card';
$lang['plan_update_plan_cc']    = 'Change plan or credit card';

/*
 * Data import/export
 */
$lang['import_page_title']       = 'Import your data';
$lang['import_import_clients']   = 'Import clients';
$lang['import_import_contacts']  = 'Import contacts';
$lang['import_upload_data_file'] = 'Upload data file';
$lang['import_first_row_headers']= 'First row of data is column headers.';
$lang['import_send_client_email']= 'Send an email message to each client with their account access information.';
$lang['import_accept_import']    = 'Accept import';
$lang['import_cancel_import']    = 'Do not accept import!';
$lang['import_import_data']      = 'Import data';
$lang['import_success_message']  = 'Your data was imported successfully.';
$lang['import_rollBack_message'] = 'Your imported data has been deleted.';
$lang['import_map_fields']       = 'Map your data fields';
$lang['import_confirm_data']     = 'Confirm your imported data';
$lang['import_client_data_column'] = 'Client data column';
$lang['import_contact_data_column'] = 'Contact data column';
$lang['import_your_data_column']    = 'Your data column';
$lang['import_contacts_are']        = 'These contacts are:';
$lang['import_from_cpo']            = 'Import data from a CreativePro Office version 1 account';
$lang['import_from_other']          = 'Import data from other software';
$lang['import_begin_import']        = 'Begin importing data';
$lang['export_cpo_data']            = 'Export your data from CreativePro Office';
$lang['export_cpo_data_confirm']    = 'Your export has started. Look for an email soon with your download link.';

$lang['feed_rss_text'] = 'Copy and paste the following link into your favorite RSS feed reader. Click the link to download the feed.';
$lang['feed_ical_text'] = 'Copy and paste the following link into Google Calendar or other ical applications. Click the link to download the feed.';
?>