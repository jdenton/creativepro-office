<?php
/*
 * ENGLISH TRANSLATION
 */
$lang['help_formfield_clientUserid'] = "Your client will need a userid in order to access their 
										CreativePro Office account. You must let your client 
										know which userid you have selected.<br /><b>CreativePro 
										Office will not	email your client the userid.</b>";

$lang['help_formfield_tags']         = "Tags are a way to categorize items like projects, clients or tasks in a way that makes sense to you. Tags are also searchable.";
$lang['help_formfield_client_search']= "Select a client by typing part of the client name in the textbox. A list of potential client matches will appear.";
$lang['help_formfield_client_send_email'] = "If you check this box, your client will receive a welcome email containing their account access user name and password.";
$lang['help_tag_format']             = "Separate tags with spaces.";
$lang['help_revise_search']          = "Try revising your search criteria and search again.";
$lang['help_select_date_range']      = 'Select a date range.';
$lang['help_search_expenses']        = 'Search for expenses.';

$lang['help_team_contractor_invoice']  = "Contractors will be able to send invoices to you only.";
$lang['help_team_add_team_member']     = "When you create a Team Member, they will receive an email message with instructions for accessing their accounts. They will login with their email address and a temporary password.";
$lang['help_team_contractor_timesheet']= "Contractors will be able to view and update their own timesheets only.";
$lang['help_formField_avatar']         = "You can upload a new photo by clicking the Upload Photo button. To use an existing photo from somewhere else on the web like your Facebook account or Gravatar.com, enter the URL in the Photo URL box. Photos from the web should be 100 x 100 pixels in size max.";
$lang['help_add_guests_calendar']      = "Select clients or contacts to view this event. Clients will see the event in their client area calendar. Contacts will be emailed a link that they can use to view the event.";
$lang['help_task_title']               = "Hit ENTER to save.";

$lang['help_delete_calendar']          = "Move events to another calendar?";
$lang['help_delete_events_too']        = "Delete events also.";

$lang['help_move_tasks_to_milestone']  = 'Move tasks to another milestone?'; // **
$lang['help_delete_tasks_too']         = "Delete tasks also.";

$lang['help_move_contents_to_folder']  = 'Move folder contents to another folder?'; // **
$lang['help_delete_contents_too']      = "Delete folder contents also.";
$lang['help_delete_file_no_undo']      = 'There is no undo when you delete files or folders!';
$lang['help_drag_files_here']          = 'Drag files here to upload them.';

$lang['help_rates']                    = "You can set rates for individual projects and clients. These rates will be used to calculate project invoices. You can always override this rate when creating an invoice if necessary.";

$lang['help_account_security_settings'] = "Selecting any of these items will <strong>reduce the security</strong> of your CreativePro Office account by allowing your account to be accessed without specifying a user name and password!";
$lang['help_account_cancel']            = 'You cannot undo this action. If you cancel your account, you will no longer have access to your data.';
$lang['help_account_cancel_message']    = 'We are sorry to see you go. If you have a moment, please send an email to <a href="mailto:'.MAIL_SUPPORT.'">'.MAIL_SUPPORT.'</a> and let us know why CreativePro Office didn\'t work out for you.<br /><br/><strong>Thanks for trying CreativePro Office!</strong>';
$lang['help_account_import_v10']        = "Import your data from a CreativePro Office version 1.0 account here. Enter the user name and password to your version 1.0 account and your data will be imported.<br />If your plan supports file uploads, your files will be moved also.";

$lang['help_account_import_v10_self']   = "Import your data from a CreativePro Office self-hosted version 1.0 account here.<br />Enter the database connection information and the user_files path for your self-hosted installation below.<br /><br /><span class='warningMessageSmall'>To use this import feature, your version 1 installation MUST be on the same server as your version 2 installation. If version 1 is on another server, you should use the Hosted Account import feature above.</span>";
$lang['help_import_file_path']          = "The file import path is the complate, absolute path to the user_files directory of your CPO version 1.0 installation. This may be something like '/var/www/cpo/apps/user_files'.";
$lang['help_import_database_fields']    = "The database information is required to pull data from your CPO version 1.0 installation.";

$lang['help_paypal_payments']           = "Add a PayPal link to your invoices by entering your PayPal email address below. If you don't have a PayPal account, <a href=\"http://www.paypal.com\">get one for free</a> and get paid faster. (Note: PayPal only accepts payments in US Dollars, Canadian Dollars, Euros, and British Pounds.)";

$lang['help_logo_lo_res']               = "<strong>Low resolution logo.</strong> Upload the logo that will appear on screen only in the client area and on emailed invoices.";
$lang['help_logo_lo_res_instructions']  = "<li>For best results, logo should be no larger than 100 x 400 pixels.</li><li>PNG, GIF, and JPEG files are allowed.</li><li>Maximum file size is ".byte_format(MAX_LOGO_UPLOAD_SIZE)."</li>";

$lang['help_logo_hi_res']               = "<strong>High resolution logo.</strong> Upload the logo that will appear on printed and PDF invoices.";
$lang['help_logo_hi_res_instructions']  = "<li>For best results, logo should be 200 dpi or better.</li><li>PNG, GIF, and JPEG files are allowed.</li><li>Maximum file size is ".byte_format(MAX_LOGO_UPLOAD_SIZE)."</li>";

$lang['help_project_deleted']           = 'Your project has been deleted.';
$lang['help_project_no_projects']       = 'You have no projects yet. <strong><a href=\"'.site_url('projects/ProjectUpdate').'\">Create a new project now.</a></strong>';
$lang['help_project_no_projects_client']= 'You have no projects yet.';
$lang['help_project_team_contacts']     = 'Assign team members to this project by typing their name in the search box below. Team members will receive an email notifying them of their assignment.<br /><br />You can also add contacts to this project in the same way.';
$lang['help_client_team_contacts']      = 'Assign team members or contacts to this client by typing their name in the search box below.<br /><br />Create a new contact by clicking the Add new Contact button.';

$lang['help_tasks_no_tasks_widget']     = 'You don\'t have any tasks due right now.';

$lang['help_invoice_no_invoices']       = 'You have no invoices yet. <strong><a href='.site_url('finances/InvoiceUpdate').'>Create a new invoice now.</a></strong>';
$lang['help_invoice_no_invoices_client']= 'You have no invoices yet.';

$lang['help_client_deleted']            = 'Your client has been deleted.';
$lang['help_client_no_clients']         = 'You have no clients yet. <strong><a href='.site_url('clients/ClientUpdate').'>Create a new client now.</a></strong>';
$lang['help_client_new_email']          = 'This client will receive a welcome email containing their account user name and password. <a href="'.site_url('settings/SettingsOwners#windowSettingsEmail').'">Click here to change this.</a>';

$lang['help_max_projects']              = 'Want to add more projects? That\'s fine but you\'ll need to upgrade your account or archive some projects first.<br /><br />Your account currently supports up to <strong>%s projects</strong>.';
$lang['help_max_team_members']          = 'Want to add more team members? That\'s fine but you\'ll need to upgrade your account first. Your account supports up to <strong>%s team members</strong>.';
$lang['help_max_files']                 = 'Want to add more files? That\'s fine but you\'ll need to upgrade your account first. Your account supports up to <strong>%s of file storage</strong>.';
$lang['help_no_files']                  = 'Your account does not support file uploads.';
$lang['help_get_more_storage']          = 'Get up to 10GB of storage by upgrading your account.';
$lang['help_upgrade_account_link']      = 'Upgrade your account here.';

$lang['help_no_search_results']         = 'Your search for <u>%s</u> did not find anything.  Go ahead and try another search!';

$lang['help_import_cancel_import']      = 'This will delete the data you just imported.';
$lang['help_import_step1']              = '<strong>Step 1: Select which type of data you want to import and upload your data file.</strong> Currently, we only support the CSV file format. We will soon support other formats like Excel and Vcard.';
$lang['help_import_step2']              = '<strong>Step 2: Map the data in your file to the CreativePro Office data fields.</strong>';
$lang['help_import_step3']              = '<strong>Step 3: Confirm that your data was correctly imported.</strong> This is just a sampling of the imported data. <strong>%s</strong> total records were imported.';
$lang['help_import_overview']           = 'Import your data from other project management, invoicing, contact management, or accounting software. We currently provide import options for Clients and Contacts in CSV format.';
$lang['help_cpo_export_overview']       = 'Clicking the Export button below will export all of your account data in Excel format. Each data type (projects, clients, invoices, etc.) will be in its own Excel worksheet.<br /><br />Since the export process can take several minutes, you will receive an email with your download link once your download file is ready.';
?>