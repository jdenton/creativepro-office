<?
$lang['db_invalid_connection_str'] = 'Nicht in der Lage, um die Datenbank-Einstellungen auf der Grundlage der von Ihnen eingereichten Verbindungszeichenfolge.';
$lang['db_unable_to_connect'] = 'Nicht in der Lage, eine Verbindung zu Ihrer Datenbank-Server mit der mitgelieferten Einstellungen.';
$lang['db_unable_to_select'] = 'Nicht in der Lage, um die angegebenen Datenbank:% s';
$lang['db_unable_to_create'] = 'Nicht in der Lage, um die angegebenen Datenbank:% s';
$lang['db_invalid_query'] = 'Die Abfrage, die Sie nicht gültig ist.';
$lang['db_must_set_table'] = 'Sie müssen die Datenbank-Tabelle für die Verwendung mit Ihrer Anfrage.';
$lang['db_must_set_database'] = 'Sie müssen die Datenbank-Namen in Ihrer Datenbank-Konfigurationsdatei.';
$lang['db_must_use_set'] = 'Sie müssen die &quot;set&quot;-Methode einen Eintrag zu aktualisieren.';
$lang['db_must_use_where'] = 'Updates sind nicht zulässig, es sei denn, sie enthalten eine Klausel &quot;Where&quot;.';
$lang['db_del_must_use_where'] = 'Löscht sind nicht zulässig, es sei denn, sie enthalten einen &quot;wo&quot; oder &quot;wie&quot;-Klausel.';
$lang['db_field_param_missing'] = 'Zu holen Bereichen erfordert den Namen der Tabelle als Parameter.';
$lang['db_unsupported_function'] = 'Diese Funktion ist nicht verfügbar für die Datenbank, die Sie verwenden.';
$lang['db_transaction_failure'] = 'Transaktion fehlgeschlagen: Rollback durchgeführt.';
$lang['db_unable_to_drop'] = 'Nicht in der Lage, um die angegebene Datenbank.';
$lang['db_unsuported_feature'] = 'Nicht mit der Datenbank-Plattform Sie verwenden.';
$lang['db_unsuported_compression'] = 'Die Datei-Komprimierung von Ihnen gewählte Format wird nicht unterstützt von Ihrem Server.';
$lang['db_filepath_error'] = 'Nicht in der Lage, Daten in die Datei Pfad haben.';
$lang['db_invalid_cache_path'] = 'Der Cache-Pfad, die nicht gültig ist oder schreibbar.';
$lang['db_table_name_required'] = 'Eine Tabelle ist erforderlich, dass für den Betrieb.';
$lang['db_column_name_required'] = 'Ein Spaltenname ist, die für diese Operation.';
$lang['db_column_definition_required'] = 'Eine Spalte Definition ist erforderlich, dass für den Betrieb.';
$lang['db_unable_to_set_charset'] = 'Nicht in der Lage, um Client-Verbindungen Zeichensatz:% s';
$lang['db_error_heading'] = 'Eine Datenbank-Fehler ist aufgetreten';
?>