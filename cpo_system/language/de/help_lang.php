<?
$lang['help_formfield_clientUserid'] = 'Ihre Kunden benötigen einen Benutzer-ID, um den Zugang zu ihren CreativePro Office-Konto. Sie müssen Sie Ihre Kunden wissen, welche Benutzer-ID die Sie ausgewählt haben. <br /> <b>CreativePro Amt nicht Ihre E-Mail-Client der Benutzer-ID.</b>';
$lang['help_formfield_tags'] = 'Tags sind eine Möglichkeit, Artikel zu kategorisieren, wie Projekte, Kunden oder Aufgaben in einer Weise, die Sinn macht für Sie. Tags sind auch suchbar.';
$lang['help_tag_format'] = 'Tags mit Leerzeichen trennen.';
$lang['help_revise_search'] = 'Versuchen Sie Ihre Suche Überarbeitung der Kriterien und erneut suchen.';
?>