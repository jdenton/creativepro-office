<?
$lang['email_must_be_array'] = 'Die E-Mail-Validierung Methode werden ein Array.';
$lang['email_invalid_address'] = 'Ungültige E-Mail-Adresse: %s';
$lang['email_attachment_missing'] = 'Nicht in der Lage, um die folgenden E-Mail-Anhang: %s';
$lang['email_attachment_unreadable'] = 'Nicht in der Lage, diesen Anhang öffnen: %s';
$lang['email_no_recipients'] = 'Sie müssen Empfänger: An, Cc oder Bcc';
$lang['email_send_failure_phpmail'] = 'Nicht in der Lage, eine E-Mail mit PHP mail (). Ihr Server möglicherweise nicht so konfiguriert werden, dass E-Mails mit dieser Methode.';
$lang['email_send_failure_sendmail'] = 'Nicht in der Lage, eine E-Mail mit PHP Sendmail. Ihr Server möglicherweise nicht so konfiguriert werden, dass E-Mails mit dieser Methode.';
$lang['email_send_failure_smtp'] = 'Nicht in der Lage, eine E-Mail mit PHP SMTP. Ihr Server möglicherweise nicht so konfiguriert werden, dass E-Mails mit dieser Methode.';
$lang['email_sent'] = 'Ihre Nachricht wurde erfolgreich versendet mit dem folgenden Protokoll: %s';
$lang['email_no_socket'] = 'Nicht in der Lage, um eine Steckdose zu Sendmail. Bitte überprüfen Sie Einstellungen.';
$lang['email_no_hostname'] = 'Haben Sie nicht einen SMTP-Host.';
$lang['email_smtp_error'] = 'Die folgenden SMTP-Fehler aufgetreten: %s';
$lang['email_no_smtp_unpw'] = 'Fehler: Sie müssen einen SMTP-Benutzernamen und ein Passwort.';
$lang['email_failed_smtp_login'] = 'Es versäumt, AUTH LOGIN Kommando. Fehler: %s';
$lang['email_smtp_auth_un'] = 'Ist an der Authentifizierung gescheitert Benutzernamen. Fehler: %s';
$lang['email_smtp_auth_pw'] = 'Fehler beim Passwort authentifizieren. Fehler: %s';
$lang['email_smtp_data_failure'] = 'Nicht in der Lage, Daten zu senden: %s';

/*
 * Canned system messages.
 */
$lang['email_shared_event'] = "Jemand will ein Ereignis mit Ihnen teilen.";
?>