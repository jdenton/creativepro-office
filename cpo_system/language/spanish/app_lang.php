<?php
/*
 * SPANISH TRANSLATION
 */

/*
 * ERROR MESSAGES
 */

/*
 * MAIN, SUB MENU ITEMS
 */
$lang['menu_clients']      = 'Clientes';
$lang['menu_view_clients'] = 'Ver todos los clientes';
$lang['menu_new_client']   = 'Nuevo cliente'; 

/*
 * LOGIN SCREEN MESSAGES
 */
$lang['login_page_header']     = 'Office Login';
$lang['login_user_name']       = 'User Name';
$lang['login_password']        = 'Password';
$lang['login_button']          = 'Login';
$lang['login_forget_password'] = 'I forgot my password.'; 


?>