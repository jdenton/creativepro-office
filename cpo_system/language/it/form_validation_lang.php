<?
$lang['required'] = 'Il campo% s è obbligatorio.';
$lang['isset'] = 'Il campo% s deve avere un valore.';
$lang['valid_email'] = 'Il campo% s deve contenere un indirizzo email valido.';
$lang['valid_emails'] = 'Il campo% s deve contenere tutti gli indirizzi e-mail validi.';
$lang['valid_url'] = 'Il campo% s deve contenere un URL valido.';
$lang['valid_ip'] = 'Il campo% s deve contenere un IP valido.';
$lang['min_length'] = 'Il campo% s deve essere di almeno% s caratteri di lunghezza.';
$lang['max_length'] = 'Il campo% s non può superare% s caratteri di lunghezza.';
$lang['exact_length'] = 'Il campo% s deve essere esattamente% s caratteri di lunghezza.';
$lang['alpha'] = 'Il campo% s può contenere solo caratteri alfabetici.';
$lang['alpha_numeric'] = 'Il campo% s può contenere solo caratteri alfanumerici.';
$lang['alpha_dash'] = 'Il campo% s può contenere solo caratteri alfanumerici, trattini di sottolineatura e trattini.';
$lang['numeric'] = 'Il campo% s deve contenere solo numeri.';
$lang['is_numeric'] = 'Il campo% s deve contenere solo caratteri numerici.';
$lang['integer'] = 'Il campo% s deve contenere un numero intero.';
$lang['matches'] = 'Il campo% s non corrisponde al campo% s.';
$lang['is_natural'] = 'Il campo% s deve contenere solo numeri positivi.';
$lang['is_natural_no_zero'] = 'Il campo% s deve contenere un numero maggiore di zero.';
$lang['form_required_clientUserid'] = 'Userid Cliente non può essere vuoto.';
$lang['form_istaken_clientUserid'] = 'Cliente userid% s è alreay adottare. Si prega di selezionare un altro.';
$lang['form_required_clientCompany'] = 'Cliente non può essere vuoto.';
$lang['form_required_client'] = 'Cliente non può essere vuoto.';
$lang['form_required_project_title'] = 'Titolo del progetto non può essere vuoto.';
$lang['form_required_email'] = 'Si prega di inserire un indirizzo email valido.';
$lang['form_minlength_clientEmail'] = 'Indirizzo e-mail Cliente deve essere di 10 caratteri.';
?>