<?
$lang['email_must_be_array'] = 'L&#39;email di convalida metodo deve essere passato un array.';
$lang['email_invalid_address'] = 'Indirizzo email non valido:% s';
$lang['email_attachment_missing'] = 'Impossibile individuare la seguente allegato e-mail:% s';
$lang['email_attachment_unreadable'] = 'Impossibile aprire questo allegato:% s';
$lang['email_no_recipients'] = 'È necessario includere i destinatari: A, Cc o Ccn';
$lang['email_send_failure_phpmail'] = 'Impossibile inviare via e-mail utilizzando PHP mail (). Il server potrebbe non essere configurato per inviare una mail usando questo metodo.';
$lang['email_send_failure_sendmail'] = 'Impossibile inviare via e-mail utilizzando PHP Sendmail. Il server potrebbe non essere configurato per inviare una mail usando questo metodo.';
$lang['email_send_failure_smtp'] = 'Impossibile inviare via e-mail utilizzando PHP SMTP. Il server potrebbe non essere configurato per inviare una mail usando questo metodo.';
$lang['email_sent'] = 'Il tuo messaggio è stato inviato con successo utilizzando il seguente protocollo:% s';
$lang['email_no_socket'] = 'Impossibile aprire un socket di Sendmail. Si prega di controllare le impostazioni.';
$lang['email_no_hostname'] = 'Se non specificare un nome host SMTP.';
$lang['email_smtp_error'] = 'Il seguente errore è stato riscontrato SMTP:% s';
$lang['email_no_smtp_unpw'] = 'Errore: è necessario assegnare un nome utente e la password SMTP.';
$lang['email_failed_smtp_login'] = 'Impossibile inviare il comando AUTH LOGIN. Errore:% s';
$lang['email_smtp_auth_un'] = 'Impossibile autenticare il nome utente. Errore:% s';
$lang['email_smtp_auth_pw'] = 'Impossibile autenticare la password. Errore:% s';
$lang['email_smtp_data_failure'] = 'Impossibile inviare i dati:% s';
?>