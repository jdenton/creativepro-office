<?
$lang['date_year'] = 'Anno';
$lang['date_years'] = 'Anni';
$lang['date_month'] = 'Mese';
$lang['date_months'] = 'Mesi';
$lang['date_week'] = 'Settimana';
$lang['date_weeks'] = 'Settimane';
$lang['date_day'] = 'Giorno';
$lang['date_days'] = 'Giorni';
$lang['date_hour'] = 'Ore';
$lang['date_hours'] = 'Orario';
$lang['date_minute'] = 'Minute';
$lang['date_minutes'] = 'Processi verbali';
$lang['date_second'] = 'Seconda';
$lang['date_seconds'] = 'Secondi';
$lang['UM12'] = '(UTC -12:00) Baker / Isola Howland';
$lang['UM11'] = '(UTC -11:00) Samoa fuso orario, Niue';
$lang['UM10'] = '(UTC -10:00) Hawaii-Aleutian Standard Time, Isole Cook, Tahiti';
$lang['UM95'] = '(UTC -9:30) Isole Marchesi';
$lang['UM9'] = '(UTC -9:00) Alaska Standard Time, Isole Gambier';
$lang['UM8'] = '(UTC -8:00) Pacific Standard Time, Clipperton Island';
$lang['UM7'] = '(UTC -7:00) Mountain Standard Time';
$lang['UM6'] = '(UTC -6:00) Central Standard Time';
$lang['UM5'] = '(UTC -5:00) Eastern Standard Time, Caraibi occidentali Standard Time';
$lang['UM45'] = '(UTC -4:30) venezuelana Standard Time';
$lang['UM4'] = '(UTC -4:00) Atlantic Standard Time, Eastern Standard Time Caraibi';
$lang['UM35'] = '(UTC -3:30) Newfoundland Standard Time';
$lang['UM3'] = '(UTC -3:00) Argentina, Brasile, Guiana francese, l&#39;Uruguay';
$lang['UM2'] = '(UTC -2:00) Georgia del Sud / Sud e Isole Sandwich';
$lang['UM1'] = '(UTC -1:00) Azzorre, Isole di Capo Verde';
$lang['UTC'] = '(UTC) Greenwich Mean Time, Western European Time';
$lang['UP1'] = '(UTC +1:00), ora dell&#39;Europa centrale, l&#39;Africa occidentale Tempo';
$lang['UP2'] = '(UTC +2:00) Tempo Africa centrale, orientale Tempo, tempo di Kaliningrad';
$lang['UP3'] = '(UTC +3:00) Mosca Tempo, East Africa Time';
$lang['UP35'] = '(UTC +3:30) Iran Standard Time';
$lang['UP4'] = '(UTC +4:00) Azerbaijan Standard Time, Tempo Samara';
$lang['UP45'] = '(UTC +4:30) Afghanistan';
$lang['UP5'] = '(UTC +5:00) Pakistan Standard Time, Yekaterinburg Time';
$lang['UP55'] = '(UTC +5:30) Standard Time indiano, Sri Lanka Tempo';
$lang['UP575'] = '(UTC +5:45) Nepal Time';
$lang['UP6'] = '(UTC +6:00) Standard Time Bangladesh, Bhutan Time, Tempo di Omsk';
$lang['UP65'] = '(UTC +6:30) Isole Cocos, Myanmar';
$lang['UP7'] = '(UTC +7:00) Krasnoyarsk Time, Cambogia, Laos, Tailandia, Vietnam';
$lang['UP8'] = '(UTC +8:00) Western Australian Standard Time, Tempo Pechino, Irkutsk Time';
$lang['UP875'] = '(UTC +8:45) Australian Standard Time centrale occidentale';
$lang['UP9'] = '(UTC +9:00) Standard Time Giappone, Corea Standard Time, Yakutsk Time';
$lang['UP95'] = '(UTC +9:30) Australian Central Standard Time';
$lang['UP10'] = '(UTC +10:00) Australian Eastern Standard Time, Tempo Vladivostok';
$lang['UP105'] = '(UTC +10:30) Lord Howe Island';
$lang['UP11'] = '(UTC +11:00) Magadan Time, Isole Salomone, Vanuatu';
$lang['UP115'] = '(UTC +11:30) Isola Norfolk';
$lang['UP12'] = '(UTC +12:00) Figi, Isole Gilbert, Kamchatka Time, New Zealand Standard Time';
$lang['UP1275'] = '(UTC +12:45) Chatham Islands Standard Time';
$lang['UP13'] = '(UTC +13:00) Isole Phoenix Tempo, Tonga';
$lang['UP14'] = '(UTC +14:00) Isole Line';
?>