<?
$lang['help_formfield_clientUserid'] = 'Il tuo client avrà bisogno di un codice utente per accedere al proprio conto CreativePro Ufficio. Devi lasciare che il cliente sa che userid che hai selezionato. <br /> <b>CreativePro Ufficio non il tuo client e-mail il codice utente.</b>';
$lang['help_formfield_tags'] = 'I tag sono un modo di classificare gli oggetti, come progetti, clienti o compiti in un modo che abbia senso per te. I tag sono anche le ricerche.';
$lang['help_tag_format'] = 'Separa i tags con spazi.';
$lang['help_revise_search'] = 'Prova a rivedere i criteri di ricerca e di eseguire nuovamente la ricerca.';
?>