<?
$lang['db_invalid_connection_str'] = 'Não foi possível determinar a base de dados com base em definições a seqüência de conexão que você enviou.';
$lang['db_unable_to_connect'] = 'Não foi possível conectar ao servidor utilizando o seu banco de dados fornecidos definições.';
$lang['db_unable_to_select'] = 'Não é possível selecionar o banco de dados especificado:% s';
$lang['db_unable_to_create'] = 'Não foi possível criar o banco de dados especificado:% s';
$lang['db_invalid_query'] = 'A consulta é apresentado não é válido.';
$lang['db_must_set_table'] = 'Você deve definir o quadro de dados a ser utilizado com a sua consulta.';
$lang['db_must_set_database'] = 'Você deverá definir o nome do banco de dados em seu banco de dados ficheiro de configuração.';
$lang['db_must_use_set'] = 'Você deve usar o &quot;set&quot; método para atualizar uma entrada.';
$lang['db_must_use_where'] = 'Atualizações não são permitidas, a menos que contenham um &quot;onde&quot; cláusula.';
$lang['db_del_must_use_where'] = 'Apaga não estão autorizados a não ser que eles contêm uma &quot;onde&quot; ou &quot;como&quot; cláusula.';
$lang['db_field_param_missing'] = 'Para buscar campos requer o nome da tabela como um parâmetro.';
$lang['db_unsupported_function'] = 'Este recurso não está disponível para o banco de dados que você está usando.';
$lang['db_transaction_failure'] = 'Transação falha: Rollback realizada.';
$lang['db_unable_to_drop'] = 'Incapaz de largar o banco de dados especificado.';
$lang['db_unsuported_feature'] = 'Unsupported funcionalidade da plataforma de dados que você está usando.';
$lang['db_unsuported_compression'] = 'O arquivo compressão formato que você escolheu não é suportado pelo seu servidor.';
$lang['db_filepath_error'] = 'Impossível escrever dados para o caminho do arquivo que você enviou.';
$lang['db_invalid_cache_path'] = 'O cache caminho que apresentou não é válida ou writable.';
$lang['db_table_name_required'] = 'Uma tabela nome é necessário para essa operação.';
$lang['db_column_name_required'] = 'Uma coluna nome é necessário para essa operação.';
$lang['db_column_definition_required'] = 'Uma coluna definição é necessária para essa operação.';
$lang['db_unable_to_set_charset'] = 'Incapaz de estabelecer conexão cliente conjunto de caracteres:% s';
$lang['db_error_heading'] = 'A Database Error Occurred';
?>