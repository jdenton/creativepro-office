<?
$lang['email_must_be_array'] = 'O método de validação e-mail deve ser passado um array.';
$lang['email_invalid_address'] = 'Endereço de email inválido:% s';
$lang['email_attachment_missing'] = 'Não foi possível localizar o seguinte e-mail attachment:% s';
$lang['email_attachment_unreadable'] = 'Não é possível abrir este anexo:% s';
$lang['email_no_recipients'] = 'Você deve incluir destinatários: Para, Cc ou Bcc';
$lang['email_send_failure_phpmail'] = 'Não foi possível enviar o e-mail utilizando o PHP mail (). Seu servidor pode não estar configurado para enviar e-mail utilizando este método.';
$lang['email_send_failure_sendmail'] = 'Não foi possível enviar o e-mail utilizando o PHP Sendmail. Seu servidor pode não estar configurado para enviar e-mail utilizando este método.';
$lang['email_send_failure_smtp'] = 'Não foi possível enviar o e-mail utilizando o PHP SMTP. Seu servidor pode não estar configurado para enviar e-mail utilizando este método.';
$lang['email_sent'] = 'Sua mensagem foi enviada com sucesso usando o protocolo seguinte:% s';
$lang['email_no_socket'] = 'Não é possível abrir um socket para Sendmail. Por favor, verifique as configurações.';
$lang['email_no_hostname'] = 'Você não especificar um host SMTP.';
$lang['email_smtp_error'] = 'SMTP O seguinte erro foi encontrado:% s';
$lang['email_no_smtp_unpw'] = 'Erro: Você deve atribuir um nome de usuário e senha SMTP.';
$lang['email_failed_smtp_login'] = 'Falha ao enviar o comando AUTH LOGIN. Erro:% s';
$lang['email_smtp_auth_un'] = 'Falha ao autenticar usuário. Erro:% s';
$lang['email_smtp_auth_pw'] = 'Falha ao autenticar senha. Erro:% s';
$lang['email_smtp_data_failure'] = 'Não é possível enviar dados:% s';
?>