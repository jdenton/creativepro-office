<?
$lang['Accounting'] = 'Contabilidade';
$lang['Advertising'] = 'Publicidade';
$lang['Aerospace'] = 'Aerospace';
$lang['Airline'] = 'Companhias Aéreas';
$lang['Automotive'] = 'Automóveis';
$lang['Banking'] = 'Bancária';
$lang['BioTech'] = 'BioTech';
$lang['Broadcasting'] = 'Radiodifusão';
$lang['Chemical'] = 'Química';
$lang['Computer Hardware/Supplies'] = 'Computer Hardware / Suprimentos';
$lang['Computer Software'] = 'Computer Software';
$lang['Construction'] = 'Construção';
$lang['Consumer Products'] = 'Consumer Products';
$lang['Distribution'] = 'Distribuição';
$lang['Education'] = 'Educação';
$lang['Engineering'] = 'Engenharia';
$lang['Entertainment'] = 'Entretenimento';
$lang['Environmental'] = 'Ambiental';
$lang['Fashion'] = 'Moda';
$lang['Financial Planning/Services'] = 'Planejamento Financeiro / Serviços';
$lang['Food & Beverage'] = 'Food &amp; Beverage';
$lang['Forestry'] = 'Silvicultura';
$lang['Freight/Shipping'] = 'Frete / Envio';
$lang['Fund Raising'] = 'Fund Raising';
$lang['Government'] = 'Governo';
$lang['Graphic Design'] = 'Design Gráfico';
$lang['Health and Fitness'] = 'Saúde e Fitness';
$lang['Healthcare'] = 'Healthcare';
$lang['Import/Export Trade'] = 'Importação / Exportação Comércio';
$lang['Industrial Supplies/Equipment'] = 'Suprimentos Industrial / Equipamento';
$lang['Insurance'] = 'Seguros';
$lang['Interior Design'] = 'Interior Design';
$lang['Internet'] = 'Internet';
$lang['Legal'] = 'Jurídico';
$lang['Manufacturing'] = 'Manufatura';
$lang['Marketing'] = 'Marketing';
$lang['Medical Supplies/Services'] = 'Medical Supplies / Serviços';
$lang['Mortgage Lending'] = 'Crédito hipotecário';
$lang['Non-Profit'] = 'Non-Profit';
$lang['Office Products'] = 'Office Products';
$lang['Oil and Gas'] = 'Petróleo e Gás';
$lang['Other'] = 'Outro';
$lang['Packaging'] = 'Embalagem';
$lang['Paper'] = 'Livro';
$lang['Payroll Services'] = 'Serviços de folha de pagamento';
$lang['Personnel/Employment'] = 'Pessoal / Emprego';
$lang['Pest Control'] = 'Pest Control';
$lang['Pharmaceutical'] = 'Farmacêutico';
$lang['Photography'] = 'Fotografia';
$lang['Printing'] = 'Imprensa';
$lang['Public Relations'] = 'Relações Públicas';
$lang['Publishing'] = 'Publishing';
$lang['Real Estate'] = 'Real Estate';
$lang['Restaurant'] = 'Restaurante';
$lang['Retail'] = 'Varejo';
$lang['Securities/Commodities'] = 'Títulos / Commodities';
$lang['Security'] = 'Segurança';
$lang['Telecom'] = 'Telecom';
$lang['Training'] = 'Treinamento';
$lang['Transportation'] = 'Transporte';
$lang['Travel'] = 'Viajar';
?>