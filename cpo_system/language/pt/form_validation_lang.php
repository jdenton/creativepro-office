<?
$lang['required'] = 'O campo% s é requerida.';
$lang['isset'] = 'O campo% s deve ter um valor.';
$lang['valid_email'] = 'O campo% s deve conter um endereço de email válido.';
$lang['valid_emails'] = 'O campo% s deve conter todos os endereços de e-mail válido.';
$lang['valid_url'] = 'O campo% s deve conter um URL válido.';
$lang['valid_ip'] = 'O campo% s deve conter um IP válido.';
$lang['min_length'] = 'O campo% s deve ser pelo menos% s caracteres de comprimento.';
$lang['max_length'] = 'O campo% s não pode exceder% s caracteres de comprimento.';
$lang['exact_length'] = 'O campo% s deve ser exatamente% s caracteres de comprimento.';
$lang['alpha'] = 'O campo% s podem conter apenas caracteres alfabéticos.';
$lang['alpha_numeric'] = 'O campo% s podem conter apenas caracteres alfanuméricos.';
$lang['alpha_dash'] = 'O campo% s podem conter apenas caracteres alfanuméricos, sublinhados e hífens.';
$lang['numeric'] = 'O campo% s deve conter apenas números.';
$lang['is_numeric'] = 'O campo% s deve conter apenas caracteres numéricos.';
$lang['integer'] = 'O campo% s deve conter um número inteiro.';
$lang['matches'] = 'O campo% s não corresponde ao% s campo.';
$lang['is_natural'] = 'O campo% s deve conter apenas números positivos.';
$lang['is_natural_no_zero'] = 'O campo% s deve conter um número maior que zero.';
$lang['form_required_clientUserid'] = 'Cliente userid não pode ser deixado em branco.';
$lang['form_istaken_clientUserid'] = 'Cliente userid% s é alreay tomadas. Selecione outro.';
$lang['form_required_clientCompany'] = 'Cliente empresa não pode ser deixado em branco.';
$lang['form_required_client'] = 'Cliente não pode ser deixado em branco.';
$lang['form_required_project_title'] = 'Título do projecto não pode ser deixado em branco.';
$lang['form_required_email'] = 'Digite um endereço de email válido.';
$lang['form_minlength_clientEmail'] = 'Cliente e-mail deve ser de 10 caracteres.';
?>