<?
$lang['help_formfield_clientUserid'] = 'Seu cliente vai precisar de um userid para acessar sua conta CreativePro Office. Você deve deixar o seu cliente sabe que userid selecionadas. <br /> <b>CreativePro Office e-mail não será o seu cliente o userid.</b>';
$lang['help_formfield_tags'] = 'Tags são uma maneira de categorizar itens como projetos, clientes ou tarefas em uma maneira que faz sentido para você. Tags também são pesquisáveis.';
$lang['help_tag_format'] = 'Separe tags com espaços.';
$lang['help_revise_search'] = 'Tente rever os seus critérios de pesquisa e busca de novo.';
?>