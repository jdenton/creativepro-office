<?
$lang['required'] = 'El campo% s es obligatorio.';
$lang['isset'] = 'El campo% s deben tener un valor.';
$lang['valid_email'] = 'El campo% s debe contener una dirección válida de correo electrónico.';
$lang['valid_emails'] = 'El campo% s debe contener todas las direcciones de correo electrónico válidas.';
$lang['valid_url'] = 'El campo% s debe contener una URL válida.';
$lang['valid_ip'] = 'El campo% s debe contener una IP válida.';
$lang['min_length'] = 'El campo% s debe ser al menos% s caracteres.';
$lang['max_length'] = 'El campo% s no puede ser superior a% s caracteres.';
$lang['exact_length'] = 'El campo% s debe ser exactamente% s caracteres.';
$lang['alpha'] = 'El campo% s sólo pueden contener caracteres alfabéticos.';
$lang['alpha_numeric'] = 'El campo% s sólo pueden contener caracteres alfanuméricos.';
$lang['alpha_dash'] = 'El campo% s sólo pueden contener caracteres alfanuméricos, guiones bajos y guiones.';
$lang['numeric'] = 'El campo% s debe contener sólo números.';
$lang['is_numeric'] = 'El campo% s debe contener sólo caracteres numéricos.';
$lang['integer'] = 'El campo% s debe contener un número entero.';
$lang['matches'] = 'El campo% s no coincide con el% s campo.';
$lang['is_natural'] = 'El campo% s debe contener sólo números positivos.';
$lang['is_natural_no_zero'] = 'El campo% s debe contener un número mayor que cero.';
$lang['form_required_clientUserid'] = 'Userid cliente no puede estar en blanco.';
$lang['form_istaken_clientUserid'] = 'Userid cliente% s es alreay adoptadas. Por favor seleccione otro.';
$lang['form_required_clientCompany'] = 'Empresa cliente no puede estar en blanco.';
$lang['form_required_client'] = 'Cliente no puede estar en blanco.';
$lang['form_required_project_title'] = 'Título del proyecto no puede estar en blanco.';
$lang['form_required_email'] = 'Por favor, introduzca una dirección de correo electrónico válida.';
$lang['form_minlength_clientEmail'] = 'Dirección de correo electrónico del cliente debe ser de 10 caracteres.';
?>