<?

$lang['email_must_be_array'] = 'El método de validación de correo electrónico debe ser aprobado por una matriz.';

$lang['email_invalid_address'] = 'Dirección de correo electrónico no válido:% s';

$lang['email_attachment_missing'] = 'No se puede localizar el archivo adjunto de correo electrónico siguiente:% s';

$lang['email_attachment_unreadable'] = 'No se puede abrir este archivo adjunto:% s';

$lang['email_no_recipients'] = 'Usted debe incluir a los beneficiarios: A, Cc o Cco';

$lang['email_send_failure_phpmail'] = 'No se puede enviar correos electrónicos utilizando PHP mail (). El servidor podría no estar configurado para enviar correo utilizando este método.';

$lang['email_send_failure_sendmail'] = 'No se puede enviar correos electrónicos utilizando PHP Sendmail. El servidor podría no estar configurado para enviar correo utilizando este método.';

$lang['email_send_failure_smtp'] = 'No se puede enviar correos electrónicos utilizando PHP SMTP. El servidor podría no estar configurado para enviar correo utilizando este método.';

$lang['email_sent'] = 'Su mensaje ha sido enviado con éxito utilizando el siguiente protocolo:% s';

$lang['email_no_socket'] = 'No se puede abrir un socket a Sendmail. Por favor, compruebe los ajustes.';

$lang['email_no_hostname'] = 'Usted no se especifica un nombre de host SMTP.';

$lang['email_smtp_error'] = 'El siguiente error fue encontrado SMTP:% s';

$lang['email_no_smtp_unpw'] = 'Error: Usted debe asignar un nombre de usuario y la contraseña SMTP.';

$lang['email_failed_smtp_login'] = 'Error al enviar el comando AUTH LOGIN. Error:% s';

$lang['email_smtp_auth_un'] = 'No se ha podido autenticar el nombre de usuario. Error:% s';

$lang['email_smtp_auth_pw'] = 'No se ha podido autenticar contraseña. Error:% s';

$lang['email_smtp_data_failure'] = 'No se puede enviar datos:% s';



/*

 * Canned system messages.

 */

$lang['email_shared_event'] = "Someone wants to share an event with you.";



$lang['email_new_team_member_message'] = "Hola. Usted ha sido añadido como un miembro del equipo.\r\nSu contraseña es:\r\nSu nombre de usuario es:\r\n-------------------";

$lang['email_new_team_member_subject'] = "Bienvenido al equipo en CreativePro Office!";



$lang['email_new_client_message'] = "Hola!"

    .Chr(10)."%s ha añadido %s como un cliente a su cuenta de CreativePro Oficina. Su CreativePro cuenta de cliente de Office le permite comunicarse rápidamente con %s sobre los proyectos, facturas, pagos, y más. Para obtener más información sobre su cuenta de CreativePro Oficina, vaya a ".SITE_URL."/clientAccount."

    .Chr(10).Chr(10)

    ."Para acceder a su cuenta, vaya a ".SITE_URL." y de acceso."

    .Chr(10).Chr(10)."Su nombre de usuario es %s".Chr(10).Chr(10)."Su contraseña es %s"

    .Chr(10)

    .Chr(10)."Usted puede cambiar la contraseña después de ingresar haciendo clic en el icono 'Configuración' en la parte superior derecha de la pantalla. Si necesita ayuda o tiene preguntas, por favor, póngase en contacto con ".MAIL_SUPPORT."."

    .Chr(10)

    .Chr(10)."Bienvenido a la Oficina CreativePro!";

$lang['email_new_client_subject'] = "Bienvenido a su cuenta CreativePro Office!";



$lang['email_task_subject']            = "Mensaje de Trabajo: %s";

$lang['email_task_message']            = "%s envió el siguiente mensaje en relación con la tarea %s.";



$lang['email_task_new_subject']        = "Nueva tarea: %s";

$lang['email_task_new_message']        = "%s ha asignado la tarea %s a ti.";



$lang['email_task_edit_subject']       = "Actualización de tareas: %s";

$lang['email_task_edit_message']       = "%s ha actualizado tarea %s.";



$lang['email_calendar_subject']        = "Calendario compartido: %s";

$lang['email_calendar_message']        = "%s ha compartido un calendario con usted: %s.";



$lang['email_invoice_subject']         = "Factura %s de %s'";



$lang['email_footer']                  = ".:-----:.\r\nPara controlar los mensajes que recibe de su CreativePro Oficina, vaya a:\r\n".EMAIL_SETTINGS_LINK;



?>