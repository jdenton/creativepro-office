<?
$lang['help_formfield_clientUserid'] = 'Su cliente necesitará un nombre de usuario para poder acceder a su cuenta CreativePro Oficina. Usted debe dejar que su cliente sabe que usuario haya seleccionado. <br /> <b>CreativePro Oficina no a su cliente de correo electrónico el nombre de usuario.</b>';
$lang['help_formfield_tags'] = 'Las etiquetas son una forma de clasificar los elementos como los proyectos, los clientes o de las tareas de una manera que tenga sentido para usted. Las etiquetas son también la búsqueda.';
$lang['help_tag_format'] = 'Separa las etiquetas con espacios.';
$lang['help_revise_search'] = 'Trate de revisar sus criterios de búsqueda y la búsqueda de nuevo.';
?>