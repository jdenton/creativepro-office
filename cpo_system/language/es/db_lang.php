<?
$lang['db_invalid_connection_str'] = 'No se puede determinar la configuración de la base de datos sobre la base de la cadena de conexión que ha remitido.';
$lang['db_unable_to_connect'] = 'No es posible conectarse a su servidor de base de datos usando la configuración de siempre.';
$lang['db_unable_to_select'] = 'No se pudo seleccionar la base de datos especificada:% s';
$lang['db_unable_to_create'] = 'No se puede crear la base de datos especificada:% s';
$lang['db_invalid_query'] = 'La consulta que ha enviado no es válida.';
$lang['db_must_set_table'] = 'Debe establecer la tabla de base de datos para ser utilizado con la consulta.';
$lang['db_must_set_database'] = 'Debe establecer el nombre de base de datos en su base de datos de archivo de configuración.';
$lang['db_must_use_set'] = 'Usted debe usar el &quot;ajuste&quot; método para actualizar una entrada.';
$lang['db_must_use_where'] = 'No se permiten las actualizaciones si incluyen un &quot;dónde&quot; cláusula.';
$lang['db_del_must_use_where'] = 'Borra no se permite a menos que contengan un &quot;dónde&quot; o &quot;como&quot; cláusula.';
$lang['db_field_param_missing'] = 'Para recuperar los campos requiere el nombre de la tabla como parámetro.';
$lang['db_unsupported_function'] = 'Esta función no está disponible para la base de datos que está utilizando.';
$lang['db_transaction_failure'] = 'Transacción fracaso: Retroceso realizado.';
$lang['db_unable_to_drop'] = 'No se puede abandonar la base de datos especificada.';
$lang['db_unsuported_feature'] = 'No característica de la plataforma de bases de datos que está utilizando.';
$lang['db_unsuported_compression'] = 'El formato de compresión de archivos que ha seleccionado no es compatible con el servidor.';
$lang['db_filepath_error'] = 'No se puede escribir datos en la ruta del archivo que ha enviado.';
$lang['db_invalid_cache_path'] = 'El caché de ruta que ha enviado no es válido o escribir.';
$lang['db_table_name_required'] = 'Un nombre de tabla es necesaria para la operación.';
$lang['db_column_name_required'] = 'Un nombre de columna es necesaria para la operación.';
$lang['db_column_definition_required'] = 'Una columna de definición es necesaria para la operación.';
$lang['db_unable_to_set_charset'] = 'No es posible establecer conexión de cliente conjunto de caracteres:% s';
$lang['db_error_heading'] = 'Se ha producido un error de base de datos';
?>