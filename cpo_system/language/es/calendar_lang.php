<?
$lang['cal_su'] = 'Do';
$lang['cal_mo'] = 'Lu';
$lang['cal_tu'] = 'Ma';
$lang['cal_we'] = 'Mx';
$lang['cal_th'] = 'Ju';
$lang['cal_fr'] = 'Vi';
$lang['cal_sa'] = 'Sa';
$lang['cal_sun'] = 'Dom';
$lang['cal_mon'] = 'Lun';
$lang['cal_tue'] = 'Mar';
$lang['cal_wed'] = 'Mié';
$lang['cal_thu'] = 'Jue';
$lang['cal_fri'] = 'Vie';
$lang['cal_sat'] = 'Sáb';
$lang['cal_sunday'] = 'Domingo';
$lang['cal_monday'] = 'Lunes';
$lang['cal_tuesday'] = 'Martes';
$lang['cal_wednesday'] = 'Miércoles';
$lang['cal_thursday'] = 'Jueves';
$lang['cal_friday'] = 'Viernes';
$lang['cal_saturday'] = 'Sábado';
$lang['cal_jan'] = 'Enero';
$lang['cal_feb'] = 'Febrero';
$lang['cal_mar'] = 'Marzo';
$lang['cal_apr'] = 'Abril';
$lang['cal_may'] = 'Mayo';
$lang['cal_jun'] = 'Junio';
$lang['cal_jul'] = 'Julio';
$lang['cal_aug'] = 'Agosto';
$lang['cal_sep'] = 'Sep';
$lang['cal_oct'] = 'Octubre';
$lang['cal_nov'] = 'Noviembre';
$lang['cal_dec'] = 'Diciembre';
$lang['cal_january'] = 'Enero';
$lang['cal_february'] = 'Febrero';
$lang['cal_march'] = 'Marzo';
$lang['cal_april'] = 'Abril';
$lang['cal_mayl'] = 'Mayo';
$lang['cal_june'] = 'Junio';
$lang['cal_july'] = 'Julio';
$lang['cal_august'] = 'Agosto';
$lang['cal_september'] = 'Septiembre';
$lang['cal_october'] = 'Octubre';
$lang['cal_november'] = 'Noviembre';
$lang['cal_december'] = 'Diciembre';
?>