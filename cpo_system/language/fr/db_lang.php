<?
$lang['db_invalid_connection_str'] = 'Impossible de déterminer les paramètres de base de données basée sur la chaîne de connexion que vous avez envoyée.';
$lang['db_unable_to_connect'] = 'Impossible de se connecter à votre serveur de base de données en utilisant les paramètres fournis.';
$lang['db_unable_to_select'] = 'Impossible de sélectionner la base de données spécifiée:% s';
$lang['db_unable_to_create'] = 'Impossible de créer la base de données spécifiée:% s';
$lang['db_invalid_query'] = 'La requête que vous avez envoyée n&#39;est pas valide.';
$lang['db_must_set_table'] = 'Vous devez définir la table de base de données pour être utilisés avec votre requête.';
$lang['db_must_set_database'] = 'Vous devez définir le nom de base de données dans votre base de données du fichier de configuration.';
$lang['db_must_use_set'] = 'Vous devez utiliser le &quot;set&quot; pour mettre à jour la méthode d&#39;entrée.';
$lang['db_must_use_where'] = 'Mises à jour ne sont pas autorisés, sauf si elles contiennent une &quot;, où&quot; clause.';
$lang['db_del_must_use_where'] = 'Supprime ne sont pas autorisés, sauf si elles contiennent un &quot;cas&quot; ou &quot;comme la&quot; clause.';
$lang['db_field_param_missing'] = 'Pour récupérer les champs requiert le nom de la table en tant que paramètre.';
$lang['db_unsupported_function'] = 'Cette fonctionnalité n&#39;est pas disponible pour la base de données que vous utilisez.';
$lang['db_transaction_failure'] = 'Transaction échec: Rollback effectué.';
$lang['db_unable_to_drop'] = 'Impossible de supprimer la base de données spécifiée.';
$lang['db_unsuported_feature'] = 'Non prise en fonction de la plate-forme de base de données que vous utilisez.';
$lang['db_unsuported_compression'] = 'Le format de compression de fichier que vous avez choisi n&#39;est pas pris en charge par votre serveur.';
$lang['db_filepath_error'] = 'Impossible d&#39;écrire des données sur le chemin du fichier que vous avez soumis.';
$lang['db_invalid_cache_path'] = 'Le chemin que vous avez présenté cache n&#39;est pas valide ou en écriture.';
$lang['db_table_name_required'] = 'Un nom de table est requise pour cette opération.';
$lang['db_column_name_required'] = 'Un nom de colonne est requis pour cette opération.';
$lang['db_column_definition_required'] = 'Une colonne de définition est nécessaire pour cette opération.';
$lang['db_unable_to_set_charset'] = 'Impossible de configurer la connexion client caractères:% s';
$lang['db_error_heading'] = 'Une base de données Une erreur s&#39;est produite';
?>