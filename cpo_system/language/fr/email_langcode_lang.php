<?
$lang['email_must_be_array'] = 'L&#39;email de validation méthode doit être un tableau.';
$lang['email_invalid_address'] = 'Adresse e-mail incorrecte:% s';
$lang['email_attachment_missing'] = 'Impossible de trouver la pièce jointe de courriel suivante:% s';
$lang['email_attachment_unreadable'] = 'Impossible d&#39;ouvrir cette pièce jointe:% s';
$lang['email_no_recipients'] = 'Vous devez inclure les bénéficiaires: A, Cc ou Cci';
$lang['email_send_failure_phpmail'] = 'Impossible d&#39;envoyer l&#39;email en utilisant PHP mail (). Votre serveur peut ne pas être configuré pour envoyer des mail en utilisant cette méthode.';
$lang['email_send_failure_sendmail'] = 'Impossible d&#39;envoyer l&#39;email en utilisant PHP Sendmail. Votre serveur peut ne pas être configuré pour envoyer des mail en utilisant cette méthode.';
$lang['email_send_failure_smtp'] = 'Impossible d&#39;envoyer des messages à l&#39;aide de PHP SMTP. Votre serveur peut ne pas être configuré pour envoyer des mail en utilisant cette méthode.';
$lang['email_sent'] = 'Votre message a été envoyé à l&#39;aide du protocole suivant:% s';
$lang['email_no_socket'] = 'Impossible d&#39;ouvrir un socket de Sendmail. S&#39;il vous plaît vérifier les paramètres.';
$lang['email_no_hostname'] = 'Vous n&#39;avez pas un nom d&#39;hôte SMTP.';
$lang['email_smtp_error'] = 'L&#39;erreur suivante a été rencontrée SMTP:% s';
$lang['email_no_smtp_unpw'] = 'Erreur: Vous devez attribuer un nom d&#39;utilisateur et votre mot de passe SMTP.';
$lang['email_failed_smtp_login'] = 'Échec de l&#39;envoi de la commande AUTH LOGIN. Erreur:% s';
$lang['email_smtp_auth_un'] = 'Échec de l&#39;authentification du nom d&#39;utilisateur. Erreur:% s';
$lang['email_smtp_auth_pw'] = 'Échec de l&#39;authentification du mot de passe. Erreur:% s';
$lang['email_smtp_data_failure'] = 'Impossible d&#39;envoyer des données:% s';
?>