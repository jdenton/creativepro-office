<?
$lang['required'] = 'Le champ% s est requis.';
$lang['isset'] = 'Le champ% s doit avoir une valeur.';
$lang['valid_email'] = 'Le champ% s doit contenir une adresse email valide.';
$lang['valid_emails'] = 'Le champ% s doit contenir toutes les adresses email valides.';
$lang['valid_url'] = 'Le champ% s doit contenir une URL valide.';
$lang['valid_ip'] = 'Le champ% s doit contenir une IP valide.';
$lang['min_length'] = 'Le champ% s doit être au moins de% s caractères.';
$lang['max_length'] = 'Le champ% s ne peut pas dépasser% s caractères.';
$lang['exact_length'] = 'Le champ% s doit être exactement de% s caractères.';
$lang['alpha'] = 'Le champ% s mai ne contiennent que des caractères alphabétiques.';
$lang['alpha_numeric'] = 'Le champ% s mai ne contiennent que des caractères alphanumériques.';
$lang['alpha_dash'] = 'Le champ% s mai ne contiennent que des caractères alphanumériques, de soulignement et des tirets.';
$lang['numeric'] = 'Le champ% s doit contenir uniquement des nombres.';
$lang['is_numeric'] = 'Le champ% s ne doit contenir que des caractères numériques.';
$lang['integer'] = 'Le champ% s doit contenir un entier.';
$lang['matches'] = 'Le champ% s ne correspond pas au domaine% s.';
$lang['is_natural'] = 'Le champ% s doit contenir uniquement des nombres positifs.';
$lang['is_natural_no_zero'] = 'Le champ% s doit contenir un nombre supérieur à zéro.';
$lang['form_required_clientUserid'] = 'Userid Client ne peut pas être vide.';
$lang['form_istaken_clientUserid'] = 'Userid Client% s est vraiment prise. S&#39;il vous plaît choisir un autre.';
$lang['form_required_clientCompany'] = 'Client entreprise ne peut pas être vide.';
$lang['form_required_client'] = 'Client ne peut pas être vide.';
$lang['form_required_project_title'] = 'Titre du projet ne peut pas être vide.';
$lang['form_required_email'] = 'S&#39;il vous plaît entrer une adresse électronique valide.';
$lang['form_minlength_clientEmail'] = 'Client e-mail doit être de 10 caractères.';
?>