<?
$lang['help_formfield_clientUserid'] = 'Votre client aura besoin d&#39;un code d&#39;utilisateur pour accéder à leur compte CreativePro Office. Vous devez informer votre client de savoir quel nom d&#39;utilisateur que vous avez sélectionné. <br /> <b>CreativePro Office ne sera pas le client e-mail de votre code d&#39;utilisateur.</b>';
$lang['help_formfield_tags'] = 'Les tags sont un moyen de classer les articles comme les projets, les clients ou les tâches d&#39;une manière qui fait sens pour vous. Les tags sont aussi consultables.';
$lang['help_tag_format'] = 'Séparez les tags avec des espaces.';
$lang['help_revise_search'] = 'Essayez de réviser vos critères de recherche et effectuer une nouvelle recherche.';
?>