var getAccountDataObj = {
    getAccountPayments : function() {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'myaccount/MyAccount/getAccountPayments/json/0',
			dataType: 'json',
		    success: function(payload){
                renderAccountDataObj.renderPaymentGrid(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    }
}

var renderAccountDataObj = {
    renderPaymentGrid : function(jsonObject) {
        var totalPayments = jsonObject.length;		
		var stringOut     = [];
        
        if (totalPayments<1) {
            stringOut.push('<div class="infoMessageBig">'+commonTerms['settings_no_payments']+'</div>');
        } else {
            stringOut.push('<table class="dataTable stripes" id="paymentGrid">');
            stringOut.push('<thead><tr>');
            stringOut.push('<th></th>');
            stringOut.push('<th>Payment Date</th>');
            stringOut.push('<th>Profile ID</th>');
            stringOut.push('<th>Amount</th>');
            stringOut.push('<th>Credit Card</th>');
            
            stringOut.push('</tr></thead>');
            stringOut.push('<tbody>');
            
            for(a=0;a<=((totalPayments)-1);a++) {
                var payID       = jsonObject[a].PaymentID;
                var profileID   = jsonObject[a].ProfileID;
                var paymentAmt  = jsonObject[a].PaymentAmount;
                var paymentDate = jsonObject[a].DatePaymentHuman;
                var paymentCurrencyMark = jsonObject[a].CurrencyMark;
                var paymentCurrency = jsonObject[a].Currency;
                var paymentCard = jsonObject[a].CardType;
                var paymentCardNumber = jsonObject[a].CardNumber;

                stringOut.push('<td><a href="#" class="linkPDF" data-paymentid="'+payID+'">Print</a></td>');
                stringOut.push('<td>'+paymentDate+'</td>');
                stringOut.push('<td>'+profileID+'</td>');
                stringOut.push('<td>'+paymentCurrencyMark+paymentAmt+' '+paymentCurrency+'</td>');
                stringOut.push('<td>'+paymentCard+' '+paymentCardNumber+'</td>');
                stringOut.push('</tr>');
            }
            stringOut.push('</tbody></table>');
        }
		var stringOutRender = stringOut.join('');
        $('#paymentGridContainer').html(stringOutRender);
		$('#paymentGrid')
			.hiliteTableRow();
            
        $('#paymentGrid').delegate('.linkPDF', 'click', function() {
               var payID = $(this).data('paymentid');
               printObj.goPrint(payID,'PaymentReceipt');
               return false;
        });    
    }
}

$(document).ready(function() {
    getAccountDataObj.getAccountPayments();
});
