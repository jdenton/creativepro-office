var initBlogObj = {
    initBlogList : function() {
        $('#blogPostGrid')
			.alternateTableRowColors()
			.hiliteTableRow()
			.tableSorter();

        $('#blogPostGrid').hoverButtons({
               elementClass:     'postRow',
               editClass:        'postUpdate',
               deleteClass:      'postDelete',
               insertionElement: 'controls',
               position:         '',
			   insertion:        'prepend'
         });

        $('#blogPostGrid').delegate('.postStatus', 'click', function() {
            var postID = $(this).parents('tr').attr('itemID');
            var newStatus = '1';
            if ($(this).hasClass('active') == true) {
                newStatus = '0';
            }
            var statusObj = $(this);
            $.get(siteURL+'blog/Blog/updatePostStatus/'+postID+'/'+newStatus, function(payload) {
                if (payload == 'active') {
                    statusObj.removeClass().addClass('postStatus').addClass('active').addClass('status_completed');
                } else {
                    statusObj.removeClass().addClass('postStatus').addClass('inactive').addClass('status_not_started');
                }
            });
            return false;
        });

        $('#blogPostGrid').delegate('.buttonEdit', 'click', function() {
            var beID = $(this).attr('itemID');
            window.location = siteURL+'blog/Blog/blogUpdate/'+beID;
            return false;
        });

        $('#blogPostGrid').delegate('.buttonDelete', 'click', function() {
            var beID = $(this).attr('itemID');
            $('#blogPostRow'+beID).fadeOut('slow');
            updateTrashObj.sendToTrash('blogPost',beID);
            return false;
        });
    },
    initBlogPostForm : function() {
        $('.tag').click(function () {
            var tagName = $(this).html();
            var postTags = $('#postTags').val();
            var newPostTags = postTags+tagName+', ';
            $('#postTags').val(newPostTags);
            return false;
        });

        $('#buttonCancel').click(function() {
            window.location = siteURL+'blog/Blog/BlogAdmin';
            return false;
         });
    },
    initBlogCommentForm : function() {
        $('#btnSaveComment').click(function() {
           /*
            * Check required
            */
           var error = false, message = null;
           if ($('#commentName').val() == '') {
               error = true;
               message = 'You must enter your name.';
           }
           if ($('#commentEmail').val() == '') {
               error = true;
               message = 'You must enter your email address.';
           }

           if (error == true) {
               $('#commentMessageContainer').systemMessage({
                    status: 'error',
                    noFade:  1,
                    size:    'Big',
                    message: message
                });
           } else {
               $('#commentMessageContainer').systemMessage({
                    status: 'success',
                    noFade:  0,
                    size:    'Big',
                    message: 'Thanks for your comment!'
                });

                /*
                 * Save comment
                 */
                var formHash = {
                    beID  : $('#commentBlogPostID').val(),
                    name  : $('#commentName').val(),
                    email : $('#commentEmail').val(),
                    url   : $('#commentURL').val(),
                    text  : $('#commentText').val()
                }
                $.ajax({
                    type:  "POST",
                    url:   siteURL+'blog/Blog/saveBlogPostComment',
                    data: formHash,
                    dataType: 'json',
                    success: function(payload) {
                        commentRenderObj.renderNewComment(payload);
                        initBlogObj.clearCommentForm();
                    }
                });
           }

           return false;
        });
    },
    clearCommentForm : function() {
        $('#commentText').val('');
    }
}

var commentRenderObj = {
    renderNewComment : function(jsonObj) {
        var commentName   = jsonObj.UserName;
        var commentEmail  = jsonObj.UserEmail;
        var commentURL    = jsonObj.UserURL;
        var commentAvatar = jsonObj.AvatarURL;
        var commentText   = jsonObj.Comment;
        var commentDate   = jsonObj.DateComment;
        var admin         = jsonObj.Admin;

        /*
         * Determine container class
         */
        var commentContainerClass = 'commentAltContainer';
        if ($('.commentContainer:last').hasClass('commentAltContainer')) {
            commentContainerClass = '';
        }

        var stringOut = [];
        stringOut.push('<div class="commenterContainer">');
        stringOut.push('<span class="avatar46background" style="margin-right: 12px;"><img class="avatar46 left" alt="'+commentName+'" src="'+commentAvatar+'" /></span>');
        stringOut.push('<h3>'+commentName+'</h3><span class="gray2">'+commentDate+'</span>');
        if (admin == 1) {
            stringOut.push('<span class="cpoAdminIcon" style="margin: 6px 0 0 60px;"></span>');
        }
        stringOut.push('</div>');
        stringOut.push('<div class="'+commentContainerClass+' commentContainer">'+commentText+'</div>');
        stringOut.push('<div style="clear: both;"></div>');
        var outString = stringOut.join('');
        $('#commentsContainer').append(outString);
    }
}

$(document).ready(function() {
    if ($('#pageLeftColumn').attr('pageID') == 'viewPosts') {
		initBlogObj.initBlogList();
	} else if ($('#pageLeftColumn').attr('pageID') == 'updatePosts') {
        initBlogObj.initBlogPostForm();
    }
    initBlogObj.initBlogCommentForm();
});


