var calendar, calendars, dateStart = '', dateEnd = '', allDay, eventGuestBox, calendarGuestBox;

var calendarObj = {
    initCalendar : function() {
        var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

        $('#formCalendarPopup').dialog({
        	autoOpen:      false,
        	closeOnEscape: true,
        	width:         '500px',
        	position:      'center',
			resizable:     false,
            draggable:     false,
			zIndex:        1000,
			open:          function(event,ui) {
                
			}
        });
        calendarObj.initPopupFormControls();

		calendar = $('#calendarContainer').fullCalendar({
            header: false,
            selectable: true,
            selectHelper: true,
            editable: true,
            defaultView: 'month',
            events: function(start, end, callback) {
                var dateStart = $.fullCalendar.formatDate(start,'yyyy-MM-dd');
                var dateEnd   = $.fullCalendar.formatDate(end,'yyyy-MM-dd');
                $.getJSON(siteURL+'calendar/CalendarView/getEvents/'+dateStart+'/'+dateEnd+'/month', function(payload) {
                    var events = [];

                    for (var a=0;a<=payload.length-1;a++) {
                        events.push(supportCalendarObj.makeFCEventObject(payload[a]));
                    }

                    callback(events);
                });
            },
            select: function(start, end, allDay, e) {
                var view = calendar.fullCalendar('getView');
                var viewName = view.name;

                dateStart = start;
                dateEnd = end;
                allDay  = allDay;

                $('#dateStartPopup').val($.fullCalendar.formatDate(start,'yyyy-MM-dd'));
                $('#dateEndPopup').val($.fullCalendar.formatDate(end,'yyyy-MM-dd'));
                
                $('#dateStartPopupText').html($.fullCalendar.formatDate(start,'ddd MMM d'));
                $('#dateEndPopupText').html($.fullCalendar.formatDate(end,'ddd MMM d'));

                if (viewName == 'month') {
                    $('#eventTimeStartPopup').val('10:00 am');
                    $('#eventTimeEndPopup').val('');
                    $('#allDay').val('1');
                } else {
                    $('#eventTimeStartPopup').val($.fullCalendar.formatDate(start,'h:mm tt'));
                    $('#eventTimeEndPopup').val($.fullCalendar.formatDate(end,'h:mm tt'));
                    $('#allDay').val('0');
                }

                $('#dateEndPopup').html();
                $('#formCalendarPopup').dialog('open');
                calendar.fullCalendar('unselect');
            },
            eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
                updateCalendarObj.updateEventAfterMove(event,$.fullCalendar.formatDate(event.start,'yyyy-MM-dd HH:mm:ss'),$.fullCalendar.formatDate(event.end,'yyyy-MM-dd HH:mm:ss'));
            },
            eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
                updateCalendarObj.updateEventAfterMove(event,$.fullCalendar.formatDate(event.start,'yyyy-MM-dd HH:mm:ss'),$.fullCalendar.formatDate(event.end,'yyyy-MM-dd HH:mm:ss'));
            },
            eventRender : function(event, element) {
                supportCalendarObj.renderEventPopup(event, element);
            }
        });

		supportCalendarObj.renderCalendarPanel(calendars);

        $('.fc-day-number').each(function() {
            var qtipElement = $(this);
            var qtipContent;

            var thisDate = new Date(qtipElement.next().html());
            qtipElement.qtip({
                position: {
                    corner: {
                        target:  'topRight',
                        tooltip: 'bottomLeft'
                    }
                },
                show: {when: {event: 'mouseover'}, solo: true},
                hide: {
                    when: {
                    event: 'mouseout'
                },
                effect: {
                    type: 'fade'
                }
                },
                style: {
                    border: {
                        width: 5,
                        radius: 5
                    },
                    tip: {
                        corner: 'bottomLeft'
                    },
                    name: 'cream',
                    width: 350,
                    padding: 6
                },
                api: {
                    onRender: function() {
                        var thisDate2 = thisDate.setHours(0);
                        var todayEvents = [];
                        calendar.fullCalendar('clientEvents', function(event) {
                            var start = new Date(event.start);
                            var end   = new Date(event.end);

                            var startDate = start.setHours(0);
                            var endDate   = end.setHours(0);
                            if( (startDate <= thisDate2 && endDate >= thisDate2) || thisDate2 == startDate || thisDate2 == endDate) {

                                todayEvents.push(event);
                            }
                        });
                        if (todayEvents.length>0) {
                            qtipContent = supportCalendarObj.renderAgendaForToday(thisDate, todayEvents);
                            this.updateContent(qtipContent);
                        } else {
                            this.destroy();
                        }
                    },
                    onHide: function() {
                    }
                }
            });
        });

        /*
		 * Look for a default month/year
		 */
		if ($('#masterMonth').val()>0) {
			var defaultMonth = $('#masterMonth').val();
			var defaultYear = $('#masterYear').val();
			var date = new Date(defaultYear,defaultMonth, 1);
			calendar.fullCalendar('gotoDate',date);
		}

        supportCalendarObj.renderDateSelectBoxes();

		/*
		 * Event form event handlers
		 */
		$('.calendarColorSwatch').click(function() {
			var colorSelected = $(this).attr('color');
			var colorOrig     = $('#calendarEditColor').val();
			$('#calendarEditColor').val(colorSelected);
            $('#calendarEditTitle').removeClass().addClass(colorSelected);
		});

		$('#linkCalendarAdd').click(function() {
			$('#panelCalendarEdit').slideDown('fast');
			$('#linkCalendarAdd').hide();
            $('#buttonDeleteCalendar').hide();
            return false;
		});

		$('#buttonSaveCalendar').click(function() {
			updateCalendarObj.saveCalendar();
            return false;
		});

		$('#buttonCancelCalendar').click(function() {
			supportCalendarObj.clearCalendarForm();
            return false;
		});

        $('#buttonDeleteCalendar').click(function() {
            var calendarID = $('#calendarEditID').val();
            var calendarTitle = $('#calendarEditTitle').val();
            $.get(siteURL+'calendar/CalendarView/getNumberOfEventsForCalendar/'+calendarID, function(payload) {
                if (payload>0) {
                    updateCalendarObj.deleteCalendarModal(calendarID,calendarTitle);
                } else {
                    updateCalendarObj.deleteCalendar(calendarID,0,true);
                }
            });
            return false;
		});

        /*
         * Make contact autosuggest box for new calendar
         */
		contactObj.makeContactAutoSuggest($('#sTeamAutocompleter'),false,false,true,false,false,'','','','calendarGuestAutocompleter');
		$('#labelAgenda').html(moment().format('dddd MMMM d'));

        supportCalendarObj.attachCalendarHeaderButtonEvents();
        supportCalendarObj.renderCalendarTitle();
        
        $('#buttonAddEvent').click(function() {
            calendarObj.showEditForm();
			return false;
		});
    },
    initPopupFormControls : function(dateStart,dateEnd) {
		$('#eventTimeStartPopup').focus(function() {
			$(this).val('');
		});

		$('#buttonSavePopup').click(function() {
			updateCalendarObj.saveEvent('popup');
			$('#formCalendarPopup').dialog('close');
			return false;
		});

		$('#formCalendarPopup .saverField').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
				updateCalendarObj.saveEvent('popup');
				$('#formCalendarPopup').dialog('close');
                return false;
            }
        });

		supportCalendarObj.populateCalendarSelectFromJSON(calendars,$('#eventCalendarPopup'),false);

		$('#linkMoreDetails').click(function() {
			calendarObj.showEditForm();

			var eventTitle     = $('#eventTitlePopup').val();
			var eventCalendar  = $('#eventCalendarPopup').val();
			var eventTimeStart = $('#eventTimeStartPopup').val();
            var eventDateStart = $('#dateStartPopup').val();
            var eventTimeEnd   = $('#eventTimeEndPopup').val();
            var eventDateEnd   = $('#dateEndPopup').val();

			$('#eventTitle').val(eventTitle);
			$('#eventDateStart').val(Date.parse(eventDateStart).toString('MM/dd/yyyy'));
            $('#eventTimeStart').val(eventTimeStart);

            $('#eventDateEnd').val(Date.parse(eventDateEnd).toString('MM/dd/yyyy'));
            $('#eventTimeEnd').val(eventTimeEnd);

			$('#eventCalendar').val(eventCalendar);
			$('#formCalendarPopup').dialog('close');
            
            $('#eventAllDay').attr('checked',false);
            $('#eventTimeStart').show();
            $('#eventTimeEnd').show();  

            calendarObj.initEditor();
			return false;
		});
	},
    showEditForm : function() {
        $('#formEventContainer').slideDown('fast');
        $('#calendarFullContainer').hide();
        calendarObj.initEditor();
        
        $('html, body').animate({
            scrollTop: $("#pageTitleContainer").offset().top
        }, 300);
    },
    initEditor : function() {
        $('#eventDescription').wysiwyg({
            controls: {
                h2: {visible: false},
                h3: {visible: false},
                insertImage: {visible: false},
                insertTable: {visible: false},
                justifyFull: {visible: false}
            },
            css: siteURL+'css/editorCSS.css'
        });
        $('#eventDescription').wysiwyg('clear');
        $('div.wysiwyg').css('margin-left','265px');
    },
	initFormControls : function() {
		/*
		 * Init date pickers
		 */
		$(".eventDates").datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

		$('textarea.expanding').elastic();

		$('#eventAllDay').click(function() {
			if ($(this).attr('checked') == true) {
				$('#eventTimeStart').hide();
				$('#eventTimeEnd').hide();
			} else {
				$('#eventTimeStart').show();
				$('#eventTimeEnd').show();
			}
		});

		$('#buttonSave').click(function() {
			updateCalendarObj.saveEvent();
			return false;
		});

		$('#buttonCancel').click(function() {
			supportCalendarObj.clearEventForm();
			return false;
		});		

		$('#formEventContainer .saverField').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                updateCalendarObj.saveEvent();
                return false;
            }
        });

		supportCalendarObj.populateCalendarSelectFromJSON(calendars,$('#eventCalendar'));

        contactObj.makeContactAutoSuggest($('#sContactsAutocompleter'),true,true,true,true,false,'','','','eventGuestAutocompleter');
        
        /*
		 * Init tag field
		 */
        var tagBox = $('#calendarTags').autoSuggest(
            tagJSON,
            {
                selectedItemProp: 'value',
                searchObjProps: 'value',
                resultsHighlight: false,
                neverSubmit: true,
                boxWidth: '400px',
                startText: '',
                valuesHolderName: 'projectTagsHolder',
                preFill: $('#tagHolder').val()
            }
        );
	}
}

var updateCalendarObj = {
    saveCalendar : function(order) {
		var calendarID   = $('#calendarEditID').val();
		var calendarName = $('#calendarEditTitle').val();
		var color        = $('#calendarEditColor').val();
        var origColor    = $('#calendarOriginalColor').val();
        var members = $('.autocompleteContactsCalendar').next().val();
        /*
		var memberString = '';
        $('#teamMemberContainer').find('.recipData').each(function() {
           var recipID       = $(this).val();
           var recipLanguage = $(this).attr('recipLanguage');
           var recipType     = $(this).attr('recipType');
           memberString += recipID+','+recipLanguage+'|';
        });
        */

		if (calendarName != '') {
			var formHash = {
                    calendarID   : calendarID,
                    calendarName : calendarName,
					color        : color,
					memberString : members
			}
			$.ajax({
				type: "POST",
				url:  siteURL+'calendar/CalendarUpdate/saveCalendar',
				data: formHash,
				dataType: 'json',
				success: function(payload) {
                    var edit = 0;
                    for(var a=0;a<=(calendars.length-1);a++) {
                        if (calendars[a].CalendarID == payload.CalendarID) {
                            calendars[a] = payload;
                            edit = 1;
                        }
                    }
                    if (edit == 0) {
                        calendars.push(payload);
                    }
					supportCalendarObj.renderCalendarPanel(calendars);

                    /*
                     * Update calendar drop boxes for entry forms
                     */
                    supportCalendarObj.populateCalendarSelectFromJSON(calendars,$('#eventCalendar'));
                    supportCalendarObj.populateCalendarSelectFromJSON(calendars,$('#eventCalendarPopup'));

                    /*
                     * Update event colors for this calendar
                     */
                    $('.fc-event').each(function() {
                       if ($(this).hasClass('calendarID_'+calendarID)) {
                           $(this).removeClass(origColor).addClass(color);
                           $(this).children('.eventContainer').removeClass(origColor).addClass(color);
                       }
                    });
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
		}
		supportCalendarObj.clearCalendarForm();
	},
    deleteCalendar : function(deleteCalendarID,moveToCalendarID,deleteEvents) {
        if (deleteEvents == true) {
            updateTrashObj.sendToTrash('calendarWithEvents',deleteCalendarID);
            /*
             * Remove events from page.
             * Loop through events, if they have class of calendarID_xx
             * then delete from the fullCalendar event collection.
             */
            var events = calendar.fullCalendar( 'clientEvents' );
            $(events).each(function() {
               if ($(this).calendarID == deleteCalendarID) {
                   calendar.fullCalendar( 'removeEvents', $(this).id );
               } 
            });
            var calendarClassName = 'calendarID_'+deleteCalendarID;
            $('.'+calendarClassName).fadeOut('fast');
        } else {
            $.ajax({
				type: "POST",
				url:  siteURL+'calendar/CalendarUpdate/moveEventsToCalendar/'+deleteCalendarID+'/'+moveToCalendarID,
				dataType: 'json',
				success: function(payload) {
                    var newColor  = payload.ColorTo;
                    var origColor = payload.ColorFrom;
                    $('.fc-event').each(function() {
                       if ($(this).hasClass('calendarID_'+deleteCalendarID)) {
                           $(this).removeClass(origColor).addClass(newColor);
                           $(this).children('.eventContainer').removeClass(origColor).addClass(newColor);
                       }
                    });

				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
            updateTrashObj.sendToTrash('calendar',deleteCalendarID);
        }
        /*
         * Remove calendar from calendars array
         */
        calendars.splice(calendars.indexOf(deleteCalendarID), 1);
        $('#modalDelete').dialog('close');

        $('.linkCalendarColorBlock').each(function() {
           if ($(this).attr('calid') == deleteCalendarID) {
               $(this).fadeOut('fast');
               $(this).next().fadeOut('fast');
           }
        });
        /*
         * Update calendar drop boxes for entry forms
         */
        supportCalendarObj.populateCalendarSelectFromJSON(calendars,$('#eventCalendar'));
        supportCalendarObj.clearCalendarForm();
    },
    deleteCalendarModal : function(calendarID,calendarTitle) {
        var modalContents = [];
        modalContents.push('<div class="errorMessageBig">'+commonTerms['deleteCalendar2']+': '+calendarTitle+'</div>');
        modalContents.push('<p>'+commonTerms['deleteCalendar']+'</p>');
        modalContents.push('<p><select id="selectMoveToCalendar" style="margin: 3px 0 3px 0; width: 300px;"></select></p>');
        var outString = modalContents.join('');
        $('#deleteContentsContainer').html(outString);
        supportCalendarObj.populateCalendarSelectFromJSON(calendars,$('#selectMoveToCalendar'),true);

        $('#selectMoveToCalendar option').each(function() {
            if ($(this).val() == calendarID) {
                $(this).remove();
            }
        });

        $('#buttonDeleteModalDelete').click(function(){
            var moveToCalendarID = $('#selectMoveToCalendar').val();
            var deleteEvents = 0;
            if (moveToCalendarID == 0) {
                deleteEvents = 1;
            }
            updateCalendarObj.deleteCalendar(calendarID,moveToCalendarID,deleteEvents);
        });
        $('#modalDelete').dialog('open');
    },
    saveEvent : function(formInterface) {
        var eventAllDay = 0, eventAllDayBool = false;
        var eventDateStart, dateTimeStart, dateTimeStartFinal;
        var eventTimeStart;
        var eventDateEnd, dateTimeEnd, dateTimeEndFinal;
        var eventTimeEnd;
        var eventTitle;
        var eventCalendar;
        var eventDescription;

        if (formInterface == 'popup' || formInterface == 'widget') {
            if (formInterface == 'popup') {
                eventAllDay    = $('#allDay').val();
                if (eventAllDay == '1') {
                    eventAllDayBool = true;
                }
                eventDateStart = $('#dateStartPopup').val();
                eventDateEnd   = $('#dateEndPopup').val();
                eventTimeStart = $('#eventTimeStartPopup').val();
                eventTimeEnd   = $('#eventTimeEndPopup').val();
                eventTitle     = $('#eventTitlePopup').val();
				eventCalendar  = $('#eventCalendarPopup').val();

                dateTimeStart = Date.parse(eventDateStart+' '+eventTimeStart);
                dateTimeEnd   = Date.parse(eventDateEnd+' '+eventTimeEnd);
                dateTimeStartFinal = dateTimeStart.toString('yyyy-MM-dd HH:mm:ss');
                dateTimeEndFinal   = dateTimeEnd.toString('yyyy-MM-dd HH:mm:ss');

                supportCalendarObj.clearEventForm(true);
			} else if (formInterface == 'widget') {
				eventDateStart   = $('#eventDateStartWidget').val();
				eventTimeStart   = $('#eventTimeStartWidget').val()
				eventTitle       = $('#eventTitleWidget').val();
				eventCalendar    = $('#eventCalendarWidget').val();
				eventDescription = $('#eventDescriptionWidget').val();

                dateTimeStart = Date.parse(eventDateStart+' '+eventTimeStart);
                dateTimeStartFinal = dateTimeStart.toString('yyyy-MM-dd HH:mm:ss');
                dateTimeEndFinal   = '0000-00-00 00:00:00';
			}

			var formHash = {
					eventForm        : 'popup',
					eventDateStart   : dateTimeStartFinal,
                    eventDateEnd     : dateTimeEndFinal,
                    eventTitle       : eventTitle,
					eventCalendar    : eventCalendar,
					eventDescription : eventDescription
			}

            $.ajax({
                type: "POST",
                url:  siteURL+'calendar/CalendarUpdate/saveEvent',
                data: formHash,
                dataType: 'json',
                success: function(payload) {
                    if (formInterface == 'popup') {
                        calendar.fullCalendar('renderEvent',
                            supportCalendarObj.makeFCEventObject(payload),
                            true
                        );
                        calendar.fullCalendar('unselect');
					} else if (formInterface == 'widget') {
                        var startDateTime = Date.parse(payload.StartDateTime);
                        var startDate = startDateTime.toString('MMM d');
                        var startDate2 = startDateTime.toString('d');
                        var startTime = startDateTime.toString('h:mm');
                        var dateString = startDate;

                        var startDateHour = startDateTime.getHours();
                        var startDateMin  = startDateTime.getMinutes();

                        if (startTime != '0:00' && startDateHour != 0) {
                            dateString += ', '+startDateTime.toString('h:mm tt');
                        }

						var dateArray = startDate2.split('/');
						var day = dateArray[1];

						var eventDetailString  = '<p><strong>'+payload.Title+'</strong> in '+payload.CalendarTitle+'<br />'+dateString+'</p>';
						var eventHTML = '<div class="EventWidget colorBlock '+payload.CssClass+'" style="margin: 1px ;">'+eventDetailString+'</div>';
                        $('#dayNum_'+day).parents('td').append(eventHTML);
						widgetToolsObj.attachEventPopups();
                        widgetToolsObj.clearEventForm();
					}
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else {
			var eventTimeStart = Date.parse($('#eventTimeStart').val());
			var eventTimeEnd   = Date.parse($('#eventTimeEnd').val());
			if ($('#eventAllDay').attr('checked') == true) {
				eventAllDay = 1;
                eventAllDayBool = true;
				eventTimeStart = '';
				eventTimeEnd   = '';
			}

			if (eventTimeStart != '') {
                eventTimeStart = eventTimeStart.toString('hh:mm tt');
			}
			if (eventTimeEnd != '' && eventTimeEnd != null) {
				eventTimeEnd = eventTimeEnd.toString('hh:mm tt');
			}

            var guests = $('.autocompleteContactsEvent').next().val();
			var eventDescription = $('#eventDescription').wysiwyg('getContent');

            var eventDateStart = Date.parse($('#eventDateStart').val());
            eventDateStart = eventDateStart.toString('yyyy-MM-dd');
            var eventDateEnd = Date.parse($('#eventDateEnd').val());
            if (eventDateEnd != null) {
                eventDateEnd = eventDateEnd.toString('yyyy-MM-dd');
            } else {
                eventDateEnd = '';
            }
			var formHash = {
					action          : $('#action').val(),
					eventForm       : '',
					eventID         : $('#eventID').val(),
					eventTitle      : $('#eventTitle').val(),
					eventDateStart  : eventDateStart,
                    eventTimeStart  : eventTimeStart,
					eventDateEnd    : eventDateEnd,
                    eventTimeEnd    : eventTimeEnd,
					eventAllDay     : eventAllDay,
					eventCalendar   : $('#eventCalendar').val(),
					eventRepeats    : $('#eventRepeats').val(),
					eventReminder   : $('#eventReminder').val(),
					eventLocation   : $('#eventLocation').val(),
					eventDescription: eventDescription,
					eventTags       : $('#calendarTags').val(),
					guests          : guests

			}
			$.ajax({
				type: "POST",
				url:  siteURL+'calendar/CalendarUpdate/saveEvent',
				data: formHash,
				dataType: 'json',
				success: function(payload) {
                    supportCalendarObj.clearEventForm();
                    if ($('#action').val() == 'edit') {
                        calendar.fullCalendar('removeEvents',payload.EventID);
                    }
                    calendar.fullCalendar('renderEvent',
                        supportCalendarObj.makeFCEventObject(payload),
                        true
                    );
                    calendar.fullCalendar('unselect');

				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
		}
    },
    updateEventAfterMove : function(event,newStartDate,newEndDate) {
        var eventType = event.type;
        var eventID   = event.id;

        var formHash = {
            eventID      : eventID,
            newStartDate : newStartDate,
            newEndDate   : newEndDate
        }

        if (eventType == 'Event') {
            $.ajax({
				type: "POST",
				url:  siteURL+'calendar/CalendarUpdate/updateEventDatesAfterMove',
                data: formHash,
				dataType: 'json',
				success: function(payload) {
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
        } else if (eventType == 'Task') {
            $.ajax({
				type: "POST",
				url:  siteURL+'tasks/TaskUpdate/updateTaskDatesAfterMove',
                data: formHash,
				dataType: 'json',
				success: function(payload) {
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
        } else if (eventType == 'Project') {

        }
    },
    deleteEvent : function(eventID) {
        var formHash = {
            id          : eventID,
            deleteEvent : 1
        }

        $.ajax({
            type: "POST",
            url:  'ajaxFunctions.php',
            data: formHash,
            success: function(payload) {
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    }
}

var supportCalendarObj = {
	clearEventForm : function(popup) {
        if (popup == true) {
            $('#dateStartPopup').val('');
            $('#dateEndPopup').val('');
            $('#allDay').val('');
            $('#eventTimeStartPopup').val('');
            $('#eventTimeEndPopup').val('');
            $('#eventTitlePopup').val('');
            $('#eventCalendarPopup').val('');
        } else {
            $('#formEventContainer').slideUp('fast');
            $('#calendarFullContainer').fadeIn('fast');
            $('#eventTitle').val('');
            $('#eventDateStart').val('');
            $('#eventTimeStart').val('');
            $('#eventDateEnd').val('');
            $('#eventTimeEnd').val('');
            $('#eventAllDay').attr('checked',true);
            $('#eventCalendar').val('');
            $('#eventRepeats').val('');
            $('#eventReminder').attr('checked',false);
            $('#eventLocation').val('');
            $('#calendarTags').val('');
            $('#eventDescription').wysiwyg('clear');
            $('#guestClientsCollection').html('');
            $('#guestContactsCollection').html('');
            $('#eventGuestAutocompleter').trigger('reset');
        }
	},
	clearCalendarForm : function() {
		var color = $('#calendarEditColor').val();
		$('#panelCalendarEdit').slideUp('fast', function() {
			$('#linkCalendarAdd').show();
		});
		$('#calendarEditColor').val('');
        $('#calendarOriginalColor').val('');
		$('#calendarEditTitle').removeClass(color).val('');
        $('#calendarEditID').val('');

		$('#eventGuestAutocompleter').trigger('reset');
	},
	populateCalendarSelectFromJSON : function(calendarArray,selectObj,deleteCalendar) {
        selectObj.html('');
        if (deleteCalendar == true) {
            selectObj.html('<option value="0">'+commonTerms['deleteEventsToo']+'</option><option value="0"></option>');
        }
        for(var a=0;a<=(calendarArray.length-1);a++) {
			var jsonObject = calendarArray[a];
			var calendarID = jsonObject.CalendarID;
			var calendarTitle = jsonObject.Title;

			var option = '<option value="'+calendarID+'">'+calendarTitle+'</option>';
			selectObj.append(option);
		}
	},
    renderAgendaForToday : function(agendaDate,events,element) {
        var humanDate = Date.parse(agendaDate);
		$('#labelAgenda').html(humanDate.toString('dddd MMMM d'));

        /*
         * Render event list
         */
        var outArray = [], outString;
        if (events.length>0) {
			outArray.push('<ul class="listLined">');
            for(var a in events) {
                var startDateTime = events[a].start;
                var startDateHour = startDateTime.getHours();
                var startDateMin  = startDateTime.getMinutes();

                var startTime = startDateTime.toString('h:mm tt');
                startTime = startTime.trim();

                if (startDateHour == 0 && startDateMin == 0) {
                    startTime = '';
                } else {
                    if (startTime == '0:00 AM' || startTime == '0:00 PM' || startTime == '0:00') {
                        startTime = '';
                    }
                }
                outArray.push('<li><span class="colorText '+events[a].cssClass+' agendaItem" eventID="'+events[a].id+'" calendarID="'+events[a].calendarID+'" style="font-weight: bold;">'+events[a].title+'</span> <span class="subText">'+startTime+'</span></li>');
            }
			outArray.push('</ul>');
            outString = outArray.join('');
        } else {
            outString = commonTerms['no_events_today'];
        }
        $('#containerAgenda').html(outString);
        return outString;
	},
	renderCalendarPanel : function(calendarArray) {
		$('#containerYourCalendars').html('');
		for(var a=0;a<=(calendarArray.length-1);a++) {
			var jsonObject = calendarArray[a];
			var calendarID = jsonObject.CalendarID;
			var calendarTitle = jsonObject.Title;
			var calendarColor = jsonObject.Color;
			var memberInfo    = jsonObject.MemberInfo;
            var deletable     = jsonObject.Deletable;
			var data = a+'_'+calendarID+'_'+calendarColor+'_'+deletable;
            var memberInfoArray = [];
            if (memberInfo) {
                memberInfoArray = memberInfo.split('||');
            }
			var calendar = '<p class="colorBlock '+calendarColor+' linkCalendarColorBlock" style="cursor: pointer;" calID="'+calendarID+'"></p>';

            if (permJSON['calendarUpdate'] == '1') {
                calendar += '<a href="#" class="linkCalendarText" data="'+data+'" style="display: inline-block;">'+calendarTitle+'</a>';
            } else {
                calendar += calendarTitle;
            }

            for(var b in memberInfoArray) {
                if (memberInfoArray[b]) {
                    calendar += '<input type="hidden" class="memberInfo'+calendarID+'" value="'+memberInfoArray[b]+'" />';
                }
            }
            calendar += '<p></p>';
			$('#containerYourCalendars').append(calendar);
		}

		$('.linkCalendarText').click(function() {
			var data = $(this).attr('data');
			var dataArray = data.split('_');
			var count     = dataArray[0];
			var calID     = dataArray[1];
			var color     = dataArray[2];
            var deletable = dataArray[3];

            var memberOut = [];
            var data;
            $('.memberInfo'+calID).each(function() {
               var memberData = $(this).val();
               var memberArray = memberData.split('|');
               // ID|Name|Language
               if (memberArray[0] != '' && memberArray[1] != '') {
                   data = [{'value': memberArray[0]+'-team-'+memberArray[2], 'contactName': memberArray[1]}];
                   $('#calendarGuestAutocompleter').trigger("add_item_extra", data);
               }
            });
            //var memberString = memberOut.join('');
			var title   = $(this).html();

			$('#panelCalendarEdit').slideDown('fast');
			$('#linkCalendarAdd').hide();

			$('#calendarEditID').val(calID);
			$('#calendarEditColor').val(color);
            $('#calendarOriginalColor').val(color);
			$('#calendarEditTitle')
                .removeClass()
                .addClass(color)
                .val(title);

			//$('#teamMemberContainer').html(memberString);
            $('.removeContact').unbind('click').click(function() {
                $(this).parent().remove();
                return false;
            });

            /*
             * Show delete button if deletable = 1
             */
            if (deletable == 1) {
                $('#buttonDeleteCalendar').show();
            } else {
                $('#buttonDeleteCalendar').hide();
            }

			return false;
		});

		$('.linkCalendarColorBlock').click(function() {
			var calID = $(this).attr('calID');

			if ($(this).hasClass('off')) {
				$(this).removeClass('off');
				supportCalendarObj.toggleCalendar(calID,'on');
			} else {
				$(this).addClass('off');
				supportCalendarObj.toggleCalendar(calID,'off');
			}
			return false;
		});
	},
	toggleCalendar : function(calendarID,state) {
        var calendarClassName = 'calendarID_'+calendarID;
		if (state == 'off') {
            $('.'+calendarClassName).fadeOut('fast');
		} else {
            $('.'+calendarClassName).fadeIn('fast');
		}
	},
    renderEventPopup : function(event,element) {
        var outString  = [];
		var startDate  = event.start.toString('MMM d');
        var startTime  = event.start.toString('h:mm');
        var endDate, endTime, endDateHour, endDateMin, endTimeFormatted;
        if (event.end != null) {
            endDate    = event.end.toString('MMM d');
            endTime    = event.end.toString('h:mm');
            endTimeFormatted = event.end.toString('h:mm tt');
            endDateHour = event.end.getHours();
            endDateMin  = event.end.getMinutes();
        }
        
		var dateString = startDate;
        var startDateHour = event.start.getHours();
        var startDateMin  = event.start.getMinutes();

		if (startTime != '0:00' && startDateHour != 0 && event.type != 'Project') {
			dateString += ', '+event.start.toString('h:mm tt');
		}

        if (typeof(endDate) != 'undefined') {
            if (endDate == startDate && endTime != startTime && endTime != '0:00') {
                dateString += ' to '+endTimeFormatted;
            } else if (endDate != '' && endDate != startDate) {
                if (endTime != '0:00' && endDateHour != 0 && endDateMin != 0) {
                    endDate += ', '+endTimeFormatted;
                }
                dateString += ' to '+endDate;
            }
        }
        
		var eventTitle = '';
		if (event.type == 'Task') {
			eventTitle = '<a href="'+siteURL+'tasks/TaskViewExternal/viewTask/">'+event.title+'</a>';
		} else {
			eventTitle = event.title;
		}
        outString.push('<p style="margin-bottom: 6px;"><strong>'+eventTitle+'</strong> ('+dateString+')</p>');
		outString.push('<p>'+event.description+'</p>');

		if (event.canedit == 1) {
			outString.push('<button class="smallButton buttonEditEvent" eventID="'+event.id+'" style="margin: 6px 12px 0 0; float: left;"><span class="edit single"></span></button>');
		}
        if (event.deletable == 1) {
            outString.push('<button class="smallButton buttonDeleteEvent" eventID="'+event.id+'" style="margin: 6px 12px 0 0; float: left;"><span class="delete single"></span></button>');
        }

        var renderString = outString.join('');

        element.qtip({
            content: {
                text: renderString
            },
            position: {
                corner: {
                       target:  'topRight',
                       tooltip: 'bottomLeft'
                   }
            },
            show: {solo: true},
            hide: {fixed: true, delay: 500},
            style: {
				   border: {
					   width: 5,
                       radius: 5
				   },
				   tip: {
					   corner: 'bottomLeft'
				   },
				   name: 'cream',
				   width: 350,
				   padding: 6
            },
            api: {
                onRender: function() {
                    var popupObj = this;
                    $('.buttonEditEvent').unbind();
                    $('.buttonDeleteEvent').unbind();
                    supportCalendarObj.attachEventPopupHandlers(popupObj,event,element);
                },
                onHide: function() {
                    //supportCalendarObj.clearEventForm(true);
                }
            }
        });
    },
    attachEventPopupHandlers : function(popupObj,event,element) {
        $('.buttonEditEvent').click(function() {
            var eventID = $(this).attr('eventID');
            popupObj.hide();
            calendarObj.showEditForm();

            /*
             * Get any guests for this event
             */
            var data;
                
            $.getJSON(siteURL+'calendar/CalendarView/getEventGuests/'+eventID+'/json/0', function(payload) {
                $.each(payload, function(i,item){
                    var guestPID = payload[i].ItemID;
                    var guestUID = payload[i].LoginUserid;
					var language = payload[i].Language;
                    var itemType = payload[i].ItemType;
                    var itemText, itemSystemType;
                    
                    if (language == null) {
                        language = 'en';
                    }
                    
                    if (itemType == 'C') {
                        itemText = payload[i].Company;
                        itemSystemType = 'client';
                        //supportCalendarObj.renderGuestItem($('#guestClientsCollection'),'client_'+itemID,itemText);
                    } else if (itemType == 'P') {
                        itemText = payload[i].NameFirst+' '+payload[i].NameLast;
                        itemSystemType = 'team';
                        //supportCalendarObj.renderGuestItem($('#guestContactsCollection'),'contact_'+itemID+'_'+language,itemText);
                    }
                    if (itemText != '') {
                        data = [{'value': guestPID+'-'+itemSystemType+'-'+language, 'contactName': itemText}];
                        $('#eventGuestAutocompleter').trigger("add_item_extra", data);
                    }
                });
                
            });            

			/*
			 * Populate event form from object
			 */
			$('#eventID').val(eventID);
			$('#action').val('edit');
			$('#eventTitle').val(event.title);

			$('#eventTimeStart').show();
			$('#eventTimeEnd').show();
            $('#eventAllDay').attr('checked',event.allDay);

			var startDate = event.start.toString("M/d/yyyy");
			var startTime = event.start.toString("h:mm tt");

            var endDate, endTime;
            if (event.end != null) {
                endDate   = event.end.toString("M/d/yyyy");
                endTime   = event.end.toString("h:mm tt");
            }

			$('#eventDateStart').val(startDate);
			$('#eventTimeStart').val(startTime);
			if (endDate != ''  && (endDate != startDate || endTime != '')) {
				/*
				 * This is a multi-day event or a 1 day event with a specified end time,
                 * populate end date
				 */
				$('#eventDateEnd').val(endDate);
				$('#eventTimeEnd').val(endTime);
			} else {
				$('#eventTimeEnd').val('');
			}
			$('#eventCalendar').val(event.calendarID);
			$('#eventRepeats').val(event.recurring);
			$('#eventReminder').attr('checked',false);
			if (event.EmailReminder == 1) {
				$('#eventReminder').attr('checked',true);
			}
			$('#eventLocation').val(event.location);
			$('#calendarTags').val(event.tags);
            $('#eventDescription').wysiwyg('setContent',event.description);
			return false;
		});

        $('.buttonDeleteEvent').click(function() {
            var eventID = $(this).attr('eventID');
			updateTrashObj.sendToTrash('event',eventID);
            calendar.fullCalendar( 'removeEvents', eventID );
            popupObj.hide();
            return false;
        });
    },
    makeFCEventObject : function(eventObj) {
        var allDay = false, dragable = true;
        if (eventObj.AllDay == '1') {
            allDay = true;
        }
        if (eventObj.Dragable != '1') {
            dragable = false;
        }
        var newEventObj = {
            id:          eventObj.EventID,
            title:       genericFunctions.txtToField(eventObj.Title),
            start:       eventObj.StartDateTimeTS,
            end:         eventObj.EndDateTimeTS,
            className:   [eventObj.IconClass,eventObj.CssClass,'calendarID_'+eventObj.CalendarID],
            allDay:      allDay,
            canedit:     eventObj.Editable,
            deletable:   eventObj.Deletable,
            description: eventObj.Description,
            location:    eventObj.Location,
            tags:        eventObj.Tags,
            recurring:   eventObj.Recurring,
            entered:     eventObj.NameFirst+' '+eventObj.NameLast,
            type:        eventObj.Type,
            calendarID:  eventObj.CalendarID,
            cssClass:    eventObj.CssClass,
            editable:    dragable
        }
        return newEventObj;
    },
    removeEventFromArray : function(eventID) {
        for(var a in eventArray) {
            if (eventArray[a].EventID == eventID) {
                eventArray[a] = '';
            }
        }
    },
    renderGuestItem : function(containerObj,id,text) {
        var itemHTML = '<div><span class="buttonDeleteSmall removeClient" id="'+id+'"></span>'+text+'</div>';
		containerObj.append(itemHTML);

        $('#'+id).click(function() {
            $(this).parent().remove();
        });
    },
    attachCalendarHeaderButtonEvents : function() {
        $('#viewMonth').click(function() {
            $('#calendarContainer').fullCalendar( 'changeView', 'month' );
            supportCalendarObj.renderCalendarTitle();
            return false;
        });

        $('#viewWeek').click(function() {
            $('#calendarContainer').fullCalendar( 'changeView', 'agendaWeek' );
            supportCalendarObj.renderCalendarTitle();
            return false;
        });

        $('#viewDay').click(function() {
            $('#calendarContainer').fullCalendar( 'changeView', 'agendaDay' );
            supportCalendarObj.renderCalendarTitle();
            return false;
        });

        $('#viewPrev').click(function() {
            $('#calendarContainer').fullCalendar( 'prev' );
            supportCalendarObj.renderCalendarTitle();
            return false;
        });

        $('#viewNext').click(function() {
            $('#calendarContainer').fullCalendar( 'next' );
            supportCalendarObj.renderCalendarTitle();
            return false;
        });

        $('#viewToday').click(function() {
            $('#calendarContainer').fullCalendar( 'today' );
            supportCalendarObj.renderCalendarTitle();
            return false;
        });

        $('#monthSelect').change(function() {
            $('#calendarContainer').fullCalendar( 'gotoDate', $('#yearSelect').val(), $(this).val() );
            supportCalendarObj.renderCalendarTitle();
            return false;
		});
        
		$('#yearSelect').change(function() {
			$('#calendarContainer').fullCalendar( 'gotoDate', $(this).val(), $('#monthSelect').val() );
            supportCalendarObj.renderCalendarTitle();
            return false;
		});
    },
    renderCalendarTitle : function() {
        var view = $('#calendarContainer').fullCalendar('getView');
        $('#calTitle').html(view.title);
    },
    renderDateSelectBoxes : function() {
        $('#monthSelect').val($('#masterMonth').val());
        $('#yearSelect').val($('#masterYear').val());
    }
}

var getCalendarDataObj = {
	getEventsForCalendarMonth : function(dateStart,dateEnd) {
        $.ajax({
            type: "POST",
            url:  siteURL+'calendar/CalendarView/getEvents/'+dateStart+'/'+dateEnd+'/month',
            dataType: 'json',
            success: function(payload) {
                eventArray = payload;
                $.jMonthCalendar.ReplaceEventCollection(eventArray);
				initCalendarObj.attachEventPopups();
                var today = moment().format('YYYY-MM-DD');
				var events = $.jMonthCalendar.GetEventsForDate(today);
				supportCalendarObj.renderAgendaForToday(today,events);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    }
}

$(document).ready(function() {
    if ($('#pageLeftColumn').attr('pageID') == 'viewCalendar') {
		calendarObj.initCalendar();
        calendarObj.initFormControls();
        $('#calendarContainer').fullCalendar( 'today' );

        $('#feedExportContainer').toggle(true);

		$('#buttonprint').unbind('click').click(function() {
			var calendarIDString = '';
			$('#containerYourCalendars > p.colorBlock:not(.off)').each(function(){
				calendarIDString += $(this).attr('calid')+'-';
			});
            var yearPrint  = $('#yearSelect').val();
            var monthPrint = parseInt($('#monthSelect').val());
            monthPrint = monthPrint+1;
			var itemIDString = calendarIDString+'--'+yearPrint+'---'+monthPrint;
			printObj.goPrint(itemIDString,'Calendar');
			return false;
		});
	}
});