var getUserDataObj = {
	getUserInformation : function(userID) {
		//$('#userInformationContainer').show('fast');
		$.ajax({
		    type: "POST",
		    url:  siteURL+'administration/AdminUsers/getUserInformation',
		    data: 'accountUserid='+userID,
            dataType: 'json',
		    success: function(payload){
		    	renderUserObj.renderUserInformation(payload);
		    },
		    failure: function() {
		    	
		    }
	    });
	}
}

var updateUserObj = {
	toggleUserStatus : function(accountID) {
		$.ajax({
		    type: "POST",
		    url:  siteURL+'administration/AdminUsers/toggleUserStatus',
		    data: 'accountUserid='+accountID,
		    success: function(payload){
		    },
		    failure: function() {
		    	
		    }
	    });
	},
	deleteUser : function(userID) {
		
	}
}

var renderUserObj = {
	renderUserInformation : function(jsonObject) {
        var accountID  = jsonObject.AccountID;
		var userID     = jsonObject.Userid;
		var company    = jsonObject.Company;
		var name       = jsonObject.NameFirst+' '+jsonObject.NameLast;
		var address    = jsonObject.Address1;
		var city       = jsonObject.City;
		var state      = jsonObject.State;
		var zip        = jsonObject.Zip;
		var country    = jsonObject.Country;
		var email      = jsonObject.Email;
		var URL        = jsonObject.URL;
		var dateSignup = jsonObject.Signup_Date;
		var isActive   = jsonObject.IsActive;
		var projects   = jsonObject.Projects;
		var invoices   = jsonObject.Invoices;
		var clients    = jsonObject.Clients;
		var team       = jsonObject.Team;
		var lastLogin  = jsonObject.LastLogin;
		var logins     = jsonObject.Logins;
		var userName   = jsonObject.UserName;
        var planName   = jsonObject.PlanName;
        var paymentProfileID = jsonObject.PaymentProfileID;
        var datePaid         = jsonObject.DatePaid;
        var isPaid           = jsonObject.IsPaid;

        /*
         * Elipse user name
         */
        var userNameDisplay;
        if (userName.length>30) {
            userNameDisplay = userName.substr(0,30)+'...';
        } else {
            userNameDisplay = userName;
        }
		
		var userInfoString = '<p><h2>#'+accountID+' '+name+': '+planName+'</h2>';
		if (company != null && company != 'null') {
			userInfoString    += '<strong>'+company+'</strong><br />';
		}
		if (address != null && address != 'null') {
			userInfoString    += address+'<br />'+city+' '+state+' '+country+'<br />';
		}
		userInfoString    += '<a href="mailto:'+email+'" class="email">'+email+'</a><br />';
		if (URL != null) {
			userInfoString    += '<a href="'+URL+'" class="url" target="_blank">'+URL+'</a><br />';
		}

        if (isPaid == 1) {
            userInfoString += 'Paying customer on: '+datePaid+'<br />Payment Profile ID: '+paymentProfileID;
        } else {
            userInfoString += 'Not paying customer';
        }

        userInfoString += '</p>';
		var userStatsString  = '<table style="width: 100%;" cellspacing="4" cellpadding="4">';
		userStatsString += '<tr><td style="width: 30%;"><b>Projects:</b></td>';
		userStatsString += '<td style="width: 15%;">'+projects+'</td>';
		userStatsString += '<td style="width: 30%;"><b>Signup Date:</b></td>';
		userStatsString += '<td style="width: 20%;">'+dateSignup+'</td></tr>';
		
		userStatsString += '<tr><td style="width: 30%;"><b>Invoices:</b></td>';
		userStatsString += '<td style="width: 15%;">'+invoices+'</td>';
		userStatsString += '<td style="width: 30%;"><b>Last Login:</b></td>';
		userStatsString += '<td style="width: 20%;">'+lastLogin+'</td></tr>';
		
		userStatsString += '<tr><td style="width: 30%;"><b>Clients:</b></td>';
		userStatsString += '<td style="width: 15%;">'+clients+'</td>';
		userStatsString += '<td style="width: 30%;"><b>Number of Logins:</b></td>';
		userStatsString += '<td style="width: 20%;">'+logins+'</td></tr>';
		
		userStatsString += '<tr><td style="width: 30%;"><b>Team:</b></td>';
		userStatsString += '<td style="width: 15%;">'+team+'</td>';
		userStatsString += '<td style="width: 30%;"><b>User Name</b></td>';
		userStatsString += '<td style="width: 20%;" title="'+userName+'">'+userNameDisplay+'</td></tr>';
		userStatsString += '</table>';
		
        $('#userAccountID').val(accountID);
        $('#userUserid').val(userID);
        $('#userIsActive').val(isActive);
		
		renderUserObj.toggleActiveButton(isActive);
		$('#userInformation').html(userInfoString);
		$('#userStats').html(userStatsString);
		$('#loadingIndicator').systemMessage({ status: 'clearStatus' });
		$('#userInformationInner').show('fast');
				
		$('#buttonDeleteAccount').click(function() {
			updateUserObj.deleteUser(userID);
			return false;
		});

        $('#buttonLogin').click(function() {
            window.location = siteURL+'login/adminLoginToUserAccount/'+accountID;
        });
	},
	toggleActiveButton : function(status) {
		if (status == 1) {
            $('#buttonActive').show();
            $('#buttonInactive').hide();
		} else {
            $('#buttonActive').hide();
            $('#buttonInactive').show();
		}
	}
}

$(document).ready(function() {
	$("#sUserAutocompleter").autocomplete(siteURL+'administration/AdminUsers/searchUsers', {
		width:        400,
		highlight:    false,
		minChars:     3,
		scroll:       true,
		scrollHeight: 300,
		loadingClass: 'formFieldWait'
	});

	$('#sUserAutocompleter').keyup(function(e) {
		if(e.keyCode == 13) {
			getUserDataObj.getUserInformation($('#sUserID').val());
		}
		return false;
	});
	
	$("#sUserAutocompleter").result(function(event, data, formatted) {
		var userID = data[1];
		$('#sUserID').val(userID);	
		getUserDataObj.getUserInformation(userID);
		$('#userInformationInner').hide();
		$('#loadingIndicator').systemMessage({
			status:  'wait',
			noFade:  1,
			message: 'Loading user information...',
			size:    'Big'
		});
	});
	
	$('#textChangePassword').focus(function() {
		if ($(this).val() == 'Change password...') {
			$(this).val('');
		}
	});
	
	$('#buttonChangePassword').click(function() {
		var newPassword = $('#textChangePassword').val();
		$("#dialogPassword").dialog('open');
		return false;
	});

    $('.buttonStatus').click(function() {
        var userAccountID = $('#userAccountID').val();
        var userID = $('#userUserid').val();
        var userIsActive = $('#userIsActive').val();
        var newActiveStatus;

        if (userIsActive == 1) {
            newActiveStatus = 0;
        } else {
            newActiveStatus = 1;
        }
        $('#userIsActive').val(newActiveStatus);

        updateUserObj.toggleUserStatus(userAccountID);
		renderUserObj.toggleActiveButton(newActiveStatus);
		return false;
    });
	
	$("#dialogPassword").dialog({ 
	    modal:    true, 
	    autoOpen: false,
	    overlay: { 
	        opacity: 0.5, 
	        background: "black" 
	    },
	    buttons: { 
	        "Ok": function() { 
	            alert("Ok"); 
	            $(this).dialog("close");
	        }, 
	        "Cancel": function() { 
	            $(this).dialog("close"); 
	        } 
	    }		        
	});
	
});