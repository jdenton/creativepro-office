/*
 * Script from NETTUTS.com [by James Padolsey]
 * @requires jQuery($), jQuery UI & sortable/draggable UI modules
 */

var processedWidgetArray = [];
var dashboardOwners = {
    
    jQuery : $,
    
    settings : {
        columns :        '.column',
        widgetSelector:  '.widget',
        handleSelector:  '.widgetHead',
        contentSelector: '.widgetContent',
        controlSelector: '.widgetControls',
        widgetDefault : {
            movable:     true,
            removable:   true,
            collapsible: false,
            editable:    false,
            colorClasses : ['color-yellow', 'color-red', 'color-blue', 'color-white', 'color-orange', 'color-green']
        },
        widgetIndividual : {
        	/*
        	 * Individual widget settings go here
        	 */
            intro : {
                movable: false,
                removable: false,
                collapsible: false,
                editable: false
            }
        }
    },

    init : function () {
        this.addWidgetControls();
		$(this.settings.columns).sortable('destroy');
        this.makeSortable();
    },
    
    getWidgetSettings : function (id) {
        var $ = this.jQuery,
        settings = this.settings;
        return (id&&settings.widgetIndividual[id]) ? $.extend({},settings.widgetDefault,settings.widgetIndividual[id]) : settings.widgetDefault;
    },
    
    addWidgetControls : function () {
        var dashboard = this,
            $ = this.jQuery,
            settings = this.settings;
            
        $(settings.widgetSelector, $(settings.columns)).each(function () {
            var thisWidgetSettings = dashboard.getWidgetSettings(this.id);

            if (processedWidgetArray[this.id] != 1) {
				processedWidgetArray[this.id] = 1;
                
				if (thisWidgetSettings.removable) {
					$('<a href="#" class="remove" title="Close"></a>').mousedown(function(e){
						e.stopPropagation();
					}).unbind('click').click(function() {
						var widgetName = $(this).parents(settings.widgetSelector).attr('widgetName');
						var widgetId = $(this).parents(settings.widgetSelector).attr('id');
                        var idArray = widgetId.split('_');
						var wID = idArray[1];
                        processedWidgetArray[widgetId] = 0;
                        
						/*
						 * Toggle available widget list in popup
						 */
						$('#activate_' + widgetName).show();
						$('#active_' + widgetName).hide();
                        var widgetMenuAPI = $('#buttonaddDashboardWidgets').qtip('api');
						widgetMenuAPI.updateContent($('#widgetSelector').html());
						/*
						 * Remove widget from database
						 */
						$.ajax({
							type: "POST",
							url: siteURL + 'dashboard/DashboardOwners/removeWidget/'+wID+'/dashboard',
							success: function(msg){
							
							},
							error: function(xhr, desc, exceptionobj){
								errorObj.ajaxError(xhr, desc, exceptionobj);
							}
						});
						
						$(this).parents(settings.widgetSelector).animate({
							opacity: 0
						}, function(){
							$(this).wrap('<div/>').parent().slideUp(function(){
								$(this).remove();                                
							});
						});
						return false;
					}).appendTo($(settings.controlSelector, this));
				}
				
				if (thisWidgetSettings.editable) {
					$('<a href="#" class="edit" title="Edit"></a>').mousedown(function(e){
						e.stopPropagation();
					}).toggle(function(){
						$(this).css({
							backgroundPosition: '-66px 0',
							width: '55px'
						}).parents(settings.widgetSelector).find('.edit-box').show().find('input').focus();
						return false;
					}, function(){
						$(this).css({
							backgroundPosition: '',
							width: ''
						}).parents(settings.widgetSelector).find('.edit-box').hide();
						return false;
					}).appendTo($(settings.handleSelector, this));
					$('<div class="edit-box" style="display:none;"/>').append('<ul><li class="item"><label>Change the title?</label><input value="' + $('h3', this).text() + '"/></li>').append((function(){
						var colorList = '<li class="item"><label>Available colors:</label><ul class="colors">';
						$(thisWidgetSettings.colorClasses).each(function(){
							colorList += '<li class="' + this + '"/>';
						});
						return colorList + '</ul>';
					})()).append('</ul>').insertAfter($(settings.controlSelector, this));
				}
				
				if (thisWidgetSettings.collapsible) {
					$('<a href="#" class="collapsible" title="Collapse"></a>').mousedown(function(e){
						e.stopPropagation();
					}).toggle(function(){
						$(this).css({
							backgroundPosition: '0 0'
						}).parents(settings.widgetSelector).find(settings.contentSelector).slideUp();
						return false;
					}, function(){
						$(this).css({
							backgroundPosition: '0 -18px'
						}).parents(settings.widgetSelector).find(settings.contentSelector).slideDown();
						return false;
					}).prependTo($(settings.controlSelector, this));
				} else {
					$('<div style="display: inline-block; width: 19px;"></div>')
						.prependTo($(settings.controlSelector, this));
				}
				
			}		
        });		
        
        $('.edit-box').each(function () {
            $('input',this).keyup(function () {
                $(this).parents(settings.widgetSelector).find('h3').text( $(this).val().length>20 ? $(this).val().substr(0,20)+'...' : $(this).val() );
            });
            $('ul.colors li',this).click(function () {
                
                var colorStylePattern = /\bcolor-[\w]{1,}\b/,
                    thisWidgetColorClass = $(this).parents(settings.widgetSelector).attr('class').match(colorStylePattern)
                if (thisWidgetColorClass) {
                    $(this).parents(settings.widgetSelector)
                        .removeClass(thisWidgetColorClass[0])
                        .addClass($(this).attr('class').match(colorStylePattern)[0]);
                }
                return false;
                
            });
        });
        
    },
    
    attachStylesheet : function (href) {
        var $ = this.jQuery;
        return $('<link href="' + href + '" rel="stylesheet" type="text/css" />').appendTo('head');
    },
    
    makeSortable : function () {
        var dashboard = this,
        $ = this.jQuery,
        settings = this.settings,
        $sortableItems = (function () {
            var notSortable = '';
            var $column = $(settings.columns);
            $(settings.widgetSelector, $column).each(function (i) {
                if (!dashboard.getWidgetSettings(this.id).movable) {
                     if (!this.id) {
                          this.id = 'widget-no-id-' + i;
                     }
                     notSortable += '#' + this.id + ',';
                }
            });
            if (notSortable == '') {
                return $('> li', settings.columns);
            } else {
                return $('> li:not(' + notSortable + ')', settings.columns);
            }
        })();

        $(settings.columns).sortable({
            items: $sortableItems,
            connectWith: $(settings.columns),
            handle: settings.handleSelector,
            placeholder: 'widget-placeholder',
            forcePlaceholderSize: true,
            revert: 300,
            delay: 100,
            opacity: 0.8,
            containment: 'document',
            start: function (e,ui) {
				$(ui.helper).addClass('dragging');
            },
            stop: function (e,ui) {
                $(ui.item).css({width:''}).removeClass('dragging');
                $(settings.columns).sortable('enable');   
                
                /*
                 * Serialize widget positions to JSON and save.
                 */
                var jsonString = '{';
                for(a=1;a<=3;a++) {
                	jsonString += '"Column'+a+'":[';
					var d=0;
	                $('ul#column'+a).children().each(function(i) {
	                	var widget = $(this);
	                	jsonString += '{"WidgetID":"'+widget.attr('id')+'","Order":"'+i+'"},';
						d++;
	                });
					if (d>0) {
						jsonString = jsonString.substr(0,(jsonString.length-1)); // Remove trailing comma
					}
	                jsonString += '],';
                }    
                jsonString = jsonString.substr(0,(jsonString.length-1)); // Remove trailing comma
	            jsonString += '}';
                var formHash = {
                            jsonString : jsonString
                }
	            $.ajax({
				    type: "POST",
				    url:  siteURL+'dashboard/DashboardOwners/saveDashboardOrder',
				    data: formHash,
                    dataType: 'json',
				    success: function(payload){
				    	
				    },
				    error: function (xhr, desc, exceptionobj) {
						errorObj.ajaxError(xhr,desc,exceptionobj);
					}
			    });
            }
        });
    }
  
};