
var initMessageBox = {
    createMessageBox : function(toJSON,messageBoxContainerElement,messageListContainerElement,reply,fileAttach,clients,contacts,team,vendors,showToPanel) {
        var elementID = messageBoxContainerElement.attr('id');
        var itemType  = messageBoxContainerElement.attr('itemType');
        var itemID    = messageBoxContainerElement.attr('itemID');
        
        var guid = genericFunctions.createGUID();

        var sendToString = '';
        if (toJSON != '' && toJSON != null) {
            sendToString = initMessageBox.createMessageSendToElements(toJSON,showToPanel);
        }
        var messageBoxString = [];
            messageBoxString.push('<div class="messageFormContainer">');
            messageBoxString.push('<input type="hidden" class="guid" value="'+guid+'" />');
            messageBoxString.push('<textarea class="messageBox small">Leave a comment</textarea>');
                         
            messageBoxString.push('<div class="messageFormAssetContainer hide">');
            
            /*
             * Send to people
             */
            if (clients == true || contacts == true || team == true || vendors == true) {
                messageBoxString.push('<div class="messageFormAssetRow">');
                messageBoxString.push('<span class="icon_client_small icon"></span>');
                messageBoxString.push('<input type="text" class="messageToBox autocompleteContacts" style="width: 650px;" data-defaulttext="'+commonTerms['common_search_contacts']+'" tabindex="300" />');
                messageBoxString.push('<div style="clear: left;"></div>');
                messageBoxString.push('</div>');
            }    
            
            /*
             * File attach
             */
            if (fileAttach == true) {
                messageBoxString.push('<div class="messageFormAssetRow">');
                    messageBoxString.push('<span class="icon_attach icon"></span>');
                    messageBoxString.push('<div id="fileUploadingContainer2" class="dragDropUploadContainer">');
                        messageBoxString.push('<div id="dropFilesHere2" class="fileUploadBox small left" style="margin: 3px 0 0 0;">'+commonTerms['drop_files_here']+'</div>');
                    messageBoxString.push('</div>');
                    messageBoxString.push('<div style="clear: left;"></div>');
                    messageBoxString.push('<div id="msg_FilesContainer" class="messageFileContainer mTop12 mBottom12 pill"></div>');                
                    messageBoxString.push('<div style="clear: both;"></div>');
                messageBoxString.push('</div>');
            }    
            
            /*
             * Submit, Cancel buttons
             */
            messageBoxString.push('<div class="messageFormAssetRow" style="padding: 11px 0 0 32px;">');
                messageBoxString.push('<button class="smallButton submitMessage" style="float: left; margin-right: 12px;" tabindex="351"><span class="sendMessage">'+commonTerms['send']+'</span></button>');
                messageBoxString.push('<button class="smallButton cancelMessage" style="float: left; margin-right: 12px;" tabindex="352"><span class="cancel">'+commonTerms['close']+'</span></button>');
                messageBoxString.push('<div style="clear: left;"></div>');
            messageBoxString.push('</div>');    
            
            messageBoxString.push('</div>');
            messageBoxString.push('</div>');
                
        var messageBoxOutString = messageBoxString.join('');

        /*
         * Render the message box HTML
         */
        messageBoxContainerElement.html(messageBoxOutString);

        if (userType == USER_TYPE_CLIENT) {
            $('.recipData').each(function() {
                if ($(this).attr('reciptype') == 'owner') {
                    $(this).attr('checked',true);
                }
            });
        }
        initMessageBox.attachEventHandlers(messageBoxContainerElement,messageListContainerElement,clients,contacts,team,vendors,fileAttach);
    },
    createMessageSendToElements : function(toJSON,showToPanel) {
        var sendToOutArray = [];
        var toArray = [];
        
        if (typeof toJSON != 'object') {
            toArray = eval(toJSON);
        } else {
            toArray = toJSON;
        }
        if (toArray.length>0) {
            var tabindex = 301;
            for(var a=0;a<=(toArray.length-1);a++) {
                var toUID      = toArray[a].UID;
                var toPID      = toArray[a].PID;
                var toName     = toArray[a].NameFull;
                var toType     = toArray[a].Type;
                var toLanguage = toArray[a].Language;
                var randomID = Math.floor(Math.random()*200);

                if (showToPanel != false) {
                    sendToOutArray.push('<p class="messageRecipientContainer" style="margin: 0; padding: 0;" title="'+toName+'">');
                    sendToOutArray.push('<label class="normal" for="contactCheckbox_'+randomID+'">');
                    if (toType == 'client') {
                        sendToOutArray.push('<input type="checkbox" id="contactCheckbox_'+randomID+'" class="recipData" recipLanguage="'+toLanguage+'" recipType="'+toType+'" value="'+toUID+'" tabindex="'+tabindex+'" />');
                        sendToOutArray.push('<a href="#" class="clientLink" clientID="'+toPID+'">'+toName+'</a>');
                    } else {
                        sendToOutArray.push('<input type="checkbox" id="contactCheckbox_'+randomID+'" class="recipData" recipLanguage="'+toLanguage+'" recipType="'+toType+'" value="'+toPID+'" tabindex="'+tabindex+'" />');
                        sendToOutArray.push('<a href="#" class="personLink" personID="'+toPID+'">'+toName+'</a>');
                    }
                    sendToOutArray.push('</label></p>');
                } else {
                    if (toType == 'client') {
                        sendToOutArray.push('<input type="hidden" class="recipData" recipLanguage="'+toLanguage+'" recipType="'+toType+'" value="'+toUID+'" />');
                    } else {
                        sendToOutArray.push('<input type="hidden" class="recipData" recipLanguage="'+toLanguage+'" recipType="'+toType+'" value="'+toPID+'" />');
                    }
                }

                tabindex++;
            }
            var sendToOutString = sendToOutArray.join('');
            return sendToOutString;
        } else {
            return false;
        }
    },
    attachEventHandlers : function(messageContainerObj,messageListObj,clients,contacts,team,vendors,fileAttach) {
        var containerMessageForm = messageContainerObj.find('.messageFormContainer');
        var toBox      = messageContainerObj.find('.messageToBox');
        var messageBox = messageContainerObj.find('.messageBox');
        var assetContainer = messageContainerObj.find('.messageFormAssetContainer');

        var buttonSave    = messageContainerObj.find('.submitMessage');
        var buttonCancel  = messageContainerObj.find('.cancelMessage');
        var buttonFiles   = messageContainerObj.find('.fileUploadButton');
        var guid          = messageContainerObj.find('.guid').val();
        
        if (!$.browser.msie && fileAttach == true) {
            filesObj.initPluploadDragNDrop(0,'message',$('#msg_FilesContainer'),'dropFilesHere2','fileUploadingContainer2');
        }
        var fileContainer = messageContainerObj.find('.messageFileContainer');
        
        messageBox.bind('focus', function() {
            var txtBox = $(this);
            txtBox.removeClass('small').addClass('big');
            assetContainer.show();
            
            if (txtBox.val() == 'Leave a comment') {
                txtBox.val('');
            }
        });
        
        buttonSave.unbind('click').click(function() {
            updateMessageObj.saveMessage(messageContainerObj,messageListObj);
        });

        buttonCancel.unbind('click').click(function() {
            messageHelperObj.clearMessageForm(messageContainerObj);
            var fileLinkIDString = messageHelperObj.getMessageAttachedFiles(fileContainer);

            /*
             * Remove any attached files
             */
            updateFilesObj.removeAttachedFilesFromItem(fileLinkIDString);

            containerNewMessage.show();
            containerMessageForm.hide();
            return false;

        });

        /*
         * 'Send to' autocompleter
         */
        if (clients == true || contacts == true || team == true || vendors == true) {
            contactObj.makeContactAutoSuggest(toBox,clients,contacts,team,vendors);
        }    
        fileManagerObj.fileManagerButton();
    }    
}

var messageHelperObj = {
    clearMessageForm : function(messageContainerObj) {
        var messageBox = messageContainerObj.find('.messageBox');
        var fileContainer = messageContainerObj.find('.messageFileContainer');
        
        messageContainerObj.find('.as-selection-item').remove();
        messageContainerObj.find('.as-values').val('');
        
        messageBox.val('');
        fileContainer.html('');
    },
    getMessageAttachedFiles : function(fileContainer) {
        var fileLinkIDString = '';
        fileContainer.find('ul').children('li').each(function() {
           var fileNode = $(this);          
           var fileLinkID = fileNode.attr('linkid');
           fileLinkIDString += fileLinkID+'|';
        });

        return fileLinkIDString;
    }
}

var updateMessageObj = {
    saveMessage : function(messageContainerObj,messageListObj) {
        var elementID     = messageContainerObj.attr('id');
        var itemID        = messageContainerObj.attr('itemID');
        var itemType      = messageContainerObj.attr('itemType');
        var message       = messageContainerObj.find('.messageBox').val();
        var fileContainer = messageContainerObj.find('.messageFileContainer');
        var fromID    = numbers[0];
        var fromType  = numbers[3];

        /*
         * Creat our 'To' string
         */
        var toIDs = '', toType = '', toLanguage = '';
        var recipients = messageContainerObj.find('.as-values').val();
        
        if (undefined != recipients) {        
            var recipientArray = recipients.split(',');
            var individualArray = [];
            for(var a=0;a<=recipientArray.length-1;a++) {
                if (recipientArray[a] != '') {
                    individualArray = recipientArray[a].split('-');
                    toIDs      += individualArray[0]+'|';
                    toType     += individualArray[1]+'|';
                    toLanguage += individualArray[2]+'|';
                }    
            }
        } else {
            /*
             * Try looking for hidden recipient fields.
             */
            messageContainerObj.find('.recipData').each(function() {
                if ($(this).attr('type') == 'checkbox' && $(this).attr('checked') == true) {
                    toIDs      += $(this).val()+'|';
                    toType     += $(this).attr('recipType')+'|';
                    toLanguage += $(this).attr('recipLanguage')+'|';
                } else if ($(this).attr('type') == 'hidden') {
                    toIDs      += $(this).val()+'|';
                    toType     += $(this).attr('recipType')+'|';
                    toLanguage += $(this).attr('recipLanguage')+'|';
                }
            });
        }    

        /*
         * Remove trailing |
         */
        toIDs      = toIDs.slice(0, -1);
        toType     = toType.slice(0, -1);
        toLanguage = toLanguage.slice(0, -1);
        
        /*
         * Get attached files
         */
        var attachedFileString = messageHelperObj.getMessageAttachedFiles(fileContainer);

        /*
         * Now save the message
         */
        var formHash = {
                    toIDs      : toIDs,
                    toType     : toType,
                    toLanguage : toLanguage,
                    message    : message,
                    itemType   : itemType,
                    itemID     : itemID,
                    from       : fromID,
                    fromType   : fromType,
                    fileLinkIDString : attachedFileString
        }
        if (message != '') {
            $.ajax({
                type     : "POST",
                url      : siteURL+'messaging/Messages/saveMessage',
                data     : formHash,
                dataType : 'json',
                success: function(payload){
                    renderMessageObj.renderMessages(payload,'',messageListObj);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });

            messageHelperObj.clearMessageForm(messageContainerObj);
        }    
    }
}

var renderMessageObj = {
    renderMessages : function(messageJSON,renderType,renderContainerObj,returnString) {
        var jsonObject = messageJSON;
		if (jsonObject.Messages) {
			var numberMessages = jsonObject.Messages.length;
			var messages       = jsonObject.Messages;
            var totalMessages  = jsonObject.TotalMessages;
		}

        /*
         * Update message tab if there is one.
         */
        if (totalMessages>0) {
            $('#messageCount').html('('+totalMessages+')').removeClass('lightGray');
        }

        var mid,from,message,avatarLink,itemIcon,avatar;
		var messageString = [];
		if (numberMessages>0) {
			for(var a=0;a<=(numberMessages-1);a++) {
				if (messages[a].FromName) {
					from = messages[a].FromName;
				} else {
					from = messages[a].Company;
				}
				mid      = messages[a].MID;
				avatar   = messages[a].Avatar;
				itemIcon = messages[a].ItemIcon;
				message  = messages[a].Message;
                UIDFrom  = messages[a].UIDFrom;
                PIDFrom  = messages[a].PIDFrom;
				var sendDate = messages[a].DateSent;
                var files    = messages[a].Files;

                if (avatar == '') {
                    avatar = siteURL+'images/avatars/stockAvatar46.png';
                }

				if (renderType == 'alert') {
					avatarLink = '<img src="'+avatar+'" class="avatar22 left" />';
				} else {
					avatarLink = '<img src="'+avatar+'" class="avatar33 left" />';
				}
				messageString.push('<div class="messageContainer" id="'+mid+'">');
				messageString.push('<div class="oddRow" style="padding: 2px;">');
				messageString.push(avatarLink+'<div style="float: left;"><a href="#" class="personLink" personID="'+PIDFrom+'">'+from+'</a> <span class="subText">'+sendDate+'</span> '+itemIcon+'</div>');
				
                if (renderType == 'alert') {
                    messageString.push('<div style="float: right;"><a href="#" class="markMessageRead buttonCloseSmall" title="'+commonTerms['markMessageRead']+'" messageID="'+mid+'"></a></div>');
                }

                messageString.push('<div style="clear: both;"></div></div>');
				messageString.push('<p style="margin: 6px;">'+message+'</p>');
                if (files && files.length>0) {
                    var fileString = renderFileObj.renderFileListItems(files,null,true);

                    messageString.push('<div style="margin: 0 0 6px 12px;"><ul class="fileListBlock">'+fileString+'</ul></div>');
                }
                messageString.push('</div>');
			}
            var messageStringOut = messageString.join('');

			if (returnString == true) {
				return messageStringOut;
			} else {
				if (renderType == 'alert') {
					$('#buttonSiteMail').removeClass('iconMailInactive');
					$('#buttonSiteMail').addClass('iconMailActive');
					$('#buttonSiteMail span').html(numberMessages);
					messageData = messageStringOut;                    
				} else {
					renderContainerObj.prepend(messageStringOut);
					var messageCountRendered = renderContainerObj.children('.messageContainer').size();
				}
			}
            //contactObj.makeContactLinks();
		}
	},
    renderDiscussions : function(messageJSON,renderContainerObj) {
        
    }
}

var getMessageObj = {
    getDiscussions : function(itemID,itemType,renderContainerObj,toUserOnly) {
        if (start == '' || typeof start == 'undefined') {
			start = '0';
		}
        if (limit == '' || typeof limit == 'undefined') {
            limit = '0';
		}
        if (viewAll == '' || typeof viewAll == 'undefined') {
            viewAll = '1';
		}
        if (toUserOnly == '' || typeof toUserOnly == 'undefined') {
            toUserOnly = '0';
		}
		$.ajax({
		    type:  "POST",
            dataType: 'json',
            url:   siteURL+'messaging/Messages/getDiscussions/'+itemID+'/'+itemType+'/'+toUserOnly+'/json/0/',
		    success: function(payload){
				renderMessageObj.renderDiscussions(payload,renderContainerObj);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getMessages : function(itemID,itemType,renderContainerObj,start,limit,viewAll,toUserOnly) {
        if (start == '' || typeof start == 'undefined') {
			start = '0';
		}
        if (limit == '' || typeof limit == 'undefined') {
            limit = '0';
		}
        if (viewAll == '' || typeof viewAll == 'undefined') {
            viewAll = '1';
		}
        if (toUserOnly == '' || typeof toUserOnly == 'undefined') {
            toUserOnly = '0';
		}
		$.ajax({
		    type:  "POST",
            dataType: 'json',
            url:   siteURL+'messaging/Messages/getMessages/'+itemID+'/'+itemType+'/0/'+viewAll+'/'+toUserOnly+'/json/0/'+start+'/'+limit,
		    success: function(payload){
				renderMessageObj.renderMessages(payload,'normal',renderContainerObj);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
    pollMessages : function() {
        /*
         * This gets messages for our mail window in the lower right.
         */
		$.getJSON(siteURL+'messaging/Messages/getMessages/0/0/1/1/1/json/0', function(payload) {
			renderMessageObj.renderMessages(payload,'alert');
		});

        /*
         * This gets any unread message for our message growl widget.
         */
		$.ajax({
	        type: "POST",
	        url: siteURL+"messaging/Messages/getMessagesForUpdateWindow",
			dataType: 'json',
	        success: function(payload) {
				var messageString = renderMessageObj.renderMessages(payload,'normal',null,true);
				$("#popupGrowlWindow").notify("create", "basic-template", {
                    position: 'above',
					text: messageString
				});
	        },
	        error: function(xhr,msg,e){
	        }
	    });
	}
}