var formValidation = {
	formLoginValidation : function() {
		if($('#userid').val() == '') {
			var error = 1;
			var message = 'You must enter a user name.';
		}
		if($('#password').val() == '') {
			var error = 1;
			var message = 'You must enter a password.';
		}
		if (error != 1) {
			$('#formLogin').submit();
		} else {
			$('#messageContainer').systemMessage({
				noFade: 1,
        		message: message,
        		status:  'error'
        	});
		}
	},
	formFindPasswordValidation : function() {
		if($('#email').val() == '') {
			var error = 1;
			var message = 'You must enter an email address.';
		}
		if (error != 1) {
			$('#formFindPassword').submit();
		} else {
			$('#messageContainer').systemMessage({
				noFade: 1,
        		message: message,
        		status:  'error'
        	});
		}
	},
	formClientLoginValidation : function() {
		if($('#userid').val() == '') {
			var error = 1;
			var message = 'You must enter a user name.';
		}
		if (error != 1) {
			$('#formLoginClient').submit();
		} else {
			$('#messageContainer').systemMessage({
				noFade: 1,
        		message: message,
        		status:  'error'
        	});
		}
	}
}

$(document).ready(function() {
	if ($('#loginBox').attr('pageID') == 'clientLogin') {
          $('#buttonLoginClient').click(function() {
               formValidation.formClientLoginValidation();
               return false;
          });
     } else {
          $('#buttonLogin').click(function() {
               formValidation.formLoginValidation();
               return false;
          });
	
          $('#buttonFindPassword').click(function() {
               formValidation.formFindPasswordValidation();
               return false;
          });

          $('#linkFindPassword').click(function() {
               $('#formLogin').hide();
               $('#formFindPassword').show();
               var message = '<div class="iconChangePasswordSmall"><strong>Reset your password.</strong>&nbsp;';
               message +=    'Please enter your account email address, we will reset your password and email it to you.<br /><br />';
               message +=    '<span style="color: red; font-weight: bold;">If you have multiple accounts under 1 email address</span> the passwords for all accounts will be changed.&nbsp;';
               message +=    'Each account password will be unique and instructions will be sent to you telling you how to access each account.&nbsp;';
               message +=    'Please <a href="/contact">Contact Us</a> with any questions.</div>';
               $('#messageContainer').html(message);
               return false;
          });

          $('#buttonCancelFindPassword').click(function() {
               $('#formLogin').show();
               $('#formFindPassword').hide();
               $('#messageContainer').html('<div class="bigText">Welcome! Please login to your CreativePro Office account below.</div>');
          });
     }
	
	$('#userid').focus();
});