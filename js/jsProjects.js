var projectTaskTotal;
var templatesLoaded = false;
var archive = $('#archive').val();

var getProjectDataObj = {
    getProjectList : function(itemID,group,displayType,view,tag,status,archive) {
        if (tag == '' || typeof tag == 'undefined') {
            tag = '0';
        }
        if (status == '' || typeof status == 'undefined') {
            status = '0';
        }
        if (archive == '' || typeof archive == 'undefined') {
            archive = '0';
        }

        $.ajax({
                type:     "POST",
                url:      siteURL+'projects/ProjectView/getProjectList/'+itemID+'/'+group+'/'+tag+'/json/0/0/'+status+'/'+archive,
                dataType: 'json',
                success: function(payload){
                var jsonObject = payload;
                projectArray = jsonObject.Projects;
                if (displayType == 'list') {
                    renderProjectDataObj.renderProjectGrid(jsonObject);
                } else {
                    if (view == 'client') {
                        renderProjectDataObj.renderProjectsForClient();
                    } else if (view == 'owner') {
                        projectArray.sort(sortObj.sortByTitleAsc);
                        renderProjectDataObj.renderProjectsForOwner(projectArray);
                    }
                }

                /*
                 * Are we maxed out on projects?
                 */
                if ($('#maxProjects').val() == 1) {
                    supportProjectObj.showMaxProjectsModal();
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
    getProjectTemplates : function() {
        $.ajax({
		    type:     "POST",
		    url:      siteURL+'projects/ProjectTemplates/getProjectTemplates/json',
			dataType: 'json',
		    success: function(payload){
		    	renderProjectDataObj.renderTemplates(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
        });
    }
}

var renderProjectDataObj = {
    sortProjectsForUser : function(sortBy) {
		var acdc     = $('#acdc').val();
		var origSort = $('#origSort').val();

		if (origSort == sortBy) {
			/*
			 * Then switch acdc
			 */
			if (acdc == 'asc') {
				var newAcDc = 'desc';
			} else {
				var newAcDc = 'asc';
			}
		} else {
			var newAcDc = 'asc';
		}
		switch (sortBy) {
            case 'title':
				projectArray.sort(sortObj.sortByTitleAsc);
				break;
            case 'status':
				projectArray.sort(sortObj.sortByStatusAsc);
				break;
			case 'company':
				projectArray.sort(sortObj.sortByCompanyAsc);
				break;
			case 'invoice':
				projectArray.sort(sortObj.sortByInvoiceAmountDesc);
				break
			default:
				projectArray.sort(sortObj.sortByTitleAsc);
		}

		renderProjectDataObj.renderProjectsForOwner(projectArray);
	},
	renderProjectGrid : function(jsonObject) {
		var totalProjects = jsonObject.Projects.length;
		var projects      = jsonObject.Projects;
		var headerArray   = jsonObject.HeaderLabels.split('|');
		var totalHours    = jsonObject.TotalHours;
		var totalLabel    = jsonObject.FooterLabels;
		var projects      = jsonObject.Projects;
		var stringOut     = [];

        if (totalProjects<1) {
            $('#projectInformationContainer')
            .removeClass('activityMessageBig')
            .empty()
            .systemMessage({
                status: 'information',
                noFade:  1,
                size:    'Big',
                message: commonTerms['noProjects']
            });
        } else {
            stringOut.push('<input type="hidden" id="acdcProjectGrid" value="asc" />');

            stringOut.push('<table class="dataTable" id="projectGrid">');
            stringOut.push('<thead><tr>');
            stringOut.push('<th style="width: 50px;"></th>');
            stringOut.push('<th style="width: 200px;" acdc="1" class="sortable sort-alpha">'+headerArray[0]+'</th>');
            stringOut.push('<th style="width: 150px;" acdc="1" class="sortable sort-date">'+headerArray[1]+'</th>');
            stringOut.push('<th style="width: 100px;">'+headerArray[2]+'</th>');
            stringOut.push('<th style="width: 100px; text-align: right;" acdc="1" class="sortable sort-num">'+headerArray[3]+'</th>');
            stringOut.push('</tr></thead>');
            stringOut.push('<tbody>');
            for(a=0;a<=((projects.length)-1);a++) {
                var projID            = projects[a].ProjectID;
                var projTitle         = projects[a].Title;
                var projEndDate       = projects[a].Date_End;
                var projEndDateSort   = projects[a].Date_End_Sort;
                var projStatus        = projects[a].Status;
                var projStatusHuman   = projects[a].StatusHuman;
                var projStatusMachine = projects[a].StatusMachine;
                var projHours         = projects[a].ProjectTime;

                stringOut.push('<tr id="projRow'+projID+'" itemID="'+projID+'" class="projectRow">');
                stringOut.push('<td class="controls"></td>');
                stringOut.push('<td><a href="'+siteURL+'projects/ProjectDetail/index/'+projID+'" id="projectName_'+projID+'">'+projTitle+'</a></td>');
                stringOut.push('<td dateSort="'+projEndDateSort+'">'+projEndDate+'</td>');
                stringOut.push('<td style="text-align: center;">');
                stringOut.push('<span class="'+projStatusMachine+' statusProject" title="'+projStatusHuman+'" statusDesc="'+projStatusHuman+'"></span>');
                stringOut.push('</td>');
                stringOut.push('<td style="text-align: right;">'+projHours+'<a href="#" class="icon_timer_small_right projectTimer" state="stop" itemID="'+projID+'" title="'+commonTerms['timer_start']+'"></a></td>');
                stringOut.push('</tr>');
            }
            stringOut.push('</tbody>');
            stringOut.push('<tfoot><tr><td>'+totalLabel+'</td><td colspan="4" style="text-align: right;">'+totalHours+'</td></tr></tfoot></table>');

            /*
             * Create options menu
             */
            stringOut.push('<div id="optionsMenuProject" projectID="" class="tooltipMenuContainer"></div> ');
            var stringOutRender = stringOut.join('');
            $('#projectInformationContainer').html(stringOutRender);
            $('#projectGrid')
                .alternateTableRowColors()
                .hiliteTableRow()
                .tableSorter();
        
            /*
             * Attach timer control to clock icons
             */
            $('.projectTimer').click(function() {
                var projectID = $(this).attr('itemID');
                var timerState = $(this).attr('state');
                if (timerState == 'stop') {
                    var timerElementHash = {
                        elementID   : projectID,
                        projectName : $('#projectName_'+projectID).html(),
                        taskName    : '',
                        projectID   : projectID,
                        taskID      : 0,
                        timerRunning: 0,
                        elapsedTime : '00:00:00',
                        timeSheetID : '0'
                    }
                    widgetRenderObj.renderWidgetTimerElement(timerElementHash);
                    widgetToolsObj.widgetTimerToggle(projectID,'start');
                    $(this).attr('state','start');
                    $(this).removeClass('icon_timer_small_right').addClass('icon_timer_running_small_right');
                } else {
                    $(this).attr('state','stop');
                    widgetToolsObj.widgetTimerToggle(projectID,'stop');
                    $(this).removeClass('icon_timer_running_small_right').addClass('icon_timer_small_right');
                }

                return false;
            });

            /*
             * Create our row menus
             */
            $('#projectGrid').hoverButtons({
                   elementClass:     'projectRow',
                   insertionElement: 'controls',
                   editClass:        'projectUpdate',
                   deleteClass:      'projectDelete',
                   position:         '',
                   insertion:        'html',
                   width:            '70px'
              });
            
              /*
               * Attach event handlers
               */
              $('.buttonEdit').click(function() {
                   var projectID = $(this).attr('itemID');
                   window.location = siteURL+'projects/ProjectUpdate/index/'+projectID;
                   return false;
              });

              $('.buttonDelete').click(function() {
                   var projectID = $(this).attr('itemID');
                   $('#projRow'+projectID).fadeOut('slow');
                   updateTrashObj.sendToTrash('project',projectID);
                   return false;
              });
        }
	},
	renderProjectsForOwner : function(projectArray) {			
		var totalProjects = ((projectArray.length)-1);

        if (projectArray.length<1) {
            $('#projectListContainer')
            .removeClass('activityMessageBig')
            .empty()
            .systemMessage({
                status: 'information',
                noFade:  1,
                size:    'Big',
                message: commonTerms['noProjects']
            });
        } else {
            var stringOut = '<input type="hidden" id="acdc" value="asc" />';
            stringOut    += '<input type="hidden" id="origSort" value="company" />';
            for(a=0;a<=totalProjects;a++) {
                stringOut += renderProjectDataObj.getProjectRenderString(projectArray[a],'list');
            }
            $('#projectListContainer')
                .removeClass('activityMessageBig')
                .html(stringOut)
                .alternateContainerRowColors();

            /*
             * Make little edit/delete buttons (jquery.commonAppMethods.js)
             */
            if (archive == 0) {
                $('#projectListContainer').hoverButtons({
                    archiveOption: true,
                    elementClass:  'projectRow',
                    editClass:     'projectUpdate',
                    deleteClass:   'projectDelete',
                    archiveClass:  'projectUpdate',
                    width: '70px'
                });
            } else {
                $('#projectListContainer').hoverButtons({
                    openOption:    true,
                    elementClass:  'projectRow',
                    editClass:     'projectUpdate',
                    deleteClass:   'projectDelete',
                    archiveClass:  'projectUpdate',
                    width:        '70px'
                });
            }
		
            /*
             * Attach event handlers
             */
            $('.buttonEdit').click(function() {
                var projectID = $(this).attr('itemID');
                window.location = siteURL+'projects/ProjectUpdate/index/'+projectID;
                return false;
            }); 
            
            $('.buttonDelete').click(function() {
                var projectID = $(this).attr('itemID');
                $('#projRow'+projectID).fadeOut('slow');
                updateTrashObj.sendToTrash('project',projectID);
                return false;
            });

            $('.buttonArchive').click(function() {
                var projectID = $(this).attr('itemID');
                $('#projRow'+projectID).fadeOut('slow');
                supportProjectObj.updateArchive(projectID);
                return false;
            });

            $('.buttonOpen').click(function() {
                var projectID = $(this).attr('itemID');                
                supportProjectObj.updateArchive(projectID);
                return false;
            });

            $('.linkTasks').click(function(){
                /*
                 * Click over to ProjectDetail/tasks tab for this project
                 */
                var projectID = $(this).attr('projID');
                var linkURL = siteURL+'projects/ProjectDetail/index/'+projectID+'#windowTasks'
                window.location = linkURL;
                return false;
            });

            $('.linkInvoices').click(function(){
                /*
                 * Click over to invoice tab for this project
                 */
                var projectID = $(this).attr('projID');
                var linkURL = siteURL+'projects/ProjectDetail/index/'+projectID+'#windowInvoices'
                window.location = linkURL;
                $('#tabsProjectDashboard > ul').tabs('select', 1);
                return false;
            });

            $('.linkFiles').click(function(){
                /*
                 * Click over to files tab for this project
                 */
                var projectID = $(this).attr('projID');
                var linkURL = siteURL+'projects/ProjectDetail/index/'+projectID+'#windowFiles'
                window.location = linkURL;
                $('#tabsProjectDashboard > ul').tabs('select', 1);
                return false;
            });
            
            /*
             * Turn on our knob charts.
             */
            $('.knob').knob({
                angleOffset: -125,
                angleArc: 250,
                width: 70,
                height: 60,
                thickness: .3,
                readOnly: true,
                fgColor: '#76c0e6'
            });
            
            permissionsObj.renderControlsBasedOnPermissions();
            supportProjectObj.projectStatusHandler();
        }
	},
	renderProjectsForClient : function() {		
		var totalProjects = projectArray.length;
		var stringOut = '';
        if (totalProjects<1) {
            $('#projectInformationContainer')
            .removeClass('activityMessageBig')
            .empty()
            .systemMessage({
                status: 'information',
                noFade:  1,
                size:    'Big',
                message: commonTerms['noProjects']
            });
        } else {
            for(a=0;a<=(totalProjects-1);a++) {
                fileCount = parseInt(fileCount+parseInt(projectArray[a].FileTotal));
                stringOut += renderProjectDataObj.getProjectRenderString(projectArray[a],'list');
            }
            $('#projectInformationContainer')
                .removeClass('activityMessageBig')
                .html(stringOut)
                .alternateContainerRowColors();

            $('.linkInvoices').click(function(){
                clientTabs.tabs('select',1);
                return false;
            });

            $('.linkFiles').click(function(){
                clientTabs.tabs('select',3);
                return false;
            });

            $('#fileCount').html('('+fileCount+')').removeClass('lightGray');
        }
	},
	renderProjectInformation : function() {
		/*
		 * Get project data
		 * projectData is a JSON string generated by
		 * the ProjectDetail controler.
		 */
		var jsonObject = eval('('+projectData+')');
		var projectContacts = jsonObject.Contacts;

		var stringOut = renderProjectDataObj.getProjectRenderString(jsonObject,'single');
		$('#projectInformationContainer').html(stringOut);
        supportProjectObj.projectStatusHandler();
        
        $("#progressbar").progressbar({ value: 57 });

		/*
		 * Remove bottom border from container div
		 */
		$('#projectInformationContainer > div.row').css('border-bottom','none');

		/*
		 * Render any contacts we have for this project
		 */
		if (projectContacts.length>0) {
            var contactString = renderContactsObj.renderContacts(projectContacts,0);
		} else {
            /*
             * Turn on add contact link
             */
            $('#noContactLinkContainer').show();
        }

		/*
		 * Some event handlers
		 */
		$('.linkInvoices').click(function(){
			projectTabs.tabs('select',2);
			return false;
		});

		$('.linkTasks').click(function(){
			projectTabs.tabs('select',1);
			return false;
		});

        $('.linkFiles').click(function(){
            projectTabs.tabs('select',3);
            return false;
        });

		/*
		 * Update tab counts
		 */
		$('#taskCount').html('('+jsonObject.TaskTotal+')');
		$('#invoiceCount').html('('+jsonObject.InvoiceCount+')');

        /*
         * Class fileCount because we have multiple places where file counts exist
         */
		$('.fileCount').html('('+jsonObject.FileTotal+')');
        $('#messageCount').html('('+jsonObject.MessageTotal+')');

		$('.tabCount').each(function() {
			if ($(this).html() != '(0)') {
				$(this).removeClass('lightGray');
			}
		});

        projectTaskTotal = jsonObject.TaskTotal;
	},
	getProjectRenderString : function(projectObj,type) {
        var p = projectObj;
		var projID           = p.ProjectID;
		var projTitle        = p.Title;		
		var projDesc         = p.Description;
		var projDateStart    = p.Date_Start;
		var projDateEnd      = p.Date_End;
		var projDateRange    = p.Date_Range;
        var status           = p.Status;
		var projStatus       = p.StatusHuman;
		var projStatusClass  = p.StatusMachine;
		var projURL          = p.URL;
		var projInvoiceCount = p.InvoiceCount;
		var projInvoiceTotal = p.InvoiceTotal;
		var projInvoiceTotalHuman = p.InvoiceTotalHuman;
		var clientID         = p.ClientID;
		var clientCompany    = p.Company;
		var projTasks        = p.TaskTotal;
        var projTasksCompleted = p.TasksCompleted;
		var taskEstimatedTime= p.TaskEstimatedHours;
		var projFileCount    = p.FileTotal;
		if (taskEstimatedTime === null) {
			taskEstimatedTime = '0';
		}
		if (projURL.length>35) {
			var projURLDisplay = projURL.substr(0,35)+'...';
		} else {
			var projURLDisplay = projURL;
		}

        var projStatusColorClass = statusObj.getStatusBackground(projStatusClass);
		var stringOut = [];
		
		stringOut.push('<div class="row projectRow" style="padding: 6px;" id="projRow'+projID+'" itemID="'+projID+'">');
		if ($('#pageLeftColumn').attr('pageID') == 'clientDashboard') {
			stringOut.push('<h2>'+projTitle+'</h2>');
		} else if (type == 'single') {
            stringOut.push('<h2 id="projectTitle" class="icon_dropdown_small">'+projTitle+'</h2>');
            stringOut.push('<ul class="menu dropDownMenu">');
            stringOut.push('<li class="icon_invoice_small invoiceCreateSend invoiceCreateSendToAccountOwner"><a href="'+siteURL+'finances/InvoiceUpdate/index/0/'+clientID+'/'+projID+'">New invoice</a></li>');
            stringOut.push('<li class="icon_template_small projectCreate"><a href="#" id="lnkTemplate">Create Template</a></li></ul>');
        } else {
			stringOut.push('<h3><a href="'+siteURL+'projects/ProjectDetail/index/'+projID+'">'+projTitle+'</a></h3>');
		}
        if (clientCompany != '' && $('#pageLeftColumn').attr('pageID') != 'clientDashboard') {
            stringOut.push('<a href="'+siteURL+'clients/ClientDetail/index/'+clientID+'" class="grayLink-allCaps">'+clientCompany+'</a>');
        }
        stringOut.push('<div style="clear: left;"></div>');
		stringOut.push('<div style="float: left; width: 60%; margin: 6px 12px 0 0;"><p>'+projDesc+'&nbsp;</p>');
        
        /*
         * Charts...
         */
        var taskCompletePercent = 0;
        if (projTasksCompleted>0) {
            taskCompletePercent = parseInt(parseFloat(projTasksCompleted/projTasks)*100);
        } else {
            projTasksCompleted = 0;
        }
        
        // Budget spent (percent).        
        stringOut.push('<div class="projectKnobContainer"><input type="text" value="' + p.BudgetPercent + '" data-min="0" data-max="100" class="knob"><br /><span>Hi there</span></div>');
        // Tasks competed (percent)
        stringOut.push('<div class="projectKnobContainer"><input type="text" value="' + taskCompletePercent + '" data-min="0" data-max="100" class="knob"><br /><span>Hi there</span></div>');
        // Due invoices collected (percent). 
        // Number of due invoices collected / number of due invoices outstanding
        stringOut.push('<div class="projectKnobContainer"><input type="text" value="20" data-min="0" data-max="100" class="knob"><br /><span>Hi there</span></div>');
        
        stringOut.push('</div>');
        
		stringOut.push('<div style="float: left; width: 38%;">');
        stringOut.push('<ul class="iconList noBullet">');
        stringOut.push('<li class="'+projStatusClass+' clickable projectStatus colorBackground '+projStatusColorClass+' status" style="width: 200px; margin-bottom: 12px;" itemID="'+projID+'" status="'+status+'" title="'+commonTerms['status']+': '+projStatus+'">'+projStatus+'</li>');
        if (projTasks>0) {
            stringOut.push('<li class="taskView icon_tasks_small hide" title="'+commonTerms['tasks']+'"><a href="#" class="linkTasks" projID="'+projID+'">'+commonTerms['total']+': '+projTasks+', '+commonTerms['complete']+': '+projTasksCompleted+' ('+taskCompletePercent+'%)</a></li>');
		}

		stringOut.push('<li class="icon_calendar_small" title="'+commonTerms['projectDates']+': '+projDateRange+'">'+projDateRange+'</li>');
		if (projURL.length>2) {
			stringOut.push('<li class="icon_url"><a href="'+projURL+'" title="'+projTitle+'" target="_blank">'+projURLDisplay+'</a></li>');
		}
		if (projInvoiceCount>0) {
            stringOut.push('<li class="invoiceViewAll invoiceViewOwn hide icon_invoice_small">');
			stringOut.push('<a href="#" class="linkInvoices" projID="'+projID+'">');		
            stringOut.push(projInvoiceTotalHuman+' ('+projInvoiceCount+')');
            stringOut.push('</a>');
            stringOut.push('</li>');
        }
		if (projFileCount>0) {
			stringOut.push('<li class="icon_file_small" title="'+commonTerms['files']+'"><a href="#" class="linkFiles" projID="'+projID+'">'+commonTerms['files']+' <span class="fileCount">('+projFileCount+')</span></a></li>');
		}
		stringOut.push('</ul></div>');
		stringOut.push('<div style="clear: left;"></div>');
		stringOut.push('</div>');
		
		return stringOut.join('');
	},
    renderTemplates : function(jsonObject) {
        var totalTemplates = jsonObject.length;
        
        if (totalTemplates == 0) {
            $('#templateListContainer')
            .removeClass('activityMessageBig')
            .empty()
            .systemMessage({
                status: 'information',
                noFade:  1,
                size:    'Big',
                message: commonTerms['no_templates']
            });
        } else {
            templatesLoaded = true;
            var stringOut = [];
            
            for(var a=0;a<=totalTemplates-1;a++) {
                var templateID   = jsonObject[a].TemplateID;
                var title        = jsonObject[a].Title;
                var description  = jsonObject[a].Description;
                var projectTitle = jsonObject[a].ProjectTitle;
                var projectID    = jsonObject[a].ProjectID;
                var userNameFull = jsonObject[a].NameFirst+' '+jsonObject[a].NameLast;
                var pid          = jsonObject[a].PID;
                stringOut.push('<div class="row templateRow" style="padding: 6px;" id="templateRow'+templateID+'" itemID="'+templateID+'">');
                stringOut.push('<div style="float: left; width: 60%;"><h2>'+title+'</h2>');
                stringOut.push('<a href="'+siteURL+'projects/ProjectDetail/index/'+projectID+'" class="grayLink-allCaps">'+projectTitle+'</a>');
                stringOut.push('<p>'+commonTerms['created_by']+': <a href="#" class="personLink" personid="'+pid+'">'+userNameFull+'</a></p>');
                stringOut.push('<p style="margin-top: 12px;">'+description+'</p></div>');
                stringOut.push('<div style="float: left; margin-right: 12px;">');

                if (jsonObject[a].IncludeMilestones == 1) {
                    stringOut.push('<p>Milestones</p>');
                }
                if (jsonObject[a].IncludeTasks == 1) {
                    stringOut.push('<p>Tasks</p>');
                }
                if (jsonObject[a].IncludeTeam == 1) {
                    stringOut.push('<p>Team members</p>');
                }
                if (jsonObject[a].IncludeContacts == 1) {
                    stringOut.push('<p>Project contacts</p>');
                }

                stringOut.push('</div><div style="float: left;">')
                if (jsonObject[a].IncludeFiles == 1) {
                    stringOut.push('<p>Files</p>');
                }
                if (jsonObject[a].IncludeNotes == 1) {
                    stringOut.push('<p>Notes</p>');
                }
                if (jsonObject[a].IncludeExpenses == 1) {
                    stringOut.push('<p>Expenses</p>');
                }

                stringOut.push('</div><div style="clear: left;"></div>');
                stringOut.push('</div>');
            }
            var finalStringOut = stringOut.join('');
            $('#templateListContainer')
                .removeClass('activityMessageBig')
                .html(finalStringOut)
                .alternateContainerRowColors();

            $('#templateListContainer').hoverButtons({
                elementClass:  'templateRow',
                width:        '70px'
            });
            
            $('#templateListContainer .buttonEdit').click(function() {
                var templateID = $(this).attr('itemID');
                supportProjectObj.renderTemplateEditForm(templateID);
                return false;
            });

            $('#templateListContainer .buttonDelete').click(function() {
                var templateID = $(this).attr('itemID');
                $('#templateRow'+templateID).fadeOut('slow');
                $.getJSON(siteURL+'projects/ProjectTemplates/deleteTemplate/'+templateID, function(payload) {
                });
                return false;
            });

        }
    }
}

var supportProjectObj = {
    updateArchive : function(projectID) {
        $.ajax({
		    type:     "POST",
		    url:      siteURL+'projects/ProjectUpdate/updateArchive/'+projectID,
		    success: function(payload){
                if (payload == 'maxProjects') {
                    supportProjectObj.showMaxProjectsModal();
                } else {
                    $('#projRow'+projectID).fadeOut('slow');
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    checkForMaxProjects : function() {

    },
    showMaxProjectsModal : function() {
        $('#modalAccountLimit').dialog('open');
        $('#maxProjectsContainer').show();
        $('#maxTeamMembersContainer').hide();
        $('#maxFilesContainer').hide();
    },
    clearTemplateForm : function() {
        $('#chkMilestones').attr('checked',false);
        $('#chkTasks').attr('checked',false);
        $('#chkContacts').attr('checked',false);
        $('#chkTeam').attr('checked',false);
        $('#chkExpenses').attr('checked',false);
        $('#chkNotes').attr('checked',false);
        $('#chkFiles').attr('checked',false);
        $('#templateAction').val('add');
        $('#templateID').val('');
        $('#templateProjectID').val('');
        $('#templateName').val('');
        $('#templateDesc').val('');

    },
    projectStatusHandler : function() {
        $('.projectStatus').click(function() {
            var statusContainer = $(this);
            var currentProjectStatus = statusContainer.attr('status');
            var projectID = statusContainer.attr('itemid');
			$.get(siteURL+'projects/ProjectUpdate/updateProjectStatus/'+currentProjectStatus+'/'+projectID, function(payload) {
				var statusObject = eval('('+payload+')');
                var projStatusColorClass = statusObj.getStatusBackground(statusObject.StatusCSSClass);
                statusContainer
                    .removeClass()
                    .addClass(statusObject.StatusCSSClass)
                    .addClass('clickable')
                    .addClass('colorBackground')
                    .addClass(projStatusColorClass)
                    .addClass('status');

				statusContainer.attr('title',commonTerms['status']+': '+statusObject.StatusText);
                statusContainer.attr('status',statusObject.Status);
				statusContainer.html(statusObject.StatusText);
			});
			return false;
		});
    },
    renderTemplateEditForm : function(templateID) {
        supportProjectObj.clearTemplateForm();
        $('#projectTemplateDialog').dialog('open');
        $('#projectTemplateDialog').dialog( "option", "title", commonTerms['template_edit'] );
        
        $.ajax({
		    type:     "POST",
		    url:      siteURL+'projects/ProjectTemplates/getProjectTemplate/'+templateID,
			dataType: 'json',
		    success: function(payload){
                $('#templateAction').val('edit');
                $('#templateID').val(templateID);
                $('#templateProjectID').val(payload.ProjectID);
		    	$('#templateName').val(genericFunctions.txtToField(payload.Title));
                $('#templateDesc').val(genericFunctions.txtToField(payload.Description));
                if (payload.IncludeMilestones == '1') {
                    $('#chkMilestones').attr('checked',true);
                }
                if (payload.IncludeTasks == '1') {
                    $('#chkTasks').attr('checked',true);
                }
                if (payload.IncludeContacts == '1') {
                    $('#chkContacts').attr('checked',true);
                }
                if (payload.IncludeTeam == '1') {
                    $('#chkTeam').attr('checked',true);
                }
                if (payload.IncludeExpenses == '1') {
                    $('#chkExpenses').attr('checked',true);
                }
                if (payload.IncludeNotes == '1') {
                    $('#chkNotes').attr('checked',true);
                }
                if (payload.IncludeFiles == '1') {
                    $('#chkFiles').attr('checked',true);
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
        });
    },
    saveTemplate : function() {
        var chkMilestones  = 0;
        var chkTasks       = 0;
        var chkContacts    = 0;
        var chkTeamMembers = 0;
        var chkFiles       = 0;
        var chkExpenses    = 0;
        var chkNotes       = 0;
        if ($('#chkMilestones').is(':checked')) {
            chkMilestones = 1;
        }
        if ($('#chkTasks').is(':checked')) {
            chkTasks = 1;
        }
        if ($('#chkContacts').is(':checked')) {
            chkContacts = 1;
        }
        if ($('#chkTeamMembers').is(':checked')) {
            chkTeamMembers = 1;
        }
        if ($('#chkFiles').is(':checked')) {
            chkFiles = 1;
        }
        if ($('#chkExpenses').is(':checked')) {
            chkExpenses = 1;
        }
        if ($('#chkNotes').is(':checked')) {
            chkNotes = 1;
        }

        if ($('#templateName').val() == '') {
            $('#templateResponseContainer').addClass('errorMessageBig').html('Please enter a template name.');
        } else {
            var formHash = {
                    action         : $('#templateAction').val(),
                    templateID     : $('#templateID').val(),
                    projectID      : $('#templateProjectID').val(),
                    templateName   : $('#templateName').val(),
                    templateDesc   : $('#templateDesc').val(),
                    chkMilestones  : chkMilestones,
                    chkTasks       : chkTasks,
                    chkContacts    : chkContacts,
                    chkTeamMembers : chkTeamMembers,
                    chkFiles       : chkFiles,
                    chkExpenses    : chkExpenses,
                    chkNotes       : chkNotes
            }
            $.ajax({
                type: "POST",
                url:  siteURL+'projects/ProjectTemplates/saveProjectTemplate',
                data: formHash,
                dataType: 'json',
                success: function(payload) {
                    if (payload.Status == 'success') {
                        $('#templateResponseContainer').removeClass().addClass('successMessageBig').html(payload.Message);
                        $('#templateResponseContainer').fadeOut(3000,function() {
                            $('#projectTemplateDialog').dialog('close');
                        });
                        if ($('#pageLeftColumn').attr('pageID') == 'viewAllProjects') {
                            getProjectDataObj.getProjectTemplates();
                        }
                    } else {
                        $('#templateResponseContainer').addClass('errorMessageBig').html(payload.Message);
                    }
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    }
}

var initProjectObj = {
	initProjectForm : function() {
		/*
		 * Prevent form submit on client field ENTER
		 */
		$('#clientID').bind("keypress", function(e) {
             if (e.keyCode == 13) {
                 return false;
            }
         });

         $('#buttonCancel').click(function() {
            if ($('#projectID').val()>0) {
                var projectID = $('#projectID').val();
                window.location = siteURL+'projects/ProjectDetail/index/'+projectID;
            } else {
                window.location = siteURL+'projects/ProjectView';
            }
            return false;
         });
				
		/*
		 * Init date picker
		 */
		$(".dateField").datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});
		
		$('textarea.expanding').elastic();
		
		/*
		 * Init client select combobox
		 */
        $('#clientID').autocomplete({
            source: clients,
            select: function(event, ui) {
                var clientID = ui.item.id;
                ui.item.value = genericFunctions.txtToField(ui.item.value);
                $('#clientIDHolder').val(clientID);
           }
        });
        
        /*
		 * Category and Tag edit boxes
		 */
		$('#projectCategorySelect').combobox({
           source: categoryJSON, 
           select: function(event, ui) {
                var categoryID = ui.item.id;
                $('#projectCategory').val(categoryID);
           },
           width: '374px',
           tabindex: 7,
           id: 'projectCategoryCombo',
           name: 'projectCategoryCombo'
        });
        
        /*
		 * Init tag field
		 */
        var tagBox = $('#projectTags').autoSuggest(
            tagJSON,
            {
                selectedItemProp: 'value',
                searchObjProps: 'value',
                resultsHighlight: false,
                neverSubmit: true,
                boxWidth: '400px',
                startText: '',
                valuesHolderName: 'projectTagsHolder',
                preFill: $('#tagHolder').val()
            }
        );

        /*
		 * Init validation
		 */
		$("#formProject").validate({
			errorClass: 'formError',
			errorElement: 'div',
			rules: {
				clientID:     "required",
				projectTitle: "required"
			},
			messages: validationMessages
		});
		
        /*
         * Project rate stuff
         */
        $('#projectRateAmount').focus(function() {
            if ($(this).val() == commonTerms['amount']) {
                $(this).val('');
            }
        });
        $('#projectRateDesc').focus(function() {
            if ($(this).val() == commonTerms['description']) {
                $(this).val('');
            }
        });
	},
	initProjectView : function() {
		$('#tabsProject').tabs({ 
            fx: {opacity: 'toggle'},
            show: function(event,ui) {
				var panel = ui.panel.id;
                if (panel == 'windowViewProjects') {
                } else if (panel == 'windowMessages') {
                } else if (panel == 'windowTemplates') {
                    if (templatesLoaded == false) {
                        getProjectDataObj.getProjectTemplates();
                    }
                }
            }
        });

        /*
         * Init messaging stuff
         */
        initMessageBox.createMessageBox(null,$('#newMessage'),$('#messageListProject'),false,true,true,true,true,true);

        /*
         * Go get messages for all projects.
         * We are not stuffing messages into an array because they can
         * change i.e. a message may be received while viewing the page.
         */
        getMessageObj.getMessages(null,'project',$('#messageListProject'),0,10,1,0);
				
		/*
		 * Prepare autocompleter for project search
		 */
        $("#sProjectAutocompleter").autocomplete({
            source: siteURL+'projects/ProjectView/searchProjects',
            select: function(event, ui) {
                var projectID = ui.item.id;
                $('#sProjectID').val(projectID);
                window.location = siteURL+'projects/ProjectDetail/index/'+projectID;
           }
        });
		
		$('#selectProjectSort').change(function() {
			var sortBy = $('#selectProjectSort').val();
			renderProjectDataObj.sortProjectsForUser(sortBy);
			return false;
		});
		
		/*
		 * Load default data : projects for this user and tag
		 */
		var tag = $('#tag').val();
		getProjectDataObj.getProjectList(0,'user',0,'owner',tag);

        $('#buttonadd').click(function() {
           window.location = siteURL+'projects/ProjectUpdate';
        });

        /*
		 * Update tab counts
		 */
        var messageCount = $('#numberOfMessages').val();

        if (messageCount>0) {
            $('#messageCount').html('('+messageCount+')');
        }

		$('.tabCount').each(function() {
			if ($(this).html() != '(0)') {
				$(this).removeClass('lightGray');
			}
		});

        $('#buttonArchivedOpen').click(function() {
            initProjectObj.initArchiveOpenButton();
            return false;
        });        
	},
    initArchiveOpenButton : function() {
        if (archive == 1) {
            archive = 0;
            $('#archive').val('0');
            $('#buttonArchivedOpenLabel')
                .removeClass()
                .addClass('archive')
                .html(commonTerms['project_view_archived']);
            getProjectDataObj.getProjectList(0,'user',0,'owner',0,0,0);
        } else {
            archive = 1;
            $('#archive').val('1');
            $('#buttonArchivedOpenLabel')
                .removeClass()
                .addClass('open')
                .html(commonTerms['project_view_open']);
            getProjectDataObj.getProjectList(0,'user',0,'owner',0,0,1);
        }
    },
    initProjectDetails : function() {
        var projectID = $('#projectID').val();
        $('#templateProjectID').val(projectID);

		projectTabs = $('#tabsProject').tabs({
			fx: {opacity: 'toggle'},
			show: function(event,ui) {
				var panel = ui.panel.id;
                if (panel == 'windowProjectDetails') {
                    $('#tagsContainer').show();
                    genericFunctions.togglePageHeaderButtons('.buttonEditMain','on');
                    genericFunctions.togglePageHeaderButtons('.buttonDeleteMain','on');
                    genericFunctions.togglePageHeaderButtons('.buttonPrint','off');
                    genericFunctions.togglePageHeaderButtons('.buttonExportExcel','off');

                    $('#buttonprint').unbind('click').click(function() {
                        var projectID = $('#projectID').val();
                        printObj.goPrint(projectID,'Project');
                        return false;
                    });
					$('#sidebarTasks').hide();
                } else if (panel == 'windowInvoices') {
                    appSection = 'projectInvoices';

					if (invoiceArray.length == 0) {
                        initFinanceObj.attachInvoiceEventHandlers();
						getFinanceDataObj.getInvoiceList(projectID,'project');
					}
					$('#sidebarTasks').hide();
				} else if (panel == 'windowTasks') {
					$('#sidebarTasks').show();
					renderTaskDataObj.toggleTaskFilter();
					
					if (taskArray.length == 0) {
						/*
						 * Go get tasks for this project
						 */
                        getTaskDataObj.getTasks(projectID);

                        genericFunctions.togglePageHeaderButtons('.buttonEditMain','off');
                        genericFunctions.togglePageHeaderButtons('.buttonDeleteMain','off');
                        genericFunctions.togglePageHeaderButtons('.buttonPrint','on');
                        genericFunctions.togglePageHeaderButtons('.buttonExportExcel','on');
                        initTaskObj.initTaskSearchSort();

						$('#buttonprint').unbind('click').click(function() {
							var seeOnlyMyTasks = 0,seeOnlyCompleteTasks = 0,seeOnlyIncompleteTasks = 0;
							var dateStart              = $('#dateStart').val();
							var dateEnd                = $('#dateEnd').val();
							var userID                 = $('#userID').val();
							var projectID              = $('#projectID').val();
							var milestoneID            = $('#selectedMilestoneID').val();
							if ($('#chkViewMyTasks').is(':checked')) {
								seeOnlyMyTasks = 1;
							}
							if ($('#chkViewCompletedTasks').is(':checked')) {
								seeOnlyCompleteTasks = 1;
							}
							if ($('#chkViewIncompleteTasks').is(':checked')) {
								seeOnlyIncompleteTasks = 1;
							}
							var itemIDString = projectID+'---'+milestoneID+'---'+dateStart+'---'+dateEnd+'---'+userID+'---'+seeOnlyMyTasks+'---'+seeOnlyCompleteTasks+'---'+seeOnlyIncompleteTasks;
                            printObj.goPrint(itemIDString,'Tasks');
                            return false;
                        });
                        $('#buttonexportExcel').unbind('click').click(function(){
							var seeOnlyMyTasks = 0,seeOnlyCompleteTasks = 0,seeOnlyIncompleteTasks = 0;
							var dateStart              = $('#dateStart').val();
							var dateEnd                = $('#dateEnd').val();
							var userID                 = $('#userID').val();
							var projectID              = $('#projectID').val();
							var milestoneID            = $('#selectedMilestoneID').val();
							if ($('#chkViewMyTasks').is(':checked')) {
								seeOnlyMyTasks = 1;
							}
							if ($('#chkViewCompletedTasks').is(':checked')) {
								seeOnlyCompleteTasks = 1;
							}
							if ($('#chkViewIncompleteTasks').is(':checked')) {
								seeOnlyIncompleteTasks = 1;
							}
							var itemIDString = projectID+'---'+milestoneID+'---'+dateStart+'---'+dateEnd+'---'+userID+'---'+seeOnlyMyTasks+'---'+seeOnlyCompleteTasks+'---'+seeOnlyIncompleteTasks;
                            exportObj.exportExcel(itemIDString,'Tasks','ExportExcel');
                            return false;
                        });

                        $('#tagsContainer').hide();
					}
				} else if (panel == 'windowFiles') {
                    $('#tagsContainer').show();
					$('#sidebarTasks').hide();
					if (fileArray.length == 0) {
                        genericFunctions.togglePageHeaderButtons('.buttonEditMain','off');
                        genericFunctions.togglePageHeaderButtons('.buttonDeleteMain','off');
                        genericFunctions.togglePageHeaderButtons('.buttonPrint','off');
                        genericFunctions.togglePageHeaderButtons('.buttonExportExcel','off');
						/*
						 * Go get files for this project
						 */
                        attachedFilesView = true;
						getFileDataObj.getFilesForSomething(projectID,'project','#project_'+projectID+'_fileContainer');
					}
				} else if (panel == 'windowMessages') {
                    $('#tagsContainer').show();
					$('#sidebarTasks').hide();
                    genericFunctions.togglePageHeaderButtons('.buttonEditMain','off');
                    genericFunctions.togglePageHeaderButtons('.buttonDeleteMain','off');
                    genericFunctions.togglePageHeaderButtons('.buttonPrint','off');
                    genericFunctions.togglePageHeaderButtons('.buttonExportExcel','off');

					/*
					 * Go get messages for this project.
					 * We are not stuffing messages into an array because they can
					 * change i.e. a message may be received while viewing the page.
					 */
                    $('#messageListProject').html('');
					getMessageObj.getMessages(projectID,'project',$('#messageListProject'),0,10,1,0);
				} else if (panel == 'windowNotes') {
                    
                }
			}
		});

		$('#buttonNewMilestone').click(function() {
			initTaskObj.renderMilestoneForm(0);
		});
		
		$('#linkFileUpload').click(function() {
			$('#modalFileUpload').dialog('open');
			return false;
		});

        $('#buttonprint').attr('printWhat','Project');

        /*
         * Attach page event handlers
		 */
        $('#buttonedit').click(function() {
            window.location = siteURL+'projects/ProjectUpdate/index/'+projectID;
        });
        $('#buttondelete').click(function() {
            updateTrashObj.sendToTrash('project',projectID);
            $('#projRow'+projectID).fadeOut('slow');
            window.location = siteURL+'projects/ProjectView/deleted';
        });

		/*
		 * Get project information
		 */
		renderProjectDataObj.renderProjectInformation();

		/*
		 * Init contacts stuff
		 */
        helperContactsObj.initContactsContainer();
		helperContactsObj.initContactsForm('C');

        contactObj.makeContactAutoSuggest($('#sContactsAutocompleter'),true,true,true,true,true,'P',projectID);

        /*
         * Init messaging stuff
         */
        var jsonObject = eval('('+projectData+')');
        var messageTo = jsonObject.MessageRecipients;
        initMessageBox.createMessageBox(messageTo,$('#newMessage'),$('#messageListProject'),false,true,false,true,true,true);
        initMessageBox.createMessageBox(messageTo,$('#newMessageFile'),$('#messageListFile'),false,false,false,true,true,true);

        permissionsObj.renderControlsBasedOnPermissions();

        /*
         * Get notes for this project
         */
        getNotesObj.getNotes(0,projectID,'P',0,20,'noteListContainer');

        /*
         * Attach task event delegates
         */
        renderTaskDataObj.attachTaskContainerEventHandlers();

        $('#lnkTemplate').click(function() {
           $('#projectTemplateDialog').dialog('open');
           return false;
        });
        
        /*
         * Notifications
         */
        appActivity.getAppActivity(projectID, "#notifications");
        setInterval('appActivity.getAppActivity(' + projectID + ', "#notifications", 1)',30000);
        
        
        $('.buttonDelete').hide();
	}
}

$(document).ready(function() {
    if ($('#pageLeftColumn').attr('pageID') == 'updateProject') {
        initProjectObj.initProjectForm();
    } else if ($('#pageLeftColumn').attr('pageID') == 'viewAllProjects') {
        initProjectObj.initProjectView();
    } else if ($('#pageLeftColumn').attr('pageID') == 'viewProjectDetails') {
        initProjectObj.initProjectDetails();
    }

    $('#projectTitle').createContextMenu({
        menuElement: '.menu'
    });
    
    $('#projectTemplateDialog').dialog({
        autoOpen:      false,
        closeOnEscape: true,
        width:         '700px',
        position:      'center',
        resizable:     false,
        draggable:     false,
        zIndex:        1000,
        open:          function(event,ui) {}
    });

    $('#buttonTemplateModalCancel').click(function() {
       $('#projectTemplateDialog').dialog('close');
       return false;
    });

    $('#buttonTemplateModalSave').click(function() {
        supportProjectObj.saveTemplate();
        return false;
    });
});