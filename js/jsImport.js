var importUploadObj = {
    uploadImportClientFile : function() {
	    var buttonClientUpload = $('#buttonClientUpload'), interval2;
        new Ajax_upload(buttonClientUpload, {
			action:   siteURL+'import/Import/uploadImportFile',
			name:     'Filedata',
			onSubmit: function(file, ext){
                $('#buttonClientUpload .buttonDecorator').removeClass('upload').addClass('busy');
                this.disable();
                interval2 = window.setInterval(function() {}, 200);
			},
			onComplete: function(file, payload) {
				 $('#buttonClientUpload .buttonDecorator').removeClass('busy').addClass('upload');
				 window.clearInterval(interval2);
				 this.enable();
                 var jsonObj = eval('('+payload+')');
                 if (jsonObj.Result == 'success') {
                     var fileName = jsonObj.Filename;
                     window.location = siteURL+'import/Import/mapFieldsCSV/clients';
                 } else if (jsonObj.Result == 'error') {
                     $('#uploadErrorMessageClients').html(jsonObj.Message).addClass('errorMessageSmall');
                 }
			}
	   });
	},
    uploadImportContactFile : function() {
	    var buttonContactUpload = $('#buttonContactUpload'), interval2;
        new Ajax_upload(buttonContactUpload, {
			action:   siteURL+'import/Import/uploadImportFile',
			name:     'Filedata',
			onSubmit: function(file, ext){
                $('#buttonContactUpload .buttonDecorator').removeClass('upload').addClass('busy');
                this.disable();
                interval2 = window.setInterval(function() {}, 200);
			},
			onComplete: function(file, payload) {
				 $('#buttonContactUpload .buttonDecorator').removeClass('busy').addClass('upload');
				 window.clearInterval(interval2);
				 this.enable();
                 var jsonObj = eval('('+payload+')');
                 if (jsonObj.Result == 'success') {
                     var fileName = jsonObj.Filename;
                     window.location = siteURL+'import/Import/mapFieldsCSV/contacts';
                 } else if (jsonObj.Result == 'error') {
                     $('#uploadErrorMessageContacts').html(jsonObj.Message).addClass('errorMessageSmall');
                 }
			}
	   });
	}
}

var importFormsInitObj = {
    initImportFormHandlers : function() {
        $('#buttonImportMapping').click(function() {
            $('#formMapFields').submit();
        });

        $('#buttonCancelImportMapping').click(function() {
            window.location = siteURL+'import/Import';
            return false;
        });

        $('#buttonConfirmImport').click(function() {
            window.location = siteURL+'import/Import/index/success';
        });

        $('#buttonDeleteImport').click(function() {
            var importType = $('#importType').val();
            window.location = siteURL+'import/Import/rollBackImport/'+importType;
        });
    },
    initImportForm : function() {
        $('#mapTable').hiliteTableRow();
        $('#confirmTable').hiliteTableRow();
    }
}

$(document).ready(function() {
    if ($('#pageLeftColumn').attr('pageID') == 'importFileUpload') {
        importUploadObj.uploadImportClientFile();
        importUploadObj.uploadImportContactFile();
    }
    importFormsInitObj.initImportFormHandlers();
    importFormsInitObj.initImportForm();
});

