var tabsBugs;

var getBugDataObj = {
	getBugs : function(open,closed) {
        $.ajax({
		    type: "POST",
		    url:  siteURL+'bugs/Bugs/getBugs/'+open+'/'+closed+'/json/0',
		    dataType: 'json',
		    success: function(payload){
		    	renderBugObj.renderBugGrid(payload,open,closed);
		    },
		    failure: function() {

		    }
	    });
	}
}

var supportBugObj = {
    calculateBugStats : function() {
        var openBugs = $('tr.open').length;
        var closedBugs = $('tr.closed').length;
        $('#bugCountOpen').html('('+openBugs+')');
        $('#bugCountClosed').html('('+closedBugs+')');
    }
}

var renderBugObj = {
    renderBugGrid : function(jsonObject,open,closed) {
        var totalBugs        = jsonObject.length;
        var stringOut = [];

        var renderObj = $('#bugListContainerOpen');
        var noMessage = 'There are no open bugs right now! Yeah!';
        var bugState  = 'open';
        if (closed == 1) {
            renderObj = $('#bugListContainerClosed');
            noMessage = 'There are no closed bugs right now! Boooo get to work, slacker!';
            bugState  = 'closed';
        }

        if (totalBugs<1) {
            renderObj
            .removeClass('activityMessageBig')
            .empty()
            .systemMessage({
                status: 'information',
                noFade:  1,
                size:    'Big',
                message: noMessage
            });
        } else {
            stringOut.push('<input type="hidden" class="acdcBugGrid" value="asc" />');

            stringOut.push('<table class="dataTable bugGrid" id="bugGrid">');
            stringOut.push('<thead><tr>');
            stringOut.push('<th style="width: 50px;"></th>');
            stringOut.push('<th style="width: 350px;" acdc="1">Description</th>');
            stringOut.push('<th style="width: 110px;" acdc="1" class="sortable sort-alpha">Submitter</th>');
            stringOut.push('<th style="width: 80px;" acdc="1" class="sortable sort-date">Date</th>');
            stringOut.push('</tr></thead>');
            stringOut.push('<tbody>');
            for(a=0;a<=((jsonObject.length)-1);a++) {
                var bugSubmitEmail = '', bugSubmitEmailDisplay;
                var bugID   = jsonObject[a].BID;
                var bugDesc = '<strong>#'+bugID+'</strong> &nbsp;'+jsonObject[a].BugText;
                var bugDateEntrySort = jsonObject[a].BugDateEntrySort;
                var bugDateEntry     = jsonObject[a].BugDateEntry;
                var bugDateClosed    = jsonObject[a].BugDateClose;
                var bugSubmitName    = jsonObject[a].NameFirst+' '+jsonObject[a].NameLast;
                var bugSubmitCo      = jsonObject[a].Company;
                var bugPlatform      = jsonObject[a].UserAgent+' '+jsonObject[a].UserAgentVersion+' '+jsonObject[a].Platform;
                var bugPage          = jsonObject[a].BugPage;
                if (jsonObject[a].Email != null) {
                    bugSubmitEmail   = jsonObject[a].Email;
                }
                var bugSubmitAccountID = jsonObject[a].AccountID;

                /*
                 * Elipse bugSubmitEmail
                 */

                if (bugSubmitEmail.length>20) {
                    bugSubmitEmailDisplay = bugSubmitEmail.substr(0,20)+'...';
                } else {
                    bugSubmitEmailDisplay = bugSubmitEmail;
                }

                if (closed == 1) {
                    var closer = jsonObject[a].Closer;
                }

                bugDesc += '<br /><br /><strong>Platform:</strong> '+bugPlatform+'<br /><strong>Page:</strong> '+bugPage;
                stringOut.push('<tr id="bugRow'+bugID+'" itemID="'+bugID+'" class="bugRow '+bugState+'">');
                stringOut.push('<td class="controls"></td>');
                stringOut.push('<td>');
                stringOut.push('<div id="rewordedTextContainer_'+bugID+'" class="hide" style="margin-bottom: 12px;"><textarea id="rewordedText_'+bugID+'" style="width: 320px; height: 80px;"></textarea>');
                stringOut.push('<button style="float: left;" class="buttonExpand blueGray textSubmit" id="buttonSave" itemID="'+bugID+'"><span class="buttonOuterSpan blueGray"><span class="buttonDecorator save">Save Comment</span></span></button>');
                stringOut.push('<div style="float: left;"><input type="radio" name="radioType" value="1" id="typeBug" checked /> Bug ');
                stringOut.push('<input type="radio" name="radioType" value="1" id="typeFeature" /> Feature ');
                stringOut.push('<input type="radio" name="radioType" value="1" id="typeUpdate" /> Update </div>');
                stringOut.push('<div style="clear: left;"></div></div>'+bugDesc+'</td>');
                stringOut.push('<td><a href="'+siteURL+'login/adminLoginToUserAccount/'+bugSubmitAccountID+'">'+bugSubmitName+'</a><br />'+bugSubmitCo+'<br /><span class="icon_email"><a href="mailto:'+bugSubmitEmail+'">'+bugSubmitEmailDisplay+'</a></span></td>');
                stringOut.push('<td dateSort="'+bugDateEntrySort+'">'+bugDateEntry);
                if (closed == 1) {
                    stringOut.push('<br /><span style="color: green;"><strong>'+bugDateClosed+' by '+closer+'</strong></font>');
                }



                stringOut.push('</td></tr>');
            }
            stringOut.push('</tbody>');
            stringOut.push('</table>');

            /*
             * Create options menu
             */
            stringOut.push('<div id="optionsMenuProject" projectID="" class="tooltipMenuContainer"></div> ');
            var stringOutRender = stringOut.join('');
            renderObj.html(stringOutRender);
            $('.bugGrid')
                .alternateTableRowColors()
                .hiliteTableRow()
                .tableSorter();

            if (open == 1) {
                $('.bugGrid').hoverButtons({
                    archiveOption:    true,
                    editOption:       false,
                    elementClass:     'bugRow',
                    insertionElement: 'controls',
                    position:         '',
                    insertion:        'html'
                });

                $('.buttonArchive').click(function() {
                    var bugID = $(this).attr('itemID');
                    $('#rewordedTextContainer_'+bugID).show();

                    return false;
                });

                $('.textSubmit').click(function() {
                    var bugID = $(this).attr('itemID');
                    var rewordedText = $('#rewordedText_'+bugID).val();

                    var type;
                    if ($('#typeBug').attr('checked') == true) {
                        type = 'bug';
                    } else if ($('#typeUpdate').attr('checked') == true) {
                        type = 'update';
                    } else {
                        type = 'feature';
                    }

                    var formHash = {
                        bugID        : bugID,
                        rewordedText : rewordedText,
                        type         : type
                    }

                    $.ajax({
                        type: "POST",
                        url:  siteURL+'bugs/Bugs/closeBugReport',
                        data: formHash,
                        dataType: 'json',
                        success: function(payload){
                            $('#bugRow'+bugID).fadeOut('fast', function() {
                                $(this).remove();
                                supportBugObj.calculateBugStats();
                            });
                        },
                        failure: function() {

                        }
                    });
                });

                $('.buttonDelete').click(function() {
                    var bugID = $(this).attr('itemID');
                    $.ajax({
                        type: "POST",
                        url:  siteURL+'bugs/Bugs/deleteBugReport/'+bugID,
                        dataType: 'json',
                        success: function(payload){
                            $('#bugRow'+bugID).fadeOut('fast', function() {
                                $(this).remove();
                                supportBugObj.calculateBugStats();
                            });
                        },
                        failure: function() {

                        }
                    });

                    return false;
                });
            }

            supportBugObj.calculateBugStats();
        }
    },
    initBugGrid : function() {
        tabsBugs = $('#tabsBugs').tabs({
			fx: { opacity: 'toggle' },
			show: function(event,ui) {
				var panel = ui.panel.id;
                if (panel == 'windowProjectDetails') {

                }
            }
        });
    }
}

$(document).ready(function() {
    getBugDataObj.getBugs(1,0);
    getBugDataObj.getBugs(0,1);
    renderBugObj.initBugGrid();
});