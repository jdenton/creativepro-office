function replace_brs_with_n(element_id, html, body) {
	html = html.replace(/<br\s\>/g, "\n");
	return html;
}
tinyMCE.init({
	mode                            : "textareas",
	theme                           : "advanced",
	theme_advanced_buttons1         : "bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,undo,redo,link,unlink,|,formatselect,|,html",
	theme_advanced_buttons2         : "",
	theme_advanced_buttons3         : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align    : "left",
	content_css                     : siteURL+"css/stylesMaster.css",
	extended_valid_elements         : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
	editor_selector                 : "editorSimple",
	document_base_url               : siteURL+"js/tinyeditor/"
});
tinyMCE.init({
	mode                               : "textareas",
	theme                              : "advanced",
	theme_advanced_buttons1            : "bold,italic,underline,|,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,blockquote,|,formatselect,|,print,fullscreen,|,syntaxhl,|,media",
	theme_advanced_toolbar_location    : "top",
	theme_advanced_toolbar_align       : "left",
	theme_advanced_resizing            : true,
	theme_advanced_statusbar_location  : "bottom",
	plugins                            : "table,filemanager,imagemanager,print,fullscreen,paste,syntaxhl,media",
	theme_advanced_buttons3_add_before : "tablecontrols,separator",
	content_css                        : siteURL+"css/stylesMaster.css",
	debug                              : false,
	editor_selector                    : "editorAdvanced",
	document_base_url                  : siteURL+"js/tinyeditor/",
	extended_valid_elements            : "pre[class|align|style|name]|a[name|href|target|title|onclick|rel]|code[class|dir<ltr?rtl|id|lang|onclick|"
                                       +"ondblclick|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout|"
                                       +"onmouseover|onmouseup|style|title]|img[src|border|alt=|title|width|height|align|class]|"
                                       +"textarea[cols|rows|disabled|name|readonly|class]|"
                                       +"blockquote[class|name]",
    // popups
	dialog_types                       : "modal",

    // whitespace (from http://www.spencerkellis.net/downloads/tinymce_init.js)
	entity_encoding :                     "named",   // convert characters to named entities, e.g., &nbsp;
  	preformatted :                        false,     // whitespace such as tabs and spaces will be preserved
	paste_create_paragraphs :             false,
	paste_create_linebreaks:              false,
	paste_auto_cleanup_on_paste :         true,
	apply_source_formatting :             false,    // apply some source formatting to the output HTML code
	force_p_newlines :                    true,     // creation of paragraphs on return/enter in Mozilla/Firefox
	force_br_newlines :                   false,
	convert_newlines_to_brs :             false,
	remove_linebreaks :                   false,     // if linebreak characters should be removed from output HTML or not
	indentation :                         '30px',   // specify the indentation level for indent/outdent buttons in the UI
	remove_trailing_nbsp :                true     // remove any traling &nbsp; characters in block elements if you start to write inside them
});
