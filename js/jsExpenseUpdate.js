var initExpenseFormObj = {
    initExpenseForm : function() {
        /*
		 * Prevent form submit on client field ENTER
		 */
		$('#clientID').bind("keypress", function(e) {
             if (e.keyCode == 13) {
                 return false;
            }
         });

		$('textarea.expanding').elastic();

        /*
		 * Init date pickers
		 */
		$(".dateField").datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

        $('#buttonCancel').click(function() {
            window.location = siteURL+'finances/FinanceView';
            return false;
         });

		 /*
		 * Init client select combobox
		 */
        $('#clientID').autocomplete({
            source: clients,
            select: function(event, ui) {
                var clientID = ui.item.id;
                ui.item.value = genericFunctions.txtToField(ui.item.value);
                $('#clientIDHolder').val(clientID);
                $('#clientID').blur();
                initInvoiceFormObj.getProjectsFromClientID(clientID);
                getFinanceDataObj.getInvoiceListForSelect(clientID,0,'open_not_sent','invoiceID',true);
                $('#invoiceContainer').show();
           }
        });

        $('#projectID').change(function() {
            var clientID  = $('#clientIDHolder').val();
            var projectID = $(this).val();
            $('#projectIDHolder').val(projectID);
            getFinanceDataObj.getInvoiceListForSelect(clientID,projectID,'open_not_sent','invoiceID',true);
        });

        /*
         * If we're in edit mode, we need to get client projects and select project
         * if they are specified.
         */
        if ($('#clientIDHolder').val()>0) {
            initInvoiceFormObj.getProjectsFromClientID($('#clientIDHolder').val());
        }

        if ($('#projectIDHolder').val()>0) {
            $('#projectID').val($('#projectIDHolder').val());
        }

		$('#expenseReimburse').change(function() {
			if ($(this).val() == 'yes') {
                $('#billableContainer').addClass('barBlue');
				$('#billableToContainer').slideDown('fast').addClass('barBlue');
			} else {
                $('#billableContainer').removeClass('barBlue');
				$('#billableToContainer').slideUp('fast').removeClass('barBlue');
                $('#createNewInvoice').attr('checked',false);
				$('#clientID').val('');
				$('#projectID').html('');
			}
		});

        /*
		 * Init validation
		 */
		$("#formExpense").validate({
			errorClass: 'formError',
			errorElement: 'div',
			rules: {
				expenseAmount: "required",
				expenseTitle:  "required"
			},
			messages: validationMessages
		});

        /*
         * See if we have any settings to put into the form
         */
        if ($('#action').val() == 'add') {
            settingsObj.putSettingValuesInForm();
        }

        $('#linkAddNewVendor').click(function() {
            window.location = siteURL+"/finances/FinanceView#windowManageVendors";
        });

        /*
		 * Category and Tag edit boxes
		 */
		$('#expenseCategorySelect').combobox({
           source: categoryJSON, 
           select: function(event, ui) {
                var categoryID = ui.item.id;
                $('#expenseCategory').val(categoryID);
           },
           width: '374px',
           tabindex: 7,
           id: 'expenseCategoryCombo',
           name: 'expenseCategoryCombo'
        });
        
        /*
		 * Init tag field
		 */
        var tagBox = $('#expenseTags').autoSuggest(
            tagJSON,
            {
                selectedItemProp: 'value',
                searchObjProps: 'value',
                resultsHighlight: false,
                neverSubmit: true,
                boxWidth: '400px',
                startText: '',
                valuesHolderName: 'expenseTagsHolder',
                preFill: $('#tagHolder').val()
            }
        ); 

        $('#expenseRepeats').change(function() {
            if ($(this).val() == 'week') {
                $('#containerMonth').hide();
                $('#containerYear').hide();
                $('#containerWeek').toggle(true);
            } else if ($(this).val() == 'month') {
                $('#containerWeek').hide();
                $('#containerYear').hide();
                $('#containerMonth').toggle(true);
            } else if ($(this).val() == 'year') {
                $('#containerMonth').hide();
                $('#containerWeek').hide();
                $('#containerYear').toggle(true);
            }
		});

        /*
         * Toggle recurring panels if we already have an invoiceRecurringWhen selection (edit)
         */
        if ($('#expenseRepeats').val() != 0) {
            if ($('#expenseRepeats').val() == 'week') {
                $('#containerWeek').toggle(true);
            } else if ($('#expenseRepeats').val() == 'month') {
                $('#containerMonth').toggle(true);
            } else if ($('#expenseRepeats').val() == 'year') {
                $('#containerYear').toggle(true);
            }
        }
    }
}