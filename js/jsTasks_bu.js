var getTaskDataObj = {
	getTasks : function(projectID) {
		$.ajax({
		    type:  "POST",
		    url:   siteURL+'tasks/TaskView/getTasksForProject/'+projectID+'/json',
		    success: function(payload){
		    	var jsonObject   = eval('(' + payload + ')');
		    	taskArray = jsonObject.MilestonesTasks;
		    	renderTaskDataObj.renderTaskGrid(jsonObject);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
	getFrequentTasks : function() {
		
	}
}

var renderTaskDataObj = {
	renderTaskGrid : function(jsonObject) {
		var milestones      = jsonObject.MilestonesTasks;
		var totalMilestones = milestones.length;
		var headerArray     = jsonObject.HeaderLabels.split('|');
		var stringOut       = [];
						
		for(var a=0;a<=(totalMilestones)-1;a++) {
			var milestoneID        = milestones[a].MilestoneID;
			var milestoneTitle     = milestones[a].Title;
			var milestoneDateStart = milestones[a].Date_Start;
			var milestoneDateEnd   = milestones[a].Date_End;
			var tasks              = milestones[a].Tasks;
			var totalTasks         = tasks.length;
			var taskStringOut      = [];
			stringOut.length = 0;
						
			stringOut.push('<div style="width: 730px; margin: 12px 0 12px 0;">');
			stringOut.push('<div style="float: left; width: 80%;"><h2>'+milestoneTitle+'</h2></div>');
			stringOut.push('<div style="float: right; width: 20%;">');
			stringOut.push('<button class="coolButton buttonBGGreen newTask"><span class="add">New Task</span></button>');
			stringOut.push('</div><div style="clear: both;"></div></div>');
			stringOut.push('<div id="taskContainer'+milestoneID+'"></div>');
			var milestoneString = stringOut.join('');
			
			/*
			 * Send the milestone header and taskContainer to the DOM
			 */
			$('#taskInformationContainer').append(milestoneString);
			
			/*
			 * Now print tasks
			 */
			taskStringOut.length = 0;
			if (totalTasks>0) {				
				taskStringOut.push('<input type="hidden" id="acdcTaskGrid'+milestoneID+'" value="asc" />');
				taskStringOut.push('<table class="dataTable taskTable" id="TaskGrid'+milestoneID+'">');
				taskStringOut.push('<thead><tr>');
				taskStringOut.push('<th style="width: 50%;" acdc="1" class="sortable sort-alpha">'+headerArray[0]+'</th>');
				taskStringOut.push('<th style="width: 10%;" acdc="1" class="sortable sort-date">'+headerArray[1]+'</th>');
				taskStringOut.push('<th style="width: 10%;" acdc="1" class="sortable sort-date">'+headerArray[2]+'</th>');
				taskStringOut.push('<th style="width: 20%;">'+headerArray[3]+'</th>');
				taskStringOut.push('<th style="width: 5%; text-align: right;" acdc="1" class="sortable sort-num">'+headerArray[4]+'</th>');
				taskStringOut.push('<th style="width: 5%; text-align: right;" acdc="1" class="sortable sort-num">'+headerArray[5]+'</th>');
				taskStringOut.push('</tr></thead>');
				
				for(var b=0;b<=(totalTasks)-1;b++) {
					var taskTitle         = tasks[b].Title;
					var taskDateStart     = tasks[b].Date_Start;
					var taskDateEnd       = tasks[b].Date_End;
					var taskDateCompleted = tasks[b].Date_Completed;
					var creatorName       = tasks[b].CreatorName;
					var creatorEmail      = tasks[b].Email;
					var teamMembers       = tasks[b].TeamMembers;
					var totalTeamMembers  = teamMembers.length;					
								
					if (totalTeamMembers>0) {
						var teamList = [];
						for(var c=0;c<=(totalTeamMembers)-1;c++) {
							var teamMemberName  = teamMembers[c].NameFirst+' '+teamMembers[c].NameLast;
							var teamMemberEmail = teamMembers[c].Email;
							var teamMemberID    = teamMembers[c].Userid;
							teamList.push('<p><a href="mailto:'+teamMemberEmail+'">'+teamMemberName+'</a></p>');
						}
						var teamListFinal = teamList.join('');
					} else {
						var teamListFinal = '';
					}
					
					taskStringOut.push('<tr style="width: 730px;" class="sortableTableRow">');
					taskStringOut.push('<td style="width: 50%;">'+taskTitle+'</td>');
					taskStringOut.push('<td style="width: 10%; text-align: right;">'+taskDateStart+'</td>');
					taskStringOut.push('<td style="width: 10%; text-align: right;">'+taskDateEnd+'</td>');
					taskStringOut.push('<td style="width: 20%;">'+teamListFinal+'</td>');
					taskStringOut.push('<td style="width: 5%;"></td>');
					taskStringOut.push('<td style="width: 5%;"></td>');
					taskStringOut.push('</tr>');
					
				}
				taskStringOut.push('</table>');
				
				var taskString = taskStringOut.join('');
				$('#taskContainer'+milestoneID).html(taskString);
				$('#TaskGrid'+milestoneID)
					.alternateTableRowColors()
					.hiliteTableRow()
					.tableSorter()
					.sortable({
						items:       '.sortableTableRow',
						placeholder: 'blueRow',
						axis:        'y',
						forcePlaceholderSize: true
					});					
			}			
		}	
	}
}

var updateTaskObj = {
	
}

var initTaskObj = {
	initTaskForm : function() {
		/*
		 * Init date picker
		 */
		$("#taskDates").datepicker({ 
		    showOn:          'both', 
		    buttonImage:     siteURLJS+'images/icons/calendar.png', 
		    buttonImageOnly: true,
		    rangeSelect:     true,
		    numberOfMonths:  2
		});	
		
		$('textarea.expanding').autogrow({
		});
		
		/*
		 * Prevent task forms from default submit
		 * when hitting enter in fields.
		 */
		$('.taskForm').submit(function() {
			return false;
		});

		$('.taskElement').bind('keydown', function(e) {
			if (e.keyCode == 13) {           
				alert('Saving task');
				return false;
			} else if (e.keyCode == 9) {
				$(this).next().slideDown('fast');
			}
		});
		
		$('.taskElementTeam').bind('keyup', function(e) {
        	 e.stopPropagation();
             if (e.keyCode == 13) {
                  alert('Showing team members');
             }
        });
          
		$('.buttonTeam')
			.focus(function() {
				if ( $('#priorityMenu').is(':visible') ) {
					$('#priorityMenu').fadeOut('fast');
				}
				//$('.taskElementTeamMember:first')[0].focus();
			})
			.click(function(event) {
				/*
				 * This prevents the menu from vanishing 
				 * as soon as it's opened because of the body
				 * click handler that we attached in jquery.ui.tooltip.js
				 */
				event.stopPropagation();
			})
			.tooltip({
				tooltip: '#teamMemberMenu',
				mode: 'manual'
			});
			
		$('.buttonPriority')
			.focus(function() {
				if ( $('#teamMemberMenu').is(':visible') ) {
					$('#teamMemberMenu').fadeOut('fast');
				}	
				$('.taskElementPriority:first')[0].focus();
			})
			.click(function(event) {
				/*
				 * This prevents the menu from vanishing 
				 * as soon as it's opened because of the body
				 * click handler that we attached in jquery.ui.tooltip.js
				 */
				event.stopPropagation();
			})
			.blur(function() {
				if ( $('#priorityMenu').is(':visible') ) {
					$('#priorityMenu').fadeOut('fast');
				}
			})
			.tooltip({
				tooltip: '#priorityMenu',
				mode: 'manual'
			});	
		
		//$('#teamMemberList').arrowThruList();	
		$('#teamMemberList li').keynav('liFocus','');

		var selectedElement = 1;	
		$('.taskElementTeamMember')
			.bind('keydown', function(e) {				
				//e.preventDefault();           		
	            if (e.keyCode == 13) {
	            	if ($(this).prev().is(':checked')) {
	           			$(this).prev().removeAttr('checked');
	           		} else {
	           			$(this).prev().attr('checked','checked');
	           		}
	            } else if (e.keyCode == 9) {
	            	$('.buttonPriority').focus().css('background-position','0 -26px');
	            }
	            return false;
			});
		
		$('.taskElementPriority')
			.bind('keydown', function(e) {
				//e.preventDefault();           		
	            if (e.keyCode == 13) {
	            	if ($(this).prev().is(':checked')) {
	           			$(this).prev().removeAttr('checked');
	           		} else {
	           			$(this).prev().attr('checked','checked');
	           		}
	            } else if (e.keyCode == 40) {
	            	var listItem = $(this).next().html();
	            	alert(listItem);
	            } else if (e.keyCode == 9) {
	            	$('.buttonTaskSave').focus();
	            }
	            return false;
			});
		
		$('#buttonTaskSave').bind('keydown', function(e) {
			if (e.keyCode == 13) {
				alert('Saving task');	
			}
		});
	}
}