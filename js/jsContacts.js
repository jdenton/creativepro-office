var getContactObj = {
	getContacts : function(itemType,itemID,teamMember,vendor) {
        $.ajax({
            type: "POST",
            url:  siteURL+'contacts/AppContacts/getContacts/'+itemType+'/'+itemID+'/'+teamMember+'/'+vendor+'/json/0',
            dataType: 'json',
            success: function(payload) {
                renderContactsObj.renderContacts(payload);
                helperContactsObj.initContactsContainer();
                helperContactsObj.attachContactEventHandlers();
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    }
}

var renderContactsObj = {
	renderContacts : function(contactArray) {
		$('#contactsContainer').html('');
		var totalContacts = contactArray.length;
        if (totalContacts>0) {
            var contactHTML = [];
            var a;
            for(a=0;a<=totalContacts-1;a++) {
                contactHTML.push(renderContactsObj.createContactRenderString(contactArray[a]));
                var contactHTMLString = contactHTML.join('');
                $('#contactsContainer').append(contactHTMLString);
                contactHTML = [];
            }
            $('#noContactLinkContainer').hide();
            return contactHTML;
        } else {
            /*
             * Turn on add contact link
             */
			$('#noContactLinkContainer').show();
        }
	},
	createContactRenderString : function(contactArray) {
		var contactItems      = contactArray.ContactItems;
        if (contactItems) {
            var totalContactItems = (contactItems.length)-1;
            var contactItemCount  = contactItems.length;
            var contactString     = [];
            var returnString      = '';
            var contactType       = '';
            var pClass, visible, moreInfo = false, contactItemsCount = 0;
            var address = '', contactItemString = '', tagString = '', avatarFile;

            if (contactArray.AvatarURL != '' && contactArray.AvatarURL != null) {
                avatarFile = contactArray.AvatarURL;
            } else {
                avatarFile = siteURL+'images/avatars/stockAvatar46.png';
            }

            var creatorClass = '';
            if (contactArray.AccountCreator == 1) {
                creatorClass = 'accountCreator';
            }
            contactString.push('<div class="contactHolder vcard '+creatorClass+'" itemID="'+contactArray.PID+'" id="contactHolder'+contactArray.PID+'">');
            contactString.push('<div><img src="'+avatarFile+'" class="avatar46 avatar left mBottom12" />');
            contactString.push('<span class="fn"><strong>'+contactArray.NameFirst+' '+contactArray.NameLast+'</strong></span>');
            
            if (contactArray.Title != null && contactArray.Title != '') {
                contactString.push('<br /><span class="title">'+contactArray.Title+'</span>');
            }

            if (contactArray.Title != '' && contactArray.Company != '') {
                contactString.push(', ');
            }

            if (contactArray.Company != null && contactArray.Company != '') {
                contactString.push('<span class="org">'+contactArray.Company+'</span><br />');
            }

            if (contactArray.RoleName == null) {
                contactArray.RoleName = '';
            } else {
                contactArray.RoleName = ', '+contactArray.RoleName;
            }

            if (contactArray.AccountCreator == 1) {
                contactString.push('<p class="subText">'+commonTerms['accountCreator']+contactArray.RoleName+'</p>');
            } else if (contactArray.TeamMember == 1) {
                contactString.push('<p class="subText">'+commonTerms['teamMember']+contactArray.RoleName+'</p>');
            } else if (contactArray.Vendor == 1) {
                contactString.push('<p class="subText">'+commonTerms['vendor']+'</p>');
            }
            contactString.push('</div>');

            contactString.push('<div style="clear:both;">');

            if (contactArray.Address != null && contactArray.Address != '' && contactArray.Address != 'null') {
                address += '<span class="street-address contactItem">'+contactArray.Address+'</span><br />';
                contactItemsCount = contactItemsCount+1;
            }
            if (contactArray.City != null && contactArray.City != '' && contactArray.City != 'null') {
                address += '<span class="locality contactItem">'+contactArray.City+'</span> ';
                contactItemsCount = contactItemsCount+1;
            }
            if (contactArray.State != null && contactArray.State != '' && contactArray.State != 'null') {
                address += '<span class="region contactItem">'+contactArray.State+'</span> <span class="postal-code">'+contactArray.Zip+'</span><br />';
                contactItemsCount = contactItemsCount+1;
            }
            if (contactArray.Country != null && contactArray.Country != '' && contactArray.Country != 'null') {
                address += '<span class="country-name contactItem">'+contactArray.Country+'</span>';
                contactItemsCount = contactItemsCount+1;
            }

            if (contactItemCount>0) {
                var contactItemString = '';
                for(var b=0;b<=totalContactItems;b++) {
                    var newContactType  = contactItems[b].ContactType;
                    var contactLocation = contactItems[b].ContactLocation;
                    /*
                     * If we're adding more than 1 of the same kind of contact
                     * e.g. 2+ emails or 2+ phone numbers, then see if we have
                     * other contact information to render and make a determination
                     * to render.
                     */
                    if (newContactType != contactType) {
                        pClass = newContactType;
                        contactType = newContactType;
                        visible = '';
                    } else if (newContactType == contactType && contactItemCount>2) {
                        pClass = 'spacer';
                        visible = 'hide  moreContactInfo';
                    } else {
                        pClass = 'spacer';
                    }

                    if (newContactType == 'phone') {
                        var vCardType = 'tel';
                    } else {
                        var vCardType = newContactType;
                    }

                    if (contactType == 'url' || contactType == 'im') {
                        visible = 'hide  moreContactInfo';
                    }
                    if (pClass != null) {
                        contactItemString += '<p class="icon_'+pClass+' '+visible+' contactItem"><span class="'+vCardType+'" location="'+contactLocation+'">';
                        if (contactType == 'email') {
                            contactItemString += '<a href="mailto:'+contactItems[b].ContactData+'">'+contactItems[b].ContactData+'</a>';
                        } else if (contactType == 'url') {
                            contactItemString += '<a href="'+contactItems[b].ContactData+'">'+contactItems[b].ContactData+'</a>';
                        } else if (contactType == 'im') {
                            var imArray = contactItems[b].ContactData.split('|');
                            var imUserName = imArray[0];
                            var imProtocol = imArray[1];
                            contactItemString += imUserName+' ['+imProtocol+']';
                        } else {
                            contactItemString += contactItems[b].ContactData;
                        }
                        contactItemString += '</span> <span class="subText">'+contactItems[b].ContactLocation+'</span></p>';
                        contactItemsCount = contactItemsCount+1;
                    }
                }
            }
            if (contactArray.Tags != '' && contactArray.Tags != null) {
                tagString = '<span class="icon_tag contactItem">'+contactArray.Tags+'</span>';
                contactItemsCount = contactItemsCount+1;
            }

            /*
             * If we have more than 3 contact items, don't render address
             */
            if (contactItemsCount>3) {
                contactString.push('<p style="display: none; margin-bottom: 6px;" class="addressBlock moreContactInfo">'+address+'</p>');
            } else {
                contactString.push('<p style="margin-bottom: 6px;" class="addressBlock">'+address+'</p>');
            }
            contactString.push('<p class="contactItemBlock">'+contactItemString+'</p>');
            contactString.push('<p class="tagBlock">'+tagString+'</p>');

            if (contactItemsCount>3) {
                contactString.push('<a href="#" style="margin-top: 12px;" class="icon_more_small toggleView">'+commonTerms['more']+'</a>');
            }
            contactString.push('</div></div>');
            returnString += contactString.join('');
            return returnString;
        }
	},
	makeVcard : function(hcard) {
		var email = '',phone = '';

		var fn      = hcard.find('.fn').text();
		var title   = hcard.find('.title').text();
		var org     = hcard.find('.org').text();
		var address = hcard.find('.street-address').text();
		var city    = hcard.find('.locality').text();
		var region  = hcard.find('.region').text();
		var zip     = hcard.find('.postal-code').text();
		var country = hcard.find('.country-name').text();
		hcard.find('.email').each(function() {
			email += $(this).text()+'|';
		});
		hcard.find('.tel').each(function() {
			phone += $(this).text()+'|'+$(this).attr('location')+'||';
		});
		var url     = hcard.find('.url').text();
		
		var dataString = 'fn='+fn+'&title='+title+'&org='+org+'&address='+address+'&city='+city+'&region='+region+'&zip='+zip+'&country='+country+'&email='+email+'&url='+url+'&phone='+phone;
		$('#vCardFrame').contents().find('#vCardParams').val(dataString);
		$('#vCardFrame').contents().find('#formMakeVCard').submit();		
	}
}

var updateContactsObj = {
	saveContact : function() {
        /*
         * Get form data
         */
        var itemID = 0, teamMember = 0, contact = 0, itemType = 0, primaryContact = 0, vendor = 0;
        itemType = $('#contactType').val();
        switch(itemType) {
            case 'C':
                itemID = $('#clientID').val();
                break;
            case 'P':
                itemID = $('#projectID').val();
                break;
            case 'T':
                teamMember = 1;
                break;
            case 'V':
                vendor = 1;
                break;
            default:
                itemType = 0;
        }

        var roleID;
		if ($('#contactTeamMemberRole').val() == '0') {
            roleID = 0;
		} else {
			roleID = $('#contactTeamMemberRole').val();
            /*
             * If we've selected a role, let's assume we want to make this
             * person a Team Member.  Set the Team Member flag to 1.
             */
            teamMember = 1;
		}

        if (teamMember != 1 && vendor != 1) {
            /*
             * Then we mush be a contact
             */
            contact = 1;
        }        

        var formHash = {
                    contactType      : $('#contactType').val(),
                    action           : $('#contactAction').val(),
                    contactID        : $('#contactID').val(),
                    contactUID       : $('#contactUID').val(),
					teamMember       : teamMember,
                    contact          : contact,
                    vendor           : vendor,
					roleID           : roleID,
                    itemID           : itemID,
                    itemType         : itemType,
					avatarFilename   : $('#avatarFilename').val(),
                    NameFirst        : $('#contactNameFirst').val(),
                    NameLast         : $('#contactNameLast').val(),
                    ContactTitle     : $('#contactTitle').val(),
                    Company          : $('#contactCompany').val(),
                    Address          : $('#contactAddress').val(),
                    City             : $('#contactCity').val(),
                    State            : $('#contactState').val(),
                    Zip              : $('#contactZip').val(),
                    Country          : $('#contactCountry').val(),
                    Timezone         : $('#contactTimezone').val(),
					Language         : $('#contactLanguage').val(),
                    Rate             : $('#contactRate').val(),
                    ContactItems     : [],
                    contactPhoneData : '',
                    contactEmailData : '',
                    contactURLData   : '',
                    contactIMData    : ''
        }
        var contactPhone         = '';
        var contactEmail         = '';
        var contactURL           = '';
        var contactIM            = '';
        var contactIMLocation    = '';
        var a=0;
        $('.contactPhoneData').each(function(i) {
            contactPhone += $(this).val()+'|'+$(this).next().val()+'||';
            formHash.ContactItems[a] = {
                ContactType     : 'phone',
                ContactData     : $(this).val(),
                ContactLocation : $(this).next().val()
            }
            a++;
        });

		var firstEmail = '';
        $('.contactEmailData').each(function(i) {
            contactEmail += $(this).val()+'|'+$(this).next().val()+'||';
            formHash.ContactItems[a] = {
                ContactType     : 'email',
                ContactData     : $(this).val(),
                ContactLocation : $(this).next().val()
            }
			if (firstEmail == '') {
				firstEmail = $(this).val();
			}
            a++;
        });

        $('.contactURLData').each(function(i) {
            contactURL += $(this).val()+'|'+$(this).next().val()+'||';
            formHash.ContactItems[a] = {
                ContactType     : 'url',
                ContactData     : $(this).val(),
                ContactLocation : $(this).next().val()
            }
            a++;
        });

        $('.contactIMData').each(function(i) {
            contactIM += $(this).val()+'|'+$(this).next().val()+'|'+$(this).next().next().val()+'||';
            formHash.ContactItems[a] = {
                ContactType     : 'im',
                ContactData     : $(this).val(),
                ContactLocation : $(this).next().val()
            }
            a++;
        });

        formHash.contactPhoneData     = contactPhone;
        formHash.contactEmailData     = contactEmail;
        formHash.contactURLData       = contactURL;
        formHash.contactIMData        = contactIM;

		/*
		 * If we want to save a team member but we don't have an email address
		 * or a role selected, throw an error.
		 */
		if (teamMember == 1 && firstEmail == '') {
			$('#messageContactForm').systemMessage({
				status:  'error',
				message: commonTerms['contact_error_noemail'],
				size:    'Small',
				noFade:  1
			});
		} else if (teamMember == 1 && roleID == 0) {
            $('#messageContactForm').systemMessage({
				status:  'error',
				message: commonTerms['contact_error_no_role'],
				size:    'Small',
				noFade:  1
			});
        } else {
			$.ajax({
				type: "POST",
				url:  siteURL+'contacts/AppContacts/saveContact',
				data: formHash,
				success: function(payload){
					var PID = payload;
					formHash.PID = PID;
					var contactArray = [];
					contactArray[0] = formHash;
                    /******
                     * Commented out by Jeff D. on 5/14/10 because I'm not sure
                     * why it's here.
					renderContactsObj.renderContacts(contactArray,0);
                    helperContactsObj.initContactsContainer();
                    helperContactsObj.attachContactEventHandlers();
                    */

                    /*
                     * If we're adding a team member, increment maxTeamMembers by 1
                     */
                    var maxTeamMembers = $('#maxTeamMembers').val();
                    var newMaxTeamMembers = maxTeamMembers++;
                    $('#maxTeamMembers').val(newMaxTeamMembers);
                    
                    /*
                     * Get contacts. Now let's be careful here.
                     * If we're adding a new Team Member from the Team Member screen,
                     * then we only want to pull Team Members back. But, if we're updating
                     * a contact on a project page to a Team Member status, then we need
                     * to pull all project contacts back - not just Team Members.
                     */
                    if (itemType != 'T') {
                        teamMember = 0;
                    }
                    getContactObj.getContacts(itemType,itemID,teamMember,vendor);
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});

			helperContactsObj.clearContactForm();            
		}       
	},
    populateEditForm : function(jsonObj) {
		$('#contactNameFirst').val(jsonObj.NameFirst);
		$('#contactNameLast').val(jsonObj.NameLast);
		$('#contactTitle').val(jsonObj.ContactTitle);
		$('#contactCompany').val(jsonObj.Company);
        $('#contactUID').val(jsonObj.UID);
        var contactItems = jsonObj.ContactItems;
        
        /*
         * Put contact items into nice arrays
         */
        var ContactEmail = [];
        var ContactPhone = [];
        var ContactURL   = [];
        var ContactIM    = [];

        for(var a in contactItems) {
            if (contactItems[a].ContactType == 'phone') {
                ContactPhone.push(contactItems[a]);
            } else if (contactItems[a].ContactType == 'email') {
                ContactEmail.push(contactItems[a]);
            } else if (contactItems[a].ContactType == 'url') {
                ContactURL.push(contactItems[a]);
            } else if (contactItems[a].ContactType == 'im') {
                ContactIM.push(contactItems[a]);
            }
        }
        
        /*
         * Phone Numbers
         */
        if (ContactPhone.length>0) {
            $('.contactPhoneData').val(ContactPhone[0].ContactData);
            $('.contactPhoneLocation').val(ContactPhone[0].ContactLocation);
        }
        if (ContactPhone.length>1) {
            /*
             * Render new input fields
             */
            for(a=1;a<=(ContactPhone.length-1);a++) {
                helperContactsObj.addAnother($('#Phone'));
                $('.contactPhoneData:last').val(ContactPhone[a].ContactData);
                $('.contactPhoneLocation:last').val(ContactPhone[a].ContactLocation);
            }
        }

        /*
         * Email Addresses
         */
        if (ContactEmail.length>0) {
            $('.contactEmailData').val(ContactEmail[0].ContactData);
            $('.contactEmailLocation').val(ContactEmail[0].ContactLocation);
        }
        if (ContactEmail.length>1) {
            /*
             * Render new input fields
             */
            for(a=1;a<=(ContactEmail.length-1);a++) {
                helperContactsObj.addAnother($('#Email'));
                $('.contactEmailData:last').val(ContactEmail[a].ContactData);
                $('.contactEmailLocation:last').val(ContactEmail[a].ContactLocation);
            }
        }

        /*
         * URL's
         */
        if (ContactURL.length>0) {
            $('.contactURLData').val(ContactURL[0].ContactData);
            $('.contactURLLocation').val(ContactURL[0].ContactLocation);
        }
        if (ContactURL.length>1) {
            /*
             * Render new input fields
             */
            for(a=1;a<=(ContactURL.length-1);a++) {
                helperContactsObj.addAnother($('#URL'));
                $('.contactURLData:last').val(ContactURL[a].ContactData);
                $('.contactURLLocation:last').val(ContactURL[a].ContactLocation);
            }
        }

        /*
         * IM's
         */
        if (ContactIM.length>0) {
            var contactDataArray = ContactIM[0].ContactData.split('|');
            $('.contactIMData').val(contactDataArray[0]);
            $('.contactIMProtocol').val(contactDataArray[1]);
            $('.contactIMLocation').val(ContactIM[0].ContactLocation);
        }
        if (ContactIM.length>1) {
            /*
             * Render new input fields
             */
            for(a=1;a<=(ContactIM.length-1);a++) {
                helperContactsObj.addAnother($('#IM'));
                contactDataArray = ContactIM[a].ContactData.split('|');
                $('.contactIMData:last').val(contactDataArray[0]);
                $('.contactIMProtocol:last').val(contactDataArray[1]);
                $('.contactIMLocation:last').val(ContactIM[a].ContactLocation);
            }
        }

		$('#contactAddress').val(jsonObj.Address);
        $('#contactCity').val(jsonObj.City);
        $('#contactState').val(jsonObj.State);
        $('#contactZip').val(jsonObj.Zip);
        $('#contactCountry').val(jsonObj.Country);
        $('#contactTimezone').val(jsonObj.Timezone);
        $('#contactLanguage').val(jsonObj.Language);
        $('#contactTeamMemberRole').val(jsonObj.RoleID);
        $('#contactRate').val(jsonObj.Rate);
        
        /*
         * Avatar
         */
        $('#avatarFilename').val(jsonObj.AvatarURL);
        $('#containerAvatar').html('<img src="'+jsonObj.AvatarURL+'" class="avatar80" />');

        $('#contactNameFirst').focus();
    },
    removeContactFromItem : function(pID,itemID,itemType) {
        $.ajax({
            type: "POST",
            url:  siteURL+'contacts/AppContacts/removePersonFromItem/'+pID+'/'+itemID+'/'+itemType,
            success: function(payload) {
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    }
}

var helperContactsObj = {
	initContactsForm : function(contactType) {
        /*
         * Contact Team: T = team member, C = contact, V = vendor
         */

        /*
         * Total team members = numbers[5] + 1 to account for the account owner/creator.
         */
        var totalTeamMembers = parseInt(numbers[5]+1);
		$('#buttonNewContact').click(function() {
            if ($('#contactType').val() == 'T' && $('.contactHolder').length >= totalTeamMembers) {
                $('#modalAccountLimit').dialog('open');
                $('#maxProjectsContainer').hide();
                $('#maxTeamMembersContainer').show();
                $('#maxFilesContainer').hide();
            } else {
                $('#formContactContainer').show();
                $('#buttonNewContact').hide();
                $('#contactSearchContainer').hide();
                $('#contactsContainer').hide();
                $('#noContactLinkContainer').hide();
            }
			return false;
		});
		
		$('#linkNewContact').click(function(){
			$('#formContactContainer').show();
			$('#buttonNewContact').hide();
            $('#contactsContainer').hide();
            $('#noContactLinkContainer').hide();
			return false;
		});

		$('#linkChangeAvatar').click(function() {
			$('#linkChangeAvatar').hide();
			$('#avatarUploadPanel').slideDown('fast');
			/*
			 * Initialize avatar uploader
			 */
			updateSettingsObj.uploadAvatarFiles('member');

            /*
             * Initialize avatar URL box
             */
            $('#avatarFilename').blur(function() {
               /*
                * Attempt to show new avatar photo
                */
               var avatarFileURL = $(this).val();
               var avatarImg = '<img src="'+avatarFileURL+'" class="avatar80" />';
                $('#containerAvatar').html(avatarImg);
            });
			return false;
		});

		$('#buttonSaveContact').click(function() {
			updateContactsObj.saveContact();
            return false;
		});
		
		$('#buttonCancelContact').click(function(){			
            helperContactsObj.clearContactForm();
			return false;
		});

        $('#formContact').submit(function() {
           return false;
        });

        /*
         * Add new contact points even handlers
         */
        $('.addAnother')
            .click(function(){
                helperContactsObj.addAnother($(this));
                return false;
            })
            .bind('keyup', function(e) {
                 //e.stopPropagation();
                 if (e.keyCode == 13) {
                     e.stopPropagation();
                      helperContactsObj.addAnother($(this));
                      return false;
                 }
            })
            .focus(function() {
                $(this).addClass('emphLink');
            })
            .blur(function() {
                $(this).removeClass('emphLink');
            });

        /*
		 * Init tag field
		 */        
		$("#peopleTags").autocomplete(tagJSON, {
			width: 295,
			max: 4,
            highlight: false,
            multiple: true,
            multipleSeparator: " ",
            scroll: true,
            scrollHeight: 300,
			formatItem: function(row, i, max) {
				return row.ItemName;
		    },
		    formatMatch: function(row, i, max) {
				return row.ItemName;
		    },
		    formatResult: function(row) {
				return row.ItemName;
		    }
		 });         
         
         /*
          * Check for bad data on rate field
          */
         $('#contactRate').blur(function() {
            var el = $(this);
            if ( el.val() != '' && isNaN(parseFloat(el.val())) ) {
                el.addClass('errorState');
            } else {
                el.removeClass('errorState');
            }
         });

         /*
          * Populate roles select menu
          */
         if (contactType == 'V') {
            $('#contactTeamMemberRoleContainer').hide();
         } else {
            helperContactsObj.populateRoleSelect();
         }
         helperContactsObj.attachContactEventHandlers();
	},
    attachContactEventHandlers : function() {
        /*
         * Event handlers for little hover buttons and other card-specific links
         */
        $('#contactsContainer').delegate('.buttonEdit', 'click', function() {
			var pID = $(this).attr('itemID');
			$('#formContactContainer').show();
			$('#buttonNewContact').hide();
			$('#contactsContainer').hide();

			$('#contactAction').val('edit');
			$('#contactID').val(pID);

            $.getJSON(siteURL+'contacts/AppContacts/getContact/'+pID+'/json/0/1', function(payload){
                updateContactsObj.populateEditForm(payload);
            });

			return false;
		});

        $('#contactsContainer').delegate('.buttonDelete', 'click', function() {
			var pID = $(this).attr('itemID');
			$('#contactHolder'+pID).fadeOut('slow', function() {
                $('#contactHolder'+pID).remove();
            });
            updateTrashObj.sendToTrash('contact',pID);
			return false;
		});

        $('#contactsContainer').delegate('.buttonVCard', 'click', function() {
			var contactID = $(this).attr('itemID');
			var hcard   = $('#contactHolder'+contactID);
			renderContactsObj.makeVcard(hcard);
            return false;
		});

        $('#contactsContainer').delegate('.buttonRemove', 'click', function() {
			var pID = $(this).attr('itemID');
            var itemType = $(this).parents('div #contactsContainer').attr('itemType');
            var itemID   = $(this).parents('div #contactsContainer').attr('itemID');
			$('#contactHolder'+pID).fadeOut('slow');
            updateContactsObj.removeContactFromItem(pID,itemID,itemType);
			return false;
		});

		$('.toggleView').unbind().click(function() {
			var moreStuff = $(this).parents('div .contactHolder').find('.moreContactInfo');
			if (moreStuff.is(':visible')) {
				moreStuff.hide();
				$(this).removeClass('icon_less_small').addClass('icon_more_small').html(commonTerms['more']);
			} else {
				moreStuff.show();
				$(this).removeClass('icon_more_small').addClass('icon_less_small').html(commonTerms['less']);
			}
			return false;
		});
    },
    populateRoleSelect : function() {
        $.ajax({
            type: "POST",
            url:  siteURL+'team/TeamUpdate/getRolesPermissions',
            dataType: 'json',
            success: function(payload) {
                var selectString = '';
                for(var a in payload) {
                    selectString = '<option value="'+payload[a].RoleID+'">'+payload[a].RoleName+'</option>';
                    $('#contactTeamMemberRole').append(selectString);
                }
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    },
    addAnother : function(element) {
        element.removeClass('emphLink');
        var itemType    = element.attr('id');
        var elementHTML = '<div class="newElementContainer">'+$('#elementContainer'+itemType).html();
        elementHTML    += '<a href="#" style="display: inline-block;" class="icon_delete single"></a></div>';
        elementHTML     = $(elementHTML);
        var container   = $('#contactContainer'+itemType);
        container.append(elementHTML);
        elementHTML.children('input').focus();

        /*
         * Attach icon delete event handlers
         */
        elementHTML.children('a').click(function() {
            $(this).parent('div').fadeOut('slow', function() {
                $(this).remove();
            });
            return false;
        });

        return false;
    },
    clearContactForm : function() {
        $('#formContactContainer').hide();
        $('#buttonNewContact').show();
        $('#contactSearchContainer').show();
        $('#contactsContainer').show();
        $('.newElementContainer').remove();
		$('#linkChangeAvatar').show();
        $('#containerAvatar').html('');
		$('#avatarUploadPanel').hide();

		$('#contactAction').val('add');
		$('#contactID').val('');
        $('#contactUID').val('');
        
        $('#contactNameFirst').val('');
        $('#contactNameLast').val('');
        $('#contactTitle').val('');
        $('#contactCompany').val('');
        
        $('.contactPhoneData').val('');
        $('.contactPhoneLocation').val('');
        $('.contactEmailData').val('');
        $('.contactEmailLocation').val('');
        $('.contactURLData').val('');
        $('.contactURLLocation').val('');
        $('.contactIMData').val('');
        $('.contactIMLocation').val('');
        $('.contactIMProtocol').val('');
        
        $('#contactAddress').val('');
        $('#contactCity').val('');
        $('#contactState').val('');
        $('#contactZip').val('');
        $('#contactCountry').val('');
        $('#contactTimezone').val('');
        $('#contactLanguage').val('');
        $('#contactTeamMemberRole').val('');
        $('#contactRate').val('');
        $('#peopleTags').val('');
        $('#avatarFilename').val('');
    },
    initContactsContainer : function() {
        /*
		 * Make little edit/delete buttons (jquery.commonAppMethods.js)
		 */
        var removeOption = true;
        var editOption = false;
        var deleteOption = false;
        if ($('#pageLeftColumn').attr('pageID') == 'teamUpdate') {
            removeOption = false;
            editOption = true;
            deleteOption = true;
        }

		$('#contactsContainer').hoverButtons({
            deleteOption:      deleteOption,
            editOption:        editOption,
            removeOption:      removeOption,
			elementClass:      'contactHolder',
            editClass:         'contactUpdate teamUpdate',
            deleteClass:       'contactDelete teamDelete'
		});
        permissionsObj.renderControlsBasedOnPermissions();
        $('#contactsContainer').hiliteContainerRow();

        /*
         * Turn off delete button on account owner
         */
        $('.accountCreator').find('.buttonDelete').hide();
    }
}