(function( $ ) {
		$.widget( "ui.combobox", {
			_create: function() {
				var self = this,
					select = this.element.hide(),
					selected = select.children( ":selected" ),
					value = select.is('input:text') ?
								select.val() :
									selected.val() ?  selected.text() : "";
				var input = this.input = $( "<input>" )
					.insertAfter( select )
					.val( value )
                    .css('width', self.options.width)
                    .addClass('inputCombo')
                    .attr('id',self.options.id)
                    .attr('name',self.options.name)
                    .attr('tabindex',self.options.tabindex)
					.autocomplete({
                        source: self.options.source,
                        delay: self.options.delay ? self.options.delay : 0,
                        minLength:  self.options.minLength ? self.options.minLength : 0,
                        /*
                         * We'll want to uncomment this if we ever want to ajax enable the combo box.
						source: function( request, response ) {
							if (!self.options.source) {
								var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
								response( select.children( "option" ).map(function() {
									var text = $( this ).text();
									if ( this.value && ( !request.term || matcher.test(text) ) )
										return {
											label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
											value: text,
											option: this
										};
								}) );
							} else {
                                response(self.options.source);
								//self.options.source(request, response);
							}
						},
                    */
						select: function( event, ui ) {
							if (!self.options.select) {
								ui.item.option.selected = true;
								self._trigger( "selected", event, {
									item: ui.item.option
								});
							} else {
								self.options.select(event, ui);
							}
						},
						change: function( event, ui ) {
							if ( !ui.item ) {
                                var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),valid = false;
								select.children( "option" ).each(function() {
									if ( $( this ).text().match( matcher ) ) {
										this.selected = valid = true;
										return false;
									}
								});
								if ( !valid ) {
									//$( this ).val( "" );
									//select.val( "" );
									//input.data( "autocomplete" ).term = "";
									//return false;
								}
							}
						}
					})
					.addClass( "ui-widget ui-widget-content ui-corner-left" );

				input.data( "autocomplete" )._renderItem = function( ul, item ) {
					return $( "<li></li>" )
						.data( "item.autocomplete", item )
						.append( "<a>"+item.label+"</a>" )
						.appendTo( ul );
				};

				this.button = $( "<button type=\"button\"> </button>" )
					.attr( "tabIndex", -1 )
					.attr( "title", "Show All Items" )
					.insertAfter( input )
					.button({
						icons: {
							primary: "ui-icon-triangle-1-s"
						},
						text: false
					})
					.removeClass( "ui-corner-all" )
					.addClass( "ui-corner-right ui-button-icon btnCombo" )
					.click(function() {
						// close if already visible
						if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
							input.autocomplete( "close" );
							return;
						}

						// work around a bug (likely same cause as #5265)
						$( this ).blur();

						// pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
						input.focus();
					}).find('.ui-button-text').removeClass('ui-button-text');
			},

			destroy: function() {
				this.input.remove();
				this.button.remove();
				this.element.show();
				$.Widget.prototype.destroy.call( this );
			}
		});
	})( jQuery );


