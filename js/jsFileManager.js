var fileTree;
var fileModalArray = [];
var fileManagerTriggerItemID;
var fileManagerTriggerItemType;
var fileUseRaw;
var fileView = 'thumb';
var currentFolderID, currentParentID;
var attachedFilesView = false;

var getFileDataObj = {
	getFilesForSomething : function(itemID,itemType,fileContainerElement,attachEventHandlers) {
		$.ajax({
		    type:  "POST",
		    url:   siteURL+'files/FileManager/getFilesForSomething/'+itemID+'/'+itemType+'/json/0',
			dataType: 'json',
		    success: function(payload) {
				fileArray = payload;
                renderFileObj.renderFileWindow(payload,fileContainerElement,true);
                if (attachEventHandlers != false) {
                    fileManagerObj.attachFileWindowEventHandlers(fileContainerElement);                    
                }
                fileManagerObj.attachFilePlugins(fileContainerElement);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
    getFilesForView : function(folderID,parentID,view,containerElement,fileserviceFolderID) {
        if (parentID == '') {
            parentID = '0';
        }
        if (parentID == 'google' || parentID == 'dropbox') {
            if (typeof fileserviceFolderID == 'undefined') {
                fileserviceFolderID = '';
            }

            var sendHash = {
                thisPage : thisPage,
                folderID : fileserviceFolderID
            }
            $.ajax({
                type:  "POST",
                url:   siteURL+'files/FileManager/oAuthFileService/'+parentID,
                data:  sendHash,
                dataType: 'json',
                success: function(payload) {
                    if (typeof payload.AuthLink != 'undefined') {
                        window.location = payload.AuthLink;
                    } else {
                        /*
                         * We probably have documents or folders coming back.
                         */
                        fileModalArray = payload;
                        if (view == 'window') {
                            renderFileObj.renderFileWindow(payload,containerElement);
                        }
                    }
                    
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });

        } else {
            currentFolderID = folderID;
            currentParentID = parentID;
            $.ajax({
                type:  "POST",
                url:   siteURL+'files/FileManager/getFileItems/'+folderID+'/'+parentID,
                dataType: 'json',
                success: function(payload) {
                    fileModalArray = payload;
                    if (view == 'window') {
                        renderFileObj.renderFileWindow(payload,containerElement);
                    }
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    }
}

var fileManagerObj = {
    fileManagerButton : function() {
        $('.fileUploadButton').click(function() {
            $('#modalFileUpload').dialog('open');
            fileManagerTriggerID       = $(this).attr('id');
            fileManagerTriggerItemID   = $(this).attr('itemID');
            fileManagerTriggerItemType = $(this).attr('itemType');

            return false;
        });
        fileManagerObj.initFileManagerModal();
    },
    initFileManagerModal : function() {
        $('#modalFileUpload').dialog({
        	autoOpen:      false,
        	closeOnEscape: true,
        	width:         '925px',
        	position:      'center',
			resizable:     false,
			zIndex:        1000,
			open:          function(event,ui) {
                if (fileModalArray.length == 0) {
                    getFileDataObj.getFilesForView(0,0,'window','#fileListContainer');
                    fileManagerObj.attachFileWindowEventHandlers('#fileListContainer');
                    fileManagerObj.attachFilePopupEventHandlers();
                    fileManagerObj.getFileUsage();
                }
                if (!$.browser.msie) {
                    filesObj.initPluploadDragNDrop(0,0,$('#fileListContainer'),'dropFilesHereFileManager','fileManagerUploadingContainer',true);
                }
			}
        });
	},
	uploadFileSuccess : function(jsonObject,fileManagerModal) {
        var fileWindowObj = '#fileWindowViewContainer';
        if (fileManagerModal === true) {
            fileWindowObj = '#fileListContainer';
        }
        var jsonObject = eval('('+jsonObject+')');
        renderFileObj.renderFileWindow(jsonObject,fileWindowObj);
	},
	createItemContainer : function(itemArray,isPill) {
		var fileID       = itemArray.FileID;
        var parentID     = itemArray.ParentID;
		var itemType = 'file';
        if (itemArray.Folder == 1) {
            itemType = 'folder';
        }
		var fileName     = itemArray.FileNameActual;
		var filePath     = itemArray.fileLink;
		var fileExt      = itemArray.fileExt;
        var fileUploader = itemArray.UploaderName;
        var fileSize     = itemArray.FileSize;
        var fileDate     = itemArray.DateEntry;
        var fileService  = itemArray.FileService;
        var shared       = itemArray.Shared;
        var linkID       = 0;

        if (itemArray.LinkID) {
            linkID = itemArray.LinkID;
        }

        var selector, fileServiceClass = '';
		if (itemType == 'file') {
			selector = 'fileContainer';
		} else {
			selector = 'folderContainer';
		}
        if (fileService == 1) {
            fileServiceClass = 'fileservice';
            linkID = itemArray.FolderID;
        }
		var itemContainer = '<li class="icon_file_'+fileExt+' '+selector+' itemContainer '+fileServiceClass+'" id="fileContainer_'+fileID+'" itemType="'+itemType+'" itemID="'+fileID+'" linkID="'+linkID+'" parentID="'+parentID+'" filePath="'+filePath+'">';
        if (shared == 1 && userType != USER_TYPE_CLIENT) {
            itemContainer += '<span class="shared"></span>';
        }
        
        if (isPill == true) {
            itemContainer += fileName+'&nbsp;<a class="as-close clickable buttonRemove">&times;</a>';
        } else {
            itemContainer += '<p class="title">'+fileName+'</p>';
            if (itemType == 'file') {
                itemContainer += '<p class="fileDetails hide subText">'+fileUploader+' '+fileDate+'<br />'+fileSize+'</p>';
            } else {
                itemContainer += '<p id="folderContainer_'+fileID+'" class="hide"></p>';
            }
        }    
        itemContainer += '</li>';
        return itemContainer;
	},
    attachFileWindowEventHandlers : function(containerElement) {
        $(containerElement).delegate('.fileContainer', 'click', function() {
        	if ($(this).hasClass('selected')) {
				$(this).removeClass('selected');
			} else {
				$(this).addClass('selected');
			}
		});
        
        $(containerElement).delegate('.fileContainer', 'click', function() {
        	console.log('remove file');
            return false;
		});
        
        $(containerElement).delegate('.folderContainer:not(.fileservice) .title', 'click', function() {
        	/*
             * This is the folder edit function
             */
            var obj = $(this);
            var folderID = obj.parent().attr('itemid');
            if (!obj.hasClass('editing')) { 
                var folderName = obj.html();
                var inputElement = '<input type="text" value="'+folderName+'" style="width: 78%;" class="folderNameField" />';
                obj.html(inputElement); 
                
                var inputObj = obj.find('.folderNameField');
                inputObj.focus();
                obj.addClass('editing');
                
                inputObj.bind('blur', function() {
                   saveFolderEdits();
                });
                
                inputObj.bind('keyup', function(e) {
                    e.stopPropagation();
                    if (e.keyCode == 13) {
                        saveFolderEdits();
                        return false;
                    }
                });
                
                function saveFolderEdits() {
                    obj.removeClass('editing');
                    var newFolderName = inputObj.val();
                    obj.html(newFolderName);
                    updateFilesObj.updateFolder(null,newFolderName,'edit',folderID);
                }
                
                
            }    
		});

        $(containerElement).delegate('.fileContainer', 'dblclick', function() {
            var itemID = $(this).attr('itemID');
			var filePath = $(this).attr('filePath');
            location.href = filePath;
		});

        $(containerElement).delegate('.itemContainer', 'dblclick', function() {
            if ($(this).attr('itemtype') == 'folder') {
                var folderID = $(this).attr('itemid');
                var parentID = $(this).attr('parentID');
                var linkID   = $(this).attr('linkID');
                var folderName = $(this).find('.title').html();
                $('#fileWindowFolderNameContainer').html(folderName);
                getFileDataObj.getFilesForView(folderID,parentID,'window',containerElement,linkID);
                fileManagerNodeID = folderID;
                $.get(siteURL+'files/FileManager/setNodeIDInSession/'+fileManagerNodeID);
                    
                if ($(this).hasClass('.fileservice')) {
                    /*
                     * Turn off our folder and upload buttons
                     */
                    $('#buttonupload').toggle(false);
                    $('#buttonfolderNew').toggle(false);
                } else {
                    $('#buttonupload').toggle(true);
                    $('#buttonfolderNew').toggle(true);
                }
            }
        });        

        $(containerElement).delegate('.buttonDelete', 'click', function() {
			var itemID    = $(this).attr('itemID');
            var itemType  = $(this).parents('li').parents('li').attr('itemType');
            var parentID  = $(this).parents('li').parents('li').attr('parentID');
            var itemTitle = $(this).parents('li').parents('li').find('.title').html();
            if (itemType == 'folder') {
                $.get(siteURL+'files/FileManager/getNumberOfItemsInFolder/'+itemID, function(payload) {
                    if (payload>0) {
                        updateFilesObj.deleteFolderModal(itemID, parentID, itemTitle, payload);
                    } else {
                        updateFilesObj.deleteFolder(itemID);
                    }
                });
            } else {
                updateFilesObj.deleteFileModal(itemID,itemTitle);
            }
			return false;
		});

        $(containerElement).delegate('.buttonView', 'click', function() {
			var fileID = $(this).attr('itemID');
            var linkID = $(this).closest('li.fileContainer').attr('linkID');
            
            $('#fileViewContainer').show();
            $('#pageLeftColumn').hide();

            $.ajax({
                type:  "POST",
                url:   siteURL+'files/FileManager/getFileItem/'+fileID,
                dataType: 'json',
                success: function(payload) {
                    renderFileObj.renderFileDetails(payload,linkID);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
			return false;
		});

        $('#fileWindowNavContainer').delegate('.folderCrumb', 'click', function() {
            var itemID   = $(this).attr('itemID');
            var parentID = $(this).attr('parentID');
            var folderName = $(this).html();
            fileManagerNodeID = itemID;
            $('#fileWindowFolderNameContainer').html(folderName);
            getFileDataObj.getFilesForView(itemID, parentID, 'window', containerElement);
            return false;
        });
        $('.itemContainer').liveDraggable({
            revert: 'invalid'
        });

        $(containerElement).delegate('.buttonRemove', 'click', function() {
            var fileLinkID = $(this).closest('li.fileContainer').attr('linkID');
        	updateFilesObj.removeAttachedFileFromItem(fileLinkID, $(containerElement));
            $(this).closest('li.fileContainer').fadeOut('fast', function() {
                $(this).remove();
            });
            return false;
		});

        if (userType != USER_TYPE_CLIENT) {
            $(containerElement).delegate('.sharable', 'click', function() {
                var itemContainer = $(this);
                var fileLinkID = $(this).closest('li.fileContainer').attr('linkID');
                $.ajax({
                    type:  "POST",
                    url:   siteURL+'files/FileManager/updateFileShareStatus/'+fileLinkID,
                    dataType: 'json',
                    success: function(payload){
                        if (payload.ShareStatus == '1') {
                            itemContainer.prepend('<span class="shared"></span>');
                        } else {
                            itemContainer.find('span.shared').remove();
                        }
                    },
                    error: function (xhr, desc, exceptionobj) {
                        errorObj.ajaxError(xhr,desc,exceptionobj);
                    }
                });
            });
        }

        $('body').delegate('.removeFile', 'click', function() {
            var fileLinkID = $(this).parent().attr('id');
            var fileListContainer = $(this).parent().parent();
            $(this).parent().fadeOut('fast',function() {
                $(this).remove();
            });
            var fileLinkIDArray = fileLinkID.split('_');
            fileLinkID = fileLinkIDArray[1];
            updateFilesObj.removeAttachedFileFromItem(fileLinkID, fileListContainer);

            return false;
        });

        $('#closeFileView').click(function() {
            $('#pageLeftColumn').show();
            $('#fileViewContainer').hide();
        });
    },
    attachFilePopupEventHandlers : function() {
        $('#buttonSiteFileSelect').unbind('click').click(function() {
			var destID = '#'+fileManagerTriggerID+'_fileContainer';
            var fileIDString = '';
            var fileNameArray = [];
            var filePathArray = [];
            var fileClassArray = [];
            var fileTypes = 'local';
			$('.selected').each(function(i) {
				var classStructure = $(this).attr('class');
                var classArray = classStructure.split(' ');

				var itemID         = $(this).attr('itemID');
				var fileName       = $(this).children('p').html();
                var filePath       = $(this).attr('filepath');
                var parentID       = $(this).attr('parentID');
                var fileType       = filesObj.getFileTypeFromClassName($(this));

                if (parentID == 'fileservice' || parentID == 'google') {
                    fileTypes = 'fileservice';
                    fileIDString += '{"FileName":"'+fileName+'","FilePath":"'+filePath+'","FileType":"'+fileType+'"},';
                } else {
                    fileIDString += itemID+'|';
                }                
                filePathArray.push(filePath);
                fileNameArray.push(fileName);
                fileClassArray.push(classArray[0]);				
			});
			$('.selected').removeClass('selected');
            fileIDString = fileIDString.slice(0, -1);

            if (fileTypes == 'fileservice') {
                fileIDString = '['+fileIDString+']';
            }
                
			var sendHash = {
                fileIDString : fileIDString,
                fileTypes    : fileTypes
            }
			$.ajax({
				type:  "POST",
				url:   siteURL+'files/FileManager/attachFilesToItem/'+fileManagerTriggerItemID+'/'+fileManagerTriggerItemType,
				data:  sendHash,
				success: function(payload){
                    if (payload != 0) {
                        getFileDataObj.getFilesForSomething(fileManagerTriggerItemID,fileManagerTriggerItemType,destID,false);
                        
                    }
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});

		});
    },
    attachFilePlugins : function(containerElement) {
        var removeOption = false;
        var deleteOption = true;
        if (attachedFilesView == true) {
            removeOption = true;
            deleteOption = false;            
        }
        if (userType == USER_TYPE_CLIENT) {
            removeOption = false;
            deleteOption = false;
        }

        if (!$(containerElement).hasClass('pill')) {
            $('.itemContainer').hoverButtons({
                viewOption:       true,
                editOption:       false,
                deleteOption:     deleteOption,
                removeOption:     removeOption,
                width:            '25px',
                position:         'topRight',
                elementClass:     'itemContainer'
            });
        }    
        
        if (attachedFilesView != true) {
            /*
             * Turn off some buttons
             */
            $('.fileservice ul.smallButtonCluster').html('');
            $('.folderContainer ul.smallButtonCluster .buttonView').hide();
        }

        $('.folderContainer').droppable({
            hoverClass: 'active',
            drop: function(event,ui) {
                var item = ui.draggable;
                var itemType   = item.attr('itemtype');
                var dragItemID = item.attr('itemid');
                var dropItemID = $(this).attr('itemid');
                $.get(siteURL+'files/FileManager/dropFileItem/'+dragItemID+'/'+dropItemID, function() {
                    item.fadeOut('fast');
                });
            }
        });        
    },
    attachPageEventHandlers : function() {
        $('#buttonupload').click(function() {
            $('#panelUpload').toggle(true);
            $('#panelDefault').toggle(false);
            $('#panelNewFolder').toggle(false);
        });

        $('#buttonfolderNew').click(function() {
            $('#panelUpload').toggle(false);
            $('#panelDefault').toggle(false);
            $('#panelNewFolder').toggle(true);
            $('#folderName').val('').focus();
        });

        $('#buttonNewFolderCancel').click(function() {
            $('#panelUpload').toggle(false);
            $('#panelDefault').toggle(true);
            $('#panelNewFolder').toggle(false);
        });

        $('#buttonNewFolderSave').click(function() {
            $('#panelUpload').toggle(false);
            $('#panelDefault').toggle(true);
            $('#panelNewFolder').toggle(false);
            var parentNodeID = fileManagerNodeID;
            var folderName   = $('#folderName').val();
            var action       = $('#folderAction').val();
            updateFilesObj.updateFolder(parentNodeID,folderName,action);
        });

        $('#folderName').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                $('#panelUpload').toggle(false);
                $('#panelDefault').toggle(true);
                $('#panelNewFolder').toggle(false);
                var parentNodeID = fileManagerNodeID;
                var folderName   = $('#folderName').val();
                var action       = $('#folderAction').val();
                updateFilesObj.updateFolder(parentNodeID,folderName,action);
                return false;
            }
        });

        $('#thumbnailView').click(function() {
            $('#fileListUL').removeClass().addClass('fileListBlock');
            fileView = 'thumb';
            return false;
        });

        $('#detailView').click(function() {
            $('#fileListUL').removeClass().addClass('fileListDetail');
            fileView = 'detail';
            return false;
        });
    },
	serveFileForDownload : function(itemID,hashFileName) {
		$('#downloadFrame').contents().find('#itemID').val(itemID);
        $('#downloadFrame').contents().find('#hashFileName').val(hashFileName);
		$('#downloadFrame').contents().find('#formDownload').submit();
	},
    getFileUsage : function() {
        $.get(siteURL+'files/FileManager/getFileSpaceUsage/json/0', function(payload) {
			fileManagerObj.renderFileUsage(payload);
		});
    },
    renderFileUsage : function(jsonString) {
        var jsonObject   = eval('('+jsonString+')');
        var fileUseTotal = jsonObject.FileUseTotal;
        var accountMax   = jsonObject.AccountMax;
        var fileUsePercentage = jsonObject.FileUsePercentage;

        fileUseRaw = parseInt(jsonObject.FileUseRaw);
        var accountMaxRaw = parseInt(numbers[6]);

        var useString = fileUseTotal+' of '+accountMax+' ('+fileUsePercentage+'%)';

		var barWidthDec = parseFloat(fileUsePercentage/100);
		var barWidthPixels = Math.round(barWidthDec*200);

        if (barWidthPixels > 200) {
            barWidthPixels = 200;
        }

		$('.fileManagerMeterText').html(useString);
		$('.fileManagerMeterBar').css('width',barWidthPixels);

        if ( (fileUseRaw >= accountMaxRaw || accountMaxRaw < 1) && swfUploader) {
            swfUploader.setButtonDisabled(true);
            $('#spanButtonPlaceHolderFake').show();
        }
    }
}

var renderFileObj = {
    renderFileDetails : function(jsonObject,fileLinkID) {
        var fileArray = [];
        var fileExt       = jsonObject.fileExt;
        var filePath      = jsonObject.fileLink;
        var dateUploaded  = jsonObject.DateEntry;
        var fileName      = jsonObject.FileNameActual;
        var uploadedBy    = jsonObject.NameFirst+' '+jsonObject.NameLast;
        var fileSize      = jsonObject.FileSize;
        var fileIconClass = jsonObject.FileClassName;
        if (fileSize == '0 bytes') {
            fileSize = '';
        }

        var isImage = genericFunctions.isFileAnImage(fileExt);
        if (isImage == true) {
            fileArray.push('<img src="'+filePath+'" style="max-width: 717px;" />');
        }
        fileArray.push('<div class="fileInfoContainer '+fileIconClass+'"><h2>'+fileName+'</h2>');
        fileArray.push('<p>'+uploadedBy+' '+dateUploaded+'</p><p>'+fileSize+'</p>');
        fileArray.push('</div>');

        $('#downloadFileView').unbind().click(function() {
            location.href = filePath;
        });

        /*
         * Get messages for this file.
         */
        if (fileLinkID>0) {
            /*
             * Let's look for any messages for this file.
             */
            $('#messageListFile').html('');
            $('#newMessageFile').attr('itemID',fileLinkID);
            getMessageObj.getMessages(fileLinkID,'attachedFile',$('#messageListFile'),0,10,1,0);
        }
        var fileString = fileArray.join('');
        $('#fileViewInfomationContainer').html(fileString);
    },
	renderFileListItem : function(fileName,className,targetContainerID,fileLinkID,path,doReturn) {
		var listClass  = className+'_list';
        var fileString  = '<li class="fileLinkContainer" id="fileLink_'+fileLinkID+'">';
            //fileString += '<a href="#" class="buttonDeleteSmall removeFile" title="'+commonTerms['remove']+'"></a>';
            fileString += '<a href="'+path+'" title="'+fileName+'" target="_blank" style="display: inline-block;" class="'+listClass+' linkContainer">'+fileName+'</a></li>';
		if (doReturn == true) {
            return fileString;
        } else {
            $('#'+targetContainerID).append(fileString);
        }
        renderFileObj.updateFileCount($('#'+targetContainerID), $('.fileCount'));
	},
	renderFileListItems : function(jsonObject,fileContainerID,doReturn) {
        var fileRenderArray = [];
		for(var a=0;a<=((jsonObject.length)-1);a++) {
			var fileName   = jsonObject[a].FileNameActual;
			var className  = jsonObject[a].FileClassName;
            var fileLinkID = jsonObject[a].LinkID;
            var filePath   = jsonObject[a].fileLink;
            if (jsonObject[a].FileService == 1) {
                filePath = jsonObject[a].FileServicePath;
            }

            fileRenderArray.push(renderFileObj.renderFileListItem(fileName,className,fileContainerID,fileLinkID,filePath,doReturn));
		}
        renderFileObj.updateFileCount($('#'+fileContainerID), $('.fileCount'));

        if (doReturn == true) {
            return fileRenderArray.join('');
        } else {
        }
	},
    updateFileCount : function(fileListContainer,fileCountContainer) {
        var fileCount = fileListContainer.find('ul.fileListBlock').children('.fileContainer').size();
        fileCountContainer.html('('+fileCount+')');
    },
    renderFileWindow : function(jsonObject,containerElement,attachedFiles,returnString) {
        var isPill = false;
        $(containerElement).html('');
        
        if ($(containerElement).hasClass('pill')) {
            isPill = true;
        }
        
        var fileData = jsonObject.Files;
        var items,files;
		if (fileData != null && fileData.children) {
			items = fileData.children.length;
			files = fileData.children;
		} else {
			items = fileData.length;
			files = fileData;
		}
		var stringOut = [], className;
        
        switch (fileView) {
            case 'thumb':
                className = 'fileListBlock';
                break;
            case 'detail':
                className = 'fileListDetail';
                break;
        }  
        if (isPill == true) {
            className = 'fileListPill';
        }
		stringOut.push('<ul id="fileListUL" class="'+className+'">');
		for(var a=0;a<=(items-1);a++) {
			var itemContainer = fileManagerObj.createItemContainer(files[a],isPill);
			stringOut.push(itemContainer);
		}
		stringOut.push('</ul>');
		var stringOutRender = stringOut.join('');
		if (items>0) {
			/*
			 * Clear panel for new icons
			 */
			$(containerElement).empty();
		}

        if (returnString == true) {
            return stringOutRender;
        } else {
            $(containerElement).append(stringOutRender);
        }

        if ($(containerElement).hasClass('fileAttachProject')) {
            $(containerElement).find('li.itemContainer').addClass('sharable');
        }

        if (attachedFiles != true) {
            var pathString = renderFileObj.renderFolderPath(jsonObject.Path);
            $('#fileWindowNavContainer').html(pathString);
            /*
             * Attach event handlers
             */
            if (containerElement != '#fileListContainer') {
                $(".itemContainer").draggable("destroy");
                fileManagerObj.attachFilePlugins();
            }
        } else {
            renderFileObj.updateFileCount($(containerElement), $('.fileCount'));
        }
	},
    renderFolderPath : function(pathObject) {
        var pathString = '';
        for(var a=0;a<=(pathObject.length-1);a++) {
            pathString += '<a href="#" class="folderCrumb" itemID="'+pathObject[a].NodeID+'" parentID="'+pathObject[a].ParentID+'">'+pathObject[a].FolderName+'</a>';
            if (a<pathObject.length-1) {
                pathString += ' › ';
            }
        }
        return pathString;
    }
}

var updateFilesObj = {
    removeAttachedFileFromItem : function(fileLinkID,fileListContainer) {
        $.ajax({
            type:  "POST",
            url:   siteURL+'files/FileManager/removeAttachedFileFromItem/'+fileLinkID,
            success: function(payload){
                renderFileObj.updateFileCount(fileListContainer, $('.fileCount'));
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    },
    removeAttachedFilesFromItem : function(fileLinkIDString) {
        var formHash = {
                    fileLinkIDString : fileLinkIDString
        }
        $.ajax({
            type:  "POST",
            data: formHash,
            url:   siteURL+'files/FileManager/removeAttachedFilesFromItem',
            success: function(payload){

            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    },
    updateFolder : function(parentNodeID,FolderName,action,folderID) {
        var sendHash = {
            currentParentID : currentParentID,
            parentNodeID    : parentNodeID,
            folderName      : FolderName,
            action          : action,
            folderID        : folderID
        }
        
        if (action == 'edit') {
            $.ajax({
                type:  "POST",
                url:   siteURL+'files/FileManager/renameFolder',
                data:  sendHash,
                dataType: 'json',
                success: function(payload) {
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        } else {        
            $.ajax({
                type:  "POST",
                url:   siteURL+'files/FileManager/createNewFolder',
                data:  sendHash,
                dataType: 'json',
                success: function(payload) {
                    fileModalArray = payload;
                    renderFileObj.renderFileWindow(payload,'#fileWindowViewContainer');
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }    
    },
    deleteFolderModal : function(folderID,parentID,folderTitle,itemCount) {
        var modalContents = [];
        modalContents.push('<div class="errorMessageBig">'+commonTerms['deleteFolder']+': '+folderTitle+'</div>');
        modalContents.push('<p style="margin-bottom: 12px;"><strong>'+commonTerms['deleteFileNoUndo']+'</strong></p>');
        modalContents.push('<p><input type="checkbox" id="deleteFolderContents" value="1" /> '+commonTerms['deleteContentsToo']+'</p>');
        var outString = modalContents.join('');
        $('#deleteContentsContainer').html(outString);

        $('#buttonDeleteModalDelete').unbind('click').click(function(){
            var deleteContents = 0;
            if ($('#deleteFolderContents').attr('checked')) {
                deleteContents = true;
            }
            updateFilesObj.deleteFolder(folderID,parentID,deleteContents);
        });
        $('#modalDelete').dialog('open');
    },
    deleteFolder : function(deleteFolderID,deleteParentID,deleteContents) {
        /*
         * We are not going to send files to the trash. We are going to
         * delete them permanently.
         */
        if (deleteContents == true) {
            $.get(siteURL+'files/FileManager/deleteItem/'+deleteFolderID+'/'+deleteParentID+'/folder/1', function(payload) {
                fileManagerObj.renderFileUsage(payload);
                getFileDataObj.getFilesForView(currentFolderID,currentParentID,'window','#fileWindowViewContainer');
            });
        } else {
            $.get(siteURL+'files/FileManager/deleteItem/'+deleteFolderID+'/'+deleteParentID+'/folder', function(payload) {
                fileManagerObj.renderFileUsage(payload);
                getFileDataObj.getFilesForView(currentFolderID,currentParentID,'window','#fileWindowViewContainer');
            });
        }
        $('#modalDelete').dialog('close');        
    },
    deleteFileModal : function(fileID,fileName) {
        var modalContents = [];
        modalContents.push('<div class="errorMessageBig">'+commonTerms['deleteFile']+': '+fileName+'</div>');
        modalContents.push('<p style="margin-bottom: 12px;"><strong>'+commonTerms['deleteFileNoUndo']+'</strong></p>');
        var outString = modalContents.join('');
        $('#deleteContentsContainer').html(outString);

        $('#buttonDeleteModalDelete').unbind('click').click(function(){
            $.get(siteURL+'files/FileManager/deleteItem/'+fileID+'/0/file', function(payload) {
                $('#modalDelete').dialog('close');
                $('#fileContainer_'+fileID).fadeOut('fast');
                fileManagerObj.renderFileUsage(payload);
            });            
        });
        $('#modalDelete').dialog('open');
    }
}

$(document).ready(function() {
    if ($('#pageLeftColumn').attr('pageID') == 'fileView') {
        getFileDataObj.getFilesForView(0,0,'window','#fileWindowViewContainer');
        fileManagerObj.attachFileWindowEventHandlers('#fileWindowViewContainer');
        fileManagerObj.attachPageEventHandlers();
        fileManagerObj.getFileUsage();
        $('#footerWidgetMenu li.iconfilemanager').toggle(false);
	}

    if ($('#pageLeftColumn').attr('pageID') == 'individualFileView') {
        var fileID = $('#fileID').val();
        var linkID = $('#fileLinkID').val();
        $.ajax({
            type:  "POST",
            url:   siteURL+'files/FileManager/getFileItem/'+fileID,
            dataType: 'json',
            success: function(payload) {
                renderFileObj.renderFileDetails(payload,linkID);
                $('#fileViewContainer').show();
                $('#closeFileView').hide();

                var projectObj = eval('('+projectData+')');
                var messageTo = projectObj.MessageRecipients;
                initMessageBox.createMessageBox(messageTo,$('#newMessageFile'),$('#messageListFile'),false,false,false,true,true,true);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    }
});