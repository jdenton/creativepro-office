var widgetToolsObj = {
	objectifyCookie : function(cookie) {
		var cookieString = $.cookie(cookie);
		if (cookieString) {
			/*
			 * Remove the trailing coma from time JSON string
			 */
			cookieString = cookieString.slice(0,-1);
			var cookieObj = eval('([' + cookieString + '])');
			return cookieObj;
		} else {
			return false;
		}
	},
	widgetTimerLogToDatabase : function(timerElementID,action) {
		var timeSheetID = $('#timeSheetID_'+timerElementID).val();
		var projectID   = $('#projectID_'+timerElementID).val();
		var taskID      = $('#taskID_'+timerElementID).val();
		var comments    = $('#comments_'+timerElementID).val();
		var elapsedTime = $('#elapsedTime_'+timerElementID).val();
		var billable    = 0;

		if ($('#billable_'+timerElementID).is(':checked')) {
			billable = 1;
		}

        if (action == 'save') {
            $('#containerMessage_'+timerElementID).systemMessage({status: 'success', message: commonTerms['timerTimeSaved']});
        }
		
		var formHash = {
				action      : action,
				timeSheetID : timeSheetID,
				projectID   : projectID,
				taskID      : taskID,
				comments    : comments,
				elapsedTime : elapsedTime,
				billable    : billable
		}
		$.ajax({
			type: "POST",
			data: formHash,
            dataType: 'json',
			url:  siteURL+'timesheets/TimesheetView/saveTimesheetRecordFromJobTimer',
			success: function(payload){
                var jsonObject = payload;
                /*
                 * Check for task hours container
                 */
                if ($('#taskHours_'+taskID).length>0 && (action == 'stop' || action == 'save')) {
                    var taskHours = parseFloat($('#taskHours_'+taskID).html());
                    var totalHours = parseFloat(elapsedTime) + taskHours;
                    $('#taskClock'+taskID).removeClass('icon_timer_running_small_right').addClass('icon_timer_small_right');
                }
				$('#timeSheetID_'+timerElementID).val(jsonObject.TimesheetID);
			},
			error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
		});
	},
	widgetTimerElapsedTimeDecimal : function(elapsedTime) {
		var elapsedTimeArray = elapsedTime.split(':');
		var hour = elapsedTimeArray[0];
		var min  = elapsedTimeArray[1];
		var sec  = elapsedTimeArray[2];

		if (sec.substr(0,1) == '0') {
            sec = sec.substr(1,1);
        }
        if (min.substr(0,1) == '0') {
            min = min.substr(1,1);
        }
        if (hour.substr(0,1) == '0') {
            hour = hour.substr(1,1);
        }

        sec  = parseInt(sec);
		min  = parseInt(min);
		hour = parseInt(hour);
		
		if (sec>29) { min = min+1; }
		var minDecimal = parseFloat(min/60);
		var elapsedTimeDecimal = parseFloat(hour+minDecimal);
		elapsedTimeDecimal = elapsedTimeDecimal.toFixed(2);
		return elapsedTimeDecimal;
	},
	widgetTimerToggle : function(elementID,action) {
		if (action == 'start') {
			widgetToolsObj.widgetTimerStopOtherTimers();
			$('#timerDisplayControl_'+elementID).removeClass('buttonRoundPlay').addClass('buttonRoundStop');
			$('#timerRunning_'+elementID).val('1');
			widgetToolsObj.widgetTimerControl(elementID,'start');
			if ($('#finalizePanel_'+elementID).is(':visible')) {
				$('#finalizePanel_'+elementID).hide('fast');
			}
			widgetToolsObj.widgetTimerLogToDatabase(elementID,'start');
		} else if (action == 'stop') {
			$('#timerDisplayControl_'+elementID).removeClass('buttonRoundStop').addClass('buttonRoundPlay');
			$('#timerRunning_'+elementID).val('0');
			widgetToolsObj.widgetTimerControl(elementID,'stop');
			$('#finalizePanel_'+elementID).show('fast');

            /*
			 * Figure out elapsed time
			 */
			var elapsedTimeDecimal = widgetToolsObj.widgetTimerElapsedTimeDecimal($('#timerDisplayControl_'+elementID).text());
            $('#elapsedTime_'+elementID).val(elapsedTimeDecimal);
            	widgetToolsObj.widgetTimerLogToDatabase(elementID,'stop');
		}
	},
	widgetTimerStopOtherTimers : function() {
		$('.timerRunning').each(function() {
			if ($(this).val() == 1) {
				var elementID = $(this).parent('div').attr('id');
				$('#timerDisplayControl_'+elementID).removeClass('buttonRoundStop').addClass('buttonRoundPlay');
                $('#timerRunning_'+elementID).val('0');
				widgetToolsObj.widgetTimerControl(elementID,'stop');
			}
		});
	},
    widgetTimerControl : function(timerElementID,action) {
		widgetToolsObj.widgetTimerRebuildJSONCookie();
		if (timersRunning == 0) {
            widgetToolsObj.widgetTimerCheckRunningTimers();
        }
		var currentTime = $('#timerDisplayControl_'+timerElementID).text();
		var timeArray = currentTime.split(':');
		var sec  = timeArray[2];
		var min  = timeArray[1];
		var hour = timeArray[0];
        
        if (sec.substr(0,1) == '0') {
            sec = sec.substr(1,1);
        }
        if (min.substr(0,1) == '0') {
            min = min.substr(1,1);
        }
        if (hour.substr(0,1) == '0') {
            hour = hour.substr(1,1);
        }

        sec  = parseInt(sec);
		min  = parseInt(min);
		hour = parseInt(hour);

		if (action == 'start') {
            sec++;
			if (sec == 60) {
				sec = 0;
				min = min + 1;
			}

			if (min == 60) {
			   min = 0;
			   hour = hour + 1;
			}
			
			var cookieTime = hour+':'+min+':'+sec;
			if (sec<=9) { sec = "0" + sec; }
            if (min<=9) { min = "0" + min; }
            if (hour<=9) { hour = "0" + hour; }
            var jobTime = hour+':'+min+':'+sec;

			/*
			 * Render new time to window
			 */
			$('#timerDisplayControl_'+timerElementID).html(jobTime);
            eval("timerTimeout"+timerElementID+" = window.setTimeout(\"widgetToolsObj.widgetTimerControl("+timerElementID+",'start');\", 1000);");
		} else {
            widgetToolsObj.widgetTimerRebuildJSONCookie();
            widgetToolsObj.widgetTimerCheckRunningTimers();
            eval("window.clearTimeout(timerTimeout"+timerElementID+");");
		}
	},
    widgetTimerRebuildJSONCookie : function() {
        var timerJSONString = '';
        $('.timerElement').each(function() {
            var projectName   = $(this).children('.titleContainer').children('.projectName').html();
            var taskName      = $(this).children('.titleContainer').children('.taskName').html();
            var projectID     = $(this).children('.projectID').val();
            var taskID        = $(this).children('.taskID').val();
            var timerRunning  = $(this).children('.timerRunning').val();
            var elapsedTime   = $(this).children('.timerClockReadout').html();
			var timeSheetID   = $(this).children('.timeSheetID').val();
            var elementID     = $(this).attr('id');
            var userid        = numbers[0];
			timerJSONString += '{"ElementID":"'+elementID+'","Project":"'+projectName+'","Task":"'+taskName+'","ProjectID":"'+projectID+'","TaskID":"'+taskID+'","ElapsedTime":"'+elapsedTime+'","TimerRunning":"'+timerRunning+'","TimeSheetID":"'+timeSheetID+'","Userid":"'+userid+'"},';
		});
		$.cookie('jobTimers',timerJSONString,{ expires: 2, path: '/'});
    },
    widgetTimerCheckRunningTimers : function() {
        timersRunning = 0;
        var timerLogToCookie;
        $('.timerRunning').each(function() {
           if ($(this).val() == '1') {
               timersRunning = 1;
               timerLogToCookie = window.setTimeout("widgetToolsObj.widgetTimerRebuildJSONCookie();", 1000);
			   /*
				* Change footer icon if it exists
				*/
			   $('.icontimer').addClass('icontimer_play');
			}
        });
        if (timersRunning == 0) {
            window.clearTimeout(timerLogToCookie);
        }
    },
	clearEventForm : function() {
		$('#eventDateStartWidget').val('');
		$('#eventTimeStartWidget').val('');
		$('#eventTitleWidget').val('');
		$('#eventCalendarWidget').val('');		
	},
	attachEventPopups : function() {
		$('.EventWidget').each(function() {
			var dateMySQL = $(this).parent().attr('dateMySQL');
			var tipContent = $(this).children('p').html();

			var widgetPosition = $('#widgetContent_calendar').position();
            var widgetModalPosition = 400;
            if ($('#widgetContent_calendar').parents('#bigModal').length>0) {
                widgetModalPosition = $('#widgetContent_calendar').parents('#bigModal').position();
            }

			if (widgetPosition.left>300 || widgetModalPosition.left>300) {
				var target  = 'topLeft';
				var tooltip = 'bottomRight';
				var corner  = 'bottomRight';
			} else {
				var target  = 'topRight';
				var tooltip = 'bottomLeft';
				var corner  = 'bottomLeft';
			}

			$(this).qtip({
				content:    {
					text:      tipContent
				},
				position: {
					corner: {
						   target:  target,
						   tooltip: tooltip
					   }
				},
				show: {
				   when: { event: 'mouseover' },
				   delay: 0,
				   solo: true
				},
				hide: {
				   when: {  event: 'mouseout'  }
				},
				style: {
				   border: {
					   width: 5
				   },
				   tip: {
					   corner: corner
				   },
				   name: 'cream',
				   width: 350,
				   padding: 6
				},
				api: {
					onHide: function() {

					}
				}
		   });

		   $(this).click(function() {
				return false;
			});
		});
	},
    attachHandlersTaskForm : function() {
		$('.saveTask').unbind().click(function() {
           var formObj = $(this).parent();
		   var action  = $(this).parent().find('.actionTask').val();
           widgetToolsObj.saveTask(formObj);
           widgetToolsObj.clearTaskForm(formObj,action);
           return false;
        });

        $('.cancelTask').unbind().click(function() {
			var action = $(this).parent().find('.actionTask').val();
			var formObj = $(this).parent();
			widgetToolsObj.clearTaskForm(formObj,action);
            formObj.fadeOut('fast');
            $(this).parent().prev().fadeIn('fast');
			return false;
        });

        /*
         * Populate project select
         */
        $('.taskProjectSelect').append('<option value="0"></option>');
		for(var a=0;a<=(projectListArray.length-1);a++) {
			$('.taskProjectSelect').append('<option value="'+projectListArray[a].ProjectID+'">'+projectListArray[a].Title+'</option>');
		}

        /*
         * If we select a project, get milestones.
         */
        $('.taskProjectSelect').change(function() {
            var projectID = $(this).val();
            widgetToolsObj.getMilestonesForProject(projectID,'.taskMilestoneSelect');
        });
        
        $('.taskDateDueWidget').datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

        $('.saverFieldTask').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
				widgetToolsObj.saveTask();
                return false;
            }
        });

        $('.taskTimeDue').focus(function() {
            $(this).val('');
        })
	},
    getMilestonesForProject : function(projectID,selectElement) {
        $.getJSON(siteURL+'tasks/TaskView/getMilestonesForSelect/'+projectID+'/json/0', function(payload) {
            var jsonObject = payload;
            $(selectElement).html('');
            if (jsonObject.length > 0) {
                var optionString;
                for(a=0;a<=jsonObject.length-1;a++) {
                    optionString = '<option value="'+jsonObject[a].TaskID+'">'+jsonObject[a].Title+'</option>';
                    $(optionString).appendTo(selectElement);
                }
                $(selectElement).attr('disabled','');
            } else {
                $(selectElement).attr('disabled','disabled');
            }
        });
    },
    attachHandlersTasks : function() {
        $('.linkNewTask').unbind().click(function() {
           widgetToolsObj.openTaskForm();
           return false;
        });

		$('.editTask').unbind().click(function() {
            var id = $(this).attr('id');
            var taskArray = id.split('_');
            var taskID = taskArray[1];
            var taskTitle       = $('.containerTask_'+taskID).find('#taskTitle_'+taskID).html();
            var taskProjectID   = $('.containerTask_'+taskID).find('#projectIDContainer_'+taskID).val();
            var taskMilestoneID = $('.containerTask_'+taskID).find('#milestoneIDContainer_'+taskID).val();
            var taskDate        = $('.containerTask_'+taskID).find('#taskDateContainer_'+taskID).val();
            taskDate = Date.parse(taskDate);

            var taskDateForm = taskDate.toString("M/d/yyyy");
			var taskTimeForm = taskDate.toString("h:mm tt");

            widgetToolsObj.openTaskForm();

            $('.actionTask').val('edit');
            $('.itemIDTask').val(taskID);
            $('.taskTitle').val(taskTitle);
            $('.taskDateDueWidget').val(taskDateForm);
            $('.taskTimeDue').val(taskTimeForm);
            $('.taskProjectSelect').val(taskProjectID);

            if (taskMilestoneID>0) {
                widgetToolsObj.getMilestonesForProject(taskProjectID,'.taskMilestoneSelect');
            }

            $('.taskMilestoneSelect').val(taskMilestoneID);

			return false;
		});

		$('.deleteTask').unbind().click(function() {
			var taskIDString = $(this).attr('id');
            var taskIDArray = taskIDString.split('_');
            var taskID = taskIDArray[1];
            var status = $(this).attr('status');

			updateTrashObj.sendToTrash('task',taskID);
			$('.containerTask_'+taskID).fadeOut('fast', function() {
                $(this).remove();
                widgetToolsObj.recalculateTaskTotals();
            });
            
			return false;
		});

        $('.chkTask').unbind().click(function() {
           var thisID = $(this).attr('id');
           var thisIDArray = thisID.split('_');
           var taskID = thisIDArray[1];
           var status = $(this).attr('status');
		   $('.containerTask_'+taskID).fadeOut('fast',function() {
               $(this).remove();
           });

           $.get(siteURL+'tasks/TaskUpdate/updateTaskStatus/'+taskID+'/2', function(payload) {
               widgetToolsObj.recalculateTaskTotals();
            });

			return false;
        });

        $('.taskDisplayControl').unbind().click(function() {
            var panelObject = $(this).next();
            genericFunctions.togglePanel(panelObject);
        });

        $('.taskPrint').unbind().click(function() {
            var type = $(this).attr('type');
            var itemIDString = type+'---';
            printObj.goPrint(itemIDString,'Tasks');
            return false;
        });
    },
    openTaskForm : function() {
        $('.taskControlContainer').hide();
        $('.taskFormContainer').fadeIn('fast');
    },
    clearTaskForm : function(action) {
		if (action == 'edit') {
			$('.taskFormContainer').hide();
            $('.taskControlContainer').show();
		}
        var today = new Date();
        var todayDate = today.toString('M/dd/yyyy');
        $('.actionTask').val('add');
        $('.itemIDTask').val('');
        $('.taskTitle').val('');
        $('.taskDateDueWidget').val(todayDate);
        $('.taskTimeDue').val('');
        $('.taskProjectSelect').val('');
    },
    saveTask : function(formObj) {
		var action          = $('.actionTask').val();
        var taskID          = $('.itemIDTask').val();
        var taskTitle       = $('.taskTitle').val();
        var taskDateDue     = $('.taskDateDueWidget').val();
        var taskTimeDue     = $('.taskTimeDue').val();
        var taskProjectID   = $('.taskProjectSelect').val();
        var taskMilestoneID = $('.taskMilestoneSelect').val();
        var taskProject     = $('.taskProjectSelect option:selected').text();

        taskTimeDue = Date.parse(taskTimeDue);
        if (taskTimeDue != null) {
            taskTimeDue     = taskTimeDue.toString('hh:mm tt');
        } else {
            taskTimeDue = '';
        }

        if (action == 'edit') {
            $('.containerTask_'+taskID).hide('fast',function() {
                $(this).remove();
            });
        }

        var formHash = {
            action          : $('.actionTask').val(),
			taskID          : $('.itemIDTask').val(),
            taskTitle       : $('.taskTitle').val(),
            taskDateDue     : $('.taskDateDueWidget').val(),
            taskTimeDue     : taskTimeDue,
            taskProjectID   : taskProjectID,
            taskMilestoneID : taskMilestoneID,
            taskProject     : taskProject
        };

        $.ajax({
            type:  "POST",
            url:   siteURL+'tasks/TaskUpdate/saveTaskFromWidget',
            data: formHash,
			dataType: 'json',
            success: function(payload){
				/*
                 * Figure out if the returned task JSON belongs in the Today group, Overdue group, Tomorrow, This Week, Next Week
                 */
				var groupList = payload.TaskDateText;
				var taskPanel, elementHTML, totalTasks;
				if (payload.Status == 4) {
					$('.taskWidgetContainer.overdue').append('Overdue');
				} else if (groupList.today == true) {
					taskPanel = $('.taskWidgetContainer.today');
					taskPanel.prev().show();
					taskPanel.show();
					elementHTML = widgetRenderObj.renderWidgetTaskElement(payload);
					taskPanel.append(elementHTML);
				} else if (groupList.tomorrow == true) {
					taskPanel = $('.taskWidgetContainer.tomorrow');
					taskPanel.prev().show();
					taskPanel.show();
					elementHTML = widgetRenderObj.renderWidgetTaskElement(payload);
					taskPanel.append(elementHTML);
				} else if (groupList.thisWeek == true) {
					taskPanel = $('.taskWidgetContainer.thisWeek');
					taskPanel.prev().show();
					taskPanel.show();
					elementHTML = widgetRenderObj.renderWidgetTaskElement(payload);
					taskPanel.append(elementHTML);
				} else if (groupList.nextWeek == true) {
					taskPanel = $('.taskWidgetContainer.nextWeek');
					taskPanel.prev().show();
					taskPanel.show();
					elementHTML = widgetRenderObj.renderWidgetTaskElement(payload);
					taskPanel.append(elementHTML);
				}
				widgetToolsObj.attachHandlersTasks();
                widgetToolsObj.recalculateTaskTotals();
				$('.taskWidgetContainer').alternateContainerRowColors().hiliteContainerRow();
                widgetToolsObj.clearTaskForm(action);
            },
            error: function (xhr, desc, exceptionobj) {
                errorObj.ajaxError(xhr,desc,exceptionobj);
            }
        });
    },
    recalculateTaskTotals : function() {
        var statusArray = ['overdue','today','tomorrow','thisWeek','nextWeek'];

        for(var a=0;a<=4;a++) {
            var status = statusArray[a];
            var taskTotal = $('.'+status).children('.row').length;
            $('#'+status+'_totalTasks').html(taskTotal);
        }
    }
}

var widgetLoaderObj = {
   /*
    * NOTE: This method is called down at the bottom of this script
    * in document.ready
    */
	loadWidgetContent : function(widgetName,widgetUser) {
		switch(widgetName) {
			case 'invoices':
                widgetLoaderObj.loadWidgetInvoices(widgetUser);
                break;
			case 'projects':
				widgetLoaderObj.loadWidgetProjects(widgetUser);
				widgetLoaded_projects = true;
				break;
			case 'calendar':
                widgetLoaderObj.loadWidgetCalendar();
                widgetLoaded_calendar = true;
                break;
			case 'timer':
				widgetLoaderObj.loadWidgetTimer();
				widgetLoaded_timer = true;
				break;
			case 'tasks':
                widgetLoaderObj.loadWidgetTasks();
                widgetLoaded_tasks = true;
                break;
            case 'notes':
                getNotesObj.getNotes(1,0,0,0,5,'widgetContent_notes');
                widgetLoaded_notes = true;
                break;
		}
	},
	loadWidgetCalendar : function(month,year) {
        if (typeof month == 'undefined') {
            month = 0;
        }
        if (typeof year == 'undefined') {
            year = 0;
        }
		$.ajax({
		    type: "POST",
		    url:  siteURL+'calendar/CalendarView/renderDashboardCalendar/'+month+'/'+year,
		    success: function(payload){
				widgetRenderObj.renderWidgetCalendar(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
	loadWidgetProjects : function(widgetUser) {
        var getStatus, itemID;
        if (widgetUser == 'client') {
            itemID = $('#clientID').val();
            getStatus = '/all';
        } else {
            itemID = '0';
            getStatus = '';
            widgetUser = 'user';
        }
		$.ajax({
		    type: "POST",
		    url:  siteURL+'projects/ProjectView/getProjectList/'+itemID+'/'+widgetUser+'/0/json/0/1'+getStatus,
		    success: function(payload){
				widgetProjectsObj = eval('(' + payload + ')');
                widgetRenderObj.renderWidgetProjects();	
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });	   
	},
	loadWidgetInvoices : function(widgetUser) {
        if (widgetUser == 'client') {
            var itemID = $('#clientUserid').val();
        } else {
            var itemID = '0';
        }
		$.ajax({
		    type: "POST",
		    url:  siteURL+'finances/FinanceView/getInvoiceList/'+itemID+'/'+widgetUser+'/0/0/json/0',
            dataType: 'json',
		    success: function(payload){
				widgetInvoicesObj = payload;
		    	widgetRenderObj.renderWidgetInvoices();
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	},
	loadWidgetTasks : function() {
		$.ajax({
		    type: "POST",
		    url:  siteURL+'tasks/TaskView/getTaskDigestView/json/0',
            dataType: 'json',
		    success: function(payload){
				widgetTasksObj = payload;
		    	widgetRenderObj.renderWidgetTasks();
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });		
	},
	loadWidgetTimer : function() {
		/*
		 * Look for project JSON
		 */
		if (projectListArray.length<1) {
			/*
			 * Then populate project array
			 */
			$.ajax({
				type:  "POST",
				url:   siteURL+'projects/ProjectView/getProjectsForSelect',
                dataType: 'json',
				success: function(payload) {
					projectListArray = payload;
					widgetRenderObj.renderWidgetTimer();
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
		} else {
			widgetRenderObj.renderWidgetTimer();
		}
	}
}

var widgetRenderObj = {
	renderWidgetCalendar : function(content) {
		$('#widgetContent_calendar').html(content);        

		/*
		 * Attach calendar controls
		 */
		widgetToolsObj.attachEventPopups();

		$('.changeMonth').click(function() {
			var dateCode = $(this).attr('dateCode');
			var dateCodeArray = dateCode.split('_');
			widgetLoaderObj.loadWidgetCalendar(dateCodeArray[0],dateCodeArray[1]);
			return false;
		});

		/*
		 * Highlight current day
		 */
		var d = new Date();
		var currentDay = d.getDate();
		$('.dayNum').each(function() {
			if ($(this).html() == currentDay) {
				$(this).parents('td').addClass('Today');
			}
		});		

		$("#eventDateStartWidget").datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

		$('#buttonSaveEvent').click(function() {
			updateCalendarObj.saveEvent('widget');
			widgetToolsObj.clearEventForm();
			return false;
		});

		$('.saverField').bind('keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
				updateCalendarObj.saveEvent('widget');
                return false;
            }
        });

		$('#buttonCancelEvent').click(function() {
			widgetToolsObj.clearEventForm();
            $('#formCalendarWidget').slideUp('fast');
            $('#linkEventAdd').show();
			return false;
		});

		$('#linkEventAdd').click(function() {
			$('#formCalendarWidget').slideDown('fast');
			$('#linkEventAdd').hide();

			/*
			 * Set default date
			 */
			var today = new Date();
			var todayDate = today.toString('M/dd/yyyy');
            
            widgetToolsObj.clearEventForm();
			$('#eventDateStartWidget').val(todayDate);
			return false;
		});

		$('.DateBox.widget').click(function() {
            widgetToolsObj.clearEventForm();
			$('#formCalendarWidget').slideDown('fast');
			$('#linkEventAdd').hide();
			var todayNumber = $(this).find('.dayNum').html();
			var currentDate = $('#widgetCalendarCurrentDate').val();
			var currentDateArray = currentDate.split('-');
			var selectedDate = new Date(currentDateArray[0],currentDateArray[1]-1,todayNumber);
			selectedDate = selectedDate.toString('M/dd/yyyy');
			$('#eventDateStartWidget').val(selectedDate);
			return false;
		});

		$('textarea.expanding').elastic();
	},
    renderWidgetInvoices : function() {
        var jsonObject    = widgetInvoicesObj;
	    var totalInvoices = jsonObject.Invoices.length;
        var invoices      = jsonObject.Invoices;
        var invoicesTotal = jsonObject.TotalInvoiced;

        var stringOut = [];
		stringOut.push('<table class="dataTable" id="invoiceGrid">');
		for(a=0;a<=((invoices.length)-1);a++) {
			var invoiceID         = invoices[a].InvoiceID;
			var invoiceNo         = invoices[a].InvNo;
			var invoiceTitle      = invoices[a].Title;
			var invoiceTotal      = invoices[a].InvTotal;
			var invStatusHuman    = invoices[a].StatusHuman;
			var invStatusMachine  = invoices[a].StatusMachine;
			var clientID          = invoices[a].ClientID;
			var clientCompany     = invoices[a].Company;
			stringOut.push('<tr>');
			stringOut.push('<td style="vertical-align: top; width: 25px;"><span class="'+invStatusMachine+' statusProject" style="padding-left: 0px;" title="'+invStatusHuman+'" statusDesc="'+invStatusHuman+'"></span></td>');
			stringOut.push('<td>');
			stringOut.push('<div><a href="'+siteURL+'finances/FinanceView/viewInvoice/'+invoiceID+'"><b>'+invoiceNo+'</b> '+invoiceTitle+'</a> ');
            if (clientCompany) {
                stringOut.push('for <a href="'+siteURL+'clients/ClientDetail/index/'+clientID+'">'+clientCompany+'</a>');
            }
            stringOut.push('</div>');
            stringOut.push('<div style="float: left;"><span class="subText">'+invStatusHuman+'</span>.</div>');
            stringOut.push('<div style="float: right;"><strong>'+invoiceTotal+'</strong></div>');
            stringOut.push('<div style="clear: both;"></div>');
            stringOut.push('</td>');
			stringOut.push('</tr>');
		}
        stringOut.push('<tfoot><tr><td colspan="2" style="text-align: right;">'+commonTerms['total']+' '+invoicesTotal+'</td></tr></tfoot>');
		stringOut.push('</table>');
        var stringHTML = stringOut.join('');
        
        if (totalInvoices<1) {
            stringHTML = '<div class="boxYellow"><div class="infoMessageSmall">'+jsonObject.NoDataMessage+'</div></div>';
        }

		$('#widgetContent_invoices').html(stringHTML);
		$('#invoiceGrid').alternateTableRowColors().hiliteTableRow();
    },
	renderWidgetProjects : function() {
        var jsonObject    = widgetProjectsObj;
	    var totalProjects = jsonObject.Projects.length;
        var projects      = jsonObject.Projects;

        var stringOut = [];
		stringOut.push('<table class="dataTable" id="projectGrid">');
		for(a=0;a<=((projects.length)-1);a++) {
			var projID            = projects[a].ProjectID;
			var projTitle         = projects[a].Title;
			var projEndDate       = projects[a].Date_End;
			var projEndDateSort   = projects[a].Date_End_Sort;
			var projStatus        = projects[a].Status;
			var projStatusHuman   = projects[a].StatusHuman;
			var projStatusMachine = projects[a].StatusMachine;
			var projStatusText    = projects[a].StatusText;
			var projHours         = projects[a].ProjectTime;
			var clientID          = projects[a].ClientID;
			var clientCompany     = projects[a].Company;
			stringOut.push('<tr>');
			stringOut.push('<td style="vertical-align: top; width: 25px;"><span class="'+projStatusMachine+' statusProject" style="padding-left: 0px;" title="'+projStatusHuman+'" statusDesc="'+projStatusHuman+'"></span></td>');
			stringOut.push('<td>');
			stringOut.push('<a href="'+siteURL+'projects/ProjectDetail/index/'+projID+'">'+projTitle+'</a>');
            if (clientCompany != null && clientCompany != '') {
                stringOut.push(' for <a href="'+siteURL+'clients/ClientDetail/index/'+clientID+'">'+clientCompany+'</a>');
            }
            stringOut.push('<br /><span class="subText">'+projStatusText+'</span>.');
			stringOut.push('</td>');
			stringOut.push('</tr>');
		}	
		stringOut.push('</table>');
        var stringHTML = stringOut.join('');

        if (totalProjects<1) {
            stringHTML = '<div class="boxYellow"><div class="infoMessageSmall">'+jsonObject.NoDataMessage+'</div></div>';
        }

		$('#widgetContent_projects').html(stringHTML);
		$('#projectGrid').alternateTableRowColors().hiliteTableRow();
	},
	renderWidgetTasks : function() {
		var jsonObject = widgetTasksObj;

        var totalTasksOverdue, totalTasksToday, totalTasksTomorrow, totalTasksThisWeek, totalTasksNextWeek, totalTasks = 0;
        if (jsonObject.TasksOverdue) {
            totalTasksOverdue  = jsonObject.TasksOverdue.length;
        }
        if (jsonObject.TasksToday) {
            totalTasksToday    = jsonObject.TasksToday.length;
        }
        if (jsonObject.TasksTomorrow) {
            totalTasksTomorrow = jsonObject.TasksTomorrow.length;
        }
        if (jsonObject.TasksThisWeek) {
            totalTasksThisWeek = jsonObject.TasksThisWeek.length;
        }
        if (jsonObject.TasksNextWeek) {
            totalTasksNextWeek = jsonObject.TasksNextWeek.length;
        }        

        var panelDisplayDefault = 'block';
        if (jsonObject.TotalTasks>10) {
            panelDisplayDefault = 'none';
        }
        var stringOut = [], header;

        stringOut.push('<div class="boxBlue" style="margin-bottom: 6px;"><div style="padding: 3px;">');
		stringOut.push('<div class="taskControlContainer">');
            stringOut.push('<a href="#" class="icon_tasks_small_add linkNewTask">'+commonTerms['new_task']+'</a>');
        stringOut.push('</div>');

            /*
             * New quick task form
             */
            /*
             * Set current date
             */
            var today = new Date();
            var todayDate = today.toString('M/dd/yyyy');
            var todayTime = today.toString('h:mm tt');
            stringOut.push('<div class="taskFormContainer" style="width: 99%; display: none;">');
            stringOut.push('<input type="hidden" class="actionTask" value="add" />');
            stringOut.push('<input type="hidden" class="itemIDTask" value="" />');
            stringOut.push('<div class="padBottom6"><strong>'+commonTerms['task']+':</strong><br /><input type="text" class="taskTitle saverFieldTask" style="width: 95%;" /></div>');
            stringOut.push('<div class="padBottom6"><strong>'+commonTerms['when']+':</strong><br /><input class="taskDateDueWidget saverFieldTask" type="text" style="width: 100px;" value="'+todayDate+'" /> &nbsp;<strong>at</strong>&nbsp;&nbsp;');
            stringOut.push('<input class="taskTimeDue clearOnFocus saverFieldTask" type="text" style="width: 70px;" value="'+todayTime+'" /></div>');
            stringOut.push('<div class="padBottom6"><strong>'+commonTerms['select_project']+':</strong><br /><select class="taskProjectSelect" style="width: 97%;"></select></div>');
            stringOut.push('<div class="padBottom6"><strong>'+commonTerms['selectMilestone']+':</strong><br /><select class="taskMilestoneSelect" disabled="disabled" style="width: 97%;"></select></div>');
            stringOut.push('<button class="smallButton saveTask" style="float: left; margin-right: 6px;"><span class="save">'+commonTerms['save']+'</span></button>');
            stringOut.push('<button class="smallButton cancelTask" style="float: left;"><span class="cancel">'+commonTerms['cancel']+'</span></button>');
            stringOut.push('<div style="clear: left;"></div>');
            stringOut.push('</div>');
            stringOut.push('</div></div>');

		var controlDisplay;
		if (totalTasksOverdue>0) {
			controlDisplay = '';
            totalTasks = parseInt(totalTasks+totalTasksOverdue);
		} else {
			controlDisplay = 'none';
		}
		var tasksOverdue = jsonObject.TasksOverdue;
		header       = jsonObject.TasksOverdueHeader;

        var status = 'overdue';

		stringOut.push('<div class="boxYellow clickable taskDisplayControl" style="display: '+controlDisplay+';">');
        stringOut.push('<div class="status_past_due" style="margin: 3px; float: left;"><strong class="redText"><span id="'+status+'_totalTasks" class="redText">'+totalTasksOverdue+'</span> '+header+'</strong></div>');
        stringOut.push('<!--<a href="#" class="iconPrintSmall taskPrint" style="float: right;" status="'+status+'"></a>-->');
        stringOut.push('<div style="clear: both;"></div>');
        stringOut.push('</div>');
		stringOut.push('<div class="taskWidgetContainer '+status+'" status="'+status+'" style="display: '+panelDisplayDefault+';">');
		for(a=0;a<=((totalTasksOverdue)-1);a++) {
			stringOut.push(widgetRenderObj.renderWidgetTaskElement(tasksOverdue[a],status));
		}
		stringOut.push('</div>');
		
        if (totalTasksToday>0) {
			controlDisplay = '';
            totalTasks = parseInt(totalTasks+totalTasksToday);
		} else {
			controlDisplay = 'none';
		}
		var tasksToday = jsonObject.TasksToday;
		header         = jsonObject.TasksTodayHeader;

        status = 'today';

		stringOut.push('<div class="boxYellow clickable taskDisplayControl" style="margin-top: 6px; display: '+controlDisplay+';">');
        stringOut.push('<div class="icon_calendar_today" style="margin: 3px; float: left;"><strong><span id="'+status+'_totalTasks">'+totalTasksToday+'</span> '+header+'</strong></div>');
        stringOut.push('<!--<a href="#" class="iconPrintSmall taskPrint" style="float: right;" status="'+status+'"></a>-->');
        stringOut.push('<div style="clear: both;"></div>');
        stringOut.push('</div>');
		stringOut.push('<div class="taskWidgetContainer '+status+'" status="'+status+'" style="border-top: none; display: '+panelDisplayDefault+';">');
		for(a=0;a<=((totalTasksToday)-1);a++) {
			stringOut.push(widgetRenderObj.renderWidgetTaskElement(tasksToday[a],status));
		}
		stringOut.push('</div>');
		
        if (totalTasksTomorrow>0) {
			controlDisplay = '';
            totalTasks = parseInt(totalTasks+totalTasksTomorrow);
		} else {
			controlDisplay = 'none';
		}
		var tasksTomorrow = jsonObject.TasksTomorrow;
		header            = jsonObject.TasksTomorrowHeader;

        status = 'tomorrow';

		stringOut.push('<div class="boxYellow clickable taskDisplayControl" style="margin-top: 6px; display: '+controlDisplay+';">');
        stringOut.push('<div class="icon_calendar_day" style="margin: 3px; float: left;"><strong><span id="'+status+'_totalTasks">'+totalTasksTomorrow+'</span> '+header+'</strong></div>');
        stringOut.push('<!--<a href="#" class="iconPrintSmall taskPrint" style="float: right;" status="'+status+'"></a>-->');
        stringOut.push('<div style="clear: both;"></div>');
        stringOut.push('</div>');
		stringOut.push('<div class="taskWidgetContainer '+status+'" status="'+status+'" style="display: '+panelDisplayDefault+';">');
		for(a=0;a<=((totalTasksTomorrow)-1);a++) {
			stringOut.push(widgetRenderObj.renderWidgetTaskElement(tasksTomorrow[a],status));
		}
		stringOut.push('</div>');
		
        if (totalTasksThisWeek>0) {
			controlDisplay = '';
            totalTasks = parseInt(totalTasks+totalTasksThisWeek);
		} else {
			controlDisplay = 'none';
		}
		var tasksThisWeek = jsonObject.TasksThisWeek;
		header        = jsonObject.TasksThisWeekHeader;

        status = 'thisWeek';

		stringOut.push('<div class="boxYellow clickable taskDisplayControl" style="margin-top: 6px; display: '+controlDisplay+';">');
        stringOut.push('<div class="icon_calendar_day" style="margin: 3px; float: left;"><strong><span id="'+status+'_totalTasks">'+totalTasksThisWeek+'</span> '+header+'</strong></div>');
        stringOut.push('<!--<a href="#" class="iconPrintSmall taskPrint" style="float: right;" status="'+status+'"></a>-->');
        stringOut.push('<div style="clear: both;"></div>');
        stringOut.push('</div>');
		stringOut.push('<div class="taskWidgetContainer '+status+'" status="'+status+'" style="display: '+panelDisplayDefault+';">');
		for(a=0;a<=((totalTasksThisWeek)-1);a++) {
			stringOut.push(widgetRenderObj.renderWidgetTaskElement(tasksThisWeek[a],status));
		}
		stringOut.push('</div>');
		
        if (totalTasksNextWeek>0) {
			controlDisplay = '';
            totalTasks = parseInt(totalTasks+totalTasksNextWeek);
		} else {
			controlDisplay = 'none';
		}
		var tasksNextWeek = jsonObject.TasksNextWeek;
		header        = jsonObject.TasksNextWeekHeader;

        status = 'nextWeek';

		stringOut.push('<div class="boxYellow clickable taskDisplayControl" style="margin-top: 6px; display: '+controlDisplay+';">');
        stringOut.push('<div class="icon_calendar_day" style="margin: 3px; float: left;"><strong><span id="'+status+'_totalTasks">'+totalTasksNextWeek+'</span> '+header+'</strong></div>');
        stringOut.push('<!--<a href="#" class="iconPrintSmall taskPrint" style="float: right;" status="'+status+'"></a>-->');
        stringOut.push('<div style="clear: both;"></div>');
        stringOut.push('</div>');
		stringOut.push('<div class="taskWidgetContainer '+status+'" status="'+status+'" style="display: '+panelDisplayDefault+';">');
		for(a=0;a<=((totalTasksNextWeek)-1);a++) {
			stringOut.push(widgetRenderObj.renderWidgetTaskElement(tasksNextWeek[a],status));
		}
		stringOut.push('</div>');
        var stringHTML = stringOut.join('');

        if (totalTasks<1) {
            stringHTML = '<div class="boxYellow"><div class="infoMessageSmall">'+jsonObject.NoDataMessage+'</div></div>';
        }
        
		$('#widgetContent_tasks').html(stringHTML);
        $('.taskWidgetContainer').alternateContainerRowColors().hiliteContainerRow();

        widgetToolsObj.attachHandlersTasks();

        /*
		 * Look for project JSON
		 */
		if (projectListArray.length<1) {
			/*
			 * Then populate project array
			 */
			$.ajax({
				type:  "POST",
				url:   siteURL+'projects/ProjectView/getProjectsForSelect',
                dataType: 'json',
				success: function(payload) {
					projectListArray = payload;
                    widgetToolsObj.attachHandlersTaskForm();
				}
			});
		} else {
            widgetToolsObj.attachHandlersTaskForm();
        }
	},
    renderWidgetTaskElement : function(taskArray,status) {
        var widgetHTML = [];
        widgetHTML.push('<div class="containerTask_'+taskArray.TaskID+' row" style="width: 98%;">');
        widgetHTML.push('<input type="hidden" id="milestoneIDContainer_'+taskArray.TaskID+'" value="'+taskArray.ParentTaskID+'" />');
        widgetHTML.push('<input type="hidden" id="projectIDContainer_'+taskArray.TaskID+'" value="'+taskArray.ProjectID+'" />');
        widgetHTML.push('<input type="hidden" id="taskDateContainer_'+taskArray.TaskID+'" value="'+taskArray.DateEndRender+'" />');
        
        widgetHTML.push('<span>');
        if (taskArray.TaskNo>0) {
            widgetHTML.push('<strong>['+taskArray.TaskNo+']</strong> ');
        }
        widgetHTML.push('<a href="'+taskArray.TaskLink+'" id="taskTitle_'+taskArray.TaskID+'">'+taskArray.Title+'</a>');
        if (taskArray.ProjectID>0) {
            widgetHTML.push(' for <a href="'+siteURL+'projects/ProjectDetail/index/'+taskArray.ProjectID+'">'+taskArray.ProjectTitle+'</a></span>');
        }
        widgetHTML.push('<br /><a class="buttonEditSmall editTask" status="'+status+'" id="editTask_'+taskArray.TaskID+'" title="'+commonTerms['edit']+'" href="#"></a>');
        widgetHTML.push('<a class="buttonDeleteSmall deleteTask" status="'+status+'" id="deleteTask_'+taskArray.TaskID+'" title="'+commonTerms['delete']+'" href="#"></a>');
        widgetHTML.push('<a class="buttonCheckSmall chkTask" status="'+status+'" id="chkTask_'+taskArray.TaskID+'" href="#" title="'+commonTerms['complete']+'"></a> ');
        widgetHTML.push('<span class="subText">'+taskArray.StatusText+' ('+taskArray.DateEndRender+')</span></div>');
        var returnWidgetHTML = widgetHTML.join('');
        return returnWidgetHTML;
    },
	renderWidgetTimer : function(jsonString) {
		var timerContainerStyle = '';
		if ($('#pageOneColumn').attr('pageID') != 'dashboard') {
			timerContainerStyle = 'style="overflow: auto; height: 255px;"';
		}
		var outString = [];
		outString.push('<div class="boxYellowInner">');
		outString.push('<select id="selectTimerProject" style="margin-bottom: 6px; width: 250px;"></select><br />');
		outString.push('<select id="selectTimerTask" disabled="disabled" style="width: 250px;"></select><br />');
		outString.push('<a href="'+siteURL+'timesheets/TimesheetView" class="iconAppTable" style="margin-top: 6px; font-weight: normal;">'+commonTerms['view_timesheets']+'</a>');
		outString.push('</div>');
		outString.push('<div id="activeTimerContainer" class="contentContainer" '+timerContainerStyle+'></div>');
		var outStringFinal = outString.join('');
		$('#widgetContent_timer').html(outStringFinal);

		$('#selectTimerProject').append('<option value="0">'+commonTerms['select_project']+'</option><option value="0"></option>');
		for(var a=0;a<=(projectListArray.length-1);a++) {
			$('#selectTimerProject').append('<option value="'+projectListArray[a].ProjectID+'">'+projectListArray[a].Title+'</option>');
		}

		/*
		 * Are there any timers already in cookie?
		 */
		var cookieTimers = widgetToolsObj.objectifyCookie('jobTimers');
		if (cookieTimers != false) {
            var bb=0;

			for(var b=0;b<=(cookieTimers.length-1);b++) {
                if (cookieTimers[b].Userid == numbers[0]) {
                    var timerElementHash = {
                        elementID   : cookieTimers[bb].ElementID,
                        projectName : cookieTimers[bb].Project,
                        taskName    : cookieTimers[bb].Task,
                        projectID   : cookieTimers[bb].ProjectID,
                        taskID      : cookieTimers[bb].TaskID,
                        timerRunning: cookieTimers[bb].TimerRunning,
                        elapsedTime : cookieTimers[bb].ElapsedTime,
                        timeSheetID : cookieTimers[bb].TimeSheetID
                    }
                    widgetRenderObj.renderWidgetTimerElement(timerElementHash);
                    bb++;
                }				
			}
		}
		
		$('#selectTimerProject').change(function() {
			var projectID = $(this).val();
			$.get(siteURL+'tasks/TaskView/getTasksForSelect/'+projectID+'/json/0', function(payload) {
				var jsonObject   = eval('(' + payload + ')');
				var optionString = '';
                var tasks;

				$('#selectTimerTask').html('');
				if (jsonObject.length > 0) {
                    optionString  = '<option value="0">'+commonTerms['select_task']+'</option><option value="0"></option>';
					optionString += '<option value="project" class="emph">'+commonTerms['add_project_only']+'</option>';
                    optionString += '<option value="0"></option>';
					$(optionString).appendTo('#selectTimerTask');

					for(var a=0;a<jsonObject.length;a++) {
                        if (jsonObject[a].Milestone == 1) {
                            optionString = '<option value="'+jsonObject[a].TaskID+'" class="emph">'+jsonObject[a].Title+'</option>';
                            tasks = jsonObject[a].Tasks;
                            for(var b=0;b<tasks.length;b++) {
                                optionString += '<option value="'+tasks[b].TaskID+'" class="indent">'+tasks[b].Title+'</option>';
                            }
                        } else {
                            //optionString = '<option value="'+jsonObject[a].TaskID+'" class="indent">'+jsonObject[a].Title+'</option>';
                        }
						$(optionString).appendTo('#selectTimerTask');
					}

					$('#selectTimerTask').attr('disabled','');
				} else {
					$('#selectTimerTask').attr('disabled','disabled');
					var timerElementHash = {
                        elementID   : projectID,
						projectName : $('#selectTimerProject :selected').text(),
						taskName    : '',
						projectID   : projectID,
						taskID      : 0,
						timerRunning: 0,
						elapsedTime : '00:00:00',
						timeSheetID : '0'
					}
                    widgetRenderObj.renderWidgetTimerElement(timerElementHash);
				}
			});
		});

		$('#selectTimerTask').change(function() {
            var projectID = $('#selectTimerProject').val();
			var taskID = $(this).val();
			if (taskID != 0) {
				if (taskID == 'project') {
					var taskName = '';
					var elementID = projectID;
				} else {
					var taskName = $('#selectTimerTask :selected').text();
					var elementID = projectID+taskID+'000';
				}
				var timerElementHash = {
                    elementID   : elementID,
					projectName : $('#selectTimerProject :selected').text(),
					taskName    : taskName,
					projectID   : projectID,
					taskID      : taskID,
					timerRunning: 0,
					elapsedTime : '00:00:00',
					timeSheetID : '0'
				}
				widgetRenderObj.renderWidgetTimerElement(timerElementHash);
			}
		});

        $('#activeTimerContainer').alternateContainerRowColors();
        $('#activeTimerContainer textarea.expanding').elastic();
	},
	renderWidgetTimerElement : function(timerElementHash) {
		/*
		 * Make sure we don't already have a time instance for this project/task
		 */
		var elementID   = timerElementHash.elementID;

		var thisTimer = false;
		$('#activeTimerContainer div').each(function() {
			if ($(this).attr('id') == elementID) {
				thisTimer = true;
			}
		});

		if (thisTimer == false) {
			var projectName = timerElementHash.projectName;
			var taskName    = timerElementHash.taskName;
			var projectID   = timerElementHash.projectID;
			var taskID      = timerElementHash.taskID;
			var timerRunning= timerElementHash.timerRunning;
			var elapsedTime = timerElementHash.elapsedTime;
			var timeSheetID = timerElementHash.timeSheetID;
		
			if (taskName != '' && taskName != 'null') {
				var projectString = '<span class="projectName clickable">'+projectName+'</span> / <span class="taskName clickable">'+taskName+'</span>';
			} else {
				var projectString = '<span class="projectName clickable">'+projectName+'</span>';
			}

			/*
			 * Figure out elapsed time
			 */
			var elapsedTimeDecimal = widgetToolsObj.widgetTimerElapsedTimeDecimal(elapsedTime);

			var buttonClass = 'buttonRoundPlay';
			if (timerRunning == 1) {
				buttonClass = 'buttonRoundStop';
			}

			if (timerRunning == 0 && elapsedTimeDecimal > 0) {
				var entryFormDisplay = '';
			} else {
				var entryFormDisplay = 'display: none;';
			}
			var outString = [];
			outString.push('<div class="row timerElement" style="width: 100%;" id="'+elementID+'">');
			outString.push('<input type="hidden" class="projectID" id="projectID_'+elementID+'" value="'+projectID+'" />');
			outString.push('<input type="hidden" class="taskID" id="taskID_'+elementID+'"  value="'+taskID+'" />');
			outString.push('<input type="hidden" class="timerRunning" id="timerRunning_'+elementID+'" value="'+timerRunning+'" />');
			outString.push('<input type="hidden" class="timeSheetID" id="timeSheetID_'+elementID+'" value="'+timeSheetID+'" />');
			outString.push('<a href="#" class="buttonDeleteSmall removeTimer" style="margin: 0 15px 0 5px;" id="remove_'+elementID+'" title="Remove"></a>');
			outString.push('<strong class="titleContainer">'+projectString+'</strong><br />');
			outString.push('<a href="#" style="margin: 6px 0  6px;" class="'+buttonClass+' timerClockReadout" id="timerDisplayControl_'+elementID+'" title="Start timer">'+elapsedTime+'</a>');
			outString.push('<div style="'+entryFormDisplay+'" class="finalizeContainer" id="finalizePanel_'+elementID+'">');
			outString.push('<span class="icon_comment_small">'+commonTerms['message_comments']+'</span><div><textarea id="comments_'+elementID+'" style="width: 230px;" class="expanding"></textarea><div>');
			outString.push('<div style="margin: 6px 0 6px; float: left;"><input type="text" style="width: 40px;" id="elapsedTime_'+elementID+'" value="'+elapsedTimeDecimal+'" /> '+commonTerms['elapsed_time']+'</div>');
			outString.push('<div style="float: left; margin: 8px 0 0 12px;"><input type="checkbox" id="billable_'+elementID+'" checked="checked" /> '+commonTerms['billable']+'</div>');
			outString.push('<div style="clear: left;"></div>');
			outString.push('<button class="smallButton buttonSaveEntry" elementID="'+elementID+'" id="buttonSave_'+elementID+'" style="margin-right: 6px; float: left;"><span class="save">'+commonTerms['save']+'</span></button>');
			outString.push('<button class="smallButton buttonClearEntry" elementID="'+elementID+'" id="buttonClear_'+elementID+'" style="float: left;"><span class="clearTimer">'+commonTerms['remove']+'</span></button>');
			outString.push('<a href="#" class="icon_invoice_small createInvoice" elementID="'+elementID+'" id="linkCreateInvoice_'+elementID+'" style="display: block; margin: 4px 0 0 18px; float: left;">'+commonTerms['create_invoice']+'</a>');
			outString.push('<div style="clear: left;"></div>');
            outString.push('<div id="containerMessage_'+elementID+'" style="margin-top: 6px;"></div>');
            outString.push('<div id="containerInvoice_'+elementID+'" style="margin-top: 6px;"></div>');

			outString.push('</div>');
			outString.push('</div>');
			var outStringFinal = outString.join('');
			$('#activeTimerContainer').append(outStringFinal);

			/*
			 * Do event handlers
			 */
			$('#timerDisplayControl_'+elementID).click(function() {
				var elementID = $(this).parent('div').attr('id');
				if ($(this).hasClass('buttonRoundPlay')) {
				   widgetToolsObj.widgetTimerToggle(elementID,'start');
				} else if ($(this).hasClass('buttonRoundStop')) {
				   widgetToolsObj.widgetTimerToggle(elementID,'stop');
				}
				return false;
			});

			$('.buttonSaveEntry').click(function() {
				var elementID = $(this).attr('elementID');
				widgetToolsObj.widgetTimerLogToDatabase(elementID,'save');
			});
		
			$('.buttonClearEntry').click(function() {
				/*
				 * Just remove timer entry without updating database
				 */
				var elementID = $(this).attr('elementID');
				$('#'+elementID).fadeOut('slow', function() {
					$(this).remove();
					widgetToolsObj.widgetTimerRebuildJSONCookie();
				});
			});

			$('.removeTimer').click(function() {
				var elementID = $(this).parent('div').attr('id');
				$('#'+elementID).fadeOut('slow', function() {
					$(this).remove();
					widgetToolsObj.widgetTimerRebuildJSONCookie();
					/*
					 * Delete entry from database as well
					 */
					widgetToolsObj.widgetTimerLogToDatabase(elementID,'delete');
				});
                return false;
			});

			$('#linkCreateInvoice_'+elementID).click(function() {
				var elementID = $(this).attr('elementID');
				var timeSheetID = $('#timeSheetID_'+elementID).val();
				$.ajax({
                    type:  "POST",
                    url:   siteURL+'finances/InvoiceUpdate/createInvoiceFromTimesheet/'+timeSheetID,
                    dataType: 'json',
                    success: function(payload) {
                        var outString = '<strong><a href="'+siteURL+'finances/InvoiceUpdate/index/'+payload['InvoiceID']+'" class="icon_invoice_small" style="margin: 3px;">'+commonTerms['viewInvoiceNumber']+' '+payload['InvoiceNo']+' for '+payload['Company']+'</a></strong>';
                        $('#containerInvoice_'+elementID).html(outString);
                    },
                    error: function (xhr, desc, exceptionobj) {
                        errorObj.ajaxError(xhr,desc,exceptionobj);
                    }
                });

                return false;
			});

            $('.projectName').unbind('click').click(function() {
                window.location = siteURL+'projects/ProjectDetail/index/'+projectID;
            });

            $('.taskName').unbind('click').click(function() {
                window.location = siteURL+'projects/ProjectDetail/index/'+projectID+'#windowTasks';
            });
		}
		if (timerRunning == 1) {
			widgetToolsObj.widgetTimerControl(elementID,'start');
		}
        widgetToolsObj.widgetTimerRebuildJSONCookie();
	},
    widgetMenu : function(obj) {
          /*
           * We need to add the event handler here because once
           * html is added to a qtip, event handlers seem to
           * vanish.
           */
		if ($('#pageOneColumn').attr('pageID') == 'dashboard') {
            /*
             * Also see reference to buttonaddDashboardWidgets in jsDashboardWidgets.js ~line 75.
             */
			var widgetMenuAPI = $('#buttonaddDashboardWidgets').qtip('api');
			var type = 'dashboard';
		} else {
			var widgetMenuAPI = $('#addFooterWidgets').qtip('api');
			var type = 'footer';
		}
        var widgetID = $(obj).attr('widgetID');
        var widgetId = $(obj).attr('id');
		var widgetNameArray = widgetId.split('_');
		var widgetName = widgetNameArray[1];

		$.ajax({
		    type: "POST",
		    url:  siteURL+'dashboard/DashboardOwners/addWidget/'+widgetID+'/'+widgetName+'/'+type,
		    success: function(payload){
				if ($('#pageOneColumn').attr('pageID') == 'dashboard') {
					 /*
					  * Place widget on screen
					  */
					  $('#column1').prepend(payload);
					  widgetLoaderObj.loadWidgetContent(widgetName);
					  dashboardOwners.init();
				} else {
					/*
					 * Reload footer widget icons
					 */
					footerWidgets = eval('(' + payload + ')');
					pageInitObj.footerWidgets();
				}
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	    $('#activate_'+widgetName).hide();
	    $('#active_'+widgetName).show();
        widgetMenuAPI.updateContent($('#widgetSelector').html());
     }
}

$(document).ready(function() {
    if (gadget != true) {
         /*
         * Show wait spinners in each widget until content loads
         */
        $('.dashboardWidget p').systemMessage({
            status: 'wait',
            size:   'Big',
            message: commonTerms['loading']
        });

        /*
         * Remove active widgets from available widget list
         */
        $('.widget').each(function() {
            var widgetName = $(this).attr('widgetName');
            var widgetUser = $(this).attr('widgetUser');
            $('#activate_'+widgetName).hide();
            $('#active_'+widgetName).show();

            /*
             * While we're at it, let's load widget content
             */
            widgetLoaderObj.loadWidgetContent(widgetName,widgetUser);
        });

        /*
         * Remove unauthorized widgets from dashboard and from
         * available widget list.
         */
        if (permJSON['projectViewAll'] != 1 && permJSON['projectViewOwn'] != 1) {
            $('.widget').each(function() {
                if ($(this).attr('widgetname') == 'projects') {
                    $(this).hide();
                    $('#activate_projects').hide();
                    $('#active_projects').hide();
                }
            });
        }

        if (permJSON['invoiceViewAll'] != 1 && permJSON['invoiceViewOwn'] != 1) {
            $('.widget').each(function() {
                if ($(this).attr('widgetname') == 'invoices') {
                    $(this).hide();
                    $('#activate_invoices').hide();
                    $('#active_invoices').hide();
                }
            });
        }
        if (permJSON['taskViewAll'] != 1 && permJSON['taskViewOwn'] != 1) {
            $('.widget').each(function() {
                if ($(this).attr('widgetname') == 'tasks') {
                    $(this).hide();
                    $('#activate_tasks').hide();
                    $('#active_tasks').hide();
                }
            });
        }


        /*
         * Create the dashboard
         */
        if ($('#pageOneColumn').attr('pageID') == 'dashboard') {
            dashboardOwners.init();
        }

        /*
         * Check for running job timers if we're not logged
         * in as a client.
         */
        if (numbers[3] != USER_TYPE_CLIENT) {
            /*
             * If we're NOT on the dashboard...load the timer.
             */
            if ($('#pageOneColumn').attr('pageID') != 'dashboard') {
                widgetLoaderObj.loadWidgetContent('timer');
            }
        }
    }
});	