var getReportDataObj = {
    getReportInvoicedReceived : function(year,gridOnly) {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'finances/FinanceReports/reportInvoicedReceived/'+year+'/json/0',
			dataType: 'json',
		    success: function(payload){
                renderReportObj.renderReportInvoicedReceived(payload,gridOnly);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getReportInvoicedByProjectClient : function(reportDateStart,reportDateEnd,byWhat) {
        var formHash = {
          reportDateStart : reportDateStart,
          reportDateEnd : reportDateEnd
        };

        $.ajax({
		    type:  "POST",
		    url:   siteURL+'finances/FinanceReports/reportInvoicedByProjectClient/'+byWhat+'/json/0',
			dataType: 'json',
            data: formHash,
		    success: function(payload){
                renderReportObj.renderReportInvoicedByProjectClient(payload,byWhat);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getIncomeExpenses : function(year) {
        $.ajax({
		    type:  "POST",
		    url:   siteURL+'finances/FinanceReports/reportIncomeExpenses/'+year+'/json/0',
			dataType: 'json',
		    success: function(payload){
                renderReportObj.renderReportIncomeExpenses(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    },
    getReportExpenses : function(reportDateStart,reportDateEnd,byWhat) {
        var formHash = {
          reportDateStart : reportDateStart,
          reportDateEnd : reportDateEnd
        };

        $.ajax({
		    type:  "POST",
		    url:   siteURL+'finances/FinanceReports/reportExpensesBySegment/'+byWhat+'/json/0',
			dataType: 'json',
            data: formHash,
		    success: function(payload){
                renderReportObj.renderReportExpensesBySegment(payload,byWhat);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
    }
}

var renderReportObj = {
    renderReportInvoicedReceived : function(jsonObject,gridOnly) {
        var chartData = jsonObject.ChartData;
        var gridData = jsonObject.GridData;
        if (gridOnly != 1) {
            var tmpChart = findSWF("chartContainer");
            var x = tmpChart.load( JSON.stringify(chartData) );
        }
        var stringOut = [];
        var headerArray   = gridData.HeaderLabels.split('|');
        stringOut.push('<table class="dataTable" id="reportGrid">');
        stringOut.push('<thead><tr><th>'+headerArray[0]+'</th><th style="text-align: right;">'+headerArray[1]+'</th><th style="text-align: right;">'+headerArray[2]+'</th></tr></thead>');
        stringOut.push('<tbody>');
        var income, expenses, month, monthNum;

        for(a=0;a<=11;a++) {
            stringOut.push('<tr>');
            stringOut.push('<td>'+gridData.Invoiced[a].Month+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData.Invoiced[a].Total+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData.Received[a].Total+'</td>');
            stringOut.push('</tr>');
        }

        stringOut.push('</tbody>');
        stringOut.push('<tfoot><tr>');
        stringOut.push('<td>'+commonTerms['total']+'</td>');
        stringOut.push('<td style="text-align: right;">'+gridData.TotalInvoiced+'</td>');
        stringOut.push('<td style="text-align: right;">'+gridData.TotalReceived+'</td>');
        stringOut.push('</tr></tfoot>')
        stringOut.push('</table>');
        var stringOutFinal = stringOut.join('');
        $('#gridContainer').html(stringOutFinal);
        $('#reportGrid')
			.alternateTableRowColors()
			.hiliteTableRow();

    },
    renderReportInvoicedByProjectClient : function(jsonObject,byWhat) {
        var chartData = jsonObject.ChartData;
        var gridData = jsonObject.GridData;
        var tmpChart = findSWF("chartContainer");
        var x = tmpChart.load( JSON.stringify(chartData) );

        var stringOut = [];        
        stringOut.push('<table class="dataTable" id="reportGrid">');
        stringOut.push('<tbody>');

        for(a=0;a<=((gridData.length)-1);a++) {
            if (gridData[a].Label == 'null' || gridData[a].Label == null) {
                gridData[a].Label = '';
            }

            stringOut.push('<tr>');
            stringOut.push('<td>'+gridData[a].Label+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData[a].Percentage+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData[a].Total+'</td>');
            stringOut.push('</tr>');
        }

        stringOut.push('</tbody>');
        stringOut.push('<tfoot><tr><td>'+commonTerms['total']+'</td><td></td>');
        stringOut.push('<td style="text-align: right;">'+jsonObject['GrandTotal']+'</td></tr></tfoot>');
        stringOut.push('</table>');
        var stringOutFinal = stringOut.join('');
        $('#gridContainer').html(stringOutFinal);
        $('#reportGrid')
			.alternateTableRowColors()
			.hiliteTableRow();
    },
    renderReportIncomeExpenses : function(jsonObject) {
        var chartData = jsonObject.ChartData;
        var gridData = jsonObject.GridData;
        var tmpChart = findSWF("chartContainer");
        var x = tmpChart.load( JSON.stringify(chartData) );

        var stringOut = [];
        var headerArray   = gridData.HeaderLabels.split('|');
        stringOut.push('<table class="dataTable" id="reportGrid">');
        stringOut.push('<thead><tr><th>'+headerArray[0]+'</th><th style="text-align: right;">'+headerArray[1]+'</th><th style="text-align: right;">'+headerArray[2]+'</th></tr></thead>');
        stringOut.push('<tbody>');
        var income, expenses, month, monthNum;       

        for(a=0;a<=11;a++) {
            stringOut.push('<tr>');
            stringOut.push('<td>'+gridData.Expenses[a].Month+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData.Income[a].Total+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData.Expenses[a].Total+'</td>');
            stringOut.push('</tr>');
        }

        stringOut.push('</tbody>');
        stringOut.push('<tfoot><tr>');
        stringOut.push('<td>'+commonTerms['total']+'</td>');
        stringOut.push('<td style="text-align: right;">'+gridData.TotalIncome+'</td>');
        stringOut.push('<td style="text-align: right;">'+gridData.TotalExpenses+'</td>');
        stringOut.push('</tr></tfoot>')
        stringOut.push('</table>');
        var stringOutFinal = stringOut.join('');
        $('#gridContainer').html(stringOutFinal);
        $('#reportGrid')
			.alternateTableRowColors()
			.hiliteTableRow();
    },
    renderReportExpensesBySegment : function(jsonObject,byWhat) {
        var chartData = jsonObject.ChartData;
        var gridData = jsonObject.GridData;
        var tmpChart = findSWF("chartContainer");
        var x = tmpChart.load( JSON.stringify(chartData) );

        var stringOut = [];
        stringOut.push('<table class="dataTable" id="reportGrid">');
        stringOut.push('<tbody>');

        for(a=0;a<=((gridData.length)-1);a++) {
            if (gridData[a].Label == 'null' || gridData[a].Label == null) {
                gridData[a].Label = '';
            }

            stringOut.push('<tr>');
            stringOut.push('<td>'+gridData[a].Label+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData[a].Percentage+'</td>');
            stringOut.push('<td style="text-align: right;">'+gridData[a].Total+'</td>');
            stringOut.push('</tr>');
        }

        stringOut.push('</tbody>');
        stringOut.push('<tfoot><tr><td>'+commonTerms['total']+'</td><td></td>');
        stringOut.push('<td style="text-align: right;">'+jsonObject['GrandTotal']+'</td></tr></tfoot>');
        stringOut.push('</table>');
        var stringOutFinal = stringOut.join('');
        $('#gridContainer').html(stringOutFinal);
        $('#reportGrid')
			.alternateTableRowColors()
			.hiliteTableRow();
    }
}

var reportSupportObj = {
    attachReportChangeHandlers : function() {
        $('#selectReportType').change(function() {
            reportSupportObj.prepInputData();
        });

        $('#selectReportYear').change(function() {
            reportSupportObj.prepInputData();
        });

        $('#submitFinanceReport').click(function() {
            reportSupportObj.prepInputData();
        });
    },
    prepInputData : function() {
        /*
             * Populate date range selectors with dates if empty.
             */
            if ($('#reportDateStart').val() == '') {
                var year = $('#selectReportYear').val();
                $('#reportDateStart').val('01/01/'+year);
                $('#reportDateEnd').val('12/31/'+year);
            }

            var reportType = $('#selectReportType').val();
            var reportYear = $('#selectReportYear').val();
            var reportDateStart = genericFunctions.jsToMySQLDate($('#reportDateStart').val());
            var reportDateEnd = genericFunctions.jsToMySQLDate($('#reportDateEnd').val());

            reportSupportObj.getReportDataFromReportType(reportType, reportYear, reportDateStart, reportDateEnd);
            var itemIDString = reportType+'---'+reportYear+'---'+reportDateStart+'---'+reportDateEnd;
            $('#excelFinanceReport').unbind('click').click(function() {
                exportObj.exportExcel(itemIDString,'FinanceReport','ExportExcel');
                return false;
            });
            $('.buttonPrint').attr('itemID',itemIDString);
    },
    getReportDataFromReportType : function(reportType,reportYear,reportDateStart,reportDateEnd) {
        switch (reportType) {
            case 'repInvoicedReceived':
                getReportDataObj.getReportInvoicedReceived(reportYear);
                $('#selectReportYear').toggle(true);
                $('#reportDateRangeContainer').toggle(false);
                break;
            case 'repIncomeExpenses':
                getReportDataObj.getIncomeExpenses(reportYear);
                $('#selectReportYear').toggle(true);
                $('#reportDateRangeContainer').toggle(false);
                break;
            case 'repInvoicedByProject':
                getReportDataObj.getReportInvoicedByProjectClient(reportDateStart,reportDateEnd,'project');
                $('#selectReportYear').toggle(false);
                $('#reportDateRangeContainer').toggle(true);
                break;
            case 'repInvoicedByClient':
                getReportDataObj.getReportInvoicedByProjectClient(reportDateStart,reportDateEnd,'client');
                $('#selectReportYear').toggle(false);
                $('#reportDateRangeContainer').toggle(true);
                break;           
            case 'repExpensesByProject':
                getReportDataObj.getReportExpenses(reportDateStart,reportDateEnd,'project');
                $('#selectReportYear').toggle(false);
                $('#reportDateRangeContainer').toggle(true);
                break;
            case 'repExpensesByClient':
                getReportDataObj.getReportExpenses(reportDateStart,reportDateEnd,'client');
                $('#selectReportYear').toggle(false);
                $('#reportDateRangeContainer').toggle(true);
                break;
            case 'repExpensesByCategory':
                getReportDataObj.getReportExpenses(reportDateStart,reportDateEnd,'category');
                $('#selectReportYear').toggle(false);
                $('#reportDateRangeContainer').toggle(true);
                break;
            case 'repExpensesByVendor':
                getReportDataObj.getReportExpenses(reportDateStart,reportDateEnd,'vendor');
                $('#selectReportYear').toggle(false);
                $('#reportDateRangeContainer').toggle(true);
                break;
            default:
              getReportDataObj.getReportInvoicedReceived(reportYear);
        }
    }
}