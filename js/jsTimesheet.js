var weekDates, weekdayTabs, timeSheetID = 0;
var currentDate = $('#today').val();
var dayInputTabIndex = 1;
var timesheetEntryArray = [];
var dayHoursArray = [];
var weekHours;
var viewFor = 'entry';

var getTimesheetDataObj = {
	getTimesheetData : function(date,setToday) {
        var ajaxURL = siteURL+'timesheets/TimesheetView/createTimesheetWeek/json/0';
        if (viewFor == 'grid') {
            ajaxURL = siteURL+'timesheets/TimesheetView/createTimesheetWeekGrid/json/0';
        }
		var formHash = {date : date}
		$.ajax({
		    type:        'POST',
		    url:         ajaxURL,
			data:        formHash,
			dataType:    'json',
			success: function(payload){
                if (viewFor == 'entry') {
                    renderTimesheetObj.renderTimesheetEntryWeek(payload);
                    if (setToday == true) {
                        /*
                         * Activate today tab
                         */
                        weekdayTabs.tabs('select', moment().day()-1);
                    } else {
                        /*
                         * Get new tab index so we can activate tab. This is in case
                         * the date was changed. We want to drop the user into the 
                         * date they selected.
                         */
                        var tabIndex = -1;
                        $('#timesheetWeekContainer .tab').each(function() {
                            var tab = $(this);
                            if (tab.attr('data') == date) {
                                tabIndex = $('#timesheetWeekContainer .tab').index(tab);
                            }
                        });
                        weekdayTabs.tabs('select', tabIndex);
                    }
                } else {
                    renderTimesheetObj.renderTimesheetEntryWeekGrid(payload);
                }
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });		
	},
	getTimesheetReportData : function(clientID,projectID,taskID,dateStart,dateEnd,teamMemberID) {
		var formHash = {
						dateStart : dateStart,
						dateEnd   : dateEnd
						}
		$.ajax({
		    type:        'POST',
		    url:         siteURL+'timesheets/TimesheetView/getTimesheetRecords/0/0/'+teamMemberID+'/'+clientID+'/'+projectID+'/'+taskID+'/json/0/1',
			data:        formHash,
			dataType:    'json',
			success: function(payload){
				timesheetReportObj.renderTimesheetReport(payload);
		    },
		    error: function (xhr, desc, exceptionobj) {
				errorObj.ajaxError(xhr,desc,exceptionobj);
			}
	    });
	}
}

var initTimesheetObj = {
	initTimesheetPage : function() {
		timesheetTabs = $('#tabsTimesheet').tabs({
			fx: { opacity: 'toggle' },
			show: function(event,ui) {
				var panel = ui.panel.id;
                if (panel == 'windowTimesheet') {
					/*
					 * Turn pageControls off by default
					 */
					$('#pageControlsContainer').hide();
                    $('#timesheetViewControls').show();
					$('#buttonprint').unbind('click').click(function() {
						var dateStart = $('#dateStart').val();
						var dateEnd   = $('#dateEnd').val();
						var userID    = $('#userID').val();
						var itemIDString = dateStart+'---'+dateEnd+'---'+userID;
						printObj.goPrint(itemIDString,'Timesheet');
					    return false;
					});
					$('#buttonexportExcel').unbind('click').click(function(){
						var dateStart = $('#dateStart').val();
						var dateEnd   = $('#dateEnd').val();
						var userID    = $('#userID').val();
						var itemIDString = dateStart+'---'+dateEnd+'---'+userID;
						exportObj.exportExcel(itemIDString,'TimesheetReport','ExportExcel');
						return false;
					});
                    $('#totalBadgeWeekly').show();
                    $('#totalBadgeReports').hide();
				} else if (panel == 'windowTimesheetReports') {
                    if (permJSON.timesheetViewOwn == 1) {
                        $('#timesheetSearchTeamMember').hide();
                    }

                    $('#timesheetViewControls').hide();
					$('#pageControlsContainer').hide();
					$('#buttonprint').unbind('click').click(function(){
						var taskID = 0,dateStart = 0,dateEnd = 0;
						var clientID  = $('#timesheetSearchClient').val();
						var projectID = $('#timesheetSearchProject').val();

						if ($('#timesheetSearchTask').val() != '' || $('#timesheetSearchTask').val()  == null) {
							taskID    = $('#timesheetSearchTask').val();
						}
						if ($('#timesheetSearchDateStart').val() != '') {
							dateStart = genericFunctions.jsToMySQLDate($('#timesheetSearchDateStart').val());
						}
						if ($('#timesheetSearchDateEnd').val() != '') {
							dateEnd   = genericFunctions.jsToMySQLDate($('#timesheetSearchDateEnd').val());
						}
						var userID    = $('#timesheetSearchTeamMember').val();

						var itemIDString = clientID+'---'+projectID+'---'+taskID+'---'+dateStart+'---'+dateEnd+'---'+userID;
						printObj.goPrint(itemIDString,'ReportTimesheet');
						return false;
					});

					$('#buttonexportExcel').unbind('click').click(function(){
						var taskID = 0,dateStart = 0,dateEnd = 0;
						var clientID  = $('#timesheetSearchClient').val();
						var projectID = $('#timesheetSearchProject').val();
						if ($('#timesheetSearchTask').val() != '') {
							taskID    = $('#timesheetSearchTask').val();
						}
						if ($('#timesheetSearchDateStart').val() != '' || $('#timesheetSearchTask').val()  == null) {
							dateStart = genericFunctions.jsToMySQLDate($('#timesheetSearchDateStart').val());
						}
						if ($('#timesheetSearchDateEnd').val() != '') {
							dateEnd   = genericFunctions.jsToMySQLDate($('#timesheetSearchDateEnd').val());
						}
						var userID    = $('#timesheetSearchTeamMember').val();

						var itemIDString = clientID+'---'+projectID+'---'+taskID+'---'+dateStart+'---'+dateEnd+'---'+userID;
						exportObj.exportExcel(itemIDString,'TimesheetReport','ExportExcel');
						return false;
					});
                    $('#totalBadgeWeekly').hide();
                    $('#totalBadgeReports').show();
				}
			}
		});		

		/*
		 * Attach event handlers
		 */
		$('#goToWeekPrev').click(function() {
			getTimesheetDataObj.getTimesheetData($(this).attr('date'), false);
			return false;
		});

		$('#goToWeekNext').click(function() {
			getTimesheetDataObj.getTimesheetData($(this).attr('date'), false);
			return false;
		});

		$('.dateField').datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

		$('#goToWeek').change(function() {
			getTimesheetDataObj.getTimesheetData($(this).val());
			return false;
		});

        $('#btnViewGrid').click(function() {
            $('#btnViewEntry').toggle();
            $(this).toggle();
            viewFor = 'grid';
            getTimesheetDataObj.getTimesheetData(currentDate);
            $('#timesheetWeekEntryContainer').toggle();
            $('#weekTabs').toggle();
            $('#timesheetViewOnlyGridContainer').toggle();
            return false;
        });

        $('#btnViewEntry').click(function() {
            $(this).toggle();
            viewFor = 'entry';
            getTimesheetDataObj.getTimesheetData(currentDate);
            $('#btnViewGrid').toggle();
            $('#timesheetViewOnlyGridContainer').toggle();
            $('#timesheetWeekEntryContainer').toggle();
            $('#weekTabs').toggle();
            return false;
        });

        /*
         * Time entry form event handlers
         */
        $('#btnAddEntry').click(function() {
            $('#timesheetEntryControl').attr('disabled','disabled');
            $('#mainTimesheetForm').slideDown('fast');
            return false;
        });

        $('#timesheetWeekEntryContainer').delegate('.btnTimeSave', 'click', function() {
            var formObj = $(this).parents('.timesheetEntryForm');
            timesheetUpdateObj.saveTimesheetEntry(formObj);            
            return false;
        });
        
        $('#timesheetWeekEntryContainer').delegate('.txtTime', 'keyup', function(e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                var formObj = $(this).parents('.timesheetEntryForm');
                timesheetUpdateObj.saveTimesheetEntry(formObj);            
                return false;
            }
        });

        $('#timesheetWeekEntryContainer').delegate('.btnTimeCancel', 'click', function() {
            var formObj = $(this).parents('.timesheetEntryForm');
            var timesheetID = formObj.find('.timesheetID').val();
            if (timesheetID > 0) {
                $('#entryContentContainer'+timesheetID).show();
                $('#editFormContainer'+timesheetID).hide().html('');
            } else {
                $('#mainTimesheetForm').slideUp('fast');
                $('#timesheetEntryControl').removeAttr('disabled');
            }
            timesheetToolsObj.clearTimeEntryForm('all');
            
            return false;
        });

        $('#timesheetWeekEntryContainer').delegate('.lblBillable', 'click', function() {
            if ($(this).prev().is(':checked')) {
                $(this).prev().attr('checked','');
            } else {
                $(this).prev().attr('checked','checked');
            }
            return false;
        });

        $('#timesheetWeekEntryContainer').delegate('.selectProject', 'change', function() {
            var formObj   = $(this).parents('.timesheetEntryForm');
            var taskSelectObj = formObj.find('.selectTask');
			var projectID = $(this).val();
            if (projectID>0) {
                $.getJSON(siteURL+'tasks/TaskView/getTasksForSelect/'+projectID+'/json/0', function(payload) {
                    var jsonObject = payload;
                    taskSelectObj.val('');
                    if (jsonObject.length == 0) {
                        taskSelectObj.attr('disabled','disabled');
                    } else {
                        timesheetToolsObj.populateTaskSelect(jsonObject,taskSelectObj);
                    }
                });
            } else {
                $('#selectTask > option').remove();
                $('#selectTask').attr('disabled','disabled');
            }
		});

		$('#selectTask').change(function() {
			var taskID = $(this).val();
		});

        /*
         * Time entry event handlers.
         */
        $('.entryWindow').delegate('.buttonEdit', 'click', function() {
            /*
             * Clear out any other open edit forms.
             */
            $('.editFormContainer').html('').hide();
            $('.entryContentContainer').show();

            var itemID = $(this).closest('div.row').attr('itemID');
            timesheetUpdateObj.renderEditForm(itemID);

            return false;
        });

        $('.entryWindow').delegate('.buttonDelete', 'click', function() {
            var itemID = $(this).closest('div.row').attr('itemID');
            $.get(siteURL+'timesheets/TimesheetView/deleteTimesheetRecord/'+itemID, function() {});
            $(this).closest('div.row').fadeOut('fast',function() {
                $(this).remove();
                timesheetToolsObj.calculateTotalHours();
            });
            
            return false;
        });

		/*
		 * Timesheet report things
		 */
		$('#timesheetSearchProject').change(function() {
			var projectID = $(this).val();
			$.getJSON(siteURL+'tasks/TaskView/getTasksForSelect/'+projectID+'/json/0', function(payload) {
				var jsonObject = payload;
				if (jsonObject.length == 0) {
					$('#timesheetSearchTask').attr('disabled','disabled');
				} else {
					timesheetToolsObj.populateTaskSelect(jsonObject,$('#timesheetSearchTask'));
				}
			});
		});

		$('#timesheetSearchClient').change(function() {
			var clientID = $(this).val();
			$.getJSON(siteURL+'projects/ProjectView/getProjectsForSelect/'+clientID, function(payload) {
				var jsonObject = payload;
				if (jsonObject.length > 0) {
					$('#timesheetSearchProject').removeAttr('disabled');
					timesheetToolsObj.populateProjectSelect(jsonObject,'timesheetSearchProject');
				} else {
					$('#timesheetSearchProject').attr('disabled','disabled');
				}
			});
		});

		/*
		 * Init date pickers
		 */
		$(".dateField").datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
		});

		$('#buttonCreateTimesheetReport').click(function() {
			var clientID     = $('#timesheetSearchClient').val();
			var projectID    = $('#timesheetSearchProject').val();
			var taskID       = $('#timesheetSearchTask').val();
			var dateStart    = $('#timesheetSearchDateStart').val();
			var dateEnd      = $('#timesheetSearchDateEnd').val();
			var teamMemberID = $('#timesheetSearchTeamMember').val();

			getTimesheetDataObj.getTimesheetReportData(clientID,projectID,taskID,dateStart,dateEnd,teamMemberID);
		});
	},
    initTimesheetEntryView : function() {
        /*
		 * Render our week tabs
		 */
        weekdayTabs = $('#timesheetWeekContainer').tabs({
			fx: { opacity: 'toggle' },
			show: function(event,ui) {
				var panel = ui.panel.id;
            },
            select: function(event,ui) {
                var tab = ui.tab.parentNode;
                currentDate = $(tab).attr('data');
            }
        });

		getTimesheetDataObj.getTimesheetData(0,true);        
    }
}

var renderTimesheetObj = {
	renderTimesheetEntryWeek : function(jsonObject) {
        $('#timesheetButtonContainer').hide();
		var prevWeekStart = jsonObject.WeekPrevStart;
		var nextWeekStart = jsonObject.WeekNextStart;
        var timeSheetEntries = jsonObject.TimesheetEntries;
        $('#goToWeekPrev').attr('date',moment(prevWeekStart).format('YYYY-MM-DD'));
		$('#goToWeekNext').attr('date',moment(nextWeekStart).format('YYYY-MM-DD'));

		weekDates = jsonObject.WeekDates;

        var selectedTabIndex = $('#timesheetWeekContainer').tabs('option', 'selected');
        currentDate = weekDates[selectedTabIndex].date;

		/*
		 * Set start|end date hidden fields
		 */
		$('#dateStart').val(weekDates[0].date);
		$('#dateEnd').val(weekDates[6].date);

        /*
         * Render tab titles
         */
        $('#tabMo span').html(weekDates[0].dayDisplay);
        $('#tabTu span').html(weekDates[1].dayDisplay);
        $('#tabWe span').html(weekDates[2].dayDisplay);
        $('#tabTh span').html(weekDates[3].dayDisplay);
        $('#tabFr span').html(weekDates[4].dayDisplay);
        $('#tabSa span').html(weekDates[5].dayDisplay);
        $('#tabSu span').html(weekDates[6].dayDisplay);

        $('#tabMo').attr('data',weekDates[0].date);
        $('#tabTu').attr('data',weekDates[1].date);
        $('#tabWe').attr('data',weekDates[2].date);
        $('#tabTh').attr('data',weekDates[3].date);
        $('#tabFr').attr('data',weekDates[4].date);
        $('#tabSa').attr('data',weekDates[5].date);
        $('#tabSu').attr('data',weekDates[6].date);
        
        $('#windowMo').attr('data',weekDates[0].date);
        $('#windowTu').attr('data',weekDates[1].date);
        $('#windowWe').attr('data',weekDates[2].date);
        $('#windowTh').attr('data',weekDates[3].date);
        $('#windowFr').attr('data',weekDates[4].date);
        $('#windowSa').attr('data',weekDates[5].date);
        $('#windowSu').attr('data',weekDates[6].date);

        /*
         * Render any timesheet entries for the week.
         */
        var c=0;
        for(var a=0;a<=6;a++) {
            var entryDate = weekDates[a].date;
            $('.entryWindow[data="'+entryDate+'"]').html('');
            
            /*
             * Do we have entries for this date?
             */
            if (typeof timeSheetEntries[c] != 'undefined') {
                
                var timeSheetDateArray = timeSheetEntries[c].Date.split(' ');
                var timeSheetDate = timeSheetDateArray[0];
                if (timeSheetDate == entryDate) {
                    var entries = timeSheetEntries[c].Entries;
                    for(var b=0;b<=entries.length-1;b++) {
                        renderTimesheetObj.addEntryToTimesheet(entries[b]);
                    }
                    c++;
                }
            } else {
                $('.entryWindow[data="'+entryDate+'"]').html('');
            }
        }
        /*
         * Get weekly hour totals.
         */
        timesheetToolsObj.calculateTotalHours();
        contactObj.makeContactLinks();
	},
    addEntryToTimesheet : function(jsonObject,editedTimeSheetID) {
        var timeSheetID  = jsonObject.TimeSheetID;
        var projectID    = jsonObject.ProjectID;
        var taskID       = jsonObject.TaskID;
        var projectTitle = jsonObject.ProjectTitle;
        var taskTitle    = jsonObject.TaskTitle;
        var company      = jsonObject.Company;
        var comments     = jsonObject.Comments;
        var billable     = jsonObject.Billable;
        var elapsedTime  = jsonObject.ElapsedTime;
        var entryDateArray = jsonObject.DateClockEnd.split(' ');
        var entryDate      = entryDateArray[0];
        var enteredBy      = jsonObject.NameFirst+' '+jsonObject.NameLast;
        var personID       = jsonObject.PID;

        timesheetEntryArray[timeSheetID] = jsonObject;
        
        var stringOut1 = [], stringOut2 = [], stringOut3 = [];

        var billableString = '';
        if (billable == 1) {
            billableString = '<span class="status_completed">'+commonTerms['billable']+'</span>';
        }

        stringOut1.push('<div class="timesheetRow row entryContentContainer" style="width: 713px; padding: 9px 0 9px;" id="entryContentContainer'+timeSheetID+'" itemID="'+timeSheetID+'">');
        stringOut1.push('<div class="stupidDivForHoverButtons">')
            stringOut2.push('<div style="float: left; width: 40%;">');
                stringOut2.push('<p><strong><a href="'+siteURL+'projects/ProjectDetail/index/'+projectID+'">'+projectTitle+'</a></strong> ');
                if (taskTitle != '' && taskTitle != 'null' && taskTitle != null) {
                    stringOut2.push(' | <a href="'+siteURL+'projects/ProjectDetail/index/'+projectID+'#windowTasks">'+taskTitle+'</a></p>');
                }
                stringOut2.push('<p><a href="'+siteURL+'clients/ClientDetail/index/'+projectID+'" class="grayLink-allCaps">'+company+'</a></p>');
            stringOut2.push('</div>');
            stringOut2.push('<div class="colorBackground light-blue" style="text-align: right; float: left; width: 10%; margin-right: 12px;">');
                stringOut2.push('<span class="bigText"><span class="hours">'+elapsedTime+'</span> '+commonTerms['hrs']+'</span>');
            stringOut2.push('</div>');
            stringOut2.push('<div style="float: left; width: 40%;"><p style="margin-bottom: 6px;">'+comments+'</p>');
            stringOut2.push(billableString+'<span class="subText">'+commonTerms['created_by']+'</span> <a href="#" class="personLink" personid="'+personID+'">'+enteredBy+'</a></div>');
            stringOut2.push('<div style="clear: left;"></div>');
        stringOut3.push('</div></div>');
        stringOut3.push('<div id="editFormContainer'+timeSheetID+'" class="editFormContainer"></div>');

        var stringOutFinal1 = stringOut1.join('');
        var stringOutFinal2 = stringOut2.join('');
        var stringOutFinal3 = stringOut3.join('');
        
        if (editedTimeSheetID > 0) {
            /*
             * We have just edited a timesheet entry.
             */
            $('#entryContentContainer'+editedTimeSheetID+' .stupidDivForHoverButtons').html(stringOutFinal2);
            $('#entryContentContainer'+editedTimeSheetID).show();
            $('#editFormContainer'+editedTimeSheetID).hide();
        } else {
            $('.entryWindow[data="'+entryDate+'"]').append(stringOutFinal1+stringOutFinal2+stringOutFinal3);
        }

        $('.entryWindow[data="'+entryDate+'"]').hoverButtons({
            elementClass:  'timesheetRow',
            width: '50px'
        });
    },
	renderTimesheetEntryWeekGrid : function(jsonObject) {
        $('#timesheetViewOnlyGridContainer').html('');

        var prevWeekStart = jsonObject.WeekPrevStart;
		var nextWeekStart = jsonObject.WeekNextStart;
        var timeSheetEntries = jsonObject.TimesheetEntries;

		$('#goToWeekPrev').attr('date',prevWeekStart);
		$('#goToWeekNext').attr('date',nextWeekStart);

        weekDates = jsonObject.WeekDates;
        currentDate = weekDates[0].date;

        /*
		 * Create week header
		 */
        weekDates = jsonObject.WeekDates;
		var outString = [];
		outString.push('<table id="timesheetTable" class="dataTable"><thead><tr>');
		outString.push('<th style="width: 25%;"></th>');
		for(var i=0;i<=6;i++) {
			outString.push('<th style="width: 9%; font-weight: bold; text-align: right;" class="dateColumn" id="'+weekDates[i].date+'">'+weekDates[i].dayDisplay+'</th>');
		}
		outString.push('<th style="text-align: right; width: 10%; font-weight: normal;"></th>');
		outString.push('</tr></thead><tbody>');

		/*
		 * Total footer
		 */
        outString.push('</tbody><tfoot><tr id="timeSheetFooter" style="display: none;">');
        outString.push('<td style="padding-left: 10px;">'+commonTerms['total']+'</td>');
        for(i=0;i<=6;i++) {
            outString.push('<td style="text-align: right;"><span class="totalDay" id="totalDay_'+weekDates[i].date+'"></span></td>');
        }
        outString.push('<td style="text-align: right;"><span id="grandTotal"></span></td>');
        outString.push('</tr></tfoot>');
		outString.push('</tbody></table>');
		var outStringFinal = outString.join('');
		$('#timesheetViewOnlyGridContainer').html(outStringFinal).show();

		/*
		 * Now add content to Timesheet Table
		 */
		if (jsonObject.TimesheetEntries.length > 0) {
			for(var a in jsonObject.TimesheetEntries) {
				var entryObject = jsonObject.TimesheetEntries[a];
				renderTimesheetObj.addRowToTimesheetGrid(entryObject,weekDates,a);
			}
		} else {
			$('#timesheetTable tbody').append('<tr class="hoverRow" id="noProjectWarningRow"><td colspan="9"><span class="errorMessageSmall">'+commonTerms['no_entries_this_week']+'</span></td></tr>');
			var noRows = 1;
		}

		if (noRows != 1) {
			$('#timesheetTable')
				.alternateTableRowColors()
				.hiliteTableRow();
		}
        timesheetToolsObj.calulateTimesheetTotals();
    },
    addRowToTimesheetGrid : function(jsonObject,weekDates,rowCount) {
		var projectID = '',projectTitle = '',taskID = '',taskTitle = '',elementID = 0;

        var timeSheetID = 0, rowExists = false;

        projectID    = jsonObject.ProjectID;
        taskID       = jsonObject.TaskID;
        projectTitle = jsonObject.ProjectTitle;
        taskTitle    = jsonObject.TaskTitle;

        if (taskTitle != '' && taskTitle != null) {
            var projectString = '<span class="projectName"><strong><a href="'+siteURL+'projects/ProjectDetail/index/'+projectID+'" title="'+projectTitle+'">'+projectTitle+'</a></strong></span><br /><span class="taskName" style="margin-left: 22px;"><a href="'+siteURL+'projects/ProjectDetail/index/'+projectID+'#windowTasks" title="'+taskTitle+'">'+taskTitle+'</a></a></span>';
        } else {
            var projectString = '<span class="projectName"><strong><a href="'+siteURL+'projects/ProjectDetail/index/'+projectID+'" title="'+projectTitle+'">'+projectTitle+'</a></strong></span>';
        }

        var outString = [];
        outString.push('<tr class="timesheetRow">');
        outString.push('<td><div class="td_hide_overflow" style="width: 180px;">');
        outString.push(projectString);
        outString.push('</div></td>');

        var timeValue;
        var commentValue = '';
        var commentIconClass = 'gray';
        var timeSheetID = null;
        for(var i=0;i<=6;i++) {
            timeValue = 0;
            commentValue = '';
            var billableChecked = 'checked="checked"';
            
            var gotHours = 0;
            var gotComment = 0;
            for(var d in jsonObject.Entries) {
                var thisDate = jsonObject.Entries[d].Date;
                var comments = jsonObject.Entries[d].Comments;
                if (thisDate == weekDates[i].date) {
                    timeValue   = parseFloat(timeValue+parseFloat(jsonObject.Entries[d].ElapsedTime));
                    timeSheetID = jsonObject.Entries[d].TimeSheetID;
                    gotHours    = 1;

                    if (comments != null && comments != '') {
                        commentValue = jsonObject.Entries[d].Comments;
                        commentIconClass = '';
                        gotComment = 1;
                    }

                    if (jsonObject.Entries[d].Billable == 0) {
                        billableChecked = '';
                    } else {
                        billableChecked = 'checked="checked"';
                    }
                }
            }
            if (gotHours == 0) {
                timeSheetID = null;
                timeValue = 0;
            }
            if (gotComment == 0) {
                commentValue = '';
                commentIconClass = 'gray';
            }

            var fontFormat = 'subText';
            if (timeValue>0) {
                fontFormat = 'bigText';
            }
            
            outString.push('<td class="elapsedTimeCell '+fontFormat+' hours" style="text-align: right;">'+timeValue+'</td>');
            dayInputTabIndex++;
        }
        outString.push('<td style="text-align: right;"><span class="totalWeek bigText" style="font-weight: bold;"></span></td></tr>');
        dayInputTabIndex++;

        var outStringFinal = outString.join('');
        $('.elapsedTimeCell').formatCurrency({symbol: '',useHtml: true});
        $('#timesheetTable tbody').append(outStringFinal);
        $('#timesheetTable')
            .alternateTableRowColors()
            .hiliteTableRow();
        $('#timeSheetFooter').show();
	},
    attachTimesheetEventHandlers : function() {
        $('.timesheetAddComment').each(function() {
            var commentSaveButton = $('#buttonSaveComment_'+thisElementID+'_'+thisDate+'_'+rowCount);
            var thisID        = $(this).attr('id');
            var thisIDArray   = thisID.split('_');
            var thisElementID = thisIDArray[1];
            var thisDate      = thisIDArray[2];
            var rowCount      = thisIDArray[3];
            var tipContent = $('#commentElement_'+thisElementID+'_'+thisDate+'_'+rowCount).parent().html();
            if (tipContent != null) {
                $(this).qtip({
                    content: {
						text: tipContent
					},
                    position: {
                        corner: {
                           target:  'rightMiddle',
                           tooltip: 'leftMiddle'
                        }
                    },
                    show: {
                       when: {
                         event: 'click'
                       },
                       delay: 0
                    },
                    hide: {
                       when: {
                           event: 'unfocus'
                       },
                       effect: {
                           type: 'fade'
                       }
                    },
                    style: {
                       tip: {
                           corner: 'leftMiddle'
                       },
                       name: 'cream',
                       width: 250,
                       padding: 12
                    },
					api: {
						onRender : function() {
							commentSaveButton = this.elements.content.find('.buttonSaveComment');
							attachSaveButtonEventHandler(this,commentSaveButton);
						}
					}
               });
            }
		});

        $('.inputDateTime').unbind('blur').blur(function() {
            var thisID = $(this).attr('id');
			var thisIDArray   = thisID.split('_');
            var thisElementID = thisIDArray[1];
            var thisDate      = thisIDArray[2];
            var rowCount      = thisIDArray[3];
            var timeSheetID   = $('#timeSheetID_'+thisElementID+'_'+thisDate+'_'+rowCount).val();

            var elapsedTime     = parseFloat($(this).val());
            var projectID       = $('#projectID_'+thisElementID).val();
            var taskID          = $('#taskID_'+thisElementID).val();
            var elementIDString = thisElementID+'_'+thisDate+'_'+rowCount;

            var formHash = {
                    projectID   : projectID,
                    taskID      : taskID,
                    date        : thisDate,
                    elapsedTime : elapsedTime,
                    timeSheetID : timeSheetID,
                    elementID   : thisElementID
            }
            if ( (timeSheetID > 0 || elapsedTime > 0) && elapsedTime != NaN) {
                $.ajax({
                    type:  "POST",
                    url:   siteURL+'timesheets/TimesheetView/saveTimesheetRecordFromTimesheet',
                    data: formHash,
                    dataType: 'json',
                    success: function(payload) {
                        $('#timeSheetID_'+elementIDString).val(payload.TimeSheetID);
                    },
                    error: function (xhr, desc, exceptionobj) {
                        errorObj.ajaxError(xhr,desc,exceptionobj);
                    }
                });
            } else {
                $(this).val('');
            }

			/*
			 * Redo grid calculations
			 */
			timesheetToolsObj.calulateTimesheetTotals();
        });

		function attachSaveButtonEventHandler(popupObj,buttonObj) {
			$(buttonObj).unbind('click').click(function() {
				var thisID = $(this).attr('id');
				var thisIDArray   = thisID.split('_');
				var thisElementID = thisIDArray[1];
				var thisDate      = thisIDArray[2];
                var rowCount      = thisIDArray[3];

				var timeSheetID   = $('#timeSheetID_'+thisElementID+'_'+thisDate+'_'+rowCount).val();
				var comments = '';
				$(this).prevAll().each(function() {
					if (this.tagName == 'TEXTAREA') {
						comments = $(this).val();
					}
				});

				var billable = 0;
				if ($(this).prev().is(':checked')) {
					billable = 1;
				}
				if (timeSheetID>0) {
					var elapsedTime     = parseFloat($(this).val());
					var projectID       = $('#projectID_'+thisElementID).val();
					var taskID          = $('#taskID_'+thisElementID).val();
                    var elementIDString = thisElementID+'_'+thisDate+'_'+rowCount;

					var formHash = {
							elementID   : thisElementID,
							date        : thisDate,
							comments    : comments,
							timeSheetID : timeSheetID,
							billable    : billable
					}
					$.ajax({
						type:  "POST",
						url:   siteURL+'timesheets/TimesheetView/saveTimesheetComments',
						data:  formHash,
						dataType: 'json',
						success: function(payload) {
                            if (comments.length > 0) {
                                $('#commentTriggerElement_'+elementIDString).removeClass('gray');
                            } else {
                                $('#commentTriggerElement_'+elementIDString).addClass('gray');
                            }
						},
						error: function (xhr, desc, exceptionobj) {
							errorObj.ajaxError(xhr,desc,exceptionobj);
						}
					});
				}
                popupObj.hide();
			});
		}

		$('.removeTimesheetEntry').unbind('click').click(function() {
			var elementIDString = $(this).attr('id');
			var elementIDArray  = elementIDString.split('_');
			var elementID       = elementIDArray[1];
            var rowCount        = elementIDArray[2];

			$('#timeSheetRow_'+elementID+'_'+rowCount).fadeOut('fast', function() {
				$('#timeSheetRow_'+elementID+'_'+rowCount).remove();
				$('#timesheetTable')
				.alternateTableRowColors()
				.hiliteTableRow();

				timesheetToolsObj.calulateTimesheetTotals();
			});
			var projectID = $('#projectID_'+elementID).val();
			var taskID    = $('#taskID_'+elementID).val();
			var dateStart = $('#dateStart').val();
			var dateEnd   = $('#dateEnd').val();

			/*
			 * Get timesheet ID's
			 */
			var timeSheetIDString = '';
			$(this).parents('tr').contents().find('.timeSheetIDElement').each(function(){
				if ($(this).val() != 'null') {
					timeSheetIDString += $(this).val()+'|';
				}
			});

			var formHash = {
				timeSheetIDString : timeSheetIDString
			}
			
			$.ajax({
				type:  "POST",
				url:   siteURL+'timesheets/TimesheetView/deleteTimesheetRecord',
				data: formHash,
				success: function(payload) {
					timesheetToolsObj.toggleTimesheetButtons();
				},
				error: function (xhr, desc, exceptionobj) {
					errorObj.ajaxError(xhr,desc,exceptionobj);
				}
			});
			
			return false;
		});
    }
}

var timesheetUpdateObj = {
    saveTimesheetEntry : function(formObj) {
        var billable = 0;
        if (formObj.find('.chkBillable').is(':checked')) {
            billable = 1;
        }
        var elapsedTime = parseFloat(formObj.find('.txtTime').val());
        var formHash = {
                projectID     : formObj.find('.selectProject').val(),
                taskID        : formObj.find('.selectTask').val(),
                timesheetDate : currentDate,
                comments      : formObj.find('.txtComments').val(),
                billable      : billable,
                elapsedTime   : elapsedTime,
                timeSheetID   : formObj.find('.timesheetID').val()
        }
        if (elapsedTime > 0) {
            $.ajax({
                type:  "POST",
                url:   siteURL+'timesheets/TimesheetView/saveTimesheetRecordFromTimesheet',
                data: formHash,
                dataType: 'json',
                success: function(payload) {
                    renderTimesheetObj.addEntryToTimesheet(payload,formObj.find('.timesheetID').val());
                    timesheetToolsObj.clearTimeEntryForm();
                    timesheetToolsObj.calculateTotalHours();
                    getTimesheetDataObj.getTimesheetData(currentDate, false);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
    },
    renderEditForm : function(itemID) {
        var entryContainer = $('#entryContentContainer'+itemID);
        var formContainer  = $('#editFormContainer'+itemID);

        entryContainer.hide();
        formContainer.html($('#mainTimesheetForm').html()).show();
        var projectID = timesheetEntryArray[itemID].ProjectID;
        var taskSelectObj = formContainer.find('.selectTask');

        formContainer.find('.timesheetID').val(itemID);
        formContainer.find('.selectProject').val(projectID);
        
        var txtDate = formContainer.find('.txtDate');
        txtDate.show();
        txtDate.val(moment(currentDate).format('dddd, MMM d'))
        txtDate.datepicker({
		    changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            onSelect: function(date,picker) {
                txtDate.val(moment(date).format('dddd, MMM d'));
                currentDate = moment(date).format('YYYY-MM-DD');
            }
		});

        if (timesheetEntryArray[itemID].TaskID > 0) {
            /*
             * Get tasks for this project and populate task select
             */
            $.getJSON(siteURL+'tasks/TaskView/getTasksForSelect/'+projectID+'/json/0', function(payload) {
                var jsonObject = payload;
                if (jsonObject.length == 0) {
                    taskSelectObj.attr('disabled','disabled');
                } else {
                    timesheetToolsObj.populateTaskSelect(jsonObject,taskSelectObj);
                    taskSelectObj.val(timesheetEntryArray[itemID].TaskID);
                }
            });
        }
        formContainer.find('.txtTime').val(timesheetEntryArray[itemID].ElapsedTime);
        formContainer.find('.txtComments').val(timesheetEntryArray[itemID].Comments);
        if (timesheetEntryArray[itemID].Billable == '1') {
            formContainer.find('.chkBillable').attr('checked','checked');
        } else {
            formContainer.find('.chkBillable').attr('checked','');
        }
    }
}

var timesheetCreateInvoiceObj = {
	createInvoicesFromTimesheet : function() {
		var projectIDString = '';
		var taskIDString = '';
		$('.chkAddToInvoice').each(function() {
			if ($(this).is(':checked')) {
				var projectID = $(this).parents('td').contents().find('.elementProjectID').val();
				var taskID    = $(this).parents('td').contents().find('.elementTaskID').val();
				projectIDString += projectID+'|';
				taskIDString += taskID+'|';
			}
		});

        if (projectIDString.length<1) {
            $('#createInvoicesMessage')
                .show()
                .systemMessage({
                    status: 'information',
                    noFade:  1,
                    size:    'Small',
                    message: commonTerms['timesheets_no_invoice']
                });
        } else {
            $('#createInvoicesMessage').html('');
            var formHash = {
                    projectIDCollection : projectIDString,
                    taskIDCollection    : taskIDString,
                    dateStart           : $('#dateStart').val(),
                    dateEnd             : $('#dateEnd').val()
            }

            $.ajax({
                type:  "POST",
                url:   siteURL+'finances/InvoiceUpdate/createInvoiceFromTimesheet',
                data: formHash,
                dataType: 'json',
                success: function(payload) {
                    var outString = [];
                    var totalInvoices = payload.length
                    for(var a=0;a<=(totalInvoices-1);a++) {
                        outString.push('<strong><a href="'+siteURL+'finances/InvoiceUpdate/index/'+payload[a]['InvoiceID']+'" class="icon_invoice_small" style="margin: 3px;">'+commonTerms['viewInvoiceNumber']+' '+payload[a]['InvoiceNo']+' for '+payload[a]['Company']+'</a></strong>');
                    }
                    var output = outString.join('');
                    $('#createInvoicesMessage').show().append(output);
                },
                error: function (xhr, desc, exceptionobj) {
                    errorObj.ajaxError(xhr,desc,exceptionobj);
                }
            });
        }
	}
}

var timesheetReportObj = {
	renderTimesheetReport : function(jsonObject) {
		if (jsonObject.Clients && jsonObject.Clients.length>0) {
			$('#timesheetReportContainer').html('');
			$('#pageControlsContainer').show();
			var outString = [];

			var numClients = jsonObject.Clients.length;
			var clients = jsonObject.Clients;
			var headers = jsonObject.Headers;
			var totals  = jsonObject.Totals;
            
            $('#totalHoursContainerReports').text(totals.TotalHours);
			for(var a=0;a<=numClients-1;a++) {
				var clientID    = clients[a].ClientID;
				var companyName = clients[a].Company;
				var numProjects = clients[a].Projects.length;
				var projects    = clients[a].Projects;
				outString.push('<div class="barBlue bottom"><div style="padding: 6px;">');
				outString.push('<h2 class="companyName"><a href="'+siteURL+'clients/ClientDetail/index/'+clientID+'">'+companyName+'</a></h2>');
				outString.push('</div></div>');

				for(var b=0;b<=numProjects-1;b++) {
					outString.push('<h4 class="icon_project_small projectTitle" style="margin: 12px 0 12px 12px;"><a href="'+siteURL+'projects/ProjectDetail/index/'+projects[b].ProjectID+'">'+projects[b].ProjectTitle+'</a></h4>');
					outString.push('<table class="dataTable timesheetReportTable padBottom18">');
					if (a==0 || a>0) {
						/*
						 * Render table header
						 */
						outString.push('<thead><tr>');
						outString.push('<th>'+headers.Task+'</th>');
						outString.push('<th>'+headers.TeamMember+'</th>');
						outString.push('<th>'+headers.Billable+'</th>');
						outString.push('<th style="text-align: right;">'+headers.Date+'</th>');
						outString.push('<th style="text-align: right;">'+headers.Hours+'</th>');
						outString.push('</tr></thead>');
					}
					var projectTotalHours = projects[b].ProjectTotalHours;
					var entries = projects[b].Entries;
					for(var c=0;c<=entries.length-1;c++) {
                        var entry = entries[c];
						var timeSheetID = entries[c].TimeSheetID;
						var taskID      = entries[c].TaskID;
						var taskTitle   = entries[c].TaskTitle;
						var elapsedTime = entries[c].ElapsedTime;
						var comments    = entries[c].Comments;
						var billable    = entries[c].Billable;
						var dateTimeSheet = entries[c].DateClockEnd;
						var teamMember    = entries[c].TeamMember;
						var teamMemberID  = entries[c].TeamMemberID;
                        var invoiceID     = entries[c].InvoiceID;

						if (taskTitle == null) {
							var taskString = '';
						} else {
							var taskString = '<p class="icon_tasks_small"><strong>'+taskTitle+'</strong></p>';
						}

						if (comments != null) {
							taskString += comments;
						}

						if (billable == 1) {
							var billableString = '<span class="status_completed">&nbsp;</span>';
						} else {
							var billableString = '';
						}
                        
                        var billedNotice = '';
                        if (invoiceID>0) {
                            billedNotice = '<span style="margin-left: 20px;" class="subText colorBackground label light-blue">Billed invoice ' + entry['InvoiceNo'] + ' on ' + entry['InvoiceDate'] + '</span>';
                        }

						outString.push('<tr>');
						outString.push('<td style="width: 45%;">'+taskString+billedNotice+'</td>');
						outString.push('<td style="width: 20%;"><a href="#" class="personLink" personID="'+teamMemberID+'" id="'+teamMemberID+'">'+teamMember+'</a></td>');
						outString.push('<td style="width: 10%; text-align: center;">'+billableString+'</td>');
						outString.push('<td style="text-align: right;">'+dateTimeSheet+'</td>');
						outString.push('<td style="width: 5%; text-align: right;"><strong>'+elapsedTime+'</strong></td>');
						outString.push('</tr>');

					}
					outString.push('<tfoot><tr><td colspan="4">'+commonTerms['total']+': '+projects[b].ProjectTitle+'</td>');
					outString.push('<td style="text-align: right;" colspan="2">'+projectTotalHours+' '+commonTerms['hrs']+'</td></tr></tfoot>');
					outString.push('</table>');
				}				
			}
            outString.push('<div class="boxBlue bigText" style="padding: 12px 0 12px 0; text-align: right;">'+commonTerms['total_hours']+': <span class="hugeTextEntry">'+totals.TotalHours+'</span> '+commonTerms['hrs']+'</div>');
			var outStringFinal = outString.join('');
			$('#timesheetReportContainer').append(outStringFinal);
			$('.timesheetReportTable')
				.alternateTableRowColors()
				.hiliteTableRow();
            contactObj.makeContactLinks();
		} else {
			$('#pageControlsContainer').hide();
			$('#timesheetReportContainer').html('<span class="errorMessageSmall">'+commonTerms['no_records']+'</span>'+commonTerms['revise_search']);
		}
	}
}

var timesheetToolsObj = {
	populateTaskSelect : function(jsonObject,selectObj) {
		var optionString = '';
        var tasks;
		selectObj.html('');
		optionString  = '<option value="0">'+commonTerms['select_task']+'</option>';
		optionString += '<option value="0"></option>';
		optionString += '<option value="project" class="emph">'+commonTerms['select_project_only']+'</option>';
        optionString += '<option value="0"></option>';
		$(optionString).appendTo(selectObj);
		for(var a=0;a<jsonObject.length;a++) {
            if (jsonObject[a].Milestone == 1) {
                optionString = '<option value="'+jsonObject[a].TaskID+'" class="emph">'+jsonObject[a].Title+'</option>';
                tasks = jsonObject[a].Tasks;

                for(var b=0;b<tasks.length;b++) {
                    optionString += '<option value="'+tasks[b].TaskID+'" class="indent">'+tasks[b].Title+'</option>';
                }
            } else {
                //optionString = '<option value="'+jsonObject[a].TaskID+'" class="indent">'+jsonObject[a].Title+'</option>';
            }
			$(optionString).appendTo(selectObj);
		}
		$(selectObj).removeAttr('disabled');
	},
	populateProjectSelect : function(jsonObject,controlID) {
		var optionString = '';
		$('#'+controlID).html('');
		optionString  = '<option value="0">'+commonTerms['select_project']+'</option>';
		optionString += '<option value="0"></option>';
		$(optionString).appendTo('#'+controlID);
		for(var a=0;a<jsonObject.length;a++) {
			optionString = '<option value="'+jsonObject[a].ProjectID+'">'+jsonObject[a].Title+'</option>';
			$(optionString).appendTo('#'+controlID);
		}
	},
    calulateTimesheetTotals : function() {
		/*
		 * Tally days for this project (end of week total)
		 */
		var dayArray = [0,0,0,0,0,0,0];
		$('.timesheetRow').each(function() {
			var rowTotal = 0;
			var row = $(this);
			$('td.elapsedTimeCell', row).each(function(i) {
				var itemTotal = parseFloat($(this).html());
				if (itemTotal>0) {
					rowTotal = parseFloat(rowTotal+itemTotal);
					dayArray[i] = parseFloat(dayArray[i]+itemTotal);
				}
			});
			$('td span.totalWeek', row).html(rowTotal).formatCurrency({symbol: '',useHtml: true});
		});

		/*
		 * Tally projects for this day (footer total)
		 */
		$('.totalDay').each(function(i) {
			$(this).html(dayArray[i]).formatCurrency({symbol: '',useHtml: true});
		});

		/*
		 * Tally grand total
		 */
		var grandTotal = 0;
		$('.totalWeek').each(function() {
			var weekTotal = parseFloat($(this).html());
			grandTotal = parseFloat(grandTotal+weekTotal);
			$('#grandTotal').html(grandTotal).formatCurrency({symbol: '',useHtml: true});
		});

        timesheetToolsObj.renderTimesheetChart(dayArray);
    },
    calculateTotalHours : function() {
        $('.entryWindow').each(function(i) {
            var day = $(this);
            var dayHours = 0;
			$('.hours', day).each(function() {
				var itemTotal = parseFloat($(this).html());
				if (itemTotal>0) {
					dayHours = parseFloat(dayHours+itemTotal);
				}
			});
            dayHoursArray[i] = dayHours;
        });

        timesheetToolsObj.renderTimesheetChart(dayHoursArray);
    },
    renderTimesheetChart : function(hoursArray) {
        weekHours = 0;
        $('#sparkGraphContainer').sparkline(
            hoursArray, {
                type: 'bar',
                height: '100px',
                chartRangeMin: 0,
                barSpacing: 8,
                barWidth: 25,
                colorMap: chartColors
        });

        for(var a in hoursArray) {
            weekHours = parseFloat(weekHours+hoursArray[a]);
            if (hoursArray[a]>0) {
                $('#dayHoursChart'+a).html(hoursArray[a]);
                $('#dayHoursChart'+a).formatCurrency({symbol: '', useHtml: true});
            } else {
                $('#dayHoursChart'+a).html('&nbsp;');
            }
        }

        $('#totalHoursContainer').html('<span id="weeklyHours" class="hugeTextEntry">'+weekHours+'</span> '+commonTerms['hrs']);
        $('#weeklyHours').formatCurrency({symbol: '', useHtml: true});
    },
	toggleTimesheetButtons : function() {
		if ($('.timesheetRow').size()>0) {
			$('#timesheetButtonContainer').show();
			//$('#pageControlsContainer').show();
		} else {
			$('#timesheetButtonContainer').hide();
			$('#pageControlsContainer').hide();
		}
	},
    clearTimeEntryForm : function(all) {
        if (all == true) {
            $('.selectProject').val('');
            $('.selectTask').val('').attr('disabled','disabled');
        }
        $('.timesheetID').val('0');
        $('.txtTime').val('');
        $('.txtComments').val('');
        $('.chkBillable').attr('checked','checked');
    }
 }

$(document).ready(function() {
	initTimesheetObj.initTimesheetPage();
    initTimesheetObj.initTimesheetEntryView();
		
	var options = {
		grid: {
			color: '#cccccc',
			backgroundColor: '#ffffff',
			borderColor: '#eeeeee',
			tickColor: '#cccccc',
			borderWidth: 1
		}
	};
});