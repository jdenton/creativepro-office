var taskViewObj = {
	init : function() {
		$('#buttonprint').click(function() {
			$('.stuff').each(function() {
				var content = $(this).attr('title')+$(this).html();
				$(this).html(content);
			});
			window.print();
		});

		$('#buttonlogin').click(function() {
			window.location = siteURL+'login';
		});
	}
}

$(document).ready(function() {
	taskViewObj.init();
});